<?php // 팝업창 시작
if (!defined("_WEB_")) exit;
if (!$check_touch) {
?>
<script type="text/javascript">
var zIndex = 1000000;
$(document).ready( function() {

    $('.layout-homepopup').each(function() {

        if ($.cookie('layout-homepopup-'+$(this).attr('rel')) != 'hide') {

            $(this).show().draggable({

                start: function( event, ui ) {

                    var thisZindex = parseInt($(this).css('z-index'));

                    if (thisZindex < zIndex) {

                        zIndex = zIndex + 1;

                    } else {

                        zIndex = thisZindex + 1;

                    }

                    $(this).css({ 'z-index' :  zIndex });

                }

            });

        }

    });

    $('.layout-homepopup .cookieclose').click(function() {

        $('#layout-homepopup-'+$(this).attr('rel')).hide();

        $.cookie('layout-homepopup-'+$(this).attr('rel'), 'hide', { expires: 7, path: '/' });

    });

});
</script>
<?
$list = array();
$result = sql_query(" select * from $web[popup_table] where popup_start <= '".$web['time_ymdhis']."' and popup_end >= '".$web['time_ymdhis']."' ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    if ($row['popup_z']) {

        $row['popup_z'] = 1000000 + $row['popup_z'];

    } else {

        $row['popup_z'] = 1000000;

    }

    $list[$i] = $row;
    $list[$i]['html'] = "";
    $list[$i]['html'] .= "<div id='layout-homepopup-".$row['popup_id']."' rel='".$row['popup_id']."' class='layout-homepopup' style='position:absolute; z-index:".$row['popup_z']."; left:".$row['popup_x']."px; top:".$row['popup_y']."px;'>";
    $list[$i]['html'] .= "<div class='block'>";
    $list[$i]['html'] .= "<div style='width:".$row['popup_width']."px; height:".$row['popup_height']."px;'>";

    if ($row['popup_mode'] == 1) {

        $list[$i]['html'] .= "<div class='text'>";
        $list[$i]['html'] .= text2($row['popup_content'], 1);
        $list[$i]['html'] .= "</div>";

    } else {

        if ($row['popup_url']) {

            $list[$i]['html'] .= "<a href='".url_http($row['popup_url'])."'";

            if ($row['popup_blank']) {

                $list[$i]['html'] .= " target='_blank'";

            }

            $list[$i]['html'] .= ">";

        }

        $list[$i]['html'] .= "<img src='".$disk['server_popup']."/".data_path("u", $row['upload_time'])."/".$row['upload_file']."' alt='' border='0'>";

        if ($row['popup_url']) {

            $list[$i]['html'] .= "</a>";

        }

    }

    $list[$i]['html'] .= "</div>";
    $list[$i]['html'] .= "<p class='closeicon' onclick=\"$('#layout-homepopup-".$row['popup_id']."').hide();\" title='닫기'></p>";
    $list[$i]['html'] .= "<p class='btnclose'>";
    $list[$i]['html'] .= "<span class='cookieclose' rel='".$row['popup_id']."'><span class='icon'></span>일주일간 이 창을 열지않음</span>";
    $list[$i]['html'] .= "<span class='icon2' onclick=\"$('#layout-homepopup-".$row['popup_id']."').hide();\">닫기</span>";
    $list[$i]['html'] .= "</p>";
    $list[$i]['html'] .= "</div>";
    $list[$i]['html'] .= "</div>";

}

for ($i=0; $i<count($list); $i++) {

    echo $list[$i]['html'];

}
}
?>