<?php // 사이드뷰
include_once("./_tb.php");

$mb = member($mid);

if ($mb['mid']) {

    $nick = $mb['nick'];

} else {

    $mid = "";

}

if (!$setup['sideview_onoff'] && !$check_admin) {

    exit;

}

if ($check_admin) {

    $setup['sideview_message'] = true;
    $setup['sideview_email'] = true;
    $setup['sideview_sms'] = true;
    $setup['sideview_homepage'] = true;
    $setup['sideview_profile'] = true;
    $setup['sideview_bbs'] = true;

    $setup['hp_consent'] = true;
    $setup['email_consent'] = true;

}
?>
<div class="block">
<div class="bgt"></div>
<div class="bgi">
<? if ($setup['image_onoff']) { ?>
<div class="member_thumb"><div class="bg"></div><?=member_thumb($mid)?></div>
<? } ?>
<p class="nick" title="<?=text($nick)?>" <? if (!$setup['image_onoff']) { echo "style='padding-bottom:10px;'"; } ?>><?=text($nick)?></p>
</div>
<div class="bgl">
<div class="line"></div>
<? if ($mid && $setup['sideview_message']) { ?><a href="#" onclick="messageRegist('<?=text($mb['mid'])?>'); return false;">메세지 보내기</a><? } ?>
<? if ($mid && $setup['email'] && $setup['email_onoff'] && $setup['sideview_email'] && $mb['email'] && $mb['email_consent']) { ?><a href="#" onclick="emailRegist('<?=text($mb['mid'])?>'); return false;">이메일 보내기</a><? } ?>
<? if ($mid && $setup['sms_onoff'] && $setup['sideview_sms'] && $mb['hp'] && $mb['hp_consent']) { ?><a href="#" onclick="smsRegist('<?=text($mb['mid'])?>'); return false;">문자 보내기</a><? } ?>
<? if ($mid && $setup['sideview_homepage'] && $mb['homepage']) { ?><a href="<?=url_http($mb['homepage'])?>" target="_blank">홈페이지 방문</a><? } ?>
<? if ($mid && $setup['sideview_profile']) { ?><a href="#" onclick="memberProfile('<?=text($mb['mid'])?>'); return false;">회원 프로필</a><? } ?>
<? if ($setup['sideview_bbs'] && $bbs_id) { ?><a href="<?=http_bbs($bbs_id,"")?>&amp;c=3&amp;q=<?=urlencode(text($nick))?>">게시물 보기</a><? } ?>
<? if ($mid && $setup['sideview_bbs']) { ?><a href="<?=$web['host_member']?>/bbs/all_article.php?q=<?=urlencode(text($nick))?>" target="_blank">전체 게시물</a><? } ?>
<? if ($mid && $check_admin) { ?><a href="<?=$web['host_adm']?>/member_form.php?m=u&amp;mid=<?=text($mid)?>" target="_blank">회원정보 수정</a><? } ?>
</div>
<div class="bgb"></div>
</div>