<?php // 브라우저업데이트
include_once("./_tb.php");
?>
<div class="block">
<p class="topline"></p>
<p class="browsertitle">브라우저 업그레이드 안내</p>
<p class="browsertext">IE9 이하 버전의 웹브라우저는 속도 · 보안 · 웹표준에 있어 문제가 발생될 수 있습니다. 아래의 링크를 참고하시어, 최신 버전의 브라우저 설치/사용을 권장드립니다. 감사합니다.</p>
<div class="download">
<a href="http://windows.microsoft.com/ko-kr/internet-explorer/download-ie" title="익스플로러" target="_blank" class="msie"><span class="icon"></span><span class="text">익스플로러</span></a>
<a href="https://www.google.com/intl/ko/chrome/browser/features.html" title="구글 크롬" target="_blank" class="chrome"><span class="icon"></span><span class="text">구글 크롬</span></a>
<a href="https://www.mozilla.org/ko/firefox/channel/" title="파이어폭스" target="_blank" class="firefox"><span class="icon"></span><span class="text">파이어폭스</span></a>
</div>
<p class="btnclose">다음에 설치<span class="icon"></span></p>
</div>
