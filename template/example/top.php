<?php // 상단
if (!defined("_WEB_")) exit;

// 메뉴가 없다면 member으로 기본 지정하기
if (!$web['menu']) {
    $web['menu'] = "member";
}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<?
include_once("$web[path]/_head.php");
?>
<link rel="stylesheet" href="<?=$template_skin_url?>/template.css" type="text/css" />
<script type="text/javascript">
var web_menu = "<?=text($web['menu'])?>";
var web_menusub = "<?=text($web['menusub'])?>";
var template_skin_url = "<?=$template_skin_url?>";
</script>
<script type="text/javascript" src="<?=$template_skin_url?>/template.js"></script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>

여기부터 상단 디자인 HTML 코딩하기
