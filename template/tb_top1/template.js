/*템플릿*/
var layoutSide = false;
$(document).ready( function() {

    var overlay = $("#overlay");

    // 메뉴활성
    var webMenu = function(name) {

        $('.layout-top .menu ul li').removeClass('on');
        $('.layout-top .menu ul li[name="'+name+'"]').addClass('on');
        $('.layout-top .menusub').addClass('on');
        $('.layout-top .menusub ul').hide();
        $('.layout-top .menusub ul[name="'+name+'"]').show();

        $('.layout-side .menu ul li.title').removeClass('on');

        var obj = $('.layout-side .menu ul[name="'+name+'"] .list');

        if (obj.is(':hidden') == true) {

            $('.layout-side .menu ul[name="'+name+'"] li.title').addClass('on');

            $('.layout-side .menu ul .list').hide();

            obj.show();

        } else {

            $('.layout-side .menu ul .list').hide();

        }

    };

    if (web_menu && web_menusub) {

        webMenu(web_menu);

        $('.layout-top .menusub ul li[name="'+web_menusub+'"]').addClass('on');
        $('.layout-side .menu .list li[name="'+web_menusub+'"]').addClass('on');
        $('.layout-side .menubottom a[name="'+web_menusub+'"]').addClass('on');
        $('.layout-wrap').addClass('on');

    }

    // 메뉴클릭
    $('.layout-top .menu ul li').click(function() {

        webMenu($(this).attr('name'));

    });

    $('.layout-side .menu ul li.title').click(function() {

        webMenu($(this).parent('ul').attr('name'));

    });

    // 검색창 활성화
    $('.layout-top .nav .login ul li.btn_search, .layout-top .nav .search ul li.close').click(function() {

        var obj = $('.layout-top .nav .search');

        if (obj.is(':hidden') == true) {

            $('.layout-top .nav .login ul li.btn_search').addClass('on');

            obj.show();
            obj.find('input[name=q]').focus();

        } else {

            $('.layout-top .nav .login ul li.btn_search').removeClass('on');

            obj.hide();

        }

    });

    // 모바일 검색창 활성화
    $('.layout-top .mobile-top .btn_search, .layout-top .mobile-top .search ul li.close').click(function() {

        var obj = $('.layout-top .mobile-top .search');

        if (obj.is(':hidden') == true) {

            $('.layout-top .mobile-top .btn_search').addClass('on');
            obj.show();
            obj.find('input[name=q]').focus();

        } else {

            $('.layout-top .mobile-top .btn_search').removeClass('on');
            obj.hide();

        }

    });

    // 모바일 사이드메뉴
    $('.layout-top .mobile-top .btn_side, .layout-side-close').click(function() {

        if (overlay.is(':hidden') == true) {

            layoutSide = true;

            $('html').css( { 'overflow' : 'hidden'} );
            $('.layout-side').stop().css({'left': '-270px'}).show().animate({'left': '0px'}, 300);
            $('.layout-side-close').show();

            overlay.css( { 'opacity' : '0.5'} );
            overlay.fadeIn(300);

        } else {

            layoutSide = false;

            $('html').css( { 'overflow' : 'visible'} );
            $('.layout-side').stop().animate({'left': '-270px'}, 300);
            $('.layout-side-close').hide();

            overlay.fadeOut(300);

        }

    });

    // 오버레이 클릭하면
    overlay.click(function() {

        if (layoutSide) {

            $('.layout-top .mobile-top .btn_side').click();

        }

    });

    // 모바일 퀵메뉴
    var side_quick = false;

    $('.layout-side .quick').click(function(e) {

        if (!$(e.target).is('.layout-side .quick .list *')) {

            var obj = $('.layout-side .quick .list');
            var obj2 = $('.layout-side .quick .list .block');

            if (side_quick) {

                side_quick = false;

                $('.layout-side .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            } else {

                side_quick = true;

                $('.layout-side .quick .arrow span').addClass('on');

                obj.stop().animate({'height': obj2.height()+'px', 'top': -(obj2.height()+1)+'px'}, 300);

            }

        }

    });

    // 하단 퀵메뉴
    var bottom_quick = false;

    $('.layout-bottom .wrap .quick').click(function(e) {

        if (!$(e.target).is('.layout-bottom .wrap .quick .menu *')) {

            var obj = $('.layout-bottom .wrap .quick .menu');
            var obj2 = $('.layout-bottom .wrap .quick .menu .block');

            if (bottom_quick) {

                bottom_quick = false;

                $('.layout-bottom .wrap .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            } else {

                bottom_quick = true;

                $('.layout-bottom .wrap .quick .arrow span').addClass('on');

                obj.stop().animate({'height': obj2.height()+'px', 'top': -(obj2.height()+1)+'px'}, 300);

            }

        }

    });

    // 탑버튼
    $('.layout-bottom .wrap .btn_top').click(function() {

        pageTop();

    });

    // 소셜로그인
    $('.login-naver').click(function() {

        naverLogin();

    });

    $('.login-kakao').click(function() {

        kakaoLogin();

    });

    $('.login-facebook').click(function() {

        facebookLogin();

    });

    $('.login-twitter').click(function() {

        twitterLogin();

    });

    $('.login-google').click(function() {

        googleLogin();

    });

    $('.login-instagram').click(function() {

        instagramLogin();

    });

    // 스마트기기
    $('.kakao-link').click(function() {

        kakaoLink();

    });

    $('.tel-link').click(function() {

        telLink();

    });

    $('.sms-link').click(function() {

        smsLink();

    });

    // 위치조정
    var webEvent = function() {

        if ($('.layout-top').css('position') == 'absolute') {

            var winTop = $(window).scrollTop();

            if (winTop < 65) {

                $('.layout-top .wrap').css({ 'position' : 'absolute', 'top' : '64px' });

            } else {

                $('.layout-top .wrap').css({ 'top' : '0px' });
                $('.layout-top .wrap').css({ 'position' : 'fixed' });

            }

        }

    };

    $(window).scroll(function() {

        webEvent();

    });

    $(window).resize(function() {

        if ($('.layout-top').css('position') == 'fixed') {

            $('.layout-top .wrap').css({ 'top' : '0px' });

        } else {

            $('.layout-top .wrap').css({ 'top' : '64px' });

        }

        webEvent();

    });

    // 기타이벤트
    $(document).click(function(e) {

        if (!$(e.target).is('.layout-bottom .wrap .quick, .layout-bottom .wrap .quick *, .layout-side .quick, .layout-side .quick *')) {

            if (bottom_quick) {

                var obj = $('.layout-bottom .wrap .quick .menu');
                var obj2 = $('.layout-bottom .wrap .quick .menu .block');

                bottom_quick = false;

                $('.layout-bottom .wrap .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            }

            if (side_quick) {

                var obj = $('.layout-side .quick .list');
                var obj2 = $('.layout-side .quick .list .block');

                side_quick = false;

                $('.layout-side .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            }

        }

    });

});

// 검색포커스
function searchFocus(obj, m, text)
{

    if (m == 'in') {

        if (obj.value == text) {
            obj.value = "";
        }

    } else {

        if (obj.value == '') {
            obj.value = text;
        }

    }

}