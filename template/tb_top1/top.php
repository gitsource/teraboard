<?php // 상단
if (!defined("_WEB_")) exit;

if (!$web['menu']) {
    $web['menu'] = "member";
}

// 메뉴 자동완성(게시판+페이지)
$array = array();
$seq = array();
$result = sql_query(" select * from $web[bbs_group_table] where bbs_group not in ('bottom') and bbs_group_onoff = 1 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $menu_id = $row['bbs_group'];

    $array[$menu_id] = $row;
    $array[$menu_id]['menu_id'] = $row['bbs_group'];
    $array[$menu_id]['title'] = $row['bbs_group_title'];
    $seq[$menu_id] = $row['bbs_group_position']."-".$row['bbs_group_title'];

}

$result = sql_query(" select * from $web[page_group_table] where page_group not in ('bottom') and page_group_onoff = 1 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $menu_id = $row['page_group'];

    $array[$menu_id] = $row;
    $array[$menu_id]['menu_id'] = $row['page_group'];
    $array[$menu_id]['title'] = $row['page_group_title'];
    $seq[$menu_id] = $row['page_group_position']."-".$row['page_group_title'];

}

natsort($seq);

$i = 0;
$menu = array();
foreach ($seq as $key => $val) {

    $menu[$i] = $array[$key];

    $i++;

}

$n = 0;
$menusub = array();
$result = sql_query(" select * from $web[bbs_table] where bbs_onoff = 1 order by bbs_position desc, bbs_title asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $menu_id = $row['bbs_group'];

    $row['menu_id'] = "B-".$row['bbs_id'];
    $row['level'] = $row['bbs_level_list'];
    $row['href'] = http_bbs($row['bbs_id'], "");
    $row['target'] = "";

    if ($row['bbs_start'] == 1) {

        $row['href'] = $web['host_rbbs']."/write.php?bbs_id=".$row['bbs_id'];
        $row['level'] = $row['bbs_level_write'];

    }

    $row['title'] = $row['bbs_title'];
    $row['class'] = "bbs";
    $row['position'] = $row['bbs_position']."-".$row['bbs_title'];

    if ($member['level'] >= $row['level']) {

        $menusub[$menu_id][$n] = $row;
        $n++;

    }

}

$result = sql_query(" select * from $web[page_table] where page_onoff = 1 order by page_position desc, page_title asc ");
for ($i=$n; $row=sql_fetch_array($result); $i++) {

    $menu_id = $row['page_group'];

    $row['menu_id'] = "P-".$row['page_id'];
    $row['level'] = $row['page_level'];
    $row['href'] = $web['host_page']."/".$row['page_id'];
    $row['title'] = $row['page_title'];
    $row['class'] = "page";
    $row['position'] = $row['page_position']."-".$row['page_title'];
    $row['target'] = "";

    if ($row['page_mode'] == 2) {

        $row['href'] = url_http($row['page_url']);

        if ($row['page_blank']) {

            $row['target'] = "target='_blank'";

        }

    }

    if ($member['level'] >= $row['level']) {

        $menusub[$menu_id][$n] = $row;
        $n++;

    }

}

$menu_percent = 100;

if (count($menu)) {

    $menu_percent = number(100 / count($menu),4);

}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<?
include_once("$web[path]/_head.php");
?>
<link rel="stylesheet" href="<?=$template_skin_url?>/template.css" type="text/css" />
<style type="text/css">
@media screen and (max-width:1000px) {

.layout-top .menu ul li {width:<?=$menu_percent?>%;}

}
</style>
<script type="text/javascript">
var web_menu = "<?=text($web['menu'])?>";
var web_menusub = "<?=text($web['menusub'])?>";
var template_skin_url = "<?=$template_skin_url?>";
</script>
<script type="text/javascript" src="<?=$template_skin_url?>/template.js"></script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<header>
<nav>
<!-- layout-top start //-->
<div class="layout-top">
<!-- nav start //-->
<div class="nav">
<div class="block">
<div class="logo"><a href="<?=$web['host_default']?>/"></a></div>
<div class="login<? if ($check_login) { echo " on"; } ?>">
<ul>
<li class="btn_search"></li>
<? if ($check_login) { ?>
<? if ($check_admin) { ?>
<li><a href="<?=$web['host_adm']?>/"><span></span>ADM</a></li>
<? } ?>
<li><a href="<?=$web['host_member']?>/"><span></span>MYPAGE</a></li>
<li><a href="<?=$web['host_member']?>/logout.php"><span></span>LOGOUT</a></li>
<? } else { ?>
<li><a href="<?=$web['host_member']?>/login/?url=<?=text($urlencode)?>"><span></span>LOGIN</a></li>
<li><a href="<?=$web['host_member']?>/join/"><span></span>JOIN</a></li>
<? } ?>
</ul>
</div>
<div class="search">
<form method="get" name="formSearchN" action="<?=$web['host_default']?>/search/" autocomplete="off">
<ul>
<li class="close"></li>
<li class="input"><input type="text" name="q" value="검색어 입력" onfocus="$(this).addClass('on'); searchFocus(this, 'in', '검색어 입력');" onblur="$(this).removeClass('on'); searchFocus(this, 'out', '검색어 입력');" /></li>
<li class="submit"><input type="image" src="<?=$web['host_img']?>/blank.png" /></li>
</ul>
</form>
</div>
<? if ($setup_api['social_login_onoff'] && !$check_login) { ?>
<div class="social">
<ul>
<? if ($setup_api['login_naver']) { ?>
<li class="login-naver" title="네이버"></li>
<? } ?>
<? if ($setup_api['login_kakao']) { ?>
<li class="login-kakao" title="카카오톡"></li>
<? } ?>
<? if ($setup_api['login_facebook']) { ?>
<li class="login-facebook" title="페이스북"></li>
<? } ?>
<? if ($setup_api['login_twitter']) { ?>
<li class="login-twitter" title="트위터"></li>
<? } ?>
<? if ($setup_api['login_google']) { ?>
<li class="login-google" title="구글"></li>
<? } ?>
<? if ($setup_api['login_instagram']) { ?>
<li class="login-instagram" title="인스타그램"></li>
<? } ?>
</ul>
</div>
<? } ?>
</div>
</div>
<!-- nav end //-->
<!-- wrap start //-->
<div class="wrap">
<!-- menu start //-->
<div class="menu">
<ul>
<?
for ($i=0; $i<count($menu); $i++) {

    echo "<li name='".$menu[$i]['menu_id']."'><span title='".text($menu[$i]['title'])."'>".text($menu[$i]['title'])."</span></li>";

}
?>
</ul>
</div>
<!-- menu end //-->
<!-- menusub start //-->
<div class="menusub">
<div class="block">
<?
for ($i=0; $i<count($menu); $i++) {

    $menu_id = $menu[$i]['menu_id'];

    if ($menusub[$menu_id]) {

        echo "<ul name='".$menu_id."'>";

        $n = 0;
        $array = array();
        $seq = array();
        foreach ($menusub[$menu_id] as $row) {

            $array[$n] = $row;
            $seq[$n] = $row['position'];

            $n++;

        }

        $submenu_percent = 100;

        if ($n) {

            $submenu_percent = number(100 / $n, 5);

        }

        natsort($seq);

        foreach ($seq as $key => $val) {

            $row = $array[$key];

            echo "<li name='".text($row['menu_id'])."' style='width:".$submenu_percent."%;'>";
            echo "<a href='".$row['href']."' ".$row['target']." title='".text($row['title'])."'>";
            echo text($row['title']);
            echo "</a>";
            echo "</li>";

        }

        echo "</ul>";

    }

}
?>
</div>
</div>
<!-- menusub end //-->
</div>
<!-- wrap end //-->
<!-- mobile-top start //-->
<div class="mobile-top">
<div class="btn_search"></div>
<div class="btn_side"></div>
<div class="search">
<form method="get" name="formSearchM" action="<?=$web['host_default']?>/search/" autocomplete="off">
<ul>
<li class="input"><input type="text" name="q" value="검색어 입력" onfocus="$(this).addClass('on'); searchFocus(this, 'in', '검색어 입력');" onblur="$(this).removeClass('on'); searchFocus(this, 'out', '검색어 입력');" /></li>
<li class="submit"><input type="image" src="<?=$web['host_img']?>/blank.png" /></li>
<li class="close"></li>
</ul>
</form>
</div>
<a href="<?=$web['host_default']?>/"><?=text($setup['title'])?></a>
</div>
<!-- mobile-top end //-->
</div>
<!-- layout-top end //-->
<!-- layout-side start //-->
<div class="layout-side-close"></div>
<div class="layout-side">
<div class="login">
<? if ($check_login) { ?>
<? if ($check_admin) { ?>
<a href="<?=$web['host_adm']?>/"><span></span>ADM</a>
<? } ?>
<a href="<?=$web['host_member']?>/"><span></span>MYPAGE</a>
<a href="<?=$web['host_member']?>/logout.php"><span></span>LOGOUT</a>
<? } else { ?>
<a href="<?=$web['host_member']?>/login/?url=<?=text($urlencode)?>"><span></span>LOGIN</a>
<? } ?>
</div>
<? if ($setup_api['social_login_onoff'] && !$check_login) { ?>
<div class="social">
<ul>
<? if ($setup_api['login_naver']) { ?>
<li class="login-naver" title="네이버"></li>
<? } ?>
<? if ($setup_api['login_kakao']) { ?>
<li class="login-kakao" title="카카오톡"></li>
<? } ?>
<? if ($setup_api['login_facebook']) { ?>
<li class="login-facebook" title="페이스북"></li>
<? } ?>
<? if ($setup_api['login_twitter']) { ?>
<li class="login-twitter" title="트위터"></li>
<? } ?>
<? if ($setup_api['login_google']) { ?>
<li class="login-google" title="구글"></li>
<? } ?>
<? if ($setup_api['login_instagram']) { ?>
<li class="login-instagram" title="인스타그램"></li>
<? } ?>
</ul>
</div>
<? } ?>
<!-- menu start //-->
<div class="menu">
<?
for ($i=0; $i<count($menu); $i++) {

    $menu_id = $menu[$i]['menu_id'];

    if ($menusub[$menu_id]) {

        echo "<ul name='".$menu_id."'>";
        echo "<li class='title'>".text($menu[$i]['title'])."<span class='arrow'></span></li>";

        echo "<div class='list'>";

        $n = 0;
        $array = array();
        $seq = array();
        foreach ($menusub[$menu_id] as $row) {

            $array[$n] = $row;
            $seq[$n] = $row['position'];

            $n++;

        }

        natsort($seq);

        foreach ($seq as $key => $val) {

            $row = $array[$key];

            echo "<li name='".text($row['menu_id'])."'>";
            echo "<a href='".$row['href']."' ".$row['target']." title='".text($row['title'])."'>";
            echo text($row['title']);
            echo "</a>";
            echo "<span class='arrow'></span></li>";

        }

        echo "</div>";
        echo "</ul>";

    }

}
?>
</div>
<!-- menu end //-->
<!-- tel start //-->
<div class="tel">
<ul class="icon">
<li><span class="kakao-link"></span></li>
<li><span class="sms-link"></span></li>
<li><span class="tel-link"></span></li>
</ul>
<? if ($setup['tel'] || $setup['fax']) { ?>
<ul class="text">
<? if ($setup['tel']) { ?>
<li>대표번호 : <?=text($setup['tel'])?></li>
<? } ?>
<? if ($setup['fax']) { ?>
<li>팩스번호 : <?=text($setup['fax'])?></li>
<? } ?>
</ul>
<? } ?>
</div>
<!-- tel end //-->
<!-- menubottom start //-->
<div class="menubottom">
<?
// 메뉴 자동완성(bottom)
$bottom = array();
$result = sql_query(" select * from $web[bbs_table] where bbs_group = 'bottom' and bbs_onoff = 1 order by bbs_position desc, bbs_title asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $row['menu_id'] = "B-".$row['bbs_id'];
    $row['href'] = http_bbs($row['bbs_id'], "");

    if ($row['bbs_start'] == 1) {

        $row['href'] = $web['host_rbbs']."/write.php?bbs_id=".$row['bbs_id'];

    }

    $row['title'] = $row['bbs_title'];
    $row['position'] = $row['bbs_position']."-".$row['bbs_title'];
    $row['target'] = "";

    $bottom[$i] = $row;

}

$result = sql_query(" select * from $web[page_table] where page_group = 'bottom' and page_onoff = 1 order by page_position desc, page_title asc ");
for ($i=$i; $row=sql_fetch_array($result); $i++) {

    $row['menu_id'] = "P-".$row['page_id'];
    $row['href'] = $web['host_page']."/".$row['page_id'];
    $row['title'] = $row['page_title'];
    $row['position'] = $row['page_position']."-".$row['page_title'];
    $row['target'] = "";

    if ($row['page_mode'] == 2) {

        $row['href'] = url_http($row['page_url']);

        if ($row['page_blank']) {

            $row['target'] = "target='_blank'";

        }

    }

    $bottom[$i] = $row;

}

$n = 0;
$array = array();
$seq = array();
foreach ($bottom as $row) {

    $array[$n] = $row;
    $seq[$n] = $row['position'];

    $n++;

}

natsort($seq);

$i = 0;
foreach ($seq as $key => $val) {

    $row = $array[$key];

    echo "<a name='".text($row['menu_id'])."' href='".$row['href']."' ".$row['target']." title='".text($row['title'])."'>";
    echo text($row['title']);
    echo "</a>";

    $i++;

}
?>
</div>
<!-- menubottom end //-->
<!-- company start //-->
<div class="company">
<?
if ($setup['company']) {

    echo "회사명 : ".text($setup['company']);

}

if ($setup['ceo']) {

    echo " / 대표자명 : ".text($setup['ceo']);

}

if ($setup['company'] || $setup['ceo']) {

    echo "<br />";

}

if ($setup['number1']) {

    echo "사업자등록번호 : ".text($setup['number1']);

    echo "<br />";

}

if ($setup['number2']) {

    echo "통신판매업 신고번호 : ".text($setup['number2']);

    echo "<br />";

}

if ($setup['privace_name']) {

    echo "개인정보 책임자 : ".text($setup['privace_name']);

    echo "<br />";

}

if ($setup['addr']) {

    echo text($setup['addr']);

}
?>
</div>
<!-- company end //-->
<!-- quick start //-->
<div class="quick">
<div class="title">Quick MENU</div>
<div class="arrow"><span></span></div>
<div class="list">
<div class="block">
<ul>
<?
for ($i=0; $i<count($menu); $i++) {

    $menu_id = $menu[$i]['menu_id'];

    if ($menusub[$menu_id]) {

        $n = 0;
        $array = array();
        $seq = array();
        foreach ($menusub[$menu_id] as $row) {

            $array[$n] = $row;
            $seq[$n] = $row['position'];

            $n++;

        }

        natsort($seq);

        foreach ($seq as $key => $val) {

            $row = $array[$key];

            echo "<li class='".$row['class']."' name='".text($row['menu_id'])."'>";
            echo "<a href='".$row['href']."' ".$row['target']." title='".text($row['title'])."'>";
            echo text($row['title']);
            echo "</a>";
            echo "</li>";

        }

    }

}
?>
</ul>
</div>
</div>
</div>
<!-- quick end //-->
<!-- copyright start //-->
<div class="copyright">Copyright © <b><?=text($setup['title'])?></b> Corp.</div>
<!-- copyright end //-->
</div>
<!-- layout-side end //-->
</nav>
</header>
<!-- layout-wrap start //-->
<div class="layout-wrap">
<div class="layout-contents">
<div class="layout-main">