<?php // 하단
if (!defined("_WEB_")) exit;
?>
</div>
</div>
</div>
<!-- layout-wrap end //-->
<!-- layout-bottom start //-->
<?
// 메뉴 자동완성(bottom)
$n = 0;
$bottom = array();
$result = sql_query(" select * from $web[bbs_table] where bbs_group = 'bottom' and bbs_onoff = 1 order by bbs_position desc, bbs_title asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $row['menu_id'] = "B-".$row['bbs_id'];
    $row['level'] = $row['bbs_level_list'];
    $row['href'] = http_bbs($row['bbs_id'], "");

    if ($row['bbs_start'] == 1) {

        $row['href'] = $web['host_rbbs']."/write.php?bbs_id=".$row['bbs_id'];
        $row['level'] = $row['bbs_level_write'];

    }

    $row['title'] = $row['bbs_title'];
    $row['position'] = $row['bbs_position']."-".$row['bbs_title'];
    $row['target'] = "";

    if ($member['level'] >= $row['level']) {

        $bottom[$n] = $row;
        $n++;

    }

}

$result = sql_query(" select * from $web[page_table] where page_group = 'bottom' and page_onoff = 1 order by page_position desc, page_title asc ");
for ($i=$n; $row=sql_fetch_array($result); $i++) {

    $row['menu_id'] = "P-".$row['page_id'];
    $row['level'] = $row['page_level'];
    $row['href'] = $web['host_page']."/".$row['page_id'];
    $row['title'] = $row['page_title'];
    $row['position'] = $row['page_position']."-".$row['page_title'];
    $row['target'] = "";

    if ($row['page_mode'] == 2) {

        $row['href'] = url_http($row['page_url']);

        if ($row['page_blank']) {

            $row['target'] = "target='_blank'";

        }

    }

    if ($member['level'] >= $row['level']) {

        $bottom[$n] = $row;
        $n++;

    }

}
?>
<div class="layout-bottom">
<div class="wrap">
<div class="logo"><a href="<?=$web['host_default']?>/"></a></div>
<!-- list start //-->
<div class="list">
<ul class="link">
<?
$n = 0;
$array = array();
$seq = array();
foreach ($bottom as $row) {

    $array[$n] = $row;
    $seq[$n] = $row['position'];

    $n++;

}

natsort($seq);

$i = 0;
foreach ($seq as $key => $val) {

    $row = $array[$key];

    if ($i > 0) {

        echo "<li class='line'></li>";

    }

    echo "<li class='text' name='".text($row['menu_id'])."'>";
    echo "<a href='".$row['href']."' ".$row['target']." title='".text($row['title'])."'>";
    echo text($row['title']);
    echo "</a>";
    echo "</li>";

    $i++;

}
?>
</ul>
<ul class="info">
<?
if ($setup['company']) {

    echo "<li class='text2'>".text($setup['company'])."</li>";

}

if ($setup['ceo']) {

    echo "<li class='line'></li>";

    echo "<li class='text2 ceo'>대표자 : ".text($setup['ceo']);

    if ($setup['email']) {

        echo "<a href='mailto:".text($setup['email'])."' class='email'></a>";

    }

    echo "</li>";

}

if ($setup['number1']) {

    echo "<li class='br2'></li>";

    echo "<li class='line'></li>";

    echo "<li class='text2'>사업자등록번호 : ".text($setup['number1'])."</li>";

}

if ($setup['number2']) {

    echo "<li class='br2'></li>";

    echo "<li class='line'></li>";

    echo "<li class='text2'>통신판매업신고번호 : ".text($setup['number2'])."</li>";

}

if ($setup['tel']) {

    echo "<li class='br'></li>";
    echo "<li class='br2'></li>";

    echo "<li class='text2'>대표번호 : ".text($setup['tel'])."</li>";

}

if ($setup['fax']) {

    echo "<li class='br2'></li>";

    echo "<li class='line'></li>";

    echo "<li class='text2'>팩스 : ".text($setup['fax'])."</li>";

}

if ($setup['privace_name']) {

    echo "<li class='br2'></li>";

    echo "<li class='line'></li>";

    echo "<li class='text2'>개인정보 책임자 : ".text($setup['privace_name']);

    if ($setup['privace_email']) {

        echo "<a href='mailto:".text($setup['privace_email'])."' class='email'></a>";

    }

    echo "</li>";

}

if ($setup['addr']) {

    echo "<li class='br'></li>";
    echo "<li class='br2'></li>";

    echo "<li class='text2'>".text($setup['addr'])."</li>";

}
?>
</ul>
<ul class="copyright">
<li class='text2'>COPYRIGHT © <?=text($setup['company'])?>. All RIGHTS RESERVED</li>
</ul>
</div>
<!-- list end //-->
<!-- side start //-->
<div class="side">
<!-- quick start //-->
<div class="quick">
<div class="title">Quick MENU</div>
<div class="arrow"><span></span></div>
<div class="menu">
<div class="block">
<ul>
<?
for ($i=0; $i<count($menu); $i++) {

    $menu_id = $menu[$i]['menu_id'];

    if ($menusub[$menu_id]) {

        $n = 0;
        $array = array();
        $seq = array();
        foreach ($menusub[$menu_id] as $row) {

            $array[$n] = $row;
            $seq[$n] = $row['position'];

            $n++;

        }

        natsort($seq);

        foreach ($seq as $key => $val) {

            $row = $array[$key];

            echo "<li class='".$row['class']."' name='".text($row['menu_id'])."'>";
            echo "<a href='".$row['href']."' ".$row['target']." title='".text($row['title'])."'>";
            echo text($row['title']);
            echo "</a>";
            echo "</li>";

        }

    }

}
?>
</ul>
</div>
</div>
</div>
<!-- quick end //-->
</div>
<!-- side end //-->
<div class="btn_top"></div>
</div>
</div>
<!-- layout-bottom end //-->
</body>
</html>