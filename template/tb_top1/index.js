$(document).ready( function() {

    // 브라우저 체크
    if (browserCheck()) {
        return false;
    }

    // 상단배너
    $("#main_top").bannerSlide();

    // 사이드배너
    $("#main_side").bannerSlide({
        time: 5000
    });

    // step3 갤러리
    var step3Thumb = function(obj, m) {

        if ($('.layout-home .step3 .list a .date').is(':hidden') == true) {

            return false;

        }

        if (m == 'over') {

            $(obj).find('span.thumb img').stop().animate({'left': '-50px', 'top': '-35px', 'width': '300px', 'height': '210px'}, 250);

        } else {

            $(obj).find('span.thumb img').stop().animate({'left': '0px', 'top': '0px', 'width': '200px', 'height': '140px'}, 250);

        }

    };

    $('.layout-home .step3 .list a').mouseenter(function() {

        step3Thumb(this, 'over');

    }).mouseleave(function() {

        step3Thumb(this, 'out');

    });

    $(window).resize(function() {

        $('.layout-home .step3 .list a span.thumb img').stop().css({'left': '0px', 'top': '0px'});

        if ($('.layout-home .step3 .list a .date').is(':hidden') == false) {

            $('.layout-home .step3 .list a span.thumb').stop().css({'width': '200px', 'height': '140px'});
            $('.layout-home .step3 .list a span.thumb img').stop().css({'width': '200px', 'height': '140px'});

        } else {

            $('.layout-home .step3 .list a span.thumb').stop().css({'width': '110px', 'height': '75px'});
            $('.layout-home .step3 .list a span.thumb img').stop().css({'width': '110px', 'height': '75px'});

        }

    });

    // 로그인박스
    $('#uid, #upw').focus(function() {

        $(this).parent().addClass('on');
        $('#msg').hide();

    }).blur(function() {

        $(this).parent().removeClass('on');
        $('#msg').hide();

        if ($(this).val() == '') {

            $(this).parent().find('span').show();

        }

    }).keydown(function() {

        $(this).parent().find('span').hide();
 
    });

    $('.uid span, .upw span').click(function() {

        $(this).parent().find('input').focus();

    });

    $('#uid, #upw, .layout-home .login .submit').keydown(function(e) {

        if (e.keyCode == 13) {

            loginCheck();

        }

    });

    $('.layout-home .login .submit').click(function() {

        loginCheck();

    });

    // 소셜로그인
    $('.login-naver').click(function() {

        naverLogin();

    });

    $('.login-kakao').click(function() {

        kakaoLogin();

    });

    $('.login-facebook').click(function() {

        facebookLogin();

    });

    $('.login-twitter').click(function() {

        twitterLogin();

    });

    $('.login-google').click(function() {

        googleLogin();

    });

    $('.login-instagram').click(function() {

        instagramLogin();

    });

    // 주간베스트
    var articleBest = function(n) {

        $('.layout-home .articlebest .block').hide();
        $('.layout-home .articlebest .block[name="'+n+'"]').show();

        $('.layout-home .articlebest .tab span').removeClass('on');
        $('.layout-home .articlebest .tab span[name="'+n+'"]').addClass('on');

        $.post(template_skin_url+"/index.articlebest.php", {"m" : n}, function(data) {

            $('.layout-home .articlebest .block[name="'+n+'"]').html(data);

        });

    };

    articleBest(1);

    $('.layout-home .articlebest .tab span').click(function() {

        articleBest(parseInt($(this).attr('name')));

    });

    // 방문자카운트
    var visitCount = function(n) {

        $('.layout-home .visitcount .tab span').removeClass('on');
        $('.layout-home .visitcount .tab span[name="'+n+'"]').addClass('on');

        $.post(template_skin_url+"/index.visitcount.php", {"m" : n}, function(data) {

            $('#update_data').html(data);

        });

    };

    visitCount(1);

    $('.layout-home .visitcount .tab span').click(function() {

        visitCount(parseInt($(this).attr('name')));

    });

});

function loginCheck()
{

    var f = document.formSideLogin;

    if (f.uid.value == '') {
        alert("아이디를 입력하세요.");
        f.uid.focus();
        return false;
    }

    if (f.upw.value == '') {
        alert("비밀번호를 입력하세요.");
        f.upw.focus();
        return false;
    }

    f.action = web_url+"/member/login/check.php";
    f.submit();

}