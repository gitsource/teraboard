<?php // 홈
if (!defined("_WEB_")) exit;
include_once("$web[path]/_top.php");
?>
<link rel="stylesheet" href="<?=$template_skin_url?>/index.css" type="text/css" />
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.tb-slide.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.banner-slide.js"></script>
<script type="text/javascript" src="<?=$template_skin_url?>/index.js"></script>
<!-- layout-banner start //-->
<div class="layout-banner" id="main_top">
<?
$list = array();
$result = sql_query(" select * from $web[banner_table] where onoff = 1 and group_id = 'main_top' and pc_upload_file != '' order by rand() ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    sql_query(" update $web[banner_table] set hit = hit + 1 where id = '".$row['id']."' ");

    $list[$i] = $row;
    $list[$i]['html'] = "";
    $list[$i]['html'] .= "<div class='block' name='".($i+1)."'";

    if ($row['pc_upload2_file']) {

        $list[$i]['html'] .= " style=\"background:url('".$disk['server_banner']."/".data_path("u", $row['pc_upload2_time'])."/".$row['pc_upload2_file']."') repeat;\"";

    }

    $list[$i]['html'] .= ">";
    $list[$i]['html'] .= "<div class='wrap'>";

    if ($row['pc_url']) {

        $list[$i]['html'] .= "<a href='".url_http($row['pc_url'])."'";

        if ($row['pc_blank']) {

            $list[$i]['html'] .= " target='_blank'";

        }

        $list[$i]['html'] .= " onclick=\"bannerClick('".$row['id']."', '".substr(md5($row['datetime']),0,20)."');\">";

    }

    $list[$i]['html'] .= "<img src='".$disk['server_banner']."/".data_path("u", $row['pc_upload_time'])."/".$row['pc_upload_file']."' alt=''>";

    if ($row['pc_url']) {

        $list[$i]['html'] .= "</a>";

    }

    $list[$i]['html'] .= "</div>";
    $list[$i]['html'] .= "</div>";

}

for ($i=0; $i<count($list); $i++) {

    echo $list[$i]['html'];

}
?>
<? if (count($list) > 1) { ?>
<div class="num">
<ul>
<?
for ($i=0; $i<count($list); $i++) {

    echo "<li name='".($i+1)."'></li>";

}
?>
</ul>
</div>
<? } ?>
</div>
<!-- layout-banner end //-->
<!-- layout-home start //-->
<div class="layout-home<? if ($setup['image_onoff']) { echo " image_onoff"; } ?>">
<div class="wrap">
<?
$bbs_id1 = "bbs1";
$bbs_id2 = "bbs2";
include("$template_skin_path/index.article.php");

$bbs_id1 = "bbs3";
$bbs_id2 = "bbs4";
include("$template_skin_path/index.article.php");

$bbs_id1 = "bbs5";
$bbs_id2 = "bbs6";
include("$template_skin_path/index.article.php");

$bbs_id1 = "bbs7";
$bbs_id2 = "bbs8";
include("$template_skin_path/index.article.php");

$bbs_id1 = "bbs9";
$bbs_id2 = "bbs10";
include("$template_skin_path/index.article.php");
?>
<!-- side start //-->
<div class="side">
<!-- login start //-->
<div class="login">
<p class="title">로그인</p>
<div class="box">
<div class="linebox">
<? if ($check_login) { ?>
<div class="memberblock">
<? if ($setup['image_onoff']) { ?>
<div class="member_thumb"><?=member_thumb($member['mid'])?></div>
<div class="photo_change" onclick="photoOpen('<?=text($member['mid'])?>');" title="사진변경"><span></span></div>
<? } ?>
<div class="list<? if ($setup['image_onoff']) { echo " photo"; } ?>">
<ul>
<li><p><span class="subj">닉네임 :</span><?=text($member['nick'])?></p></li>
<li><p><span class="subj">아이디 :</span><?=text($member['uid'])?></p></li>
<li><p><span class="subj">포인트 :</span><a href="<?=$web['host_member']?>/point/" title="포인트"><?=number($member['point'], $setup['point_number'])?>점</a></p></li>
</ul>
</div>
<div class="loginbtn">
<a href="<?=$web['host_member']?>/modify/" title="정보수정">정보수정</a>
<a href="<?=$web['host_member']?>/" title="마이페이지">마이페이지</a>
<a href="<?=$web['host_member']?>/logout.php" title="로그아웃" class="logout">로그아웃</a>
</div>
</div>
<? if ($member['social'] && !$member['social_uid']) { ?>
<div class="socialmsg">
<a href="<?=$web['host_member']?>/modify/">전환</a>
<span class="text">소셜회원은 일반회원으로 전환하여<br />모든 서비스를 이용하실 수 있어요.</span>
</div>
<?
} else {

// 안 읽음
$chk1 = sql_fetch(" select count(id) as cnt from $web[message_receive_table] where mid = '".$member['mid']."' and type = 0 ");
$chk2 = sql_fetch(" select count(id) as cnt from $web[message_receive_table] where mid = '".$member['mid']."' ");

$member['message_count1'] = $chk1['cnt'];
$member['message_count2'] = $chk2['cnt'];
?>
<div class="countbox">
<ul>
<li><span class="icon1<? if ($member['message_count1']) { echo " on"; } ?>"></span><a href="<?=$web['host_member']?>/message/" title="읽지 않은 메세지" <? if ($member['message_count1']) { echo "class='on'"; } ?>><?=number($member['message_count1'])?></a> / <a href="<?=$web['host_member']?>/message/" title="전체 메세지"><?=number($member['message_count2'])?></a></li>
<li><span class="icon2"></span><a href="<?=$web['host_member']?>/bbs/" title="내 글"><?=number($member['bbs_write_count'])?></a> / <a href="<?=$web['host_member']?>/bbs/reply.php" title="내 댓글"><?=number($member['bbs_reply_count'])?></a></li>
</ul>
</div>
<? } ?>
<? } else { ?>
<div class="loginblock">
<form method="post" name="formSideLogin" autocomplete="off">
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<p class="autologin"><input type="checkbox" id="autologin" name="autologin" value="1" /><label for="autologin">로그인 상태 유지</label></p>
<div class="idpw">
<div class="uid"><input type="text" id="uid" name="uid" value="" tabindex="1" /><span>아이디</span></div>
<div class="upw"><input type="password" id="upw" name="upw" value="" tabindex="2" /><span>비밀번호</span></div>
</div>
<div class="submit"><span tabindex="3">로그인</span></div>
</form>
<div class="join">
<a href="<?=$web['host_member']?>/join/">회원가입</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/id_hp.php">아이디/비밀번호 찾기</a>
</div>
</div>
<? if ($setup_api['social_login_onoff']) { ?>
<div class="social">
<ul>
<? if ($setup_api['login_naver']) { ?>
<li class="login-naver" title="네이버"></li>
<? } ?>
<? if ($setup_api['login_kakao']) { ?>
<li class="login-kakao" title="카카오톡"></li>
<? } ?>
<? if ($setup_api['login_facebook']) { ?>
<li class="login-facebook" title="페이스북"></li>
<? } ?>
<? if ($setup_api['login_twitter']) { ?>
<li class="login-twitter" title="트위터"></li>
<? } ?>
<? if ($setup_api['login_google']) { ?>
<li class="login-google" title="구글"></li>
<? } ?>
<? if ($setup_api['login_instagram']) { ?>
<li class="login-instagram" title="인스타그램"></li>
<? } ?>
</ul>
</div>
<? } ?>
<? } ?>
</div>
</div>
</div>
<!-- login end //-->
<!-- banner start //-->
<div class="banner">
<div class="box" id="main_side">
<?
$list = array();
$result = sql_query(" select * from $web[banner_table] where onoff = 1 and group_id = 'main_side' and pc_upload_file != '' order by rand() ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    sql_query(" update $web[banner_table] set hit = hit + 1 where id = '".$row['id']."' ");

    $list[$i] = $row;
    $list[$i]['html'] = "";
    $list[$i]['html'] .= "<div class='block' name='".($i+1)."'>";

    if ($row['pc_url']) {

        $list[$i]['html'] .= "<a href='".url_http($row['pc_url'])."'";

        if ($row['pc_blank']) {

            $list[$i]['html'] .= " target='_blank'";

        }

        $list[$i]['html'] .= " onclick=\"bannerClick('".$row['id']."', '".substr(md5($row['datetime']),0,20)."');\">";

    }

    $list[$i]['html'] .= "<img src='".$disk['server_banner']."/".data_path("u", $row['pc_upload_time'])."/".$row['pc_upload_file']."' alt=''>";

    if ($row['pc_url']) {

        $list[$i]['html'] .= "</a>";

    }

    $list[$i]['html'] .= "</div>";

}

for ($i=0; $i<count($list); $i++) {

    echo $list[$i]['html'];

}
?>
<? if (count($list) > 1) { ?>
<div class="num">
<ul>
<?
for ($i=0; $i<count($list); $i++) {

    echo "<li name='".($i+1)."'></li>";

}
?>
</ul>
</div>
<? } ?>
</div>
</div>
<!-- banner end //-->

<!-- articlebest start //-->
<div class="articlebest">
<p class="title">주간 베스트</p>
<div class="tab">
<span name="1">조회수</span>
<span name="2">댓글수</span>
<span name="3">추천수</span>
</div>
<div id="articlebest1" class="layer">
<?
$articlebest = 1;
include("$template_skin_path/index.articlebest.php");
?>
</div>
<div id="articlebest2" class="layer">
<?
$articlebest = 2;
include("$template_skin_path/index.articlebest.php");
?>
</div>
<div id="articlebest3" class="layer">
<?
$articlebest = 3;
include("$template_skin_path/index.articlebest.php");
?>
</div>
</div>
<!-- articlebest end //-->
<!-- notice start //-->
<?
$bbs_id = "notice"; // 게시판아이디 입력
$bbs = bbs($bbs_id);

$list = array();

if ($bbs['bbs_id']) {

    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} order by id desc limit 0, 5 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $list[$i] = $row;

        if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'])) {

            $list[$i]['date'] = date("H:i", strtotime($row['datetime']));

        } else {

            $list[$i]['date'] = date("m.d", strtotime($row['datetime']));

        }

    }

} else {

    $bbs['bbs_title'] = "게시판 '".text($bbs_id)."' 없음";

}

if (count($list) < 5) {

    for ($i=count($list); $i<5; $i++) {

        $list[$i]['ar_title'] = "등록된 게시물이 없습니다.";

    }

}
?>
<div class="notice">
<div class="box">
<p class="title"><a href="<?=http_bbs($bbs_id, "")?>" title="<?=text($bbs['bbs_title'])?>"><?=text($bbs['bbs_title'])?></a></p>
<div class="list">
<?
for ($i=0; $i<5; $i++) {

    echo "<a href='".http_bbs($bbs_id, $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>";
    echo text($list[$i]['ar_title']);
    echo "<span class='date'>".$list[$i]['date']."</span>";
    echo "</a>";

}
?>
</div>
</div>
</div>
<!-- notice end //-->
<!-- connect start //-->
<?
$chk = sql_fetch(" select count(id) as total_count from $web[connect_table] ");

$list = array();
$result = sql_query(" select * from $web[connect_table] order by loadtime desc limit 0, 6 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $mb = member($row['mid']);

    $list[$i] = $row;
    $list[$i]['thumb'] = member_thumb($list[$i]['mid']);
    $list[$i]['ip'] = preg_replace("/^([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$/", "\\1.\\2.\\3.***", $row['ip']);

    if ($mb['mid']) {

        $list[$i]['uid'] = $mb['uid'];
        $list[$i]['nick'] = $mb['nick'];

    }

    else if ($row['robot']) {

        $list[$i]['nick'] = "로봇";

    } else {

        $list[$i]['nick'] = "비회원";

    }

}
?>
<div class="connect">
<p class="title"><a href="<?=$web['host_member']?>/connect/" title="현재 접속자">현재 접속자 (<?=number($chk['total_count'])?>)</a></p>
<div>
<ul>
<?
for ($i=0; $i<count($list); $i++) {

    echo "<li>";
    echo "<p>";

    if ($setup['image_onoff']) {

        echo "<span class='member_thumb'>".$list[$i]['thumb']."</span>";

    }

    echo "<span class='block'>";
    echo "<span class='nick'>".text($list[$i]['nick'])."</span>";
    echo "<span class='ip'>".text($list[$i]['ip'])."</span>";
    echo "</span>";
    echo "</p>";
    echo "</li>";

}
?>
</ul>
</div>
<a href="<?=$web['host_member']?>/connect/" class="more">접속자 모두 보기</a>
</div>
<!-- connect end //-->
<!-- visitcount start //-->
<div class="visitcount">
<div class="tab">
<span name="1">방문자</span>
<span name="2">게시물</span>
<span name="3">댓글</span>
</div>
<div class="block">
<ul>
<li><span class="subj">오늘</span><span id="visitcount1" style="color:#ff503f;">0</span></li>
<li><span class="subj">주간</span><span id="visitcount2">0</span></li>
</ul>
<ul>
<li><span class="subj">어제</span><span id="visitcount3">0</span></li>
<li><span class="subj">누적</span><span id="visitcount4">0</span></li>
</ul>
</div>
</div>
<!-- visitcount end //-->
</div>
<!-- side end //-->
</div>
</div>
<!-- layout-home end //-->
<?
include_once("$web[path]/_popup.php");
include_once("$web[path]/_bottom.php");
?>