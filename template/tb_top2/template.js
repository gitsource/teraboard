/*템플릿*/
var layoutSide = false;
$(document).ready( function() {

    var overlay = $("#overlay");

    var webMenu = function(name) {

        $('.layout-top .menu .wrap ul li').removeClass('on');
        $('.layout-top .menu .wrap ul li[name="'+name+'"]').addClass('on');
        $('.layout-top .menusub .wrap ul').removeClass('on');
        $('.layout-top .menusub .wrap ul[name="'+name+'"]').addClass('on');

        $('.layout-side .menu ul li.title').removeClass('on');
        $('.layout-side .menu ul[name="'+name+'"] li.title').addClass('on');
        $('.layout-side .menu ul .list').hide();
        $('.layout-side .menu ul[name="'+name+'"] .list').show();

    };

    $('.layout-top .menu .wrap ul li, .layout-top .menusub .wrap ul').mouseenter(function() {

        $('.layout-top .menusub').show();
        $('.layout-top .menusub .wrap ul').css({ 'height' : ($('.layout-top .menusub .wrap').height())+'px' });

        webMenu($(this).attr('name'));

    });

    // side
    $('.layout-side .menu ul li.title').click(function() {

        webMenu($(this).parent('ul').attr('name'));

    });

    $('.layout-top').mouseleave(function() {

        webMenu(web_menu);
        $('.layout-top .menusub').hide();

    });

    if (web_menu && web_menusub) {

        webMenu(web_menu);

        $('.layout-top .menusub .wrap ul li a[name="'+web_menusub+'"]').addClass('on');
        $('.layout-side .menu .list li[name="'+web_menusub+'"]').addClass('on'); // side

    }

    $('.layout-top .nav .wrap .login ul li.btn_search').click(function() {

        var obj = $('.layout-top .nav .wrap .search');

        if (obj.is(':hidden') == true) {

            obj.show();
            obj.find('input[name=q]').focus();

        } else {

            obj.hide();

        }

    });

    $('.layout-top .nav .wrap .search ul li.close').click(function() {

        $('.layout-top .nav .wrap .search').hide();

    });

    $('.login-naver').click(function() {

        naverLogin();

    });

    $('.login-kakao').click(function() {

        kakaoLogin();

    });

    $('.login-facebook').click(function() {

        facebookLogin();

    });

    $('.login-twitter').click(function() {

        twitterLogin();

    });

    $('.login-google').click(function() {

        googleLogin();

    });

    $('.layout-bottom .wrap .btn_top').click(function() {

        pageTop();

    });

    var bottom_quick = false;

    $('.layout-bottom .wrap .quick').click(function(e) {

        if (!$(e.target).is('.layout-bottom .wrap .quick .menu *')) {

            var obj = $('.layout-bottom .wrap .quick .menu');
            var obj2 = $('.layout-bottom .wrap .quick .menu .block');

            if (bottom_quick) {

                bottom_quick = false;

                $('.layout-bottom .wrap .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            } else {

                bottom_quick = true;

                $('.layout-bottom .wrap .quick .arrow span').addClass('on');

                obj.stop().animate({'height': obj2.height()+'px', 'top': -(obj2.height()+1)+'px'}, 300);

            }

        }

    });

    var side_quick = false;

    $('.layout-side .quick').click(function(e) {

        if (!$(e.target).is('.layout-side .quick .list *')) {

            var obj = $('.layout-side .quick .list');
            var obj2 = $('.layout-side .quick .list .block');

            if (side_quick) {

                side_quick = false;

                $('.layout-side .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            } else {

                side_quick = true;

                $('.layout-side .quick .arrow span').addClass('on');

                obj.stop().animate({'height': obj2.height()+'px', 'top': -(obj2.height()+1)+'px'}, 300);

            }

        }

    });

    // side start
    $('.layout-top .btn-side').click(function() {

        if (overlay.is(':hidden') == true) {

            layoutSide = true;

            $('html').css( { 'overflow' : 'hidden'} );
            $('.layout-top .btn-side').stop().animate({'left': '270px'}, 300).addClass('open');
            $('.layout-side').stop().addClass('open').css({'left': '-270px'}).show().animate({'left': '0px'}, 300);

            overlay.css( { 'opacity' : '0.5'} );
            overlay.fadeIn(300);

        } else {

            layoutSide = false;

            $('html').css( { 'overflow' : 'visible'} );
            $('.layout-top .btn-side').stop().animate({'left': '0'}, 300).removeClass('open');
            $('.layout-side').stop().removeClass('open').animate({'left': '-270px'}, 300);

            overlay.fadeOut(300);

        }

    });

    overlay.click(function() {

        if (layoutSide) {

            $('.layout-top .btn-side').click();

        }

    });

    $('.layout-side .btn-top .btn_search').click(function() {

        var obj = $('.layout-side .btn-top .search');

        if (obj.is(':hidden') == true) {

            obj.show();
            obj.find('input[name=q]').focus();

        } else {

            obj.hide();

        }

    });

    $('.kakao-link').click(function() {

        kakaoLink();

    });

    $('.tel-link').click(function() {

        telLink();

    });

    $('.sms-link').click(function() {

        smsLink();

    });
    // side end

    $(document).click(function(e) {

        if (!$(e.target).is('.layout-bottom .wrap .quick, .layout-bottom .wrap .quick *, .layout-side .quick, .layout-side .quick *')) {

            if (bottom_quick) {

                var obj = $('.layout-bottom .wrap .quick .menu');
                var obj2 = $('.layout-bottom .wrap .quick .menu .block');

                bottom_quick = false;

                $('.layout-bottom .wrap .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            }

            if (side_quick) {

                var obj = $('.layout-side .quick .list');
                var obj2 = $('.layout-side .quick .list .block');

                side_quick = false;

                $('.layout-side .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            }

        }

    });

});

function searchFocus(obj, m, text)
{

    if (m == 'in') {

        if (obj.value == text) {
            obj.value = "";
        }

    } else {

        if (obj.value == '') {
            obj.value = text;
        }

    }

}