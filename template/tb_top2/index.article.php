<?php // 스킨
if (!defined("_WEB_")) exit;
?>
<div class="article">
<!-- bbs1 start //-->
<?
$bbs = bbs($bbs_id1);

$img = array();
$list = array();
$not = array();
$n = 0;

if ($bbs['bbs_id']) {

    $thumb_width = 350;
    $thumb_height = 215;
    $thumb_cut = "sc";

    $thumb_path = $disk['path']."/thumb/main";

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $thumb_path = $thumb_path."/".data_path("", "");

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $result = sql_query(" select * from {$web['article_table']}{$bbs['bbs_id']} where ar_img = 1 order by id desc limit 0, 3 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $not[$i] = $row['id'];
        $img[$i] = $row;

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($bbs['bbs_id'])."' and article_id = '".$row['id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$bbs['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($row['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $img[$i]['thumb'] = $thumb;

        } else {

            $img[$i]['thumb'] = "";

        }

    }

    $result = sql_query(" select * from {$web['article_table']}{$bbs['bbs_id']} order by id desc limit 0, 20 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        if (!in_array($row['id'], $not)) {

            $list[$n] = $row;

            if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'])) {

                $list[$n]['date'] = date("H : i", strtotime($row['datetime']));

            } else {

                $list[$n]['date'] = date("m. d", strtotime($row['datetime']));

            }

            $n++;

        }

        if ($n == 5) {

            break;

        }

    }

} else {

    $bbs['bbs_title'] = "게시판 '".text($bbs_id1)."' 없음";
    $bbs['bbs_id'] = $bbs_id1;

}

if (count($img) < 3) {

    for ($i=count($img); $i<3; $i++) {

        $img[$i]['ar_title'] = "등록된 게시물이 없습니다.";
        $img[$i]['thumb'] = $template_skin_url."/img/home_noimg_w350_h215.png";

    }

}

if (count($list) < 5) {

    for ($i=count($list); $i<5; $i++) {

        $list[$i]['ar_title'] = "등록된 게시물이 없습니다.";

    }

}
?>
<div class="bbs1">
<div class="box">
<p class="bbstitle"><a href="<?=http_bbs($bbs['bbs_id'], "")?>" title="<?=text($bbs['bbs_title'])?>"><?=text($bbs['bbs_title'])?></a></p>
<div class="block">
<!-- list start //-->
<div class="list">
<div>
<?
for ($i=0; $i<5; $i++) {

    echo "<a href='".http_bbs($bbs['bbs_id'], $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>".text($list[$i]['ar_title']);

    if ($list[$i]['ar_reply']) {

        echo "<span class='reply'>(".$list[$i]['ar_reply'].")</span>";

    }

    echo "<span class='date'>".$list[$i]['date']."</span>";

    echo "</a>";

}
?>
</div>
</div>
<!-- list end //-->
<!-- image start //-->
<div class="image">
<?
for ($i=0; $i<count($img); $i++) {

    echo "<a href='".http_bbs($bbs['bbs_id'], $img[$i]['id'])."' title='".text($img[$i]['ar_title'])."'>";
    echo "<span class='line'>";
    echo "<span class='thumb'><img src='".$template_skin_url."/img/home_article_thumb.gif' class='layout'><img src='".$img[$i]['thumb']."' alt='' class='thumb'></span>";
    echo "<span class='title'>".text($img[$i]['ar_title'])."</span>";
    echo "</span>";
    echo "</a>";

}
?>
</div>
<!-- image end //-->
</div>
</div>
</div>
<!-- bbs1 end //-->
<!-- bbs2 start //-->
<?
$bbs = bbs($bbs_id2);

$img = array();
$list = array();
$not = array();
$n = 0;

if ($bbs['bbs_id']) {

    $thumb_width = 350;
    $thumb_height = 215;
    $thumb_cut = "sc";

    $thumb_path = $disk['path']."/thumb/main";

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $thumb_path = $thumb_path."/".data_path("", "");

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $result = sql_query(" select * from {$web['article_table']}{$bbs['bbs_id']} where ar_img = 1 order by id desc limit 0, 3 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $not[$i] = $row['id'];
        $img[$i] = $row;

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($bbs['bbs_id'])."' and article_id = '".$row['id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$bbs['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($row['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $img[$i]['thumb'] = $thumb;

        } else {

            $img[$i]['thumb'] = "";

        }

    }

    $result = sql_query(" select * from {$web['article_table']}{$bbs['bbs_id']} order by id desc limit 0, 20 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        if (!in_array($row['id'], $not)) {

            $list[$n] = $row;

            if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'])) {

                $list[$n]['date'] = date("H : i", strtotime($row['datetime']));

            } else {

                $list[$n]['date'] = date("m. d", strtotime($row['datetime']));

            }

            $n++;

        }

        if ($n == 5) {

            break;

        }

    }

} else {

    $bbs['bbs_title'] = "게시판 '".text($bbs_id2)."' 없음";
    $bbs['bbs_id'] = $bbs_id2;

}

if (count($img) < 3) {

    for ($i=count($img); $i<3; $i++) {

        $img[$i]['ar_title'] = "등록된 게시물이 없습니다.";
        $img[$i]['thumb'] = $template_skin_url."/img/home_noimg_w350_h215.png";

    }

}

if (count($list) < 5) {

    for ($i=count($list); $i<5; $i++) {

        $list[$i]['ar_title'] = "등록된 게시물이 없습니다.";

    }

}
?>
<div class="bbs2">
<div class="box">
<p class="bbstitle"><a href="<?=http_bbs($bbs['bbs_id'], "")?>" title="<?=text($bbs['bbs_title'])?>"><?=text($bbs['bbs_title'])?></a></p>
<div class="block">
<!-- list start //-->
<div class="list">
<div>
<?
for ($i=0; $i<5; $i++) {

    echo "<a href='".http_bbs($bbs['bbs_id'], $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>".text($list[$i]['ar_title']);

    if ($list[$i]['ar_reply']) {

        echo "<span class='reply'>(".$list[$i]['ar_reply'].")</span>";

    }

    echo "<span class='date'>".$list[$i]['date']."</span>";

    echo "</a>";

}
?>
</div>
</div>
<!-- list end //-->
<!-- image start //-->
<div class="image">
<?
for ($i=0; $i<count($img); $i++) {

    echo "<a href='".http_bbs($bbs['bbs_id'], $img[$i]['id'])."' title='".text($img[$i]['ar_title'])."'>";
    echo "<span class='line'>";
    echo "<span class='thumb'><img src='".$template_skin_url."/img/home_article_thumb.gif' class='layout'><img src='".$img[$i]['thumb']."' alt='' class='thumb'></span>";
    echo "<span class='title'>".text($img[$i]['ar_title'])."</span>";
    echo "</span>";
    echo "</a>";

}
?>
</div>
<!-- image end //-->
</div>
</div>
</div>
<!-- bbs2 end //-->
</div>