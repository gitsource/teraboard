<?php // 주간베스트
if (!defined("_WEB_")) exit;

$thumb_width = 100;
$thumb_height = 76;
$thumb_cut = "sc";

$thumb_path = $disk['path']."/thumb/main";

@mkdir($thumb_path, 0707);
@chmod($thumb_path, 0707);

$thumb_path = $thumb_path."/".data_path("", "");

@mkdir($thumb_path, 0707);
@chmod($thumb_path, 0707);

$list = array();

// 주간 게시물이 있나? 없다면 최신순으로 뽑는다.
$chk = sql_fetch(" select count(id) as total_count from $web[search_table] where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' ");

// 대글순
if ($articlebest == '2') {

    if ($chk['total_count']) {

        $result = sql_query(" select * from $web[search_table] where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' order by ar_reply desc limit 0, 16 ");

    } else {

        $result = sql_query(" select * from $web[search_table] order by datetime desc limit 0, 16 ");

    }

    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $article = article($row['bbs_id'], $row['article_id']);

        $list[$i] = $row;
        $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);
        $list[$i]['num'] = $i+1;
        $list[$i]['ar_title'] = $article['ar_title'];
        $list[$i]['count'] = number($row['ar_reply']);

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($row['bbs_id'])."' and article_id = '".$row['article_id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$row['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($article['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $list[$i]['thumb'] = $thumb;

        } else {

            $list[$i]['thumb'] = "";

        }

    }

}

// 추천순
else if ($articlebest == '3') {

    if ($chk['total_count']) {

        $result = sql_query(" select * from $web[search_table] where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' order by ar_good desc limit 0, 16 ");

    } else {

        $result = sql_query(" select * from $web[search_table] order by datetime desc limit 0, 16 ");

    }

    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $article = article($row['bbs_id'], $row['article_id']);

        $list[$i] = $row;
        $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);
        $list[$i]['num'] = $i+1;
        $list[$i]['ar_title'] = $article['ar_title'];
        $list[$i]['count'] = number($row['ar_good']);

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($row['bbs_id'])."' and article_id = '".$row['article_id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$row['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($article['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $list[$i]['thumb'] = $thumb;

        } else {

            $list[$i]['thumb'] = "";

        }

    }

} else {
// 조회순

    if ($chk['total_count']) {

        $result = sql_query(" select * from $web[search_table] where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' order by ar_hit desc limit 0, 16 ");

    } else {

        $result = sql_query(" select * from $web[search_table] order by datetime desc limit 0, 16 ");

    }

    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $article = article($row['bbs_id'], $row['article_id']);

        $list[$i] = $row;
        $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);
        $list[$i]['num'] = $i+1;
        $list[$i]['ar_title'] = $article['ar_title'];
        $list[$i]['count'] = number($row['ar_hit']);

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($row['bbs_id'])."' and article_id = '".$row['article_id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$row['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($article['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $list[$i]['thumb'] = $thumb;

        } else {

            $list[$i]['thumb'] = "";

        }

    }

}

if (count($list) < 16) {

    for ($i=count($list); $i<16; $i++) {

        $list[$i]['href'] = http_bbs($list[$i]['bbs_id'], "");
        $list[$i]['ar_title'] = "게시물이 없습니다.";
        $list[$i]['ar_hit'] = 0;
        $list[$i]['ar_reply'] = 0;
        $list[$i]['ar_good'] = 0;

    }

}

$n = 0;
for ($i=0; $i<4; $i++) {

    echo "<div class='block' name='".($i+1)."'>\n";

    for ($k=0; $k<4; $k++) {

        echo "<a href='".$list[$n]['href']."' title='".text($list[$n]['ar_title'])."'";

        if ($list[$n]['thumb']) {

            echo " class='image'";

        }

        echo ">";

        if ($list[$n]['thumb']) {

            echo "<span class='thumb'><img src='".$list[$n]['thumb']."' alt=''></span>";

        }

        echo "<span class='conts'>";
        echo "<span class='subj'>".text($list[$n]['ar_title'])."</span>";

        echo "<span class='count'>";
        echo "<span class='ar_hit' title='조회'><span></span>".$list[$n]['ar_hit']."</span>";
        echo "<span class='ar_reply' title='댓글'><span></span>".$list[$n]['ar_reply']."</span>";
        echo "<span class='ar_good' title='추천'><span></span>".$list[$n]['ar_good']."</span>";
        echo "</span>";

        echo "</span>";

        echo "</a>";

        $n++;

    }

    echo "</div>\n";

}
?>
<? if (count($list) > 1) { ?>
<div class="num">
<ul>
<?
for ($i=0; $i<4; $i++) {

    echo "<li name='".($i+1)."'></li>";

}
?>
</ul>
</div>
<? } ?>