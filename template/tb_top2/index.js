$(document).ready( function() {

    // 브라우저 체크
    if (browserCheck()) {
        return false;
    }

    // 상단배너
    $("#main_top").bannerSlide();

    // 사이드배너
    $("#main_side").bannerSlide({
        time: 5000
    });

    // 로그인박스
    $('#uid, #upw').focus(function() {

        $(this).parent().addClass('on');
        $('#msg').hide();

    }).blur(function() {

        $(this).parent().removeClass('on');
        $('#msg').hide();

        if ($(this).val() == '') {

            $(this).parent().find('span').show();

        }

    }).keydown(function() {

        $(this).parent().find('span').hide();
 
    });

    $('.uid span, .upw span').click(function() {

        $(this).parent().find('input').focus();

    });

    $('#uid, #upw, .layout-home .login .submit').keydown(function(e) {

        if (e.keyCode == 13) {

            loginCheck();

        }

    });

    $('.layout-home .login .submit').click(function() {

        loginCheck();

    });

    // 소셜로그인
    $('.login-naver').click(function() {

        naverLogin();

    });

    $('.login-kakao').click(function() {

        kakaoLogin();

    });

    $('.login-facebook').click(function() {

        facebookLogin();

    });

    $('.login-twitter').click(function() {

        twitterLogin();

    });

    $('.login-google').click(function() {

        googleLogin();

    });

    $('.login-instagram').click(function() {

        instagramLogin();

    });

    // 주간베스트
    var articleBest = function(n) {

        $('.layout-home .articlebest .tab span').removeClass('on');
        $('.layout-home .articlebest .tab span[name="'+n+'"]').addClass('on');

        $('.layout-home .articlebest .layer').hide();
        $("#articlebest"+n).show();

    };

    $('.layout-home .articlebest .tab span').click(function() {

        articleBest(parseInt($(this).attr('name')));

    });

    articleBest(1);

    $('#articlebest1').tbSlide();
    $('#articlebest2').tbSlide();
    $('#articlebest3').tbSlide();

    // 방문자카운트
    var visitCount = function(n) {

        $('.layout-home .visitcount .tab span').removeClass('on');
        $('.layout-home .visitcount .tab span[name="'+n+'"]').addClass('on');

        $.post(template_skin_url+"/index.visitcount.php", {"m" : n}, function(data) {

            $('#update_data').html(data);

        });

    };

    visitCount(1);

    $('.layout-home .visitcount .tab span').click(function() {

        visitCount(parseInt($(this).attr('name')));

    });

});

function loginCheck()
{

    var f = document.formSideLogin;

    if (f.uid.value == '') {
        alert("아이디를 입력하세요.");
        f.uid.focus();
        return false;
    }

    if (f.upw.value == '') {
        alert("비밀번호를 입력하세요.");
        f.upw.focus();
        return false;
    }

    f.action = web_url+"/member/login/check.php";
    f.submit();

}