/*템플릿*/
var layoutSide = false;
$(document).ready( function() {

    var overlay = $("#overlay");

    var webMenu = function(name) {

        $('.layout-side .menu ul').addClass('on');
        $('.layout-side .menu ul li.title').removeClass('on');
        $('.layout-side .menu ul[name="'+name+'"] li.title').addClass('on');
        $('.layout-side .menu ul .list').hide();
        $('.layout-side .menu ul[name="'+name+'"] .list').show();
        $('.layout-side .menu .list li').removeClass('on');
        $('.layout-side .menu .list li[name="'+name+'"]').addClass('on');

    };

    $('.layout-side .menu ul li.title').click(function() {

        webMenu($(this).parent('ul').attr('name'));

    });

    if (web_menu && web_menusub) {

        webMenu(web_menu);

        $('.layout-side .menu .list li[name="'+web_menusub+'"]').addClass('on');

    }

    $('.layout-side .btn-side').click(function() {

        if ($('.layout-side .btn-top').is(':hidden') == true) {

            $('.layout-side').addClass('open');
            $('.layout-wrap').addClass('open');

        } else {

            $('.layout-side').removeClass('open');
            $('.layout-wrap').removeClass('open');

        }

    });

    $('.layout-top .btn-side').click(function() {

        if (overlay.is(':hidden') == true) {

            layoutSide = true;

            $('html').css( { 'overflow' : 'hidden'} );
            $('.layout-top .btn-side').stop().animate({'left': '270px'}, 300).addClass('open');
            $('.layout-side').stop().addClass('open').css({'left': '-270px'}).show().animate({'left': '0px'}, 300);

            overlay.css( { 'opacity' : '0.5'} );
            overlay.fadeIn(300);

        } else {

            layoutSide = false;

            $('html').css( { 'overflow' : 'visible'} );
            $('.layout-top .btn-side').stop().animate({'left': '0'}, 300).removeClass('open');
            $('.layout-side').stop().removeClass('open').animate({'left': '-270px'}, 300);

            overlay.fadeOut(300);

        }

    });

    overlay.click(function() {

        if (layoutSide) {

            $('.layout-top .btn-side').click();

        }

    });

    $('.layout-side .btn-top .btn_search').click(function() {

        var obj = $('.layout-side .btn-top .search');

        if (obj.is(':hidden') == true) {

            obj.show();
            obj.find('input[name=q]').focus();

        } else {

            obj.hide();

        }

    });

    var bottom_quick = false;

    $('.layout-side .quick').click(function(e) {

        if (!$(e.target).is('.layout-side .quick .list *')) {

            var obj = $('.layout-side .quick .list');
            var obj2 = $('.layout-side .quick .list .block');

            if (bottom_quick) {

                bottom_quick = false;

                $('.layout-side .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            } else {

                bottom_quick = true;

                $('.layout-side .quick .arrow span').addClass('on');

                obj.stop().animate({'height': obj2.height()+'px', 'top': -(obj2.height()+1)+'px'}, 300);

            }

        }

    });

    $('.kakao-link').click(function() {

        kakaoLink();

    });

    $('.tel-link').click(function() {

        telLink();

    });

    $('.sms-link').click(function() {

        smsLink();

    });

    $(document).click(function(e) {

        if (!$(e.target).is('.layout-side .quick, .layout-side .quick *')) {

            if (bottom_quick) {

                var obj = $('.layout-side .quick .list');
                var obj2 = $('.layout-side .quick .list .block');

                bottom_quick = false;

                $('.layout-side .quick .arrow span').removeClass('on');

                obj.stop().animate({'height': '0px', 'top': '0px'}, 300);

            }

        }

    });

    $(window).resize(function() {

        var w = $(document).width();

        if (w > 640) {

            if (layoutSide) {

                layoutSide = false;

                $('html').css( { 'overflow' : 'visible'} );
                $('.layout-top .btn-side').css({'left': '0'}).removeClass('open');
                $('.layout-side').removeClass('open').css({'left': '0px'});

                overlay.fadeOut(300);

            }

        } else {

            if (!layoutSide) {

                layoutSide = true;

                $('.layout-top .btn-side').css({'left': '0'});
                $('.layout-side').css({'left': '-270px'});
                $('.layout-side').removeClass('open');
                $('.layout-wrap').removeClass('open');

            }

        }

    });

});

function searchFocus(obj, m, text)
{

    if (m == 'in') {

        if (obj.value == text) {
            obj.value = "";
        }

    } else {

        if (obj.value == '') {
            obj.value = text;
        }

    }

}