<?php // 홈
if (!defined("_WEB_")) exit;

/*
최신글을 표기하기 위해서 게시판 아이디를 지정합니다.
찾기메뉴를 이용하여 아래와 같은 부분을 찾은 후 게시판아이디를 입력하세요.
$bbs_id = "bbs1"; // 게시판아이디 입력
*/

include_once("$web[path]/_top.php");
?>
<link rel="stylesheet" href="<?=$template_skin_url?>/index.css" type="text/css" />
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.banner-slide.js"></script>
<script type="text/javascript" src="<?=$template_skin_url?>/index.js"></script>
<!-- layout-banner start //-->
<div class="layout-banner" id="main_top">
<?
$list = array();
$result = sql_query(" select * from $web[banner_table] where onoff = 1 and group_id = 'main_top' and pc_upload_file != '' order by rand() ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    sql_query(" update $web[banner_table] set hit = hit + 1 where id = '".$row['id']."' ");

    $list[$i] = $row;
    $list[$i]['html'] = "";
    $list[$i]['html'] .= "<div class='block' name='".($i+1)."'";

    if ($row['pc_upload2_file']) {

        $list[$i]['html'] .= " style=\"background:url('".$disk['server_banner']."/".data_path("u", $row['pc_upload2_time'])."/".$row['pc_upload2_file']."') repeat;\"";

    }

    $list[$i]['html'] .= ">";
    $list[$i]['html'] .= "<div class='wrap'>";

    if ($row['pc_url']) {

        $list[$i]['html'] .= "<a href='".url_http($row['pc_url'])."'";

        if ($row['pc_blank']) {

            $list[$i]['html'] .= " target='_blank'";

        }

        $list[$i]['html'] .= " onclick=\"bannerClick('".$row['id']."', '".substr(md5($row['datetime']),0,20)."');\">";

    }

    $list[$i]['html'] .= "<img src='".$disk['server_banner']."/".data_path("u", $row['pc_upload_time'])."/".$row['pc_upload_file']."' alt=''>";

    if ($row['pc_url']) {

        $list[$i]['html'] .= "</a>";

    }

    $list[$i]['html'] .= "</div>";
    $list[$i]['html'] .= "</div>";

}

for ($i=0; $i<count($list); $i++) {

    echo $list[$i]['html'];

}
?>
<? if (count($list) > 1) { ?>
<div class="num">
<ul>
<?
for ($i=0; $i<count($list); $i++) {

    echo "<li name='".($i+1)."'></li>";

}
?>
</ul>
</div>
<? } ?>
</div>
<!-- layout-banner end //-->
<!-- layout-home start //-->
<div class="layout-home">
<div class="wrap">
<!-- step1 start //-->
<div class="step1">
<!-- bbs1 start //-->
<?
$bbs_id = "bbs1"; // 게시판아이디 입력
$bbs = bbs($bbs_id);

$img = array();
$list = array();
$not = array();
$n = 0;

if ($bbs['bbs_id']) {

    $thumb_width = 350;
    $thumb_height = 215;
    $thumb_cut = "sc";

    $thumb_path = $disk['path']."/thumb/main";

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $thumb_path = $thumb_path."/".data_path("", "");

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} where ar_img = 1 order by id desc limit 0, 2 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $not[$i] = $row['id'];
        $img[$i] = $row;

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($bbs_id)."' and article_id = '".$row['id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($row['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $img[$i]['thumb'] = $thumb;

        } else {

            $img[$i]['thumb'] = "";

        }

    }

    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} order by id desc limit 0, 20 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        if (!in_array($row['id'], $not)) {

            $list[$n] = $row;

            $n++;

        }

        if ($n == 10) {

            break;

        }

    }

} else {

    $bbs['bbs_title'] = "게시판 '".text($bbs_id)."' 없음";

}

if (count($img) < 2) {

    for ($i=count($img); $i<2; $i++) {

        $img[$i]['ar_title'] = "등록된 게시물이 없습니다.";
        $img[$i]['thumb'] = $template_skin_url."/img/home_noimg_w350_h215.png";

    }

}

if (count($list) < 10) {

    for ($i=count($list); $i<10; $i++) {

        $list[$i]['ar_title'] = "등록된 게시물이 없습니다.";

    }

}
?>
<div class="bbs1">
<div class="box">
<p class="bbstitle"><a href="<?=http_bbs($bbs_id, "")?>" title="<?=text($bbs['bbs_title'])?>"><?=text($bbs['bbs_title'])?></a></p>
<div class="block">
<!-- list start //-->
<div class="list">
<div>
<?
for ($i=0; $i<5; $i++) {

    echo "<a href='".http_bbs($bbs_id, $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>".text($list[$i]['ar_title']);

    if ($list[$i]['ar_reply']) {

        echo "<span class='reply'>(".$list[$i]['ar_reply'].")</span>";

    }

    echo "</a>";

}
?>
</div>
<div>
<?
for ($i=5; $i<10; $i++) {

    echo "<a href='".http_bbs($bbs_id, $list[$i]['id'])."'>".text($list[$i]['ar_title']);

    if ($list[$i]['ar_reply']) {

        echo "<span class='reply'>(".$list[$i]['ar_reply'].")</span>";

    }

    echo "</a>";

}
?>
</div>
</div>
<!-- list end //-->
<!-- image start //-->
<div class="image">
<?
for ($i=0; $i<count($img); $i++) {

    echo "<a href='".http_bbs($bbs_id, $img[$i]['id'])."' title='".text($img[$i]['ar_title'])."'>";
    echo "<span class='line'>";
    echo "<span class='thumb'><img src='".$template_skin_url."/img/home_step1_thumb.gif' class='layout'><img src='".$img[$i]['thumb']."' alt='' class='thumb'></span>";
    echo "<span class='title'>".text($img[$i]['ar_title'])."</span>";
    echo "</span>";
    echo "</a>";

}
?>
</div>
<!-- image end //-->
</div>
</div>
</div>
<!-- bbs1 end //-->
<!-- bbs2 start //-->
<?
$bbs_id = "bbs2"; // 게시판아이디 입력
$bbs = bbs($bbs_id);

$img = array();
$list = array();
$not = array();
$n = 0;

if ($bbs['bbs_id']) {

    $thumb_width = 350;
    $thumb_height = 215;
    $thumb_cut = "sc";

    $thumb_path = $disk['path']."/thumb/main";

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $thumb_path = $thumb_path."/".data_path("", "");

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} where ar_img = 1 order by id desc limit 0, 3 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $not[$i] = $row['id'];
        $img[$i] = $row;

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($bbs_id)."' and article_id = '".$row['id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($row['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $img[$i]['thumb'] = $thumb;

        } else {

            $img[$i]['thumb'] = "";

        }

    }

    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} order by id desc limit 0, 20 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        if (!in_array($row['id'], $not)) {

            $list[$n] = $row;

            if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'])) {

                $list[$n]['date'] = date("H : i", strtotime($row['datetime']));

            } else {

                $list[$n]['date'] = date("m. d", strtotime($row['datetime']));

            }

            $n++;

        }

        if ($n == 5) {

            break;

        }

    }

} else {

    $bbs['bbs_title'] = "게시판 '".text($bbs_id)."' 없음";

}

if (count($img) < 3) {

    for ($i=count($img); $i<3; $i++) {

        $img[$i]['ar_title'] = "등록된 게시물이 없습니다.";
        $img[$i]['thumb'] = $template_skin_url."/img/home_noimg_w350_h215.png";

    }

}

if (count($list) < 5) {

    for ($i=count($list); $i<5; $i++) {

        $list[$i]['ar_title'] = "등록된 게시물이 없습니다.";

    }

}
?>
<div class="bbs2">
<div class="box">
<p class="bbstitle"><a href="<?=http_bbs($bbs_id, "")?>" title="<?=text($bbs['bbs_title'])?>"><?=text($bbs['bbs_title'])?></a></p>
<div class="block">
<!-- list start //-->
<div class="list">
<div>
<?
for ($i=0; $i<5; $i++) {

    echo "<a href='".http_bbs($bbs_id, $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>".text($list[$i]['ar_title']);

    if ($list[$i]['ar_reply']) {

        echo "<span class='reply'>(".$list[$i]['ar_reply'].")</span>";

    }

    echo "<span class='date'>".$list[$i]['date']."</span>";

    echo "</a>";

}
?>
</div>
</div>
<!-- list end //-->
<!-- image start //-->
<div class="image">
<?
for ($i=0; $i<count($img); $i++) {

    echo "<a href='".http_bbs($bbs_id, $img[$i]['id'])."' title='".text($img[$i]['ar_title'])."'>";
    echo "<span class='line'>";
    echo "<span class='thumb'><img src='".$template_skin_url."/img/home_step1_thumb.gif' class='layout'><img src='".$img[$i]['thumb']."' alt='' class='thumb'></span>";
    echo "<span class='title'>".text($img[$i]['ar_title'])."</span>";
    echo "</span>";
    echo "</a>";

}
?>
</div>
<!-- image end //-->
</div>
</div>
</div>
<!-- bbs2 end //-->
</div>
<!-- step1 end //-->
<!-- step2 start //-->
<?
$bbs_id = "bbs3"; // 게시판아이디 입력
$bbs = bbs($bbs_id);

$list = array();

if ($bbs['bbs_id']) {

    $thumb_width = 350;
    $thumb_height = 245;
    $thumb_cut = "sc";

    $thumb_path = $disk['path']."/thumb/main";

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $thumb_path = $thumb_path."/".data_path("", "");

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} where ar_img = 1 order by id desc limit 0, 8 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $list[$i] = $row;
        $list[$i]['member_thumb'] = member_thumb($row['mid']);

        if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'])) {

            $list[$i]['date'] = date("H : i", strtotime($row['datetime']));

        } else {

            $list[$i]['date'] = date("m. d", strtotime($row['datetime']));

        }

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($bbs_id)."' and article_id = '".$row['id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($row['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $list[$i]['thumb'] = $thumb;

        } else {

            $list[$i]['thumb'] = "";

        }

    }

} else {

    $bbs['bbs_title'] = "게시판 '".text($bbs_id)."' 없음";

}

if (count($list) < 8) {

    for ($i=count($list); $i<8; $i++) {

        $list[$i]['ar_title'] = "등록된 게시물이 없습니다.";
        $list[$i]['thumb'] = $template_skin_url."/img/home_noimg_w350_h245.png";
        $list[$i]['member_thumb'] = "";

    }

}
?>
<div class="step2">
<div class="box">
<p class="bbstitle"><a href="<?=http_bbs($bbs_id, "")?>" title="<?=text($bbs['bbs_title'])?>"><?=text($bbs['bbs_title'])?></a></p>
<div>
<!-- image start //-->
<div class="image">
<?
for ($i=0; $i<4; $i++) {

    if ($i == 2) {

        echo "<div class='br'></div>";

    }

    echo "<a href='".http_bbs($bbs_id, $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>";
    echo "<span class='block'>";
    echo "<span class='lineout'>";
    echo "<span class='linein'>";
    echo "<span class='thumb'><img src='".$template_skin_url."/img/home_step2_thumb.gif' class='layout'><img src='".$list[$i]['thumb']."' alt='' class='thumb'></span>";
    echo "<span class='title'>";

    echo text($list[$i]['ar_title']);

    if ($list[$i]['ar_reply']) {

        echo "<span class='reply'>(".$list[$i]['ar_reply'].")</span>";

    }

    echo "</span>";
    echo "<span class='member_thumb'>".$list[$i]['member_thumb']."</span>";
    echo "<span class='nick";

    if ($setup['image_onoff']) {
        echo " photo";
    }

    echo "'>".text($list[$i]['nick'])."</span>";
    echo "<span class='date'>".$list[$i]['date']."</span>";
    echo "<span class='content'>".text3($list[$i]['ar_content'])."</span>";
    echo "</span>";
    echo "</span>";
    echo "</span>";
    echo "</a>";

}
?>
</div>
<!-- image end //-->
<!-- image start //-->
<div class="image">
<?
for ($i=$i; $i<8; $i++) {

    if ($i == 2) {

        echo "<div class='br'></div>";

    }

    echo "<a href='".http_bbs($bbs_id, $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>";
    echo "<span class='block'>";
    echo "<span class='lineout'>";
    echo "<span class='linein'>";
    echo "<span class='thumb'><img src='".$template_skin_url."/img/home_step2_thumb.gif' class='layout'><img src='".$list[$i]['thumb']."' alt='' class='thumb'></span>";
    echo "<span class='title'>";

    echo text($list[$i]['ar_title']);

    if ($list[$i]['ar_reply']) {

        echo "<span class='reply'>(".$list[$i]['ar_reply'].")</span>";

    }

    echo "</span>";
    echo "<span class='member_thumb'>".$list[$i]['member_thumb']."</span>";
    echo "<span class='nick";

    if ($setup['image_onoff']) {
        echo " photo";
    }

    echo "'>".text($list[$i]['nick'])."</span>";
    echo "<span class='date'>".$list[$i]['date']."</span>";
    echo "<span class='content'>".text3($list[$i]['ar_content'])."</span>";
    echo "</span>";
    echo "</span>";
    echo "</span>";
    echo "</a>";

}
?>
</div>
<!-- image end //-->
</div>
</div>
</div>
<!-- step2 end //-->
<!-- step3 start //-->
<?
$bbs_id = "bbs4"; // 게시판아이디 입력
$bbs = bbs($bbs_id);

$list = array();

if ($bbs['bbs_id']) {

    $thumb_width = 300;
    $thumb_height = 210;
    $thumb_cut = "sc";

    $thumb_path = $disk['path']."/thumb/main";

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $thumb_path = $thumb_path."/".data_path("", "");

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} where ar_img = 1 order by id desc limit 0, 6 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $list[$i] = $row;
        $list[$i]['member_thumb'] = member_thumb($list[$i]['mid']);

        if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'])) {

            $list[$i]['date'] = date("H : i", strtotime($row['datetime']));

        } else {

            $list[$i]['date'] = date("m. d", strtotime($row['datetime']));

        }

        $source = "";
        $thumb_file = "";
        $thumb = "";

        $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($bbs_id)."' and article_id = '".$row['id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

        if ($file['upload_file']) {

            $thumb_file = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
            $thumb = $thumb_path."/".$thumb_width."x".$thumb_height."_".$file['upload_file'];

            if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

                image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

            }

            $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        }

        if (!$thumb) {

            $thumb = bbs_thumb_path($row['ar_content'], $thumb_path, $thumb_width, $thumb_height, $thumb_cut, 100);

        }

        if ($thumb) {

            $list[$i]['thumb'] = $thumb;

        } else {

            $list[$i]['thumb'] = "";

        }

    }

} else {

    $bbs['bbs_title'] = "게시판 '".text($bbs_id)."' 없음";

}

if (count($list) < 6) {

    for ($i=count($list); $i<6; $i++) {

        $list[$i]['ar_title'] = "등록된 게시물이 없습니다.";
        $list[$i]['thumb'] = $template_skin_url."/img/home_noimg_w300_h210.png";
        $list[$i]['member_thumb'] = "";
        $list[$i]['date'] = "&nbsp;";

    }

}
?>
<div class="step3">
<div class="box">
<p class="bbstitle"><a href="<?=http_bbs($bbs_id, "")?>" title="<?=text($bbs['bbs_title'])?>"><?=text($bbs['bbs_title'])?></a></p>
<div class="list">
<ul>
<?
for ($i=0; $i<count($list); $i++) {

    echo "<li>";
    echo "<a href='".http_bbs($bbs_id, $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>";
    echo "<span class='lineout'>";
    echo "<span class='linein'>";
    echo "<span class='thumb'><img src='".$list[$i]['thumb']."' alt=''></span>";
    echo "<span class='conts'>";
    echo "<span class='title'>";

    echo text($list[$i]['ar_title']);

    if ($list[$i]['ar_reply']) {

        echo "<span class='reply'>(".$list[$i]['ar_reply'].")</span>";

    }

    echo "</span>";
    echo "<span class='content'>".text3($list[$i]['ar_content'])."</span>";
    echo "<span class='member_thumb'>".$list[$i]['member_thumb']."</span>";
    echo "<span class='nick";

    if ($setup['image_onoff']) {
        echo " photo";
    }

    echo "'>".text($list[$i]['nick'])."</span>";
    echo "<span class='date'>".$list[$i]['date']."</span>";
    echo "</span>";
    echo "</span>";
    echo "</span>";
    echo "</a>";
    echo "</li>";

}
?>
</ul>
</div>
</div>
</div>
<!-- step3 end //-->
<!-- side start //-->
<div class="side">
<!-- login start //-->
<div class="login">
<p class="title">로그인</p>
<div class="box">
<div class="linebox">
<? if ($check_login) { ?>
<div class="memberblock">
<? if ($setup['image_onoff']) { ?>
<div class="member_thumb"><?=member_thumb($member['mid'])?></div>
<div class="photo_change" onclick="photoOpen('<?=text($member['mid'])?>');" title="사진변경"><span></span></div>
<? } ?>
<div class="list<? if ($setup['image_onoff']) { echo " photo"; } ?>">
<ul>
<li><p><span class="subj">닉네임 :</span><?=text($member['nick'])?></p></li>
<li><p><span class="subj">아이디 :</span><?=text($member['uid'])?></p></li>
<li><p><span class="subj">포인트 :</span><a href="<?=$web['host_member']?>/point/" title="포인트"><?=number($member['point'], $setup['point_number'])?>점</a></p></li>
</ul>
</div>
<div class="loginbtn">
<a href="<?=$web['host_member']?>/modify/" title="정보수정">정보수정</a>
<a href="<?=$web['host_member']?>/" title="마이페이지">마이페이지</a>
<a href="<?=$web['host_member']?>/logout.php" title="로그아웃" class="logout">로그아웃</a>
</div>
</div>
<? if ($member['social'] && !$member['social_uid']) { ?>
<div class="socialmsg">
<a href="<?=$web['host_member']?>/modify/">전환</a>
<span class="text">소셜회원은 일반회원으로 전환하여<br />모든 서비스를 이용하실 수 있어요.</span>
</div>
<?
} else {

// 안 읽음
$chk1 = sql_fetch(" select count(id) as cnt from $web[message_receive_table] where mid = '".$member['mid']."' and type = 0 ");
$chk2 = sql_fetch(" select count(id) as cnt from $web[message_receive_table] where mid = '".$member['mid']."' ");

$member['message_count1'] = $chk1['cnt'];
$member['message_count2'] = $chk2['cnt'];
?>
<div class="countbox">
<ul>
<li><span class="icon1<? if ($member['message_count1']) { echo " on"; } ?>"></span><a href="<?=$web['host_member']?>/message/" title="읽지 않은 메세지" <? if ($member['message_count1']) { echo "class='on'"; } ?>><?=number($member['message_count1'])?></a> / <a href="<?=$web['host_member']?>/message/" title="전체 메세지"><?=number($member['message_count2'])?></a></li>
<li><span class="icon2"></span><a href="<?=$web['host_member']?>/bbs/" title="내 글"><?=number($member['bbs_write_count'])?></a> / <a href="<?=$web['host_member']?>/bbs/reply.php" title="내 댓글"><?=number($member['bbs_reply_count'])?></a></li>
</ul>
</div>
<? } ?>
<? } else { ?>
<div class="loginblock">
<form method="post" name="formSideLogin" autocomplete="off">
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<p class="autologin"><input type="checkbox" id="autologin" name="autologin" value="1" /><label for="autologin">로그인 상태 유지</label></p>
<div class="idpw">
<div class="uid"><input type="text" id="uid" name="uid" value="" tabindex="1" /><span>아이디</span></div>
<div class="upw"><input type="password" id="upw" name="upw" value="" tabindex="2" /><span>비밀번호</span></div>
</div>
<div class="submit"><span tabindex="3">로그인</span></div>
</form>
<div class="join">
<a href="<?=$web['host_member']?>/join/">회원가입</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/id_hp.php">아이디/비밀번호 찾기</a>
</div>
</div>
<? if ($setup_api['social_login_onoff']) { ?>
<div class="social">
<ul>
<? if ($setup_api['login_naver']) { ?>
<li class="login-naver" title="네이버"></li>
<? } ?>
<? if ($setup_api['login_kakao']) { ?>
<li class="login-kakao" title="카카오톡"></li>
<? } ?>
<? if ($setup_api['login_facebook']) { ?>
<li class="login-facebook" title="페이스북"></li>
<? } ?>
<? if ($setup_api['login_twitter']) { ?>
<li class="login-twitter" title="트위터"></li>
<? } ?>
<? if ($setup_api['login_google']) { ?>
<li class="login-google" title="구글"></li>
<? } ?>
<? if ($setup_api['login_instagram']) { ?>
<li class="login-instagram" title="인스타그램"></li>
<? } ?>
</ul>
</div>
<? } ?>
<? } ?>
</div>
</div>
</div>
<!-- login end //-->
<!-- banner start //-->
<div class="banner">
<div class="box" id="main_side">
<?
$list = array();
$result = sql_query(" select * from $web[banner_table] where onoff = 1 and group_id = 'main_side' and pc_upload_file != '' order by rand() ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    sql_query(" update $web[banner_table] set hit = hit + 1 where id = '".$row['id']."' ");

    $list[$i] = $row;
    $list[$i]['html'] = "";
    $list[$i]['html'] .= "<div class='block' name='".($i+1)."'>";

    if ($row['pc_url']) {

        $list[$i]['html'] .= "<a href='".url_http($row['pc_url'])."'";

        if ($row['pc_blank']) {

            $list[$i]['html'] .= " target='_blank'";

        }

        $list[$i]['html'] .= " onclick=\"bannerClick('".$row['id']."', '".substr(md5($row['datetime']),0,20)."');\">";

    }

    $list[$i]['html'] .= "<img src='".$disk['server_banner']."/".data_path("u", $row['pc_upload_time'])."/".$row['pc_upload_file']."' alt=''>";

    if ($row['pc_url']) {

        $list[$i]['html'] .= "</a>";

    }

    $list[$i]['html'] .= "</div>";

}

for ($i=0; $i<count($list); $i++) {

    echo $list[$i]['html'];

}
?>
<? if (count($list) > 1) { ?>
<div class="num">
<ul>
<?
for ($i=0; $i<count($list); $i++) {

    echo "<li name='".($i+1)."'></li>";

}
?>
</ul>
</div>
<? } ?>
</div>
</div>
<!-- banner end //-->
<!-- articlebest start //-->
<div class="articlebest">
<p class="title">주간 베스트</p>
<div class="tab">
<span name="1" class="hit">조회수</span>
<span name="2" class="reply">댓글수</span>
<span name="3" class="good">추천수</span>
</div>
<div name="1" class="block hit"></div>
<div name="2" class="block reply"></div>
<div name="3" class="block good"></div>
</div>
<!-- articlebest end //-->
<!-- notice start //-->
<?
$bbs_id = "notice"; // 게시판아이디 입력
$bbs = bbs($bbs_id);

$list = array();

if ($bbs['bbs_id']) {

    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} order by id desc limit 0, 4 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $list[$i] = $row;

        if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'])) {

            $list[$i]['date'] = date("H:i", strtotime($row['datetime']));

        } else {

            $list[$i]['date'] = date("m.d", strtotime($row['datetime']));

        }

    }

} else {

    $bbs['bbs_title'] = "게시판 '".text($bbs_id)."' 없음";

}

if (count($list) < 4) {

    for ($i=count($list); $i<4; $i++) {

        $list[$i]['ar_title'] = "등록된 게시물이 없습니다.";

    }

}
?>
<div class="notice">
<div class="box">
<p class="title"><a href="<?=http_bbs($bbs_id, "")?>" title="<?=text($bbs['bbs_title'])?>"><?=text($bbs['bbs_title'])?></a></p>
<div class="list">
<?
for ($i=0; $i<5; $i++) {

    echo "<a href='".http_bbs($bbs_id, $list[$i]['id'])."' title='".text($list[$i]['ar_title'])."'>";
    echo text($list[$i]['ar_title']);
    echo "<span class='date'>".$list[$i]['date']."</span>";
    echo "</a>";

}
?>
</div>
</div>
</div>
<!-- notice end //-->
<!-- connect start //-->
<?
$chk = sql_fetch(" select count(id) as total_count from $web[connect_table] ");

$list = array();
$result = sql_query(" select * from $web[connect_table] order by loadtime desc limit 0, 5 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $mb = member($row['mid']);

    $list[$i] = $row;
    $list[$i]['thumb'] = member_thumb($list[$i]['mid']);
    $list[$i]['ip'] = preg_replace("/^([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$/", "\\1.\\2.\\3.***", $row['ip']);

    if ($mb['mid']) {

        $list[$i]['uid'] = $mb['uid'];
        $list[$i]['nick'] = $mb['nick'];

    }

    else if ($row['robot']) {

        $list[$i]['nick'] = "로봇";

    } else {

        $list[$i]['nick'] = "비회원";

    }

}

if (count($list) < 5) {

    for ($i=count($list); $i<5; $i++) {

        $list[$i]['thumb'] = "";
        $list[$i]['nick'] = "";
        $list[$i]['ip'] = "";

    }

}
?>
<div class="connect">
<div class="box">
<p class="title"><a href="<?=$web['host_member']?>/connect/" title="현재 접속자">현재 접속자 (<?=number($chk['total_count'])?>)</a></p>
<div class="list">
<?
for ($i=0; $i<5; $i++) {

    echo "<p>";

    if ($setup['image_onoff']) {

        echo "<span class='member_thumb'>".$list[$i]['thumb']."</span>";

    }

    echo "<span class='nick";

    if ($setup['image_onoff']) {

        echo " photo";

    }

    echo "'>".text($list[$i]['nick'])."</span>";
    echo "<span class='ip'>".text($list[$i]['ip'])."</span>";
    echo "</p>";

}
?>
</div>
</div>
</div>
<!-- connect end //-->
<!-- visitcount start //-->
<div class="visitcount">
<div class="tab">
<span name="1">방문자</span>
<span name="2">게시물</span>
<span name="3">댓글</span>
</div>
<div class="block">
<ul>
<li><span class="subj">오늘</span><span id="visitcount1" style="color:#ff503f;">0</span></li>
<li><span class="subj">주간</span><span id="visitcount2">0</span></li>
</ul>
<ul>
<li><span class="subj">어제</span><span id="visitcount3">0</span></li>
<li><span class="subj">누적</span><span id="visitcount4">0</span></li>
</ul>
</div>
</div>
<!-- visitcount end //-->
</div>
<!-- side end //-->
</div>
</div>
<!-- layout-home end //-->
<?
include_once("$web[path]/_popup.php");
include_once("$web[path]/_bottom.php");
?>