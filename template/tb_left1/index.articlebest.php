<?php // 주간베스트
include_once("./_tb.php");

$list = array();

// 주간 게시물이 있나? 없다면 최신순으로 뽑는다.
$chk = sql_fetch(" select count(id) as total_count from $web[search_table] where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' ");

// 대글순
if ($m == '2') {

    if ($chk['total_count']) {

        $result = sql_query(" select * from $web[search_table] where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' order by ar_reply desc limit 0, 10 ");

    } else {

        $result = sql_query(" select * from $web[search_table] order by datetime desc limit 0, 10 ");

    }

    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $article = article($row['bbs_id'], $row['article_id']);

        $list[$i] = $row;
        $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);
        $list[$i]['num'] = $i+1;
        $list[$i]['ar_title'] = $article['ar_title'];
        $list[$i]['count'] = number($row['ar_reply']);

    }

}

// 추천순
else if ($m == '3') {

    if ($chk['total_count']) {

        $result = sql_query(" select * from $web[search_table] where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' order by ar_good desc limit 0, 10 ");

    } else {

        $result = sql_query(" select * from $web[search_table] order by datetime desc limit 0, 10 ");

    }

    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $article = article($row['bbs_id'], $row['article_id']);

        $list[$i] = $row;
        $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);
        $list[$i]['num'] = $i+1;
        $list[$i]['ar_title'] = $article['ar_title'];
        $list[$i]['count'] = number($row['ar_good']);

    }

} else {
// 조회순

    if ($chk['total_count']) {

        $result = sql_query(" select * from $web[search_table] where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' order by ar_hit desc limit 0, 10 ");

    } else {

        $result = sql_query(" select * from $web[search_table] order by datetime desc limit 0, 10 ");

    }

    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $article = article($row['bbs_id'], $row['article_id']);

        $list[$i] = $row;
        $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);
        $list[$i]['num'] = $i+1;
        $list[$i]['ar_title'] = $article['ar_title'];
        $list[$i]['count'] = number($row['ar_hit']);

    }

}

if (count($list) < 10) {

    for ($i=count($list); $i<10; $i++) {

        $list[$i]['href'] = http_bbs($list[$i]['bbs_id'], "");
        $list[$i]['num'] = $i+1;
        $list[$i]['ar_title'] = "게시물이 없습니다.";
        $list[$i]['count'] = "";

    }

}

for ($i=0; $i<count($list); $i++) {

    echo "<a href='".$list[$i]['href']."' title='".text($list[$i]['ar_title'])."'>";
    echo "<span class='num'>".$list[$i]['num']."</span>";
    echo "<span class='subj'>".text($list[$i]['ar_title'])."</span>";
    echo "<span class='count'>".$list[$i]['count']."</span>";
    echo "</a>";

}
?>