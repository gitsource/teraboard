<?php // 헤드
if (!defined("_WEB_")) exit;

if ($web['title']) {

    $web['title'] = $web['title'];

} else {

    $web['title'] = $setup['browsertitle'];

}

// 접속위치
$connect_title = $web['title'];
$connect_url = 'http'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 's' : '').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

// 현재접속자
if ($web['menu'] && !$check_admin) {

    $connect_host = $web['menu'];

    $chk = sql_fetch(" select * from $web[connect_table] where user = '".addslashes($member['user'])."' limit 0, 1 ");

    if ($chk['id']) {

        $sql_common = "";
        $sql_common .= " set user = '".addslashes($member['user'])."' ";
        $sql_common .= ", mid = '".$member['mid']."' ";
        $sql_common .= ", title = '".trim(strip_tags(sql_real_escape_string($connect_title)))."' ";
        $sql_common .= ", host = '".trim(strip_tags(sql_real_escape_string($connect_host)))."' ";
        $sql_common .= ", url = '".trim(strip_tags(sql_real_escape_string($connect_url)))."' ";
        $sql_common .= ", updatetime = '".$web['time_ymdhis']."' ";
        $sql_common .= ", loadtime = '".$web['time_ymdhis']."' ";
        $sql_common .= ", robot = '".addslashes($check_robot)."' ";

        sql_query(" update $web[connect_table] $sql_common where id = '".addslashes($chk['id'])."' ");

    } else {

        $sql_common = "";
        $sql_common .= " set user = '".addslashes($member['user'])."' ";
        $sql_common .= ", mid = '".$member['mid']."' ";
        $sql_common .= ", title = '".trim(strip_tags(sql_real_escape_string($connect_title)))."' ";
        $sql_common .= ", host = '".trim(strip_tags(sql_real_escape_string($connect_host)))."' ";
        $sql_common .= ", url = '".trim(strip_tags(sql_real_escape_string($connect_url)))."' ";
        $sql_common .= ", updatetime = '".$web['time_ymdhis']."' ";
        $sql_common .= ", loadtime = '".$web['time_ymdhis']."' ";
        $sql_common .= ", ip = '".trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])))."' ";
        $sql_common .= ", robot = '".addslashes($check_robot)."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[connect_table] $sql_common ");

    }

}

// 방문자
$chk = sql_fetch(" select id from $web[visit_table] where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and substring(datetime,1,10) = '".$web['time_ymd']."' limit 0, 1 ");

if (!$chk['id']) {

    $sql_common = "";
    $sql_common .= " set ip = '".trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])))."' ";
    $sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";

    $preg_match_http_host = $_SERVER['HTTP_HOST'];

    if ($_SERVER['HTTP_REFERER'] && !preg_match("/^(http:\/\/)?(https:\/\/)?+([a-z0-9.]?)+($preg_match_http_host)/i", $_SERVER['HTTP_REFERER'])) {

        $sql_common .= ", referer = '".trim(strip_tags(sql_real_escape_string($_SERVER['HTTP_REFERER'])))."' ";

    }

    $sql_common .= ", keyword = '".trim(strip_tags(sql_real_escape_string(visit_keyword($_SERVER['HTTP_REFERER']))))."' ";
    $sql_common .= ", connect = '".trim(strip_tags(sql_real_escape_string($connect_url)))."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[visit_table] $sql_common ");

}

if (!$check_robot && !preg_match("/\/adm\//i", addslashes($connect_url))) {

    $sql_common = "";
    $sql_common .= " set mid = '".$member['mid']."' ";
    $sql_common .= ", ip = '".trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])))."' ";
    $sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", title = '".trim(strip_tags(sql_real_escape_string($connect_title)))."' ";
    $sql_common .= ", connect = '".trim(strip_tags(sql_real_escape_string($connect_url)))."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[visit_page_table] $sql_common ");

}

sql_query(" delete from $web[connect_table] where updatetime < '".date("Y-m-d H:i:s", $web['server_time'] - 60)."' ");

if (!$web['meta_type']) {
    $web['meta_type'] = "website";
}

$web['meta_url'] = $connect_url;
?>
<meta charset="<?=$web['charset']?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,chrome=1" />
<? if ($web['description']) { ?>
<meta name="description" content="<?=$web['description']?>" />
<? } ?>
<title><?=text($web['title'])?></title>
<meta name="twitter:card" content="summary_large_image" />
<meta property="fb:app_id" content="<?=text($setup_api['login_facebook_id'])?>" />
<meta property="og:url" content="<?=text($web['meta_url'])?>" />
<meta property="og:type" content="<?=text($web['meta_type'])?>" />
<meta property="og:title" content="<?=text($web['title'])?>" />
<meta property="og:subject" content="<?=text($web['meta_subject'])?>" />
<meta property="og:description" content="<?=$web['description']?>" />
<meta property="og:image" content="<?=text($web['meta_image'])?>" />
<meta property="og:image:width" content="<?=text($web['meta_image_width'])?>" />
<meta property="og:image:height" content="<?=text($web['meta_image_height'])?>" />
<meta property="og:locale" content="ko_KR" />
<meta property="og:site_name" content="<?=text($setup['browsertitle'])?>" />
<link rel="stylesheet" href="//fonts.googleapis.com/earlyaccess/nanumgothic.css" type="text/css" />
<link rel="stylesheet" href="<?=$web['host_css']?>/web.css" type="text/css" />
<link rel="stylesheet" href="<?=$web['host_css']?>/blueimp-gallery.min.css">
<link rel="stylesheet" href="<?=$web['host_css']?>/jquery.selectBox.css" type="text/css" />
<?
echo "<script type=\"text/javascript\">";
echo "var web_url = '".text($web['url'])."';";
echo "var web_kakaomsg = '".text($setup['title'])." ".$web['host_default']."';";
echo "var web_tel = '".text($setup['tel'])."';";
echo "var web_sms = '".text($setup['hp'])."';";
echo "var bbs_id = '".text($bbs_id)."';";
echo "var bbs_url = '".text($web['host_bbs'])."';";
echo "var rbbs_url = '".text($web['host_rbbs'])."';";
echo "var article_id = '".text($article_id)."';";
echo "var check_login = '".$check_login."';";
echo "var check_admin = '".$check_admin."';";
echo "var check_bbsadmin = '".$check_bbsadmin."';";
echo "var check_touch= '".$check_touch."';";
echo "var check_browser= '".visit_browser($_SERVER['HTTP_USER_AGENT'])."';";
echo "var form_check = '".text($member['form_check'])."';";
if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
echo "</script>\n";
?>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.cookie.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.selectBox.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/web.js"></script>
<?
echo $web['head'];

if ($setup['analytics_onoff']) {
    echo stripslashes($setup['analytics_code']);
}
?>
<? if ($web['menu']) { ?>
<script type="text/javascript">$(document).ready( function() { function checkLoading() { $.post("/_check.php", {"load" : "check"}, function(data) { $("#update_data").html(data); }); setTimeout(checkLoading, 30000); }; setTimeout( function() { checkLoading();  }, 10000 ); });</script>
<?
}

// kakaotalk
if ($setup_api['login_kakao_key']) {
?>
<script src="https://developers.kakao.com/sdk/js/kakao.min.js"></script>
<script type="text/javascript">
Kakao.init('<?=text($setup_api['login_kakao_key'])?>');
function kakaoLink()
{

    var label = "";
    var label_title = $('meta[property="og:title"]').attr('content');
    var label_subject = $('meta[property="og:subject"]').attr('content');
    var label_url = $('meta[property="og:url"]').attr('content');

    if (label_title) { label += label_title; }
    if (label_subject) { label += "\n"+label_subject; }
    if (label_url) { label += "\n"+label_url; }

    Kakao.Link.sendTalkLink({
    label: label,
<? if ($web['meta_image']) { ?>
    image: {
        src: $('meta[property="og:image"]').attr('content'),
        width: $('meta[property="og:image:width"]').attr('content'),
        height: $('meta[property="og:image:height"]').attr('content')
    }
<? } ?>
    });

}
</script>
<? } ?>