<?php // 목록
if (!defined("_WEB_")) exit;

if ($bbs['bbs_skin_install'] != 'tb_webzine1') {

    sql_query(" ALTER TABLE $web[bbs_table] ADD `bbs_skin_install` varchar( 50 ) NOT NULL COMMENT '스킨인스톨' ", false);
    sql_query(" ALTER TABLE {$web['article_table']}{$bbs_id} ADD `ar_subject` varchar( 255 ) NOT NULL COMMENT '부제목' ", false);

    $sql_common = "";
    $sql_common .= " set bbs_skin_install = 'tb_webzine1' ";
    $sql_common .= ", bbs_extend1_name = '섬네일 생성방식(0/1/2)' ";
    $sql_common .= ", bbs_extend1_data = '2' ";
    $sql_common .= ", bbs_extend2_name = '모바일 섬네일 가로 px' ";
    $sql_common .= ", bbs_extend2_data = '80' ";
    $sql_common .= ", bbs_extend3_name = '모바일 섬네일 세로 px' ";
    $sql_common .= ", bbs_extend3_data = '60' ";
    $sql_common .= ", bbs_extend4_name = '부제 사용(y/n)' ";
    $sql_common .= ", bbs_extend4_data = 'y' ";
    $sql_common .= ", bbs_extend5_name = '' ";
    $sql_common .= ", bbs_extend5_data = '' ";
    $sql_common .= ", bbs_extend6_name = '' ";
    $sql_common .= ", bbs_extend6_data = '' ";
    $sql_common .= ", bbs_extend7_name = '' ";
    $sql_common .= ", bbs_extend7_data = '' ";
    $sql_common .= ", bbs_extend8_name = '' ";
    $sql_common .= ", bbs_extend8_data = '' ";
    $sql_common .= ", bbs_extend9_name = '' ";
    $sql_common .= ", bbs_extend9_data = '' ";
    $sql_common .= ", bbs_extend10_name = '' ";
    $sql_common .= ", bbs_extend10_data = '' ";
    $sql_common .= ", bbs_thumb_width = '100' ";
    $sql_common .= ", bbs_thumb_height = '80' ";

    sql_query(" update $web[bbs_table] $sql_common where bbs_id = '".$bbs_id."' ");

}

$thumb_cut = false;
if ($bbs['bbs_extend1_data'] == 1) {
    $thumb_cut = "oc"; // 원본중앙
}
else if ($bbs['bbs_extend1_data'] == 2) {
    $thumb_cut = "sc"; // 비율중앙
}

$thumb_width = $bbs['bbs_thumb_width'];
$thumb_height = $bbs['bbs_thumb_height'];

$dir = $disk['path']."/thumb/".$bbs_id;

@mkdir($dir, 0707);
@chmod($dir, 0707);

// 새글
$chk = sql_fetch(" select count(id) as cnt from {$web['article_table']}{$bbs_id} where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (3600 * $bbs['bbs_icon_new']))."' ");
$bbs['bbs_write_count_new'] = (int)($chk['cnt']);

$colspan = 5;

if ($bbs['bbs_category_onoff']) {
    $colspan++;
}

if ($bbs['bbs_good']) {
    $colspan++;
}

if ($bbs['bbs_nogood']) {
    $colspan++;
}

$dq = "Search";

if (!$q) {
    $q = $dq;
}

if ($check_login) {

    $replymsg = "회원님의 소중한 의견을 남겨주세요!";

} else {

    $replymsg = "비회원은 가입을 하지않아도 소셜 로그인을 통해 간편하게 의견을 남기실 수 있습니다.";

}
?>
<style type="text/css">
.bbs-array .array .thumb {width:<?=$thumb_width?>px;}
.bbs-array .array .thumb .table div img {max-width:<?=$thumb_width?>px; max-height:<?=$thumb_height?>px;}
.bbs-array .array .list {min-height:<?=$thumb_height?>px;}
.bbs-array .array.thumbblock .list .subj {margin-left:<?=($thumb_width + 115)?>px;}

@media screen and (max-width:700px) {

.bbs-array .array .thumb {width:<?=$bbs['bbs_extend2_data']?>px;}
.bbs-array .array .thumb .table div img {max-width:<?=$bbs['bbs_extend2_data']?>px; max-height:<?=$bbs['bbs_extend3_data']?>px; text-align:center;}
.bbs-array .array .list {min-height:<?=$bbs['bbs_extend3_data']?>px; padding:15px 10px 15px 10px;}
.bbs-array .array.thumbblock .list .subj {margin-left:<?=($bbs['bbs_extend2_data'] + 75)?>px;}

}

@media screen and (max-width:350px) {

.bbs-array .array.thumbblock .list .subj {margin-left:<?=($bbs['bbs_extend2_data'] + 10)?>px;}

}
</style>
<script type="text/javascript">
var dq = "<?=text($dq)?>";
var replymsg = "<?=text($replymsg)?>";
</script>
<script type="text/javascript" src="<?=$bbs_skin_url?>/bbs.js"></script>
<!-- bbs-count start //-->
<form method="get" name="formSearch">
<input type="hidden" name="bbs_id"  value="<?=text($bbs_id)?>" />
<input type="hidden" name="c" value="<?=text($c)?>" />
<input type="hidden" name="q" value="<?=text($q)?>" />
<div class="bbs-count<? if (!$bbs['bbs_category_onoff'] && !$bbs['btn_write']) { echo " display-none"; } ?>">
<? if ($bbs['bbs_category_onoff']) { ?>
<div class="category">
<!-- selectbox //-->
<div class="selectbox">
<div class="select" rel="bbsSearch">
<div class="block">
<span class="text"></span>
<div class="option"><input type="hidden" name="ct" value="<?=text($ct)?>" class="add" />
<ul class="array">
<li name="">전체 카테고리</li>
<?
$row = explode("|", $bbs['bbs_category']);
for ($i=0; $i<count($row); $i++) {

    echo "<li name='".text($row[$i])."'>".text($row[$i])."</li>";

}
?>
</ul>
<ul class="bg"></ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
</div>
<? } ?>
<? if ($bbs['btn_write']) { ?><a href="<?=$web['host_rbbs']?>/write.php?bbs_id=<?=$bbs_id?>" class="write"><span class="icon"></span>글쓰기</a><? } ?>
<div class="blockright">
<div class="count">New <?=number($bbs['bbs_write_count_new']);?> / All <?=number($total_count);?></div>
<div class="notice" title="공지숨기기"></div>
<div class="rows">
<!-- selectbox //-->
<div class="selectbox">
<div class="select" rel="bbsSearch">
<div class="block">
<span class="text"><?=text($bbs['bbs_rows'])?>개씩 보기</span>
<div class="option"><input type="hidden" name="rows" value="<?=text($rows)?>" class="add" />
<ul class="array">
<li name="<?=text($bbs['bbs_rows'])?>"><?=text($bbs['bbs_rows'])?>개씩 보기</li>
<li name="50">50개씩 보기</li>
<li name="100">100개씩 보기</li>
</ul>
<ul class="bg"></ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
</div>
</div>
</div>
</form>
<!-- bbs-count end //-->
<!-- bbs-array start //-->
<div class="bbs-array">
<form method="post" name="formList" class="form_list" autocomplete="off">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<? if (count($notice)) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<? for ($i=0; $i<count($notice); $i++) { ?>
<tr class="notice">
    <td class="num"><span>공지</span></td>
    <td class="subj" colspan="<?=($colspan - 1)?>">
<?
echo "<a href='".$notice[$i]['href']."' title='".$notice[$i]['title']."' class='font-zoom'>";

echo $notice[$i]['ar_title'];

if ($bbs['bbs_reply'] && $notice[$i]['ar_reply']) {
    echo "<span class='reply'>(".number($notice[$i]['ar_reply']).")</span>";
}

echo "</a>";
?>
    </td>
</tr>
<? } ?>
</table>
<? } ?>
<?
for ($i=0; $i<count($list); $i++) {

    $member_thumb = member_thumb($list[$i]['mid']);

    $source = "";
    $thumb_file = "";
    $thumb = "";

    $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($bbs_id)."' and article_id = '".$list[$i]['id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

    if ($file['upload_file']) {

        $thumb_file = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
        $thumb_path = $dir."/".data_path("u", $file['upload_time']);
        $thumb = $thumb_path."/thumb".$thumb_width."x".$thumb_height."_".$file['upload_file'];

        if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

            @mkdir($thumb_path, 0707);
            @chmod($thumb_path, 0707);

            image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $thumb_cut);

        }

        $thumb = str_replace($disk['path'], $disk['server'], $thumb);

    }

    if (!$thumb) {

        $thumb = bbs_thumb_create($bbs_id, $list[$i]['datetime'], $list[$i]['ar_content'], $thumb_width, $thumb_height, $thumb_cut);
        $thumb_file = image_editor($list[$i]['ar_content']);

    }

    $source = $thumb_file;
    $source = str_replace($disk['path'], $disk['server'], $source);

    if ($thumb) { $thumb = "<img src='".$thumb."' border='0'>"; }
?>
<div class="array<? if ($list[$i]['icon_answer']) { echo " answer"; } if ($thumb) { echo " thumbblock"; } ?>">
<? if ($check_bbsadmin || $check_admin) { ?><div class="chkbox chk_id"><input type="hidden" name="list_id[<?=$i?>]" value="<?=text($list[$i]['id'])?>" /><input type="checkbox" name="chk_id[]" value="<?=$i?>" class="checkbox" /></div><? } ?>
<? if ($thumb) { ?>
<div class="thumb"><div class="table"><div>
<?
if ($thumb_file) {

    echo "<a href='".$source."' data-gallery='' title='".$list[$i]['title']."'>".$thumb."</a>";

} else {

    echo $thumb;

}
?>
</div></div></div>
<? } ?>
<a href="<?=$list[$i]['href']?>" title="<?=$list[$i]['title']?>" name="list-array<?=$list[$i]['id']?>" class="list">
<div class="date">
<div class="table">
<div>
<span class="date1"><?=text_date("Y.m", $list[$i]['datetime'],"")?></span>
<span class="date2"><?=text_date("d", $list[$i]['datetime'],"")?></span>
</div>
</div>
</div>
<div class="font-zoom subj">
<?
if ($bbs['bbs_category_onoff']) {
    echo "<span class='category'>[".text($list[$i]['ar_category'])."]</span>";
}

if ($list[$i]['icon_answer']) { echo "<span class='icon_answer'></span>"; }

echo "<span class='title'>".$list[$i]['ar_title']."</span>";

if ($bbs['bbs_reply'] && $list[$i]['ar_reply']) {
    echo "<span class='reply'>(".number($list[$i]['ar_reply']).")</span>";
}

echo "<span class='icon'>";
if ($list[$i]['icon_new']) { echo "<span class='icon_new'></span>"; }
if ($list[$i]['icon_hot']) { echo "<span class='icon_hot'></span>"; }
if ($list[$i]['icon_file']) { echo "<span class='icon_file'></span>"; }
if ($list[$i]['icon_img']) { echo "<span class='icon_img'></span>"; }
if ($list[$i]['icon_source']) { echo "<span class='icon_source'></span>"; }
if ($list[$i]['icon_secret']) { echo "<span class='icon_secret'></span>"; }
echo "</span>";

echo "<br /><span class='content'>";

if ($list[$i]['ar_secret']) {

    echo "비밀 게시물입니다.";

} else {

    if (strtolower($bbs['bbs_extend4_data']) == 'y') {

        if ($list[$i]['ar_subject']) {

            echo text($list[$i]['ar_subject']);

        } else {

            echo text3($list[$i]['ar_content']);

        }

    } else {

        echo text3($list[$i]['ar_content']);

    }

}

echo "</span>";
?>
</div>
<div class="block">
<span class="nick"><nobr><? if ($setup['image_onoff']) { echo $member_thumb; } ?><span class="sideview" onclick="sideviewLoad(this, '<?=text($list[$i]['mid'])?>'); return false;" title="<?=text($list[$i]['nick'])?>"><?=text($list[$i]['nick'])?></span></nobr></span>
<span class="line"></span>
<span class="hit">조회 : <?=text($list[$i]['ar_hit'])?></span>
<? if ($bbs['bbs_good']) { ?>
<span class="line"></span>
<span class="hit">추천 : <?=text($list[$i]['ar_good'])?></span>
<? } ?>
<? if ($bbs['bbs_nogood']) { ?>
<span class="line"></span>
<span class="hit">반대 : <?=text($list[$i]['ar_nogood'])?></span>
<? } ?>
</div>
</a>
</div>
<? } ?>
<? if (count($list) == 0) { ?>
<p class="not">검색된 게시물이 없습니다.</p>
<? } ?>
</form>
</div>
<!-- bbs-array end //-->
<!-- bbs-search start //-->
<div class="bbs-search">
<!-- selectbox //-->
<div class="selectbox">
<div class="select">
<div class="block">
<span class="text"></span>
<div class="option"><input type="hidden" id="bbs_c" value="<?=text($c)?>" class="add" />
<ul class="array">
<li name="">전체</li>
<li name="1">제목</li>
<li name="2">내용</li>
<li name="3">작성자</li>
<li name="4">키워드</li>
</ul>
<ul class="bg"></ul>
</div>
</div>
</div>
<div class="input"><input type="text" id="bbs_q" value="<?=text($q)?>" onfocus="searchFocus(this, 'in', '<?=text($dq)?>');" onblur="searchFocus(this, 'out', '<?=text($dq)?>');" /></div>
<div class="submit"></div>
</div>
<!-- selectbox //-->
<? if ($bbs['btn_write']) { ?><div class="btn"><a href="<?=$web['host_rbbs']?>/write.php?bbs_id=<?=$bbs_id?>" class="write"><span class="icon"></span>글쓰기</a></div><? } ?>
</div>
<!-- bbs-search end //-->
<? if ($total_count && $total_count > $rows) { ?>
<div class="web-page"><?=$paging?></div>
<? } ?>