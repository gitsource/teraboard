<?php // 댓글
if (!defined("_WEB_")) exit;
?>
<div class="reply-line"><div></div></div>
<!-- reply-write start //-->
<form method="post" name="formReplyW" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<input type="hidden" name="article_id" value="<?=$article_id?>" />
<input type="hidden" name="reply_id" value="" />
<?
if (!$check_login) {

    echo "<div class='reply-login'>";

    if ($bbs['btn_reply']) {

        echo "<span class='guest' title='비회원'>비회원 작성</span>";

    }

    if ($setup_api['social_login_onoff']) {

        if ($setup_api['login_naver']) {

            echo "<span class='naver' title='네이버'></span>";

        }

        if ($setup_api['login_kakao']) {

            echo "<span class='kakao' title='카카오'></span>";

        }

        if ($setup_api['login_facebook']) {

            echo "<span class='facebook' title='페이스북'></span>";

        }

        if ($setup_api['login_twitter']) {

            echo "<span class='twitter' title='트위터'></span>";

        }

        if ($setup_api['login_google']) {

            echo "<span class='google' title='구글'></span>";

        }

        if ($setup_api['login_instagram']) {

            echo "<span class='instagram' title='인스타그램'></span>";

        }

    }

    echo "</div>";

    echo "<div class='reply-guest'>";
    echo "<ul>";
    echo "<li class='title'>비회원</li>";
    echo "<li class='input'><div class='block'><div class='input_focus'><input type='text' name='nick' value='' maxlength='20' /><span class='name'>작성자</span></div></div></li>";
    echo "<li class='input'><div class='block'><div class='input_focus'><input type='password' name='upw' value='' maxlength='20' /><span class='name'>비밀번호</span></div></div></li>";
    echo "<li class='cancel'>취소</li>";
    echo "</ul>";
    echo "</div>";

}
?>
<div class="reply-write<? if (!$check_login) { echo " guest"; } ?>">
<div class="block">
<? if ($check_login) { ?>
<div class="member">
<? if ($setup['image_onoff']) { ?>
<div class="member_thumb"><?=member_thumb($member['mid'])?></div>
<? } ?>
<p class="nick"><nobr><span class="sideview" onclick="sideviewLoad(this, '<?=text($member['mid'])?>');" title="<?=text($member['nick'])?>"><?=text($member['nick'])?></span></nobr></p>
</div>
<? } ?>
</div>
<div class="textarea"><textarea id="content" name="content"></textarea></div>
<a href="#" class="submit" onclick="submitReply(''); return false;"></a>
</div>
</form>
<!-- reply-write end //-->
<!-- reply-reply start //-->
<div id="reply-reply">
<form method="post" name="formReplyR" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<input type="hidden" name="article_id" value="<?=$article_id?>" />
<input type="hidden" name="reply_id" value="" />
<div class="reply-reply">
<p class="title">대댓글 작성</p>
<div class="textarea"><textarea id="reply_content" name="content"></textarea></div>
<div class="submit">
<a href="#" onclick="submitReply('r'); return false;">확인</a>
<a href="#" onclick="replyReplyCancel(); return false;">취소</a>
</div>
</div>
</form>
</div>
<!-- reply-reply end //-->
<!-- reply-edit start //-->
<div id="reply-edit">
<form method="post" name="formReplyU" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<input type="hidden" name="article_id" value="<?=$article_id?>" />
<input type="hidden" name="reply_id" value="" />
<div class="reply-edit">
<div class="textarea"><textarea id="edit_content" name="content"></textarea></div>
<div class="submit">
<a href="#" onclick="submitReply('u'); return false;">확인</a>
<a href="#" onclick="replyEditCancel(); return false;">취소</a>
</div>
</div>
</form>
</div>
<!-- reply-edit end //-->
<!-- reply-upw start //-->
<div id="reply-upw">
<form method="post" name="formReplyP" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<input type="hidden" name="article_id" value="<?=$article_id?>" />
<input type="hidden" name="reply_id" value="" />
<div class="reply-upw">
<div class="box">
<p class="msg">비밀번호를 입력하세요.</p>
<div class="upw">
<span>비밀번호</span>
<span class="input_focus"><input type="password" name="upw" value="" /></span>
</div>
<div class="submit">
<a href="#" onclick="submitReply('p'); return false;">확인</a>
<a href="#" onclick="replyUpwCancel(); return false;">취소</a>
</div>
</div>
</div>
</form>
</div>
<!-- reply-upw end //-->
<!-- reply-best start //-->
<? if (count($best)) { ?>
<div class="reply-best">
<? for ($i=0; $i<count($best); $i++) { ?>
<div class="array">
<div class="list">
<div class="best"></div>
<? if ($setup['image_onoff']) { ?>
<div class="photo"><div class="member_thumb"><?=member_thumb($best[$i]['mid'])?></div></div>
<? } ?>
<div class="block">
<div class="nick">
<span class="sideview" onclick="sideviewLoad(this, '<?=text($best[$i]['mid'])?>');" title="<?=text($best[$i]['nick'])?>"><nobr><?=text($best[$i]['nick'])?></nobr></span>
<span class="icon lv<?=$best[$i]['level']?>"></span>
<span class="date"><span><?=text_date("Y. m. d (H:i)", $best[$i]['datetime'], "")?></span><span class="mobile"><?=text_date("m.d (H:i)", $best[$i]['datetime'], "")?></span></span>
</div>
<?
if ($bbs['bbs_good'] || $bbs['bbs_nogood']) {

    echo "<div class='good'>";

    if ($bbs['bbs_good']) {

        echo "<a href='#' onclick=\"replyGood('".$bbs_id."', '".$article_id."', '".$best[$i]['id']."', '0', 'best'); return false;\" class='btn_good' title='추천'><span class='icon'></span><span class='count' id='best_good_".$best[$i]['id']."'>".text($best[$i]['good'])."</span></a>";

    }

    if ($bbs['bbs_nogood']) {

        echo "<a href='#' onclick=\"replyGood('".$bbs_id."', '".$article_id."', '".$best[$i]['id']."', '1', 'best'); return false;\" class='btn_nogood' title='반대'><span class='icon'></span><span class='count' id='best_nogood_".$best[$i]['id']."'>".text($best[$i]['nogood'])."</span></a>";

    }

    echo "</div>";

}
?>
<div class="font-zoom content"><?=$best[$i]['content']?></div>
</div>
</div>
</div>
<? } ?>
</div>
<? } ?>
<!-- reply-best end //-->
<!-- reply-list start //-->
<? if (count($list)) { ?>
<div class="reply-list">
<? for ($i=0; $i<count($list); $i++) { ?>
<? if ($list[$i]['delmsg']) { ?>
<div class="list del"><div class="block"><div class="content">작성자에 의해 원글이 삭제되었습니다.</div></div></div>
<? } ?>
<div id="current_reply<?=$list[$i]['id']?>" class="list <? if ($list[$i]['reply']) { echo "reply"; } ?>">
<? if ($setup['image_onoff']) { ?>
<div class="photo"><div class="member_thumb"><?=member_thumb($list[$i]['mid'])?></div></div>
<div class="arrow"></div>
<? } ?>
<div class="block">
<div class="nick">
<span class="sideview" onclick="sideviewLoad(this, '<?=text($list[$i]['mid'])?>');" title="<?=text($list[$i]['nick'])?>"><nobr><?=text($list[$i]['nick'])?></nobr></span>
<span class="icon lv<?=$list[$i]['level']?>"></span>
<span class="date"><span><?=text_date("Y. m. d (H:i)", $list[$i]['datetime'], "")?></span><span class="mobile"><?=text_date("m.d (H:i)", $list[$i]['datetime'], "")?></span></span>
</div>
<div class="btn">
<? if ($list[$i]['btn_edit']) { ?><a href="#" onclick="replyEdit('<?=$list[$i]['id']?>','<?=$list[$i]['mid']?>', ''); return false;">수정</a><? } ?>
<? if ($list[$i]['btn_delete']) { ?><a href="#" onclick="replyDelete('<?=$list[$i]['id']?>','<?=$list[$i]['mid']?>', ''); return false;">삭제</a><? } ?>
<? if ($list[$i]['btn_reply']) { ?><a href="#" onclick="replyReply('<?=$list[$i]['id']?>'); return false;">대댓글</a><? } ?>
<a href="#" onclick="replyPolice('<?=$bbs_id?>', '<?=$article_id?>', '<?=$list[$i]['id']?>'); return false;">신고</a>
</div>
<?
if ($bbs['bbs_good'] || $bbs['bbs_nogood']) {

    echo "<div class='good'>";

    if ($bbs['bbs_good']) {

        echo "<a href='#' onclick=\"replyGood('".$bbs_id."', '".$article_id."', '".$list[$i]['id']."', '0', ''); return false;\" class='btn_good' title='추천'><span class='icon'></span><span class='count' id='reply_good_".$list[$i]['id']."'>".text($list[$i]['good'])."</span></a>";

    }

    if ($bbs['bbs_nogood']) {

        echo "<a href='#' onclick=\"replyGood('".$bbs_id."', '".$article_id."', '".$list[$i]['id']."', '1', ''); return false;\" class='btn_nogood' title='반대'><span class='icon'></span><span class='count' id='reply_nogood_".$list[$i]['id']."'>".text($list[$i]['nogood'])."</span></a>";

    }

    echo "</div>";

}
?>
<div id="content_<?=$list[$i]['id']?>" class="font-zoom content"><div class="icon"><?=$list[$i]['content']?></div></div>
<?
echo "<span id='edit_".$list[$i]['id']."' style='display:none;'></span>";
echo "<span id='upw_".$list[$i]['id']."' style='display:none;'></span>";
echo "<textarea id='save_".$list[$i]['id']."' style='display:none;'>".$list[$i]['ori_content']."</textarea>";
?>
</div>
<span id="reply_<?=$list[$i]['id']?>" style="display:none;"></span>
</div>
<? } ?>
</div>
<? } ?>
<!-- reply-list end //-->