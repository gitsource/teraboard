<?php // 내용
if (!defined("_WEB_")) exit;

if ($check_login) {

    $replymsg = "회원님의 소중한 의견을 남겨주세요!";

} else {

    $replymsg = "비회원은 가입을 하지않아도 소셜 로그인을 통해 간편하게 의견을 남기실 수 있습니다.";

}
?>
<? if (!$bbs['bbs_view_list']) { ?>
<script type="text/javascript">
var replymsg = "<?=text($replymsg)?>";
</script>
<script type="text/javascript" src="<?=$bbs_skin_url?>/bbs.js<?=$web['version']?>"></script>
<? } ?>
<style type="text/css">
.bbs-count {margin-top:20px;}
</style>
<div class="bbs-view">
<!-- bbs-viewtop start //-->
<div class="bbs-viewtop">
<div class="btn">
<? if ($article['href_prev']) { ?>
<a href="<?=$article['href_prev']?>">이전글</a>
<? } ?>
<a href="<?=$article['href_list']?>" class="icon"><span class="list"></span>목록</a>
<? if ($article['href_next']) { ?>
<a href="<?=$article['href_next']?>">다음글</a>
<? } ?>
<a href="#" onclick="bbsScrap('<?=$bbs_id?>', '<?=$article_id?>'); return false;">스크랩</a>
<a href="#" onclick="bbsPolice('<?=$bbs_id?>', '<?=$article_id?>'); return false;">신고</a>
</div>
</div>
<!-- bbs-viewtop end //-->
<section itemscope itemtype="http://schema.org/NewsArticle">
<article itemprop="articleBody">
<!-- bbs-subj start //-->
<div class="bbs-subj">
<div class="block">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="member">
<? if ($setup['image_onoff']) { ?>
<div class="member_thumb"><?=member_thumb($article['mid'])?></div>
<? } ?>
<p class="nick"><nobr><span class="sideview" onclick="sideviewLoad(this, '<?=text($article['mid'])?>');" title="<?=text($article['nick'])?>" itemprop="publisher"><?=text($article['nick'])?></span></nobr></p>
    </td>
    <td class="subj">
<div class="line">
<h1 itemprop="headline" class="font-zoom"><?=$article['ar_title']?></h1>
<div class="block2">
<div class="date">
<? if ($bbs['bbs_category_onoff']) { ?>
<span class="font-zoom category">[<?=text($article['ar_category'])?>]</span>
<? } ?>
<span class="font-zoom datetime" itemprop="datePublished" content="<?=date("Y-m-d\\TH:i:s", strtotime($article['datetime']))?>"><span>작성일시 : </span><?=text_date("Y. m. d (H:i)", $article['datetime'], "")?></span>
</div>
<div class="hit">
<ul>
<li class="font-zoom">조회 : <?=number($article['ar_hit'])?><? if ($bbs['bbs_reply']) { ?> / 댓글 : <?=number($article['ar_reply'])?><? } ?></li>
</ul>
</div>
</div>
</div>
    </td>
</tr>
</table>
</div>
</div>
<!-- bbs-subj end //-->
<!-- bbs-file start //-->
<? if (count($articlefile)) { ?>
<div class="bbs-file">
<div class="title">
<div class="text">첨부파일 (<span class="count"><?=count($articlefile)?></span>)</div>
<div class="btn"><span class="icon"></span>DOWNLOAD</div>
</div>
<div class="list">
<div class="block">
<p class="help">
<?
$check_download = false;
if ($setup['point_onoff'] && $bbs['bbs_point_download']) {

    if (!$check_login) {

        echo "<font color='#9b280d'>본 파일은 회원만 다운로드 하실 수 있습니다.<br />가입 후 이용하시기 바랍니다.</font>";

    }

    else if ($member['point'] >= $bbs['bbs_point_download']) {

        $check_download = true;

        echo "다운로드 시, 포인트 ".$bbs['bbs_point_download']."점이 차감됩니다.<br />첨부 파일을 클릭하세요.";

    } else {

        echo "<font color='#9b280d'>다운로드 포인트 ".$bbs['bbs_point_download']."점이 부족하여<br />첨부파일을 내려 받으실 수 없습니다.</font>";

    }

} else {

    $check_download = true;

    echo "첨부 파일을 클릭하세요.";

}
?>
</p>
<?
for ($i=0; $i<count($articlefile); $i++) {

    echo "<p class='array";

    if (!$check_download) {

        echo " no";

    }

    echo "'><a href='".$articlefile[$i]['href']."' class='".bbs_file_icon($articlefile[$i]['upload_file'])."'";

    if (!$check_download) {
        echo " onclick='return false;'";
    }

    echo "><span class='source'>".text($articlefile[$i]['upload_source'])."</span><br /><span class='filesize'>(".file_size($articlefile[$i]['upload_filesize']).")</span><span class='filecount'>DOWN : ".number($articlefile[$i]['upload_count'])."</span></a></p>";

}
?>
</div>
</div>
</div>
<? } ?>
<!-- bbs-file end //-->
<!-- bbs-content start //-->
<div class="bbs-content">
<div id="description" itemprop="description" class="font-zoom">
<?
// 이미지 파일첨부시 내용에 추가
if (count($articleimage)) {

    $ar_content = "";
    for ($i=0; $i<count($articleimage); $i++) {

        $ar_content .= "<img src='".$disk['server_bbs']."/bbs/".$bbs_id."/".data_path("u", $articleimage[$i]['upload_time'])."/".text($articleimage[$i]['upload_file'])."' border='0'><br />";

    }

    $article['ar_content'] = $ar_content.$article['ar_content'];

}

$article['ar_content'] = text2($article['ar_content'], 1);
$article['ar_content'] = preg_replace("/(\<img[[:space:]])(.*?)?(src=\"?'?)([^\"?'?]*)([\"?'?]*)([^\>]*)(\>)/i", "<a name='image' href='\\4' itemprop='image' data-gallery=\"#view\" title='".text($article['ar_title'])."'>\\1\\2\\3\\4\\5\\6\\7</a>", $article['ar_content']);

echo $article['ar_content'];
?>
</div>
</div>
<!-- bbs-content end //-->
</article>
</section>
<!-- bbs-source start //-->
<? if ($bbs['bbs_source'] && $article['ar_source']) { ?>
<div class="bbs-source">
<a href="<?=url_http($article['ar_source'])?>" target="_blank" title="<?=url_http($article['ar_source'])?>"><span class="icon"></span><nobr><b>출처(링크) :</b> <?=url_http($article['ar_source'])?></nobr></a>
</div>
<? } ?>
<!-- bbs-source end //-->
<!-- bbs-good start //-->
<? if ($bbs['bbs_good'] || $bbs['bbs_nogood']) { ?>
<div class="bbs-good">
<? if ($bbs['bbs_good']) { ?><a href="#" class="btn_good" onclick="articleGood('<?=$bbs_id?>', '<?=$article_id?>', 0); return false;" title="추천"><span class="icon"></span><span class="count" id="ar_good"><?=number($article['ar_good'])?></span><span class="text">LIKE</span></a><? } ?>
<? if ($bbs['bbs_nogood']) { ?><a href="#" class="btn_nogood" onclick="articleGood('<?=$bbs_id?>', '<?=$article_id?>', 1); return false;" title="반대"><span class="icon"></span><span class="count" id="ar_nogood"><?=number($article['ar_nogood'])?></span><span class="text">BAD</span></a><? } ?>
</div>
<? } ?>
<!-- bbs-good end //-->

<?
if ($bbs['bbs_reply']) {

    include_once("./reply.php");

}
?>

<!-- bbs-viewbottom start //-->
<? if ($bbs['btn_write'] || $bbs['btn_edit'] || $bbs['btn_delete'] || $bbs['btn_answer']) { ?>
<div class="bbs-viewbottom">
<? if ($bbs['btn_write']) { ?><div class="write"><a href="<?=$web['host_rbbs']?>/write.php?bbs_id=<?=$bbs_id?>" class="btn_write"><span class="icon"></span>글쓰기</a></div><? } ?>
<? if ($bbs['btn_edit']) { ?><a href="<?=$web['host_rbbs']?>/write.php?m=u&amp;bbs_id=<?=$bbs_id?>&amp;article_id=<?=$article_id?>" class="btn_edit"><span class="icon"></span>수정</a><? } ?>
<? if ($bbs['btn_delete']) { ?><a href="#" onclick="articleDel('<?=$bbs_id?>','<?=$article_id?>'); return false;" class="btn_delete"><span class="icon"></span>삭제</a><? } ?>
<? if ($bbs['btn_answer']) { ?><a href="<?=$web['host_rbbs']?>/write.php?m=a&amp;bbs_id=<?=$bbs_id?>&amp;article_id=<?=$article_id?>" class="btn_answer"><span class="icon"></span>답변</a><? } ?>
<a href="<?=$article['href_list']?>" class="btn_list"><span class="icon"></span>목록</a>
</div>
<? } ?>
<!-- bbs-viewbottom end //-->
</div>

<? if ($bbs['bbs_view_list']) { ?><div class="bbs-viewline"></div><? } ?>