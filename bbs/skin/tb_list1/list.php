<?php // 목록
if (!defined("_WEB_")) exit;

// 새글
$chk = sql_fetch(" select count(id) as cnt from {$web['article_table']}{$bbs_id} where datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (3600 * $bbs['bbs_icon_new']))."' ");
$bbs['bbs_write_count_new'] = (int)($chk['cnt']);

$colspan = 5;

if ($bbs['bbs_category_onoff']) {
    $colspan++;
}

if ($bbs['bbs_good']) {
    $colspan++;
}

if ($bbs['bbs_nogood']) {
    $colspan++;
}

$dq = "Search";

if (!$q) {
    $q = $dq;
}

if ($check_login) {

    $replymsg = "회원님의 소중한 의견을 남겨주세요!";

} else {

    $replymsg = "비회원은 가입을 하지않아도 소셜 로그인을 통해 간편하게 의견을 남기실 수 있습니다.";

}
?>
<script type="text/javascript">
var dq = "<?=text($dq)?>";
var replymsg = "<?=text($replymsg)?>";
</script>
<script type="text/javascript" src="<?=$bbs_skin_url?>/bbs.js"></script>
<!-- bbs-count start //-->
<form method="get" name="formSearch">
<input type="hidden" name="bbs_id"  value="<?=text($bbs_id)?>" />
<input type="hidden" name="c" value="<?=text($c)?>" />
<input type="hidden" name="q" value="<?=text($q)?>" />
<div class="bbs-count<? if (!$bbs['bbs_category_onoff'] && !$bbs['btn_write']) { echo " display-none"; } ?>">
<? if ($bbs['bbs_category_onoff']) { ?>
<div class="category">
<!-- selectbox //-->
<div class="selectbox">
<div class="select" rel="bbsSearch">
<div class="block">
<span class="text"></span>
<div class="option"><input type="hidden" name="ct" value="<?=text($ct)?>" class="add" />
<ul class="array">
<li name="">전체 카테고리</li>
<?
$row = explode("|", $bbs['bbs_category']);
for ($i=0; $i<count($row); $i++) {

    echo "<li name='".text($row[$i])."'>".text($row[$i])."</li>";

}
?>
</ul>
<ul class="bg"></ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
</div>
<? } ?>
<? if ($bbs['btn_write']) { ?><a href="<?=$web['host_rbbs']?>/write.php?bbs_id=<?=$bbs_id?>" class="write"><span class="icon"></span>글쓰기</a><? } ?>
<div class="blockright">
<div class="count">New <?=number($bbs['bbs_write_count_new']);?> / All <?=number($total_count);?></div>
<div class="notice" title="공지숨기기"></div>
<div class="rows">
<!-- selectbox //-->
<div class="selectbox">
<div class="select" rel="bbsSearch">
<div class="block">
<span class="text"><?=text($bbs['bbs_rows'])?>개씩 보기</span>
<div class="option"><input type="hidden" name="rows" value="<?=text($rows)?>" class="add" />
<ul class="array">
<li name="<?=text($bbs['bbs_rows'])?>"><?=text($bbs['bbs_rows'])?>개씩 보기</li>
<li name="50">50개씩 보기</li>
<li name="100">100개씩 보기</li>
</ul>
<ul class="bg"></ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
</div>
</div>
</div>
</form>
<!-- bbs-count end //-->
<!-- bbs-array start //-->
<div class="bbs-array">
<form method="post" name="formList" class="form_list" autocomplete="off">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<!-- bbs-pc start //-->
<div class="bbs-pc">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr class="title">
    <td class="num">
<? if ($check_bbsadmin || $check_admin) { ?>
<input type="checkbox" onclick="if (this.checked) checkAll(true); else checkAll(false);" class="checkbox" />
<? } else { ?>
번호
<? } ?>
    </td>
<? if ($bbs['bbs_category_onoff']) { ?>
    <td class="category">분류</td>
<? } ?>
    <td class="subj">제목</td>
    <td class="nick">작성자</td>
    <td class="date">작성일</td>
    <td class="hit">조회</td>
<? if ($bbs['bbs_good']) { ?>
    <td class="good">추천</td>
<? } ?>
<? if ($bbs['bbs_nogood']) { ?>
    <td class="nogood">반대</td>
<? } ?>
</tr>
<? for ($i=0; $i<count($notice); $i++) { ?>
<tr class="notice">
    <td class="num"><span>공지</span></td>
    <td class="subj" colspan="<?=($colspan - 1)?>">
<?
echo "<a href='".$notice[$i]['href']."' title='".$notice[$i]['title']."' class='font-zoom'>";

echo $notice[$i]['ar_title'];

if ($bbs['bbs_reply'] && $notice[$i]['ar_reply']) {
    echo "<span class='reply'>(".number($notice[$i]['ar_reply']).")</span>";
}

echo "</a>";
?>
    </td>
</tr>
<?
}

for ($i=0; $i<count($list); $i++) {

$member_thumb = member_thumb($list[$i]['mid']);
?>
<tr class="array" name="list-array<?=$list[$i]['id']?>">
    <td class="num chk_id">
<? if ($check_bbsadmin || $check_admin) { ?>
<input type="hidden" name="list_id[<?=$i?>]" value="<?=text($list[$i]['id'])?>" /><input type="checkbox" name="chk_id[]" value="<?=$i?>" class="checkbox" />
<? } else { ?>
<?=text($list[$i]['id'])?>
<? } ?>
    </td>
<? if ($bbs['bbs_category_onoff']) { ?>
    <td class="category"><a href="<?=http_bbs($bbs_id, "")?>&amp;ct=<?=urlencode(text($list[$i]['ar_category']))?>" class="font-zoom"><?=text($list[$i]['ar_category'])?></a></td>
<? } ?>
    <td class="subj"><?
echo "<a href='".$list[$i]['href']."' title='".$list[$i]['title']."' class='font-zoom'>";

if ($list[$i]['icon_answer']) { echo "<span class='icon_answer'></span>"; }

echo $list[$i]['ar_title'];

if ($bbs['bbs_reply'] && $list[$i]['ar_reply']) {
    echo "<span class='reply'>(".number($list[$i]['ar_reply']).")</span>";
}

echo "<span class='icon'>";
if ($list[$i]['icon_new']) { echo "<span class='icon_new'></span>"; }
if ($list[$i]['icon_hot']) { echo "<span class='icon_hot'></span>"; }
if ($list[$i]['icon_file']) { echo "<span class='icon_file'></span>"; }
if ($list[$i]['icon_img']) { echo "<span class='icon_img'></span>"; }
if ($list[$i]['icon_source']) { echo "<span class='icon_source'></span>"; }
if ($list[$i]['icon_secret']) { echo "<span class='icon_secret'></span>"; }
echo "</span>";

echo "</a>";
?></td>
    <td class="nick"><nobr><? if ($setup['image_onoff']) { echo $member_thumb; } ?><span class="sideview" onclick="sideviewLoad(this, '<?=text($list[$i]['mid'])?>');" title="<?=text($list[$i]['nick'])?>"><?=text($list[$i]['nick'])?></span></nobr></td>
    <td class="date"><?=text($list[$i]['date'])?></td>
    <td class="hit"><?=number($list[$i]['ar_hit'])?></td>
<? if ($bbs['bbs_good']) { ?>
    <td class="good"><?=number($list[$i]['ar_good'])?></td>
<? } ?>
<? if ($bbs['bbs_nogood']) { ?>
    <td class="nogood"><?=number($list[$i]['ar_nogood'])?></td>
<? } ?>
</tr>
<? } ?>
<? if (count($list) == 0) { ?>
<tr><td colspan="<?=$colspan?>" class="not">검색된 게시물이 없습니다.</td></tr>
<? } ?>
<tr><td colspan="<?=$colspan?>" class="line"></td></tr>
</tbody>
</table>
</div>
<!-- bbs-pc end //-->
<!-- bbs-mobile start //-->
<div class="bbs-mobile">
<div class="notice">
<? for ($i=0; $i<count($notice); $i++) { ?>
<a href="<?=$notice[$i]['href']?>" title="<?=$notice[$i]['title']?>">
<p class="num"><span>공지</span></p>
<p class="font-zoom subj"><?
echo $notice[$i]['ar_title'];

if ($bbs['bbs_reply'] && $notice[$i]['ar_reply']) {
    echo "<span class='reply'>(".number($notice[$i]['ar_reply']).")</span>";
}
?></p>
</a>
<? } ?>
</div>
<div class="array">
<? for ($i=0; $i<count($list); $i++) { ?>
<a href="<?=$list[$i]['href']?>" title="<?=$list[$i]['title']?>" name="list-array<?=$list[$i]['id']?>">
<p class="font-zoom subj"><?
if ($list[$i]['icon_answer']) { echo "<span class='icon_answer'></span>"; }

echo $list[$i]['ar_title'];

if ($bbs['bbs_reply'] && $list[$i]['ar_reply']) {
    echo "<span class='reply'>(".number($list[$i]['ar_reply']).")</span>";
}
?></p>
<p class="font-zoom nick">
<? if ($bbs['bbs_category_onoff']) { ?><span class="category">[<?=text($list[$i]['ar_category'])?>]</span><? } ?>
<span class="nick sideview"><?=text($list[$i]['nick'])?></span>
<span class="date"><?=text($list[$i]['date'])?></span>
</p>
</a>
<? } ?>
<? if (count($list) == 0) { ?>
<p class="not">검색된 게시물이 없습니다.</p>
<? } ?>
</div>
</div>
<!-- bbs-mobile end //-->
</form>
</div>
<!-- bbs-array end //-->
<!-- bbs-search start //-->
<div class="bbs-search">
<!-- selectbox //-->
<div class="selectbox">
<div class="select">
<div class="block">
<span class="text"></span>
<div class="option"><input type="hidden" id="bbs_c" value="<?=text($c)?>" class="add" />
<ul class="array">
<li name="">전체</li>
<li name="1">제목</li>
<li name="2">내용</li>
<li name="3">작성자</li>
<li name="4">키워드</li>
</ul>
<ul class="bg"></ul>
</div>
</div>
</div>
<div class="input"><input type="text" id="bbs_q" value="<?=text($q)?>" onfocus="searchFocus(this, 'in', '<?=text($dq)?>');" onblur="searchFocus(this, 'out', '<?=text($dq)?>');" /></div>
<div class="submit"></div>
</div>
<!-- selectbox //-->
<? if ($bbs['btn_write']) { ?><div class="btn"><a href="<?=$web['host_rbbs']?>/write.php?bbs_id=<?=$bbs_id?>" class="write"><span class="icon"></span>글쓰기</a></div><? } ?>
</div>
<!-- bbs-search end //-->
<? if ($total_count && $total_count > $rows) { ?>
<div class="web-page"><?=$paging?></div>
<? } ?>