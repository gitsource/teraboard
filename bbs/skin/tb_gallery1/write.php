<?php // 글쓰기
if (!defined("_WEB_")) exit;

$article['d_ar_keyword'] = "키워드 복수 입력시, 콤마(,) 사용";

if (!$article['ar_keyword']) {

    $article['ar_keyword'] = $article['d_ar_keyword'];

}

if ($setup['editor'] == 1) {

    echo "<script type=\"text/javascript\" src=\"".$web['host_cheditor']."/cheditor.js\"></script>";

}

else if ($setup['editor'] == 2) {

    echo "<script type=\"text/javascript\" src=\"".$web['host_smarteditor']."/js/HuskyEZCreator.js\" charset=\"utf-8\"></script>";

}
?>
<script type="text/javascript" src="<?=$bbs_skin_url?>/bbs.js"></script>
<script type="text/javascript">
var oEditors = [];
$(document).ready( function() {

    $(".selectb select").selectBox();

    $("#ar_title").focus().parent('.input_focus').addClass('focus');

<?
if ($setup['editor'] == 1) {

    echo "setTimeout( function() { cheditorIcon();  }, 500 );";
    echo "$(window).resize(function() { cheditorIcon(); });";

}

else if ($setup['editor'] == 2) {

    echo "nhn.husky.EZCreator.createInIFrame({";
    echo "oAppRef: oEditors,";
    echo "elPlaceHolder: 'ar_content',";
    echo "sSkinURI: '".$web['host_smarteditor']."/SmartEditor2Skin.html',";
    echo "fCreator: 'createSEditor2'";
    echo "});";

}
?>

});
</script>
<form method="post" name="formWrite" enctype="multipart/form-data" onsubmit="return false;" autocomplete="off">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="m" value="<?=$m?>" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<input type="hidden" name="article_id" value="<?=$article_id?>" />
<input type="hidden" name="editor" value="<?=$setup['editor']?>" />
<div class="bbs-write">
<div class="bbs-title">
<div class="lineout">
<div class="linein">
<?
echo "게시물 ";

if ($m == '') {

    echo "작성";

}

else if ($m == 'u') {

    echo "수정";

}

else if ($m == 'a') {

    echo "답변";

}
?>
</div>
</div>
</div>
<? if ($check_bbsadmin || $check_admin || $bbs['bbs_secret']) { ?>
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">옵 션</td>
    <td class="ess"></td>
    <td class="line"></td>
    <td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<? if ($check_bbsadmin || $check_admin) { ?>
    <td><input type="checkbox" id="ar_notice" name="ar_notice" value="1" class="input_checkbox" <? if ($check_notice) { echo "checked"; } ?> /></td>
    <td width="6"></td>
    <td><label for="ar_notice">공지</label></td>
    <td width="30"></td>
<? } ?>
<? if ($bbs['bbs_secret']) { ?>
    <td><input type="checkbox" id="ar_secret" name="ar_secret" value="1" class="input_checkbox" <? if ($article['ar_secret']) { echo "checked"; } ?> /></td>
    <td width="6"></td>
    <td><label for="ar_secret">비밀글</label></td>
<? } else { ?>
    <td><input type="hidden" name="ar_secret" value="<?=text($article['ar_secret'])?>" /></td>
<? } ?>
</tr>
</table>
    </td>
</tr>
</table>
</div>
<? } ?>
<? if (!$check_login) { ?>
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">작성자</td>
    <td class="ess on"></td>
    <td class="line"></td>
    <td><div class="block"><div class="input_focus"><input type="text" name="nick" value="<?=text($article['nick'])?>" maxlength="20" /><div class="mobile"><span class="name">작성자</span></div></div></div></td>
</tr>
</table>
</div>
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">비밀번호</td>
    <td class="ess on"></td>
    <td class="line"></td>
    <td><div class="block"><div class="input_focus"><input type="password" name="upw" value="" maxlength="20" /><div class="mobile"><span class="name">비밀번호</span></div></div></div></td>
</tr>
</table>
</div>
<? } ?>
<? if ($bbs['bbs_category_onoff']) { ?>
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">분 류</td>
    <td class="ess on"></td>
    <td class="line"></td>
    <td class="selectb selectb200">
<select id="ar_category" name="ar_category" class="select">
    <option value="">분류를 선택하세요.</option>
<?
$row = explode("|", $bbs['bbs_category']);
for ($i=0; $i<count($row); $i++) {

    echo "<option value='".text($row[$i])."'>".text($row[$i])."</option>";

}
?>
</select>
<script type="text/javascript">
<? if ($article['ar_category']) { ?>
document.getElementById("ar_category").value = "<?=text($article['ar_category'])?>";
<? } ?>
</script>
    </td>
</tr>
</table>
</div>
<? } ?>
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">제 목</td>
    <td class="ess on"></td>
    <td class="line"></td>
    <td><div class="block w100"><div class="input_focus"><input type="text" id="ar_title" name="ar_title" value="<?=text($article['ar_title'])?>" maxlength="100" /><div class="mobile"><span class="name">제목</span></div></div></div></td>
</tr>
</table>
</div>
<div class="list">
<div class="ar_keyword"><p>검색 키워드</p><div class="input_keyword"><input type="text" id="ar_keyword" name="ar_keyword" value="<?=text($article['ar_keyword'])?>" data-value="<?=text($article['d_ar_keyword'])?>" maxlength="100" /></div></div>
<div class="ar_content">
<textarea id="ar_content" name="ar_content"><?=text($article['ar_content'])?></textarea>
<? if ($setup['editor'] == 1) { ?>
<script type="text/javascript">
var ar_content = new cheditor();
ar_content.config.editorWidth = '100%';
ar_content.config.editorHeight = '500px';
ar_content.inputForm = 'ar_content';
ar_content.run();
</script>
<? } ?>
</div>
</div>
<? if ($bbs['bbs_source']) { ?>
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">출처 (링크)</td>
    <td class="ess"></td>
    <td class="line"></td>
    <td><div class="block w100"><div class="input_focus"><input type="text" name="ar_source" value="<?=text($article['ar_source'])?>" maxlength="255" /><div class="mobile"><span class="name">출처 (링크)</span></div></div></div></td>
</tr>
</table>
</div>
<? } ?>
<?
if ($bbs['bbs_file_count']) {
for ($i=1; $i<=$bbs['bbs_file_count']; $i++) {

if ($m == '' || $m == 'u') {

    $file = bbs_file($bbs_id, $article_id, $i);

} else {

    $file = array();

}
?>
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">첨부파일 #<?=$i?></td>
    <td class="ess"></td>
    <td class="line"></td>
    <td>
<div class="block w100 input_file"><div class="input_focus"><input type="file" name="file<?=$i?>" value="" /></div></div>

<p class="help">업로드 파일 선택 (제한 크기 : <?=file_size($bbs['bbs_file_size'])?>)</p>

<? if ($file['upload_source']) { ?>
<div style="margin-top:6px; border:1px solid #dbe1e8; background-color:#f8f8f8; padding:9px;">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<? if (preg_match("/\.(jp[e]?g|gif|png)$/i", $file['upload_file'])) { ?>
    <td><div style="background-color:#dbe1e8; padding:2px; width:48px; height:48px;"><a href="<?=$disk['server_bbs']?>/bbs/<?=$bbs_id?>/<?=data_path("u", $file['upload_time'])?>/<?=$file['upload_file']?>" target="_blank"><img src="<?=$disk['server_bbs']?>/bbs/<?=$bbs_id?>/<?=data_path("u", $file['upload_time'])?>/<?=$file['upload_file']?>" width="48" height="48" border="0"></a></div></td>
    <td width="20"></td>
<? } ?>
    <td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><a href="<?=$web['host_rbbs']?>/download.php?bbs_id=<?=text($bbs_id)?>&amp;article_id=<?=text($article_id)?>&amp;number=<?=text($file['number'])?>" class="filesource"><?=text($file['upload_source'])?></a></td>
    <td width="5"></td>
    <td class="filesize">(<?=file_size($file['upload_filesize'])?>)</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
<tr>
    <td><input type="checkbox" name="filedel_<?=$i?>" value="1" class="checkbox" /></td>
    <td width="5"></td>
    <td class="filedel" onclick="elementCheck('formBBSWrite', 'filedel_<?=$i?>');">삭제</td>
    <td width="5"></td>
    <td class="help2" style="color:#dbe1e8;">|</td>
    <td width="5"></td>
    <td class="help2">첨부된 파일을 제거 합니다.</td>
</tr>
</table>
    </td>
</tr>
</table>
</div>
<? } ?>
    </td>
</tr>
</table>
</div>
<?
}
}
?>
<div class="submit"><a href="#" onclick="submitWrite(); return false;">등 록</a><a href="<?=http_bbs($bbs_id, "");?>">취 소</a></div>
</div>
</form>