/* 게시판 */
var zindex = 100;
var edit_before = "#reply-edit";
var edit_html = "";
var reply_before = "#reply-reply";
var reply_html = "";
var upw_before = "#reply-upw";
var upw_html = "";
$(document).ready( function() {

    edit_html = $('#reply-edit').html();
    reply_html = $('#reply-reply').html();
    upw_html = $('#reply-upw').html();

    var currentId = document.location.hash.split('#')[1];

    if (currentId) {

        var win = $(window);
        var box = $("#current_"+currentId);

        if (box != '') {

            box.addClass('on');

            var boxTop = box.offset().top - ((win.height() / 2) - (box.height() / 2));

            $('html,body').animate({scrollTop: boxTop}, 700);

        }

    }

    $('.select .block span').click(function() {

        if ($(this).parent().children('div.option').is(':hidden') == true) {

            zindex++;

            $(this).parent().parent().parent('div.selectbox').addClass('opt').zIndex(zindex);

        } else {

            $(this).parent().parent().parent('div.selectbox').removeClass('opt');

        }

    });

    $('.select .block .option ul.array li').click(function() {

        $(this).parent().parent().children('input.add').val($(this).attr('name'));
        $(this).parent().parent().parent().children('span.text').text($(this).text());
        $(this).parent().parent().parent().parent().parent('div.selectbox').removeClass('opt');

        var act = $(this).parent().parent().parent().parent().attr('rel');

        if (act == 'bbsSearch') {

            bbsSearch();

        }

    });

    $('.selectbox .add').each(function() {

        var val = $(this).val();

        var addtext = $(this).parent().children('ul.array').children('li[name="'+val+'"]').text();

        if (addtext) {

            $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').addClass('on');
            $(this).parent().parent().children('span.text').text(addtext);

        } else {

            $(this).parent().parent().children('span.text').html('&nbsp;');

        }

    });

    $('#bbs_q').keyup(function(e) {

        if (e.keyCode == 13) {

            bbsSearch();

        }

    });

    $('.bbs-search .selectbox .submit').click(function() {

        bbsSearch();

    });

    $('.bbs-count .blockright .notice').click(function() {

        var obj = $('.bbs-array tr.notice');

        if (obj.is(':hidden') == true) {

            obj.show();
            $(this).attr('title', '공지숨기기').removeClass('off');
            $.cookie('bbsnotice', 'hide', { expires: 0, path: '/' });

        } else {

            obj.hide();
            $(this).attr('title', '공지열기').addClass('off');
            $.cookie('bbsnotice', 'hide', { expires: 365, path: '/' });

        }

    });

    if ($.cookie('bbsnotice') == 'hide') {

        $('.bbs-pc tr.notice').hide();
        $('.bbs-count .blockright .notice').attr('title', '공지열기').addClass('off');

    }

    $('.bbs-file .title').click(function() {

        var obj = $('.bbs-file .list');

        if (obj.is(':hidden') == true) {

            obj.show();

        } else {

            obj.hide();

        }

    });

    if (bbs_id && article_id) {

        $(".container .item[name='list-array"+article_id+"'] .block .thumb .table div a").addClass('view');

    }

    $('.reply-login span.guest').click(function() {

        $('.reply-login').hide();
        $('.reply-guest').show();

    });

    $('.reply-guest ul li.cancel').click(function() {

        $('.reply-login').show();
        $('.reply-guest').hide();

    });

    $('.reply-guest .input_focus input').focus(function() {

        $(this).parent('.input_focus').addClass('focus');

    }).blur(function() {

        $(this).parent('.input_focus').removeClass('focus');

        if ($(this).val() == '') {

            $(this).parent().find('.name').show();

        }

    }).keydown(function() {

        $(this).parent().find('.name').hide();

    });

    $('.reply-guest .name').click(function() {

        $(this).parent().parent().find('input').focus();

    });

    $('.reply-login span.naver').click(function() {

        naverLogin();

    });

    $('.reply-login span.kakao').click(function() {

        kakaoLogin();

    });

    $('.reply-login span.facebook').click(function() {

        facebookLogin();

    });

    $('.reply-login span.twitter').click(function() {

        twitterLogin();

    });

    $('.reply-login span.google').click(function() {

        googleLogin();

    });

    $('.reply-login span.instagram').click(function() {

        instagramLogin();

    });

    $('.reply-write .textarea textarea').focus(function() {

        $(this).parent('.textarea').addClass('focus');

        if ($(this).val() == replymsg) {

            $(this).val('');

        }

    }).blur(function() {

        $(this).parent('.textarea').removeClass('focus');

        if ($(this).val() == '') {

            $(this).val(replymsg);

        }

    });

    if (document.getElementById("content")) {

        $('#content').val(replymsg);

    }

    $('.bbs-write .input_focus input').focus(function() {

        $(this).parent('.input_focus').addClass('focus');

    }).blur(function() {

        $(this).parent('.input_focus').removeClass('focus');

        if ($(this).val() == '') {

            $(this).parent().find('.name').show();

        }

    }).keydown(function() {

        $(this).parent().find('.name').hide();

    });

    $('.bbs-write .input_keyword input').focus(function() {

        $(this).parent('.input_keyword').addClass('focus');

        if ($(this).val() == '키워드 복수 입력시, 콤마(,) 사용') {

            $(this).val('');

        }

    }).blur(function() {

        if ($(this).val() == '') {

            $(this).parent('.input_keyword').removeClass('focus');

            $(this).val('키워드 복수 입력시, 콤마(,) 사용');

        }

    });

    $('.bbs-write .input_keyword input').each(function() {

        if ($(this).val() != '' && $(this).val() != '키워드 복수 입력시, 콤마(,) 사용') {

            $(this).parent('.input_keyword').addClass('focus');

        }

    });

    $('.bbs-write .name').click(function() {

        $(this).parent().parent().find('input').focus();

    });

    $('.bbs-write .input_focus input').each(function() {

        if ($(this).val() != '') {

            $(this).parent().find('.name').hide();

        }

    });

    $(document).click(function(e) {

        if (!$(e.target).is('.selectbox *')) {

            $('.selectbox').removeClass('opt');

        }

        if (!$(e.target).is('.bbs-file *')) {

            $('.bbs-file .list').hide();

        }

    });

    bbsResize();
    $(window).resize(function() { bbsResize(); });

});

function bbsResize()
{

    $('#description').css({ 'overflow' : 'visible' });
    $('#description img').css({ 'max-width' : $('#description').width()+'px', 'height' : 'auto' });

}

function checkAll(mode)
{

    $('.form_list .chk_id input.checkbox').prop('checked', mode);

}

function checkConfirm(msg)
{

    var n = $('.form_list .chk_id input.checkbox:checked').length;

    if (n <= 0) {

        alert(msg + "할 게시물을 선택하세요.");
        return false;

    }

    return true;

}

function checkMove()
{

    if (!checkConfirm("이동")) { return false; }

    window.open("","checkMovePopup","width=600, height=600, scrollbars=no");

    var f = document.formList;

    f.action = rbbs_url+"/move.php";
    f.target = "checkMovePopup";
    f.submit();

}

function checkCopy()
{

    if (!checkConfirm("복제")) { return false; }

    window.open("","checkCopyPopup","width=600, height=600, scrollbars=no");

    var f = document.formList;

    f.action = rbbs_url+"/copy.php";
    f.target = "checkCopyPopup";
    f.submit();

}

function checkDel()
{

    if (!checkConfirm("삭제")) { return false; }

    var f = document.formList;

    f.m.value = "check_del";

    var n = $('.form_list .chk_id input.checkbox:checked').length;

    if (confirm("선택하신 게시물 "+n+"건을 삭제 하시겠습니까?\n삭제된 게시물은 복원하실 수 없습니다.")) {

        f.action = rbbs_url+"/write_update.php";
        f.submit();

    } else {

        return false;

    }

}

function articleDel(bbs_id, article_id)
{

    var f = document.formUpdate;

    f.m.value = "d";
    f.bbs_id.value = bbs_id;
    f.article_id.value = article_id;

    if (confirm("게시물을 삭제하시겠습니까?")) {

        f.action = rbbs_url+"/write_update.php";
        f.submit();

    }

}

function articleGood(bbs_id, article_id, good)
{

    var f = document.formUpdate;

    $.post(bbs_url+"/good.php", {"form_check" : f.form_check.value, "bbs_id" : bbs_id, "article_id" : article_id, "good" : good}, function(data) {

        $("#update_data").html(data);

    });

}

function bbsSearch()
{

    var f = document.formSearch;
    var bbs_q = $('#bbs_q').val();

    f.c.value = $('#bbs_c').val();

    if (bbs_q == dq) {

        f.q.value = "";

    } else {

        f.q.value = bbs_q;

    }

    f.action = bbs_url+"/list.php";
    f.submit();

}

function searchFocus(obj, m, text)
{

    if (m == 'in') {

        if (obj.value == text) {
            obj.value = "";
        }

        $(obj).parent().parent('div.selectbox').addClass('on');

    } else {

        if (obj.value == '') {
            obj.value = text;
        }

        $(obj).parent().parent('div.selectbox').removeClass('on');

    }

}

function cheditorIcon()
{

    var w = $('.bbs-write').width();

    if (w > 640) {

        $(".cheditor-tb-wrapper div[name='LineHeight']").show();
        $(".cheditor-tb-wrapper div[name='BackColor']").show();
        $(".cheditor-tb-wrapper div[name='ForeColor']").show();
        $(".cheditor-tb-wrapper div[name='Bold']").show();
        $(".cheditor-tb-wrapper div[name='Italic']").show();
        $(".cheditor-tb-wrapper div[name='Underline']").show();
        $(".cheditor-tb-wrapper div[name='Strikethrough']").show();
        $(".cheditor-tb-wrapper div[name='JustifyLeft']").show();
        $(".cheditor-tb-wrapper div[name='JustifyCenter']").show();
        $(".cheditor-tb-wrapper div[name='JustifyRight']").show();
        $(".cheditor-tb-wrapper div[name='JustifyFull']").show();
        $(".cheditor-tb-wrapper div[name='HR']").show();
        $(".cheditor-tb-wrapper div[name='SmileyIcon']").show();
        $(".cheditor-tb-wrapper div[name='Table']").show();
        $(".cheditor-tb-wrapper div[name='ModifyTable']").show();
        $(".cheditor-tb-wrapper div[name='Superscript']").show();
        $(".cheditor-tb-wrapper div[name='Subscript']").show();
        $(".cheditor-tb-wrapper div[name='Link']").show();
        $(".cheditor-tb-wrapper div[name='UnLink']").show();
        $(".cheditor-tb-wrapper div[name='Image']").show();

    } else {

        $(".cheditor-tb-wrapper div[name='LineHeight']").hide();
        $(".cheditor-tb-wrapper div[name='BackColor']").hide();
        $(".cheditor-tb-wrapper div[name='ForeColor']").hide();
        $(".cheditor-tb-wrapper div[name='Bold']").hide();
        $(".cheditor-tb-wrapper div[name='Italic']").hide();
        $(".cheditor-tb-wrapper div[name='Underline']").hide();
        $(".cheditor-tb-wrapper div[name='Strikethrough']").hide();
        $(".cheditor-tb-wrapper div[name='JustifyLeft']").hide();
        $(".cheditor-tb-wrapper div[name='JustifyCenter']").hide();
        $(".cheditor-tb-wrapper div[name='JustifyRight']").hide();
        $(".cheditor-tb-wrapper div[name='JustifyFull']").hide();
        $(".cheditor-tb-wrapper div[name='HR']").hide();
        $(".cheditor-tb-wrapper div[name='SmileyIcon']").hide();
        $(".cheditor-tb-wrapper div[name='Table']").hide();
        $(".cheditor-tb-wrapper div[name='ModifyTable']").hide();
        $(".cheditor-tb-wrapper div[name='Superscript']").hide();
        $(".cheditor-tb-wrapper div[name='Subscript']").hide();
        $(".cheditor-tb-wrapper div[name='Link']").hide();
        $(".cheditor-tb-wrapper div[name='UnLink']").hide();
        $(".cheditor-tb-wrapper div[name='Image']").hide();

    }

}

function submitWrite()
{

    var f = document.formWrite;

    if (f.editor.value == '1') {

        f.ar_content = ar_content.outputBodyHTML();

    }

    else if (f.editor.value == '2') {

        oEditors.getById["ar_content"].exec("UPDATE_CONTENTS_FIELD", []);

    }

    if (f.nick && f.nick.value == '') {

        alert('작성자를 입력하세요.');
        f.nick.focus();
        return false;

    }

    if (f.upw && f.upw.value == '') {

        alert('비밀번호를 입력하세요.');
        f.upw.focus();
        return false;

    }

    if (f.ar_category && f.ar_category.value == '') {

        alert('분류를 선택하세요.');
        f.ar_category.focus();
        return false;

    }

    if (f.ar_title.value == '') {

        alert('제목을 입력하세요.');
        f.ar_title.focus();
        return false;

    }

    if (f.ar_keyword.value == '키워드 복수 입력시, 콤마(,) 사용') {

        f.ar_keyword.value = "";

    }

    if (f.ar_content.value == '' || f.ar_content.value == '<br>' || f.ar_content.value == '<br />' || f.ar_content.value == '<BR>' || f.ar_content.value == '<BR />') {

        alert('내용을 입력하세요.');
        f.ar_content.focus();
        return false;

    }

    var content = f.ar_content.value.replace(/\n/g, '');

    $.post(rbbs_url+"/write_update.php", {"m" : "blockkeyword", "title" : f.ar_title.value, "content" : content}, function(data) {

        $("#update_data").html(data);

    });

}

function submitWriteUpdate()
{

    var f = document.formWrite;

    var keyword = $('#ar_keyword');

    if (keyword.val() == keyword.attr('data-value')) {

        keyword.val('');

    }

    f.action = rbbs_url+"/write_update.php";
    f.submit();

}

function submitReply(mode)
{

    if (mode == '') {

        var f = document.formReplyW;

        f.m.value = "";
        f.reply_id.value = "";

        if (f.nick && f.nick.value == '') {

            alert('로그인하지 않았거나,\n비회원 작성버튼이 있는경우, 비회원 작성을 클릭하여 작성자를 입력하세요.');
            f.nick.focus();
            return false;

        }

        if (f.upw && f.upw.value == '') {

            alert('비밀번호를 입력하세요.');
            f.upw.focus();
            return false;

        }

        if (f.content.value == '' || f.content.value == replymsg) {

            alert('내용을 입력하세요.');
            f.content.focus();
            return false;

        }

        var content = f.content.value.replace(/\n/g, '');

        $.post(bbs_url+"/reply_update.php", {"m" : "blockkeyword", "mode" : "", "content" : content}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (mode == 'r') {

        var f = document.formReplyR;

        f.m.value = "";

        if (f.content.value == '') {

            alert('내용을 입력하세요.');
            f.content.focus();
            return false;

        }

        var content = f.content.value.replace(/\n/g, '');

        $.post(bbs_url+"/reply_update.php", {"m" : "blockkeyword", "mode" : "r", "content" : content}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (mode == 'u') {

        var f = document.formReplyU;

        f.m.value = "u";

        if (f.content.value == '') {

            alert('내용을 입력하세요.');
            f.content.focus();
            return false;

        }

        var content = f.content.value.replace(/\n/g, '');

        $.post(bbs_url+"/reply_update.php", {"m" : "blockkeyword", "mode" : "u", "content" : content}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (mode == 'p') {

        var f = document.formReplyP;

        if (f.upw.value == '') {

            alert('비밀번호를 입력하세요.');
            f.upw.focus();
            return false;

        }

        $.post(bbs_url+"/reply_update.php", {"m" : "p", "callback" : f.m.value, "form_check" : f.form_check.value, "bbs_id" : f.bbs_id.value, "article_id" : f.article_id.value, "reply_id" : f.reply_id.value, "upw" : f.upw.value}, function(data) {

            $(".reply-upw p.msg").html(data);

        });

    }

}

function submitReplyUpdate(mode)
{

    if (mode == '') {

        var f = document.formReplyW;

    }

    else if (mode == 'r') {

        var f = document.formReplyR;

    }

    else if (mode == 'u') {

        var f = document.formReplyU;

    }

    f.action = bbs_url+"/reply_update.php";
    f.submit();

}

function replyReply(reply_id)
{

    $(reply_before).hide();
    $(reply_before).html("");

    reply_before = '#reply_'+reply_id;

    $('#reply_'+reply_id).show();
    $('#reply_'+reply_id).html(reply_html);

    var f = document.formReplyR;

    f.reply_id.value = reply_id;

    $('#reply_content').focus(function() {

        $(this).parent('.textarea').addClass('focus');

    }).blur(function() {

        $(this).parent('.textarea').removeClass('focus');

    });

    $('#reply_content').focus();

}

function replyReplyCancel()
{

    $(reply_before).hide();

    $('#reply_content').val('');

}

function replyEdit(reply_id, mid, callback)
{

    if (mid && mid != 0 || check_admin || check_bbsadmin || callback == 1) {

        $(edit_before).hide();
        $(edit_before).html("");

        $(upw_before).hide();
        $(upw_before).html("");

        edit_before = '#edit_'+reply_id;

        $('.reply-list .block .content').show();
        $('#content_'+reply_id).hide();

        $('#edit_'+reply_id).show();
        $('#edit_'+reply_id).html(edit_html);

        var f = document.formReplyU;

        f.reply_id.value = reply_id;

        $('#edit_content').focus(function() {

            $(this).parent('.textarea').addClass('focus');

        }).blur(function() {

            $(this).parent('.textarea').removeClass('focus');

        });

        $('#edit_content').val($("#save_"+reply_id).val()).focus();

    } else {

        $(edit_before).hide();
        $(edit_before).html("");

        $(upw_before).hide();
        $(upw_before).html("");

        upw_before = '#upw_'+reply_id;

        $('.reply-list .block .content').show();
        $('#content_'+reply_id).hide();

        $('#upw_'+reply_id).show();
        $('#upw_'+reply_id).html(upw_html);

        var f = document.formReplyP;

        f.m.value = "u";
        f.reply_id.value = reply_id;

        $('.reply-upw .input_focus input').focus(function() {

            $(this).parent('.input_focus').addClass('focus');

        }).blur(function() {

            $(this).parent('.input_focus').removeClass('focus');

        });

        $('.reply-upw .input_focus input').focus();

    }

}

function replyEditCancel()
{

    var f = document.formReplyU;

    $(edit_before).hide();

    $('.reply-list .block .content').show();

    $('#edit_content').val('');

}

function replyDelete(reply_id, mid, callback)
{

    var f = document.formReplyW;

    f.m.value = "d";
    f.reply_id.value = reply_id;

    if (mid && mid != 0 || check_admin || check_bbsadmin || callback == 1) {

        if (callback) {

            f.action = bbs_url+"/reply_update.php";
            f.submit();

        } else {

            if (confirm("댓글을 삭제하시겠습니까?")) {

                f.action = bbs_url+"/reply_update.php";
                f.submit();

            } else {

                return false;

            }

        }

    } else {

        $(upw_before).hide();
        $(upw_before).html("");

        upw_before = '#upw_'+reply_id;

        $('.reply-list .block .content').show();
        $('#content_'+reply_id).hide();

        $('#upw_'+reply_id).show();
        $('#upw_'+reply_id).html(upw_html);

        var f = document.formReplyP;

        f.m.value = "d";
        f.reply_id.value = reply_id;

        $('.reply-upw .input_focus input').focus(function() {

            $(this).parent('.input_focus').addClass('focus');

        }).blur(function() {

            $(this).parent('.input_focus').removeClass('focus');

        });

        $('.reply-upw .input_focus input').focus();

    }

}

function replyUpwCancel()
{

    var f = document.formReplyP;

    $(upw_before).hide();

    $('.reply-list .block .content').show();

    $('#edit_content').val('');

}

function replyGood(bbs_id, article_id, reply_id, good, mode)
{

    var f = document.formUpdate;

    $.post(bbs_url+"/reply_good.php", {"form_check" : f.form_check.value, "bbs_id" : bbs_id, "article_id" : article_id, "reply_id" : reply_id, "good" : good, "mode" : mode}, function(data) {

        $("#update_data").html(data);

    });

}