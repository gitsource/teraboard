<?php
include_once("./_tb.php");
if ($good) { $good = preg_match("/^[0-1]+$/", $good) ? $good : ""; }
if ($mode) { $mode = preg_match("/^[a-zA-Z]+$/", $mode) ? $mode : ""; }

if ($member['level'] < $bbs['bbs_level_good']) {

    if ($check_login) {

        if ($member['level'] < $bbs['bbs_level_good']) {

            message("<p class='title'>알림</p><p class='text'>권한이 없습니다.</p>", "", "", false, true);

        }

    } else {

        message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "", "", false, true);

    }

}

if (!$bbs['bbs_id']) {

    message("<p class='title'>알림</p><p class='text'>존재하지 않는 게시판입니다.</p>", "", "", false, true);

}

if ($good == 0) {

    $msg = "추천";

    if (!$bbs['bbs_good']) {

        message("<p class='title'>알림</p><p class='text'>추천을 사용하지 않는 게시판입니다.</p>", "", "", false, true);

    }

}

else if ($good == '1') {

    $msg = "반대";

    if (!$bbs['bbs_nogood']) {

        message("<p class='title'>알림</p><p class='text'>반대를 사용하지 않는 게시판입니다.</p>", "", "", false, true);

    }

} else {

    message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", "", false, true);

}

if (!$article['id']) {

    message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", "", false, true);

}

$reply = reply($bbs_id, $reply_id);

if (!$reply['id']) {

    message("<p class='title'>알림</p><p class='text'>댓글이 삭제되었거나 존재하지 않습니다.</p>", "", "", false, true);

}

if ($check_login) {

    if ($member['mid'] == $reply['mid']) {

        message("<p class='title'>알림</p><p class='text'>자신의 댓글은 {$msg}할 수 없습니다.</p>", "", "", false, true);

    }

    $chk = sql_fetch(" select count(*) as cnt from $web[bbs_good_table] where mid = '".$member['mid']."' and substring(datetime,1,10) = '".$web['time_ymd']."' ");

    if ($chk['cnt'] && $chk['cnt'] >= 100) {

        message("<p class='title'>알림</p><p class='text'>하루 100번까지만 할 수 있습니다.</p>", "", "", false, true);

    }

    $chk = sql_fetch(" select * from $web[bbs_good_table] where mid = '".$member['mid']."' and bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply_id = '".$reply_id."' ");

} else {

    if ($_SERVER['REMOTE_ADDR'] == $reply['ip']) {

        message("<p class='title'>알림</p><p class='text'>자신의 댓글은 {$msg}할 수 없습니다.</p>", "", "", false, true);

    }

    $chk = sql_fetch(" select count(*) as cnt from $web[bbs_good_table] where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and substring(datetime,1,10) = '".$web['time_ymd']."' ");

    if ($chk['cnt'] && $chk['cnt'] >= 100) {

        message("<p class='title'>알림</p><p class='text'>하루 100번까지만 할 수 있습니다.</p>", "", "", false, true);

    }

    $chk = sql_fetch(" select * from $web[bbs_good_table] where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply_id = '".$reply_id."' ");

}

if ($chk['id']) {

    message("<p class='title'>알림</p><p class='text'>이미 추천 또는 반대하였습니다.</p>", "", "", false, true);

} else {

    if ($good == 0) {

        sql_query(" update {$web['reply_table']}{$bbs_id} set good = good + 1 where id = '".$reply_id."' ");

        if ($reply['mid']) {

            sql_query(" update $web[member_table] set good = good + 1 where mid = '".$reply['mid']."' ");

        }

        echo "<script type='text/javascript'>";
        if ($mode == 'best') {
            echo "document.getElementById('best_good_".$reply_id."').innerHTML = \"".number_format($reply['good'] + 1)."\";";
        } else {
            echo "document.getElementById('reply_good_".$reply_id."').innerHTML = \"".number_format($reply['good'] + 1)."\";";
        }
        echo "</script>";

    } else {

        sql_query(" update {$web['reply_table']}{$bbs_id} set nogood = nogood + 1 where id = '".$reply_id."' ");

        if ($reply['mid']) {

            sql_query(" update $web[member_table] set nogood = nogood + 1 where mid = '".$reply['mid']."' ");

        }

        echo "<script type='text/javascript'>";
        if ($mode == 'best') {
            echo "document.getElementById('best_nogood_".$reply_id."').innerHTML = \"".number_format($reply['nogood'] + 1)."\";";
        } else {
            echo "document.getElementById('reply_nogood_".$reply_id."').innerHTML = \"".number_format($reply['nogood'] + 1)."\";";
        }
        echo "</script>";

    }

    $sql_common = "";
    $sql_common .= " set mid = '".$member['mid']."' ";
    $sql_common .= ", ip = '".trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])))."' ";
    $sql_common .= ", good = '".$good."' ";
    $sql_common .= ", bbs_id = '".$bbs_id."' ";
    $sql_common .= ", article_id = '".$article_id."' ";
    $sql_common .= ", reply_id = '".$reply_id."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[bbs_good_table] $sql_common ");

}
?>