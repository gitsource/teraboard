<?php // 댓글
if (!defined("_WEB_")) exit;

$bbs['btn_reply'] = false;
if ($member['level'] >= $bbs['bbs_level_reply']) {

    $bbs['btn_reply'] = true;

}

$best = array();
$result = sql_query(" select * from {$web['reply_table']}{$bbs_id} where article_id = '".$article_id."' and good >= 5 order by good desc limit 0, 3 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $best[$i] = $row;

    $best[$i]['nick'] = text($row['nick']);
    $best[$i]['content'] = text2($row['content'], 0);
    $best[$i]['content'] = preg_replace("/\[\<a\s*href\=\"(http|https|ftp)\:\/\/([^[:space:]]+)\.(gif|png|jpg|jpeg|bmp)\"\s*[^\>]*\>[^\s]*\<\/a\>\]/i", "<img src='$1://$2.$3'>", $best[$i]['content']);
    $best[$i]['ori_content'] = text($row['content']);

    $best[$i]['level'] = 1;

    if ($row['mid']) {

        $mb = member($best[$i]['mid']);

        $best[$i]['level'] = $mb['level'];

    }

}

$list = array();
$reply_id = 0;
$result = sql_query(" select * from {$web['reply_table']}{$bbs_id} where article_id = '".$article_id."' order by reply_id asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;

    $list[$i]['btn_edit'] = false;
    $list[$i]['btn_delete'] = false;
    $list[$i]['btn_reply'] = false;

    if (!$check_login && !$row['mid'] || $row['mid'] && $member['mid'] == $row['mid'] || $check_admin) {

        $list[$i]['btn_edit'] = true;
        $list[$i]['btn_delete'] = true;

    }

    if ($check_login && $bbs['btn_reply'] && $row['id'] == $row['reply_id']) {

        $list[$i]['btn_reply'] = true;

    }

    $list[$i]['nick'] = text($row['nick']);
    $list[$i]['content'] = text2($row['content'], 0);
    $list[$i]['content'] = preg_replace("/\[\<a\s*href\=\"(http|https|ftp)\:\/\/([^[:space:]]+)\.(gif|png|jpg|jpeg|bmp)\"\s*[^\>]*\>[^\s]*\<\/a\>\]/i", "<img src='$1://$2.$3'>", $list[$i]['content']);
    $list[$i]['ori_content'] = text($row['content']);

    $list[$i]['level'] = 1;

    if ($row['mid']) {

        $mb = member($list[$i]['mid']);

        $list[$i]['level'] = $mb['level'];

    }

    // 원본
    if ($row['id'] == $row['reply_id']) {

        $list[$i]['reply'] = false;

    } else {
    // 대댓글

        $list[$i]['reply'] = true;

        if ($row['del'] && $reply_id != $row['reply_id']) {

            $list[$i]['delmsg'] = true;

            $reply_id = $row['reply_id'];

        }

    }

}

include_once("$bbs_skin_path/reply.php");
?>