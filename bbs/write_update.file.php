<?php // 파일
if (!defined("_WEB_")) exit;

$tmp_name = $_FILES['file'.$i]['tmp_name'];
$name = $_FILES['file'.$i]['name'];
$size = $_FILES['file'.$i]['size'];
$error = $_FILES['file'.$i]['error'];

$file = bbs_file($bbs_id, $article_id, $i);

if ($file['upload_file']) {

    if (is_uploaded_file($tmp_name) || $_POST['filedel_'.$i]) {

        if ($file['upload_file']) {

            $file_path = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];

            if (file_exists($file_path) && $file['upload_file']) {

                @unlink($file_path);

            }

            sql_query(" delete from $web[bbs_file_table] where id = '".addslashes($file['id'])."' ");

        }

    }

}

if ($size && $size > $bbs['bbs_file_size']) {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";
    echo "<script type='text/javascript'>";
    echo "alert('".text($name)." 파일이 업로드 제한크기(".file_size($bbs['bbs_file_size']).")를 초과하였습니다.');";
    echo "</script>";

}

if ($size && $size < $bbs['bbs_file_size']) {

    if (is_uploaded_file($tmp_name)) {

        $upload = array();
        $upload['source'] = $name;
        $upload['size'] = $size;

        $name = preg_replace("/\.(php|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $name);

        $upload['file'] = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr(md5(uniqid($web['server_time'])),0,8).'_'.str_replace('%', '', urlencode(substr($name,-10)));

        $dir = $disk['path']."/bbs/".$bbs_id;

        @mkdir($dir, 0707);
        @chmod($dir, 0707);

        $dir = $dir."/".data_path("", "");

        @mkdir($dir, 0707);
        @chmod($dir, 0707);

        $dest_file = $dir.'/'.$upload['file'];

        $error_code = move_uploaded_file($tmp_name, $dest_file) or die($error);

        @chmod($dest_file, 0606);

        $upload['image'] = @getimagesize($dest_file);

        $sql_common = "";
        $sql_common .= " set bbs_id = '".addslashes($bbs_id)."' ";
        $sql_common .= ", article_id = '".addslashes($article_id)."' ";
        $sql_common .= ", number = '".addslashes($i)."' ";
        $sql_common .= ", upload_source = '".trim(strip_tags(sql_real_escape_string($upload['source'])))."' ";
        $sql_common .= ", upload_file = '".$upload['file']."' ";
        $sql_common .= ", upload_filesize = '".$upload['size']."' ";
        $sql_common .= ", upload_width = '".$upload['image'][0]."' ";
        $sql_common .= ", upload_height = '".$upload['image'][1]."' ";
        $sql_common .= ", upload_type = '".$upload['image'][2]."' ";
        $sql_common .= ", upload_time = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[bbs_file_table] $sql_common ");

        $file_id = sql_insert_id();

        if (preg_match("/\.(jp[e]?g|gif|png)$/i", $dest_file) && ($upload['image'][2] == 1 || $upload['image'][2] == 2 || $upload['image'][2] == 3)) {

            if ($setup['image_scaling']) {

                if ($upload['image'][0] > $setup['image_scaling']) {

                    $dest_file2 = $dir . '/thumb' . $upload['file'];

                    image_resize($setup['image_scaling'], $setup['image_scaling'], $dest_file, $dest_file2, 100);

                    @unlink($dest_file);

                    $upload['image'] = @getimagesize($dest_file2);

                    $sql_common = "";
                    $sql_common .= " set upload_file = 'thumb".$upload['file']."' ";
                    $sql_common .= ", upload_filesize = '".@filesize($dest_file2)."' ";
                    $sql_common .= ", upload_width = '".$upload['image'][0]."' ";
                    $sql_common .= ", upload_height = '".$upload['image'][1]."' ";
                    $sql_common .= ", upload_type = '".$upload['image'][2]."' ";

                    sql_query(" update $web[bbs_file_table] $sql_common where id = '".$file_id."' ");

                }

            }

            $check_img = true;

        } else {

            $check_file = true;

        }

    }

}
?>