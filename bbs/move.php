<?php // 이동
include_once("./_tb.php");

if (!$check_bbsadmin && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>이동할 권한이 없습니다.</p>", "c");

}

$list = array();
$result = sql_query(" select * from $web[bbs_table] where bbs_id != '".$bbs_id."' order by bbs_title asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;

}
?>
<!DOCTYPE html>
<html>
<head>
<?
include_once("$web[path]/_head.php");
?>
<link rel="stylesheet" href="<?=$web['host_css']?>/adm.popup.css" type="text/css" />
<script type="text/javascript" src="<?=$web['host_js']?>/adm.popup.js"></script>
<script type="text/javascript">
function submitSetup()
{

    var f = document.formSetup;

    if (f.bbs_id2.value == '') {

        alert("이동 경로를 선택하세요.");
        return false;

    }

    f.action = "<?=$web['host_rbbs']?>/write_update.php";
    f.submit();

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<form method="post" name="formSetup">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="check_move" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<?
arsort($chk_id);

$i = 0;
foreach ($chk_id as $key => $val) {

    $k = $val;

    echo "<input type='hidden' name='list_id[".$i."]' value='".text($_POST['list_id'][$k])."' />";
    echo "<input type='hidden' name='chk_id[]' value='".text($i)."' />\n";

    $i++;

}
?>
<div class="popup">
<div class="form-top">
<div class="title"><span>게시물 이동</span></div>
<div class="text"><span>목록에서 선택하신 게시물을 포함된 댓글과 함께 다른 게시판으로 이동합니다.<br />(답변 글의 경우, 이동/복사 시 속성이 사라지니 유의하세요.)</span></div>
</div>
<!-- form-array start //-->
<div class="form-array">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="round1"></td>
    <td class="width1"></td>
    <td class="round2"></td>
</tr>
<tr>
    <td class="height1"></td>
    <td>
<!-- form-box start //-->
<div class="form-box">
<!-- list start //-->
<div class="list first">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">선택 게시물</td>
    <td class="ess on"></td>
    <td class="line"></td>
    <td class="text">총 <?=count($chk_id)?> 건</td>
</tr>
</table>
</div>
<!-- list end //-->
<!-- list start //-->
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">현재 경로</td>
    <td class="ess on"></td>
    <td class="line"></td>
    <td class="text"><?=text($bbs['bbs_title'])?></td>
</tr>
</table>
</div>
<!-- list end //-->
<!-- list start //-->
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">이동 경로</td>
    <td class="ess on"></td>
    <td class="line"></td>
    <td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>
<!-- selectbox //-->
<div class="selectbox">
<div class="select">
<div class="block">
<span class="text"></span>
<div class="option"><input type="hidden" name="bbs_id2" value="" class="add" />
<ul class="array">
<li name="" class="first">선택하세요.</li>
<?
for ($i=0; $i<count($list); $i++) {

    echo "<li name='".text($list[$i]['bbs_id'])."'>".text($list[$i]['bbs_title'])."</li>";

}
?>
</ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
    </td>
</tr>
</table>
    </td>
</tr>
</table>
</div>
<!-- list end //-->
</div>
<!-- form-box end //-->
    </td>
    <td class="height2"></td>
</tr>
<tr>
    <td class="round3"></td>
    <td class="width2"></td>
    <td class="round4"></td>
</tr>
</table>
</div>
<!-- form-array end //-->
</div>
<div class="form-save">
<div class="btn">
<a href="#" onclick="submitSetup(); return false;" class="submit"></a>
<a href="#" onclick="window.close();" class="cancel"></a>
</div>
</div>
</form>
</body>
</html>