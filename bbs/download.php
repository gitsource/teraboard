<?php
include_once("./_tb.php");
if ($number) { $number = preg_match("/^[0-9]+$/", $number) ? $number : ""; }

if (!$bbs_id || !$article_id || !$number) {

    message("<p class='title'>알림</p><p class='text'>파일이 존재하지 않습니다.</p>", "b");

}

if (!$article['id']) {

    message("<p class='title'>알림</p><p class='text'>파일이 존재하지 않습니다.</p>", "b");

}

// 성인인증
if ($bbs['bbs_adult']) {

    if ($check_login) {

        if (!$member['certify_name'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>본인확인이 필요합니다.</p>", "", $web['host_member']."/", true, true);

        }

        if (!$member['certify_adult'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>성인이 아닙니다.</p>", "", $web['host_member']."/", true, true);

        }

    } else {

        message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

    }

}

if ($member['level'] < $bbs['bbs_level_download']) {

    if ($check_login) {

        if ($member['level'] < $bbs['bbs_level_download']) {

            message("<p class='title'>알림</p><p class='text'>다운로드 권한이 없습니다.</p>", "b");

        }

    } else {

        message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

    }

}

// 비회원
$check_article_session = false;
if (!$article['mid'] && !$check_login) {

    $ss_name = "article_write_".$bbs_id."_".$article_id;
    if (get_session($ss_name)) {

        $check_article_session = true;

    }

}

// 비밀글
if ($article['ar_secret']) {

    $check_article_original = false;

    // 답글이다.
    if ($article['ar_id'] != $article_id) {

        // 원글
        $chk = article($bbs_id, $article['ar_id']);

        if ($chk['id']) {

            // 비회원
            if (!$chk['mid'] && !$check_login) {

                $ss_name = "article_write_".$bbs_id."_".$article['ar_id'];
                if (get_session($ss_name)) {

                    $check_article_original = true;

                }

            }

            // 회원
            if ($chk['mid'] && $member['mid'] == $chk['mid']) {

                $check_article_original = true;

            }

        }

    }

    if (!$article['mid'] && !$check_article_session && !$check_login) {

        message("<p class='title'>알림</p><p class='text'>게시물을 읽은 후 이용하여주세요.</p>", "b");

    }

    if ($check_bbsadmin || $check_admin) {

        // pass

    }

    else if ($article['mid'] && $member['mid'] == $article['mid'] || !$article['mid'] && $check_article_session || $check_article_original) {

        // pass

    } else {

        // 답글이면서 비회원이면
        if ($article['ar_id'] != $article_id && !$check_login) {

            message("<p class='title'>알림</p><p class='text'>게시물을 읽은 후 이용하여주세요.</p>", "b");

        } else {

            message("<p class='title'>알림</p><p class='text'>열람할 권한이 없습니다.</p>", "b");

        }

    }

}

$file = bbs_file($bbs_id, $article_id, $number);

if (!$file['upload_file']) {

    message("<p class='title'>알림</p><p class='text'>파일이 존재하지 않습니다.</p>", "b");

}

$file_path = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
$file_path = addslashes($file_path);

if (!is_file($file_path) || !file_exists($file_path)) {

    message("<p class='title'>알림</p><p class='text'>파일이 존재하지 않습니다.</p>", "b");

}

@include_once("$bbs_skin_path/download.top.php");

if ($setup['point_onoff'] && $bbs['bbs_point_download']) {

    if (!$check_login) {

        message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

    }

    if ($member['point'] < $bbs['bbs_point_download']) {

        message("<p class='title'>알림</p><p class='text'>다운로드 포인트 ".$bbs['bbs_point_download']."점이 부족하여<br />첨부파일을 내려 받으실 수 없습니다.</p>", "b");

    }

    member_point($member['mid'], ($bbs['bbs_point_download'] * -1), 3, "[{$bbs['bbs_title']} - {$article['ar_title']}] 다운로드", "article|".$bbs_id."|".$article_id."|download");

}

if ($setup['point_onoff'] && $bbs['bbs_point_download_write']) {

    if ($article['mid']) {

        member_point($article['mid'], $bbs['bbs_point_download_write'], 3, "[{$bbs['bbs_title']} - {$article['ar_title']}] 다운로드", "article|".$bbs_id."|".$article_id."|download_write");

    }

}

sql_query(" update $web[bbs_file_table] set upload_count = upload_count + 1 where id = '".$file['id']."' ");

@include_once("$bbs_skin_path/download.bottom.php");

if (preg_match("/msie/i", $_SERVER[HTTP_USER_AGENT]) || preg_match("/trident/i", $_SERVER[HTTP_USER_AGENT])) {

    $original = addslashes(urlencode($file['upload_source']));

    header("content-type: doesn/matter");
    header("content-length: ".filesize("$file_path"));
    header("content-disposition: attachment; filename=\"$original\"");
    header("content-transfer-encoding: binary");

} else {

    $original = addslashes($file['upload_source']);

    header("content-type: file/unknown");
    header("content-length: ".filesize("$file_path"));
    header("content-disposition: attachment; filename=\"$original\"");
    header("content-description: php generated data");

}

header("pragma: no-cache");
header("expires: 0");
flush();

$fp = fopen("$file_path", "rb");

while(!feof($fp)) {

    echo fread($fp, 100*1024);
    flush();

}

fclose ($fp);
flush();
?>