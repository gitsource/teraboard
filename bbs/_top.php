<?php // 상단
if (!defined("_WEB_")) exit;

if ($bbs['bbs_include_top']) {

    include_once("{$bbs['bbs_include_top']}");

} else {

    include_once("$web[path]/_top.php");

}
?>
<style type="text/css">
body {min-width:320px;}

.layout-subtop-adm {font-size:0; line-height:0;}
.layout-subtop-adm .wrap {position:relative; vertical-align:middle; height:40px; background-color:#f0f3f6; border:1px solid #dbe1e8;}
.layout-subtop-adm img {position:absolute; left:0; top:0;}
.layout-subtop-adm .title {margin-left:40px;}
.layout-subtop-adm .title {font-weight:700; line-height:40px; font-size:13px; color:#9baabb; font-family:'Nanum Gothic',gulim,serif;}
.layout-subtop-adm .btn {position:absolute; right:5px; top:5px; font-size:0px;}
.layout-subtop-adm .btn a {display:inline-block;}
.layout-subtop-adm .btn a {width:56px; text-align:center; font-size:0px; margin-left:4px; display:inline-block; background-color:#a1b1c2; border:1px solid #9baabb; border-radius:3px; padding:0 0 1px 0; cursor:pointer;}
.layout-subtop-adm .btn a {text-decoration:none; font-weight:700; line-height:27px; font-size:13px; color:#f0f3f6; font-family:'Nanum Gothic',gulim,serif;}
.layout-subtop-adm .btn a:first-child {margin-left:0px;}
.layout-subtop-adm .btn a:hover {background-color:#42abd7; border:1px solid #1192ca; color:#ffffff;}
.layout-subtop-adm .btn a:active {background-color:#1192ca; border:1px solid #1192ca; color:#ffffff;}
.layout-subtop-adm .btn div {display:inline-block;}
.layout-subtop-adm .btn div input {width:13px; height:13px; position:relative; overflow:hidden; left:0; top:3px;}
.layout-subtop-adm .btn div span {font-weight:700; line-height:12px; font-size:12px; color:#9baabb; font-family:'Nanum Gothic',gulim,serif;}
.layout-subtop-adm .btn div span {margin:0 10px 0 5px;}

.layout-subtop-title {width:100%; padding:50px 0; font-size:0; line-height:0;}
.layout-subtop-title .wrap {position:relative;}
.layout-subtop-title p {margin:0;}
.layout-subtop-title .title .text {display:table; text-decoration:none; font-weight:800; line-height:42px; font-size:42px; color:#111111; font-family:'Nanum Gothic',gulim,serif;}
.layout-subtop-title .title .text2 {margin-top:10px;}
.layout-subtop-title .title .text2 {font-weight:700; line-height:18px; font-size:16px; color:#111111; font-family:'Nanum Gothic',gulim,serif;}
.layout-subtop-title .line {width:100%; border-top:2px solid #d8d8d8; margin-top:40px;}
.layout-subtop-title .social .icon,
.layout-subtop-title .zoom .icon {display:block; position:absolute; left:-38px; top:0; width:38px; height:38px; background:url('<?=$web['host_img']?>/subtop_icon.png') no-repeat; background-size:76px 114px;}
.layout-subtop-title .zoom .icon {background-position:-38px 0;}
.layout-subtop-title .social {position:absolute; right:127px; bottom:40px;}
.layout-subtop-title .social .block {position:relative;}
.layout-subtop-title .social ul li {border:1px solid #ebebeb; border-left:0px; display:inline-block; vertical-align:top; width:38px; height:38px; background:url('<?=$web['host_img']?>/subtop_sns.png') no-repeat; background-size:190px 114px;}
.layout-subtop-title .social ul li:first-child {border-left:1px solid #ebebeb;}
.layout-subtop-title .social ul li.kakaostory {background-position:-38px 0;}
.layout-subtop-title .social ul li.kakaotalk {background-position:-76px 0;}
.layout-subtop-title .social ul li.twitter {background-position:-114px 0;}
.layout-subtop-title .social ul li.facebook {background-position:-152px 0;}
.layout-subtop-title .social ul li a {border:3px solid #ffffff; display:block; width:32px; height:32px;}
.layout-subtop-title .social ul li a:hover {border:3px solid transparent;}
.layout-subtop-title .zoom {position:absolute; right:0; bottom:40px;}
.layout-subtop-title .zoom .block {position:relative;}
.layout-subtop-title .zoom ul li {border:1px solid #ebebeb; border-left:0px; display:inline-block; vertical-align:top; width:38px; height:38px; background:url('<?=$web['host_img']?>/subtop_zoom.png') no-repeat; background-size:76px 114px;}
.layout-subtop-title .zoom ul li:first-child {border-left:1px solid #ebebeb;}
.layout-subtop-title .zoom ul li.zoom-in {background-position:-38px 0;}
.layout-subtop-title .zoom ul li span {border:3px solid #ffffff; display:block; width:32px; height:32px; cursor:pointer;}
.layout-subtop-title .zoom ul li span:hover {border:3px solid transparent;}

.layout-bbs {font-size:0; line-height:0;}
.layout-bbs p {margin:0px;}

@media screen and (max-width:1249px) {

.layout-subtop-title .wrap {margin:0 10px 0 10px;}

}

@media screen and (max-width:1000px) {

.layout-subtop-title .wrap {margin:0;}
.layout-subtop-title .title {margin:0 10px;}
.layout-subtop-title .social {left:10px; right:0; bottom:30px;}
.layout-subtop-title .zoom {right:10px; bottom:30px;}
.layout-subtop-title .line {margin-top:100px;}

}

@media screen and (max-width:640px) {

.layout-subtop-adm {display:none;}
.layout-subtop-adm .title {font-weight:bold; line-height:40px; font-size:13px; color:#9baabb; font-family:gulim,serif;}
.layout-subtop-adm .btn a {text-decoration:none; font-weight:bold; line-height:27px; font-size:13px; color:#f0f3f6; font-family:gulim,serif;}
.layout-subtop-title {padding:20px 0;}
.layout-subtop-title .social .icon,
.layout-subtop-title .zoom .icon {display:none;}
.layout-subtop-title .social {bottom:20px;}
.layout-subtop-title .zoom {bottom:20px;}
.layout-subtop-title .title .text {font-weight:bold; line-height:38px; font-size:36px; color:#111111; font-family:gulim,serif;}
.layout-subtop-title .title .text2 {margin-top:5px;}
.layout-subtop-title .title .text2 {font-weight:bold; line-height:18px; font-size:16px; color:#111111; font-family:gulim,serif;}
.layout-subtop-title .line {margin-top:85px;}

}
</style>
<script type="text/javascript">
var bbszoom_id = "bbszoom-<?=text($bbs_id)?>";
$(document).ready( function() {

    var bbszoom = parseInt($.cookie(bbszoom_id));

    if (!bbszoom) {
        bbszoom = 0;
    }

    var bbszoomUpdate = function(zoom) {

        $('.font-zoom, .font-zoom *').each(function() {

            $(this).css({
                'font-size': parseInt($(this).css("font-size")) + zoom+'px'
            });

        });

    }

    $('.zoom-in').click(function() {

        bbszoomUpdate(1);

        bbszoom++;
        $.cookie(bbszoom_id, bbszoom, { expires: 365, path: '/' });

    });

    $('.zoom-out').click(function() {

        bbszoomUpdate(-1);

        bbszoom--;

        $.cookie(bbszoom_id, bbszoom, { expires: 365, path: '/' });

    });

    if (bbszoom) {

        bbszoomUpdate(bbszoom);

    }

});
</script>
<? if ($check_bbsadmin || $check_admin) { ?>
<div class="layout-subtop-adm">
<div class="wrap">
<img src="<?=$web['host_img']?>/subtop_logo.png">
<div class="title">ADM OPTION</div>
<div class="btn">
<? if (preg_match("/(list\.php)/i", $_SERVER['PHP_SELF'])) { ?>
<? if ($article_id) { ?>
<? if ($bbs['btn_write']) { ?><a href="<?=$web['host_rbbs']?>/write.php?bbs_id=<?=$bbs_id?>">글쓰기</a><? } ?>
<? if ($bbs['btn_edit']) { ?><a href="<?=$web['host_rbbs']?>/write.php?m=u&amp;bbs_id=<?=$bbs_id?>&amp;article_id=<?=$article_id?>">수정</a><? } ?>
<? if ($bbs['btn_delete']) { ?><a href="#" onclick="articleDel('<?=$bbs_id?>','<?=$article_id?>'); return false;" class="btn_del">삭제</a><? } ?>
<? if ($bbs['btn_answer']) { ?><a href="<?=$web['host_rbbs']?>/write.php?m=a&amp;bbs_id=<?=$bbs_id?>&amp;article_id=<?=$article_id?>">답변</a><? } ?>
<? } else { ?>
<div><input type="checkbox" onclick="if (this.checked) checkAll(true); else checkAll(false);" class="checkbox" /><span>전체선택</span></div>
<? if ($bbs['btn_write']) { ?><a href="<?=$web['host_rbbs']?>/write.php?bbs_id=<?=$bbs_id?>">글쓰기</a><? } ?>
<? if ($bbs['btn_move']) { ?><a href="#" onclick="checkMove(); return false;" class="btn_move">이동</a><? } ?>
<? if ($bbs['btn_copy']) { ?><a href="#" onclick="checkCopy(); return false;" class="btn_copy">복제</a><? } ?>
<? if ($bbs['btn_delete']) { ?><a href="#" onclick="checkDel(); return false;" class="btn_del">삭제</a><? } ?>
<? } ?>
<? } ?>
<? if ($check_admin) { ?><a href="<?=$web['host_adm']?>/bbs_form.php?m=u&amp;bbs_id=<?=$bbs_id?>">설정</a><? } ?>
</div>
</div>
</div>
<? } ?>
<?
if ($bbs['bbs_text_top'] && $bbs['bbs_text_top'] != '<br>' && $bbs['bbs_text_top'] != '<br />' && $bbs['bbs_text_top'] != '<p>&nbsp;</p>' && $bbs['bbs_text_top'] != '&nbsp;') { echo "<div>".text2($bbs['bbs_text_top'],1)."</div>"; }
?>
<div class="layout-subtop-title">
<div class="wrap">
<div class="title">
<a href="<?=http_bbs($bbs_id, "")?>" class="font-zoom text"><?=text($bbs['bbs_title'])?></a>
<? if ($bbs['bbs_subject']) { ?><p class="font-zoom text2"><?=text($bbs['bbs_subject'])?></p><? } ?>
</div>
<? if (preg_match("/(list\.php)/i", $_SERVER['PHP_SELF'])) { ?>
<? if ($bbs['bbs_sns']) { ?>
<div class="social">
<div class="block">
<p class="icon"></p>
<ul>
<?
if ($article_id) {

    if ($check_touch) {

        echo "<li class='naverline'><a href='http://line.me/R/msg/text/?".urlencode(text($article['ar_title'],1))."%0d%0a".urlencode($web['host_bbs']."/".$bbs_id."/".$article_id)."' target='_blank' title='라인 퍼가기'></a></li>";
        echo "<li class='kakaostory'><a href='#' onclick=\"Kakao.Story.share({url: '".$web['host_bbs']."/".$bbs_id."/".$article_id."'}); return false;\" title='카카오스토리 퍼가기'></a></li>";
        echo "<li class='kakaotalk'><a href='#' onclick=\"kakaoLink();  return false;\" title='카카오톡 퍼가기'></a></li>";
        echo "<li class='twitter'><a href='https://twitter.com/intent/tweet?url=".urlencode($web['host_bbs']."/".$bbs_id."/".$article_id)."&amp;text=".urlencode(text($article['ar_title'],1))."' target='_blank' title='트위터 퍼가기'></a></li>";
        echo "<li class='facebook'><a href='https://www.facebook.com/sharer/sharer.php?u=".urlencode($web['host_bbs']."/".$bbs_id."/".$article_id)."' target='_blank' title='페이스북 퍼가기'></a></li>";

    } else {

        echo "<li class='kakaostory'><a href='https://story.kakao.com/share?url=".urlencode($web['host_bbs']."/".$bbs_id."/".$article_id)."' target='_blank' title='카카오스토리 퍼가기'></a></li>";
        echo "<li class='twitter'><a href='https://twitter.com/intent/tweet?url=".urlencode($web['host_bbs']."/".$bbs_id."/".$article_id)."&amp;text=".urlencode(text($article['ar_title'],1))."' target='_blank' title='트위터 퍼가기'></a></li>";
        echo "<li class='facebook'><a href='https://www.facebook.com/sharer/sharer.php?u=".urlencode($web['host_bbs']."/".$bbs_id."/".$article_id)."' target='_blank' title='페이스북 퍼가기'></a></li>";

    }

} else {

    if ($check_touch) {

        echo "<li class='naverline'><a href='http://line.me/R/msg/text/?".urlencode(text($bbs['bbs_title'],1))."%0d%0a".urlencode($web['host_bbs']."/".$bbs_id)."' target='_blank' title='라인 퍼가기'></a></li>";
        echo "<li class='kakaostory'><a href='#' onclick=\"Kakao.Story.share({url: '".$web['host_bbs']."/".$bbs_id."'}); return false;\" title='카카오스토리 퍼가기'></a></li>";
        echo "<li class='kakaotalk'><a href='#' onclick=\"kakaoLink();  return false;\" title='카카오톡 퍼가기'></a></li>";
        echo "<li class='twitter'><a href='https://twitter.com/intent/tweet?url=".urlencode($web['host_bbs']."/".$bbs_id)."&amp;text=".urlencode(text($bbs['bbs_title'],1))."' target='_blank' title='트위터 퍼가기'></a></li>";
        echo "<li class='facebook'><a href='https://www.facebook.com/sharer/sharer.php?u=".urlencode($web['host_bbs']."/".$bbs_id)."' target='_blank' title='페이스북 퍼가기'></a></li>";

    } else {

        echo "<li class='kakaostory'><a href='https://story.kakao.com/share?url=".urlencode($web['host_bbs']."/".$bbs_id)."' target='_blank' title='카카오스토리 퍼가기'></a></li>";
        echo "<li class='twitter'><a href='https://twitter.com/intent/tweet?url=".urlencode($web['host_bbs']."/".$bbs_id)."&amp;text=".urlencode(text($bbs['bbs_title'],1))."' target='_blank' title='트위터 퍼가기'></a></li>";
        echo "<li class='facebook'><a href='https://www.facebook.com/sharer/sharer.php?u=".urlencode($web['host_bbs']."/".$bbs_id)."' target='_blank' title='페이스북 퍼가기'></a></li>";

    }

}
?>
</ul>
</div>
</div>
<? } ?>
<div class="zoom">
<div class="block">
<p class="icon"></p>
<ul>
<li class="zoom-out" title="축소"><span></span></li>
<li class="zoom-in" title="확대"><span></span></li>
</ul>
</div>
</div>
<? } ?>
<p class="line"></p>
</div>
</div>