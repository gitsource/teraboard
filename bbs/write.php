<?php // 게시판
include_once("./_tb.php");

$check_notice = false;

if (!$bbs['bbs_id']) {

    message("<p class='title'>알림</p><p class='text'>존재하지 않는 게시판입니다.</p>", "b");

}

if (!$bbs['bbs_onoff'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>접근할 수 없습니다.</p>", "b");

}

// 성인인증
if ($bbs['bbs_adult']) {

    if ($check_login) {

        if (!$member['certify_name'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>본인확인이 필요합니다.</p>", "", $web['host_member']."/", true, true);

        }

        if (!$member['certify_adult'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>성인이 아닙니다.</p>", "", $web['host_member']."/", true, true);

        }

    } else {

        url($web['host_member']."/login/?url={$urlencode}");

    }

}

if ($m == '') {

    if ($member['level'] < $bbs['bbs_level_write']) {

        if ($check_login) {

            if ($member['level'] < $bbs['bbs_level_write']) {

                message("<p class='title'>알림</p><p class='text'>글을 작성할 권한이 없습니다.</p>", "b");

            }

        } else {

            url($web['host_member']."/login/?url={$urlencode}");

        }

    }

    if ($setup['bbs_write_time'] && !$check_bbsadmin && !$check_admin) {

        $chk = sql_fetch(" select id from {$web['article_table']}{$bbs_id} where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and datetime > '".date("Y-m-d H:i:s", $web['server_time'] - $setup['bbs_write_time'])."'  limit 0, 1 ");

        if ($chk['id']) {

            message("<p class='title'>알림</p><p class='text'>게시물 작성 제한으로 ".$setup['bbs_write_time']."초 후에 다시 시도하여주시기 바랍니다.</p>", "b");

        }

    }

    unset($article);

    $article['ar_content'] = $bbs['bbs_text_write'];

    if ($article['ar_content'] == '&nbsp;' || $article['ar_content'] == '<p>&nbsp;</p>') {

        $article['ar_content'] = "";

    }

}

else if ($m == 'a') {

    if (!$article['id']) {

        message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    $row = explode(",", $bbs['bbs_notice']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] == $article_id) {

            $check_notice = true;

            break;

        }

    }

    if ($member['level'] < $bbs['bbs_level_answer']) {

        if ($check_login) {

            if ($member['level'] < $bbs['bbs_level_answer']) {

                message("<p class='title'>알림</p><p class='text'>답변할 권한이 없습니다.</p>", "b");

            }

        } else {

            url($web['host_member']."/login/?url={$urlencode}");

        }

    }

    if ($article['ar_id'] != $article_id) {

        message("<p class='title'>알림</p><p class='text'>원글에만 답변이 가능합니다.</p>", "", http_bbs($bbs_id, ""));

    }

    if ($check_notice) {

        message("<p class='title'>알림</p><p class='text'>공지사항에는 답변할 수 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    if ($article['ar_secret'] && !$check_bbsadmin && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>비밀글에는 답변할 수 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    if ($setup['bbs_write_time'] && !$check_bbsadmin && !$check_admin) {

        $chk = sql_fetch(" select id from {$web['article_table']}{$bbs_id} where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and datetime > '".date("Y-m-d H:i:s", $web['server_time'] - $setup['bbs_write_time'])."'  limit 0, 1 ");

        if ($chk['id']) {

            message("<p class='title'>알림</p><p class='text'>게시물 작성 제한으로 ".$setup['bbs_write_time']."초 후에 다시 시도하여주시기 바랍니다.</p>", "b");

        }

    }

    $ar_title = $article['ar_title'];

    // 답변글 초기화
    unset($article);

    $article['ar_title'] = "[Re:]".$ar_title;

}

else if ($m == 'u') {

    if (!$article['id']) {

        message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    $row = explode(",", $bbs['bbs_notice']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] == $article_id) {

            $check_notice = true;

            break;

        }

    }

    // 비회원
    $check_article_session = false;
    if (!$article['mid'] && !$check_article_session && !$check_login) {

        $ss_name = "article_write_".$bbs_id."_".$article_id;
        if (get_session($ss_name)) {

            $check_article_session = true;

        } else {

            url($web['host_bbs']."/password.php?m=u&bbs_id={$bbs_id}&article_id={$article_id}");

        }

    }

    if ($check_bbsadmin || $check_admin) {

        // pass

    }

    else if ($article['mid'] && $member['mid'] == $article['mid'] || !$article['mid'] && $check_article_session) {

        // pass

    } else {

        message("<p class='title'>알림</p><p class='text'>수정할 권한이 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

} else {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "", http_bbs($bbs_id, ""));

}

$web['menu'] = $bbs['bbs_group'];
$web['menusub'] = "B-".$bbs['bbs_id'];
$web['title'] = $bbs['bbs_title'];
$web['description'] = text($bbs['bbs_subject']);

// head start
ob_start();

if ($web['head']) { echo $web['head']; }

// bbs css
echo "<link rel=\"stylesheet\" href=\"".$bbs_skin_url."/bbs.css\" type=\"text/css\" />\n";

$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("./_top.php");

echo "<div class='layout-bbs";

if ($setup['image_onoff']) {

    echo " image_onoff";

}

if ($setup['icon_onoff']) {

    echo " icon_onoff";

}

if ($bbs['bbs_good']) {

    echo " bbs_good";

}

if ($bbs['bbs_nogood']) {

    echo " bbs_nogood";

}

echo "'>";

@include_once("$bbs_skin_path/write.top.php");
include_once("$bbs_skin_path/write.php");
@include_once("$bbs_skin_path/write.bottom.php");

echo "</div>";

include_once("./_bottom.php");
?>