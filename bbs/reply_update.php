<?php
include_once("./_tb.php");
if ($callback) { $callback = preg_match("/^[a-zA-Z0-9_\-]+$/", $callback) ? $callback : ""; }
if ($mode) { $mode = preg_match("/^[a-zA-Z0-9_\-]+$/", $mode) ? $mode : ""; }

if ($m == 'blockkeyword') {

    $content = trim(sql_real_escape_string($_POST['content']));

    if ($content) {

        $chk = blockkeyword($content);

        if ($chk) {

            message("<p class='title'>알림</p><p class='text'>금지 단어가 포함되어있습니다. (".text($chk).")</p>", "", "", false, true);

        }

    }

    echo "<script type='text/javascript'>";
    echo "submitReplyUpdate('".$mode."');";
    echo "</script>";

    exit;

}

// 폼 체크
if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

if (!$bbs['bbs_id']) {

    message("<p class='title'>알림</p><p class='text'>존재하지 않는 게시판입니다.</p>", "b");

}

if (!$bbs['bbs_onoff'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>접근할 수 없습니다.</p>", "b");

}

if (!$article['id']) {

    message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, ""));

}

if ($m == '') {

    if ($reply_id) {

        $reply = reply($bbs_id, $reply_id);

        if (!$reply['id']) {

            message("<p class='title'>알림</p><p class='text'>댓글이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, $article_id));

        }

    }

    if ($member['level'] < $bbs['bbs_level_reply']) {

        if ($check_login) {

            if ($member['level'] < $bbs['bbs_level_reply']) {

                message("<p class='title'>알림</p><p class='text'>댓글을 작성할 권한이 없습니다.</p>", "b");

            }

        } else {

            message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

        }

    }

    if (!$content) {

        message("<p class='title'>알림</p><p class='text'>내용이 입력되지 않았습니다.</p>", "b");

    }

    $ip = trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])));
    $content = trim(sql_real_escape_string($_POST['content']));

    if ($check_login) {

        $mid = trim(strip_tags(sql_real_escape_string($member['mid'])));
        $nick = trim(strip_tags(sql_real_escape_string($member['nick'])));
        $upw = "";

    } else {

        $mid = "";
        $nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));
        $upw = sql_password(trim(strip_tags(sql_real_escape_string($_POST['upw']))));

    }

    @include_once("$bbs_skin_path/reply_update.top.php");

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", upw = '".$upw."' ";
    $sql_common .= ", ip = '".$ip."' ";
    $sql_common .= ", article_id = '".$article_id."' ";
    $sql_common .= ", content = '".$content."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into {$web['reply_table']}{$bbs_id} $sql_common ");

    $reply_id = sql_insert_id();

    if ($reply['id']) {

        sql_query(" update {$web['reply_table']}{$bbs_id} set reply_id = '".$reply['id']."' where id = '".$reply_id."' ");

    } else {

        sql_query(" update {$web['reply_table']}{$bbs_id} set reply_id = '".$reply_id."' where id = '".$reply_id."' ");

    }

    sql_query(" update {$web['article_table']}{$bbs_id}  set ar_reply = ar_reply + 1 where id = '".$article_id."' ");
    sql_query(" update $web[bbs_table] set bbs_reply_count = bbs_reply_count + 1 where bbs_id = '".$bbs_id."' ");

    sql_query(" update $web[member_table] set bbs_reply_count = bbs_reply_count + 1 where mid = '".$mid."' ");
    sql_query(" update $web[search_table] set ar_reply = ar_reply + 1 where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
    sql_query(" update $web[notice_table] set ar_reply = ar_reply + 1, updatetime = '".$web['time_ymdhis']."', readtime = '0000-00-00 00:00:00', reply_new = '".$reply_id."' where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply = '0' ");

    if ($reply['id']) {

        sql_query(" update $web[notice_table] set updatetime = '".$web['time_ymdhis']."', readtime = '0000-00-00 00:00:00' where bbs_id = '".$bbs_id."' and reply_first = '".$reply['id']."' ");
        sql_query(" update $web[notice_table] set updatetime = '".$web['time_ymdhis']."', readtime = '0000-00-00 00:00:00' where bbs_id = '".$bbs_id."' and reply_id = '".$reply['id']."' ");

    }

    member_level_auto($mid);

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", bbs_id = '".$bbs_id."' ";
    $sql_common .= ", bbs_title = '".trim(strip_tags(sql_real_escape_string(stripslashes($bbs['bbs_title']))))."' ";
    $sql_common .= ", article_id = '".$article_id."' ";
    $sql_common .= ", ar_title = '".trim(strip_tags(sql_real_escape_string(stripslashes($article['ar_title']))))."' ";
    $sql_common .= ", content = '".$content."' ";
    $sql_common .= ", reply = '1' ";
    $sql_common .= ", reply_id = '".$reply_id."' ";
    if ($reply['id']) {
        $sql_common .= ", reply_first = '".$reply['id']."' ";
    }
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";
    $sql_common .= ", updatetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[notice_table] $sql_common ");

    if ($bbs['bbs_point_reply']) {

        member_point($mid, $bbs['bbs_point_reply'], 3, "[{$bbs['bbs_title']} - {$article['ar_title']}] 댓글 등록", "article|".$bbs_id."|".$article_id."|".$reply_id."|regist");

    }

    if (!$check_login) {

        $ss_name = "reply_write_".$bbs_id."_".$article_id."_".$reply_id;
        if (!get_session($ss_name)) {

            set_session($ss_name, true);

        }

    }

    @include_once("$bbs_skin_path/reply_update.bottom.php");

    url(http_bbs($bbs_id, $article_id)."#reply{$reply_id}");

}

else if ($m == 'u') {

    $reply = reply($bbs_id, $reply_id);

    if (!$reply['id']) {

        message("<p class='title'>알림</p><p class='text'>댓글이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, $article_id));

    }

    // 비회원
    $check_reply_session = false;
    if (!$reply['mid'] && !$check_login) {

        $ss_name = "reply_write_".$bbs_id."_".$article_id."_".$reply_id;
        if (get_session($ss_name)) {

            $check_reply_session = true;

        }

    }

    if ($check_bbsadmin || $check_admin) {

        // pass

    }

    else if ($reply['mid'] && $member['mid'] == $reply['mid'] || !$reply['mid'] && $check_reply_session) {

        // pass

    } else {

        message("<p class='title'>알림</p><p class='text'>수정할 권한이 없습니다.</p>", "", http_bbs($bbs_id, $article_id));

    }

    $ip = trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])));
    $content = trim(sql_real_escape_string($_POST['content']));

    if ($check_login) {

        if ($check_bbsadmin || $check_admin) {

            if ($member['mid'] == $reply['mid']) {

                $nick = trim(strip_tags(sql_real_escape_string($member['nick'])));

            }

            else if ($reply['mid']) {

                $nick = trim(strip_tags(sql_real_escape_string($reply['nick'])));

            } else {

                $nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));

            }

        } else {

            $nick = trim(strip_tags(sql_real_escape_string($member['nick'])));

        }


    } else {

        $nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));

    }

    @include_once("$bbs_skin_path/reply_update.top.php");

    $sql_common = "";
    $sql_common .= " set nick = '".$nick."' ";
    $sql_common .= ", content = '".$content."' ";

    sql_query(" update {$web['reply_table']}{$bbs_id} $sql_common where id = '".$reply_id."' ");

    $sql_common = "";
    $sql_common .= " set nick = '".$nick."' ";
    $sql_common .= ", content = '".$content."' ";

    sql_query(" update $web[notice_table] $sql_common where bbs_id = '".$bbs_id."' and reply_id = '".$reply_id."' ");

    @include_once("$bbs_skin_path/reply_update.bottom.php");

    url(http_bbs($bbs_id, $article_id)."#reply{$reply_id}");

}

else if ($m == 'd') {

    $reply = reply($bbs_id, $reply_id);

    if (!$reply['id']) {

        message("<p class='title'>알림</p><p class='text'>댓글이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, $article_id));

    }

    // 비회원
    $check_reply_session = false;
    if (!$reply['mid'] && !$check_login) {

        $ss_name = "reply_write_".$bbs_id."_".$article_id."_".$reply_id;
        if (get_session($ss_name)) {

            $check_reply_session = true;

        }

    }

    if ($check_bbsadmin || $check_admin) {

        // pass

    }

    else if ($reply['mid'] && $member['mid'] == $reply['mid'] || !$reply['mid'] && $check_reply_session) {

        // pass

    } else {

        message("<p class='title'>알림</p><p class='text'>삭제할 권한이 없습니다.</p>", "", http_bbs($bbs_id, $article_id));

    }

    @include_once("$bbs_skin_path/reply_update.top.php");

    sql_query(" delete from {$web['reply_table']}{$bbs_id} where id = '".$reply_id."' ");
    sql_query(" update {$web['reply_table']}{$bbs_id} set del = 1 where reply_id = '".$reply_id."' ");
    sql_query(" update {$web['article_table']}{$bbs_id} set ar_reply = ar_reply - 1 where id = '".$article_id."' ");
    sql_query(" update $web[bbs_table] set bbs_reply_count = bbs_reply_count - 1 where bbs_id = '".$bbs_id."' ");

    sql_query(" update $web[search_table] set ar_reply = ar_reply - 1 where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
    sql_query(" update $web[notice_table] set ar_reply = ar_reply - 1 where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
    sql_query(" delete from $web[notice_table] where bbs_id = '".$bbs_id."' and reply_id = '".$reply_id."' ");

    if ($bbs['bbs_point_reply']) {

        member_point($reply['mid'], $bbs['bbs_point_reply'] * -1, 3, "[{$bbs['bbs_title']} - {$article['ar_title']}] 댓글 삭제", "article|".$bbs_id."|".$article_id."|".$reply_id."|delete");

    }

    $sql_common = "";
    $sql_common .= " set bbs_reply_count = bbs_reply_count - 1 ";

    sql_query(" update $web[member_table] $sql_common where mid = '".$reply['mid']."' ");

    @include_once("$bbs_skin_path/reply_update.bottom.php");

    url(http_bbs($bbs_id, $article_id));

}

else if ($m == 'p') {

    $reply = reply($bbs_id, $reply_id);

    if (!$reply['id']) {

        echo "<font color='#f26d7d'>댓글이 삭제되었거나 존재하지 않습니다.</font>";
        exit;

    }

    if ($check_login) {

        echo "<font color='#f26d7d'>비회원이 아닙니다.</font>";
        exit;

        message("<p class='title'>알림</p><p class='text'></p>", "", "", false, true);

    }

    if ($reply['mid']) {

        echo "<font color='#f26d7d'>비회원이 작성한 댓글이 아닙니다.</font>";
        exit;

    }

    $upw = trim($_POST['upw']);

    if (!$upw) {

        echo "<font color='#f26d7d'>비밀번호를 입력하세요.</font>";
        exit;

    }

    if (sql_password($upw) != $reply['upw']) {

        echo "<font color='#f26d7d'>비밀번호가 일치하지 않습니다.</font>";
        exit;

    }

    $ss_name = "reply_write_".$bbs_id."_".$article_id."_".$reply_id;
    if (!get_session($ss_name)) {

        set_session($ss_name, true);

    }

    if ($callback == 'u') {

        echo "<script type='text/javascript'>";
        echo "replyEdit('".$reply_id."','', '1');";
        echo "</script>";

    }

    else if ($callback == 'd') {

        echo "<script type='text/javascript'>";
        echo "replyDelete('".$reply_id."','', '1');";
        echo "</script>";

    }

    exit;

} else {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "", http_bbs($bbs_id, ""));

}
?>