<?php // 게시판
include_once("./_tb.php");

if ($sort) { $sort = preg_match("/^[0-9]+$/", $sort) ? $sort : ""; }
if ($rows) { $rows = preg_match("/^[0-9]+$/", $rows) ? $rows : ""; }

if (!$bbs['bbs_id']) {

    message("<p class='title'>알림</p><p class='text'>존재하지 않는 게시판입니다.</p>", "b");

}

if (!$bbs['bbs_onoff'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>접근할 수 없습니다.</p>", "b");

}

$bbs['btn_move'] = false;
$bbs['btn_copy'] = false;
$bbs['btn_write'] = false;
$bbs['btn_delete'] = false;
$bbs['btn_edit'] = false;
$bbs['btn_answer'] = false;

if ($check_bbsadmin || $check_admin) {

    $bbs['btn_move'] = true;
    $bbs['btn_copy'] = true;
    $bbs['btn_delete'] = true;

}

if ($member['level'] >= $bbs['bbs_level_write']) {

    $bbs['btn_write'] = true;

}

// 성인인증
if ($bbs['bbs_adult']) {

    if ($check_login) {

        if (!$member['certify_name'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>본인확인이 필요합니다.</p>", "", $web['host_member']."/", true, true);

        }

        if (!$member['certify_adult'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>성인이 아닙니다.</p>", "", $web['host_member']."/", true, true);

        }

    } else {

        url($web['host_member']."/login/?url={$urlencode}");

    }

}

if ($member['level'] < $bbs['bbs_level_list']) {

    if ($check_login) {

        if ($member['level'] < $bbs['bbs_level_list']) {

            message("<p class='title'>알림</p><p class='text'>접근할 수 없는 게시판입니다.</p>", "b");

        }

    } else {

        url($web['host_member']."/login/?url={$urlencode}");

    }

}

$href = "";
if ($ct) {

    $href .= "&amp;ct=".urlencode($ct);

}

if ($sort) {

    $href .= "&amp;sort=".$sort;

}

if ($c) {

    $href .= "&amp;c=".addslashes($c);

}

if ($q) {

    $href .= "&amp;q=".addslashes($q);

}

if ($p && $p > 1) {

    $href .= "&amp;p=".$p;

}

$web['menu'] = $bbs['bbs_group'];
$web['menusub'] = "B-".$bbs['bbs_id'];
$web['title'] = $bbs['bbs_title'];
$web['description'] = text($bbs['bbs_subject']);

// 뷰
if ($article_id) {

    if (!$article['id']) {

        url(http_bbs($bbs_id,""));

    }

    if ($member['level'] < $bbs['bbs_level_view']) {

        if ($check_login) {

            if ($member['level'] < $bbs['bbs_level_view']) {

                message("<p class='title'>알림</p><p class='text'>열람할 권한이 없습니다.</p>", "b");

            }

        } else {

            url($web['host_member']."/login/?url={$urlencode}");

        }

    }

    // 공지글
    $check_notice = false;
    $row = explode(",", $bbs['bbs_notice']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] == $article_id) {

            $check_notice = true;

            break;

        }

    }

    // 비회원
    $check_article_session = false;
    if (!$article['mid'] && !$check_login) {

        $ss_name = "article_write_".$bbs_id."_".$article_id;
        if (get_session($ss_name)) {

            $check_article_session = true;

        }

    }

    // 비밀글
    if ($article['ar_secret']) {

        $check_article_original = false;

        // 답글이다.
        if ($article['ar_id'] != $article_id) {

            // 원글
            $chk = article($bbs_id, $article['ar_id']);

            if ($chk['id']) {

                // 비회원
                if (!$chk['mid'] && !$check_login) {

                    $ss_name = "article_write_".$bbs_id."_".$article['ar_id'];
                    if (get_session($ss_name)) {

                        $check_article_original = true;

                    }

                }

                // 회원
                if ($chk['mid'] && $member['mid'] == $chk['mid']) {

                    $check_article_original = true;

                }

            }

        }

        if (!$article['mid'] && !$check_article_session && !$check_login) {

            url($web['host_bbs']."/password.php?bbs_id={$bbs_id}&article_id={$article_id}");

        }

        if ($check_bbsadmin || $check_admin) {

            // pass

        }

        else if ($article['mid'] && $member['mid'] == $article['mid'] || !$article['mid'] && $check_article_session || $check_article_original) {

            // pass

        } else {

            // 답글이면서 비회원이면
            if ($article['ar_id'] != $article_id && !$check_login) {

                url($web['host_bbs']."/password.php?bbs_id={$bbs_id}&article_id={$article['ar_id']}&callback_id={$article_id}");

            } else {

                message("<p class='title'>알림</p><p class='text'>열람할 권한이 없습니다.</p>", "b");

            }

        }

    }

    $ss_name = "article_view_".$bbs_id."_".$article_id;
    if (!get_session($ss_name)) {

        // 조회수 증가
        sql_query(" update {$web['article_table']}{$bbs_id} set ar_hit = ar_hit + 1 where id = '".$article_id."' ");
        sql_query(" update $web[search_table] set ar_hit = ar_hit + 1 where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

        set_session($ss_name, true);

    }

    if ($check_login) {

        // 알림 업데이트
        sql_query(" update $web[notice_table] set readtime = '".$web['time_ymdhis']."' where mid = '".$member['mid']."' and bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    }

    // 이전글, 다음글을 위한 조건절(list 와 동일)
    $sql_search = "";

    if ($ct) {

        $sql_search .= " and ar_category = '".addslashes($ct)."' ";

    }

    if ($q) {

        $q = addslashes($q);

        if ($c && $q) {

            if ($c == 1) {

                $sql_search .= " and INSTR(ar_title, '".$q."') ";

            }

            else if ($c == 2) {

                $sql_search .= " and INSTR(ar_content, '".$q."') ";

            }

            else if ($c == 3) {

                $sql_search .= " and INSTR(nick, '".$q."') ";

            }

            else if ($c == 4) {

                $sql_search .= " and INSTR(ar_keyword, '".$q."') ";

            }

            else if ($c == 5) {

                $sql_search .= " and mid = '".$q."' ";

            }

        } else {

            $sql_search .= " and (INSTR(ar_title, '".$q."') or INSTR(ar_content, '".$q."') or INSTR(nick, '".$q."') or INSTR(ar_keyword, '".$q."')) ";

        }

    }

    // 링크
    $article['href_list'] = http_bbs($bbs_id,"").$href;

    // 이전글, 다음글
    $article_prev = sql_fetch(" select * from {$web['article_table']}{$bbs_id} where id > '".$article_id."' $sql_search order by id asc ");
    $article_next = sql_fetch(" select * from {$web['article_table']}{$bbs_id} where id < '".$article_id."' $sql_search order by id desc ");
    $article['href_prev'] = "";
    $article['href_next'] = "";
    if ($article_prev['id']) { $article['href_prev'] = http_bbs($bbs_id, $article_prev['id']).$href; }
    if ($article_next['id']) { $article['href_next'] = http_bbs($bbs_id, $article_next['id']).$href; }

    // 수정삭제 버튼
    if ($article['mid'] && $member['mid'] == $article['mid'] || !$article['mid'] && !$check_login || $check_bbsadmin || $check_admin) {

        $bbs['btn_edit'] = true;
        $bbs['btn_delete'] = true;

    }

    //  답글사용, 원글만, 공지불가, 권한
    if ($bbs['bbs_answer'] && $article['ar_id'] == $article_id && !$check_notice && $member['level'] >= $bbs['bbs_level_answer']) {

        // 비밀글은 관리자만
        if ($article['ar_secret']) {

            if ($check_bbsadmin || $check_admin) {

                $bbs['btn_answer'] = true;

            }

        } else {

            $bbs['btn_answer'] = true;

        }

    }

    // 첨부파일
    $articleimage = array();
    $articlefile = array();
    if ($bbs['bbs_file_count']) {

        $n = 0;
        $n2 = 0;
        $result = sql_query(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($bbs_id)."' and article_id = '".$article_id."' order by number+1 asc limit 0, 100 ");
        for ($i=0; $row=sql_fetch_array($result); $i++) {

            if ($row['upload_type'] == 1 || $row['upload_type'] == 2 || $row['upload_type'] == 3) {

                $articleimage[$n] = $row;

                $n++;

            } else {

                $articlefile[$n2] = $row;
                $articlefile[$n2]['href'] = $web['host_rbbs']."/download.php?bbs_id=".$bbs_id."&amp;article_id=".$article_id."&amp;number=".$row['number'];

                $n2++;

            }

        }

    }

    // 작성자, 제목, 내용
    $article['nick'] = text($article['nick']);

    // 타이틀 제목
    $web['title'] = $article['ar_title'];
    $web['description'] = text3($article['ar_content'], 255);

    if ($articleimage[0]['upload_file']) {

        $web['meta_image'] = $disk['server_bbs']."/bbs/".$bbs_id."/".data_path("u", $articleimage[0]['upload_time'])."/".text($articleimage[0]['upload_file']);
        $web['meta_image_width'] = $articleimage[0]['upload_width'];
        $web['meta_image_height'] = $articleimage[0]['upload_height'];

    } else {

        $web['meta_image'] = image_editor($article['ar_content']);

        if ($web['meta_image']) {
            $web['meta_image_width'] = 100;
            $web['meta_image_height'] = 100;
        }

    }

    $article['ar_title'] = text($article['ar_title']);

}

$web['meta_subject'] = $bbs['bbs_title'];
$web['meta_type'] = "article";

// head start
ob_start();

if ($web['head']) { echo $web['head']; }

// bbs css
echo "<link rel=\"stylesheet\" href=\"".$bbs_skin_url."/bbs.css\" type=\"text/css\" />\n";

$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("./_top.php");

echo "<div class='layout-bbs";

if ($setup['image_onoff']) {

    echo " image_onoff";

}

if ($setup['icon_onoff']) {

    echo " icon_onoff";

}

if ($bbs['bbs_good']) {

    echo " bbs_good";

}

if ($bbs['bbs_nogood']) {

    echo " bbs_nogood";

}

echo "'>";

// 뷰
if ($article_id) {

    @include_once("$bbs_skin_path/view.top.php");
    include_once("$bbs_skin_path/view.php");
    @include_once("$bbs_skin_path/view.bottom.php");

}

// 리스트
if (!$article_id || $bbs['bbs_view_list']) {

    $check_count = false;

    $sql_search = " where 1 ";

    if ($ct) {

        $sql_search .= " and ar_category = '".addslashes($ct)."' ";

        $check_count = true;

    }

    if ($q) {

        $q = addslashes($q);

        if ($c && $q) {

            if ($c == 1) {

                $sql_search .= " and INSTR(ar_title, '".$q."') ";

            }

            else if ($c == 2) {

                $sql_search .= " and INSTR(ar_content, '".$q."') ";

            }

            else if ($c == 3) {

                $sql_search .= " and INSTR(nick, '".$q."') ";

            }

            else if ($c == 4) {

                $sql_search .= " and INSTR(ar_keyword, '".$q."') ";

            }

            else if ($c == 5) {

                $sql_search .= " and mid = '".$q."' ";

            }

        } else {

            $sql_search .= " and (INSTR(ar_title, '".$q."') or INSTR(ar_content, '".$q."') or INSTR(nick, '".$q."') or INSTR(ar_keyword, '".$q."')) ";

        }

        $check_count = true;

    }

    if ($sort == 1) { $sql_order = " id desc "; }
    else if ($sort == 2) { $sql_order = " ar_reply desc "; }
    else if ($sort == 3) { $sql_order = " ar_hit desc "; }
    else if ($sort == 4) { $sql_order = " ar_good desc "; }
    else if ($sort == 5) { $sql_order = " ar_nogood desc "; }
    else { $sql_order = " ".$bbs['bbs_order']." "; }

    if ($check_count) {

        $cnt = sql_fetch(" select count(id) as cnt from {$web['article_table']}{$bbs_id} $sql_search ");

        $total_count = $cnt['cnt'];

    } else {

        $total_count = $bbs['bbs_write_count'];

    }

    if (!$rows || $rows && $rows > 100) {

        $rows = $bbs['bbs_rows'];

    }

    $paging = http_bbs($bbs_id,"");
    if ($ct) { $paging .= "&amp;ct=".addslashes($ct); }
    if ($rows && $bbs['bbs_rows'] != $rows) { $paging .= "&amp;rows=".addslashes($rows); }
    if ($c) { $paging .= "&amp;c=".addslashes($c); }
    if ($q) { $paging .= "&amp;q=".addslashes($q); }
    if ($sort) { $paging .= "&amp;sort=".addslashes($sort); }
    $paging .= "&amp;p=";

    $total_page  = ceil($total_count / $rows);
    if (!$p) { $p = 1; }
    $from_record = ($p - 1) * $rows;
    $paging = paging(5, $p, $total_page, $paging);

    // 공지
    $notice = array();

    // 1페이지, 분류, 검색이 아닐 때
    if ($p == 1 && !$ct && !$q) {

        if ($bbs['bbs_notice']) {

            $k = 0;
            $s = explode(",", $bbs['bbs_notice']);
            for ($i=0; $i<count($s); $i++) {

                $row = article($bbs_id, $s[$i]);

                if ($row['id']) {

                    $notice[$k] = $row;
                    $notice[$k]['href'] = http_bbs($bbs_id, $row['id']).$href;
                    $notice[$k]['number'] = "notice";
                    $notice[$k]['title'] = text($row['ar_title']);
                    $notice[$k]['ar_title'] = text($row['ar_title']);

                    $k++;

                }

            }

        }

    }

    // 일반
    $list = array();
    $result = sql_query(" select * from {$web['article_table']}{$bbs_id} $sql_search order by $sql_order limit $from_record, $rows ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $list[$i] = $row;
        $list[$i]['href'] = http_bbs($bbs_id, $row['id']).$href;
        $list[$i]['number'] = $total_count - ($p - 1) * $rows - $i;
        $list[$i]['title'] = text($row['ar_title']);
        $list[$i]['ar_title'] = text($row['ar_title']);

        // 수정삭제 버튼
        $list[$i]['btn_edit'] = false;
        $list[$i]['btn_delete'] = false;

        if ($row['mid'] && $member['mid'] == $row['mid'] || !$row['mid'] && !$check_login || $check_bbsadmin || $check_admin) {

            $list[$i]['btn_edit'] = true;
            $list[$i]['btn_delete'] = true;

        }

        if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'])) {

            $list[$i]['date'] = "오늘";

        }

        else if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'] - (86400 * 1))) {

            $list[$i]['date'] = "어제";

        }

        else if (date("Y-m-d", strtotime($row['datetime'])) == date("Y-m-d", $web['server_time'] - (86400 * 2))) {

            $list[$i]['date'] = "그제";

        } else {

            $list[$i]['date'] = date("m-d", strtotime($row['datetime']));

        }

        $list[$i]['icon_new'] = false;
        $list[$i]['icon_hot'] = false;
        $list[$i]['icon_img'] = false;
        $list[$i]['icon_file'] = false;
        $list[$i]['icon_source'] = false;
        $list[$i]['icon_secret'] = false;
        $list[$i]['icon_answer'] = false;

        if (date("Y-m-d", strtotime($row['datetime'])) >= date("Y-m-d H:i:s", $web['server_time'] - (3600 * $bbs['bbs_icon_new']))) {

            $list[$i]['icon_new'] = true;

        }

        if ($row['ar_hit'] >= $bbs['bbs_icon_hit']) {

            $list[$i]['icon_hot'] = true;

        }

        if ($row['ar_img']) {

            $list[$i]['icon_img'] = true;

        }

        if ($row['ar_file']) {

            $list[$i]['icon_file'] = true;

        }

        if ($row['ar_source']) {

            $list[$i]['icon_source'] = true;

        }

        if ($row['ar_secret']) {

            $list[$i]['icon_secret'] = true;

        }

        if ($row['id'] != $row['ar_id']) {

            $list[$i]['icon_answer'] = true;

        }

    }

    include_once("$bbs_skin_path/list.php");

}

echo "</div>";

include_once("./_bottom.php");
?>