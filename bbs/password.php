<?php // 비밀번호
include_once("./_tb.php");
if ($callback) { $callback = preg_match("/^[a-zA-Z0-9_\-]+$/", $callback) ? $callback : ""; }
if ($callback_id) { $callback_id = preg_match("/^[0-9]+$/", $callback_id) ? $callback_id : ""; }

if ($m == 'check') {

    $upw = trim($_POST['upw']);

    if (!$article['id']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "<font color='#f26d7d'>게시물이 삭제되었습니다.</font>";
        exit;

    }

    if ($article['mid']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "<font color='#f26d7d'>비회원이 작성한 게시물이 아닙니다.</font>";
        exit;

    }

    if (!$upw) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "비밀번호를 입력하세요.";
        exit;

    }

    if (sql_password($upw) != $article['upw']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "<font color='#f26d7d'>비밀번호가 일치하지 않습니다.</font>";
        exit;

    }

    $ss_name = "article_write_".$bbs_id."_".$article_id;
    if (!get_session($ss_name)) {

        set_session($ss_name, true);

    }

    if ($callback == 'u') {

        url($web['host_rbbs']."/write.php?m=u&bbs_id=".$bbs_id."&article_id=".$article_id);

    }

    else if ($callback == 'd') {

        echo "<script type='text/javascript'>";
        echo "articleDel();";
        echo "</script>";

    } else {

        if ($callback_id) {

            url(http_bbs($bbs_id, $callback_id));

        } else {

            url(http_bbs($bbs_id, $article_id));

        }

    }

    exit;

}

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px; min-height:400px;}

.layout-bbs {width:300px; margin:0 auto; padding:50px 0;}

<? if ($m == '') { ?>
.layout-bbs .title {position:relative; margin:0 auto; margin-bottom:19px; width:162px; height:162px; background:url('<?=$web['host_img']?>/title_secret.png') no-repeat;}
<? } else { ?>
.layout-bbs .title {position:relative; margin:0 auto; margin-bottom:19px; width:162px; height:162px; background:url('<?=$web['host_img']?>/title_guest.png') no-repeat;}
<? } ?>

.layout-bbs .help {text-align:center; padding-top:10px;}
.layout-bbs .help {font-weight:bold; line-height:22px; font-size:13px; color:#999999; font-family:gulim,serif;}

.layout-bbs .upw {margin-top:26px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_pw.png') no-repeat left 0; background-color:#ffffff;}
.layout-bbs .upw input {margin-left:40px; width:256px; height:49px; border:0px; background:transparent;}
.layout-bbs .upw input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-bbs .upw.on {border:1px solid #1192ca; background-position:0 -49px;}
.layout-bbs .upw.on input {color:#000000;}
.layout-bbs .upw span {position:absolute; left:40px; top:0px; display:block; width:100px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}

.layout-bbs .msg {display:none; text-align:center; padding-top:30px;}
.layout-bbs .msg {font-weight:bold; line-height:18px; font-size:13px; color:#f26d7d; font-family:gulim,serif;}

 .submit {margin-top:20px;}
 .submit div {display:inline-block; width:50%;}
 .submit div span {margin-left:2px; display:block; text-align:center; cursor:pointer; height:59px; border:1px solid #dadada; background-color:#ffffff;}
 .submit div span {font-weight:700; line-height:59px; font-size:20px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
 .submit div.on span {margin-left:0px; margin-right:2px; border:1px solid #1192ca; background-color:#42abd7; color:#ffffff;}

@media screen and (max-width:600px) {

.layout-bbs {width:320px;}
.layout-bbs .upw input {width:278px;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    $('#upw').focus(function() {

        $(this).parent().addClass('on');
        $('#msg').hide();

    }).blur(function() {

        $(this).parent().removeClass('on');
        $('#msg').hide();

        if ($(this).val() == '') {

            $(this).parent().find('span').show();

        }

    }).keydown(function() {

        $(this).parent().find('span').hide();
 
    });

    $('.upw span').click(function() {

        $(this).parent().find('input').focus();

    });

    $('#upw').keyup(function(e) {

        if (e.keyCode == 13) {

            upwCheck();

        }

    });

    $('.layout-bbs .submit .btn_confirm').click(function() {

        upwCheck();

    });

    $('.layout-bbs .submit .btn_cancel').click(function() {

        history.go(-1);

    });

});

function upwCheck()
{

    var f = document.formSetup;

    if (f.upw.value == '') {

        alert("비밀번호를 입력하세요.");
        f.upw.focus();
        return false;

    }

    $.post("<?=$web['host_bbs']?>/password.php", {"m" : "check", "callback" : f.m.value, "callback_id" : f.callback_id.value, "bbs_id" : f.bbs_id.value, "article_id" : f.article_id.value, "upw" : f.upw.value}, function(data) {

        $("#msg").html(data);

    });

}

function articleDel()
{

    var f = document.formSetup;

    f.action = "<?=$web['host_rbbs']?>/write_update.php";
    f.submit();

}
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("$web[path]/_top.php");
?>
<form method="post" name="formSetup" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="m" value="<?=$m?>" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<input type="hidden" name="article_id" value="<?=$article_id?>" />
<input type="hidden" name="callback_id" value="<?=$callback_id?>" />
<div class="layout-bbs">
<div class="title"><span></span></div>
<div class="help">
<?
if ($m == '') {

    echo "비회원이 작성한 비밀글입니다.<br />작성 시 입력한 비밀번호를 입력하세요.";

}

else if ($m == 'u' || $m == 'd') {

    echo "비회원이 작성한 게시물입니다. <br />작성 시 입력한 비밀번호를 입력하세요.";

}
?>
</div>
<div class="upw"><input type="password" id="upw" name="upw" value="" tabindex="1" /><span>비밀번호</span></div>
<div class="msg" id="msg">
<?
if ($m == '') {

    echo "비회원이 작성한 비밀글입니다.<br />작성 시 입력한 비밀번호를 입력하세요.";

}

else if ($m == 'u' || $m == 'd') {

    echo "비회원이 작성한 게시물입니다. <br />작성 시 입력한 비밀번호를 입력하세요.";

}
?>
</div>
<div class="submit"><div class="on"><span tabindex="1" class="btn_confirm">확인</span></div><div><span tabindex="3" class="btn_cancel">뒤로</span></div></div>
</div>
</form>
<?
include_once("$web[path]/_bottom.php");
?>