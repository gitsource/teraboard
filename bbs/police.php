<?php
include_once("./_tb.php");
if ($mode) { $mode = preg_match("/^[0-9]+$/", $mode) ? $mode : ""; }

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "c");

}

if ($mode == '1') {

    if (!$article['id']) {

        message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "c");

    }

    $chk = sql_fetch(" select * from $web[bbs_police_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    if ($chk['id']) {

        message("<p class='title'>알림</p><p class='text'>이미 신고가 접수되었습니다.</p>", "c");

    }

    $title = text($article['ar_title']);
    $nick = text($article['nick']);
    $url = http_bbs($bbs_id, $article_id);

}

else if ($mode == '2') {

    $reply = reply($bbs_id, $reply_id);

    if (!$reply['id']) {

        message("<p class='title'>알림</p><p class='text'>댓글이 삭제되었거나 존재하지 않습니다.</p>", "c");

    }

    $chk = sql_fetch(" select * from $web[bbs_police_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply_id = '".$reply_id."' ");

    if ($chk['id']) {

        message("<p class='title'>알림</p><p class='text'>이미 신고가 접수되었습니다.</p>", "c");

    }

    $title = text($reply['content'], 20);
    $nick = text($reply['nick']);
    $url = http_bbs($bbs_id, $article_id)."#reply".$reply_id;

} else {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

$web['title'] = "신고하기";
?>
<!DOCTYPE html>
<html>
<head>
<?
include_once("$web[path]/_head.php");
?>
<style type="text/css">
html {overflow:hidden;}
</style>
<script type="text/javascript">window.onload=function() {  setTimeout( function() { reasonHelp('1', 'open'); }, 100 ); }</script>
<script type="text/javascript">
function reasonHelp(id, mode)
{

    if (mode == 'open') {

        $("#reason_title_"+id+"_1").hide();
        $("#reason_title_"+id+"_2").show();
        $("#reason_view_"+id+"").show();

    } else {

        $("#reason_title_"+id+"_1").show();
        $("#reason_title_"+id+"_2").hide();
        $("#reason_view_"+id+"").hide();

    }

    resizeWindow("550", "");

}

function submitSetup()
{

    var f = document.formSetup;

    f.action = "<?=$web['host_bbs']?>/police_update.php";
    f.submit();

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div id="pop_wrap" class="layout-popup">
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="mode" value="<?=$mode?>" />
<input type="hidden" name="bbs_id" value="<?=$bbs_id?>" />
<input type="hidden" name="article_id" value="<?=$article_id?>" />
<input type="hidden" name="reply_id" value="<?=$reply_id?>" />
<div class="title">
<img src="<?=$web['host_img']?>/popup_arrow.png"><?=text($web['title']);?><div class="close"><a href="#" onclick="window.close(); return false;" title="닫기"><img src="<?=$web['host_img']?>/popup_close.png"></a></div>
</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="list-array">
<colgroup>
    <col width="5">
    <col width="75">
    <col width="10">
    <col width="">
</colgroup>
<tr height="13">
    <td bgcolor="#fafafa"></td>
    <td bgcolor="#fafafa"></td>
    <td></td>
    <td></td>
</tr>
<tr height="26">
    <td bgcolor="#fafafa"></td>
    <td bgcolor="#fafafa"><span class="subj">제목 :</span></td>
    <td></td>
    <td><span class="text"><?=$title?></span></td>
</tr>
<tr height="26">
    <td bgcolor="#fafafa"></td>
    <td bgcolor="#fafafa"><span class="subj">게시자 :</span></td>
    <td></td>
    <td><span class="text"><?=$nick?></span></td>
</tr>
<tr height="26">
    <td bgcolor="#fafafa"></td>
    <td bgcolor="#fafafa"><span class="subj">URL :</span></td>
    <td></td>
    <td><span class="text4"><?=$url?></span></td>
</tr>
<tr height="8">
    <td bgcolor="#fafafa"></td>
    <td bgcolor="#fafafa"></td>
    <td></td>
    <td></td>
</tr>
<tr><td height="1" colspan="4" bgcolor="#eaeaea"></td></tr>
<tr height="20">
    <td bgcolor="#fafafa"></td>
    <td bgcolor="#fafafa"></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td bgcolor="#fafafa"></td>
    <td bgcolor="#fafafa" valign="top"><span class="subj">신고사유 :</span></td>
    <td></td>
    <td valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><span class="text2" style="color:#c46b6b;">아래의 목록에서 대표적인 신고사유 1개를 선택해 주세요.</span></td>
</tr>
</table>

<!-- reason 1 start //-->
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:8px;">
<tr height="23">
    <td width="18"><input type="radio" name="reason" value="1" checked /></td>
    <td><span class="text bold" style="color:#666666;"><?=bbs_police_reason(1)?></span></td>
    <td width="5"></td>
    <td><span class="text2" style="color:#666666;">(예 : 온라인 도박, 불법복제품 거래)</span></td>
    <td width="5"></td>
    <td><div id="reason_title_1_1" style="display:inline;"><a href="#" onclick="reasonHelp('1', 'open'); return false;"><span class="text2" style="color:#999999;">상세안내</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div><div id="reason_title_1_2" style="display:none;"><a href="#" onclick="reasonHelp('1', 'close'); return false;"><span class="text2" style="color:#999999;">닫기</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div></td>
</tr>
</table>

<div id="reason_view_1" style="display:none;">
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 바카라, 릴게임 등 불법 사행성, 도박 사이트를 홍보하는 경우</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 모조품, 최음제 등 불법 제품을 판매, 홍보하거나 관련 정보를 제공하는 경우</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 범법행위에 대한 동기 부여 및 실행에 도움이 되는 정보를 제공하는 경우 등</span></td>
</tr>
</table>
</div>
<!-- reason 1 end //-->

<!-- reason 2 start //-->
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="23">
    <td width="18"><input type="radio" name="reason" value="2" /></td>
    <td><span class="text bold" style="color:#666666;"><?=bbs_police_reason(2)?></span></td>
    <td width="5"></td>
    <td><span class="text2" style="color:#666666;">(예 : 사진, 동영상, 글 등의 음란물)</span></td>
    <td width="5"></td>
    <td><div id="reason_title_2_1" style="display:inline;"><a href="#" onclick="reasonHelp('2', 'open'); return false;"><span class="text2" style="color:#999999;">상세안내</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div><div id="reason_title_2_2" style="display:none;"><a href="#" onclick="reasonHelp('2', 'close'); return false;"><span class="text2" style="color:#999999;">닫기</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div></td>
</tr>
</table>

<div id="reason_view_2" style="display:none;">
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 불법 음란물을 게재한 경우</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 음란한 행위를 묘사하거나 이미지 또는 동영상으로 관련 내용 공유</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 성매매 관련 장소를 안내 또는 매개하거나 관련 정보를 공유하는 경우</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 타인에게 성적 수치심이나 불쾌감을 유발할 수 있는 내용을 포함한 경우 등</span></td>
</tr>
</table>
</div>
<!-- reason 2 end //-->

<!-- reason 3 start //-->
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="23">
    <td width="18"><input type="radio" name="reason" value="3" /></td>
    <td><span class="text bold" style="color:#666666;"><?=bbs_police_reason(3)?></span></td>
    <td width="5"></td>
    <td><span class="text2" style="color:#666666;">(예 : 타인의 저작권 프로그램, 이미지, 게시물)</span></td>
    <td width="5"></td>
    <td><div id="reason_title_3_1" style="display:inline;"><a href="#" onclick="reasonHelp('3', 'open'); return false;"><span class="text2" style="color:#999999;">상세안내</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div><div id="reason_title_3_2" style="display:none;"><a href="#" onclick="reasonHelp('3', 'close'); return false;"><span class="text2" style="color:#999999;">닫기</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div></td>
</tr>
</table>

<div id="reason_view_3" style="display:none;">
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 타인의 저작물울 저작자의 허가 없이 배포하거나 수정, 사용하는 경우</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 프리서버, CD키 등 타인의 저작물을 불법적으로 공유하거나 방법을 제공</span></td>
</tr>
</table>
</div>
<!-- reason 3 end //-->

<!-- reason 4 start //-->
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="23">
    <td width="18"><input type="radio" name="reason" value="4" /></td>
    <td><span class="text bold" style="color:#666666;"><?=bbs_police_reason(4)?></span></td>
    <td width="5"></td>
    <td><span class="text2" style="color:#666666;">(예 : 욕설이나 불쾌감, 모욕감을 유발하는 게시물)</span></td>
    <td width="5"></td>
    <td><div id="reason_title_4_1" style="display:inline;"><a href="#" onclick="reasonHelp('4', 'open'); return false;"><span class="text2" style="color:#999999;">상세안내</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div><div id="reason_title_4_2" style="display:none;"><a href="#" onclick="reasonHelp('4', 'close'); return false;"><span class="text2" style="color:#999999;">닫기</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div></td>
</tr>
</table>

<div id="reason_view_4" style="display:none;">
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 욕설이나 공포심이나 불안감, 불쾌감과 모욕감을 유발하는 경우</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 타인의 신체, 외모, 취향 등에 대해 비하하고 모욕하는 내용</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 특정 계층, 지역, 국민, 종교를 근거 없이 비하하거나 비방</span></td>
</tr>
</table>
</div>
<!-- reason 4 end //-->

<!-- reason 5 start //-->
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="23">
    <td width="18"><input type="radio" name="reason" value="5" /></td>
    <td><span class="text bold" style="color:#666666;"><?=bbs_police_reason(5)?></span></td>
    <td width="5"></td>
    <td><span class="text2" style="color:#666666;">(예 : 도배, 로봇을 통한 부적합한 게시물)</span></td>
    <td width="5"></td>
    <td><div id="reason_title_5_1" style="display:inline;"><a href="#" onclick="reasonHelp('5', 'open'); return false;"><span class="text2" style="color:#999999;">상세안내</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div><div id="reason_title_5_2" style="display:none;"><a href="#" onclick="reasonHelp('5', 'close'); return false;"><span class="text2" style="color:#999999;">닫기</span> <img src="<?=$web['host_img']?>/police_dot.gif" border="0" align="absmiddle"></a></div></td>
</tr>
</table>

<div id="reason_view_5" style="display:none;">
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 단어 반복 삽입 등 검색 노출을 꾀하여 부적합한 방식으로 작성한 게시물</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 덧글도배기 등 DDOS,넷봇 프로그램을 유포하는 경우</span></td>
</tr>
<tr height="16">
    <td width="18"></td>
    <td><span class="text2" style="color:#999999;">- 서비스의 운영 목적과 취지에 맞지 않는 내용인 경우 등</span></td>
</tr>
</table>
</div>
<!-- reason 5 end //-->
    </td>
</tr>
<tr height="10">
    <td bgcolor="#fafafa"></td>
    <td bgcolor="#fafafa"></td>
    <td></td>
    <td></td>
</tr>
<tr><td height="1" colspan="4" bgcolor="#eaeaea"></td></tr>
</table>
<div style="padding:30px 0;">
<table border="0" cellspacing="0" cellpadding="0" class="auto btn">
<tr>
    <td><a href="#" onclick="submitSetup(); return false;" class="btn">확인</a></td>
    <td><a href="#" onclick="window.close(); return false;">닫기</a></td>
</tr>
</table>
</div>
</div>
</form>
</body>
</html>