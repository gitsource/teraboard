<?php
include_once("./_tb.php");
if ($mode) { $mode = preg_match("/^[0-9]+$/", $mode) ? $mode : ""; }
if ($reason) { $reason = preg_match("/^[0-9]+$/", $reason) ? $reason : ""; }

if ($check_login) {

    // 폼 체크
    if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

} else {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "c");

}

if ($mode == '1') {

    if (!$article['id']) {

        message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "c");

    }

    $chk = sql_fetch(" select * from $web[bbs_police_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    if ($chk['id']) {

        message("<p class='title'>알림</p><p class='text'>이미 신고가 접수되었습니다.</p>", "c");

    }

    $name = trim(sql_real_escape_string(stripslashes($article['nick'])));
    $title = trim(sql_real_escape_string(stripslashes($article['ar_title'])));
    $user_id = $article['mid'];

}

else if ($mode == '2') {

    $reply = reply($bbs_id, $reply_id);

    if (!$reply['id']) {

        message("<p class='title'>알림</p><p class='text'>댓글이 삭제되었거나 존재하지 않습니다.</p>", "c");

    }

    $chk = sql_fetch(" select * from $web[bbs_police_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply_id = '".$reply_id."' ");

    if ($chk['id']) {

        message("<p class='title'>알림</p><p class='text'>이미 신고가 접수되었습니다.</p>", "c");

    }

    $name = trim(sql_real_escape_string(stripslashes($reply['nick'])));
    $title = trim(sql_real_escape_string(stripslashes(text3($reply['content'], 50))));
    $user_id = $reply['mid'];

} else {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}


$sql_common = "";
$sql_common .= " set bbs_id = '".$bbs_id."' ";
$sql_common .= ", article_id = '".$article_id."' ";
$sql_common .= ", reply_id = '".$reply_id."' ";
$sql_common .= ", mid = '".$member['mid']."' ";
$sql_common .= ", user_id = '".$user_id."' ";
$sql_common .= ", name = '".$name."' ";
$sql_common .= ", title = '".$title."' ";
$sql_common .= ", reason = '".$reason."' ";
$sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

sql_query(" insert into $web[bbs_police_table] $sql_common ");

message("<p class='title'>알림</p><p class='text'>신고하였습니다.</p>", "c");
?>