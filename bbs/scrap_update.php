<?php
include_once("./_tb.php");
if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }
if ($added_list) { $added_list = preg_match("/^[0-9\,]+$/", $added_list) ? $added_list : ""; }

if ($check_login) {

    // 폼 체크
    if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

        if ($m == '') {

            message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "", "", false, true);

        } else {

            message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

        }

    }

} else {

    if ($m == '') {

        message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "", "", false, true);

    } else {

        message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

    }

}

if ($m == '') {

    if (!$article['id']) {

        message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", "", false, true);

    }

    $data = sql_fetch(" select * from $web[bbs_scrap_table] where mid = '".$member['mid']."' and bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    if ($data['id']) {

        message("<p class='title'>알림</p><p class='text'>이미 스크랩한 게시물 입니다.</p>", "", "", false, true);

    }

    $sql_common = "";
    $sql_common .= " set mid = '".$member['mid']."', ";
    $sql_common .= " bbs_id = '".$bbs_id."', ";
    $sql_common .= " article_id = '".$article_id."', ";
    $sql_common .= " ar_title = '".trim(strip_tags(sql_real_escape_string(stripslashes($article['ar_title']))))."', ";
    $sql_common .= " ar_datetime = '".$article['datetime']."', ";
    $sql_common .= " position = '99999999', ";
    $sql_common .= " datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[bbs_scrap_table] $sql_common ");

    message("<p class='title'>알림</p><p class='text'>스크랩하였습니다.</p>", "", "", false, true);

}

else if ($m == 'u') {

    if ($added_list) {

        // 받은 값을 쪼개고
        $str = explode(",", trim($added_list));
        for ($i=0; $i<count($str); $i++) {

            // 아이디 지정
            $k = $str[$i];

            // 값이 있다면
            if ($k) {

                // 업데이트
                sql_query(" update $web[bbs_scrap_table] set position = '".$i."' where mid = '".$member['mid']."' and id = '".$k."' ");

            }

        }

    }

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "opener.location.reload();";
    echo "</script>";

    url($web['host_member']."/bbs/scrap_position.php");

}

else if ($m == 'd') {

    if (!$id) {

        message("<p class='title'>알림</p><p class='text'>이미 삭제 되었거나 존재하지 않는 스크랩입니다.</p>", "b");

    }

    $data = sql_fetch(" select * from $web[bbs_scrap_table] where id = '".$id."' ");

    if (!$data['id']) {

        message("<p class='title'>알림</p><p class='text'>이미 삭제 되었거나 존재하지 않는 스크랩입니다.</p>", "b");

    }

    if ($member['mid'] != $data['mid']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    sql_query(" delete from $web[bbs_scrap_table] where id = '".$id."' ");

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "opener.location.reload();";
    echo "</script>";

    url($web['host_member']."/bbs/scrap.php?mid=".$mid."&p=".$p."");

} else {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}
?>