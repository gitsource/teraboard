<?php // 게시판
include_once("./_tb.php");

if ($m == 'blockkeyword') {

    $title = trim(sql_real_escape_string($_POST['title']));
    $content = trim(sql_real_escape_string($_POST['content']));

    if ($title) {

        $chk = blockkeyword($title);

        if ($chk) {

            message("<p class='title'>알림</p><p class='text'>금지 단어가 포함되어있습니다. (".text($chk).")</p>", "", "", false, true);

        }

    }

    if ($content) {

        $chk = blockkeyword($content);

        if ($chk) {

            message("<p class='title'>알림</p><p class='text'>금지 단어가 포함되어있습니다. (".text($chk).")</p>", "", "", false, true);

        }

    }

    echo "<script type='text/javascript'>";
    echo "submitWriteUpdate();";
    echo "</script>";

    exit;

}

$check_notice = false;
if ($ar_notice) { $ar_notice = preg_match("/^[0-9]+$/", $ar_notice) ? $ar_notice : ""; }
if ($ar_secret) { $ar_secret = preg_match("/^[0-9]+$/", $ar_secret) ? $ar_secret : ""; }
if ($ar_adult) { $ar_adult = preg_match("/^[0-9]+$/", $ar_adult) ? $ar_adult : ""; }
if ($_POST['ar_content'] == '<br>' || $_POST['ar_content'] == '<br />' || $_POST['ar_content'] == '<BR>' || $_POST['ar_content'] == '<BR />') { $_POST['ar_content'] = ""; }

// 폼 체크
if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

if (!$bbs['bbs_id']) {

    message("<p class='title'>알림</p><p class='text'>존재하지 않는 게시판입니다.</p>", "b");

}

if (!$bbs['bbs_onoff'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>접근할 수 없습니다.</p>", "b");

}

// 성인인증
if ($bbs['bbs_adult']) {

    if ($check_login) {

        if (!$member['certify_name'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>본인확인이 필요합니다.</p>", "", $web['host_member']."/", true, true);

        }

        if (!$member['certify_adult'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>성인이 아닙니다.</p>", "", $web['host_member']."/", true, true);

        }

    } else {

        message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

    }

    $ar_adult = 1;

}

// 새글
if ($m == '') {

    if ($member['level'] < $bbs['bbs_level_write']) {

        if ($check_login) {

            if ($member['level'] < $bbs['bbs_level_write']) {

                message("<p class='title'>알림</p><p class='text'>글을 작성할 권한이 없습니다.</p>", "b");

            }

        } else {

            message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

        }

    }

    if (!$ar_title) {

        message("<p class='title'>알림</p><p class='text'>제목이 입력되지 않았습니다.</p>", "b");

    }

    if (!$ar_content) {

        message("<p class='title'>알림</p><p class='text'>내용이 입력되지 않았습니다.</p>", "b");

    }

    if ($ar_secret && !$bbs['bbs_secret']) {

        message("<p class='title'>알림</p><p class='text'>비밀글을 사용할 수 없는 게시판입니다.</p>", "b");

    }

    if ($ar_notice && !$check_bbsadmin && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    if ($setup['bbs_write_time'] && !$check_bbsadmin && !$check_admin) {

        $chk = sql_fetch(" select id from {$web['article_table']}{$bbs_id} where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and datetime > '".date("Y-m-d H:i:s", $web['server_time'] - $setup['bbs_write_time'])."'  limit 0, 1 ");

        if ($chk['id']) {

            message("<p class='title'>알림</p><p class='text'>게시물 작성 제한으로 ".$setup['bbs_write_time']."초 후에 다시 시도하여주시기 바랍니다.</p>", "b");

        }

    }

    $ip = trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])));
    $ar_notice = trim(strip_tags(sql_real_escape_string($ar_notice)));
    $ar_secret = trim(strip_tags(sql_real_escape_string($ar_secret)));
    $ar_adult = trim(strip_tags(sql_real_escape_string($ar_adult)));
    $ar_category = trim(strip_tags(sql_real_escape_string($_POST['ar_category'])));
    $ar_title = trim(sql_real_escape_string($_POST['ar_title']));
    $ar_content = trim(sql_real_escape_string($_POST['ar_content']));
    $ar_keyword = trim(strip_tags(sql_real_escape_string($_POST['ar_keyword'])));
    $ar_source = trim(strip_tags(sql_real_escape_string($_POST['ar_source'])));

    if ($check_login) {

        $mid = trim(strip_tags(sql_real_escape_string($member['mid'])));
        $nick = trim(strip_tags(sql_real_escape_string($member['nick'])));
        $upw = "";

    } else {

        $mid = "";
        $nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));
        $upw = sql_password(trim(strip_tags(sql_real_escape_string($_POST['upw']))));

    }

    @include_once("$bbs_skin_path/write_update.top.php");

    $ar_img = 0;
    if (image_editor($ar_content)) { $ar_img = 1; }

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", upw = '".$upw."' ";
    $sql_common .= ", ip = '".$ip."' ";
    $sql_common .= ", ar_category = '".$ar_category."' ";
    $sql_common .= ", ar_title = '".$ar_title."' ";
    $sql_common .= ", ar_content = '".$ar_content."' ";
    $sql_common .= ", ar_keyword = '".$ar_keyword."' ";
    $sql_common .= ", ar_source = '".$ar_source."' ";
    $sql_common .= ", ar_secret = '".$ar_secret."' ";
    $sql_common .= ", ar_adult = '".$ar_adult."' ";
    $sql_common .= ", ar_img = '".$ar_img."' ";
    $sql_common .= ", ar_jump = '".$web['time_ymdhis']."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into {$web['article_table']}{$bbs_id} $sql_common ");

    $article_id = sql_insert_id();

    sql_query(" update {$web['article_table']}{$bbs_id} set ar_id = '".$article_id."' where id = '".$article_id."' ");

    unset($article);

    sql_query(" update $web[bbs_table] set bbs_write_count = bbs_write_count + 1 where bbs_id = '".$bbs_id."' ");

    if ($bbs['bbs_point_write']) {

        member_point($mid, $bbs['bbs_point_write'], 3, "[{$bbs['bbs_title']} - {$ar_title}] 게시물 등록", "article|".$bbs_id."|".$article_id."|regist");

    }

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", bbs_group = '".addslashes($bbs['bbs_group'])."' ";
    $sql_common .= ", bbs_id = '".$bbs_id."' ";
    $sql_common .= ", bbs_search = '".addslashes($bbs['bbs_search'])."' ";
    $sql_common .= ", article_id = '".$article_id."' ";
    $sql_common .= ", ar_img = '".$ar_img."' ";
    $sql_common .= ", ar_adult = '".$ar_adult."' ";
    $sql_common .= ", q = '".trim(strip_tags(sql_real_escape_string(search_text(2, $ar_title.$ar_keyword.$ar_content))))."' ";
    $sql_common .= ", ar_jump = '".$web['time_ymdhis']."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[search_table] $sql_common ");

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", bbs_id = '".$bbs_id."' ";
    $sql_common .= ", bbs_title = '".trim(strip_tags(sql_real_escape_string($bbs['bbs_title'])))."' ";
    $sql_common .= ", article_id = '".$article_id."' ";
    $sql_common .= ", ar_title = '".$ar_title."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";
    $sql_common .= ", updatetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[notice_table] $sql_common ");

    $sql_common = "";
    $sql_common .= " set bbs_write_count = bbs_write_count + 1 ";

    sql_query(" update $web[member_table] $sql_common where mid = '".$mid."' ");

    member_level_auto($mid);

    if ($check_bbsadmin || $check_admin) {

        $bbs_notice = "";
        if ($ar_notice) { $bbs_notice .= $article_id; }
        $row = explode(",", $bbs['bbs_notice']);
        for ($i=0; $i<count($row); $i++) {

            if ($row[$i] && $row[$i] != $article_id) {

                if ($bbs_notice) {
                    $bbs_notice .= ",";
                }

                $bbs_notice .= $row[$i];

            }

        }

        sql_query(" update $web[bbs_table] set bbs_notice = '".$bbs_notice."' where bbs_id = '".$bbs_id."' ");

    }

    $check_file = false;
    $check_img = false;

    if ($bbs['bbs_file_count']) {

        for ($i=1; $i<=$bbs['bbs_file_count']; $i++) {

            $number = $i;
            include("write_update.file.php");

        }

    }

    if ($check_file) {

        $sql_common = "";
        $sql_common .= " set ar_file = '1' ";

        sql_query(" update {$web['article_table']}{$bbs_id} $sql_common where id = '".$article_id."' ");

    }

    if (!$ar_img && $check_img) {

        $sql_common = "";
        $sql_common .= " set ar_img = '1' ";

        sql_query(" update {$web['article_table']}{$bbs_id} $sql_common where id = '".$article_id."' ");

        $sql_common = "";
        $sql_common .= "set ar_img = '1' ";

        sql_query(" update $web[search_table] $sql_common where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    }

    if (!$check_login) {

        $ss_name = "article_write_".$bbs_id."_".$article_id;
        if (!get_session($ss_name)) {

            set_session($ss_name, true);

        }

    }

    @include_once("$bbs_skin_path/write_update.bottom.php");

    if ($setup_api['syndi_onoff'] && $setup_api['syndi_token'] && $bbs['bbs_level_list'] <= 1 && $bbs['bbs_level_view'] <= 1 && !$ar_secret) {

        syndi_create("syndi.xml", syndi_article($bbs_id, $article_id, "regist"));

    }

    url(http_bbs($bbs_id, $article_id));

}

// 답글
else if ($m == 'a') {

    if (!$article['id']) {

        message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    $row = explode(",", $bbs['bbs_notice']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] == $article_id) {

            $check_notice = true;

            break;

        }

    }

    if ($member['level'] < $bbs['bbs_level_answer']) {

        if ($check_login) {

            if ($member['level'] < $bbs['bbs_level_answer']) {

                message("<p class='title'>알림</p><p class='text'>답변할 권한이 없습니다.</p>", "b");

            }

        } else {

            message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

        }

    }

    if ($article['ar_id'] != $article_id) {

        message("<p class='title'>알림</p><p class='text'>원글에만 답변이 가능합니다.</p>", "", http_bbs($bbs_id, ""));

    }

    if ($check_notice) {

        message("<p class='title'>알림</p><p class='text'>공지사항에는 답변할 수 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    if ($article['ar_secret'] && !$check_bbsadmin && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>비밀글에는 답변할 수 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    if ($setup['bbs_write_time'] && !$check_bbsadmin && !$check_admin) {

        $chk = sql_fetch(" select id from {$web['article_table']}{$bbs_id} where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and datetime > '".date("Y-m-d H:i:s", $web['server_time'] - $setup['bbs_write_time'])."'  limit 0, 1 ");

        if ($chk['id']) {

            message("<p class='title'>알림</p><p class='text'>게시물 작성 제한으로 ".$setup['bbs_write_time']."초 후에 다시 시도하여주시기 바랍니다.</p>", "b");

        }

    }

    $ip = trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])));
    $ar_notice = trim(strip_tags(sql_real_escape_string($ar_notice)));
    $ar_secret = trim(strip_tags(sql_real_escape_string($ar_secret)));
    $ar_adult = trim(strip_tags(sql_real_escape_string($ar_adult)));
    $ar_category = trim(strip_tags(sql_real_escape_string($_POST['ar_category'])));
    $ar_title = trim(sql_real_escape_string($_POST['ar_title']));
    $ar_content = trim(sql_real_escape_string($_POST['ar_content']));
    $ar_keyword = trim(strip_tags(sql_real_escape_string($_POST['ar_keyword'])));
    $ar_source = trim(strip_tags(sql_real_escape_string($_POST['ar_source'])));

    if ($check_login) {

        $mid = trim(strip_tags(sql_real_escape_string($member['mid'])));
        $nick = trim(strip_tags(sql_real_escape_string($member['nick'])));
        $upw = "";

    } else {

        $mid = "";
        $nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));
        $upw = sql_password(trim(strip_tags(sql_real_escape_string($_POST['upw']))));

    }

    @include_once("$bbs_skin_path/write_update.top.php");

    $ar_img = 0;
    if (image_editor($ar_content)) { $ar_img = 1; }

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", upw = '".$upw."' ";
    $sql_common .= ", ip = '".$ip."' ";
    $sql_common .= ", ar_category = '".$ar_category."' ";
    $sql_common .= ", ar_title = '".$ar_title."' ";
    $sql_common .= ", ar_content = '".$ar_content."' ";
    $sql_common .= ", ar_keyword = '".$ar_keyword."' ";
    $sql_common .= ", ar_source = '".$ar_source."' ";
    $sql_common .= ", ar_secret = '".$ar_secret."' ";
    $sql_common .= ", ar_adult = '".$ar_adult."' ";
    $sql_common .= ", ar_img = '".$ar_img."' ";
    $sql_common .= ", ar_jump = '".$web['time_ymdhis']."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into {$web['article_table']}{$bbs_id} $sql_common ");

    $article_id = sql_insert_id();

    sql_query(" update {$web['article_table']}{$bbs_id} set ar_id = '".$article['id']."' where id = '".$article_id."' ");

    unset($article);

    sql_query(" update $web[bbs_table] set bbs_write_count = bbs_write_count + 1 where bbs_id = '".$bbs_id."' ");

    if ($bbs['bbs_point_write']) {

        member_point($mid, $bbs['bbs_point_write'], 3, "[{$bbs['bbs_title']} - {$ar_title}] 답글 등록", "article|".$bbs_id."|".$article_id."|regist");

    }

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", bbs_group = '".addslashes($bbs['bbs_group'])."' ";
    $sql_common .= ", bbs_id = '".$bbs_id."' ";
    $sql_common .= ", bbs_search = '".addslashes($bbs['bbs_search'])."' ";
    $sql_common .= ", article_id = '".$article_id."' ";
    $sql_common .= ", ar_img = '".$ar_img."' ";
    $sql_common .= ", ar_adult = '".$ar_adult."' ";
    $sql_common .= ", q = '".trim(strip_tags(sql_real_escape_string(search_text(2, $ar_title.$ar_keyword.$ar_content))))."' ";
    $sql_common .= ", ar_jump = '".$web['time_ymdhis']."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[search_table] $sql_common ");

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", bbs_id = '".$bbs_id."' ";
    $sql_common .= ", bbs_title = '".trim(strip_tags(sql_real_escape_string($bbs['bbs_title'])))."' ";
    $sql_common .= ", article_id = '".$article_id."' ";
    $sql_common .= ", ar_title = '".$ar_title."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";
    $sql_common .= ", updatetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[notice_table] $sql_common ");

    $sql_common = "";
    $sql_common .= " set bbs_write_count = bbs_write_count + 1 ";

    sql_query(" update $web[member_table] $sql_common where mid = '".$mid."' ");

    member_level_auto($mid);

    if ($check_bbsadmin || $check_admin) {

        $bbs_notice = "";
        if ($ar_notice) { $bbs_notice .= $article_id; }
        $row = explode(",", $bbs['bbs_notice']);
        for ($i=0; $i<count($row); $i++) {

            if ($row[$i] && $row[$i] != $article_id) {

                if ($bbs_notice) {
                    $bbs_notice .= ",";
                }

                $bbs_notice .= $row[$i];

            }

        }

        sql_query(" update $web[bbs_table] set bbs_notice = '".$bbs_notice."' where bbs_id = '".$bbs_id."' ");

    }

    $check_file = false;
    $check_img = false;

    if ($bbs['bbs_file_count']) {

        for ($i=1; $i<=$bbs['bbs_file_count']; $i++) {

            $number = $i;
            include("write_update.file.php");

        }

    }

    if ($check_file) {

        $sql_common = "";
        $sql_common .= " set ar_file = '1' ";

        sql_query(" update {$web['article_table']}{$bbs_id} $sql_common where id = '".$article_id."' ");

    }

    if (!$ar_img && $check_img) {

        $sql_common = "";
        $sql_common .= " set ar_img = '1' ";

        sql_query(" update {$web['article_table']}{$bbs_id} $sql_common where id = '".$article_id."' ");

        $sql_common = "";
        $sql_common .= "set ar_img = '1' ";

        sql_query(" update $web[search_table] $sql_common where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    }

    if (!$check_login) {

        $ss_name = "article_write_".$bbs_id."_".$article_id;
        if (!get_session($ss_name)) {

            set_session($ss_name, true);

        }

    }

    @include_once("$bbs_skin_path/write_update.bottom.php");

    if ($setup_api['syndi_onoff'] && $setup_api['syndi_token'] && $bbs['bbs_level_list'] <= 1 && $bbs['bbs_level_view'] <= 1 && !$ar_secret) {

        syndi_create("syndi.xml", syndi_article($bbs_id, $article_id, "regist"));

    }

    url(http_bbs($bbs_id, $article_id));

}

// 수정
else if ($m == 'u') {

    if (!$article['id']) {

        message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    $row = explode(",", $bbs['bbs_notice']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] == $article_id) {

            $check_notice = true;

            break;

        }

    }

    // 비회원
    $check_article_session = false;
    if (!$article['mid'] && !$check_article_session && !$check_bbsadmin && !$check_admin) {

        $ss_name = "article_write_".$bbs_id."_".$article_id;
        if (get_session($ss_name)) {

            $check_article_session = true;

        }

    }

    if ($check_bbsadmin || $check_admin) {

        // pass

    }

    else if ($article['mid'] && $member['mid'] == $article['mid'] || !$article['mid'] && $check_article_session) {

        // pass

    } else {

        message("<p class='title'>알림</p><p class='text'>수정할 권한이 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    if (!$ar_title) {

        message("<p class='title'>알림</p><p class='text'>제목이 입력되지 않았습니다.</p>", "b");

    }

    if (!$ar_content) {

        message("<p class='title'>알림</p><p class='text'>내용이 입력되지 않았습니다.</p>", "b");

    }

    if ($ar_secret && !$bbs['bbs_secret']) {

        message("<p class='title'>알림</p><p class='text'>비밀글을 사용할 수 없는 게시판입니다.</p>", "b");

    }

    if (!$article['ar_notice'] && $ar_notice && !$check_bbsadmin && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    $ip = trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])));
    $ar_notice = trim(strip_tags(sql_real_escape_string($ar_notice)));
    $ar_secret = trim(strip_tags(sql_real_escape_string($ar_secret)));
    $ar_adult = trim(strip_tags(sql_real_escape_string($ar_adult)));
    $ar_category = trim(strip_tags(sql_real_escape_string($_POST['ar_category'])));
    $ar_title = trim(sql_real_escape_string($_POST['ar_title']));
    $ar_content = trim(sql_real_escape_string($_POST['ar_content']));
    $ar_keyword = trim(strip_tags(sql_real_escape_string($_POST['ar_keyword'])));
    $ar_source = trim(strip_tags(sql_real_escape_string($_POST['ar_source'])));

    if ($check_login) {

        if ($check_bbsadmin || $check_admin) {

            if ($member['mid'] == $article['mid']) {

                $nick = trim(strip_tags(sql_real_escape_string($member['nick'])));
                $upw = "";

            }

            else if ($article['mid']) {

                $nick = trim(strip_tags(sql_real_escape_string($article['nick'])));
                $upw = "";

            } else {

                //$nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));
                //$upw = sql_password(trim(strip_tags(sql_real_escape_string($_POST['upw']))));
                $nick = trim(strip_tags(sql_real_escape_string($article['nick'])));
                $upw = trim(strip_tags(sql_real_escape_string($article['upw'])));

            }

        } else {

            $nick = trim(strip_tags(sql_real_escape_string($member['nick'])));
            $upw = "";

        }


    } else {

        $nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));
        $upw = sql_password(trim(strip_tags(sql_real_escape_string($_POST['upw']))));

    }

    if ($article['ar_adult'] && !$check_bbsadmin && !$check_admin) {

        $ar_adult = 1;

    }

    @include_once("$bbs_skin_path/write_update.top.php");

    $ar_img = 0;
    if (image_editor($ar_content)) { $ar_img = 1; }

    $sql_common = "";
    $sql_common .= " set nick = '".$nick."' ";
    $sql_common .= ", upw = '".$upw."' ";
    $sql_common .= ", ip = '".$ip."' ";
    $sql_common .= ", ar_category = '".$ar_category."' ";
    $sql_common .= ", ar_title = '".$ar_title."' ";
    $sql_common .= ", ar_content = '".$ar_content."' ";
    $sql_common .= ", ar_keyword = '".$ar_keyword."' ";
    $sql_common .= ", ar_source = '".$ar_source."' ";
    $sql_common .= ", ar_secret = '".$ar_secret."' ";
    $sql_common .= ", ar_adult = '".$ar_adult."' ";
    $sql_common .= ", ar_img = '".$ar_img."' ";

    sql_query(" update {$web['article_table']}{$bbs_id} $sql_common where id = '".$article_id."' ");

    $chk = sql_fetch(" select * from $web[search_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' limit 0, 1 ");

    if ($chk['id']) {

        $sql_common = "";
        $sql_common .= "set ar_img = '".$ar_img."' ";
        $sql_common .= ", ar_adult = '".$ar_adult."' ";
        $sql_common .= ", q = '".trim(strip_tags(sql_real_escape_string(search_text(2, $ar_title.$ar_keyword.$ar_content))))."' ";

        sql_query(" update $web[search_table] $sql_common where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    } else {

        $sql_common = "";
        $sql_common .= " set mid = '".$article['mid']."' ";
        $sql_common .= ", nick = '".$nick."' ";
        $sql_common .= ", bbs_group = '".addslashes($bbs['bbs_group'])."' ";
        $sql_common .= ", bbs_id = '".$bbs_id."' ";
        $sql_common .= ", bbs_search = '".addslashes($bbs['bbs_search'])."' ";
        $sql_common .= ", article_id = '".$article_id."' ";
        $sql_common .= ", ar_img = '".$ar_img."' ";
        $sql_common .= ", ar_hit = '".$article['ar_hit']."' ";
        $sql_common .= ", ar_good = '".$article['ar_good']."' ";
        $sql_common .= ", ar_nogood = '".$article['ar_nogood']."' ";
        $sql_common .= ", ar_reply = '".$article['ar_reply']."' ";
        $sql_common .= ", ar_adult = '".$ar_adult."' ";
        $sql_common .= ", q = '".trim(strip_tags(sql_real_escape_string(search_text(2, $ar_title.$ar_keyword.$ar_content))))."' ";
        $sql_common .= ", ar_jump = '".$article['datetime']."' ";
        $sql_common .= ", datetime = '".$article['datetime']."' ";

        sql_query(" insert into $web[search_table] $sql_common ");

    }

    $sql_common = "";
    $sql_common .= "set ar_title = '".$ar_title."' ";
    $sql_common .= ", updatetime = '".$web['time_ymdhis']."' ";

    sql_query(" update $web[notice_table] $sql_common where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    if ($check_bbsadmin || $check_admin) {

        $bbs_notice = "";
        if ($ar_notice) { $bbs_notice .= $article_id; }
        $row = explode(",", $bbs['bbs_notice']);
        for ($i=0; $i<count($row); $i++) {

            if ($row[$i] && $row[$i] != $article_id) {

                if ($bbs_notice) {
                    $bbs_notice .= ",";
                }

                $bbs_notice .= $row[$i];

            }

        }

        sql_query(" update $web[bbs_table] set bbs_notice = '".$bbs_notice."' where bbs_id = '".$bbs_id."' ");

    }

    $check_file = false;
    $check_img = false;

    if ($bbs['bbs_file_count']) {

        $id = $article_id;
        for ($i=1; $i<=$bbs['bbs_file_count']; $i++) {

            $number = $i;
            include("write_update.file.php");

        }

    }

    if (!$check_file) {

        $chk = sql_fetch(" select id from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and upload_type not in (1,2,3) limit 0, 1 ");

        if ($chk['id']) {

            $check_file = 1;

        }

    }

    if (!$ar_img && !$check_img) {

        $chk = sql_fetch(" select id from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and upload_type in (1,2,3) limit 0, 1 ");

        if ($chk['id']) {

            $check_img = 1;

        }

    }

    if ($check_file) {

        $sql_common = "";
        $sql_common .= " set ar_file = '1' ";

        sql_query(" update {$web['article_table']}{$bbs_id} $sql_common where id = '".$article_id."' ");

    } else {

        $sql_common = "";
        $sql_common .= " set ar_file = '0' ";

        sql_query(" update {$web['article_table']}{$bbs_id} $sql_common where id = '".$article_id."' ");

    }

    if ($check_img) {

        $sql_common = "";
        $sql_common .= " set ar_img = '1' ";

        sql_query(" update {$web['article_table']}{$bbs_id} $sql_common where id = '".$article_id."' ");

        $sql_common = "";
        $sql_common .= "set ar_img = '1' ";

        sql_query(" update $web[search_table] $sql_common where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    }

    @include_once("$bbs_skin_path/write_update.bottom.php");

    url(http_bbs($bbs_id, $article_id));

}

// 삭제
else if ($m == 'd') {

    if (!$article['id']) {

        message("<p class='title'>알림</p><p class='text'>게시물이 삭제되었거나 존재하지 않습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    // 비회원
    $check_article_session = false;
    if (!$article['mid'] && !$check_article_session && !$check_login) {

        $ss_name = "article_write_".$bbs_id."_".$article_id;
        if (get_session($ss_name)) {

            $check_article_session = true;

        } else {

            url($web['host_bbs']."/password.php?m=d&bbs_id={$bbs_id}&article_id={$article_id}");

        }

    }

    if ($check_bbsadmin || $check_admin) {

        // pass

    }

    else if ($article['mid'] && $member['mid'] == $article['mid'] || !$article['mid'] && $check_article_session) {

        // pass

    } else {

        message("<p class='title'>알림</p><p class='text'>삭제할 권한이 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    @include_once("$bbs_skin_path/write_update.top.php");

    if ($bbs['bbs_point_write']) {

        member_point($article['mid'], $bbs['bbs_point_write'] * -1, 3, "[{$bbs['bbs_title']} - {$article['ar_title']}] 게시물 삭제", "article|".$bbs_id."|".$article_id."|delete");

    }

    $result = sql_query(" select * from {$web['reply_table']}{$bbs_id} where article_id = '".$article_id."' ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        if ($row['mid']) {

            member_point($row['mid'], $bbs['bbs_point_reply'] * -1, 3, "[{$bbs['bbs_title']} - {$article['ar_title']}] 댓글 삭제", "article|".$bbs_id."|".$article_id."|".$row['id']."|delete");

            sql_query(" update $web[member_table] set bbs_reply_count = bbs_reply_count - 1 where mid = '".$row['mid']."' ");

        }

    }

    sql_query(" delete from {$web['article_table']}{$bbs_id} where id = '".$article_id."' ");
    sql_query(" delete from {$web['reply_table']}{$bbs_id} where article_id = '".$article_id."' ");

    sql_query(" update $web[bbs_table] set bbs_write_count = bbs_write_count - 1, bbs_reply_count = bbs_reply_count - ".(int)($article['ar_reply'])." where bbs_id = '".$bbs_id."' ");
    sql_query(" delete from $web[bbs_good_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
    sql_query(" delete from $web[search_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
    sql_query(" delete from $web[notice_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    $sql_common = "";
    $sql_common .= " set bbs_write_count = bbs_write_count - 1 ";

    sql_query(" update $web[member_table] $sql_common where mid = '".$article['mid']."' ");

    $bbs_notice = "";
    $row = explode(",", $bbs['bbs_notice']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] != $article_id) {

            if ($bbs_notice) {
                $bbs_notice .= ",";
            }

            $bbs_notice .= $row[$i];

        }

    }

    sql_query(" update $web[bbs_table] set bbs_notice = '".$bbs_notice."' where bbs_id = '".$bbs_id."' ");

    $result = sql_query(" select * from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $file_path = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $row['upload_time'])."/".$row['upload_file'];

        if (file_exists($file_path) && $row['upload_file']) {

            @unlink($file_path);

        }

    }

    sql_query(" delete from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

    @include_once("$bbs_skin_path/write_update.bottom.php");

    if ($setup_api['syndi_onoff'] && $setup_api['syndi_token'] && $bbs['bbs_level_list'] <= 1 && $bbs['bbs_level_view'] <= 1 && !$ar_secret) {

        syndi_create("syndi.xml", syndi_article($bbs_id, $article_id, "delete"));

    }

    url(http_bbs($bbs_id, ""));

}

// 삭제
else if ($m == 'check_del') {

    if (!$check_bbsadmin && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>삭제할 권한이 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    @include_once("$bbs_skin_path/write_update.top.php");

    $kk = 0;
    $listarticle = array();
    for ($i=0; $i<count($chk_id); $i++) {

        $k = $chk_id[$i];
        $article_id = addslashes($_POST['list_id'][$k]);

        $article = article($bbs_id, $article_id);

        if ($article['id']) {

            $listarticle[$kk] = $article;
            $kk++;

            if ($bbs['bbs_point_write']) {

                member_point($article['mid'], $bbs['bbs_point_write'] * -1, 3, "[{$bbs['bbs_title']} - {$article['ar_title']}] 게시물 삭제", "article|".$bbs_id."|".$article_id."|delete");

            }

            $result = sql_query(" select * from {$web['reply_table']}{$bbs_id} where article_id = '".$article_id."' ");
            for ($n=0; $row=sql_fetch_array($result); $n++) {

                if ($row['mid']) {

                    member_point($row['mid'], $bbs['bbs_point_reply'] * -1, 3, "[{$bbs['bbs_title']} - {$article['ar_title']}] 댓글 삭제", "article|".$bbs_id."|".$article_id."|".$row['id']."|delete");

                    sql_query(" update $web[member_table] set bbs_reply_count = bbs_reply_count - 1 where mid = '".$row['mid']."' ");

                }

            }

            sql_query(" delete from {$web['article_table']}{$bbs_id} where id = '".$article_id."' ");
            sql_query(" delete from {$web['reply_table']}{$bbs_id} where article_id = '".$article_id."' ");

            sql_query(" update $web[bbs_table] set bbs_write_count = bbs_write_count - 1, bbs_reply_count = bbs_reply_count - ".(int)($article['ar_reply'])." where bbs_id = '".$bbs_id."' ");
            sql_query(" delete from $web[bbs_good_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
            sql_query(" delete from $web[search_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
            sql_query(" delete from $web[notice_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

            $sql_common = "";
            $sql_common .= " set bbs_write_count = bbs_write_count - 1 ";

            sql_query(" update $web[member_table] $sql_common where mid = '".$article['mid']."' ");

            $bbs_notice = "";
            $row = explode(",", $bbs['bbs_notice']);
            for ($n=0; $n<count($row); $n++) {

                if ($row[$n] && $row[$n] != $article_id) {

                    if ($bbs_notice) {
                        $bbs_notice .= ",";
                    }

                    $bbs_notice .= $row[$n];

                }

            }

            sql_query(" update $web[bbs_table] set bbs_notice = '".$bbs_notice."' where bbs_id = '".$bbs_id."' ");

            $result = sql_query(" select * from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
            for ($n=0; $row=sql_fetch_array($result); $n++) {

                $file_path = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $row['upload_time'])."/".$row['upload_file'];

                if (file_exists($file_path) && $row['upload_file']) {

                    @unlink($file_path);

                }

            }

            sql_query(" delete from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

        }

    }

    @include_once("$bbs_skin_path/write_update.bottom.php");

    if ($url) {

        $urlencode = urldecode($url);

    } else {

        $urlencode = urldecode($_SERVER[REQUEST_URI]);

    }

    message("<p class='title'>알림</p><p class='text'>삭제 하였습니다.</p>", "", $urlencode, true, true);

}

// 복제, 이동
else if ($m == 'check_copy' || $m == 'check_move') {

    if ($m == 'check_copy') {
        $msg = "복제";
    } else {
        $msg = "이동";
    }

    if ($bbs_id2) { $bbs_id2 = preg_match("/^[a-zA-Z0-9_\-]+$/", $bbs_id2) ? $bbs_id2 : ""; }

    if (!$check_bbsadmin && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>".$msg."할 권한이 없습니다.</p>", "", http_bbs($bbs_id, ""));

    }

    if ($bbs_id == $bbs_id2) {

        message("<p class='title'>알림</p><p class='text'>동일한 게시판으로 ".$msg."할 수 없습니다.</p>", "b");

    }

    if (!$bbs_id2) {

        message("<p class='title'>알림</p><p class='text'>".$msg."할 게시판이 없습니다.</p>", "b");

    }

    $bbs2 = bbs($bbs_id2);

    if (!$bbs2['bbs_id']) {

        message("<p class='title'>알림</p><p class='text'>".$msg."할 게시판이 없습니다.</p>", "b");

    }

    @include_once("$bbs_skin_path/write_update.top.php");

    $kk = 0;
    $listarticle = array();
    for ($i=0; $i<count($chk_id); $i++) {

        $k = $chk_id[$i];
        $article_id = addslashes($_POST['list_id'][$k]);

        $article = article($bbs_id, $article_id);

        if ($article['id']) {

            // 게시물 등록
            $sql_common = "";
            $sql_common .= " set mid = '".addslashes($article['mid'])."' ";
            $sql_common .= ", nick = '".addslashes($article['nick'])."' ";
            $sql_common .= ", upw = '".addslashes($article['upw'])."' ";
            $sql_common .= ", ip = '".addslashes($article['ip'])."' ";
            $sql_common .= ", ar_category = '".addslashes($article['ar_category'])."' ";
            $sql_common .= ", ar_title = '".addslashes($article['ar_title'])."' ";
            $sql_common .= ", ar_content = '".addslashes($article['ar_content'])."' ";
            $sql_common .= ", ar_keyword = '".addslashes($article['ar_keyword'])."' ";
            $sql_common .= ", ar_source = '".addslashes($article['ar_source'])."' ";
            $sql_common .= ", ar_img = '".addslashes($article['ar_img'])."' ";
            $sql_common .= ", ar_file = '".addslashes($article['ar_file'])."' ";
            $sql_common .= ", ar_secret = '".addslashes($article['ar_secret'])."' ";
            $sql_common .= ", ar_hit = '".addslashes($article['ar_hit'])."' ";
            $sql_common .= ", ar_reply = '".addslashes($article['ar_reply'])."' ";
            $sql_common .= ", ar_good = '".addslashes($article['ar_good'])."' ";
            $sql_common .= ", ar_nogood = '".addslashes($article['ar_nogood'])."' ";
            $sql_common .= ", ar_adult = '".addslashes($article['ar_adult'])."' ";
            $sql_common .= ", ar_blind = '".addslashes($article['ar_blind'])."' ";
            $sql_common .= ", ar_jump = '".addslashes($article['ar_jump'])."' ";
            $sql_common .= ", datetime = '".addslashes($article['datetime'])."' ";

            sql_query(" insert into {$web['article_table']}{$bbs_id2} $sql_common ");

            $article_id2 = sql_insert_id();

            $listarticle[$kk] = $article;
            $listarticle[$kk]['new_bbs_id'] = $bbs_id2;
            $listarticle[$kk]['new_article_id'] = $article_id2;
            $kk++;

            sql_query(" update {$web['article_table']}{$bbs_id2} set ar_id = '".$article_id2."' where id = '".$article_id2."' ");

            // 댓글 등록
            $reply = array();
            $replynew = array();
            $result = sql_query(" select * from {$web['reply_table']}{$bbs_id} where article_id = '".$article_id."' order by id asc ");
            for ($n=0; $row=sql_fetch_array($result); $n++) {

                $sql_common = "";
                $sql_common .= " set mid = '".addslashes($row['mid'])."' ";
                $sql_common .= ", nick = '".addslashes($row['nick'])."' ";
                $sql_common .= ", upw = '".addslashes($row['upw'])."' ";
                $sql_common .= ", ip = '".addslashes($row['ip'])."' ";
                $sql_common .= ", article_id = '".addslashes($article_id2)."' ";
                $sql_common .= ", content = '".addslashes($row['content'])."' ";
                $sql_common .= ", good = '".addslashes($row['good'])."' ";
                $sql_common .= ", nogood = '".addslashes($row['nogood'])."' ";
                $sql_common .= ", blind = '".addslashes($row['blind'])."' ";
                $sql_common .= ", del = '".addslashes($row['del'])."' ";
                $sql_common .= ", datetime = '".addslashes($row['datetime'])."' ";

                sql_query(" insert into {$web['reply_table']}{$bbs_id2} $sql_common ");

                $reply_id = sql_insert_id();

                $reply[$n] = $row;
                $reply[$n]['id2'] = $reply_id;

                if ($row['id'] == $row['reply_id']) {

                    $replynew_id = $row['reply_id'];
                    $replynew[$replynew_id] = $reply_id;

                }

            }

            for ($n=0; $n<count($reply); $n++) {

                $replynew_id = $reply[$n]['reply_id'];

                if ($reply[$n]['id'] != $reply[$n]['reply_id']) {

                    $reply_id = $replynew[$replynew_id];

                } else {

                    $reply_id = $reply[$n]['id2'];

                }

                sql_query(" update {$web['reply_table']}{$bbs_id2} set reply_id = '".$reply_id."' where id = '".$reply[$n]['id2']."' ");

                // 추천
                $result = sql_query(" select * from $web[bbs_good_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply_id != 0 ");
                for ($n2=0; $row=sql_fetch_array($result); $n2++) {

                    $sql_common = "";
                    $sql_common .= " set mid = '".addslashes($row['mid'])."' ";
                    $sql_common .= ", good = '".addslashes($row['good'])."' ";
                    $sql_common .= ", bbs_id = '".addslashes($bbs_id2)."' ";
                    $sql_common .= ", article_id = '".addslashes($article_id2)."' ";
                    $sql_common .= ", reply_id = '".addslashes($reply[$n]['id'])."' ";
                    $sql_common .= ", datetime = '".addslashes($row['datetime'])."' ";

                    sql_query(" insert into $web[bbs_good_table] $sql_common ");

                }

                // 알림
                $row = sql_fetch(" select * from $web[notice_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply_id = '".$reply[$n]['id']."' ");

                if ($row['id']) {

                    $sql_common = "";
                    $sql_common .= " set mid = '".addslashes($row['mid'])."' ";
                    $sql_common .= ", nick = '".addslashes($row['nick'])."' ";
                    $sql_common .= ", bbs_id = '".addslashes($bbs_id2)."' ";
                    $sql_common .= ", bbs_title = '".addslashes($bbs2['bbs_title'])."' ";
                    $sql_common .= ", article_id = '".addslashes($article_id2)."' ";
                    $sql_common .= ", ar_title = '".addslashes($row['ar_title'])."' ";
                    $sql_common .= ", ar_reply = '".addslashes($row['ar_reply'])."' ";
                    $sql_common .= ", content = '".addslashes($row['content'])."' ";
                    $sql_common .= ", reply = '".addslashes($row['reply'])."' ";
                    $sql_common .= ", reply_id = '".addslashes($reply[$n]['id2'])."' ";
                    $sql_common .= ", reply_first = '".addslashes($reply_id)."' ";
                    $sql_common .= ", reply_new = '".addslashes($reply_id)."' ";
                    $sql_common .= ", datetime = '".addslashes($row['datetime'])."' ";
                    $sql_common .= ", updatetime = '".addslashes($row['updatetime'])."' ";
                    $sql_common .= ", readtime = '".addslashes($row['readtime'])."' ";

                    sql_query(" insert into $web[notice_table] $sql_common ");

                }

            }

            // 파일
            $result = sql_query(" select * from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
            for ($n=0; $row=sql_fetch_array($result); $n++) {

                $file_path = $disk['path']."/bbs/".$bbs_id."/".data_path("u", $row['upload_time'])."/".$row['upload_file'];

                if (file_exists($file_path) && $row['upload_file']) {

                    $dir = $disk['path']."/bbs/".$bbs_id2;

                    @mkdir($dir, 0707);
                    @chmod($dir, 0707);

                    $dir = $dir."/".data_path("u", $row['upload_time']);

                    @mkdir($dir, 0707);
                    @chmod($dir, 0707);

                    $file_path2 = $dir."/".$row['upload_file'];

                    @copy($file_path, $file_path2);
                    @chmod($file_path2, 0606);

                    $sql_common = "";
                    $sql_common .= " set bbs_id = '".addslashes($bbs_id2)."' ";
                    $sql_common .= ", article_id = '".addslashes($article_id2)."' ";
                    $sql_common .= ", number = '".addslashes($row['number'])."' ";
                    $sql_common .= ", upload_source = '".addslashes($row['upload_source'])."' ";
                    $sql_common .= ", upload_file = '".addslashes($row['upload_file'])."' ";
                    $sql_common .= ", upload_filesize = '".addslashes($row['upload_filesize'])."' ";
                    $sql_common .= ", upload_width = '".addslashes($row['upload_width'])."' ";
                    $sql_common .= ", upload_height = '".addslashes($row['upload_height'])."' ";
                    $sql_common .= ", upload_type = '".addslashes($row['upload_type'])."' ";
                    $sql_common .= ", upload_time = '".addslashes($row['upload_time'])."' ";

                    sql_query(" insert into $web[bbs_file_table] $sql_common ");

                    if ($m == 'check_move') {

                        @unlink($file_path);

                    }

                }

            }

            // 추천
            $result = sql_query(" select * from $web[bbs_good_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply_id = 0 ");
            for ($n=0; $row=sql_fetch_array($result); $n++) {

                $sql_common = "";
                $sql_common .= " set mid = '".addslashes($row['mid'])."' ";
                $sql_common .= ", good = '".addslashes($row['good'])."' ";
                $sql_common .= ", bbs_id = '".addslashes($bbs_id2)."' ";
                $sql_common .= ", article_id = '".addslashes($article_id2)."' ";
                $sql_common .= ", datetime = '".addslashes($row['datetime'])."' ";

                sql_query(" insert into $web[bbs_good_table] $sql_common ");

            }

            // 검색
            $row = sql_fetch(" select * from $web[search_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."'");

            if ($row['id']) {

                $sql_common = "";
                $sql_common .= " set mid = '".addslashes($row['mid'])."' ";
                $sql_common .= ", nick = '".addslashes($row['nick'])."' ";
                $sql_common .= ", bbs_group = '".addslashes($bbs2['bbs_group'])."' ";
                $sql_common .= ", bbs_id = '".addslashes($bbs_id2)."' ";
                $sql_common .= ", bbs_search = '".addslashes($bbs2['bbs_search'])."' ";
                $sql_common .= ", article_id = '".addslashes($article_id2)."' ";
                $sql_common .= ", ar_img = '".addslashes($row['ar_img'])."' ";
                $sql_common .= ", ar_hit = '".addslashes($row['ar_hit'])."' ";
                $sql_common .= ", ar_good = '".addslashes($row['ar_good'])."' ";
                $sql_common .= ", ar_nogood = '".addslashes($row['ar_nogood'])."' ";
                $sql_common .= ", ar_reply = '".addslashes($row['ar_reply'])."' ";
                $sql_common .= ", ar_adult = '".addslashes($row['ar_adult'])."' ";
                $sql_common .= ", ar_blind = '".addslashes($row['ar_blind'])."' ";
                $sql_common .= ", q = '".addslashes($row['q'])."' ";
                $sql_common .= ", ar_jump = '".addslashes($row['ar_jump'])."' ";
                $sql_common .= ", datetime = '".addslashes($row['datetime'])."' ";

                sql_query(" insert into $web[search_table] $sql_common ");

            }

            // 알림
            $row = sql_fetch(" select * from $web[notice_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and reply = 0 ");

            if ($row['id']) {

                $sql_common = "";
                $sql_common .= " set mid = '".addslashes($row['mid'])."' ";
                $sql_common .= ", nick = '".addslashes($row['nick'])."' ";
                $sql_common .= ", bbs_id = '".addslashes($bbs_id2)."' ";
                $sql_common .= ", bbs_title = '".addslashes($bbs2['bbs_title'])."' ";
                $sql_common .= ", article_id = '".addslashes($article_id2)."' ";
                $sql_common .= ", ar_title = '".addslashes($row['ar_title'])."' ";
                $sql_common .= ", datetime = '".addslashes($row['datetime'])."' ";
                $sql_common .= ", updatetime = '".addslashes($row['updatetime'])."' ";

                sql_query(" insert into $web[notice_table] $sql_common ");

            }

            if ($m == 'check_move') {

                sql_query(" delete from {$web['article_table']}{$bbs_id} where id = '".$article_id."' ");
                sql_query(" delete from {$web['reply_table']}{$bbs_id} where article_id = '".$article_id."' ");
                sql_query(" delete from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

                sql_query(" delete from $web[bbs_good_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
                sql_query(" delete from $web[search_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");
                sql_query(" delete from $web[notice_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' ");

                // 공지해제
                $bbs_notice = "";
                $row = explode(",", $bbs['bbs_notice']);
                for ($n=0; $n<count($row); $n++) {

                    if ($row[$n] && $row[$n] != $article_id) {

                        if ($bbs_notice) {
                            $bbs_notice .= ",";
                        }

                        $bbs_notice .= $row[$n];

                    }

                }

                sql_query(" update $web[bbs_table] set bbs_notice = '".$bbs_notice."' where bbs_id = '".$bbs_id."' ");

            }

        }

    }

    // 카운트 조정
    $cnt1 = sql_fetch(" select count(id) as cnt from {$web['article_table']}{$bbs_id} ");
    $cnt2 = sql_fetch(" select count(id) as cnt from {$web['reply_table']}{$bbs_id} ");
    $cnt3 = sql_fetch(" select count(id) as cnt from {$web['article_table']}{$bbs_id2} ");
    $cnt4 = sql_fetch(" select count(id) as cnt from {$web['reply_table']}{$bbs_id2} ");

    $sql_common = "";
    $sql_common .= " set bbs_write_count = '".$cnt1['cnt']."' ";
    $sql_common .= ", bbs_reply_count = '".$cnt2['cnt']."' ";

    sql_query(" update $web[bbs_table] $sql_common where bbs_id = '".$bbs_id."' ");

    $sql_common = "";
    $sql_common .= " set bbs_write_count = '".$cnt3['cnt']."' ";
    $sql_common .= ", bbs_reply_count = '".$cnt4['cnt']."' ";

    sql_query(" update $web[bbs_table] $sql_common where bbs_id = '".$bbs_id2."' ");

    @include_once("$bbs_skin_path/write_update.bottom.php");

    if ($url) {

        $urlencode = urldecode($url);

    } else {

        $urlencode = urldecode($_SERVER[REQUEST_URI]);

    }

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "opener.location.reload();";
    echo "</script>";

    message("<p class='title'>알림</p><p class='text'>".$msg." 하였습니다.</p>", "c");

} else {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "", http_bbs($bbs_id, ""));

}
?>