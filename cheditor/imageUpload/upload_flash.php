<?php
include_once("./_tb.php");

$dir = $disk['path']."/editor/".data_path("", "");
$path = $disk['server']."/editor/".data_path("", "");

@mkdir($dir, 0707);
@chmod($dir, 0707);

$tempfile = $_FILES['file']['tmp_name'];
$filename = $_FILES['file']['name'];

$type = substr($filename, strrpos($filename, ".")+1);
$found = false;
switch ($type) {
case "jpg":
case "jpeg":
case "gif":
case "png":
$found = true;
}

if ($found != true) {
exit;
}

$savefile = $dir . '/' . $filename;

move_uploaded_file($tempfile, $savefile);
$imgsize = getimagesize($savefile);
$filesize = filesize($savefile);

if (!$imgsize) {
$filesize = 0;
$random_name = '-ERR';
unlink($savefile);
};


if ($imgsize && $setup['image_scaling']) {

    if (file_exists($savefile) && preg_match("/\.(jp[e]?g|png)$/i", $savefile) && $imgsize[0] > $setup['image_scaling']) {

        $savefile2 = $dir . '/thumb' . $filename;

        image_resize($setup['image_scaling'], $setup['image_scaling'], $savefile, $savefile2, 100);

        @unlink($savefile);

        $savefile = $savefile2;
        $filename = "thumb".$filename;

    }

}

$rdata = sprintf('{"fileUrl": "%s/%s", "filePath": "%s", "fileName": "%s", "fileSize": "%d" }',
$path,
$filename,
$savefile,
$filename,
$filesize );

echo $rdata;
?>