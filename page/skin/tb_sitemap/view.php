<?php // 스킨
if (!defined("_WEB_")) exit;

// 메뉴 자동완성(게시판+페이지)
$array = array();
$seq = array();
$result = sql_query(" select * from $web[bbs_group_table] where bbs_group_onoff = 1 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $map_id = $row['bbs_group'];

    $array[$map_id] = $row;
    $array[$map_id]['menu_id'] = $row['bbs_group'];
    $array[$map_id]['title'] = $row['bbs_group_title'];
    $seq[$map_id] = $row['bbs_group_position'].$row['bbs_group_title'];

}

$result = sql_query(" select * from $web[page_group_table] where page_group_onoff = 1 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $map_id = $row['page_group'];

    $array[$map_id] = $row;
    $array[$map_id]['menu_id'] = $row['page_group'];
    $array[$map_id]['title'] = $row['page_group_title'];
    $seq[$map_id] = $row['page_group_position'].$row['page_group_title'];

}

natsort($seq);

$i = 0;
$map = array();
foreach ($seq as $key => $val) {

    $map[$i] = $array[$key];

    $i++;

}

$mapsub = array();
$result = sql_query(" select * from $web[bbs_table] where bbs_onoff = 1 order by bbs_position desc, bbs_title asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $map_id = $row['bbs_group'];

    $row['menu_id'] = "B-".$row['bbs_id'];
    $row['href'] = http_bbs($row['bbs_id'], "");

    if ($row['bbs_start'] == 1) {

        $row['href'] = $web['host_rbbs']."/write.php?bbs_id=".$row['bbs_id'];

    }

    $row['title'] = $row['bbs_title'];
    $row['class'] = "bbs";
    $row['position'] = $row['bbs_position'].$row['bbs_title'];
    $row['target'] = "";

    $mapsub[$map_id][$i] = $row;

}

$result = sql_query(" select * from $web[page_table] where page_onoff = 1 order by page_position desc, page_title asc ");
for ($i=$i; $row=sql_fetch_array($result); $i++) {

    $map_id = $row['page_group'];

    $row['menu_id'] = "P-".$row['page_id'];
    $row['href'] = $web['host_page']."/".$row['page_id'];
    $row['title'] = $row['page_title'];
    $row['class'] = "page";
    $row['position'] = $row['page_position'].$row['page_title'];
    $row['target'] = "";

    if ($row['page_mode'] == 2) {

        $row['href'] = url_http($row['page_url']);

        if ($row['page_blank']) {

            $row['target'] = "target='_blank'";

        }

    }

    $mapsub[$map_id][$i] = $row;

}

$mod = 4;
$item = 0;
if (count($map)) {

    $item = ceil(count($map) / $mod);

}
?>
<style type="text/css">
.page-hidden {position:relative; z-index:-999; font-size:0; line-height:0; width:1px; height:0px;}

.page-map {font-size:0; line-height:0; padding-bottom:50px; margin-top:-101px;}
.page-map p {margin:0;}
.page-map .item {width:25%; vertical-align:top; display:inline-block; margin-top:50px; padding-top:50px; border-top:1px solid #dadada;}
.page-map .item .block {padding:0 25px;}
.page-map .item.item0 .block,
.page-map .item.item1 .block,
.page-map .item.item2 .block {background:url('<?=$page_skin_url?>/img/line.gif') repeat-y right 0;}
.page-map .title {font-weight:700; line-height:30px; font-size:24px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-map .titleline {margin-top:20px; width:40px; border-bottom:2px solid #1192ca;}

.page-map .list {margin-top:19px;}
.page-map .list li {display:block; background:url('<?=$page_skin_url?>/img/dot.png') repeat-x 0 top;}
.page-map .list li a {position:relative; display:block; padding:10px 0 10px 14px;}
.page-map .list li a {text-decoration:none; font-weight:700; line-height:24px; font-size:18px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-map .list li a .arrow {display:block; position:absolute; left:0; top:20px; width:4px; height:4px; background:url('<?=$page_skin_url?>/img/arrow.png') no-repeat;}
.page-map .list li:first-child {background-image:none;}
.page-map .list li.page:hover a {color:#87bb0c;}
.page-map .list li.page:hover a .arrow {background-position:-4px 0;}
.page-map .list li.bbs:hover a {color:#42abd7;}
.page-map .list li.bbs:hover a .arrow {background-position:-8px 0;}

@media screen and (max-width:1249px) {

}

@media screen and (max-width:1000px) {

.page-map .item {width:50%;}
.page-map .item.item0 .block,
.page-map .item.item2 .block {background:url('<?=$page_skin_url?>/img/line.gif') repeat-y right 0;}
.page-map .item.item1 .block,
.page-map .item.item3 .block {background-image:none;}

.page-map .item.item2.not,
.page-map .item.item3.not {display:none;}

}

@media screen and (max-width:640px) {

.page-hidden {display:none;}

.page-map {padding-bottom:30px; margin-top:-30px;}
.page-map .item {margin-top:25px; padding-top:30px;}
.page-map .item:first-child {margin-top:0px; border-top:0;}

.page-map .item {width:100%;}
.page-map .item .block {padding:0 10px;}
.page-map .item.item0 .block,
.page-map .item.item1 .block,
.page-map .item.item2 .block,
.page-map .item.item3 .block {background-image:none;}

.page-map .titleline {margin-top:10px;}
.page-map .title {font-weight:bold; line-height:30px; font-size:20px; color:#1192ca; font-family:gulim,serif;}
.page-map .list {margin-top:9px;}
.page-map .list li a {font-weight:bold; line-height:24px; font-size:15px; color:#555555; font-family:gulim,serif;}
.page-map .item.not {display:none;}

}
</style>
<script type="text/javascript">
var item = <?=$item?>;
$(document).ready( function() {

    var itemResize = function() {

        if ($('.page-hidden').is(':hidden') == true) {

            $('.page-map .item .block').css({ 'min-height' : 'auto' });

        } else {

            for (i=1; i<=item; i++) {

                var itemh = 0;
                var itemg = 0;

                $('.page-map .item[name="'+[i]+'"] .block').each(function() {

                    itemg = $(this).parent().attr('name');

                    if (itemh < $(this).height()) {

                        itemh = $(this).height();

                    }

                });

                $('.page-map .item[name="'+[i]+'"] .block').css({ 'min-height' : (itemh)+'px' });

            }

        }

    };

    itemResize();

    var resizeTime;

    $(window).resize(function() {

        clearTimeout(resizeTime);

        resizeTime = setTimeout(function() {

            itemResize();

        }, 10);

    });

});
</script>
<div class="page-map">
<?
$k = 0;
$g = 0;
for ($i=0; $i<count($map); $i++) {

    $map_id = $map[$i]['menu_id'];

    if ($mapsub[$map_id]) {

        if ($k%$mod == 0) {

            $g++;

        }

        echo "<div name='".$g."' class='item item".($k%$mod)."'>";
        echo "<div class='block'>";
        echo "<p class='title'>".text($map[$i]['title'])."</p>";
        echo "<p class='titleline'></p>";
        echo "<ul class='list'>";

        $n = 0;
        $array = array();
        $seq = array();
        foreach ($mapsub[$map_id] as $row) {

            $array[$n] = $row;
            $seq[$n] = $row['position'];

            $n++;

        }

        natsort($seq);

        foreach ($seq as $key => $val) {

            $row = $array[$key];

            echo "<li class='".text($row['class'])."'>";
            echo "<a href='".$row['href']."' ".$row['target']." title='".text($row['title'])."'><span class='arrow'></span>";
            echo text($row['title']);
            echo "</a>";
            echo "</li>";

        }

        echo "</ul>";
        echo "</div>";
        echo "</div>\n";

        $k++;

    }

}

$k = count($map);
$cnt = $k%$mod;
if ($cnt) {

    for ($k=$cnt; $k<$mod; $k++) {

        echo "<div name='".$g."' class='item item".($k%$mod)." not'>";
        echo "<div class='block'></div>";
        echo "</div>\n";

    }

}
?>
</div>
<div class="page-hidden"></div>