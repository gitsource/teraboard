<?php // 스킨
if (!defined("_WEB_")) exit;
?>
<style type="text/css">
.layout-page {padding-bottom:40px;}

.page-map {width:100%; height:500px; border-top:2px solid #363e4a; border-bottom:2px solid #363e4a;}

.page-text {font-size:0; line-height:0; margin-top:30px; padding:20px 0 20px 0; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text p {margin:0;}
.page-text .item {width:50%; display:inline-block; vertical-align:top;}
.page-text .item .line {border-left:1px solid #dadada;}
.page-text .item:first-child .line {border-left:0; padding:30px 30px 30px 0;}
.page-text .item:last-child .line {padding:30px 0 30px 30px;}
.page-text .title {font-weight:700; line-height:24px; font-size:24px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text .titleline {margin:15px 0 25px 0; width:30px; border-bottom:2px solid #555555;}
.page-text .text {font-weight:400; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}
.page-text .info {margin-top:25px;}
.page-text .info p {margin-top:10px; padding:10px 20px 10px 20px; background-color:#363e4a;}
.page-text .info p {font-weight:700; line-height:18px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text .info p .arrow {margin-top:7px; margin-right:10px; vertical-align:top; display:inline-block; width:4px; height:4px; background-color:#ffffff;}
.page-text .subj {margin:0 0 12px 0;}
.page-text .subj {font-weight:700; line-height:18px; font-size:18px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text .subj .arrow {margin-top:7px; margin-right:10px; vertical-align:top; display:inline-block; width:4px; height:4px; background-color:#555555;}
.page-text .busnum {padding:5px 0 0 10px; margin:-4px 0 0 -4px;}
.page-text .busnum span {margin:4px 0 0 4px; display:inline-block; vertical-align:top; border-radius:2px; padding:3px 8px 2px 9px;}
.page-text .busnum span {font-weight:bold; line-height:15px; font-size:13px; color:#ffffff; font-family:gulim,serif;}
.page-text .busnum .bus1 {background-color:#34a2f7;}
.page-text .busnum .bus2 {background-color:#4dc74d;}
.page-text .busnum .bus3 {background-color:#ecb646;}
.page-text .busnum .bus4 {background-color:#b16d79;}

@media screen and (max-width:1249px) {

.page-text .item:first-child .line {padding:30px 30px 30px 10px;}
.page-text .item:last-child .line {padding:30px 10px 30px 30px;}

}

@media screen and (max-width:1000px) {

.page-map {height:350px;}
.page-text .item {width:100%; display:block; vertical-align:top;}
.page-text .item .line {border-left:0;}
.page-text .item:last-child {border-top:1px solid #dadada;}
.page-text .item:first-child .line {padding:30px 10px 30px 10px;}
.page-text .item:last-child .line {padding:30px 10px 30px 10px;}

}

@media screen and (max-width:640px) {

.layout-page {padding-top:54px; padding-bottom:0px;}
.page-text {padding:0px 0 20px 0;}
.page-text .title {font-weight:bold; line-height:24px; font-size:18px; color:#555555; font-family:gulim,serif;}
.page-text .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}
.page-text .info p {font-weight:bold; line-height:18px; font-size:16px; color:#ffffff; font-family:gulim,serif;}
.page-text .subj {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}

}
</style>
<script type="text/javascript" src="http://apis.daum.net/maps/maps3.js?apikey=<?=text($setup_api['daum_apikey'])?>" charset="utf-8"></script>
<script type="text/javascript">
/*
1. 다음로컬을 이용하여, 경위도 좌표 구하는 방법(브라우저를 통해 접속)
https://apis.daum.net/local/geo/addr2coord?apikey={apikey}&q={주소입력}
*/
var map;
var mapx = "126.9786567859313"; // 경위도 좌표 입력
var mapy = "37.566826005485716";
$(document).ready( function() {

    map = new daum.maps.Map(document.getElementById('map'), {
    center: new daum.maps.LatLng(mapy, mapx),
    level: 3
    });

    var mapTypeControl = new daum.maps.MapTypeControl();
    map.addControl(mapTypeControl, daum.maps.ControlPosition.TOPRIGHT);

    var zoomControl = new daum.maps.ZoomControl();
    map.addControl(zoomControl, daum.maps.ControlPosition.RIGHT);

    var position =  new daum.maps.LatLng(mapy, mapx);
    var marker = new daum.maps.Marker({ position: position, clickable: true });

    marker.setMap(map);

    var itemResize = function() {

        map.setCenter(position);

    };

    itemResize();

    var resizeTime;

    $(window).resize(function() {

        clearTimeout(resizeTime);

        resizeTime = setTimeout(function() {

            itemResize();

        }, 10);

    });

});
</script>
<div class="page-map" id="map"></div>
<div class="page-text">
<div class="item">
<div class="line">
<p class="title"><?=text($setup['company'])?> 방문 안내</p>
<p class="titleline"></p>
<p class="text">다음 지도의 API 활용한 ‘오시는 길’ 안내 페이지입니다. 사용 시 관리자 모드의 SETUP > 연동 서비스 설정란의 다음 API 키를 입력하고, 정확한 위치 표기를 위해 소스코드에서 경도(X)와 위도(Y)를 삽입하시면 됩니다. 경위도의 추출방법은 소스코드 내에 설명되어 있습니다.</p>
<div class="info">
<p><span class="arrow"></span>방문 예약/문의 : <?=text($setup['tel'])?></p>
<p><span class="arrow"></span>주소 : <?=text($setup['addr'])?></p>
</div>
</div>
</div>
<div class="item">
<div class="line">
<p class="title">대중교통 이용 안내</p>
<p class="titleline"></p>
<p class="subj"><span class="arrow"></span>지하철 이용시</p>
<p class="text">- 지하철 1호선 [시청역] 4번 출구 이용 (도보 180M)</p>
<p class="text">- 지하철 2호선 [을지로 입구역] 1-1번 출구 이용 (도보 250M)</p>
<p class="subj" style="margin-top:25px;"><span class="arrow"></span>버스 이용시</p>
<p class="text">- 시청.서울신문사(02-706, 02-137) 정류소 이용 (도보 250M)</p>
<div class="busnum">
<span class="bus1">101</span>
<span class="bus1">105</span>
<span class="bus1">402</span>
<span class="bus1">405</span>
<span class="bus1">501</span>
<span class="bus1">506</span>
<span class="bus2">1711</span>
<span class="bus2">7016</span>
<span class="bus3">91S</span>
<span class="bus4">6005</span>
</div>
</div>
</div>
</div>
