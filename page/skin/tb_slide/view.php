<?php // 스킨
if (!defined("_WEB_")) exit;
?>
<style type="text/css">
.layout-wrap,
.layout-contents,
.layout-main {}
.layout-contents {max-width:100%;}

.layout-subtop-adm {max-width:1250px; margin-left:auto; margin-right:auto;}
.layout-subtop-title {max-width:1250px; margin-left:auto; margin-right:auto;}

.page-text1,
.page-text2,
.page-text3 .linetop,
.page-text3 .wrap,
.page-text4 .linetop,
.page-text4 .wrap {margin:0 auto; max-width:1250px;}

.page-text1 {position:relative; font-size:0; line-height:0; padding:0 0 45px 0; margin-top:5px; border-bottom:1px solid #dadada;}
.page-text1:after {display:block; clear:both; content:'';}
.page-text1 p {margin:0;}
.page-text1 .slide {position:relative; max-width:1250px; width:100%; margin:0 auto; font-size:0;}
.page-text1 .slide .block {display:none;}
.page-text1 .slide .block img {max-width:1250px; width:100%; border:0;}
.page-text1 .slide .num {position:absolute; left:0px; bottom:15px; width:100%;}
.page-text1 .slide .num ul {font-size:0; text-align:center;}
.page-text1 .slide .num ul li {margin-left:10px; width:15px; height:15px; vertical-align:top; display:inline-block; background-color:#ffffff; opacity:0.5; border-radius:25px; border:1px solid #dadada; cursor:pointer;}
.page-text1 .slide .num ul li.on {background-color:#ffffff; opacity:1;}
.page-text1 .slide .num ul li:first-child {margin-left:0;}
.page-text1 .wrap .title {margin-top:30px;}
.page-text1 .wrap .title {font-weight:400; line-height:18px; font-size:18px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.page-text1 .wrap .text {margin-top:15px;}
.page-text1 .wrap .text {font-weight:400; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text2 {position:relative; font-size:0; line-height:0; padding:50px 0 45px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text2:after {display:block; clear:both; content:'';}
.page-text2 p {margin:0;}
.page-text2 .slide {position:relative; max-width:1250px; width:100%; margin:0 auto; font-size:0;}
.page-text2 .slide .block {display:none;}
.page-text2 .slide .block img {max-width:1250px; width:100%; border:0;}
.page-text2 .slide .num {position:absolute; left:0px; bottom:15px; width:100%;}
.page-text2 .slide .num ul {font-size:0; text-align:center;}
.page-text2 .slide .num ul li {margin-left:10px; width:15px; height:15px; vertical-align:top; display:inline-block; background-color:#ffffff; opacity:0.5; border-radius:25px; border:1px solid #dadada; cursor:pointer;}
.page-text2 .slide .num ul li.on {background-color:#ffffff; opacity:1;}
.page-text2 .slide .num ul li:first-child {margin-left:0;}
.page-text2 .slide .box {position:absolute; left:50px; top:0; height:100%;}
.page-text2 .slide .table {display:table; vertical-align:middle; width:100%; height:100%;}
.page-text2 .slide .table-sell {display:table-cell; vertical-align:middle;}
.page-text2 .slide .title {font-weight:700; line-height:72px; font-size:72px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .slide .title2 {margin-top:15px;}
.page-text2 .slide .title2 {font-weight:700; line-height:60px; font-size:60px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .wrap .title {margin-top:30px;}
.page-text2 .wrap .title {font-weight:400; line-height:18px; font-size:18px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .wrap .text {margin-top:15px;}
.page-text2 .wrap .text {font-weight:400; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text3 {position:relative; font-size:0; line-height:0; margin-top:5px;}
.page-text3:after {display:block; clear:both; content:'';}
.page-text3 p {margin:0;}
.page-text3 .linetop {font-size:0; line-height:0; border-top:1px solid #dadada; padding-top:50px;}
.page-text3 .slide {position:relative; width:100%; font-size:0;}
.page-text3 .slide .block {display:none;}
.page-text3 .slide .block.bg1 {background:url('<?=$page_skin_url?>/img/3-1-bg.jpg') repeat center center;}
.page-text3 .slide .block.bg2 {background:url('<?=$page_skin_url?>/img/3-2-bg.jpg') repeat center center;}
.page-text3 .slide .block.bg3 {background:url('<?=$page_skin_url?>/img/3-3-bg.jpg') repeat center center;}
.page-text3 .slide .block .img {margin:0 auto; max-width:1000px; width:100%;}
.page-text3 .slide .block .img img {max-width:1000px; width:100%; border:0;}
.page-text3 .slide .num {position:absolute; left:0px; bottom:15px; width:100%;}
.page-text3 .slide .num ul {font-size:0; text-align:center;}
.page-text3 .slide .num ul li {margin-left:10px; width:15px; height:15px; vertical-align:top; display:inline-block; background-color:#ffffff; opacity:0.5; border-radius:25px; border:1px solid #dadada; cursor:pointer;}
.page-text3 .slide .num ul li.on {background-color:#ffffff; opacity:1;}
.page-text3 .slide .num ul li:first-child {margin-left:0;}
.page-text3 .slide .title {font-weight:700; line-height:72px; font-size:72px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .slide .title2 {margin-top:15px;}
.page-text3 .slide .title2 {font-weight:700; line-height:60px; font-size:60px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .wrap {border-bottom:1px solid #dadada; padding-bottom:45px;}
.page-text3 .wrap .title {margin-top:30px;}
.page-text3 .wrap .title {font-weight:400; line-height:18px; font-size:18px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .wrap .text {margin-top:15px;}
.page-text3 .wrap .text {font-weight:400; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text4 {position:relative; font-size:0; line-height:0; margin-top:5px;}
.page-text4:after {display:block; clear:both; content:'';}
.page-text4 p {margin:0;}
.page-text4 .linetop {font-size:0; line-height:0; border-top:1px solid #dadada; padding-top:50px;}
.page-text4 .slide {position:relative; width:100%; font-size:0;}
.page-text4 .slide .block {display:none;}
.page-text4 .slide .block.bg1 {background:url('<?=$page_skin_url?>/img/4-1-bg.jpg') repeat center center;}
.page-text4 .slide .block.bg2 {background:url('<?=$page_skin_url?>/img/4-2-bg.jpg') repeat center center;}
.page-text4 .slide .block.bg3 {background:url('<?=$page_skin_url?>/img/4-3-bg.jpg') repeat center center;}
.page-text4 .slide .block .img {margin:0 auto; max-width:1000px; width:100%;}
.page-text4 .slide .block .img img {max-width:1000px; width:100%; border:0;}
.page-text4 .slide .num {position:absolute; left:0px; bottom:15px; width:100%;}
.page-text4 .slide .num ul {font-size:0; text-align:center;}
.page-text4 .slide .num ul li {margin-left:10px; width:15px; height:15px; vertical-align:top; display:inline-block; background-color:#ffffff; opacity:0.5; border-radius:25px; border:1px solid #dadada; cursor:pointer;}
.page-text4 .slide .num ul li.on {background-color:#ffffff; opacity:1;}
.page-text4 .slide .num ul li:first-child {margin-left:0;}
.page-text4 .slide .box {position:absolute; left:0; right:0; top:0; margin:0 auto; max-width:1000px; width:100%; height:100%;}
.page-text4 .slide .table {display:table; vertical-align:middle; width:100%; height:100%;}
.page-text4 .slide .table-sell {display:table-cell; vertical-align:middle; padding-left:50px;}
.page-text4 .slide .title {font-weight:700; line-height:72px; font-size:72px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .slide .title2 {margin-top:15px;}
.page-text4 .slide .title2 {font-weight:700; line-height:60px; font-size:60px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .wrap {border-bottom:1px solid #dadada; padding-bottom:45px;}
.page-text4 .wrap .title {margin-top:30px;}
.page-text4 .wrap .title {font-weight:400; line-height:18px; font-size:18px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .wrap .text {margin-top:15px;}
.page-text4 .wrap .text {font-weight:400; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

@media screen and (max-width:1249px) {

.page-text1 {padding:0 10px 45px 10px;}
.page-text2 {padding:50px 10px 45px 10px;}
.page-text3 .wrap {padding:0 10px 45px 10px;}
.page-text4 .wrap {padding:0 10px 45px 10px;}
.page-text2 .slide .box {left:40px;}
.page-text4 .slide .table-sell {padding-left:40px;}

}

@media screen and (max-width:1000px) {

.page-text2 .slide .box {left:30px;}
.page-text4 .slide .table-sell {padding-left:30px;}
.page-text2 .slide .title {font-weight:700; line-height:36px; font-size:36px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .slide .title2 {margin-top:10px;}
.page-text2 .slide .title2 {font-weight:700; line-height:30px; font-size:30px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .slide .title {font-weight:700; line-height:36px; font-size:36px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .slide .title2 {margin-top:10px;}
.page-text3 .slide .title2 {font-weight:700; line-height:30px; font-size:30px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .slide .title {font-weight:700; line-height:36px; font-size:36px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .slide .title2 {margin-top:10px;}
.page-text4 .slide .title2 {font-weight:700; line-height:30px; font-size:30px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}

}

@media screen and (max-width:640px) {

.page-text1 {padding:0 10px 42px 10px;}
.page-text1 .slide .num {position:absolute; left:0px; bottom:5px; width:100%;}
.page-text1 .slide .num ul {font-size:0; text-align:center;}
.page-text1 .slide .num ul li {margin-left:4px; width:15px; height:5px; vertical-align:top; display:inline-block; background-color:#ffffff; border-radius:15px; opacity:0.5; border:1px solid #dadada; cursor:pointer;}
.page-text1 .slide .num ul li.on {background-color:#ffffff; opacity:1;}
.page-text1 .slide .num ul li:first-child {margin-left:0;}
.page-text1 .wrap .title {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text1 .wrap .text {margin-top:13px;}
.page-text1 .wrap .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text2 {padding:50px 10px 42px 10px;}
.page-text2 .slide .num {position:absolute; left:0px; bottom:5px; width:100%;}
.page-text2 .slide .num ul {font-size:0; text-align:center;}
.page-text2 .slide .num ul li {margin-left:4px; width:15px; height:5px; vertical-align:top; display:inline-block; background-color:#ffffff; border-radius:15px; opacity:0.5; border:1px solid #dadada; cursor:pointer;}
.page-text2 .slide .num ul li.on {background-color:#ffffff; opacity:1;}
.page-text2 .slide .num ul li:first-child {margin-left:0;}
.page-text2 .slide .box {left:15px;}
.page-text2 .slide .title {font-weight:bold; line-height:22px; font-size:20px; color:#ffffff; font-family:gulim,serif;}
.page-text2 .slide .title2 {margin-top:3px;}
.page-text2 .slide .title2 {font-weight:bold; line-height:18px; font-size:16px; color:#ffffff; font-family:gulim,serif;}
.page-text2 .wrap .title {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text2 .wrap .text {margin-top:13px;}
.page-text2 .wrap .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text3 .wrap {padding:0 10px 42px 10px;}
.page-text3 .slide .num {position:absolute; left:0px; bottom:5px; width:100%;}
.page-text3 .slide .num ul {font-size:0; text-align:center;}
.page-text3 .slide .num ul li {margin-left:4px; width:15px; height:5px; vertical-align:top; display:inline-block; background-color:#ffffff; border-radius:15px; opacity:0.5; border:1px solid #dadada; cursor:pointer;}
.page-text3 .slide .num ul li.on {background-color:#ffffff; opacity:1;}
.page-text3 .slide .num ul li:first-child {margin-left:0;}
.page-text3 .slide .title {font-weight:bold; line-height:22px; font-size:20px; color:#ffffff; font-family:gulim,serif;}
.page-text3 .slide .title2 {margin-top:3px;}
.page-text3 .slide .title2 {font-weight:bold; line-height:18px; font-size:16px; color:#ffffff; font-family:gulim,serif;}
.page-text3 .wrap .title {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text3 .wrap .text {margin-top:13px;}
.page-text3 .wrap .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text4 .wrap {padding:0 10px 42px 10px;}
.page-text4 .slide .num {position:absolute; left:0px; bottom:5px; width:100%;}
.page-text4 .slide .num ul {font-size:0; text-align:center;}
.page-text4 .slide .num ul li {margin-left:4px; width:15px; height:5px; vertical-align:top; display:inline-block; background-color:#ffffff; border-radius:15px; opacity:0.5; border:1px solid #dadada; cursor:pointer;}
.page-text4 .slide .num ul li.on {background-color:#ffffff; opacity:1;}
.page-text4 .slide .num ul li:first-child {margin-left:0;}
.page-text4 .slide .table-sell {padding-left:15px;}
.page-text4 .slide .title {font-weight:bold; line-height:22px; font-size:20px; color:#ffffff; font-family:gulim,serif;}
.page-text4 .slide .title2 {margin-top:3px;}
.page-text4 .slide .title2 {font-weight:bold; line-height:18px; font-size:16px; color:#ffffff; font-family:gulim,serif;}
.page-text4 .wrap .title {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text4 .wrap .text {margin-top:13px;}
.page-text4 .wrap .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

}
</style>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.tb-slide.js"></script>
<script type="text/javascript">
$(document).ready( function() {

    $('#slide1').tbSlide();
    $('#slide2').tbSlide();
    $('#slide3').tbSlide();
    $('#slide4').tbSlide();

});
</script>
<!-- page-text1 start //-->
<div class="page-text1">
<div class="slide" id="slide1">
<div class="block"><img src="<?=$page_skin_url?>/img/1-1.png" alt=""></div>
<div class="block"><img src="<?=$page_skin_url?>/img/1-2.png" alt=""></div>
<div class="block"><img src="<?=$page_skin_url?>/img/1-3.png" alt=""></div>
<div class="num">
<ul>
<li></li>
<li></li>
<li></li>
</ul>
</div>
</div>
<div class="wrap">
<p class="font-zoom title">이미지로만 구성된 슬라이드</p>
<p class="font-zoom text">텍스트와 이미지(사진)가 포함된 1250*350 크기의 이미지로만 구성된 슬라이드입니다. 가로 해상도가 1250px 미만 시, 해당 크기에 맞추어 가로/세로 비율이 함께 축소됩니다. 모바일 기기에서는 320*90 크기로 보이므로 텍스트 삽입 시, 가독성을 고려하여 최소 60pt 이상의 크기를 권장합니다. 가장 기본적인 형태의 슬라이드로 소스코드가 심플합니다.</p>
</div>
</div>
<!-- page-text1 end //-->
<!-- page-text2 start //-->
<div class="page-text2">
<div class="slide" id="slide2">
<!-- block start //-->
<div class="block">
<div class="box">
<div class="table">
<div class="table-sell">
<p class="font-zoom title" style="color:#46c8ff;">슬라이드 만들기 2장</p>
<p class="font-zoom title2" style="color:#ffffff;">텍스트+이미지 슬라이드</p>
</div>
</div>
</div>
<img src="<?=$page_skin_url?>/img/2-1.png" alt="">
</div>
<!-- block end //-->
<!-- block start //-->
<div class="block">
<div class="box">
<div class="table">
<div class="table-sell">
<p class="font-zoom title" style="color:#fcff53;">텍스트 폰트 72PT</p>
<p class="font-zoom title2" style="color:#555555;">CSS 사이즈 자동전환</p>
</div>
</div>
</div>
<img src="<?=$page_skin_url?>/img/2-2.png" alt="">
</div>
<!-- block end //-->
<!-- block start //-->
<div class="block">
<div class="box">
<div class="table">
<div class="table-sell">
<p class="font-zoom title" style="color:#7a5b42;">투명 이미지</p>
<p class="font-zoom title2" style="color:#dfceb7;">텍스트 폰트 사용</p>
</div>
</div>
</div>
<img src="<?=$page_skin_url?>/img/2-3.png" alt="">
</div>
<!-- block end //-->
<div class="num">
<ul>
<li></li>
<li></li>
<li></li>
</ul>
</div>
</div>
<div class="wrap">
<p class="font-zoom title">텍스트 + 이미지로 구성된 슬라이드</p>
<p class="font-zoom text">이미지(사진)이 포함된 1250*350 크기의 이미지 위에, 레이어 형태로 텍스트를 삽입한 슬라이드입니다. CSS로 구성된 텍스트는 PC/태블릿/스마트폰 기기의 가로 해상도에 맞추어 크기가 자동전환됩니다. 문자의 가독성을 중시할 때 사용하며, 이미지에는 텍스트를 삽입하지 마세요.(글자가 겹쳐 보일 수 있습니다.)</p>
</div>
</div>
<!-- page-text2 end //-->
<!-- page-text3 start //-->
<div class="page-text3">
<div class="linetop"></div>
<div class="slide" id="slide3">
<!-- block start //-->
<div class="block bg1">
<div class="img"><img src="<?=$page_skin_url?>/img/3-1.png" alt=""></div>
</div>
<!-- block end //-->
<!-- block start //-->
<div class="block bg2">
<div class="img"><img src="<?=$page_skin_url?>/img/3-2.png" alt=""></div>
</div>
<!-- block end //-->
<!-- block start //-->
<div class="block bg3">
<div class="img"><img src="<?=$page_skin_url?>/img/3-3.png" alt=""></div>
</div>
<!-- block end //-->
<div class="num">
<ul>
<li></li>
<li></li>
<li></li>
</ul>
</div>
</div>
<div class="wrap">
<p class="font-zoom title">이미지 + 백그라운드로 구성된 슬라이드</p>
<p class="font-zoom text">텍스트와 이미지(사진)가 포함된 1000*350 크기의 이미지와, 별도의 배경 이미지를 사용하여 2겹으로 구성된 슬라이드입니다. 1000px 이상의 해상도에서, 배경 이미지를 통해 와이드한 분위기를 연출할 수 있으며, 1000px미만의 해상도에서는 ‘이미지 슬라이드’와 동일하게 해당 크기에 맞추어 가로/세로 비율이 함께 축소됩니다.</p>
</div>
</div>
<!-- page-text3 end //-->
<!-- page-text4 start //-->
<div class="page-text4">
<div class="linetop"></div>
<div class="slide" id="slide4">
<!-- block start //-->
<div class="block bg1">
<div class="box">
<div class="table">
<div class="table-sell">
<p class="font-zoom title" style="color:#46c8ff;">슬라이드 만들기 4장</p>
<p class="font-zoom title2" style="color:#ffffff;">텍스트+이미지+BG 슬라이드</p>
</div>
</div>
</div>
<div class="img"><img src="<?=$page_skin_url?>/img/4-1.png" alt=""></div>
</div>
<!-- block end //-->
<!-- block start //-->
<div class="block bg2">
<div class="box">
<div class="table">
<div class="table-sell">
<p class="font-zoom title" style="color:#fcff53;">텍스트 폰트 72PT</p>
<p class="font-zoom title2" style="color:#555555;">CSS 사이즈 자동전환</p>
</div>
</div>
</div>
<div class="img"><img src="<?=$page_skin_url?>/img/4-2.png" alt=""></div>
</div>
<!-- block end //-->
<!-- block start //-->
<div class="block bg3">
<div class="box">
<div class="table">
<div class="table-sell">
<p class="font-zoom title" style="color:#7a5b42;">투명+배경 이미지</p>
<p class="font-zoom title2" style="color:#dfceb7;">텍스트 폰트 사용</p>
</div>
</div>
</div>
<div class="img"><img src="<?=$page_skin_url?>/img/4-3.png" alt=""></div>
</div>
<!-- block end //-->
<div class="num">
<ul>
<li></li>
<li></li>
<li></li>
</ul>
</div>
</div>
<div class="wrap">
<p class="font-zoom title">텍스트 + 이미지 + 백그라운드로 구성된 슬라이드</p>
<p class="font-zoom text">이미지(사진)가 포함된 1000*350 크기의 이미지와, 별도의 배경 이미지를 사용하고, 레이어 형태로 텍스트를 삽입하여 3겹으로 구성된 슬라이드입니다. 배경 이미지를 통해 와이드한 분위기를 연출하고, 문자의 가독성을 중시할 때 권장합니다.</p>
</div>
</div>
<!-- page-text4 end //-->