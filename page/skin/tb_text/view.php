<?php // 스킨
if (!defined("_WEB_")) exit;
?>
<style type="text/css">
.page-text1 {font-size:0; line-height:0; padding:0 0 65px 0; margin-top:5px; border-bottom:1px solid #dadada;}
.page-text1 p {margin:0;}
.page-text1 .line {margin-top:15px; width:40px; border-bottom:3px solid #1d8bc0;}
.page-text1 .title {font-weight:700; line-height:60px; font-size:60px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text1 .title2 {margin-top:12px;}
.page-text1 .title2 {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text1 .subj {margin-top:28px;}
.page-text1 .subj {font-weight:700; line-height:24px; font-size:24px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text1 .text {margin-top:35px;}
.page-text1 .text {font-weight:700; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text2 {font-size:0; line-height:0; padding:63px 0 65px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text2:after {display:block; clear:both; content:'';}
.page-text2 p {margin:0;}
.page-text2 .line {margin-top:15px; width:40px; border-bottom:3px solid #1d8bc0;}
.page-text2 .block1 {width:50%; float:left; vertical-align:top;}
.page-text2 .block2 {width:50%; float:right; vertical-align:top;}
.page-text2 .title {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .title2 {margin-top:12px;}
.page-text2 .title2 {font-weight:700; line-height:36px; font-size:36px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .subj {font-weight:700; line-height:24px; font-size:24px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .text {margin-top:35px;}
.page-text2 .text {font-weight:700; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text3 {font-size:0; line-height:0; padding:62px 0 65px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text3 p {margin:0;}
.page-text3 .line {margin-top:13px; width:20px; border-bottom:2px solid #1d8bc0;}
.page-text3 .title {margin-bottom:42px;}
.page-text3 .title {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .wrap {margin:-43px 0 0 -40px;}
.page-text3 .block {display:inline-block; vertical-align:top; width:33.333%;}
.page-text3 .block .box {margin-left:40px;}
.page-text3 .subj {margin-top:43px;}
.page-text3 .subj {font-weight:700; line-height:18px; font-size:18px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .text {margin-top:10px;}
.page-text3 .text {font-weight:400; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text4 {font-size:0; line-height:0; padding:63px 0 65px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text4 p {margin:0;}
.page-text4 .line {margin-top:10px; width:20px; border-bottom:2px solid #1d8bc0;}
.page-text4 .title {margin-bottom:50px; text-align:center;}
.page-text4 .title {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .wrap {margin:-40px 0 0 -40px;}
.page-text4 .block {display:inline-block; vertical-align:top; width:25%;}
.page-text4 .block .box {margin-left:40px;}
.page-text4 .subj {margin-top:43px;}
.page-text4 .subj {font-weight:700; line-height:18px; font-size:18px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .text {margin-top:17px;}
.page-text4 .text {font-weight:400; line-height:20px; font-size:13px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text5 {font-size:0; line-height:0; padding:66px 0 65px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text5 p {margin:0;}
.page-text5 .line {margin-top:13px; width:20px; border-bottom:2px solid #1d8bc0;}
.page-text5 .title {font-weight:700; line-height:24px; font-size:24px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text5 .subj {margin-top:28px;}
.page-text5 .subj {font-weight:700; line-height:18px; font-size:18px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text5 .text {margin-top:15px;}
.page-text5 .text {font-weight:700; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

@media screen and (max-width:1249px) {

.page-text1 {padding:0 10px 65px 10px;}
.page-text2 {padding:63px 10px 65px 10px;}
.page-text3 {padding:62px 10px 65px 10px;}
.page-text4 {padding:63px 10px 65px 10px;}
.page-text5 {padding:66px 10px 65px 10px;}

}

@media screen and (max-width:1000px) {

.page-text2 .block1 {width:100%;}
.page-text2 .block2 {width:100%;}
.page-text2 .subj {margin-top:50px;}
.page-text3 .block {width:50%;}
.page-text4 .block {width:50%;}

}

@media screen and (max-width:640px) {

.page-text2 .block1 {width:100%;}
.page-text2 .block2 {width:100%;}
.page-text2 .subj {margin-top:17px;}
.page-text3 .block {width:100%;}
.page-text4 .block {width:100%;}

.page-text1 {padding:0 10px 45px 10px;}
.page-text1 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text1 .title2 {margin-top:4px;}
.page-text1 .title2 {font-weight:bold; line-height:20px; font-size:18px; color:#1192ca; font-family:gulim,serif;}
.page-text1 .subj {margin-top:17px;}
.page-text1 .subj {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text1 .text {margin-top:25px;}
.page-text1 .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text2 {padding:45px 10px;}
.page-text2 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text2 .title2 {margin-top:4px;}
.page-text2 .title2 {font-weight:bold; line-height:20px; font-size:18px; color:#1192ca; font-family:gulim,serif;}
.page-text2 .subj {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text2 .text {margin-top:25px;}
.page-text2 .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text3 {padding:45px 10px;}
.page-text3 .line {margin-top:6px;}
.page-text3 .title {margin-bottom:22px;}
.page-text3 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text3 .subj {font-weight:bold; line-height:20px; font-size:18px; color:#000000; font-family:gulim,serif;}
.page-text3 .text {margin-top:14px;}
.page-text3 .text {font-weight:normal; line-height:15px; font-size:13px; color:#898989; font-family:gulim,serif;}

.page-text4 {padding:45px 10px;}
.page-text4 .line {margin-top:6px;}
.page-text4 .title {margin-bottom:38px;}
.page-text4 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text4 .subj {margin-top:23px;}
.page-text4 .subj {font-weight:bold; line-height:20px; font-size:18px; color:#000000; font-family:gulim,serif;}
.page-text4 .text {margin-top:12px;}
.page-text4 .text {font-weight:normal; line-height:20px; font-size:13px; color:#898989; font-family:gulim,serif;}

.page-text5 {padding:45px 10px;}
.page-text5 .line {margin-top:6px;}
.page-text5 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text5 .subj {margin-top:18px;}
.page-text5 .subj {font-weight:bold; line-height:20px; font-size:18px; color:#555555; font-family:gulim,serif;}
.page-text5 .text {margin-top:13px;}
.page-text5 .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

}
</style>
<!-- page-text1 start //-->
<div class="page-text1">
<p class="font-zoom title">MAIN TITLE</p>
<p class="font-zoom title2">메인 타이틀을 입력하세요.</p>
<p class="font-zoom subj">메인 타이틀을 보조하는 슬로건을 입력하세요.</p>
<p class="line"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 회사소개 · 회사연혁 · 제품안내 등의 웹 페이지 제작에 참고/활용하시기 바랍니다. 반응형 웹 페이지를 제작하기 위해서는 다양한 기기의 디스플레이 해상도와 실제 크기를 고려해야 합니다. 테라보드의 인터페이스는 모바일(320~640), 태블릿(641~1000), 모니터(1001~1250) 3가지의 형태로 구현됩니다. 이러한 특성 때문에 반응형 웹 페이지 소스코드는 과거의 동적인 웹페이지와 비교 시, 소스 코드의 늘어났으며 HTML과 CSS는 물론, 다양한 Script와 jQuery를 다룰 줄 알아야 합니다. 처음에는 어렵게 느껴질 수 있지만, 소스코드를 관찰하며 복사/붙여넣기 · 폰트의 크기/색상을 변경 · 자간/행간/여백 등을 조금씩 수정해보세요. 노력하다 보면, 반응형 웹 퍼블리싱의 달인이 된 자신의 모습을 보게 될 것입니다.</p>
</div>
<!-- page-text1 end //-->
<!-- page-text2 start //-->
<div class="page-text2">
<div class="block1">
<p class="font-zoom title">SUB TITLE</p>
<p class="font-zoom title2">서브 타이틀을 입력하세요.</p>
</div>
<div class="block2">
<p class="font-zoom subj">본문 내용을 보조하는 슬로건을 입력하세요.</p>
<p class="line"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 회사소개 · 회사연혁 · 제품안내 등의 웹 페이지 제작에 참고/활용하시기 바랍니다. 반응형 웹 페이지를 제작하기 위해서는 다양한 기기의 디스플레이 해상도와 실제 크기를 고려해야 합니다. 테라보드의 인터페이스는 모바일(320~640), 태블릿(641~1000), 모니터(1001~1250) 3가지의 형태로 구현됩니다. 이러한 특성 때문에 반응형 웹 페이지 소스코드는 과거의 동적인 웹페이지와 비교 시, 소스 코드의 늘어났으며 HTML과 CSS는 물론, 다양한 Script와 jQuery를 다룰 줄 알아야 합니다. 처음에는 어렵게 느껴질 수 있지만, 소스코드를 관찰하며 복사/붙여넣기 · 폰트의 크기/색상을 변경 · 자간/행간/여백 등을 조금씩 수정해보세요. 노력하다 보면, 반응형 웹 퍼블리싱의 달인이 된 자신의 모습을 보게 될 것입니다.</p>
</div>
</div>
<!-- page-text2 end //-->
<!-- page-text3 start //-->
<div class="page-text3">
<p class="font-zoom title">INTRODUCTION</p>
<div class="wrap">
<div class="block">
<div class="box">
<p class="font-zoom subj">01. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">02. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">03. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">04. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">05. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">06. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">07. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">08. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">09. Item Category</p>
<p class="line"></p>
<p class="font-zoom text">세부 아이템 명칭 또는 소개글을 입력하세요.</p>
</div>
</div>
</div>
</div>
<!-- page-text3 end //-->
<!-- page-text4 start //-->
<div class="page-text4">
<p class="font-zoom title">INTRODUCTION</p>
<div class="wrap">
<div class="block">
<div class="box">
<p class="font-zoom subj">TITLE NAME 01.</p>
<p class="line"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 회사소개 · 회사연혁 · 제품안내 등의 웹 페이지 제작에 참고/활용하시기 바랍니다. 반응형 웹 페이지를 제작하기 위해서는 다양한 기기의 디스플레이 해상도와 실제 크기를 고려해야 합니다. 테라보드의 인터페이스는 모바일(320~640), 태블릿(641~1000), 모니터(1001~1250) 3가지의 형태로 구현됩니다. 이러한 특성 때문에 반응형 웹 페이지 소스코드는 과거의 동적인 웹페이지와 비교 시, 소스 코드의 늘어났으며 HTML과 CSS는 물론, 다양한 Script와 jQuery를 다룰 줄 알아야 합니다.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">TITLE NAME 02.</p>
<p class="line"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 회사소개 · 회사연혁 · 제품안내 등의 웹 페이지 제작에 참고/활용하시기 바랍니다. 반응형 웹 페이지를 제작하기 위해서는 다양한 기기의 디스플레이 해상도와 실제 크기를 고려해야 합니다. 테라보드의 인터페이스는 모바일(320~640), 태블릿(641~1000), 모니터(1001~1250) 3가지의 형태로 구현됩니다. 이러한 특성 때문에 반응형 웹 페이지 소스코드는 과거의 동적인 웹페이지와 비교 시, 소스 코드의 늘어났으며 HTML과 CSS는 물론, 다양한 Script와 jQuery를 다룰 줄 알아야 합니다.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">TITLE NAME 03.</p>
<p class="line"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 회사소개 · 회사연혁 · 제품안내 등의 웹 페이지 제작에 참고/활용하시기 바랍니다. 반응형 웹 페이지를 제작하기 위해서는 다양한 기기의 디스플레이 해상도와 실제 크기를 고려해야 합니다. 테라보드의 인터페이스는 모바일(320~640), 태블릿(641~1000), 모니터(1001~1250) 3가지의 형태로 구현됩니다. 이러한 특성 때문에 반응형 웹 페이지 소스코드는 과거의 동적인 웹페이지와 비교 시, 소스 코드의 늘어났으며 HTML과 CSS는 물론, 다양한 Script와 jQuery를 다룰 줄 알아야 합니다.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">TITLE NAME 04.</p>
<p class="line"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 회사소개 · 회사연혁 · 제품안내 등의 웹 페이지 제작에 참고/활용하시기 바랍니다. 반응형 웹 페이지를 제작하기 위해서는 다양한 기기의 디스플레이 해상도와 실제 크기를 고려해야 합니다. 테라보드의 인터페이스는 모바일(320~640), 태블릿(641~1000), 모니터(1001~1250) 3가지의 형태로 구현됩니다. 이러한 특성 때문에 반응형 웹 페이지 소스코드는 과거의 동적인 웹페이지와 비교 시, 소스 코드의 늘어났으며 HTML과 CSS는 물론, 다양한 Script와 jQuery를 다룰 줄 알아야 합니다.</p>
</div>
</div>
</div>
</div>
<!-- page-text4 end //-->
<!-- page-text5 start //-->
<div class="page-text5">
<p class="font-zoom title">INTRODUCTION</p>
<p class="line"></p>
<p class="font-zoom subj">본문내용을 보조하는 슬로건을 입력하세요.</p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 회사소개 · 회사연혁 · 제품안내 등의 웹 페이지 제작에 참고/활용하시기 바랍니다. 반응형 웹 페이지를 제작하기 위해서는 다양한 기기의 디스플레이 해상도와 실제 크기를 고려해야 합니다. 테라보드의 인터페이스는 모바일(320~640), 태블릿(641~1000), 모니터(1001~1250) 3가지의 형태로 구현됩니다. 이러한 특성 때문에 반응형 웹 페이지 소스코드는 과거의 동적인 웹페이지와 비교 시, 소스 코드의 늘어났으며 HTML과 CSS는 물론, 다양한 Script와 jQuery를 다룰 줄 알아야 합니다. 처음에는 어렵게 느껴질 수 있지만, 소스코드를 관찰하며 복사/붙여넣기 · 폰트의 크기/색상을 변경 · 자간/행간/여백 등을 조금씩 수정해보세요. 노력하다 보면, 반응형 웹 퍼블리싱의 달인이 된 자신의 모습을 보게 될 것입니다.</p>
</div>
<!-- page-text5 end //-->