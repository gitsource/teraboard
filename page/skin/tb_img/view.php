<?php // 스킨
if (!defined("_WEB_")) exit;
?>
<style type="text/css">
.layout-wrap,
.layout-contents,
.layout-main {}
.layout-contents {max-width:100%;}

.layout-subtop-adm {max-width:1250px; margin-left:auto; margin-right:auto;}
.layout-subtop-title {max-width:1250px; margin-left:auto; margin-right:auto;}

.page-text1,
.page-text2,
.page-text3 .linetop,
.page-text3 .wrap,
.page-text4,
.page-text5,
.page-text6 {margin:0 auto; max-width:1250px;}

.page-text1 {position:relative; font-size:0; line-height:0; padding:0 0 65px 0; margin-top:5px; border-bottom:1px solid #dadada;}
.page-text1:after {display:block; clear:both; content:'';}
.page-text1 p {margin:0;}
.page-text1 .line {margin-top:15px; width:40px; border-bottom:3px solid #1d8bc0;}
.page-text1 .block1 {position:absolute; width:320px; vertical-align:top;}
.page-text1 .block1 img {width:320px; height:240px;}
.page-text1 .block2 {margin-left:360px; vertical-align:top; margin-top:-4px;}
.page-text1 .title {font-weight:700; line-height:60px; font-size:60px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text1 .title2 {margin-top:12px;}
.page-text1 .title2 {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text1 .subj {margin-top:27px;}
.page-text1 .subj {font-weight:700; line-height:24px; font-size:24px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text1 .text {margin-top:35px;}
.page-text1 .text {font-weight:700; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text2 {font-size:0; line-height:0; padding:63px 0 65px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text2:after {display:block; clear:both; content:'';}
.page-text2 p {margin:0;}
.page-text2 .line {margin-top:15px; width:40px; border-bottom:3px solid #1d8bc0;}
.page-text2 .block1 {width:50%; float:left; vertical-align:top;}
.page-text2 .block1 .wrap {margin-right:20px; text-align:center;}
.page-text2 .block1 img {width:100%; max-width:640px;}
.page-text2 .block2 {width:50%; float:right; vertical-align:top; margin-top:-4px;}
.page-text2 .block2 .wrap {margin-left:20px;}
.page-text2 .title {font-weight:700; line-height:60px; font-size:60px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .title2 {margin-top:12px;}
.page-text2 .title2 {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .subj {margin-top:27px;}
.page-text2 .subj {font-weight:700; line-height:24px; font-size:24px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text2 .text {margin-top:35px;}
.page-text2 .text {font-weight:700; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text3 {font-size:0; line-height:0; margin-top:5px;}
.page-text3 p {margin:0;}
.page-text3 .linetop {font-size:0; line-height:0; border-top:1px solid #dadada; padding-top:70px;}
.page-text3 .wrap {border-bottom:1px solid #dadada; padding-bottom:70px;}
.page-text3 .wrap:after {display:block; clear:both; content:'';}
.page-text3 .line {margin-top:15px; width:40px; border-bottom:3px solid #1d8bc0;}
.page-text3 .block1 img {width:100%;}
.page-text3 .block2 {width:50%; float:left; vertical-align:top; margin-top:45px;}
.page-text3 .block3 {width:50%; float:right; vertical-align:top; margin-top:45px;}
.page-text3 .title {font-weight:700; line-height:60px; font-size:60px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .title2 {margin-top:12px;}
.page-text3 .title2 {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .subj {margin-top:27px;}
.page-text3 .subj {font-weight:700; line-height:24px; font-size:24px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text3 .text {font-weight:700; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text4 {font-size:0; line-height:0; padding:70px 0 65px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text4:after {display:block; clear:both; content:'';}
.page-text4 p {margin:0;}
.page-text4 .line {margin-top:15px; width:40px; border-bottom:3px solid #1d8bc0;}
.page-text4 .block1 {width:100%; height:280px; background:url('<?=$page_skin_url?>/img/4.png') no-repeat center center;}
.page-text4 .block2 {width:50%; float:left; vertical-align:top; margin-top:45px;}
.page-text4 .block3 {width:50%; float:right; vertical-align:top; margin-top:45px;}
.page-text4 .title {font-weight:700; line-height:60px; font-size:60px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .title2 {margin-top:12px;}
.page-text4 .title2 {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .subj {margin-top:27px;}
.page-text4 .subj {font-weight:700; line-height:24px; font-size:24px; color:#555555; font-family:'Nanum Gothic',gulim,serif;}
.page-text4 .text {font-weight:700; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text5 {font-size:0; line-height:0; padding:62px 0 65px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text5 p {margin:0;}
.page-text5 .line {margin-top:10px; width:20px; border-bottom:2px solid #1d8bc0;}
.page-text5 .title {margin-bottom:42px;}
.page-text5 .title {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text5 .wrap {margin:-43px 0 0 -40px;}
.page-text5 .block {display:inline-block; vertical-align:top; width:33.333%;}
.page-text5 .block .box {margin-left:40px; position:relative; padding-left:100px;}
.page-text5 .block .box img {position:absolute; left:0; top:0;}
.page-text5 .subj {margin-top:43px;}
.page-text5 .subj {font-weight:700; line-height:18px; font-size:18px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.page-text5 .text {margin-top:10px;}
.page-text5 .text {font-weight:400; line-height:24px; font-size:15px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

.page-text6 {font-size:0; line-height:0; padding:63px 0 65px 0; margin-top:5px; border-top:1px solid #dadada; border-bottom:1px solid #dadada;}
.page-text6 p {margin:0;}
.page-text6 .line {margin-top:10px; width:20px; border-bottom:2px solid #1d8bc0;}
.page-text6 .title {margin-bottom:50px; text-align:center;}
.page-text6 .title {font-weight:700; line-height:48px; font-size:48px; color:#1192ca; font-family:'Nanum Gothic',gulim,serif;}
.page-text6 .wrap {margin:-40px 0 0 -40px;}
.page-text6 .block {display:inline-block; vertical-align:top; width:25%;}
.page-text6 .block .box {margin-left:40px;}
.page-text6 .img1 {margin-top:20px; width:100%; height:120px; background:url('<?=$page_skin_url?>/img/6-1.png') no-repeat center center;}
.page-text6 .img2 {margin-top:20px; width:100%; height:120px; background:url('<?=$page_skin_url?>/img/6-2.png') no-repeat center center;}
.page-text6 .img3 {margin-top:20px; width:100%; height:120px; background:url('<?=$page_skin_url?>/img/6-3.png') no-repeat center center;}
.page-text6 .img4 {margin-top:20px; width:100%; height:120px; background:url('<?=$page_skin_url?>/img/6-4.png') no-repeat center center;}
.page-text6 .subj {margin-top:43px;}
.page-text6 .subj {font-weight:700; line-height:18px; font-size:18px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.page-text6 .text {margin-top:17px;}
.page-text6 .text {font-weight:400; line-height:20px; font-size:13px; color:#898989; font-family:'Nanum Gothic',gulim,serif;}

@media screen and (max-width:1249px) {

.page-text1 {padding:0 10px 65px 10px;}
.page-text2 {padding:63px 10px 65px 10px;}
.page-text3 .wrap {padding:0 10px 0 10px;}
.page-text4 {padding:70px 10px 65px 10px;}
.page-text5 {padding:62px 10px 65px 10px;}
.page-text6 {padding:63px 10px 65px 10px;}

}

@media screen and (max-width:1000px) {

.page-text1 .block1 {position:static; width:100%; text-align:center;}
.page-text1 .block1 img {}
.page-text1 .block2 {margin-left:0; margin-top:50px;}
.page-text2 .block1 {width:100%;}
.page-text2 .block1 .wrap {margin-right:0;}
.page-text2 .block1 img {width:100%;}
.page-text2 .block2 {width:100%; margin-top:40px;}
.page-text2 .block2 .wrap {margin-left:0px;}
.page-text3 .wrap {padding-bottom:70px;}
.page-text3 .block2 {width:100%; margin-top:40px;}
.page-text3 .block3 {width:100%; margin-top:0;}
.page-text3 .text {margin-top:45px;}
.page-text4 .block2 {width:100%; margin-top:40px;}
.page-text4 .block3 {width:100%; margin-top:0px;}
.page-text4 .text {margin-top:45px;}
.page-text5 .block {width:50%;}
.page-text6 .block {width:50%;}

}

@media screen and (max-width:640px) {

.page-text1 {padding:0 10px 45px 10px;}
.page-text1 .block2 {margin-top:30px;}
.page-text1 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text1 .title2 {margin-top:4px;}
.page-text1 .title2 {font-weight:bold; line-height:20px; font-size:18px; color:#1192ca; font-family:gulim,serif;}
.page-text1 .subj {margin-top:17px;}
.page-text1 .subj {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text1 .text {margin-top:25px;}
.page-text1 .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text2 {padding:45px 10px;}
.page-text2 .block2 {margin-top:30px;}
.page-text2 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text2 .title2 {margin-top:4px;}
.page-text2 .title2 {font-weight:bold; line-height:20px; font-size:18px; color:#1192ca; font-family:gulim,serif;}
.page-text2 .subj {margin-top:17px;}
.page-text2 .subj {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text2 .text {margin-top:25px;}
.page-text2 .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text3 .linetop {padding-top:50px;}
.page-text3 .block2 {margin-top:30px;}
.page-text3 .wrap {padding-bottom:45px;}
.page-text3 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text3 .title2 {margin-top:4px;}
.page-text3 .title2 {font-weight:bold; line-height:20px; font-size:18px; color:#1192ca; font-family:gulim,serif;}
.page-text3 .subj {margin-top:17px;}
.page-text3 .subj {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text3 .text {margin-top:25px;}
.page-text3 .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text4 {padding:45px 10px;}
.page-text4 .block2 {margin-top:30px;}
.page-text4 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text4 .title2 {margin-top:4px;}
.page-text4 .title2 {font-weight:bold; line-height:20px; font-size:18px; color:#1192ca; font-family:gulim,serif;}
.page-text4 .subj {margin-top:17px;}
.page-text4 .subj {font-weight:bold; line-height:18px; font-size:16px; color:#555555; font-family:gulim,serif;}
.page-text4 .text {margin-top:25px;}
.page-text4 .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

.page-text5 .block {width:100%;}
.page-text5 {padding:45px 10px;}
.page-text5 .title {margin-bottom:38px;}
.page-text5 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text5 .subj {margin-top:23px;}
.page-text5 .subj {font-weight:bold; line-height:20px; font-size:18px; color:#000000; font-family:gulim,serif;}
.page-text5 .text {margin-top:12px;}
.page-text5 .text {font-weight:normal; line-height:20px; font-size:13px; color:#898989; font-family:gulim,serif;}

.page-text6 .block {width:100%;}
.page-text6 {padding:45px 10px;}
.page-text6 .title {font-weight:bold; line-height:26px; font-size:24px; color:#1192ca; font-family:gulim,serif;}
.page-text6 .subj {margin-top:18px;}
.page-text6 .subj {font-weight:bold; line-height:20px; font-size:18px; color:#555555; font-family:gulim,serif;}
.page-text6 .text {margin-top:13px;}
.page-text6 .text {font-weight:normal; line-height:24px; font-size:15px; color:#898989; font-family:gulim,serif;}

}
</style>
<!-- page-text1 start //-->
<div class="page-text1">
<div class="block1"><img src="<?=$page_skin_url?>/img/1.png" alt=""></div>
<div class="block2">
<p class="font-zoom title">고정된 이미지</p>
<p class="font-zoom title2">320*240픽셀 이미지</p>
<p class="font-zoom subj">이미지의 비율을 유지하며, 크기가 고정됩니다.</p>
<p class="line"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 반응형 웹 페이지를 제작에 사용되는 이미지는 디스플레이(모바일, 태블릿, 모니터)의 가로/세로 비율과 가로 폭(px)을 고려해야 합니다. 보편적으로 6:4비율의 이미지를 320px의 배수 형태로 제작하는 것을 권장합니다.</p>
</div>
</div>
<!-- page-text1 end //-->
<!-- page-text2 start //-->
<div class="page-text2">
<div class="block1"><div class="wrap"><img src="<?=$page_skin_url?>/img/2.png" alt=""></div></div>
<div class="block2">
<div class="wrap">
<p class="font-zoom title">반응하는 이미지</p>
<p class="font-zoom title2">640*480픽셀 이미지</p>
<p class="font-zoom subj">화면의 가로 폭 50~100%를 유지합니다.</p>
<p class="line"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 반응형 웹 페이지를 제작에 사용되는 이미지는 디스플레이(모바일, 태블릿, 모니터)의 가로/세로 비율과 가로 폭(px)을 고려해야 합니다. 보편적으로 6:4비율의 이미지를 320px의 배수 형태로 제작하는 것을 권장합니다. 원본 크기 320*240픽셀의 이미지가 늘어나서 보일 경우, 화질이 저하되므로, 원본 크기 640*480 혹은 1280*960로 제작하실 경우, 축소되어 보이므로 화질 손상을 예방하실 수 있습니다. 단, 고해상도의 이미지 사용 시에는 게시판 스킨과 달리 페이지 스킨은 섬네일을 자동 생성하지 않으므로, 용량이 늘어나는 단점이 있습니다. 하지만 정상적인 네트워크 환경에서는 액세스의 속도가 크게 차이가 나지 않으므로, 회사소개/제품안내 등의 중요한 페이지는 고해상도 이미지의 사용을 권장합니다.</p>
</div>
</div>
</div>
<!-- page-text2 end //-->
<!-- page-text3 start //-->
<div class="page-text3">
<div class="linetop"></div>
<div class="block1"><img src="<?=$page_skin_url?>/img/3.png" alt=""></div>
<div class="wrap">
<div class="block2">
<p class="font-zoom title">풀사이즈 이미지</p>
<p class="font-zoom title2">2700*600픽셀 이미지</p>
<p class="font-zoom subj">비율을 유지하며 가로 폭 100%를 유지합니다.</p>
<p class="line"></p>
</div>
<div class="block3">
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 반응형 웹 페이지를 제작에 사용되는 이미지는 디스플레이(모바일, 태블릿, 모니터)의 가로/세로 비율과 가로 폭(px)을 고려해야 합니다. 보편적으로 6:4비율의 이미지를 320px의 배수 형태로 제작하는 것을 권장합니다. 원본 크기 320*240픽셀의 이미지가 늘어나서 보일 경우, 화질이 저하되므로, 원본 크기 640*480 혹은 1280*960로 제작하실 경우, 축소되어 보이므로 화질 손상을 예방하실 수 있습니다. 단, 고해상도의 이미지 사용 시에는 게시판 스킨과 달리 페이지 스킨은 섬네일을 자동 생성하지 않으므로, 용량이 늘어나는 단점이 있습니다. 하지만 정상적인 네트워크 환경에서는 액세스의 속도가 크게 차이가 나지 않으므로, 회사소개/제품안내 등의 중요한 페이지는 고해상도 이미지의 사용을 권장합니다.</p>
</div>
</div>
</div>
<!-- page-text3 end //-->
<!-- page-text4 start //-->
<div class="page-text4">
<div class="block1"></div>
<div class="block2">
<p class="font-zoom title">잘라내는 이미지</p>
<p class="font-zoom title2">1250*280픽셀 이미지</p>
<p class="font-zoom subj">좌우를 잘라내어 가로 폭 100%를 유지합니다.</p>
<p class="line"></p>
</div>
<div class="block3">
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 반응형 웹 페이지를 제작에 사용되는 이미지는 디스플레이(모바일, 태블릿, 모니터)의 가로/세로 비율과 가로 폭(px)을 고려해야 합니다. 보편적으로 6:4비율의 이미지를 320px의 배수 형태로 제작하는 것을 권장합니다. 원본 크기 320*240픽셀의 이미지가 늘어나서 보일 경우, 화질이 저하되므로, 원본 크기 640*480 혹은 1280*960로 제작하실 경우, 축소되어 보이므로 화질 손상을 예방하실 수 있습니다. 단, 고해상도의 이미지 사용 시에는 게시판 스킨과 달리 페이지 스킨은 섬네일을 자동 생성하지 않으므로, 용량이 늘어나는 단점이 있습니다. 하지만 정상적인 네트워크 환경에서는 액세스의 속도가 크게 차이가 나지 않으므로, 회사소개/제품안내 등의 중요한 페이지는 고해상도 이미지의 사용을 권장합니다.</p>
</div>
</div>
<!-- page-text4 end //-->
<!-- page-text5 start //-->
<div class="page-text5">
<p class="font-zoom title">여러개의 이미지</p>
<div class="wrap">
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-1.png" alt="">
<p class="font-zoom subj">01. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-2.png" alt="">
<p class="font-zoom subj">02. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-3.png" alt="">
<p class="font-zoom subj">03. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-4.png" alt="">
<p class="font-zoom subj">04. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-5.png" alt="">
<p class="font-zoom subj">05. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-6.png" alt="">
<p class="font-zoom subj">06. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-7.png" alt="">
<p class="font-zoom subj">07. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-8.png" alt="">
<p class="font-zoom subj">08. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
<div class="block">
<div class="box">
<img src="<?=$page_skin_url?>/img/5-9.png" alt="">
<p class="font-zoom subj">09. 고정된 이미지</p>
<p class="line"></p>
<p class="font-zoom text">이미지의 원본 크기를 적용</p>
</div>
</div>
</div>
</div>
<!-- page-text5 end //-->
<!-- page-text6 start //-->
<div class="page-text6">
<p class="font-zoom title">여러개의 이미지</p>
<div class="wrap">
<div class="block">
<div class="box">
<p class="font-zoom subj">잘라내는 이미지 01.</p>
<p class="line"></p>
<p class="img1"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 반응형 웹 페이지를 제작에 사용되는 이미지는 디스플레이(모바일, 태블릿, 모니터)의 가로/세로 비율과 가로 폭(px)을 고려해야 합니다. 보편적으로 6:4비율의 이미지를 320px의 배수 형태로 제작하는 것을 권장합니다.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">잘라내는 이미지 02.</p>
<p class="line"></p>
<p class="img2"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 반응형 웹 페이지를 제작에 사용되는 이미지는 디스플레이(모바일, 태블릿, 모니터)의 가로/세로 비율과 가로 폭(px)을 고려해야 합니다. 보편적으로 6:4비율의 이미지를 320px의 배수 형태로 제작하는 것을 권장합니다.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">잘라내는 이미지 03.</p>
<p class="line"></p>
<p class="img3"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 반응형 웹 페이지를 제작에 사용되는 이미지는 디스플레이(모바일, 태블릿, 모니터)의 가로/세로 비율과 가로 폭(px)을 고려해야 합니다. 보편적으로 6:4비율의 이미지를 320px의 배수 형태로 제작하는 것을 권장합니다.</p>
</div>
</div>
<div class="block">
<div class="box">
<p class="font-zoom subj">잘라내는 이미지 04.</p>
<p class="line"></p>
<p class="img4"></p>
<p class="font-zoom text">본 페이지는 테라보드가 제공하는 ‘반응형 웹 페이지’ 제작을 위한 스킨 양식입니다. 반응형 웹 페이지를 제작에 사용되는 이미지는 디스플레이(모바일, 태블릿, 모니터)의 가로/세로 비율과 가로 폭(px)을 고려해야 합니다. 보편적으로 6:4비율의 이미지를 320px의 배수 형태로 제작하는 것을 권장합니다.</p>
</div>
</div>
</div>
</div>
<!-- page-text6 end //-->