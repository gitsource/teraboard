<?php // 페이지
include_once("./_tb.php");

unset($page_skin_path);
unset($page_skin_url);

$page = page($page_id);

if (!$page['page_id']) {

    message("<p class='title'>알림</p><p class='text'>페이지가 존재하지 않습니다.</p>", "b");

}

$page_skin_path = $web['path']."/page/skin/".$page['page_skin'];
$page_skin_url = $web['host_page']."/skin/".$page['page_skin'];

if (!$page['page_onoff'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>접근할 수 없습니다.</p>", "b");

}

// 성인인증
if ($page['page_adult']) {

    if ($check_login) {

        if (!$member['certify_name'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>본인확인이 필요합니다.</p>", "", $web['host_member']."/", true, true);

        }

        if (!$member['certify_adult'] && !$check_admin) {

            message("<p class='title'>알림</p><p class='text'>성인이 아닙니다.</p>", "", $web['host_member']."/", true, true);

        }

    } else {

        url($web['host_member']."/login/?url={$urlencode}");

    }

}

if ($member['level'] < $page['page_level']) {

    if ($check_login) {

        if ($member['level'] < $page['page_level']) {

            message("<p class='title'>알림</p><p class='text'>접근할 수 없는 페이지입니다.</p>", "b");

        }

    } else {

        url($web['host_member']."/login/?url={$urlencode}");

    }

}

if ($page['page_mode'] == 2) {

    url(url_http($page['page_url']));

}

// 치환 항목
$page['page_content'] = str_replace("{홈페이지명}", $setup['title'], $page['page_content']);
$page['page_content'] = str_replace("{도메인주소}", $web['host_default'], $page['page_content']);
$page['page_content'] = str_replace("{회사명}", $setup['company'], $page['page_content']);
$page['page_content'] = str_replace("{대표자명}", $setup['ceo'], $page['page_content']);
$page['page_content'] = str_replace("{사업자등록번호}", $setup['number1'], $page['page_content']);
$page['page_content'] = str_replace("{통신판매업신고번호}", $setup['number2'], $page['page_content']);
$page['page_content'] = str_replace("{대표메일}", $setup['email'], $page['page_content']);
$page['page_content'] = str_replace("{대표번호}", $setup['tel'], $page['page_content']);
$page['page_content'] = str_replace("{팩스번호}", $setup['fax'], $page['page_content']);
$page['page_content'] = str_replace("{회사주소}", $setup['addr'], $page['page_content']);
$page['page_content'] = str_replace("{개인정보책임자명}", $setup['privace_name'], $page['page_content']);
$page['page_content'] = str_replace("{개인정보책임자메일}", $setup['privace_email'], $page['page_content']);
$page['page_content'] = str_replace("{닉네임}", $member['nick'], $page['page_content']);
$page['page_content'] = str_replace("{회원ID}", $member['uid'], $page['page_content']);
$page['page_content'] = str_replace("{성명}", $member['name'], $page['page_content']);
$page['page_content'] = str_replace("{가입일}", text_date("Y년 m월 d일", $member['datetime'], ""), $page['page_content']);

// head start
ob_start();
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = $page['page_title'];
$web['menu'] = $page['page_group'];
$web['menusub'] = "P-".$page['page_id'];
include_once("./_top.php");
?>
<!-- layout-page start //-->
<div class="layout-page">
<?
if ($page['page_mode'] == 1) {

    include_once("$page_skin_path/view.php");

} else {

    echo "<div class='font-zoom page-content'>".text2($page['page_content'],1)."</div>";

}
?>
</div>
<!-- layout-page end //-->
<?
include_once("./_bottom.php");
?>