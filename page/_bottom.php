<?php // 하단
if (!defined("_WEB_")) exit;

if ($page['page_text_bottom'] && $page['page_text_bottom'] != '<br>' && $page['page_text_bottom'] != '<br />' && $page['page_text_bottom'] != '<p>&nbsp;</p>' && $page['page_text_bottom'] != '&nbsp;') { echo "<div>".text2($page['page_text_bottom'],1)."</div>"; }

if ($page['page_include_bottom']) {

    include_once("{$page['page_include_bottom']}");

} else {

    include_once("$web[path]/_bottom.php");

}
?>