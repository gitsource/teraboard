<?php
include_once("./_tb.php");

if (!$setup['sideview_profile'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>회원 프로필이 미사용중입니다.</p>", "c");

}

if (!$mid) {

    message("<p class='title'>알림</p><p class='text'>존재하지 않는 회원입니다.</p>", "c");

}

$mb = member($mid);

if (!$mb['mid']) {

    message("<p class='title'>알림</p><p class='text'>존재하지 않는 회원입니다.</p>", "c");

}

$level = member_level($mb['level']);

if ($check_admin) {

    $setup['sideview_message'] = true;
    $setup['sideview_email'] = true;
    $setup['sideview_sms'] = true;
    $setup['sideview_homepage'] = true;
    $setup['sideview_profile'] = true;
    $setup['sideview_bbs'] = true;

    $setup['hp_consent'] = true;
    $setup['email_consent'] = true;

}

$cnt = (int)(((strtotime($web['time_ymdhis']) - strtotime($mb['datetime'])) / 86400) + 1);
$cnt2 = (int)(((strtotime($web['time_ymdhis']) - strtotime($mb['login_datetime'])) / 86400) + 1);

$join = text_date("Y. m. d", $mb['datetime'], "");

if (date("Y-m-d", strtotime($mb['datetime'])) == $web['time_ymd']) {

    $join .= " (오늘)";

} else {

    $join .= " (".number($cnt)."일)";

}

$chk = sql_fetch(" select id from $web[connect_table] where mid = '".addslashes($mid)."' limit 0, 1 ");

if ($chk['id']) {

    $connect = "<font color='#4cff50'>현재 접속중</font>";

}

else if ($mb['login_datetime'] == '0000-00-00 00:00:00') {

    $connect = "없음";

} else {

    $connect = text_date("Y. m. d", $mb['login_datetime'], "");

    if (date("Y-m-d", strtotime($mb['login_datetime'])) == $web['time_ymd']) {

        $connect .= " (오늘)";

    } else {

        $connect .= " (".number($cnt2)."일)";

    }

}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<?
$web['title'] = "MEMBER PROFILE";
include_once("$web[path]/_head.php");
?>
<style type="text/css">
body {background-color:#ffffff; min-width:320px;}
.title {text-align:center; position:relative; height:50px; border-bottom:5px solid #4c5966; background-color:#42abd7;}
.title {font-weight:700; line-height:50px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.title img {vertical-align:middle;}
.title .btn {font-size:0px; position:absolute; right:10px; top:0; display:block; width:50px; height:50px;}
.title .btn a {display:inline-block; width:50px; height:50px; background:url('<?=$web['host_member']?>/img/profile_close.png') no-repeat;}
.title .btn a.close {background-position:0 0;}
.title .btn a.close:hover {background-position:0 -50px;}

.info {clear:both; border-bottom:5px solid #303c48; background-color:#384655;}
.info .bl {float:left;}
.info .bl {position:relative; width:280px;}
.info .bl .email,
.info .bl .memo,
.info .bl .sms {position:absolute; left:24px; top:54px; display:block; width:36px; height:36px; background:url('<?=$web['host_member']?>/img/profile_sand.png') no-repeat;}
.info .bl .email {background-position:0 0;}
.info .bl .email:hover {background-position:0 -36px;}
.info .bl .memo {background-position:-36px 0;}
.info .bl .memo:hover {background-position:-36px -36px;}
.info .bl .sms {background-position:-72px 0;}
.info .bl .sms:hover {background-position:-72px -36px;}
.info .bl .posr {left:auto; right:24px;}

.info .bl .member_thumb {margin:22px auto 0 auto; position:relative; width:94px; height:94px;}
.info .bl .member_thumb img {position:absolute; top:0; left:0; width:94px; height:94px; border-radius:94px; border:3px solid #42abd7;}
.info .bl p.nick {margin:0px; margin:14px 0 22px 0; text-align:center; overflow:hidden; display:block; height:18px;}
.info .bl p.nick {font-weight:700; line-height:18px; font-size:18px; color:#42abd7; font-family:'Nanum Gothic',gulim,serif;}

.info .br {margin:0 20px 0 280px; padding:5px 0;}
.info .br ul {font-size:0px;}
.info .br ul li {display:block; border-top:1px solid #2d3844;}
.info .br ul li .line {border-top:1px solid #465361;}
.info .br ul li .subj {display:inline-block; padding-left:9px; width:100px; font-weight:700; line-height:40px; font-size:13px; color:#a7afb7; font-family:'Nanum Gothic',gulim,serif;}
.info .br ul li nobr {vertical-align:top; display:inline-block; overflow:hidden; height:40px; max-width:60%; text-overflow:ellipsis; word-break:break-all;}
.info .br ul li nobr {font-weight:700; line-height:40px; font-size:13px; color:#f0f3f6; font-family:'Nanum Gothic',gulim,serif;}
.info .br ul li:first-child {border-top:0px;}
.info .br ul li:first-child .line {border-top:0px;}

.wrap {clear:both; padding:0 20px 0 20px;}
.wrap .content {min-height:130px; margin-top:18px; padding-top:20px; background:url('<?=$web['host_member']?>/img/_line_w7_h1.png') repeat-x 0 top;}
.wrap .content {font-weight:400; line-height:18px; font-size:14px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}

.wrap .close {margin:18px 0; padding-top:20px; background:url('<?=$web['host_member']?>/img/_line_w7_h1.png') repeat-x 0 top;}
.wrap .close div {display:block; text-align:center; cursor:pointer; height:48px; border:1px solid #dadada; background-color:#ffffff;}
.wrap .close div {font-weight:700; line-height:48px; font-size:20px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
.wrap .close div:hover {border:1px solid #303c48; color:#303c48;}

@media screen and (max-width:600px) {

.info .bl .email,
.info .bl .memo,
.info .bl .sms {left:44px;}
.info .bl .posr {left:auto; right:44px;}

.info .bl {float:clear;}
.info .bl {position:relative; width:100%;}
.info .br {margin:0; padding:5px 0;}
.info .br ul li {border-top:1px solid #2d3844;}
.info .br ul li .line {border-top:1px solid #465361;}

.wrap {padding:0px;}
.wrap .content {padding:20px 10px;}
.wrap .close {padding:20px 10px;}

}
</style>
<script type="text/javascript">
$(document).ready(function() {

    $('.wrap .close div').click(function() {

        window.close();

    });

});
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div class="title">MEMBER PROFILE
<div class="btn">
<a href="#" onclick="window.close(); return false;" title="닫기" class="close"></a>
</div>
</div>
<div class="info">
<div class="bl">
<?
$count = 0;
if ($setup['sideview_message']) {

    $count++;

    echo "<a href='#' onclick=\"messageRegist('".text($mid)."'); return false;\" title='메세지 보내기' class='memo'></a>";

}

if ($setup['sms_onoff'] && $setup['sideview_sms'] && $mb['hp'] && $mb['hp_consent']) {

    $count++;

    echo "<a href='#' onclick=\"smsRegist('".text($mid)."'); return false;\" title='문자 보내기' class='sms";

    if ($count >= 2) {
        echo " posr";
    }

    echo "'></a>";

}

if ($count < 2 && $setup['email'] && $setup['email_onoff'] && $setup['sideview_email'] && $mb['email'] && $mb['email_consent']) {

    $count++;

    echo "<a href='#' onclick=\"emailRegist('".text($mid)."'); return false;\" title='이메일 보내기' class='email";

    if ($count >= 2) {
        echo " posr";
    }

    echo "'></a>";

}
?>
<div class="member_thumb"><?=member_thumb($mb['mid'])?></div>
<p class="nick"><nobr><span class="sideview" onclick="sideviewLoad(this, '<?=text($mb['mid'])?>');"><?=text($mb['nick'])?></span></nobr></p>
</div>
<div class="br">
<ul>
<li><div class="line"><span class="subj">등급</span><nobr><?=text($level['title'])?> <font color="#42abd7">(Lv. <?=text($mb['level'])?>)</font></nobr></div></li>
<li><div class="line"><span class="subj">포인트</span><nobr class="point"><?=number($mb['point'], $setup['point_number'])?> 점</nobr></div></li>
<li><div class="line"><span class="subj">게시물 / 댓글</span><nobr class="count"><?=number($mb['bbs_write_count'])?> 건 / <?=number($mb['bbs_reply_count'])?> 건</nobr></div></li>
<li><div class="line"><span class="subj">가입일</span><nobr><?=$join?></nobr></div></li>
<li><div class="line"><span class="subj">최종접속</span><nobr><?=$connect?></nobr></div></li>
</ul>
</div>
<div class="clr"></div>
</div>
<div class="wrap">
<div class="content"><?=member_profile($mb['profile'], $mb);?></div>
<div class="close"><div>닫기</div></div>
</div>
</body>
</html>