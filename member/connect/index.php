<?php // 접속자
include_once("./_tb.php");

if (!$m) {
    $m = 0;
}

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px;}

.layout-block {padding:50px 0;}

.layout-connect {background-color:#ffffff;}

.layout-connect .menu {font-size:0px; height:51px; border-bottom:1px solid #ffffff;}
.layout-connect .menu div {display:inline-block; width:25%;}
.layout-connect .menu div a {position:relative; text-align:center; display:block; height:49px; background-color:#f9f9f9; border:1px solid #e0e1e3;}
.layout-connect .menu div a {text-decoration:none; font-weight:bold; line-height:49px; font-size:16px; color:#999999; font-family:,gulim,serif;}
.layout-connect .menu div.on a {background-color:#42abd7; border:1px solid #1192ca; color:#ffffff;}

.layout-connect .list {clear:both; margin-top:20px;}
.layout-connect .list tr.title td {position:relative; text-align:center; font-weight:700; line-height:38px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-connect .list tr.title td {border-left:1px solid #dadada;}
.layout-connect .list tr.title td:last-child {border-right:1px solid #dadada;}
.layout-connect .list tr td.date {width:160px;}
.layout-connect .list tr td.nick {width:120px;}
.layout-connect .list tr td.ip {width:120px;}
.layout-connect .list tr.array:hover {background-color:#f7fbfd;}
.layout-connect .list tr.array td.date {text-align:center; line-height:50px; font-size:13px; color:#959595; font-family:'Nanum Gothic',gulim,serif;}
.layout-connect .list tr.array td.nick {text-align:center; font-weight:bold; line-height:50px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-connect .list tr.array td.nick .member_thumb {position:relative; left:0; top:-4px; margin-right:5px; vertical-align:middle; display:inline-block; width:25px; height:25px;}
.layout-connect .list tr.array td.nick .member_thumb img {width:25px; height:25px; border-radius:25px;}
.layout-connect .list tr.array td.content {text-align:left; line-height:50px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-connect .list tr.array td.content a {text-align:left; line-height:50px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-connect .list tr.array td.ip {text-align:center; font-weight:bold; line-height:50px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-connect .list tr.array td p {margin:0px; text-align:left; overflow:hidden; height:50px; padding:0 10px; word-break:break-all;}
.layout-connect .list tr.array td p.center {text-align:center;}
.layout-connect .list tr.array .mobile {display:none;}
.layout-connect .list tr.not {text-align:center; font-weight:700; line-height:200px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-connect .list tr td.date .date2 {display:none;}

@media screen and (max-width:700px) {

.layout-connect .list tr td.date {width:70px;}
.layout-connect .list tr td.date .date1 {display:none;}
.layout-connect .list tr td.date .date2 {display:block;}

}

@media screen and (max-width:600px) {

.layout-connect .list tr.line {display:none;}
.layout-connect .list tr.title {display:none;}
.layout-connect .list tr.array td.nick {display:none;}
.layout-connect .list tr.array td.date {display:none;}
.layout-connect .list td.nick {display:none;}
.layout-connect .list td.ip {display:none;}
.layout-connect .list tr.array .pc {display:none;}
.layout-connect .list tr.array .mobile {position:relative; display:block; padding:13px 9px 12px 9px;}
.layout-connect .list tr.array td.content {font-weight:bold; line-height:18px; font-size:14px; color:#363636; font-family:gulim,serif;}
.layout-connect .list tr.array td.content a {font-weight:bold; line-height:18px; font-size:14px; color:#363636; font-family:gulim,serif;}
.layout-connect .list tr.array td p {height:18px; padding:0;}
.layout-connect .list tr.array span.sideview {line-height:13px; font-size:12px; color:#86a4b0; font-family:gulim,serif;}
.layout-connect .list tr.array span.date {line-height:13px; font-size:12px; color:#959595; font-family:gulim,serif;}
.layout-connect .list tr.array span.line {padding:0 6px 0 7px; line-height:13px; font-size:11px; color:#dadada; font-family:dotum,serif;}

}

@media screen and (max-width:550px) {

.layout-connect .menu div a {text-decoration:none; font-weight:bold; line-height:49px; font-size:14px; color:#999999; font-family:,gulim,serif;}

}
</style>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "현재 접속자";
include_once("$web[path]/_top.php");

$colspan = 4;

$sql_search = " where 1 ";

if ($m == 1) {

    $sql_search .= " and mid != '' ";

}

else if ($m == 2) {

    $sql_search .= " and mid = '' and robot = '0' ";

}

else if ($m == 3) {

    $sql_search .= " and robot = '1' ";

}

$chk2 = sql_fetch(" select count(id) as total_count from $web[connect_table] where mid != '' ");
$chk3 = sql_fetch(" select count(id) as total_count from $web[connect_table] where mid = '' and robot = '0' ");
$chk4 = sql_fetch(" select count(id) as total_count from $web[connect_table] where robot = '1' ");

$cnt = sql_fetch(" select count(*) as cnt from $web[connect_table] $sql_search ");
$total_count = $cnt['cnt'];
$rows = 20;
$total_page  = ceil($total_count / $rows);
if (!$p) { $p = 1; }
$from_record = ($p - 1) * $rows;
$paging = paging(5, $p, $total_page, "?m=".addslashes($m)."&amp;p=");
$result = sql_query(" select * from $web[connect_table] $sql_search order by id desc limit $from_record, $rows ");
$list = array();
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $mb = member($row['mid']);

    $list[$i] = $row;
    $list[$i]['thumb'] = member_thumb($list[$i]['mid']);
    $list[$i]['ip'] = preg_replace("/^([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$/", "\\1.\\2.\\3.***", $row['ip']);
    $list[$i]['date1'] = text_date("Y. m. d [H:i]", $list[$i]['loadtime'], "&nbsp;");

    if (date("Y-m-d", strtotime($list[$i]['loadtime'])) == $web['time_ymd']) {

        $list[$i]['date2'] = text_date("H:i", $list[$i]['loadtime'], "&nbsp;");

    } else {

        $list[$i]['date2'] = text_date("m.d", $list[$i]['loadtime'], "&nbsp;");

    }

    if ($mb['mid']) {

        $list[$i]['uid'] = $mb['uid'];
        $list[$i]['nick'] = $mb['nick'];

    }

    else if ($row['robot']) {

        $list[$i]['nick'] = "로봇";

    } else {

        $list[$i]['nick'] = "비회원";

    }

}
?>
<script type="text/javascript">
$(document).ready( function() {

    $('.layout-connect .menu div[name="<?=text($m)?>"]').addClass('on');

});
</script>
<div class="layout-block">
<div class="layout-connect">
<div class="menu">
<div name="0"><a href="<?=$web['host_member']?>/connect/">전체 (<span><?=$total_count?></span>)</a></div>
<div name="1"><a href="?m=1">회원 (<span><?=$chk2['total_count']?></span>)</a></div>
<div name="2"><a href="?m=2">비회원 (<span><?=$chk3['total_count']?></span>)</a></div>
<div name="3"><a href="?m=3">로봇 (<span><?=$chk4['total_count']?></span>)</a></div>
</div>
<div class="list">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr class="title">
    <td class="date">최종접속</td>
    <td class="nick">닉네임</td>
    <td class="content">현재위치</td>
    <td class="ip">아이피</td>
</tr>
<tr class="line"><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? for ($i=0; $i<count($list); $i++) { ?>
<tr class="array">
    <td class="date"><span class="date1"><?=$list[$i]['date1']?></span><span class="date2"><?=$list[$i]['date2']?></span></td>
    <td class="nick"><span class="member_thumb"><?=$list[$i]['thumb']?></span><span class="sideview" onclick="sideviewLoad(this, '<?=text($list[$i]['mid'])?>');" title="<?=text($list[$i]['nick'])?>"><?=text($list[$i]['nick'])?></span></td>
    <td class="content">
<div class="pc"><p><a href="<?=text($list[$i]['url']);?>" title="<?=text($list[$i]['title']);?>"><?=text($list[$i]['title']);?></a></p></div>
<div class="mobile">
<p><a href="<?=text($list[$i]['url']);?>" title="<?=text($list[$i]['title']);?>"><?=text($list[$i]['title']);?></a></p>
<span class="sideview" onclick="sideviewLoad(this, '<?=text($list[$i]['mid'])?>');" title="<?=text($list[$i]['nick'])?>"><?=text($list[$i]['nick'])?></span><span class="line">|</span><span class="date"><?=$list[$i]['date2']?></span>
</div>
    </td>
    <td class="ip"><?=text($list[$i]['ip']);?></td>
</tr>
<tr><td height="1" bgcolor="#f0f0f0" colspan="<?=$colspan?>"></td></tr>
<? } ?>
<? if (!$i) { ?>
<tr class="not"><td colspan="<?=$colspan?>">데이터가 없습니다.</td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? } ?>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
</table>
</div>
<div class="web-page">
<? if ($total_count && $total_count > $rows) { echo $paging; } ?>
</div>
</div>
</div>
<?
include_once("$web[path]/_bottom.php");
?>