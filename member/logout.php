<?php
include_once("./_tb.php");

session_unset();
session_destroy();

set_cookie("login_mid", "", 0);
set_cookie("login", "", 0);

if ($url) {
    $link = $url;
} else {
    $link = $web['host_default'];
}

url($link);
?>