<?php // 메뉴
if (!defined("_WEB_")) exit;
?>
<div class="member-menu">
<div name="info"><a href="<?=$web['host_member']?>/"><span class="icon1"></span>내 정보</a></div>
<div name="message"><a href="<?=$web['host_member']?>/message/"><span class="icon2"></span>메세지</a></div>
<div name="point"><a href="<?=$web['host_member']?>/point/"><span class="icon3"></span>포인트</a></div>
<div name="bbs"><a href="<?=$web['host_member']?>/bbs/"><span class="icon4"></span>내 게시물</a></div>
</div>