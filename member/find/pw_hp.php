<?php // 비번찾기
include_once("./_tb.php");
if ($check_login) { url($web['host_default'])."/"; }

if ($m == 'send') {

    if (!$setup['sms_onoff']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "SMS문자 발송 기능을 사용하지 않습니다.";
        exit;

    }

    $ss_name = "find_pw";
    if (!get_session($ss_name)) {

        echo "새로고침 후 다시 시도하세요.";
        exit;

    }

    if ($uid) { $uid = strtolower(trim($uid)); $uid = preg_match("/^[a-zA-Z0-9_\-]+$/", $uid) ? $uid : ""; }
    if ($hp) { $hp = str_replace("-", "", trim($hp)); $hp = preg_match("/^[0-9\-]+$/", $hp) ? $hp : ""; }
    if ($real_id) { $real_id = preg_match("/^[0-9]+$/", $real_id) ? $real_id : ""; }

    if (!$uid) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "아이디를 입력하세요.";
        exit;

    }

    if (!$hp) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "휴대폰 번호를 입력하세요.";
        exit;

    }

    $mb = sql_fetch(" select * from $web[member_table] where uid = '".addslashes($uid)."' and hp = '".addslashes($hp)."' and dropout = 0 limit 0, 1 ");

    if (!$mb['hp']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "홈페이지 가입 이력이 없는 아이디입니다.";
        exit;

    }

    if ($mb['dropout']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "탈퇴한 아이디입니다.";
        exit;

    }

    $chk = sql_fetch(" select count(id) as total_count from $web[real_hp_table] where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (1 * 86400))."' ");

    if ($chk['total_count'] >= $web['real_hp_count']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "1일 인증요청횟수를 초과하였습니다.";
        exit;

    }

    $chk = sql_fetch(" select * from $web[real_hp_table] where id = '".addslashes($real_id)."' limit 0, 1 ");

    if ($chk['id'] && $chk['hp'] != $hp || !$chk['id']) {

        $code = rand(1000,9999);

        $sql_common = "";
        $sql_common .= " set mid = '".addslashes($mb['mid'])."' ";
        $sql_common .= ", hp = '".addslashes($mb['hp'])."' ";
        $sql_common .= ", code = '".addslashes($code)."' ";
        $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[real_hp_table] $sql_common ");

        $real_id = sql_insert_id();

        $send_mid = $mb['mid'];

        $message = $setup['sms_real'];
        $message = str_replace("{인증코드}", $code, $message);
        $message = str_replace("{홈페이지명}", $setup['title'], $message);

        sms_send($mb['hp'], $setup['callernumber'], $message);

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "$('#real_id').val('".$real_id."');";
        echo "</script>";

        echo "<font color='#0d5c9b'>휴대폰으로 발송된 인증코드를 입력하세요.</font>";

        exit;

    } else {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "<font color='#0d5c9b'>휴대폰으로 발송된 인증코드를 입력하세요.</font>";
        exit;

    }

}

else if ($m == 'check') {

    $ss_name = "find_pw";
    if (!get_session($ss_name)) {

        echo "새로고침 후 다시 시도하세요.";
        exit;

    }

    if ($uid) { $uid = strtolower(trim($uid)); $uid = preg_match("/^[a-zA-Z0-9_\-]+$/", $uid) ? $uid : ""; }
    if ($hp) { $hp = str_replace("-", "", trim($hp)); $hp = preg_match("/^[0-9\-]+$/", $hp) ? $hp : ""; }
    if ($code) { $code = trim($code); $code = preg_match("/^[0-9]+$/", $code) ? $code : ""; }
    if ($real_id) { $real_id = preg_match("/^[0-9]+$/", $real_id) ? $real_id : ""; }

    if (!$uid) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "아이디를 입력하세요.";
        exit;

    }

    if (!$hp) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "휴대폰 번호를 입력하세요.";
        exit;

    }

    if (!$real_id) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "인증 코드가 발송되지 않았습니다.";
        exit;

    }

    if (!$code) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "인증 코드가 올바르지 않습니다. 다시 시도하세요.";
        exit;

    }

    $chk = sql_fetch(" select * from $web[real_hp_table] where id = '".addslashes($real_id)."' limit 0, 1 ");

    if (!$chk['id']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "인증 코드가 발송되지 않았습니다.";
        exit;

    }

    if ($chk['mode']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "인증 코드를 다시 발송하세요.";
        exit;

    }

    $mb = member($chk['mid']);

    if ($mb['uid'] != $uid) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "인증 코드를 다시 발송하세요.";
        exit;

    }

    if ($chk['hp'] != $hp) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "인증 코드를 다시 발송하세요.";
        exit;

    }

    if ($chk['code'] != $code) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "인증 코드가 올바르지 않습니다. 다시 시도하세요.";
        exit;

    }

    unset($_SESSION['find_pw_hp']);
    unset($_SESSION['find_pw_hp_result']);

    set_session("find_pw_hp", $chk['mid']);

    sql_query(" update $web[real_hp_table] set mode = '1' where id = '".addslashes($real_id)."' ");

    url("pw_hp_result.php");

    exit;

}

$ss_name = "find_pw";
if (!get_session($ss_name)) {

    set_session($ss_name, true);

}

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px; min-height:654px;}

.layout-find {width:460px; margin:0 auto; padding:50px 0;}

.layout-find .title {position:relative; margin:0 auto; margin-bottom:19px; width:162px; height:162px; background:url('<?=$web['host_member']?>/img/find_title_findpw.png') no-repeat;}
.layout-find .title a {position:absolute; right:0; bottom:0; display:block; width:48px; height:48px; background:url('<?=$web['host_member']?>/img/find_title_moveid.png') no-repeat;}
.layout-find .title a:hover {background-position:0 -48px;}

.layout-find .link {position:relative; text-align:right; height:24px; padding-right:16px;}
.layout-find .link a {line-height:15px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-find .link a:hover {color:#000000;}
.layout-find .link a .icon {position:absolute; right:0; top:1px; display:block; width:10px; height:12px; background:url('<?=$web['host_member']?>/img/_moveid.png') no-repeat;}
.layout-find .link a:hover .icon {background-position:0 -12px;}

.layout-find .tab {height:51px;}
.layout-find .tab a {text-decoration:none; text-align:center; display:inline-block; width:228px; height:49px; background-color:#eff0f2; border:1px solid #e0e1e3;}
.layout-find .tab a {font-weight:bold; line-height:49px; font-size:14px; color:#999999; font-family:gulim,serif;}
.layout-find .tab a.on {background-color:#485362; border:1px solid #414b58; color:#ffffff;}

.layout-find .uid {margin-top:10px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_id.png') no-repeat left 0; background-color:#ffffff;}
.layout-find .uid input {margin-left:40px; width:418px; height:49px; border:0px;}
.layout-find .uid input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-find .uid.on {border:1px solid #1192ca; background-position:0 -49px;}
.layout-find .uid.on input {color:#000000;}
.layout-find .uid span {position:absolute; left:40px; top:0px; display:block; width:100px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}

.layout-find .hp {margin-top:10px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_hp.png') no-repeat left 0; background-color:#ffffff;}
.layout-find .hp input {margin-left:40px; width:418px; height:49px; border:0px;}
.layout-find .hp input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-find .hp.on {border:1px solid #1192ca; background-position:0 -49px;}
.layout-find .hp.on input {color:#000000;}
.layout-find .hp span {position:absolute; left:40px; top:0px; display:block; width:100px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-find .hp .send {position:absolute; right:0px; top:0px; display:block; width:124px; height:49px; background:url('<?=$web['host_member']?>/img/_sandcode.png') no-repeat; cursor:pointer;}
.layout-find .hp .send:hover {background-position:0 -49px;}

.layout-find .code {margin-top:10px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_code.png') no-repeat left 0; background-color:#ffffff;}
.layout-find .code input {margin-left:40px; width:418px; height:49px; border:0px;}
.layout-find .code input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-find .code.on {border:1px solid #1192ca; background-position:0 -49px;}
.layout-find .code.on input {color:#000000;}
.layout-find .code span {position:absolute; left:40px; top:0px; display:block; width:100px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}

.layout-find .msg {display:none; text-align:center; padding-top:30px;}
.layout-find .msg {font-weight:bold; line-height:18px; font-size:13px; color:#f26d7d; font-family:gulim,serif;}

.layout-find .submit {cursor:pointer; margin-top:30px; height:59px; border:1px solid #1192ca; background-color:#42abd7;}
.layout-find .submit {font-weight:700; line-height:59px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-find .submit span {display:block; text-align:center;}

.layout-find .ipbox {margin-top:20px; position:relative; height:20px; padding-left:29px;}
.layout-find .ipbox span.ip {position:absolute; right:0; top:0; display:block; width:180px; height:20px; text-align:right; vertical-align:top;}
.layout-find .ipbox span.ip {line-height:20px; font-size:11px; color:#999999; font-family:Arial,gulim,serif;}
.layout-find .ipbox span.ip .icon {margin-right:7px; display:inline-block; width:20px; height:20px; background:url('<?=$web['host_member']?>/img/_ip.gif') no-repeat;}
.layout-find .ipbox span.ip .text {position:relative; overflow:hidden; left:0px; top:-7px;}

.layout-find .menu {padding-top:19px; text-align:center; line-height:0px; margin-top:50px; border-top:1px solid #dadada;}
.layout-find .menu a {line-height:14px; font-size:12px; color:#999999; font-family:gulim,serif;}
.layout-find .menu a:hover {color:#000000;}
.layout-find .menu .line {padding:0 4px; line-height:14px; font-size:12px; color:#dadada; font-family:gulim,serif;}

@media screen and (max-width:600px) {

.layout-find {width:320px;}

.layout-find .tab a {width:158px;}
.layout-find .hp input {width:278px;}
.layout-find .code input {width:278px;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    $('#uid, #hp, #code').focus(function() {

        $(this).parent().addClass('on');
        $('#msg').hide();

    }).blur(function() {

        $(this).parent().removeClass('on');
        $('#msg').hide();

        if ($(this).val() == '') {

            $(this).parent().find('span').show();

        }

    }).keydown(function() {

        $(this).parent().find('span').hide();

    });

    $('.uid span, .hp span, .code span').click(function() {

        $(this).parent().find('input').focus();

    });

    $('.layout-find .hp .send').click(function() {

        findSend();

    });

    $('#uid, #hp, #code, .layout-find .submit span').keyup(function(e) {

        if (e.keyCode == 13) {

            findCheck();

        }

    });

    $('.layout-find .submit span').click(function() {

        findCheck();

    });

});

function findSend()
{

    var uid = $('#uid');
    var hp = $('#hp');
    var real_id = $('#real_id');

    $.post("pw_hp.php", {"m" : "send", "uid" : uid.val(), "hp" : hp.val(), "real_id" : real_id.val()}, function(data) {

        $("#msg").html(data);

    });

}

function findCheck()
{

    var uid = $('#uid');
    var hp = $('#hp');
    var code = $('#code');
    var real_id = $('#real_id');

    $.post("pw_hp.php", {"m" : "check", "uid" : uid.val(), "hp" : hp.val(), "code" : code.val(), "real_id" : real_id.val()}, function(data) {

        $("#msg").html(data);

    });

}
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("$web[path]/_top.php");
?>
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" id="url" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" id="real_id" value="" />
<div class="layout-find">
<div class="title"><a href="<?=$web['host_member']?>/find/id_hp.php"></a></div>
<div class="link"><a href="<?=$web['host_member']?>/find/id_hp.php">아이디 찾기<span class="icon"></span></a></div>
<div class="tab"><a href="<?=$web['host_member']?>/find/pw_hp.php" class="on">휴대폰 번호로 찾기</a><a href="<?=$web['host_member']?>/find/pw_email.php">이메일 주소로 찾기</a></div>
<div class="uid"><input type="text" id="uid" name="uid" value="" tabindex="1" /><span>아이디</span></div>
<div class="hp"><input type="text" id="hp" name="hp" value="" tabindex="2" /><span>휴대폰 번호</span><div class="send" tabindex="3"></div></div>
<div class="code"><input type="text" id="code" name="code" value="" tabindex="4" /><span>인증 코드</span></div>
<div class="msg" id="msg"></div>
<div class="submit"><span tabindex="5">비밀번호 찾기</span></div>
<div class="ipbox">
<span class="ip"><span class="icon"></span><span class="text">Access IP : <?=text($_SERVER['REMOTE_ADDR'])?></span></span>
</div>
<div class="menu">
<a href="<?=$web['host_member']?>/login/">로그인</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/id_hp.php">아이디 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/pw_hp.php">비밀번호 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/join/">회원가입</a>
</div>
</div>
</form>
<?
include_once("$web[path]/_bottom.php");
?>