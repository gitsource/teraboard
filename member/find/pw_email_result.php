<?php // 아이디찾기
include_once("./_tb.php");
if ($check_login) { url($web['host_default'])."/"; }

$setup_email_upw = setup_email("upw");

$mid = get_session("find_pw_email");

if (!$mid) {

    message("<p class='title'>알림</p><p class='text'>이메일 인증이 완료되지 않았습니다.</p>", "b");

}

$mb = member($mid);

if (!$mb['mid']) {

    message("<p class='title'>알림</p><p class='text'>이메일 인증이 완료되지 않았습니다.</p>", "b");

}

if ($m == 'check') {

    $ss_name = "find_pw_email_result";
    if (get_session($ss_name)) {

        echo "<script type='text/javascript'>";
        echo "$('.layout-find .submit').addClass('on');";
        echo "$('.layout-find .submit span').text('이메일이 발송되었습니다.');";
        echo "</script>";

    }

    exit;

}

else if ($m == 'send') {

    $ss_name = "find_pw_email_result";
    if (!get_session($ss_name)) {

        set_session($ss_name, true);

        $upw = substr(md5(rand(100000,999999)),0,6);

        sql_query(" update $web[member_table] set upw = '".sql_password($upw)."' where mid = '".$mb['mid']."' ");

        if (!$setup['title']) {

            $setup['title'] = $web['host_default'];

        }

        $title = $setup_email_upw['title'];
        $title = str_replace("{임시PW}", $upw, $title);
        $title = str_replace("{닉네임}", $mb['nick'], $title);
        $title = str_replace("{회원ID}", $mb['uid'], $title);
        $title = str_replace("{성명}", $mb['name'], $title);
        $title = str_replace("{가입일}", text_date("Y년 m월 d일", $mb['datetime'], ""), $title);
        $title = str_replace("{홈페이지명}", $setup['title'], $title);
        $title = str_replace("{도메인주소}", $web['host_default'], $title);
        $title = str_replace("{회사명}", $setup['company'], $title);
        $title = str_replace("{대표자명}", $setup['ceo'], $title);
        $title = str_replace("{대표메일}", $setup['email'], $title);
        $title = str_replace("{대표번호}", $setup['tel'], $title);
        $title = str_replace("{회사주소}", $setup['addr'], $title);

        $content = $setup_email_upw['content'];
        $content = str_replace("{임시PW}", $upw, $content);
        $content = str_replace("{닉네임}", $mb['nick'], $content);
        $content = str_replace("{회원ID}", $mb['uid'], $content);
        $content = str_replace("{성명}", $mb['name'], $content);
        $content = str_replace("{가입일}", text_date("Y년 m월 d일", $mb['datetime'], ""), $content);
        $content = str_replace("{홈페이지명}", $setup['content'], $content);
        $content = str_replace("{도메인주소}", $web['host_default'], $content);
        $content = str_replace("{회사명}", $setup['company'], $content);
        $content = str_replace("{대표자명}", $setup['ceo'], $content);
        $content = str_replace("{대표메일}", $setup['email'], $content);
        $content = str_replace("{대표번호}", $setup['tel'], $content);
        $content = str_replace("{회사주소}", $setup['addr'], $content);

        $sql_common = "";
        $sql_common .= " set mid = '".$mb['mid']."' ";
        $sql_common .= ", name = '".addslashes($setup['title'])."' ";
        $sql_common .= ", send_email = '".addslashes($setup['email'])."' ";
        $sql_common .= ", email = '".addslashes($mb['email'])."' ";
        $sql_common .= ", title = '".$title."' ";
        $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[email_table] $sql_common ");

        email_send($mb['email'], $title, text2(trim($content), 1), $setup['title'], $setup['email'], 1);

        echo "<script type='text/javascript'>";
        echo "$('.layout-find .submit').addClass('on');";
        echo "$('.layout-find .submit span').text('이메일이 발송되었습니다.');";
        echo "</script>";

    }

    exit;

}

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px; min-height:654px;}

.layout-find {width:460px; margin:0 auto; padding:50px 0;}

.layout-find .title {position:relative; margin:0 auto; margin-bottom:19px; width:162px; height:162px; background:url('<?=$web['host_member']?>/img/find_title_findpw.png') no-repeat;}
.layout-find .title a {position:absolute; right:0; bottom:0; display:block; width:48px; height:48px; background:url('<?=$web['host_member']?>/img/find_title_moveid.png') no-repeat;}
.layout-find .title a:hover {background-position:0 -48px;}

.layout-find .link {position:relative; text-align:right; height:24px; padding-right:16px;}
.layout-find .link a {line-height:15px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-find .link a:hover {color:#000000;}
.layout-find .link a .icon {position:absolute; right:0; top:1px; display:block; width:10px; height:12px; background:url('<?=$web['host_member']?>/img/_moveid.png') no-repeat;}
.layout-find .link a:hover .icon {background-position:0 -12px;}

.layout-find .msg {text-align:center; padding-top:67px; border-top:1px solid #dadada;}
.layout-find .msg p {margin:0px;}
.layout-find .msg p.text {font-weight:700; line-height:36px; font-size:36px; color:#333333; font-family:'Nanum Gothic',gulim,serif;}
.layout-find .msg p.text2 {margin-top:25px; font-weight:bold; line-height:22px; font-size:13px; color:#858a8d; font-family:gulim,serif;}
.layout-find .msg p.text3 {margin-top:20px; line-height:23px; font-size:13px; color:#999999; font-family:gulim,serif;}

.layout-find .submit {display:none; cursor:pointer; margin-top:44px; height:59px; border:1px solid #1192ca; background-color:#42abd7;}
.layout-find .submit {font-weight:700; line-height:59px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-find .submit span {display:block; text-align:center;}
.layout-find .submit.on {cursor:default; border:1px solid #b0b0b0; background-color:#f5f6f7; color:#999999;}

.layout-find .menu {padding-top:19px; text-align:center; line-height:0px; margin-top:50px; border-top:1px solid #dadada;}
.layout-find .menu a {line-height:14px; font-size:12px; color:#999999; font-family:gulim,serif;}
.layout-find .menu a:hover {color:#000000;}
.layout-find .menu .line {padding:0 4px; line-height:14px; font-size:12px; color:#dadada; font-family:gulim,serif;}

@media screen and (max-width:600px) {

.layout-find {width:320px;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    $('.layout-find .submit span').click(function() {

        findSend();

    });

    $.post("pw_email_result.php", {"m" : "check"}, function(data) {

        $("#update_data").html(data);

        $('.layout-find .submit').show();

    });

});

function findSend()
{

    $.post("pw_email_result.php", {"m" : "send"}, function(data) {

        $("#update_data").html(data);

    });

}
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("$web[path]/_top.php");
?>
<form method="post" name="formSetup">
<input type="hidden" id="url" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<div class="layout-find">
<div class="title"><a href="<?=$web['host_member']?>/find/id_hp.php"></a></div>
<div class="link"><a href="<?=$web['host_member']?>/find/id_hp.php">아이디 찾기<span class="icon"></span></a></div>
<div class="msg">
<p class="text">임시 비밀번호를 발송합니다.</p>
<p class="text2">생성된 임시 비밀번호를 받기 위해<br />아래의 버튼을 클릭하세요.</p>
<p class="text3">임시 생성된 비밀번호를 통해 로그인 후, 홈페이지의 회원정보 수정 메뉴에서 비밀번호를 교체하시기 바랍니다. 감사합니다.</p>
</div>
<div class="submit"><span tabindex="1">이메일 받기</span></div>
<div class="menu">
<a href="<?=$web['host_member']?>/login/">로그인</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/id_hp.php">아이디 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/pw_hp.php">비밀번호 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/join/">회원가입</a>
</div>
</div>
</form>
<?
include_once("$web[path]/_bottom.php");
?>