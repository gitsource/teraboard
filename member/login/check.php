<?php // post 로그인
include_once("./_tb.php");
if ($check_login) { url($web['host_default'])."/"; }

$uid = strtolower(trim($uid));
$upw = trim($_POST['upw']);
$ip = trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])));

if ($autologin) { $autologin = preg_match("/^[0-9]+$/", $autologin) ? $autologin : ""; }

if (!$uid) {

    message("<p class='title'>알림</p><p class='text'>회원 아이디가 입력되지 않았습니다.</p>", "b");

}

if (!$upw) {

    message("<p class='title'>알림</p><p class='text'>회원 패스워드가 입력되지 않았습니다.</p>", "b");

}

$mb = member_uid($uid);

if (!$mb['uid']) {

    message("<p class='title'>알림</p><p class='text'>입력하신 아이디는 등록되지 않은 아이디 입니다.</p>", "b");

}

$sql_common = "";
$sql_common .= " set mid = '".$mb['mid']."' ";
$sql_common .= ", ip = '".$ip."' ";
$sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
$sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
$sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

sql_query(" insert into $web[member_login_table] $sql_common ");

$login_id = sql_insert_id();

if (!$mb['upw']) {

    message("<p class='title'>알림</p><p class='text'>비밀번호 찾기를 이용하여, 새로 비밀번호를 발급하셔야합니다.</p>", "b");

}

if (sql_password($upw) != $mb['upw']) {

    function sql_old_password($value) {

        $row = sql_fetch(" select old_password('".addslashes($value)."') as pass ");

        return $row['pass'];

    }

    if (sql_old_password($upw) == $mb['upw']) {

        sql_query(" update $web[member_table] set upw = '".sql_password($upw)."' where mid = '".$mb['mid']."' ");

    } else {

        message("<p class='title'>알림</p><p class='text'>입력하신 비밀번호가 올바르지 않습니다.</p>", "b");

    }

}

if ($mb['dropout']) {

    message("<p class='title'>알림</p><p class='text'>탈퇴한 아이디입니다.</p>", "b");

}

if ($mb['stop']) {

    message("<p class='title'>알림</p><p class='text'>정지된 아이디입니다.</p>", "b");

}

set_session('ss_mid', $mb['mid'].'|'.$_SERVER['REMOTE_ADDR'].'|'.md5($_SERVER['HTTP_USER_AGENT'].$mb['datetime']));

if ($autologin) {

    $cookie_key = md5($_SERVER['HTTP_USER_AGENT'].$mb['upw'].$mb['datetime']);
    set_cookie("login_mid", $mb['mid'], (int)(86400 * 365));
    set_cookie("login", $cookie_key, (int)(86400 * 365));

}

if ($login_id) {

    sql_query(" update $web[member_login_table] set login = 1 where id = '".$login_id."' ");

}

if ($url) {

    $urlencode = urldecode($url);

} else {

    $urlencode = urldecode($_SERVER[REQUEST_URI]);

}

if ($url) {

    $link = $urlencode;

} else {

    $link = $web['host_member']."/";

}

url($link);
?>