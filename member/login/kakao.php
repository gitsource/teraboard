<?php // 카카오
include_once("./_tb.php");
include_once("$web[path]/other/curl/Curl.php");
echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";
if ($code) { $code = preg_match("/^[a-zA-Z0-9_\-\/\.\#]+$/", $code) ? $code : ""; }
if ($callback) { $callback = preg_match("/^[a-zA-Z0-9_\-]+$/", $callback) ? $callback : ""; }
if ($loginmode) { $loginmode = preg_match("/^[a-zA-Z0-9_\-]+$/", $loginmode) ? $loginmode : ""; }

if ($loginmode == 'document') {

    $message_mode = "b";

} else {

    $message_mode = "c";

}

$setup_join = setup_join();

if (!$setup_api['social_login_onoff'] || !$setup_api['login_kakao']) {

    message("<p class='title'>알림</p><p class='text'>소셜 로그인이 미사용중입니다.</p>", $message_mode);

}

if ($m == 'refresh_token') {

    if ($callback == 'hidden') {

        $kakao_refresh_token = get_session("kakao_refresh_token");

        if (!$kakao_refresh_token) {

            message("<p class='title'>알림</p><p class='text'>새로 로그인하여주시기 바랍니다.</p>", "", "", false, true);

        }

        $curl = new Curl();
        $curl->post('https://kauth.kakao.com/oauth/token', array(
            'client_id' => addslashes($setup_api['login_kakao_key']),
            'refresh_token' => addslashes($kakao_refresh_token),
            'grant_type' => 'refresh_token',
        ));

        if ($curl->response->error) {

            message("<p class='title'>알림</p><p class='text'>오류 : ".text($curl->response->error)."<br />오류내용 : ".text($curl->response->error_description)."</p>", "", "", false, true);

        }

        set_session("kakao_access_token", $curl->response->access_token);

        exit;

    }

}

if ($check_login) {

    message("<p class='title'>알림</p><p class='text'>이미 로그인 중입니다.</p>", $message_mode);

}

if ($code) {

    $curl = new Curl();
    $curl->post('https://kauth.kakao.com/oauth/token', array(
        'client_id' => addslashes($setup_api['login_kakao_key']),
        'code' => addslashes($code),
        'redirect_uri' => addslashes($web['host_member']."/login/kakao.php"),
        'grant_type' => 'authorization_code',
    ));

    if ($curl->response->error) {

        message("<p class='title'>알림</p><p class='text'>오류 : ".text($curl->response->error)."<br />오류내용 : ".text($curl->response->error_description)."</p>", $message_mode);

    }

    set_session("kakao_access_token", $curl->response->access_token);
    set_session("kakao_refresh_token", $curl->response->refresh_token);

    url("kakao.php");

}

else if (get_session("kakao_access_token") && $m != 're') {

    $curl = new Curl();
    $curl->setHeader('Authorization', "Bearer ".addslashes(get_session("kakao_access_token")));
    $curl->get('https://kapi.kakao.com/v1/user/me');

    if (!$curl->response->id) {

        message("<p class='title'>알림</p><p class='text'>오류코드 : ".text($curl->response->code)."<br />오류내용 : ".text($curl->response->msg)."</p>", $message_mode);

    }

    $social_key = addslashes($curl->response->id);
    $nick = addslashes($curl->response->properties->nickname);
    $photo = addslashes($curl->response->properties->profile_image);
    $uid = "tbk".(int)($setup_api['login_kakao_count'] + 1);

    if (!$social_key) {

        message("<p class='title'>알림</p><p class='text'>오류가 발생하였습니다. 다시 시도하여주시기 바랍니다.</p>", $message_mode);

    }

    if (!$nick) {

        $nick = $uid;

    }

    $check_blocknick = false;
    $row = explode("|", $setup['block_id']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] == $nick) {

            $check_blocknick = true;

        }

    }

    if ($check_blocknick) {

        $nick = $uid;

    }

    $chk = sql_fetch(" select * from $web[member_table] where social = 2 and social_key = '".$social_key."' limit 0, 1 ");

    if ($chk['mid']) {

        if ($chk['dropout']) {

            message("<p class='title'>알림</p><p class='text'>탈퇴한 아이디입니다.</p>", $message_mode);

        }

        if ($chk['stop']) {

            message("<p class='title'>알림</p><p class='text'>정지된 아이디입니다.</p>", $message_mode);

        }

        $sql_common = "";
        $sql_common .= " set mid = '".$chk['mid']."' ";
        $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
        $sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
        $sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
        $sql_common .= ", login = '1' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[member_login_table] $sql_common ");

        set_session('ss_mid', $chk['mid'].'|'.$_SERVER['REMOTE_ADDR'].'|'.md5($_SERVER['HTTP_USER_AGENT'].$chk['datetime']));

        if (get_session('loginmode') == 'document') {

            $url = get_session('redirect_uri');

            if ($url) {

                url(urldecode($url));

            } else {

                url($web['host_member']."/");

            }

        } else {

            echo "<script type='text/javascript'>";
            if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
            echo "opener.loginOk();";
            echo "window.close();";
            echo "</script>";

        }

        exit;

    }

    $chk = sql_fetch(" select mid from $web[member_table] where uid = '".$uid."' limit 0, 1 ");

    if ($chk['mid']) {

        $sql_common = "";
        $sql_common .= " set login_kakao_count = login_kakao_count + 1 ";

        sql_query(" update $web[setup_api_table] $sql_common ");

        message("<p class='title'>알림</p><p class='text'>장애가 발생하였습니다. 다시 시도하여주시기 바랍니다.</p>", $message_mode);

    }

    $chk = sql_fetch(" select mid from $web[member_table] where nick = '".$nick."' limit 0, 1 ");

    if ($chk['mid']) {

        $nick = $nick.rand(10,99);

        $chk = sql_fetch(" select mid from $web[member_table] where nick = '".$nick."' limit 0, 1 ");

        if ($chk['mid']) {

            message("<p class='title'>알림</p><p class='text'>장애가 발생하였습니다. 다시 시도하여주시기 바랍니다.</p>", $message_mode);

        }

    }

    $sql_common = "";
    $sql_common .= " set login_kakao_count = login_kakao_count + 1 ";

    sql_query(" update $web[setup_api_table] $sql_common ");

    $sql_common = "";
    $sql_common .= " set uid = '".$uid."' ";
    $sql_common .= ", social_key = '".$social_key."' ";
    $sql_common .= ", social = '2' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", level = '2' ";
    $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";
    $sql_common .= ", form_check = '".addslashes(substr(md5($_SERVER['REMOTE_ADDR'].date("His", $web['server_time']).rand(100000,999999)),0,30))."' ";

    sql_query(" insert into $web[member_table] $sql_common ");

    $mid = sql_insert_id();

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
    $sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", login = '1' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[member_login_table] $sql_common ");

    set_session('ss_mid', $mid.'|'.$_SERVER['REMOTE_ADDR'].'|'.md5($_SERVER['HTTP_USER_AGENT'].$web['time_ymdhis']));

    if ($setup['point_join']) {

        member_point($mid, $setup['point_join'], 1, "회원가입", $web['server_time'].rand(10000,99999));

    }

    if ($setup['image_onoff'] && $photo) {

        $dir = $disk['path']."/photo/".data_path("", "");

        @mkdir($dir, 0707);
        @chmod($dir, 0707);

        $source = $photo;

        if (preg_match("/\.(jp[e]?g|gif|png)$/i", $source)) {

            $filename = $uid.addslashes(substr(md5(str_replace("%", "", urlencode($source))), 0, 10).image_filetype($source));
            $target = $dir.'/'.$filename;

            $source = image_save($source, $filename, 1);

            if (!file_exists($target) && preg_match("/\.(jp[e]?g|gif|png)$/i", $source)) {

                image_thumb(200, 200, $source, $target, true);

                $upload = array();
                $upload['image'] = @getimagesize($target);
                $upload['size'] = @filesize($target);

                $sql_common = "";
                $sql_common .= " set mid = '".$mid."' ";
                $sql_common .= ", onoff = 1 ";
                $sql_common .= ", upload_source = '".$filename."' ";
                $sql_common .= ", upload_file = '".$filename."' ";
                $sql_common .= ", upload_filesize = '".$upload['size']."' ";
                $sql_common .= ", upload_width = '".$upload['image'][0]."' ";
                $sql_common .= ", upload_height = '".$upload['image'][1]."' ";
                $sql_common .= ", upload_time = '".$web['time_ymdhis']."' ";

                sql_query(" insert into $web[member_photo_table] $sql_common ");

                @unlink($source);

            }

        }

    }

    if (get_session('loginmode') == 'document') {

        $url = get_session('redirect_uri');

        if ($url) {

            url(urldecode($url));

        } else {

            url($web['host_member']."/");

        }

    } else {

        echo "<script type='text/javascript'>";
        if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
        echo "opener.loginOk();";
        echo "window.close();";
        echo "</script>";

    }

    exit;

} else {

    unset($_SESSION['ss_mid']);
    unset($_SESSION['loginmode']);
    unset($_SESSION['redirect_uri']);

    if ($url) {

        set_session("loginmode", addslashes($loginmode));
        set_session("redirect_uri", addslashes($url));

    }

    set_cookie("login_mid", "", 0);
    set_cookie("login", "", 0);

    $mt = microtime();
    $rand = mt_rand();
    $state = md5($mt . $rand);

    $url = "";
    $url .= "https://kauth.kakao.com/oauth/authorize";
    $url .= "?redirect_uri=".urlencode($web['host_member']."/login/kakao.php");
    $url .= "&response_type=code";
    $url .= "&client_id=".addslashes($setup_api['login_kakao_key']);
    $url .= "&state=".$state;

    url($url);

}
?>