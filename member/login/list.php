<?php
include_once("./_tb.php");

if (!$mid) {

    $mid = $member['mid'];

}

$mb = member($mid);

if (!$mb['mid']) {

    message("<p class='title'>알림</p><p class='text'>회원이 존재하지 않습니다.</p>", "c");

}

if ($member['mid'] != $mid && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

if ($member['mid'] != $mid && $check_admin) {

    $check_auth = check_auth("member", 1);

    if ($check_auth != 'full') {

        message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

    }

}

$web['title'] = "로그인 기록";

$sql_search = " where mid = '".$mid."' ";
$cnt = sql_fetch(" select count(*) as cnt from $web[member_login_table] $sql_search ");
$total_count = $cnt['cnt'];
$rows = 10;
$total_page  = ceil($total_count / $rows);
if (!$p) { $p = 1; }
$from_record = ($p - 1) * $rows;
$paging = paging(5, $p, $total_page, "?mid=$mid&p=");
$result = sql_query(" select * from $web[member_login_table] $sql_search order by id desc limit $from_record, $rows ");

$colspan = 5;
?>
<!DOCTYPE html>
<html>
<head>
<?
include_once("$web[path]/_head.php");
?>
<style type="text/css">
html {overflow:hidden;}
</style>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div id="pop_wrap" class="layout-popup">
<div class="title">
<img src="<?=$web['host_img']?>/popup_arrow.png">로그인 기록<div class="close"><a href="#" onclick="window.close(); return false;" title="닫기"><img src="<?=$web['host_img']?>/popup_close.png"></a></div>
</div>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="list-array">
<colgroup>
    <col width="150">
    <col width="">
    <col width="80">
    <col width="80">
    <col width="80">
</colgroup>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#e8e8e8"></td></tr>
<tr class="subj">
    <td>일시</td>
    <td>아이피</td>
    <td>브라우저</td>
    <td>OS</td>
    <td>로그인</td>
</tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#e8e8e8"></td></tr>
<?
for ($i=0; $row=sql_fetch_array($result); $i++) {
    
    $mb = member($row['mid']);
?>
<tr class="array">
    <td><?=$row['datetime']?></td>
    <td><?=text($row['ip'])?></td>
    <td><?=text($row['browser'])?></td>
    <td><?=text($row['os'])?></td>
    <td>
<?
if ($row['login']) {

    echo "성공";

} else {

    echo "<font color='#ff0000'>실패</font>";

}
?>
    </td>
</tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#e8e8e8"></td></tr>
<? } ?>
<? if (!$i) { ?>
<tr class="not"><td colspan="<?=$colspan?>">데이터가 없습니다.</td></tr>
<tr><td bgcolor="#e8e8e8" colspan="<?=$colspan?>" height="1"></td></tr>
<? } ?>
</table>
<? if ($total_count && $total_count > $rows) { ?>
<div class="web-page"><?=$paging;?></div>
<? } ?>
<table border="0" cellspacing="0" cellpadding="0" class="auto btn">
<tr>
    <td><a href="#" onclick="window.close(); return false;">닫기</a></td>
</tr>
</table>
</div>
</body>
</html>