<?php // 관리자 로그인
include_once("./_tb.php");
if ($check_login) { url($web['host_adm'])."/"; }

if ($m == 'check') {

    $uid = strtolower(trim($uid));
    $upw = trim($_POST['upw']);
    $ip = trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])));

    if (!$uid) {

        echo "<script type='text/javascript'>";
        echo "$('.talk').show();";
        echo "</script>";

        echo "회원 아이디가 입력되지 않았습니다.";
        exit;

    }

    if (!$upw) {

        echo "<script type='text/javascript'>";
        echo "$('.talk').show();";
        echo "</script>";

        echo "회원 패스워드가 입력되지 않았습니다.";
        exit;

    }

    $mb = member_uid($uid);

    if (!$mb['uid']) {

        echo "<script type='text/javascript'>";
        echo "$('.talk').show();";
        echo "</script>";

        echo "등록되지 않은 아이디입니다.";
        exit;

    }

    $sql_common = "";
    $sql_common .= " set mid = '".$mb['mid']."' ";
    $sql_common .= ", ip = '".$ip."' ";
    $sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[member_login_table] $sql_common ");

    $login_id = sql_insert_id();

    if (sql_password($upw) != $mb['upw']) {

        function sql_old_password($value) {

            $row = sql_fetch(" select old_password('".addslashes($value)."') as pass ");

            return $row['pass'];

        }

        if (sql_old_password($upw) == $mb['upw']) {

            sql_query(" update $web[member_table] set upw = '".sql_password($upw)."' where mid = '".$mb['mid']."' ");

        } else {

            echo "<script type='text/javascript'>";
            echo "$('.talk').show();";
            echo "</script>";

            echo "입력하신 비밀번호가 올바르지 않습니다.";
            exit;

        }

    }

    if ($mb['dropout']) {

        echo "<script type='text/javascript'>";
        echo "$('.talk').show();";
        echo "</script>";

        echo "탈퇴한 아이디입니다.";
        exit;

    }

    if ($mb['stop']) {

        echo "<script type='text/javascript'>";
        echo "$('.talk').show();";
        echo "</script>";

        echo "정지된 아이디입니다.";
        exit;

    }

    set_session('ss_mid', $mb['mid'].'|'.$_SERVER['REMOTE_ADDR'].'|'.md5($_SERVER['HTTP_USER_AGENT'].$mb['datetime']));

    if ($login_id) {

        sql_query(" update $web[member_login_table] set login = 1 where id = '".$login_id."' ");

    }

    if ($url) {

        $urlencode = urldecode($url);

    } else {

        $urlencode = urldecode($_SERVER[REQUEST_URI]);

    }

    if ($url) {

        $link = $urlencode;

    } else {

        $link = $web['host_adm']."/";

    }

    url($link);

    exit;

}

else if ($m == 'ver') {

    include_once("$web[path]/other/curl/Curl.php");

    $curl = new Curl();
    $curl->get('http://download.teraboard.net/ver.php', array(
        'domain' => addslashes($web['host_default']),
        'ver' => addslashes($setup['ver']),
    ));

    if ($curl->response->resultcode != '00') {

        exit;

    }

    if ($curl->response->download_ver > $setup['ver']) {

        echo "<script type='text/javascript'>";
        echo "$('#upgrade').html('".text($curl->response->msg)."');";
        echo "$('.nav').slideDown(700);";
        echo "</script>";

        exit;

    }

    exit;

}
?>
<!DOCTYPE html>
<html>
<head>
<?
include_once("$web[path]/_head.php");
?>
<style type="text/css">
body {min-width:320px; min-height:650px; height:100%; background:url('<?=$web['host_member']?>/img/adm/bg_pattern.png') repeat;}

.nav {display:none; font-size:0px; z-index:9999999; position:absolute; width:100%; border-bottom:1px solid #50b1da;}
.nav .block {margin:0px; vertical-align:top; text-align:center; width:100%; min-height:58px; background-color:#384655; border-bottom:1px solid #0d6d97;}
.nav .block ul {line-height:0px; font-size:0px; display:table; margin:0 auto;}
.nav .block ul li {float:left; text-align:center;}
.nav .block ul li.text {margin-top:18px; font-weight:700; line-height:20px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.nav .block ul li.upgrade {margin:12px 0 0 20px;}
.nav .block ul li.upgrade a {display:block; vertical-align:top; width:120px; height:34px; background:url('<?=$web['host_member']?>/img/adm/upgrade.png') no-repeat;}
.nav .block ul li.upgrade a:hover {background-position:0 -34px;}
.nav .block ul li.upgrade a:active {background-position:0 -68px;}

.wrap {position:relative; display:table; margin:0 auto; width:100%; height:100%;}
.conts {display:table-cell; vertical-align:middle; margin:0 auto; background:url('<?=$web['host_member']?>/img/adm/bg_light.png') no-repeat center center;}
.conts .block {width:252px; margin:0 auto;}
.logo {width:252px; height:190px; background:url('<?=$web['host_member']?>/img/adm/logo.png') no-repeat;}
.logo.on {background-position:0 -190px;}
.logo.on2 {background-position:0 -380px;}
.title {width:252px; height:60px; background:url('<?=$web['host_member']?>/img/adm/title.png') no-repeat;}
.uid,
.upw {position:relative; width:252px; height:47px; background:url('<?=$web['host_member']?>/img/adm/idpwbox.png') no-repeat;}
.upw {background-position:0 -47px;}
.uid.on {background-position:-252px 0;}
.upw.on {background-position:-252px -47px;}
.uid input {margin:4px 0 0 50px; width:180px; height:36px; border:0px; background:transparent;}
.uid input {font-weight:700; line-height:36px; font-size:24px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.upw input {margin:3px 0 0 50px; width:138px; height:36px; border:0px; background:transparent;}
.upw input {font-weight:700; line-height:36px; font-size:24px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.upw .submit {position:absolute; right:0; top:0; display:none; width:65px; height:47px; background:url('<?=$web['host_member']?>/img/adm/enter.png') no-repeat;}
.upw .submit:hover {background-position:0 -47px;}
.upw .submit:active {background-position:0 -94px;}
.upw .talk {display:none; position:absolute; left:0; top:36px; width:252px; height:50px; background:url('<?=$web['host_member']?>/img/adm/talk.png') no-repeat;}
.upw .talk {font-weight:700; line-height:13px; font-size:13px; color:#f26d7d; font-family:'Nanum Gothic',gulim,serif;}
.upw .talk span {display:block; margin:23px 0 0 17px;}

.user {margin-top:33px;}
.user p {margin:0px; padding-left:40px;}
.user p {font-weight:700; line-height:25px; font-size:14px; color:#e1e1e1; font-family:'Nanum Gothic',gulim,serif;}
.user p span.name {color:#1192ca; width:75px; display:inline-block;}

.ver {margin-top:123px; border-top:1px solid #50b1da; text-align:center; padding-top:15px;}
.ver {font-weight:700; line-height:14px; font-size:14px; color:#73c3e4; font-family:'Nanum Gothic',gulim,serif;}

@media screen and (max-width:600px) {

.nav .block ul li {clear:both; display:block; width:100%;}
.nav .block ul li.text {margin-top:0;}
.nav .block ul li.upgrade {margin:12px 0 20px 0; text-align:center;}
.nav .block ul li.upgrade a {margin:0 auto;}
.wrap {padding-top:145px;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    // 브라우저 체크
    if (browserCheck()) {
        return false;
    }

    var w = screen.width;
    var h = screen.height;

    if (w >= 1280 && h >= 768) {

        $('#screen').html(w+' x '+h);

    } else {

        $('#screen').html("<font color='#ff9668'>"+w+" x "+h+"</font>");

    }

    $('#uid, #upw').focus(function() {

        $(this).parent().addClass('on');
        $('.talk').hide();

        if ($(this).attr('id') == 'uid') {

            $('.logo').removeClass('on2').addClass('on');

        } else {

            $('.logo').removeClass('on').addClass('on2');

        }

    }).blur(function() {

        $(this).parent().removeClass('on');
        $('.talk').hide();


        $('.logo').removeClass('on').removeClass('on2');

    });

    $('#upw').focus(function() {

        $('.submit').show();

    }).blur(function(e) {

        if ($(this).val() == '') {

            $('.submit').hide();

        }

    });

    $('#uid, #upw').keyup(function(e) {

        if (e.keyCode == 13) {

            loginCheck();

        }

    });

    $.post("adm.php", {"m" : "ver"}, function(data) {

        $("#update_data").html(data);

    });

});

function loginCheck()
{

    var uid = $('#uid');
    var upw = $('#upw');
    var url = $('#url');

    $.post("adm.php", {"m" : "check", "uid" : uid.val(), "upw" : upw.val(), "url" : url.val()}, function(data) {

        $("#talk").html(data);

    });

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div class="nav">
<div class="block">
<ul>
<li><img src="<?=$web['host_member']?>/img/adm/info.png"></li>
<li class="text" id="upgrade"></li>
<li class="upgrade"><a href="http://download.teraboard.net/" target="_blank"></a></li>
</ul>
</div>
</div>
<div class="wrap">
<div class="conts">
<div class="block">
<div class="logo"></div>
<div class="title"></div>
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" id="url" name="url" value="<?=text($url)?>" />
<input type="hidden" name="m" value="" />
<div class="uid"><input type="text" id="uid" name="uid" value="" tabindex="1" /></div>
<div class="upw"><input type="password" id="upw" name="upw" value="" tabindex="2" /><a href="#" onclick="loginCheck(); return false;" class="submit" tabindex="3"></a><div class="talk"><span id="talk"></span></div></div>
<div class="user">
<p><span class="name">IP Add</span><?=text($_SERVER['REMOTE_ADDR'])?></p>
<p><span class="name">Browser</span>
<?
$browser = visit_browser($_SERVER['HTTP_USER_AGENT']);

if ($browser == 'MSIE 5' || $browser == 'MSIE 5.5' || $browser == 'MSIE 6' || $browser == 'MSIE 7' || $browser == 'MSIE 8' || $browser == 'MSIE 9' || $browser == 'MSIE 10') {

    echo "<font color='#ff9668'>".$browser."</font>";

} else {

    echo $browser;

}
?>
</p>
<p><span class="name">O/S</span><?=visit_os($_SERVER['HTTP_USER_AGENT'])?></p>
<p><span class="name">Monitor</span><font id="screen"></font></p>
</div>
</form>
<div class="ver">USE PLATFORM Ver. TB<?=text($setup['ver'])?></div>
</div>
</div>
</div>
</body>
</html>