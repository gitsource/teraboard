<?php // 회원 로그인
include_once("./_tb.php");
include_once("$web[path]/other/curl/Curl.php");
if ($check_login) { url($web['host_default'])."/"; }

if ($m == 'check') {

    $uid = strtolower(trim($uid));
    $upw = trim($_POST['upw']);
    $ip = trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])));

    if ($autologin) { $autologin = preg_match("/^[0-9]+$/", $autologin) ? $autologin : ""; }

    if (!$uid) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "회원 아이디가 입력되지 않았습니다.";
        exit;

    }

    if (!$upw) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "회원 패스워드가 입력되지 않았습니다.";
        exit;

    }

    $mb = member_uid($uid);

    if (!$mb['uid']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "입력하신 아이디는 등록되지 않은 아이디 입니다.";
        exit;

    }

    $sql_common = "";
    $sql_common .= " set mid = '".$mb['mid']."' ";
    $sql_common .= ", ip = '".$ip."' ";
    $sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[member_login_table] $sql_common ");

    $login_id = sql_insert_id();

    if (!$mb['upw']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "비밀번호 찾기를 이용하여, 새로 비밀번호를 발급하셔야합니다.";
        exit;

    }

    if (sql_password($upw) != $mb['upw']) {

        function sql_old_password($value) {

            $row = sql_fetch(" select old_password('".addslashes($value)."') as pass ");

            return $row['pass'];

        }

        if (sql_old_password($upw) == $mb['upw']) {

            sql_query(" update $web[member_table] set upw = '".sql_password($upw)."' where mid = '".$mb['mid']."' ");

        } else {

            echo "<script type='text/javascript'>";
            echo "$('#msg').show();";
            echo "</script>";

            echo "입력하신 비밀번호가 올바르지 않습니다.";
            exit;

        }

    }

    if ($mb['dropout']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "탈퇴한 아이디입니다.";
        exit;

    }

    if ($mb['stop']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "정지된 아이디입니다.";
        exit;

    }

    set_session('ss_mid', $mb['mid'].'|'.$_SERVER['REMOTE_ADDR'].'|'.md5($_SERVER['HTTP_USER_AGENT'].$mb['datetime']));

    if ($autologin) {

        $cookie_key = md5($_SERVER['HTTP_USER_AGENT'].$mb['upw'].$mb['datetime']);
        set_cookie("login_mid", $mb['mid'], (int)(86400 * 365));
        set_cookie("login", $cookie_key, (int)(86400 * 365));

    }

    if ($login_id) {

        sql_query(" update $web[member_login_table] set login = 1 where id = '".$login_id."' ");

    }

    if ($url) {

        $urlencode = urldecode($url);

    } else {

        $urlencode = urldecode($_SERVER[REQUEST_URI]);

    }

    if ($url) {

        $link = $urlencode;

    } else {

        $link = $web['host_member']."/";

    }

    url($link);

    exit;

}

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px; min-height:654px;}

.layout-login {width:460px; margin:0 auto; padding:50px 0;}

.layout-login .title {position:relative; margin:0 auto; margin-bottom:19px; width:162px; height:162px; background:url('<?=$web['host_member']?>/img/login_title.png') no-repeat;}
.layout-login .title span {cursor:pointer; position:absolute; right:0; bottom:0; display:block; width:48px; height:48px; background:url('<?=$web['host_member']?>/img/login_socialopen.png') no-repeat;}
.layout-login .title span.on {background-position:0 -48px;}
.layout-login .title span:hover {background-position:0 -48px;}

.layout-login .socialopen {position:relative; text-align:right; height:24px; padding-right:21px;}
.layout-login .socialopen {line-height:15px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-login .socialopen span.text {cursor:pointer;}
.layout-login .socialopen span.text:hover {color:#000000;}
.layout-login .socialopen span.text.on {color:#000000;}
.layout-login .socialopen span.icon {position:absolute; right:0; top:1px; display:block; width:16px; height:12px; background:url('<?=$web['host_member']?>/img/login_socialopen2.png') no-repeat;}
.layout-login .socialopen span.icon.on {background-position:0 -12px;}
.layout-login .socialopen span.icon:hover {background-position:0 -12px;}
.layout-login .socialopen span.text:hover span.icon {background-position:0 -12px;}

.layout-login .socialopen .box {text-align:left; z-index:1000; position:absolute; left:0; top:25px; display:none; width:454px; border:3px solid #dadde0; background-color:#ffffff; padding-top:2px;}
.layout-login .socialopen .box .lineout {border-bottom:2px solid #afafaf;}
.layout-login .socialopen .box .linein {padding:23px 18px; position:relative; border-left:2px solid #ffffff; border-right:2px solid #afafaf; background-color:#eaeaea;}
.layout-login .socialopen .box .close {position:absolute; right:0; top:0; display:block; width:50px; height:50px; background:url('<?=$web['host_member']?>/img/_close.png') no-repeat; cursor:pointer;}
.layout-login .socialopen .box .close:hover {background-position:0 -50px;}
.layout-login .socialopen .box .subj {text-align:center; margin:0; font-weight:bold; line-height:18px; font-size:13px; color:#485362; font-family:gulim,serif;}
.layout-login .socialopen .box .help {text-align:center; margin:3px 0 0 0; line-height:16px; font-size:12px; color:#999999; font-family:gulim,serif;}
.layout-login .socialopen .box .line {margin-top:16px; border-top:1px solid #d5d5d5;}
.layout-login .socialopen .box .item {padding-top:24px; font-size:0px; border-top:1px solid #ffffff;}
.layout-login .socialopen .box .item .block {text-align:center; margin-left:-20px;}
.layout-login .socialopen .box .item span {cursor:pointer; margin-left:20px; display:inline-block; width:62px; height:62px; background:url('<?=$web['host_member']?>/img/login_socialbtn.png') no-repeat;}
.layout-login .socialopen .box .item span.naver {background-position:0 0;}
.layout-login .socialopen .box .item span.naver:hover {background-position:0 -62px;}
.layout-login .socialopen .box .item span.kakao {background-position:-62px 0;}
.layout-login .socialopen .box .item span.kakao:hover {background-position:-62px -62px;}
.layout-login .socialopen .box .item span.facebook {background-position:-124px 0;}
.layout-login .socialopen .box .item span.facebook:hover {background-position:-124px -62px;}
.layout-login .socialopen .box .item span.twitter {background-position:-186px 0;}
.layout-login .socialopen .box .item span.twitter:hover {background-position:-186px -62px;}
.layout-login .socialopen .box .item span.google {background-position:-248px 0;}
.layout-login .socialopen .box .item span.google:hover {background-position:-248px -62px;}
.layout-login .socialopen .box .item span.instagram {background-position:-310px 0;}
.layout-login .socialopen .box .item span.instagram:hover {background-position:-310px -62px;}

.layout-login .uid {margin-top:1px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_id.png') no-repeat left 0; background-color:#ffffff;}
.layout-login .uid input {margin-left:40px; width:418px; height:49px; border:0px; background:transparent;}
.layout-login .uid input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-login .uid.on {border:1px solid #1192ca; background-position:0 -49px;}
.layout-login .uid.on input {color:#000000;}
.layout-login .uid span {position:absolute; left:40px; top:0px; display:block; width:80px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}

.layout-login .upw {margin-top:10px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_pw.png') no-repeat left 0; background-color:#ffffff;}
.layout-login .upw input {margin-left:40px; width:418px; height:49px; border:0px; background:transparent;}
.layout-login .upw input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-login .upw.on {border:1px solid #1192ca; background-position:0 -49px;}
.layout-login .upw.on input {color:#000000;}
.layout-login .upw span {position:absolute; left:40px; top:0px; display:block; width:80px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}

.layout-login .msg {display:none; text-align:center; padding-top:30px;}
.layout-login .msg {font-weight:bold; line-height:18px; font-size:13px; color:#f26d7d; font-family:gulim,serif;}

.layout-login .submit {cursor:pointer; margin-top:30px; height:59px; border:1px solid #1192ca; background-color:#42abd7;}
.layout-login .submit {font-weight:700; line-height:59px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-login .submit span {display:block; text-align:center;}

.layout-login .ipbox {margin-top:20px; position:relative; height:20px; padding-left:29px;}
.layout-login .ipbox span.checkbox {cursor:pointer; line-height:20px; font-size:12px; color:#4f5f6f; font-family:gulim,serif;}
.layout-login .ipbox span.checkbox .icon {position:absolute; left:0; top:-1px; display:block; width:20px; height:20px; background:url('<?=$web['host_member']?>/img/_autologoin.png') no-repeat;}
.layout-login .ipbox span.checkbox .icon.on {background-position:0 -20px;}
.layout-login .ipbox span.ip {position:absolute; right:0; top:0; display:block; width:180px; height:20px; text-align:right; vertical-align:top;}
.layout-login .ipbox span.ip {line-height:20px; font-size:11px; color:#999999; font-family:Arial,gulim,serif;}
.layout-login .ipbox span.ip .icon {margin-right:7px; display:inline-block; width:20px; height:20px; background:url('<?=$web['host_member']?>/img/_ip.gif') no-repeat;}
.layout-login .ipbox span.ip .text {position:relative; overflow:hidden; left:0px; top:-7px;}

.layout-login .menu {padding-top:19px; text-align:center; line-height:0px; margin-top:50px; border-top:1px solid #dadada;}
.layout-login .menu a {line-height:14px; font-size:12px; color:#999999; font-family:gulim,serif;}
.layout-login .menu a:hover {color:#000000;}
.layout-login .menu .line {padding:0 4px; line-height:14px; font-size:12px; color:#dadada; font-family:gulim,serif;}

@media screen and (max-width:600px) {

.layout-login {width:320px;}
.layout-login .socialopen .box {width:314px;}
.layout-login .uid input {width:278px;}
.layout-login .upw input {width:278px;}
.layout-login .socialopen .box .help {padding-top:4px; margin:0 auto; width:155px; text-align:center;}
.layout-login .socialopen .box .item span {line-height:82px;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    var obj = $('#autologin');

    if (obj.val() == 1) {

        $('.layout-login .ipbox span.checkbox .icon').addClass('on');

    } else {

        $('.layout-login .ipbox span.checkbox .icon').removeClass('on');

    }

    $('.layout-login .title span, .layout-login .socialopen span.text, .layout-login .socialopen .box .close').click(function() {

        var box = $('.layout-login .socialopen .box');

        if (box.is(':hidden') == true) {

            $('.layout-login .title span').addClass('on');
            $('.layout-login .socialopen span.icon').addClass('on');
            $('.layout-login .socialopen span.text').addClass('on');

            box.show();

        } else {

            $('.layout-login .title span').removeClass('on');
            $('.layout-login .socialopen span.icon').removeClass('on');
            $('.layout-login .socialopen span.text').removeClass('on');

            box.hide();

        }

    });

    $('#uid, #upw').focus(function() {

        $(this).parent().addClass('on');
        $('#msg').hide();

    }).blur(function() {

        $(this).parent().removeClass('on');
        $('#msg').hide();

        if ($(this).val() == '') {

            $(this).parent().find('span').show();

        }

    }).keydown(function() {

        $(this).parent().find('span').hide();
 
    });

    $('.uid span, .upw span').click(function() {

        $(this).parent().find('input').focus();

    });

    $('.layout-login .ipbox .checkbox').click(function() {

        var obj = $('#autologin');

        if (obj.val() == 1) {

            obj.val('');
            $('.layout-login .ipbox span.checkbox .icon').removeClass('on');

        } else {

            obj.val('1');
            $('.layout-login .ipbox span.checkbox .icon').addClass('on');

        }

    });

    $('#uid, #upw, .layout-login .submit span').keydown(function(e) {

        if (e.keyCode == 13) {

            loginCheck();

        }

    });

    $('.layout-login .submit span').click(function() {

        loginCheck();

    });

    $('.layout-login .naver').click(function() {

        naverLogin();

    });

    $('.layout-login .kakao').click(function() {

        kakaoLogin();

    });

    $('.layout-login .facebook').click(function() {

        facebookLogin();

    });

    $('.layout-login .twitter').click(function() {

        twitterLogin();

    });

    $('.layout-login .google').click(function() {

        googleLogin();

    });

    $('.layout-login .instagram').click(function() {

        instagramLogin();

    });

});

function loginCheck()
{

    var uid = $('#uid');
    var upw = $('#upw');
    var url = $('#url');
    var autologin = $('#autologin');

    $.post("index.php", {"m" : "check", "uid" : uid.val(), "upw" : upw.val(), "autologin" : autologin.val(), "url" : url.val()}, function(data) {

        $("#msg").html(data);

    });

}
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("$web[path]/_top.php");
?>
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" id="url" name="url" value="<?=text($url)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" id="autologin" name="autologin" value="" />
<div class="layout-login">
<div class="title"><span></span></div>
<? if ($setup_api['social_login_onoff']) { ?>
<!-- socialopen start //-->
<div class="socialopen">
<span class="text">소셜 아이디 로그인<span class="icon"></span></span>
<div class="box">
<div class="lineout">
<div class="linein">
<span class="close"></span>
<p class="subj">로그인 수단 선택</p>
<p class="help">회원님께서 자주 사용하시는 소셜 서비스를 선택하세요.</p>
<div class="line">
<div class="item">
<div class="block">
<?
if ($setup_api['login_naver']) {

    echo "<span class='naver' title='네이버'></span>";

}

if ($setup_api['login_kakao']) {

    echo "<span class='kakao' title='카카오'></span>";

}

if ($setup_api['login_facebook']) {

    echo "<span class='facebook' title='페이스북'></span>";

}

if ($setup_api['login_twitter']) {

    echo "<span class='twitter' title='트위터'></span>";

}

if ($setup_api['login_google']) {

    echo "<span class='google' title='구글'></span>";

}

if ($setup_api['login_instagram']) {

    echo "<span class='instagram' title='인스타그램'></span>";

}
?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- socialopen end //-->
<? } ?>
<div class="uid"><input type="text" id="uid" name="uid" value="" tabindex="1" /><span>아이디</span></div>
<div class="upw"><input type="password" id="upw" name="upw" value="" tabindex="2" /><span>비밀번호</span></div>
<div class="msg" id="msg"></div>
<div class="submit"><span tabindex="3">로그인</span></div>
<div class="ipbox">
<span class="checkbox">로그인 상태 유지<span class="icon"></span></span>
<span class="ip"><span class="icon"></span><span class="text">Access IP : <?=text($_SERVER['REMOTE_ADDR'])?></span></span>
</div>
<div class="menu">
<a href="<?=$web['host_member']?>/login/">로그인</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/id_hp.php">아이디 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/pw_hp.php">비밀번호 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/join/">회원가입</a>
</div>
</div>
</form>
<?
include_once("$web[path]/_bottom.php");
?>