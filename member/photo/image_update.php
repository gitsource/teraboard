<?php
include_once("./_tb.php");
echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

// 폼 체크
if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

}

if ($photo) { $photo = preg_match("/^[a-zA-Z0-9_\-\.]+$/", $photo) ? $photo : ""; }
if ($photo_id) { $photo_id = preg_match("/^[0-9]+$/", $photo_id) ? $photo_id : ""; }
if ($photo_del) { $photo_del = preg_match("/^[0-9]+$/", $photo_del) ? $photo_del : ""; }
if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }
if ($x1) { $x1 = preg_match("/^[0-9]+$/", $x1) ? $x1 : ""; }
if ($y1) { $y1 = preg_match("/^[0-9]+$/", $y1) ? $y1 : ""; }
if ($x2) { $x2 = preg_match("/^[0-9]+$/", $x2) ? $x2 : ""; }
if ($y2) { $y2 = preg_match("/^[0-9]+$/", $y2) ? $y2 : ""; }
if ($w) { $w = preg_match("/^[0-9]+$/", $w) ? $w : ""; }
if ($h) { $h = preg_match("/^[0-9]+$/", $h) ? $h : ""; }

if ($m == '') {

    $data = member($mid);

    if (!$data['mid']) {

        message("<p class='title'>알림</p><p class='text'>회원이 존재하지 않습니다.</p>", "c");

    }

    if ($member['mid'] != $mid && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($member['mid'] != $mid && $check_admin) {

        $check_auth = check_auth("member", 1);

        if ($check_auth != 'full') {

            message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

        }

    }

    $sql_common = "";
    $sql_common .= " set photo = '".addslashes($photo)."' ";

    sql_query(" update $web[member_table] $sql_common where mid = '".addslashes($mid)."' ");
    sql_query(" update $web[member_photo_table] set onoff = 0 where mid = '".addslashes($mid)."' ");

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "if (opener) {";
    echo "opener.location.reload();";
    echo "}";
    echo "</script>";

    message("<p class='title'>알림</p><p class='text'>적용 하였습니다.</p>", "", "image.php?mid=".addslashes($mid), true, true);

}

else if ($m == 'file') {

    if ($member['mid'] != $mid && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($member['mid'] != $mid && $check_admin) {

        $check_auth = check_auth("member", 1);

        if ($check_auth != 'full') {

            message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

        }

    }

    $dir = $disk['path']."/photo/".data_path("", "");

    @mkdir($dir, 0707);
    @chmod($dir, 0707);

    $tmp_name = $_FILES['file']['tmp_name'];
    $name = $_FILES['file']['name'];
    $size = $_FILES['file']['size'];
    $error = $_FILES['file']['error'];

    if ($size > 1048576) {

        message("<p class='title'>알림</p><p class='text'>제한 크기를 초과하였습니다.</p>", "b");

    }

    if (is_uploaded_file($tmp_name)) {

        $upload = array();
        $upload['source'] = $name;
        $upload['size'] = $size;

        $name = preg_replace("/\.(php|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $name);

        $upload['file'] = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr(md5(uniqid($web['server_time'])),0,8).'_'.str_replace('%', '', urlencode(substr($name,-10)));

        $dest_file = $dir.'/tmp_'.$upload['file'];

        $error_code = move_uploaded_file($tmp_name, $dest_file) or die($error);

        @chmod($dest_file, 0606);

        $upload['image'] = @getimagesize($dest_file);

        if ($upload['image'][2] != 1 && $upload['image'][2] != 2 && $upload['image'][2] != 3 && !preg_match("/\.(jp[e]?g|gif|png)$/i", $dest_file)) {

            @unlink($dest_file);

            echo "<script type='text/javascript'>alert('jpg, gif, png 파일만 업로드 가능합니다.');</script>";

        } else {

            sql_query(" update $web[member_table] set photo = '' where mid = '".addslashes($mid)."' ");
            sql_query(" update $web[member_photo_table] set onoff = 0 where mid = '".addslashes($mid)."' ");

            $sql_common = "";
            $sql_common .= " set mid = '".addslashes($mid)."' ";
            $sql_common .= ", onoff = 1 ";
            $sql_common .= ", upload_source = '".trim(strip_tags(sql_real_escape_string($upload['source'])))."' ";
            $sql_common .= ", upload_file = '".$upload['file']."' ";
            $sql_common .= ", upload_filesize = '".$upload['size']."' ";
            $sql_common .= ", upload_width = '".$upload['image'][0]."' ";
            $sql_common .= ", upload_height = '".$upload['image'][1]."' ";
            $sql_common .= ", upload_time = '".$web['time_ymdhis']."' ";

            sql_query(" insert into $web[member_photo_table] $sql_common ");

            $id = sql_insert_id();

            $thumb = $dir.'/'.$upload['file'];

            image_thumb(200, 200, $dest_file, $thumb, true);

            @unlink($dest_file);

            $upload = array();
            $upload['image'] = @getimagesize($thumb);
            $upload['size'] = @filesize($thumb);

            if ($upload['size']) {

                $sql_common = "";
                $sql_common .= " set upload_filesize = '".$upload['size']."' ";
                $sql_common .= ", upload_width = '".$upload['image'][0]."' ";
                $sql_common .= ", upload_height = '".$upload['image'][1]."' ";

                sql_query(" update $web[member_photo_table] $sql_common where id = '".$id."' ");

            }

        }

    }

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "if (opener) {";
    echo "opener.location.reload();";
    echo "}";
    echo "</script>";

    message("<p class='title'>알림</p><p class='text'>적용 하였습니다.</p>", "", "image.php?mid=".addslashes($mid), true, true);

}

else if ($m == 'area') {

    if ($member['mid'] != $mid && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($member['mid'] != $mid && $check_admin) {

        $check_auth = check_auth("member", 1);

        if ($check_auth != 'full') {

            message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

        }

    }

    $dir = $disk['path']."/area/".data_path("", "");

    @mkdir($dir, 0707);
    @chmod($dir, 0707);

    $tmp_name = $_FILES['file']['tmp_name'];
    $name = $_FILES['file']['name'];
    $size = $_FILES['file']['size'];
    $error = $_FILES['file']['error'];

    if ($size > 1048576) {

        message("<p class='title'>알림</p><p class='text'>제한 크기를 초과하였습니다.</p>", "b");

    }

    if (is_uploaded_file($tmp_name)) {

        $upload = array();
        $upload['source'] = $name;
        $upload['size'] = $size;

        $name = preg_replace("/\.(php|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $name);

        $upload['file'] = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr(md5(uniqid($web['server_time'])),0,8).'_'.str_replace('%', '', urlencode(substr($name,-10)));

        $dest_file = $dir.'/'.$upload['file'];

        $error_code = move_uploaded_file($tmp_name, $dest_file) or die($error);

        @chmod($dest_file, 0606);

        $upload['image'] = @getimagesize($dest_file);

        if ($upload['image'][2] != 1 && $upload['image'][2] != 2 && $upload['image'][2] != 3) {

            @unlink($dest_file);

            message("<p class='title'>알림</p><p class='text'>jpg, gif, png 파일만 업로드 가능합니다.</p>", "b");

        }

        if ($upload['image'][0] < 200 || $upload['image'][1] < 200) {

            message("<p class='title'>알림</p><p class='text'>가로x세로 200x200 이상으로 업로드하세요.</p>", "b");

        }

        $sql_common = "";
        $sql_common .= " set mid = '".addslashes($mid)."' ";
        $sql_common .= ", upload_source = '".trim(strip_tags(sql_real_escape_string($upload['source'])))."' ";
        $sql_common .= ", upload_file = '".$upload['file']."' ";
        $sql_common .= ", upload_filesize = '".$upload['size']."' ";
        $sql_common .= ", upload_width = '".$upload['image'][0]."' ";
        $sql_common .= ", upload_height = '".$upload['image'][1]."' ";
        $sql_common .= ", upload_time = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[area_table] $sql_common ");

        $area_id = sql_insert_id();

        url("image.php?mid=".addslashes($mid)."&area_id=".$area_id);

    }

    message("<p class='title'>알림</p><p class='text'>이미지 업로드를 실패하였습니다.</p>", "b");

}

else if ($m == 'area_select') {

    $area = area($id);

    if (!$area['mid'] || !$area['upload_file']) {

        message("<p class='title'>알림</p><p class='text'>크롭 이미지가 삭제되었거나 존재하지 않습니다.</p>", "c");

    }

    $mid = $area['mid'];

    $mb = member($mid);

    if (!$mb['mid']) {

        message("<p class='title'>알림</p><p class='text'>회원이 존재하지 않습니다.</p>", "c");

    }

    if ($member['mid'] != $mid && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($member['mid'] != $mid && $check_admin) {

        $check_auth = check_auth("member", 1);

        if ($check_auth != 'full') {

            message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

        }

    }

    $source = $disk['path']."/area/".data_path("u", $area['upload_time'])."/".$area['upload_file'];

    if (!file_exists($source)) {

        message("<p class='title'>알림</p><p class='text'>크롭 이미지가 삭제되었거나 존재하지 않습니다.</p>", "c");

    }

    $thumb_width = 200;
    $thumb_height = 200;

    $dir = $disk['path']."/photo/".data_path("", "");

    @mkdir($dir, 0707);
    @chmod($dir, 0707);

    $thumb = $dir.'/'.$area['upload_file'];

    if (preg_match("/\.(jp[e]?g|gif|png)$/i", $source)) {

        if ($w || $h) {

            image_area($source, $thumb, $thumb_width, $thumb_height, $x1, $y1, $x2, $y2, $w, $h, 100);

        } else {

            image_thumb($thumb_width, $thumb_height, $source, $thumb, true);

        }

    }

    @unlink("$source");

    sql_query(" update $web[member_table] set photo = '' where mid = '".addslashes($mid)."' ");
    sql_query(" update $web[member_photo_table] set onoff = 0 where mid = '".addslashes($mid)."' ");

    $upload = array();
    $upload['image'] = @getimagesize($thumb);
    $upload['size'] = @filesize($thumb);

    $sql_common = "";
    $sql_common .= " set mid = '".addslashes($mid)."' ";
    $sql_common .= ", onoff = 1 ";
    $sql_common .= ", area_x1 = '".$x1."' ";
    $sql_common .= ", area_y1 = '".$y1."' ";
    $sql_common .= ", area_x2 = '".$x2."' ";
    $sql_common .= ", area_y2 = '".$y2."' ";
    $sql_common .= ", area_w = '".$w."' ";
    $sql_common .= ", area_h = '".$h."' ";
    $sql_common .= ", upload_source = '".addslashes($area['upload_source'])."' ";
    $sql_common .= ", upload_file = '".$area['upload_file']."' ";

    if ($upload['size']) {

        $sql_common .= ", upload_filesize = '".$upload['size']."' ";
        $sql_common .= ", upload_width = '".$upload['image'][0]."' ";
        $sql_common .= ", upload_height = '".$upload['image'][1]."' ";

    }

    $sql_common .= ", upload_time = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[member_photo_table] $sql_common ");

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "if (opener) {";
    echo "opener.location.href = 'image.php?mid=".addslashes($mid)."&mode=2';";
    echo "}";
    echo "</script>";

    message("<p class='title'>알림</p><p class='text'>저장 하였습니다.</p>", "c");

}

else if ($m == 'photo') {

    $chk = sql_fetch(" select * from $web[member_photo_table] where id = '".$photo_id."' limit 0, 1 ");

    if ($chk['mid'] != $mid) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($member['mid'] != $mid && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($member['mid'] != $mid && $check_admin) {

        $check_auth = check_auth("member", 1);

        if ($check_auth != 'full') {

            message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

        }

    }

    sql_query(" update $web[member_table] set photo = '' where mid = '".addslashes($mid)."' ");
    sql_query(" update $web[member_photo_table] set onoff = 0 where mid = '".addslashes($mid)."' ");
    sql_query(" update $web[member_photo_table] set onoff = 1 where id = '".addslashes($photo_id)."' ");

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "if (opener) {";
    echo "opener.location.reload();";
    echo "}";
    echo "</script>";

    message("<p class='title'>알림</p><p class='text'>적용 하였습니다.</p>", "", "image.php?mid=".addslashes($mid)."&mode=2", true, true);

}

else if ($m == 'photo_del') {

    $chk = sql_fetch(" select * from $web[member_photo_table] where id = '".$photo_del."' limit 0, 1 ");

    if ($chk['mid'] != $mid) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($member['mid'] != $mid && !$check_admin) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($member['mid'] != $mid && $check_admin) {

        $check_auth = check_auth("member", 1);

        if ($check_auth != 'full') {

            message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

        }

    }

    if ($chk['onoff']) {

        sql_query(" update $web[member_photo_table] set onoff = 0 where mid = '".addslashes($mid)."' ");

    }

    $file_path = $disk['path']."/photo/".data_path("u", $file['upload_time'])."/".$file['upload_file'];

    if (file_exists($file_path) && $file['upload_file']) {

        @unlink($file_path);

    }

    sql_query(" delete from $web[member_photo_table] where id = '".addslashes($photo_del)."' ");

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "if (opener) {";
    echo "opener.location.reload();";
    echo "}";
    echo "</script>";

    message("<p class='title'>알림</p><p class='text'>삭제 하였습니다.</p>", "", "image.php?mid=".addslashes($mid)."&mode=2", true, true);

} else {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}
?>