<?php // 프로필이미지
include_once("./_tb.php");
if ($mode) { $mode = preg_match("/^[0-9]+$/", $mode) ? $mode : ""; }
if ($area_id) { $area_id = preg_match("/^[0-9]+$/", $area_id) ? $area_id : ""; }

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "c");

}

if (!$mid) {

    $mid = $member['mid'];

}

$mb = member($mid);

if (!$mb['mid']) {

    message("<p class='title'>알림</p><p class='text'>회원이 존재하지 않습니다.</p>", "c");

}

if ($member['mid'] != $mid && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

if ($member['mid'] != $mid && $check_admin) {

    $check_auth = check_auth("member", 1);

    if ($check_auth != 'full') {

        message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

    }

}

if (!$mode) {
    $mode = 0;
}

if ($area_id) {
    $mode = 1;
}

$member_thumb = member_thumb($mid, $mb);

$list = array();
$result = sql_query(" select * from $web[member_photo_table] where mid = '".addslashes($mid)."' order by id desc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['image'] = $disk['server_member']."/photo/".data_path("u", $row['upload_time'])."/".$row['upload_file'];

}

$photo_id = "";
$file = sql_fetch(" select * from $web[member_photo_table] where mid = '".addslashes($mid)."' and onoff = 1 limit 0, 1 ");
if ($file['id']) {
    $photo_id = $file['id'];
}
?>
<!DOCTYPE html>
<html>
<head>
<?
include_once("$web[path]/_head.php");
?>
<style type="text/css">
body {min-width:600px; background-color:#4f5f6f;}
.popup {background-color:#f0f3f6; padding:20px 0 20px 0;}

.form-top {clear:both; margin:0 30px; padding-bottom:19px; border-bottom:2px solid #dbe1e8;}
.form-top .title {font-weight:700; line-height:20px; font-size:20px; color:#4f5f6f; font-family:'Nanum Gothic',gulim,serif;}
.form-top .text {margin-top:7px; font-weight:400; line-height:15px; font-size:13px; color:#a1b1c2; font-family:'Nanum Gothic',gulim,serif;}

.form-array {clear:both; margin:10px 20px 0 20px;}
.form-array .round1,
.form-array .round2,
.form-array .round3,
.form-array .round4 {width:10px; height:10px; background:url('<?=$web['host_member']?>/img/_list_tablebg_round.png') no-repeat;}
.form-array .round1 {background-position:0px 0px;}
.form-array .round2 {background-position:-10px 0px;}
.form-array .round3 {background-position:0px -10px;}
.form-array .round4 {background-position:-10px -10px;}
.form-array .width1,
.form-array .width2 {height:10px; background:url('<?=$web['host_member']?>/img/_list_tablebg_width.png') repeat-x;}
.form-array .width1 {background-position:0px 0px;}
.form-array .width2 {background-position:0px -10px;}
.form-array .height1,
.form-array .height2 {height:10px; background:url('<?=$web['host_member']?>/img/_list_tablebg_height.png') repeat-y;}
.form-array .height1 {background-position:0px 0px;}
.form-array .height2 {background-position:-10px 0px;}

.form-box {border:1px solid #dbe1e8; background-color:#ffffff;}
.form-box .selectbox {position:relative; width:200px; height:26px;}
.form-box .selectbox .select {background:url('<?=$web['host_member']?>/img/_selectbox_w200_h26.png') no-repeat;}
.form-box .selectbox .select {position:absolute; left:0; top:0; width:200px; height:26px; background-position:0 0; cursor:pointer;}
.form-box .selectbox .select .block {position:relative; width:200px;}
.form-box .selectbox .select .block {font-weight:bold; line-height:26px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.form-box .selectbox .select .block span.text {overflow-x:hidden; height:26px; display:block; padding:0 16px 0 6px;}
.form-box .selectbox .select .block .option {display:none; position:absolute; left:0; top:26px; width:200px;}
.form-box .selectbox .select .block .option ul.array {overflow:auto; overflow-x:hidden; max-height:200px; font-size:0px; background-color:#f0f3f6; border-left:1px solid #dbe1e8; border-right:1px solid #dbe1e8; border-bottom:1px solid #dbe1e8;}
.form-box .selectbox .select .block .option ul.array li {overflow-x:hidden; padding-left:6px; display:block; vertical-align:middle; height:30px; border-top:1px solid #dbe1e8;}
.form-box .selectbox .select .block .option ul.array li.first {border-top:0px;}
.form-box .selectbox .select .block .option ul.array li {font-weight:bold; line-height:30px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.form-box .selectbox .select .block .option ul.array li:hover {color:#42abd7;}
.form-box .selectbox .select .block .option ul.array li.on {color:#42abd7;}
.form-box .selectbox.opt .select {background-position:0 -26px;}
.form-box .selectbox.opt .select span.text {color:#ffffff;}
.form-box .selectbox.opt .select .block .option {display:block;}

.form-box .checkbox {width:13px; height:13px; position:relative; overflow:hidden; left:0; top:2px;}

.form-box .input_file {width:298px; height:24px; border:1px solid #dbe1e8; background-color:#f8f8f8;}
.form-box .input_file {line-height:24px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}

.form-box .title {position:relative; padding-bottom:25px;}
.form-box .title span.sub {font-weight:700; line-height:20px; font-size:20px; color:#384655; font-family:'Nanum Gothic',gulim,serif;}
.form-box .title div.setupall {position:absolute; right:0; top:0px; width:200px;}
.form-box .list {padding:20px 0; border-top:1px solid #dbe1e8;}
.form-box .list.first {border-top:0px}
.form-box .subj {width:160px; text-align:center;}
.form-box .subj {font-weight:400; line-height:18px; font-size:14px; color:#7e8e9f; font-family:'Nanum Gothic',gulim,serif;}
.form-box .ess {width:5px; height:26px; background-color:#f0f3f6;}
.form-box .ess.on {background-color:#42abd7;}
.form-box .line {width:20px;}
.form-box td.text {font-weight:bold; line-height:18px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.form-box .help {font-weight:400; line-height:18px; font-size:12px; color:#babec1; font-family:'Nanum Gothic',gulim,serif;}
.form-box .source {font-weight:400; line-height:18px; font-size:14px; color:#7e8e9f; font-family:'Nanum Gothic',gulim,serif;}
.form-box .filesize {margin-left:5px; font-weight:400; line-height:18px; font-size:14px; color:#b7b7b7; font-family:'Nanum Gothic',gulim,serif;}
.form-box .filedel {font-weight:400; line-height:18px; font-size:12px; color:#4f5f6f; font-family:'Nanum Gothic',gulim,serif;}
.form-box .fileline {padding:0 5px; font-weight:400; line-height:18px; font-size:12px; color:#dbe1e8; font-family:'Nanum Gothic',gulim,serif;}

.form-box .member_thumb {width:48px; height:48px; border:2px solid #dbe1e8; background-color:#f0f3f6;}
.form-box .member_thumb img {width:48px; height:48px;}

.form-box .btn_imgselecter {display:block; width:96px; height:26px; background:url('<?=$web['host_member']?>/img/_btn_imgselecter.png') no-repeat;}
.form-box .btn_imgselecter:hover {background-position:0px -26px;}
.form-box .btn_imgselecter:active {background-position:0px -52px;}

.form-save {clear:both; margin-top:20px;}
.form-save:after {display:block; clear:both; content:'';}
.form-save .btn {display:table; margin:0 auto; height:50px;}
.form-save .btn a {display:block; float:left;}
.form-save .btn .submit,
.form-save .btn .cancel {width:150px; height:60px; background:url('<?=$web['host_member']?>/img/_btn_update.png') no-repeat;}
.form-save .btn .submit:hover {background-position:0 -60px;}
.form-save .btn .submit:active {background-position:0 -120px;}
.form-save .btn .cancel {background-position:-150px 0;}
.form-save .btn .cancel:hover {background-position:-150px -60px;}
.form-save .btn .cancel:active {background-position:-150px -120px;}

.mode {display:none;}

#mode0 .item ul {font-size:0px;}
#mode0 .item ul li.block {display:inline-block; width:58px; height:58px; vertical-align:middle;}
#mode0 .item ul li.block img {display:block; width:48px; height:48px; border:5px solid #ffffff; cursor:pointer;}
#mode0 .item ul li.block img.on {border:5px solid #42abd7;}

#mode2 .item ul {font-size:0px;}
#mode2 .item ul li.block {position:relative; display:inline-block; width:58px; height:58px; vertical-align:middle;}
#mode2 .item ul li.block img {display:block; width:48px; height:48px; border:5px solid #ffffff; cursor:pointer;}
#mode2 .item ul li.block img.on {border:5px solid #42abd7;}
#mode2 .item ul li.block .del {position:absolute; right:0; bottom:0px; width:20px; height:20px; background:url('<?=$web['host_member']?>/img/_btn_del.png') no-repeat; cursor:pointer;}
#mode2 .item ul li.block .del:hover {background-position:0 -20px;}
</style>
<script type="text/javascript">
var zindex = 1;
$(document).ready( function() {

    if ($('#area_id').val() != '') {

        areaOpen();

    }

    var photo_id = $('#photo_id').val();

    $('#mode2 .item ul li.block img[name="'+photo_id+'"]').addClass('on');

    $('.select .block span').click(function() {

        if ($(this).parent().children('div.option').is(':hidden') == true) {

            zindex++;

            $(this).parent().parent().parent('div.selectbox').addClass('opt').zIndex(zindex);

        } else {

            $(this).parent().parent().parent('div.selectbox').removeClass('opt');

        }

    });

    $('.select .block .option ul.array li').click(function() {

        $(this).parent().parent().children('input.add').val($(this).attr('name'));
        $(this).parent().parent().parent().children('span.text').text($(this).text());
        $(this).parent().parent().parent().parent().parent('div.selectbox').removeClass('opt');

        $('.mode').hide();
        $('#mode'+$(this).attr('name')).show();

    });

    $('.selectbox .add').each(function() {

        var val = $(this).val();

        var addtext = $(this).parent().children('ul.array').children('li[name="'+val+'"]').text();

        if (addtext) {

            $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').addClass('on');
            $(this).parent().parent().children('span.text').text(addtext);

            $('#mode'+val).show();

            if (val == 0) {

                var photo = $('#photo').val();

                $('#mode0 .item ul li.block img[name="'+photo+'"]').addClass('on');

            }

        } else {

            $(this).parent().parent().children('span.text').html('&nbsp;');

        }

    });

    $('#mode0 .item ul li.block img').click(function() {

        $('#mode0 .item ul li.block img').removeClass('on');

        $(this).addClass('on');

        $('#photo').val($(this).attr('name'));
        $('#member_thumb').html('<img src=\"'+$(this).attr('src')+'\">');
        $('.filedelbox').hide();

    });

    $('#mode2 .item ul li.block img').click(function() {

        $('#mode2 .item ul li.block img').removeClass('on');

        $(this).addClass('on');

        $('#photo_id').val($(this).attr('name'));
        $('#member_thumb').html('<img src=\"'+$(this).attr('src')+'\">');
        $('.filedelbox').hide();

    });

    $('#mode2 .item ul li.block .del').click(function() {

        var id = $(this).parent().children('img').attr('name');

        if (!confirm("삭제 하시겠습니까?")) {

            return false;

        }

        var f = document.formSetup;

        f.m.value = "photo_del";
        f.photo_del.value = id;

        f.action = "image_update.php";
        f.submit()

    });

    $(document).click(function(e) {

        if (!$(e.target).is('.selectbox *')) {

            $('.selectbox').removeClass('opt');

        }

    });

});

function areaOpen()
{

    win_open("area.php?id="+$('#area_id').val(),"areaOpen","width=894, height=600, scrollbars=no");

    $('.form-save .btn .submit').hide();

}

function areaSetup()
{

    if (check_touch) {

        alert("이미지 셀렉터는 PC에서만 가능합니다.");
        return false;

    }

    var f = document.formSetup;

    if (f.area_id.value == '') {

        if (f.file.value == '') {

            alert("이미지를 첨부하세요.");
            return false;

        }

        f.m.value = "area";

        $('.form-save .btn').hide();

        f.action = "image_update.php";
        f.submit();

    } else {

        areaOpen();

    }

}

function fileDel()
{

    var f = document.formSetup;

    if (f.filedel.checked == false) {

        return false;

    }

    if (!confirm("삭제 하시겠습니까?")) {

        return false;

    }

    f.m.value = "photo_del";
    f.photo_del.value = "<?=text($photo_id)?>";

    f.action = "image_update.php";
    f.submit()

}

function submitSetup()
{

    var f = document.formSetup;

    if (f.mode.value == 0) {

        if (f.photo.value == '') {

            alert("기본 이미지를 선택하세요.");
            return false;

        }

        f.m.value = "";

    }

    else if (f.mode.value == 1) {

        if (f.file.value == '') {

            alert("이미지를 첨부하세요.");
            return false;

        }

        f.m.value = "file";

    }

    else if (f.mode.value == 2) {

        f.m.value = "photo";

    }

    $('.form-save .btn').hide();

    f.action = "image_update.php";
    f.submit();

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<form method="post" name="formSetup" enctype="multipart/form-data" autocomplete="off">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="mid" value="<?=text($mid)?>" />
<input type="hidden" id="photo" name="photo" value="<?=text($mb['photo'])?>" />
<input type="hidden" id="photo_id" name="photo_id" value="<?=text($photo_id)?>" />
<input type="hidden" id="area_id" name="area_id" value="<?=text($area_id)?>" />
<input type="hidden" id="photo_del" name="photo_del" value="" />
<div class="popup">
<div class="form-top">
<div class="title"><span>회원사진</span></div>
<div class="text"><span>회원사진의 권장 크기는 가로/세로 200px 입니다.<br />정사각형 비율이 아닐 경우 이미지가 틀어지거나 잘릴 수 있으니 주의하세요.</span></div>
</div>
<!-- form-array start //-->
<div class="form-array">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="round1"></td>
    <td class="width1"></td>
    <td class="round2"></td>
</tr>
<tr>
    <td class="height1"></td>
    <td>
<!-- form-box start //-->
<div class="form-box">
<!-- list start //-->
<div class="list first">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">현재 이미지</td>
    <td class="ess"></td>
    <td class="line"></td>
    <td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><div id="member_thumb" class="member_thumb"><?=$member_thumb?></div></td>
<? if ($file['id']) { ?>
    <td width="20"></td>
    <td class="filedelbox"><a href="download.php?id=<?=text($file['id'])?>" class="source"><?=text($file['upload_source'])?></a></td>
    <td class="filedelbox filesize">(<?=file_size($file['upload_filesize'])?>)</td>
<? } ?>
<? if (!$member_thumb) { ?>
    <td width="20"></td>
    <td class="help">등록된 이미지가 없습니다.</td>
<? } ?>
</tr>
</table>
<? if ($file['id']) { ?>
<div class="filedelbox">
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
<tr>
    <td><input type="checkbox" name="filedel" value="1" class="checkbox" onclick="fileDel();" /></td>
    <td width="5"></td>
    <td class="filedel" onclick="elementCheck('formSetup', 'filedel'); fileDel();">삭제</td>
    <td class="fileline">|</td>
    <td class="help">회원사진을 제거 합니다.</td>
</tr>
</table>
</div>
<? } ?>
    </td>
</tr>
</table>
</div>
<!-- list end //-->
<!-- list start //-->
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">생성 방식</td>
    <td class="ess"></td>
    <td class="line"></td>
    <td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>
<!-- selectbox //-->
<div class="selectbox">
<div class="select">
<div class="block">
<span class="text"></span>
<div class="option"><input type="hidden" name="mode" value="<?=text($mode)?>" class="add" />
<ul class="array">
<li name="0" class="first">기본 이미지 선택</li>
<li name="1">이미지 직접 등록</li>
<li name="2">등록 이미지 선택</li>
</ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
    </td>
</tr>
</table>
    </td>
</tr>
</table>
</div>
<!-- list end //-->
<!-- mode0 start //-->
<div id="mode0" class="mode">
<!-- list start //-->
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">기본 이미지</td>
    <td class="ess"></td>
    <td class="line"></td>
    <td>
<div class="item">
<ul>
<li class="block"><img src="<?=$web['host_img']?>/photo/1.jpg" name="1.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/2.jpg" name="2.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/3.jpg" name="3.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/4.jpg" name="4.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/5.jpg" name="5.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/6.jpg" name="6.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/7.jpg" name="7.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/8.jpg" name="8.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/9.jpg" name="9.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/10.jpg" name="10.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/11.jpg" name="11.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/12.jpg" name="12.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/13.jpg" name="13.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/14.jpg" name="14.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/15.jpg" name="15.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/16.jpg" name="16.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/17.jpg" name="17.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/18.jpg" name="18.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/19.jpg" name="19.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/20.jpg" name="20.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/21.jpg" name="21.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/22.jpg" name="22.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/23.jpg" name="23.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/24.jpg" name="24.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/25.jpg" name="25.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/26.jpg" name="26.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/27.jpg" name="27.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/28.jpg" name="28.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/29.jpg" name="29.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/30.jpg" name="30.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/31.jpg" name="31.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/32.jpg" name="32.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/33.jpg" name="33.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/34.jpg" name="34.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/35.jpg" name="35.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/36.jpg" name="36.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/37.jpg" name="37.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/38.jpg" name="38.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/39.jpg" name="39.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/40.jpg" name="40.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/41.jpg" name="41.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/42.jpg" name="42.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/43.jpg" name="43.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/44.jpg" name="44.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/45.jpg" name="45.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/46.jpg" name="46.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/47.jpg" name="47.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/48.jpg" name="48.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/49.jpg" name="49.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/50.jpg" name="50.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/51.jpg" name="51.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/52.jpg" name="52.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/53.jpg" name="53.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/54.jpg" name="54.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/55.jpg" name="55.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/56.jpg" name="56.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/57.jpg" name="57.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/58.jpg" name="58.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/59.jpg" name="59.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/60.jpg" name="60.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/61.jpg" name="61.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/62.jpg" name="62.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/63.jpg" name="63.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/64.jpg" name="64.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/65.jpg" name="65.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/66.jpg" name="66.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/67.jpg" name="67.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/68.jpg" name="68.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/69.jpg" name="69.jpg"></li>
<li class="block"><img src="<?=$web['host_img']?>/photo/70.jpg" name="70.jpg"></li>
</ul>
</div>
    </td>
</tr>
</table>
</div>
<!-- list end //-->
</div>
<!-- mode0 end //-->
<!-- mode1 start //-->
<div id="mode1" class="mode">
<!-- list start //-->
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">이미지 첨부</td>
    <td class="ess"></td>
    <td class="line"></td>
    <td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="file" name="file" value="" class="input_file" /></td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
<tr>
    <td class="help">JPG, GIF, PNG 파일 첨부 가능 (제한 크기 : 1MB)</td>
</tr>
</table>
    </td>
</tr>
</table>
</div>
<!-- list end //-->
<!-- list start //-->
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">이미지 편집</td>
    <td class="ess"></td>
    <td class="line"></td>
    <td><a href="#" onclick="areaSetup(); return false;" class="btn_imgselecter"></a></td>
</tr>
</table>
</div>
<!-- list end //-->
</div>
<!-- mode1 end //-->
<!-- mode2 start //-->
<div id="mode2" class="mode">
<!-- list start //-->
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">등록 이미지</td>
    <td class="ess"></td>
    <td class="line"></td>
    <td>
<div class="item">
<ul>
<?
for ($i=0; $i<count($list); $i++) {

    echo "<li class='block'><span class='del'></span><img src='".$list[$i]['image']."' name='".text($list[$i]['id'])."' title='".text_date("Y-m-d H:i", $list[$i]['upload_time'])."'></li>";

}
?>
</ul>
</div>
    </td>
</tr>
</table>
</div>
<!-- list end //-->
</div>
<!-- mode2 end //-->
</div>
<!-- form-box end //-->
    </td>
    <td class="height2"></td>
</tr>
<tr>
    <td class="round3"></td>
    <td class="width2"></td>
    <td class="round4"></td>
</tr>
</table>
</div>
<!-- form-array end //-->
</div>
<div class="form-save">
<div class="btn">
<a href="#" onclick="submitSetup(); return false;" class="submit"></a>
<a href="#" onclick="window.close();" class="cancel"></a>
</div>
</div>
</form>
</body>
</html>