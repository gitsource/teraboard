<?php // 이미지셀렉터
include_once("./_tb.php");
if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "c");

}

$area = area($id);

if (!$area['mid'] || !$area['upload_file']) {

    message("<p class='title'>알림</p><p class='text'>첨부한 이미지가 삭제되었거나 존재하지 않습니다.</p>", "c");

}

$mid = $area['mid'];

$mb = member($mid);

if (!$mb['mid']) {

    message("<p class='title'>알림</p><p class='text'>회원이 존재하지 않습니다.</p>", "c");

}

if ($member['mid'] != $mid && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

if ($member['mid'] != $mid && $check_admin) {

    $check_auth = check_auth("member", 1);

    if ($check_auth != 'full') {

        message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "c");

    }

}

$source = $disk['path']."/area/".data_path("u", $area['upload_time'])."/".$area['upload_file'];

if (file_exists($source) && $area['upload_file']) {

    $source = str_replace($disk['path'], $disk['server'], $source);

} else {

    message("<p class='title'>알림</p><p class='text'>첨부한 이미지가 삭제되었거나 존재하지 않습니다.</p>", "c");

}

$thumb_width = 200;
$thumb_height = 200;
?>
<!DOCTYPE html>
<html>
<head>
<?
include_once("$web[path]/_head.php");
?>
<style type="text/css">
html {overflow:hidden;}
body {background-color:#d7dde4; margin:0px;}

.title {background-color:#a1b1c2; border-bottom:1px solid #7e8e9f;}
.title .text {font-weight:700; line-height:50px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.title .icon, .title .close {width:50px; height:50px; background:url('<?=$web['host_member']?>/img/_imgareaselect_tilte.png') no-repeat;}
.title .close {background-position:-50px 0px;}
.title .close a {display:block; width:50px; height:50px;}

.layout-left {width:274px; height:100%; background-color:#4f5f6f; float:left;}
.layout-wrap {margin-left:274px;}
.layout-contents {position:absolute; left:274px; right:0px;}
.layout-main {min-width:620px; padding:0 30px;}

.area_size {margin:29px 0 0 30px;}
.area_size {font-weight:bold; line-height:15px; font-size:13px; color:#a1b1c2; font-family:gulim,serif;}

#preview_box {margin:15px 0 0 30px; border:7px #384655 solid; position:relative; overflow:hidden; width:<?=$thumb_width?>px; height:<?=$thumb_height?>px;}

.imgareaselect-border1 {background: url('img/border-v.gif') repeat-y left top;}
.imgareaselect-border2 {background: url('img/border-h.gif') repeat-x left top;}
.imgareaselect-border3 {background: url('img/border-v.gif') repeat-y right top;}
.imgareaselect-border4 {background: url('img/border-h.gif') repeat-x left bottom;}
.imgareaselect-border1, .imgareaselect-border2,.imgareaselect-border3, .imgareaselect-border4 {filter: alpha(opacity=50);opacity: 0.5;}
.imgareaselect-handle {background-color: #fff;border: solid 1px #000;filter: alpha(opacity=50);opacity: 0.5;}
.imgareaselect-outer {background-color: #000;filter: alpha(opacity=50);opacity: 0.5;}

.btn {margin:10px 0 0 30px;}
.btn .submit {display:block; width:214px; height:50px; background:url('<?=$web['host_member']?>/img/_imgareaselect_save.png') no-repeat;}
.btn .submit:hover {background-position:0px -50px;}
.btn .submit:active {background-position:0px -100px;}

.text {margin-top:14px;}
.text {font-weight:bold; line-height:44px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}

.source {margin-top:1px;}
.source img {max-width:560px; max-height:460px;}
</style>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.imgareaselect.min.js"></script>
<script type="text/javascript">
var thumb_width = <?=$thumb_width?>;
var thumb_height = <?=$thumb_height?>;

function preview(img, selection)
{

    var source = $('#source');

    var source_width = <?=$area['upload_width']?>;
    var source_height = <?=$area['upload_height']?>;

    var scaleX = thumb_width / selection.width; 
    var scaleY = thumb_height / selection.height; 

    $('#preview').css({width: Math.round(scaleX * source.width()) + 'px', height: Math.round(scaleY * source.height()) + 'px', marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px', marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'});

    var source_scaleX = source_width / source.width();
    var source_scaleY = source_height / source.height();

    $('#x1').val(Math.round(selection.x1 * source_scaleX));
    $('#y1').val(Math.round(selection.y1 * source_scaleY));
    $('#x2').val(Math.round(selection.x2 * source_scaleX));
    $('#y2').val(Math.round(selection.y2 * source_scaleY));
    $('#w').val(selection.width);
    $('#h').val(selection.height);
    $('#area_size').text(selection.width+'*'+selection.height);

}

$(window).load(function () {

    var ratio = thumb_height / thumb_width;
    $('#source').imgAreaSelect({ aspectRatio: '1:'+ratio, onSelectChange: preview }); 

});
</script>
<script type="text/javascript">
function submitSetup()
{

    var f = document.formSetup;

    if (f.x1.value == '' || f.y1.value == '' || f.x2.value == '' || f.y2.value == '' || f.w.value == '' || f.h.value == '') {

        alert("영역을 선택하여주세요."); 
        return false;

    }

    if (parseInt(f.w.value) < '<?=$thumb_width?>' || parseInt(f.h.value) < '<?=$thumb_height?>') {

        alert("크롭 이미지 영역은 <?=$thumb_width?>*<?=$thumb_height?>px 이상으로 선택해야합니다."); 
        return false;

    }

    $('.submit').hide();

    f.m.value = "area_select";

    f.action = "image_update.php";
    f.submit();

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="id" value="<?=$id?>" />
<input type="hidden" id="x1" name="x1" value="" />
<input type="hidden" id="y1" name="y1" value="" />
<input type="hidden" id="x2" name="x2" value="" />
<input type="hidden" id="y2" name="y2" value="" />
<input type="hidden" id="w" name="w" value="" />
<input type="hidden" id="h" name="h" value="" />
</form>
<div class="title">
<table width="100%" height="50" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="icon"></td>
    <td class="text">이미지 셀렉터</td>
    <td class="close"><a href="#" onclick="opener.location.href='image.php?mid=<?=text($mid)?>'; window.close();"></a></td>
</tr>
</table>
</div>
<div class="layout-left">
<div class="area_size">크롭 이미지 (<span id="area_size" >0*0</span>px)</div>
<div id="preview_box"><img src="<?=$source?>" id="preview"></div>
<div class="btn"><a href="#" onclick="submitSetup(); return false;" class="submit"></a></div>
</div>
<div class="layout-wrap">
<div class="layout-contents">
<div class="layout-main">
<div class="text">이 곳의 원본 이미지에 마우스를 드래그하여, 크롭 영역을 선택 하세요.</div>
<div class="source"><img src="<?=text($source)?>" id="source"></div>
</div>
</div>
</div>
</body>
</html>