<?php
include_once("./_tb.php");
if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

}

$file = sql_fetch(" select * from $web[member_photo_table] where id = '".$id."' limit 0, 1 ");

if (!$file['upload_file']) {

    message("<p class='title'>알림</p><p class='text'>파일이 존재하지 않습니다.</p>", "b");

}

if ($member['mid'] != $file['mid'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

if ($member['mid'] != $file['mid'] && $check_admin) {

    $check_auth = check_auth("member", 1);

    if ($check_auth != 'full') {

        message("<p class='title'>알림</p><p class='text'>사용 권한이 없습니다. 관리자에게 문의하세요.</p>", "b");

    }

}

$file_path = $disk['path']."/photo/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
$file_path = addslashes($file_path);

if (!is_file($file_path) || !file_exists($file_path)) {

    message("<p class='title'>알림</p><p class='text'>파일이 존재하지 않습니다.</p>", "b");

}

if (preg_match("/msie/i", $_SERVER[HTTP_USER_AGENT]) || preg_match("/trident/i", $_SERVER[HTTP_USER_AGENT])) {

    $original = addslashes(urlencode($file['upload_source']));

    header("content-type: doesn/matter");
    header("content-length: ".filesize("$file_path"));
    header("content-disposition: attachment; filename=\"$original\"");
    header("content-transfer-encoding: binary");

} else {

    $original = addslashes($file['upload_source']);

    header("content-type: file/unknown");
    header("content-length: ".filesize("$file_path"));
    header("content-disposition: attachment; filename=\"$original\"");
    header("content-description: php generated data");

}

header("pragma: no-cache");
header("expires: 0");
flush();

$fp = fopen("$file_path", "rb");

while(!feof($fp)) {

    echo fread($fp, 100*1024);
    flush();

}

fclose ($fp);
flush();
?>