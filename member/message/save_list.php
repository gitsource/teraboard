<?php // 메세지
include_once("./_tb.php");
if (!$check_login) { url($web['host_member']."/login/?url={$urlencode}"); }

// head start
ob_start();
?>
<link rel="stylesheet" href="<?=$web['host_member']?>/_menu.css" type="text/css" />
<style type="text/css">
body {min-width:320px;}

.layout-block {padding:50px 0;}

.layout-message {background-color:#ffffff;}
.layout-message .btn {clear:both; font-size:0px; padding:10px 0;}
.layout-message .btn a {margin-left:4px; display:inline-block; background-color:#ffffff; border:1px solid #dadada; border-radius:3px; padding:1px 9px 2px 9px;}
.layout-message .btn a:first-child {margin-left:10px;}
.layout-message .btn a {text-decoration:none; font-weight:700; line-height:26px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-message .btn a:hover {border:1px solid #7d7d7d;}
.layout-message .btn a.on {border:1px solid #42abd7; color:#42abd7;}

.layout-message .header {clear:both; position:relative; border-top:1px solid #dadada; padding-top:10px; margin-bottom:10px;}
.layout-message .header .count {font-weight:400; line-height:28px; font-size:13px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
.layout-message .header .count img {vertical-align:middle; margin:0 7px 0 0px;}
.layout-message .header .search {clear:both; position:absolute; right:0px; top:10px; width:206px; vertical-align:top;}
.layout-message .header .search .input, .layout-message .header .search .btn_search {float:left;}
.layout-message .header .search .input {margin-right:4px; padding:0 9px; width:150px; height:28px; border:0px; background-color:#f8f8f8; border:1px solid #dadada;}
.layout-message .header .search .input {font-weight:bold; line-height:28px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-message .header .search .input.focus {border:1px solid #59c2e6; background-color:#ffffff; color:#000000;}
.layout-message .header .search .btn_search {display:block; width:32px; height:32px; background:url('<?=$web['host_member']?>/img/_btn_search_w32_h32.png') no-repeat;}
.layout-message .header .search .btn_search:hover {background-position:0 -32px;}
.layout-message .header .search .btn_search:active {background-position:0 -64px;}
.layout-message .header .search.mobile {display:none;}
.layout-message .header .block {display:none;}
.layout-message .header .block.on {display:block;}
.layout-message .header .block {margin-top:12px; height:30px; padding:10px 10px 0 10px; clear:both; border-top:1px solid #dadada;}
.layout-message .header .block .input, .layout-message .header .block .btn_search {float:left;}
.layout-message .header .block .input {margin-right:4px; padding:0 9px; width:72%; height:28px; border:0px; background-color:#f8f8f8; border:1px solid #dadada;}
.layout-message .header .block .input {font-weight:bold; line-height:28px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-message .header .block .btn_search {display:block; width:56px; height:30px; background:url('<?=$web['host_member']?>/img/_btn_search_w56_h30.png') no-repeat;}
.layout-message .header .hide {display:none;}

.layout-message .list {clear:both;}
.layout-message .list tr.title td {position:relative; text-align:center; font-weight:700; line-height:38px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-message .list tr.title td {border-left:1px solid #dadada;}
.layout-message .list tr.title td:last-child {border-right:1px solid #dadada;}
.layout-message .list tr td.date {width:160px;}
.layout-message .list tr td.nick {width:120px;}
.layout-message .list tr td.option {width:120px;}
.layout-message .list tr.array:hover {background-color:#f7fbfd;}
.layout-message .list tr.array td.date {text-align:center; line-height:50px; font-size:13px; color:#959595; font-family:'Nanum Gothic',gulim,serif;}
.layout-message .list tr.array td.nick {text-align:center; font-weight:bold; line-height:50px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-message .list tr.array td.content {cursor:pointer; text-align:left; line-height:50px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-message .list tr.array td p {margin:0px; text-align:left; overflow:hidden; height:50px; padding:0 10px; word-break:break-all;}
.layout-message .list tr.array td p.center {text-align:center;}
.layout-message .list tr.array td p.icon {position:relative; padding-left:31px;}
.layout-message .list tr.array td p.icon span {position:absolute; left:0; top:10px; display:block; width:30px; height:30px; background:url('<?=$web['host_member']?>/img/message_icon.png') no-repeat -30px 0;}
.layout-message .list tr.array td p.icon.no span {background-position:0 -30px;}
.layout-message .list tr.array td.option {font-size:0px; text-align:center;}
.layout-message .list tr.array td.option a {display:inline-block; width:34px; height:34px; background:url('<?=$web['host_member']?>/img/message_btn_option.png') no-repeat;}
.layout-message .list tr.array td.option a.new {background-position:0 0px;}
.layout-message .list tr.array td.option a.new:hover {background-position:0 -34px;}
.layout-message .list tr.array td.option a.new:active {background-position:0 -68px;}
.layout-message .list tr.array td.option a.memo {background-position:-34px 0px;}
.layout-message .list tr.array td.option a.memo:hover {background-position:-34px -34px;}
.layout-message .list tr.array td.option a.memo:active {background-position:-34px -68px;}
.layout-message .list tr.array td.option a.save {background-position:-68px 0px;}
.layout-message .list tr.array td.option a.save:hover {background-position:-68px -34px;}
.layout-message .list tr.array td.option a.save:active {background-position:-68px -68px;}
.layout-message .list tr.array td.option a.del {background-position:-102px 0px;}
.layout-message .list tr.array td.option a.del:hover {background-position:-102px -34px;}
.layout-message .list tr.array td.option a.del:active {background-position:-102px -68px;}
.layout-message .list tr.array .mobile {display:none;}
.layout-message .list tr.not {text-align:center; font-weight:700; line-height:200px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-message .list tr td.date .date2 {display:none;}


@media screen and (max-width:700px) {

.layout-message .list tr td.date {width:70px;}
.layout-message .list tr td.date .date1 {display:none;}
.layout-message .list tr td.date .date2 {display:block;}

}

@media screen and (max-width:600px) {

.layout-message .btn a:first-child {margin-left:5px;}
.layout-message .header .count img {display:none;}
.layout-message .header .count {margin-left:5px;}
.layout-message .header .search {width:32px; right:5px;}
.layout-message .header .search .input {display:none;}

.layout-message .list tr.line {display:none;}
.layout-message .list tr.title {display:none;}
.layout-message .list tr.array td.nick {display:none;}
.layout-message .list tr.array td.date {display:none;}
.layout-message .list td.nick {display:none;}
.layout-message .list td.option {display:none;}
.layout-message .list tr.array .pc {display:none;}
.layout-message .list tr.array .mobile {position:relative; display:block; padding:13px 9px 12px 39px;}
.layout-message .list tr.array td.content {font-weight:bold; line-height:18px; font-size:14px; color:#363636; font-family:gulim,serif;}
.layout-message .list tr.array td p {height:18px;}
.layout-message .list tr.array td p.icon {padding-left:0px;}
.layout-message .list tr.array span.sideview {line-height:13px; font-size:12px; color:#86a4b0; font-family:gulim,serif;}
.layout-message .list tr.array span.date {line-height:13px; font-size:12px; color:#959595; font-family:gulim,serif;}
.layout-message .list tr.array span.line {padding:0 6px 0 7px; line-height:13px; font-size:11px; color:#dadada; font-family:dotum,serif;}
.layout-message .list tr.array td span.icon {position:absolute; left:5px; top:15px; display:block; width:30px; height:30px; background:url('<?=$web['host_member']?>/img/message_icon.png') no-repeat -30px 0;}
.layout-message .list tr.array td span.icon.no {background-position:0 -30px;}
.layout-message .header .search.pc {display:none;}
.layout-message .header .search.mobile {display:block;}
.layout-message .header .hide {display:block;}

}
</style>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "보관함";
include_once("$web[path]/_top.php");

$colspan = 4;

// 안 읽음
$no = sql_fetch(" select count(id) as cnt from $web[message_receive_table] where mid = '".$member['mid']."' and type = 0 ");

$sql_search = " where mid = '".$member['mid']."' and type = 2 ";

if ($q) {

    $sql_search .= " and (INSTR(fid, '".addslashes($q)."') or INSTR(content, '".addslashes($q)."')) ";

}

$cnt = sql_fetch(" select count(*) as cnt from $web[message_receive_table] $sql_search ");
$total_count = $cnt['cnt'];
$rows = 20;
$total_page  = ceil($total_count / $rows);
if (!$p) { $p = 1; }
$from_record = ($p - 1) * $rows;
$paging = paging(5, $p, $total_page, "?q=".addslashes($q)."&amp;p=");
$result = sql_query(" select * from $web[message_receive_table] $sql_search order by id desc limit $from_record, $rows ");
$list = array();
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $mb = member($row['fid']);

    $list[$i] = $row;
    $list[$i]['nick'] = $mb['nick'];
    $list[$i]['date1'] = text_date("Y. m. d [H:i]", $list[$i]['datetime'], "&nbsp;");

    if (date("Y-m-d", strtotime($list[$i]['datetime'])) == $web['time_ymd']) {

        $list[$i]['date2'] = text_date("H:i", $list[$i]['datetime'], "&nbsp;");

    } else {

        $list[$i]['date2'] = text_date("m.d", $list[$i]['datetime'], "&nbsp;");

    }

    $list[$i]['class'] = "icon";

    if ($row['type'] == 0) {

        $list[$i]['class'] = "icon no bold";

    }

}

$dq = "메세지 검색";

if (!$q) {
    $q = $dq;
}
?>
<script type="text/javascript">
$(document).ready( function() {

    // 메세지
    $('.member-menu div[name=message]').addClass('on');

    $('#mq, .layout-message .header .search .input').focus(function() {

        if ($(this).val() == '<?=text($dq)?>') {

            $(this).val('');
            $(this).addClass('focus');

        }

    }).blur(function() {

        if ($(this).val() == '') {

            $(this).val('<?=text($dq)?>');

        }

        $(this).removeClass('focus');

    });

    var f = document.formUpdate;

    f.table.value = "receive";
    f.return.value = "ok";

});

function memoSearch()
{

    var f = document.formMemo;

    if (f.q.value == '<?=text($dq)?>') {
        f.q.value = '';
    }

    if ($('.layout-message .header .block').is(':hidden') == false) {

        if ($('#mq').val() != '<?=text($dq)?>') {
            f.q.value = $('#mq').val();

        }

    }

    f.submit();

}

function memoSearchLayer()
{

    var obj = $('.layout-message .header .block');

    if (obj.is(':hidden') == true) {

        obj.addClass('on');

        $('#mq').focus();

    } else {

        obj.removeClass('on');

    }

}

function memoSend(mid)
{

    var url = "regist.php";

    if (mid) {

        url += "?mid="+mid;

    }

    win_open(url, "memoSend", "width=620, height=560, scrollbars=yes");

}

function memoOpen(id)
{

    win_open("receive.php?id="+id, "memoOpen"+id, "width=620, height=560, scrollbars=yes");

}

function memoDel(id)
{

    var f = document.formUpdate;

    f.m.value = "del";
    f.id.value = id;

    f.action = "update.php";
    f.submit();

}
</script>
<div class="layout-block">
<div class="layout-message">
<?
// 메뉴 호출
include_once("$web[path]/member/_menu.php");
?>
<div class="btn">
<a href="#" onclick="memoSend(''); return false;">새 메세지</a>
<a href="<?=$web['host_member']?>/message/">받은 메세지</a>
<a href="<?=$web['host_member']?>/message/send_list.php">보낸 메세지</a>
<a href="<?=$web['host_member']?>/message/save_list.php" class="on">보관함</a>
</div>
<div class="header">
<div class="count"><img src="<?=$web['host_member']?>/img/message_title.png">보관 <font color="#363636"><?=number($total_count)?></font>건</div>
<form method="get" name="formMemo">
<div class="search pc"><input type="text" name="q" value="<?=text($q)?>" class="input" /><a href="#" onclick="memoSearch(); return false;" class="btn_search"><input type="image" src="<?=$web['host_img']?>/blank.png" alt="" width="0" height="0" /></a></div>
<div class="search mobile"><a href="#" onclick="memoSearchLayer(); return false;" class="btn_search"></a></div>
<div class="hide">
<div class="block">
<input type="text" id="mq" value="<?=text($q)?>" class="input" /><a href="#" onclick="memoSearch(); return false;" class="btn_search"></a>
</div>
</div>
</form>
</div>
<div class="list">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr class="title">
    <td class="date">받은시각</td>
    <td class="nick">보낸이</td>
    <td class="content">메세지</td>
    <td class="option">관리</td>
</tr>
<tr class="line"><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? for ($i=0; $i<count($list); $i++) { ?>
<tr class="array">
    <td class="date"><span class="date1"><?=$list[$i]['date1']?></span><span class="date2"><?=$list[$i]['date2']?></span></td>
    <td class="nick"><p class="center"><span class="sideview" onclick="sideviewLoad(this, '<?=text($list[$i]['fid'])?>');"><?=text($list[$i]['nick'])?></span></p></td>
    <td class="content">
<div class="pc"><p onclick="memoOpen('<?=text($list[$i]['id'])?>'); return false;" class="<?=$list[$i]['class']?>" title="<?=text($list[$i]['content']);?>"><span></span><?=text($list[$i]['content']);?></p></div>
<div class="mobile">
<p onclick="memoOpen('<?=text($list[$i]['id'])?>'); return false;" class="<?=$list[$i]['class']?>" title="<?=text($list[$i]['content']);?>"><?=text($list[$i]['content']);?></p>
<span class="sideview" onclick="sideviewLoad(this, '<?=text($list[$i]['fid'])?>');"><?=text($list[$i]['nick'])?></span><span class="line">|</span><span class="date"><?=$list[$i]['date2']?></span>
<span class="<?=$list[$i]['class']?>"></span>
</div>
    </td>
    <td class="option">
<a href="#" onclick="memoDel('<?=text($list[$i]['id'])?>'); return false;" class="del" title="삭제"></a>
    </td>
</tr>
<tr><td height="1" bgcolor="#f0f0f0" colspan="<?=$colspan?>"></td></tr>
<? } ?>
<? if (!$i) { ?>
<tr class="not"><td colspan="<?=$colspan?>">데이터가 없습니다.</td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? } ?>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
</table>
</div>
<div class="web-page">
<? if ($total_count && $total_count > $rows) { echo $paging; } ?>
</div>
</div>
</div>
<?
include_once("$web[path]/_bottom.php");
?>