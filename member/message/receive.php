<?php
include_once("./_tb.php");
if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "c");

}

$data = sql_fetch(" select * from $web[message_receive_table] where id = '".$id."' ");

if (!$data['id']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

else if ($member['mid'] != $data['mid']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

if ($data['type'] == 0) {

    sql_query(" update $web[message_receive_table] set type = '1', readtime = '".$web['time_ymdhis']."' where id = '".$id."' ");
    sql_query(" update $web[message_send_table] set readtime = '".$web['time_ymdhis']."' where code = '".$id."' ");

}

else if ($data['type'] == 2 && $data['readtime'] == '0000-00-00 00:00:00') {

    sql_query(" update $web[message_receive_table] set readtime = '".$web['time_ymdhis']."' where id = '".$id."' ");
    sql_query(" update $web[message_send_table] set readtime = '".$web['time_ymdhis']."' where code = '".$id."' ");

}

$mb = member($data['mid']);
$mb2 = member($data['fid']);
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<?
$web['title'] = "받은 메세지";
include_once("$web[path]/_head.php");
?>
<style type="text/css">
body {background-color:#ffffff; min-width:360px;}
.title {position:relative; height:49px; border-bottom:1px solid #3d4753; background-color:#485362;}
.title {font-weight:700; line-height:49px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.title img {vertical-align:middle;}
.title .btn {font-size:0px; position:absolute; right:10px; top:9px; display:block; width:128px; height:30px; text-align:right;}
.title .btn a {display:inline-block; width:32px; height:32px; background:url('<?=$web['host_member']?>/img/message_btn_option2.png') no-repeat;}
.title .btn a.new {background-position:0 0;}
.title .btn a.new:hover {background-position:0 -32px;}
.title .btn a.new:active {background-position:0 -64px;}
.title .btn a.memo {background-position:-32px 0;}
.title .btn a.memo:hover {background-position:-32px -32px;}
.title .btn a.memo:active {background-position:-32px -64px;}
.title .btn a.save {background-position:-64px 0;}
.title .btn a.save:hover {background-position:-64px -32px;}
.title .btn a.save:active {background-position:-64px -64px;}
.title .btn a.del {background-position:-96px 0;}
.title .btn a.del:hover {background-position:-96px -32px;}
.title .btn a.del:active {background-position:-96px -64px;}
.title .btn a.close {background-position:-128px 0;}
.title .btn a.close:hover {background-position:-128px -32px;}
.title .btn a.close:active {background-position:-128px -64px;}

.wrap {padding:20px 20px 0 20px;}

.to {position:relative; height:93px; background:url('<?=$web['host_member']?>/img/_line_w7_h1.png') repeat-x 0 bottom; background-color:#f0f3f6;}
.to ul {font-size:0px;}
.to ul li {display:inline-block; vertical-align:middle;}
.to ul li.subj {margin-left:20px; margin-right:10px; font-weight:700; line-height:93px; font-size:24px; color:#a3b1c0; font-family:'Nanum Gothic',gulim,serif;}
.to ul li p {margin:0px;}
.to ul li .member_thumb {margin-right:10px; position:relative; width:48px; height:48px;}
.to ul li .member_thumb img {position:absolute; top:0; left:0; width:48px; height:48px; border-radius:48px; border:2px solid #a3b1c0;}
.to ul li.block {width:60%;}
.to ul li .nick {line-height:20px; font-size:13px; color:#1192ca; font-family:gulim,serif;}
.to ul li .nick .sideview {font-weight:bold; color:#000000;}
.to ul li .nick nobr {vertical-align:top; display:inline-block; overflow:hidden; height:20px; max-width:100%; text-overflow:ellipsis; word-break:break-all;}
.to ul li .date {line-height:20px; font-size:12px; color:#959595; font-family:dotum,serif;}

.content {min-height:290px; position:relative;}
.content div {padding:20px 10px 20px 10px; word-break:break-all;}
.content div {line-height:20px; font-size:13px; color:#464646; font-family:gulim,굴림;}

.from {clear:both; position:relative; height:93px; background:url('<?=$web['host_member']?>/img/_line_w7_h1.png') repeat-x 0 top;}
.from ul {font-size:0px;}
.from ul li {display:inline-block; vertical-align:middle;}
.from ul li.subj {margin-left:20px; margin-right:10px; font-weight:700; line-height:93px; font-size:24px; color:#959595; font-family:'Nanum Gothic',gulim,serif;}
.from ul li p {margin:0px;}
.from ul li .member_thumb {margin-right:10px; position:relative; width:48px; height:48px;}
.from ul li .member_thumb img {position:absolute; top:0; left:0; width:48px; height:48px; border-radius:48px; border:2px solid #959595;}
.from ul li.block {width:60%;}
.from ul li .nick {line-height:20px; font-size:13px; color:#1192ca; font-family:gulim,serif;}
.from ul li .nick .sideview {font-weight:bold; color:#000000;}
.from ul li .nick nobr {vertical-align:top; display:inline-block; overflow:hidden; height:20px; max-width:100%; text-overflow:ellipsis; word-break:break-all;}
.from ul li .date {line-height:20px; font-size:12px; color:#959595; font-family:dotum,serif;}

@media screen and (max-width:600px) {

.wrap {padding:0px;}
.content {min-height:240px;}
.to ul li.subj span {display:none;}
.from ul li.subj span {display:none;}
.content div {font-size:14px;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

<? if ($data['type'] == 0) { ?>
if (opener.document.getElementById("message-list-<?=text($id)?>")) {
    opener.document.getElementById("message-list-<?=text($id)?>").classList.add('read');
}
<? } ?>

    var f = document.formUpdate;

    f.id.value = "<?=$id?>";
    f.table.value = "receive";
    f.return.value = "close";
    f.popup.value = "1";

});

function memoSave()
{

    var f = document.formUpdate;

    f.m.value = "save";

    f.action = "update.php";
    f.submit();

}

function memoSend(mid)
{

    win_open("regist.php?mid="+mid, "memoSend", "width=620, height=560, scrollbars=yes");

}

function memoSave(id)
{

    var f = document.formUpdate;

    f.m.value = "save";
    f.id.value = id;

    f.action = "update.php";
    f.submit();

}

function memoDel(id)
{

    var f = document.formUpdate;

    f.m.value = "del";
    f.id.value = id;

    f.action = "update.php";
    f.submit();

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div class="title">
<img src="<?=$web['host_member']?>/img/message_title2.png">받은 메세지
<div class="btn">
<a href="#" onclick="memoSend('<?=text($data['fid'])?>'); return false;" title="답장" class="memo"></a>
<a href="#" onclick="memoSave('<?=text($data['id'])?>'); return false;" title="보관" class="save"></a>
<a href="#" onclick="memoDel('<?=text($data['id'])?>'); return false;" title="삭제" class="del"></a>
<a href="#" onclick="window.close(); return false;" title="닫기" class="close"></a>
</div>
</div>
<div class="wrap">
<div class="to">
<ul>
<li class="subj">T<span>o</span>.</li>
<? if ($setup['image_onoff']) { ?>
<li><div class="member_thumb"><?=member_thumb($mb['mid'])?></div></li>
<? } ?>
<li class="block">
<p class="nick"><nobr><span class="sideview" onclick="sideviewLoad(this, '<?=text($mb['mid'])?>');"><?=text($mb['nick'])?></span> (<?=text($mb['uid'])?>)</nobr></p>
<p class="date">열람 : <? if ($data['readtime'] != '0000-00-00 00:00:00') { echo date("Y-m-d [H:i]", strtotime($data['readtime'])); } else { echo date("Y-m-d [H:i]", $web['server_time']); } ?></p>
</li>
</ul>
</div>
<div class="content"><div><?=text2($data['content'], 0);?></div></div>
<div class="from">
<ul>
<li class="subj">F<span>rom</span>.</li>
<? if ($setup['image_onoff']) { ?>
<li><div class="member_thumb"><?=member_thumb($mb2['mid'])?></div></li>
<? } ?>
<li class="block">
<p class="nick"><nobr><span class="sideview" onclick="sideviewLoad(this, '<?=text($mb2['mid'])?>');"><?=text($mb2['nick'])?></span> (<?=text($mb2['uid'])?>)</nobr></p>
<p class="date">발송 : <?=text_date("Y-m-d [H:i]", $data['datetime'], "")?></p>
</li>
</ul>
</div>
</div>
</body>
</html>