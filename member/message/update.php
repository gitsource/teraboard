<?php
include_once("./_tb.php");
if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }
if ($return) { $return = preg_match("/^[a-zA-Z0-9_\-]+$/", $return) ? $return : ""; }
if ($table) { $table = preg_match("/^[a-zA-Z0-9_\-]+$/", $table) ? $table : ""; }
if ($popup) { $popup = preg_match("/^[0-9]+$/", $popup) ? $popup : ""; }
if ($tid) { $tid = preg_match("/^[a-zA-Z0-9_\-,]+$/", $tid) ? $tid : ""; }

// 비회원
if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

// 폼 체크
if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

if ($table == 'receive') {

    $message_table = $web['message_receive_table'];

} else {

    $message_table = $web['message_send_table'];

}

if ($m == 'send') {

    $content = trim(sql_real_escape_string($content));

    // 일괄
    if ($nicks) {

        $n = 0;
        $array = array();
        $row = explode(",", text($nicks));
        for ($i=0; $i<count($row); $i++) {

            $mb = sql_fetch(" select * from $web[member_table] where nick = '".addslashes($row[$i])."' limit 0, 1 ");

            if ($mb['mid'] && !$mb['dropout']) {

                $array[$n] = $mb['mid'];
                $n++;

            }

        }

        for ($i=0; $i<count($array); $i++) {

            $sql_common = "";
            $sql_common .= " set mid = '".$array[$i]."' ";
            $sql_common .= ", fid = '".$member['mid']."' ";
            $sql_common .= ", content = '".$content."' ";
            $sql_common .= ", type = '0' ";
            $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

            sql_query(" insert into $web[message_receive_table] $sql_common ");

            $code = sql_insert_id();

            $sql_common = "";
            $sql_common .= " set mid = '".$member['mid']."' ";
            $sql_common .= ", tid = '".$array[$i]."' ";
            $sql_common .= ", content = '".$content."' ";
            $sql_common .= ", code = '".$code."' ";
            $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

            sql_query(" insert into $web[message_send_table] $sql_common ");

        }

        if ($n > 1) {

            message("<p class='title'>알림</p><p class='text'>".text($n)." 명에게 메세지를 전송하였습니다.</p>", "c");

        }

        else if ($n) {

            message("<p class='title'>알림</p><p class='text'>메세지를 전송하였습니다.</p>", "c");

        } else {

            message("<p class='title'>알림</p><p class='text'>메세지 전송을 실패하였습니다.</p>", "c");

        }

    } else {
    // 개별

        $mb = member($tid);

        if ($mb['dropout']) {

            message("<p class='title'>알림</p><p class='text'>탈퇴한 회원입니다.</p>", "c");

        }

        $check_message = false;

        if ($mb['uid']) {

            $chk = sql_fetch(" select * from $web[message_receive_table] where mid = '".$mb['mid']."' and fid = '".$member['mid']."' and datetime = '".$web['time_ymdhis']."' ");

            if (!$chk['id']) {

                $sql_common = "";
                $sql_common .= " set mid = '".addslashes($mb['mid'])."' ";
                $sql_common .= ", fid = '".addslashes($member['mid'])."' ";
                $sql_common .= ", content = '".$content."' ";
                $sql_common .= ", type = '0' ";
                $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

                sql_query(" insert into $web[message_receive_table] $sql_common ");

                $code = sql_insert_id();

                $sql_common = "";
                $sql_common .= " set mid = '".addslashes($member['mid'])."' ";
                $sql_common .= ", tid = '".addslashes($mb['mid'])."' ";
                $sql_common .= ", content = '".$content."' ";
                $sql_common .= ", code = '".$code."' ";
                $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

                sql_query(" insert into $web[message_send_table] $sql_common ");

                $check_message = true;

            }

        }

        if ($check_message) {

            message("<p class='title'>알림</p><p class='text'>".text($mb['nick'])." 님께 메세지를 전송하였습니다.</p>", "c");

        } else {

            message("<p class='title'>알림</p><p class='text'>메세지 전송을 실패하였습니다.</p>", "c");

        }

    }

}

else if ($m == 'save') {

    $data = sql_fetch(" select * from $web[message_receive_table] where id = '".$id."' ");

    if (!$data['id']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    else if ($member['mid'] != $data['mid']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    sql_query(" update $web[message_receive_table] set readtime = '".$web['time_ymdhis']."', type = '2' where id = '".$id."' ");
    sql_query(" update $web[message_send_table] set readtime = '".$web['time_ymdhis']."' where code = '".$id."' ");

}

else if ($m == 'del') {

    $data = sql_fetch(" select * from $message_table where id = '".$id."' ");

    if (!$data['id']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    else if ($member['mid'] != $data['mid']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    if ($table == 'send') {

        sql_query(" delete from $web[message_receive_table] where id = '".$data['code']."' and readtime = '0000-00-00 00:00:00' ");

    }

    sql_query(" delete from $message_table where id = '".$id."' ");

}

if ($url) {

    $urlencode = urldecode($url);

} else {

    $urlencode = urldecode($_SERVER[REQUEST_URI]);

}

if ($return == 'ok') {

    url($urlencode);

} else {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=$web[charset]\">";
    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "opener.location.reload();";
    echo "window.close();";
    echo "</script>";

}
?>