<?php // 포인트
include_once("./_tb.php");
if (!$check_login) { url($web['host_member']."/login/?url={$urlencode}"); }

// head start
ob_start();
?>
<link rel="stylesheet" href="<?=$web['host_member']?>/_menu.css" type="text/css" />
<style type="text/css">
body {min-width:320px;}

.layout-block {padding:50px 0;}

.layout-point {background-color:#ffffff;}

.layout-point .header {clear:both; position:relative; padding-top:10px; margin-bottom:10px;}
.layout-point .header .count {font-weight:400; line-height:28px; font-size:13px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
.layout-point .header .count img {vertical-align:middle; margin:0 7px 0 0px;}
.layout-point .header .search {clear:both; position:absolute; right:0px; top:10px; width:206px; vertical-align:top;}
.layout-point .header .search .input, .layout-point .header .search .btn_search {float:left;}
.layout-point .header .search .input {margin-right:4px; padding:0 9px; width:150px; height:28px; border:0px; background-color:#f8f8f8; border:1px solid #dadada;}
.layout-point .header .search .input {font-weight:bold; line-height:28px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-point .header .search .input.focus {border:1px solid #59c2e6; background-color:#ffffff; color:#000000;}
.layout-point .header .search .btn_search {display:block; width:32px; height:32px; background:url('<?=$web['host_member']?>/img/_btn_search_w32_h32.png') no-repeat;}
.layout-point .header .search .btn_search:hover {background-position:0 -32px;}
.layout-point .header .search .btn_search:active {background-position:0 -64px;}
.layout-point .header .search.mobile {display:none;}
.layout-point .header .block {display:none;}
.layout-point .header .block.on {display:block;}
.layout-point .header .block {margin-top:12px; height:30px; padding:10px 10px 0 10px; clear:both; border-top:1px solid #dadada;}
.layout-point .header .block .input, .layout-point .header .block .btn_search {float:left;}
.layout-point .header .block .input {margin-right:4px; padding:0 9px; width:72%; height:28px; border:0px; background-color:#f8f8f8; border:1px solid #dadada;}
.layout-point .header .block .input {font-weight:bold; line-height:28px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-point .header .block .btn_search {display:block; width:56px; height:30px; background:url('<?=$web['host_member']?>/img/_btn_search_w56_h30.png') no-repeat;}
.layout-point .header .hide {display:none;}

.layout-point .list {clear:both;}
.layout-point .list tr.title td {position:relative; text-align:center; font-weight:700; line-height:38px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-point .list tr.title td {border-left:1px solid #dadada;}
.layout-point .list tr.title td:last-child {border-right:1px solid #dadada;}
.layout-point .list tr td.date {width:160px;}
.layout-point .list tr td.point1 {width:100px;}
.layout-point .list tr td.point2 {width:100px;}
.layout-point .list tr.array:hover {background-color:#f7fbfd;}
.layout-point .list tr.array td.date {text-align:center; line-height:50px; font-size:13px; color:#959595; font-family:'Nanum Gothic',gulim,serif;}
.layout-point .list tr.array td.point1 {text-align:center; line-height:50px; font-size:13px; color:#0f77b3; font-family:'Nanum Gothic',gulim,serif;}
.layout-point .list tr.array td.point2 {text-align:center; line-height:50px; font-size:13px; color:#d74842; font-family:'Nanum Gothic',gulim,serif;}
.layout-point .list tr.array td.content {text-align:left; font-weight:700; line-height:50px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-point .list tr.array td p {margin:0px; text-align:left; overflow:hidden; height:50px; padding:0 10px; word-break:break-all;}
.layout-point .list tr.array td p.center {text-align:center;}
.layout-point .list tr.array .mobile {display:none;}
.layout-point .list tr.not {text-align:center; font-weight:700; line-height:200px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-point .list tr td.date .date2 {display:none;}

@media screen and (max-width:700px) {

.layout-point .list tr td.date {width:70px;}
.layout-point .list tr td.date .date1 {display:none;}
.layout-point .list tr td.date .date2 {display:block;}

}

@media screen and (max-width:600px) {

.layout-point .header .count img {display:none;}
.layout-point .header .count {margin-left:5px;}
.layout-point .header .search {width:32px; right:5px;}
.layout-point .header .search .input {display:none;}

.layout-point .list tr.line {display:none;}
.layout-point .list tr.title {display:none;}
.layout-point .list tr.array td.date {display:none;}
.layout-point .list tr.array td.point1 {display:none;}
.layout-point .list tr.array td.point2 {display:none;}
.layout-point .list tr.array .pc {display:none;}
.layout-point .list tr.array .mobile {position:relative; display:block; padding:13px 9px 12px 0;}
.layout-point .list tr.array td.content {font-weight:bold; line-height:18px; font-size:14px; color:#363636; font-family:gulim,serif;}
.layout-point .list tr.array td p {height:18px;}
.layout-point .list tr.array span.point {line-height:13px; font-size:12px; color:#959595; font-family:gulim,serif;}
.layout-point .list tr.array span.date {line-height:13px; font-size:12px; color:#959595; font-family:gulim,serif;}
.layout-point .list tr.array span.line {padding:0 6px 0 7px; line-height:13px; font-size:11px; color:#dadada; font-family:dotum,serif;}
.layout-point .header .search.pc {display:none;}
.layout-point .header .search.mobile {display:block;}
.layout-point .header .hide {display:block;}

}
</style>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "받은 포인트";
include_once("$web[path]/_top.php");

$colspan = 4;

$sql_search = " where mid = '".$member['mid']."' ";

if ($q) {

    $sql_search .= " and INSTR(content, '".addslashes($q)."') ";

}

$cnt = sql_fetch(" select count(*) as cnt from $web[member_point_table] $sql_search ");
$total_count = $cnt['cnt'];
$rows = 20;
$total_page  = ceil($total_count / $rows);
if (!$p) { $p = 1; }
$from_record = ($p - 1) * $rows;
$paging = paging(5, $p, $total_page, "?q=".addslashes($q)."&amp;p=");
$result = sql_query(" select * from $web[member_point_table] $sql_search order by id desc limit $from_record, $rows ");
$list = array();
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['date1'] = text_date("Y. m. d [H:i]", $list[$i]['datetime'], "&nbsp;");

    if (date("Y-m-d", strtotime($list[$i]['datetime'])) == $web['time_ymd']) {

        $list[$i]['date2'] = text_date("H:i", $list[$i]['datetime'], "&nbsp;");

    } else {

        $list[$i]['date2'] = text_date("m.d", $list[$i]['datetime'], "&nbsp;");

    }

    if ($row['point'] >= 0) {

        $list[$i]['point1'] = number($row['point'], $setup['point_number']);
        $list[$i]['point2'] = "&nbsp;";
        $list[$i]['point3'] = "<font color='#0f77b3'>".$list[$i]['point1']."</font>";

    } else {

        $list[$i]['point1'] = "&nbsp;";
        $list[$i]['point2'] = str_replace("-", "", number($row['point'], $setup['point_number']));
        $list[$i]['point3'] = "<font color='#d74842'>".number($row['point'], $setup['point_number'])."</font>";

    }

    $list[$i]['content'] = text($row['content']);

    if ($row['ct'] == 3) {

        $list[$i]['content'] = "<a href='".http_bbs(text_split("|", $row['chk'], 1), text_split("|", $row['chk'], 2))."' target='_blank'>".text($row['content'])."</a>";

    }

}

$dq = "적요 검색";

if (!$q) {
    $q = $dq;
}
?>
<script type="text/javascript">
$(document).ready( function() {

    // 포인트
    $('.member-menu div[name=point]').addClass('on');

    $('#mq, .layout-point .header .search .input').focus(function() {

        if ($(this).val() == '<?=text($dq)?>') {

            $(this).val('');
            $(this).addClass('focus');

        }

    }).blur(function() {

        if ($(this).val() == '') {

            $(this).val('<?=text($dq)?>');

        }

        $(this).removeClass('focus');

    });

});

function pointSearch()
{

    var f = document.formPoint;

    if (f.q.value == '<?=text($dq)?>') {
        f.q.value = '';
    }

    if ($('.layout-point .header .block').is(':hidden') == false) {

        if ($('#mq').val() != '<?=text($dq)?>') {
            f.q.value = $('#mq').val();

        }

    }

    f.submit();

}

function pointSearchLayer()
{

    var obj = $('.layout-point .header .block');

    if (obj.is(':hidden') == true) {

        obj.addClass('on');

        $('#mq').focus();

    } else {

        obj.removeClass('on');

    }

}
</script>
<div class="layout-block">
<div class="layout-point">
<?
// 메뉴 호출
include_once("$web[path]/member/_menu.php");
?>
<div class="header">
<div class="count"><img src="<?=$web['host_member']?>/img/point_title.png">내역 <font color="#363636"><?=number($total_count)?></font>건 (내 포인트 <font color="#42abd7"><?=number($member['point'], $setup_join['point_number'])?></font>점)</div>
<form method="get" name="formPoint">
<div class="search pc"><input type="text" name="q" value="<?=text($q)?>" class="input" /><a href="#" onclick="pointSearch(); return false;" class="btn_search"><input type="image" src="<?=$web['host_img']?>/blank.png" alt="" width="0" height="0" /></a></div>
<div class="search mobile"><a href="#" onclick="pointSearchLayer(); return false;" class="btn_search"></a></div>
<div class="hide">
<div class="block">
<input type="text" id="mq" value="<?=text($q)?>" class="input" /><a href="#" onclick="pointSearch(); return false;" class="btn_search"></a>
</div>
</div>
</form>
</div>
<div class="list">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr class="title">
    <td class="date">발생시간</td>
    <td class="point1">지급</td>
    <td class="point2">차감</td>
    <td class="content">적요</td>
</tr>
<tr class="line"><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? for ($i=0; $i<count($list); $i++) { ?>
<tr class="array">
    <td class="date"><span class="date1"><?=$list[$i]['date1']?></span><span class="date2"><?=$list[$i]['date2']?></span></td>
    <td class="point1"><p class="center"><?=$list[$i]['point1']?></p></td>
    <td class="point2"><p class="center"><?=$list[$i]['point2']?></p></td>
    <td class="content">
<div class="pc"><p title="<?=$list[$i]['content']?>"><?=$list[$i]['content']?></p></div>
<div class="mobile">
<p title="<?=$list[$i]['content']?>"><?=$list[$i]['content']?></p>
<p><span class="point"><?=$list[$i]['point3']?></span><span class="line">|</span><span class="date"><?=$list[$i]['date2']?></span></p>
</div>
    </td>
</tr>
<tr><td height="1" bgcolor="#f0f0f0" colspan="<?=$colspan?>"></td></tr>
<? } ?>
<? if (!$i) { ?>
<tr class="not"><td colspan="<?=$colspan?>">데이터가 없습니다.</td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? } ?>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
</table>
</div>
<div class="web-page">
<? if ($total_count && $total_count > $rows) { echo $paging; } ?>
</div>
</div>
</div>
<?
include_once("$web[path]/_bottom.php");
?>