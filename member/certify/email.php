<?php
include_once("./_tb.php");
if ($callback) { $callback = preg_match("/^[a-zA-Z0-9_\-]+$/", $callback) ? $callback : ""; }

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "c");

}

$setup_email_real = setup_email("real");

if ($m == 'send') {

    // 폼 체크
    if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    if (!$setup['email']) {

        echo "<font color='#f26d7d'>운영정보의 대표 메일이 미설정입니다.</font>";
        exit;

    }

    if (!$setup['email_onoff']) {

        echo "<font color='#f26d7d'>이메일 발송 기능을 사용하지 않습니다.</font>";
        exit;

    }

    if ($email) { $email = trim($email); }
    if ($real_id) { $real_id = preg_match("/^[0-9]+$/", $real_id) ? $real_id : ""; }

    if (!$email) {

        echo "<font color='#f26d7d'>이메일 주소를 입력하세요.</font>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[member_table] where email = '".addslashes($email)."' and dropout = 0 limit 0, 1 ");

    if ($chk['email']) {

        echo "<font color='#f26d7d'>이미 사용중인 이메일 주소입니다.</font>";
        exit;

    }

    $chk = sql_fetch(" select count(id) as total_count from $web[real_email_table] where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (1 * 86400))."' ");

    if ($chk['total_count'] >= $web['real_email_count']) {

        echo "<font color='#f26d7d'>일일 인증 횟수를 초과했습니다.<br />내일 다시 시도하세요.</font>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[real_email_table] where id = '".addslashes($real_id)."' limit 0, 1 ");

    if ($chk['id'] && $chk['email'] != $email || !$chk['id']) {

        $code = rand(1000,9999);

        $sql_common = "";
        $sql_common .= " set mid = '".addslashes($member['mid'])."' ";
        $sql_common .= ", email = '".addslashes($email)."' ";
        $sql_common .= ", code = '".addslashes($code)."' ";
        $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[real_email_table] $sql_common ");

        $real_id = sql_insert_id();

        if (!$setup['title']) {

            $setup['title'] = $web['host_default'];

        }

        $title = $setup_email_real['title'];
        $title = str_replace("{메일인증코드}", $code, $title);
        $title = str_replace("{홈페이지명}", $setup['title'], $title);
        $title = str_replace("{도메인주소}", $web['host_default'], $title);
        $title = str_replace("{회사명}", $setup['company'], $title);
        $title = str_replace("{대표자명}", $setup['ceo'], $title);
        $title = str_replace("{대표메일}", $setup['email'], $title);
        $title = str_replace("{대표번호}", $setup['tel'], $title);
        $title = str_replace("{회사주소}", $setup['addr'], $title);

        $content = $setup_email_real['content'];
        $content = str_replace("{메일인증코드}", $code, $content);
        $content = str_replace("{홈페이지명}", $setup['title'], $content);
        $content = str_replace("{도메인주소}", $web['host_default'], $content);
        $content = str_replace("{회사명}", $setup['company'], $content);
        $content = str_replace("{대표자명}", $setup['ceo'], $content);
        $content = str_replace("{대표메일}", $setup['email'], $content);
        $content = str_replace("{대표번호}", $setup['tel'], $content);
        $content = str_replace("{회사주소}", $setup['addr'], $content);

        $sql_common = "";
        $sql_common .= " set mid = '".$member['mid']."' ";
        $sql_common .= ", name = '".addslashes($setup['title'])."' ";
        $sql_common .= ", send_email = '".addslashes($setup['email'])."' ";
        $sql_common .= ", email = '".addslashes($email)."' ";
        $sql_common .= ", title = '".$title."' ";
        $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[email_table] $sql_common ");

        email_send($email, $title, text2(trim($content), 1), $setup['title'], $setup['email'], 1);

        echo "<script type='text/javascript'>";
        echo "$('#real_id').val('".$real_id."');";
        echo "</script>";

        echo "<font color='#1192ca'>이메일로 발송된 인증코드 4자리를<br />입력하신 후, 확인버튼을 클릭하세요.</font>";

        exit;

    } else {

        echo "<font color='#1192ca'>이메일로 발송된 인증코드 4자리를<br />입력하신 후, 확인버튼을 클릭하세요.</font>";
        exit;

    }

}

else if ($m == 'check') {

    // 폼 체크
    if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    if ($email) { $email = trim($email); }
    if ($code) { $code = trim($code); $code = preg_match("/^[0-9]+$/", $code) ? $code : ""; }
    if ($real_id) { $real_id = preg_match("/^[0-9]+$/", $real_id) ? $real_id : ""; }

    if (!$email) {

        echo "<font color='#f26d7d'>이메일 주소를 입력하세요.</font>";
        exit;

    }

    if (!$real_id) {

        echo "<font color='#f26d7d'>인증 코드가 발송되지 않았습니다.</font>";
        exit;

    }

    if (!$code) {

        echo "<font color='#f26d7d'>인증 코드가 올바르지 않습니다.<br />다시 시도하세요.</font>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[real_email_table] where id = '".addslashes($real_id)."' limit 0, 1 ");

    if (!$chk['id']) {

        echo "<font color='#f26d7d'>인증 코드가 발송되지 않았습니다.</font>";
        exit;

    }

    if ($chk['mode']) {

        echo "<font color='#f26d7d'>만기되었습니다.<br />인증 코드를 다시 발송하세요.</font>";
        exit;

    }

    if ($chk['email'] != $email) {

        echo "<font color='#f26d7d'>인증 코드를 다시 발송하세요.</font>";
        exit;

    }

    if ($chk['code'] != $code) {

        echo "<font color='#f26d7d'>인증 코드가 올바르지 않습니다.<br />다시 시도하세요.</font>";
        exit;

    }

    $sql_common = "";
    $sql_common .= " set email = '".$email."' ";
    $sql_common .= ", certify_email = '1' ";

    sql_query(" update $web[member_table] $sql_common where mid = '".$member['mid']."' ");
    sql_query(" update $web[real_email_table] set mode = '1' where id = '".addslashes($real_id)."' ");

    echo "<font color='#1192ca'>인증이 완료되었습니다.<br />감사합니다.</font>";

    echo "<script type='text/javascript'>";
    if ($web['document_domain']) { echo "document.domain = '".$web['document_domain']."';"; }
    echo "$('.submit div').addClass('on');";
    echo "$('.submit div span').html(\"<a href='#' onclick='window.close(); return false;'>닫기</a>\");";
    echo "if (opener.document.getElementById('check_email_real')) { opener.document.getElementById('check_email_real').value = '".$email."'; }";
    echo "if (opener.document.getElementById('email')) { opener.document.getElementById('email').value = '".$email."'; }";
    echo "</script>";

    if ($callback == 'reload') {

        echo "<script type='text/javascript'>";
        echo "opener.location.reload();";
        echo "</script>";

    }

    exit;

}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<?
$web['title'] = "이메일 인증";
include_once("$web[path]/_head.php");
?>
<style type="text/css">
body {background-color:#ffffff; min-width:320px;}
.title {position:relative; height:49px; border-bottom:1px solid #3d4753; background-color:#485362;}
.title {font-weight:700; line-height:49px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.title img {vertical-align:middle;}

.wrap {padding:37px 20px 20px 20px; background-color:#f0f3f6;}

.msg {text-align:center; height:36px; vertical-align:top;}
.msg {font-weight:bold; line-height:18px; font-size:13px; color:#485362; font-family:gulim,serif;}

.email {margin-top:37px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_email.png') no-repeat left 0; background-color:#ffffff;}
.email input {margin-left:40px; width:218px; height:49px; border:0px;}
.email input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.email.on {border:1px solid #1192ca; background-position:0 -49px;}
.email.on input {color:#000000;}
.email span {position:absolute; left:40px; top:0px; display:block; width:100px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.email .send {position:absolute; right:8px; top:8px; display:block; font-size:0px; cursor:pointer; background-color:#ffffff; border:1px solid #dadada; border-radius:3px; padding:1px 7px 2px 7px; text-align:center;}
.email .send {text-decoration:none; font-weight:700; line-height:26px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.email .send:hover {border:1px solid #42abd7; color:#42abd7;}

.code {margin-top:10px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_code.png') no-repeat left 0; background-color:#ffffff;}
.code input {margin-left:40px; width:218px; height:49px; border:0px;}
.code input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.code.on {border:1px solid #1192ca; background-position:0 -49px;}
.code.on input {color:#000000;}
.code span {position:absolute; left:40px; top:0px; display:block; width:100px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}

.submit {padding:20px 20px; background:url('<?=$web['host_member']?>/img/_line_w7_h1.png') repeat-x 0 top;}
.submit div {cursor:pointer; height:59px; border:1px solid #1192ca; background-color:#42abd7;}
.submit div {font-weight:700; line-height:59px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.submit div span {display:block; text-align:center;}
.submit div span a {text-decoration:none; display:block; text-align:center;}
.submit div.on {cursor:default; border:1px solid #b0b0b0; background-color:#f5f6f7; color:#999999;}

.input {height:24px; border:1px solid #dbe1e8; background-color:#f8f8f8; padding:0 6px 0 6px;}
.input .input_focus {margin-top:3px; width:100%; height:20px; border:0px; background:transparent;}
.input .input_focus {font-weight:bold; line-height:20px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.input.focus {border:1px solid #59c2e6; background-color:#ffffff;}
.input.focus .input_focus {color:#000000;}
</style>
<script type="text/javascript">
$(document).ready(function() {

    $('#email, #code').focus(function() {

        $(this).parent().addClass('on');

    }).blur(function() {

        $(this).parent().removeClass('on');

        if ($(this).val() == '') {

            $(this).parent().find('span').show();

        }

    }).keydown(function() {

        $(this).parent().find('span').hide();

    });

    $('.email span, .code span').click(function() {

        $(this).parent().find('input').focus();

    });

    $('.email .send').click(function() {

        findSend();

    });

    $('#email, #code, .submit span').keyup(function(e) {

        if (e.keyCode == 13) {

            findCheck();

        }

    });

    $('.submit span').click(function() {

        findCheck();

    });

});

function findSend()
{

    var f = document.formSetup;

    var email = $('#email');
    var real_id = $('#real_id');

    $.post("email.php", {"m" : "send", "form_check" : f.form_check.value, "email" : email.val(), "real_id" : real_id.val()}, function(data) {

        $("#msg").html(data);

    });

}

function findCheck()
{

    var f = document.formSetup;

    var email = $('#email');
    var code = $('#code');
    var real_id = $('#real_id');

    $.post("email.php", {"m" : "check", "form_check" : f.form_check.value, "callback" : f.callback.value, "email" : email.val(), "code" : code.val(), "real_id" : real_id.val()}, function(data) {

        $("#msg").html(data);

    });

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<form method="post" name="formSetup" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="callback" value="<?=text($callback)?>" />
<input type="hidden" id="real_id" value="" />
<div class="title">
<img src="<?=$web['host_member']?>/img/certify_title_email.png">이메일 인증
</div>
<div class="wrap">
<div class="msg" id="msg">이메일 주소를 입력하신 후<br />코드발송 버튼을 클릭하세요.</div>
<div class="email"><input type="text" id="email" name="email" value="" tabindex="1" /><span>이메일 주소</span><button class="send" tabindex="2">코드발송</button></div>
<div class="code"><input type="text" id="code" name="code" value="" tabindex="3" /><span>인증 코드</span></div>
</div>
<div class="submit"><div><span tabindex="4">확인</span></div></div>
</form>
</body>
</html>