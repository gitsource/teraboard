<?php // 체크
if (!defined("_WEB_")) exit;

/*------------------------------
두 파일에서 같이 체크하기 때문에, 체크하는 코드가 바뀌면 같이 변경합니다.
from.check.php
form_update.php
------------------------------*/

if ($m == 'uid') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#uid').parent().find('span.msg');";
    echo "</script>";

    if (!$_POST['uid']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
        echo "</script>";
        exit;

    }

    if ($uid) { $uid = strtolower(trim($uid)); $uid = preg_match("/^[a-zA-Z0-9_\-]{3,12}+$/", $uid) ? $uid : ""; }

    if (!$uid) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>3~12자의 영문 소문자, 숫자와 특수기호(_)만 사용 가능합니다.</font>\");";
        echo "</script>";
        exit;

    }

    if (preg_match("/^([_\-])/", $uid)) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>맨 앞에 특수기호를 넣을 수 없습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $check_block = false;
    $row = explode("|", $setup['block_id']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] == $uid) {

            $check_block = true;

        }

    }

    if ($check_block) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>사용하실 수 없는 아이디입니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = member_uid($uid);

    if (!$chk['uid'] && $uid) {

        echo "<script type='text/javascript'>";
        echo "$('#check_uid').val('".text($uid)."');";
        echo "$('#uid').val('".text($uid)."');";
        echo "msg.addClass('on').html(\"<font color='#00a651'>사용하실 수 있는 아이디입니다.</font>\");";
        echo "</script>";
        exit;

    } else {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이미 사용중인 아이디입니다.</font>\");";
        echo "</script>";
        exit;

    }

}

else if ($m == 'upw') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#upw').parent().find('span.msg');";
    echo "var icon = $('#upw').parent().find('span.icon');";
    echo "</script>";

    if (!$upw) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
        echo "icon.html('').removeClass('pw1').removeClass('pw2').removeClass('pw3').removeClass('pw4').removeClass('pw5').removeClass('pw6').addClass('pw1');";
        echo "</script>";
        exit;

    }

    if ($upw) { $upw = trim($upw); $upw = preg_match("/^[a-zA-Z0-9`~!@#\$\%^&\*\(\)\-_\=\+\|\\\[\{\]\}\;\:\'\"\<\,\.\>\/\?]{6,16}+$/", $upw) ? $upw : ""; }

    if (!$upw) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>6~16자 대/소문자, 숫자, 기호를 사용하세요.</font>\");";
        echo "icon.html('사용불가').removeClass('pw1').removeClass('pw2').removeClass('pw3').removeClass('pw4').removeClass('pw5').removeClass('pw6').addClass('pw2');";
        echo "</script>";
        exit;

    }

    $n = 0;
    for ($i=0; $i<strlen($upw); $i++) {

        if (substr($upw,0,1) == substr($upw,$i,1)) {

            $n++;

        }

    }

    if ($n >= 6) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>연속된 문자를 6자이상 사용할 수 없습니다.</font>\");";
        echo "icon.html('사용불가').removeClass('pw1').removeClass('pw2').removeClass('pw3').removeClass('pw4').removeClass('pw5').removeClass('pw6').addClass('pw2');";
        echo "</script>";
        exit;

    }

    if (preg_match("/^([0-9])+$/", $upw)) {

        echo "<script type='text/javascript'>";
        echo "$('#check_upw').val('".text($upw)."');";
        echo "msg.addClass('on').html(\"<font color='#969696'>6~16자로 대/소문자, 숫자, 기호 사용을 권장합니다.</font>\");";
        echo "icon.html('위험').removeClass('pw1').removeClass('pw2').removeClass('pw3').removeClass('pw4').removeClass('pw5').removeClass('pw6').addClass('pw3');";
        echo "</script>";
        exit;

    }

    if (preg_match("/^([A-Za-z])+$/", $upw)) {

        echo "<script type='text/javascript'>";
        echo "$('#check_upw').val('".text($upw)."');";
        echo "msg.addClass('on').html(\"<font color='#969696'>6~16자로 대/소문자, 숫자, 기호 사용을 권장합니다.</font>\");";
        echo "icon.html('위험').removeClass('pw1').removeClass('pw2').removeClass('pw3').removeClass('pw4').removeClass('pw5').removeClass('pw6').addClass('pw3');";
        echo "</script>";
        exit;

    }

    if (preg_match("/^([a-z0-9])+$/", $upw)) {

        echo "<script type='text/javascript'>";
        echo "$('#check_upw').val('".text($upw)."');";
        echo "msg.removeClass('on').html(\"\");";
        echo "icon.html('보통').removeClass('pw1').removeClass('pw2').removeClass('pw3').removeClass('pw4').removeClass('pw5').removeClass('pw6').addClass('pw4');";
        echo "</script>";
        exit;

    }

    if (preg_match("/^([a-zA-Z0-9])+$/", $upw)) {

        echo "<script type='text/javascript'>";
        echo "$('#check_upw').val('".text($upw)."');";
        echo "msg.removeClass('on').html(\"\");";
        echo "icon.html('좋음').removeClass('pw1').removeClass('pw2').removeClass('pw3').removeClass('pw4').removeClass('pw5').removeClass('pw6').addClass('pw5');";
        echo "</script>";
        exit;

    }

    if (preg_match("/^([a-zA-Z0-9`~!@#\$\%^&\*\(\)\-_\=\+\|\\\[\{\]\}\;\:\'\"\<\,\.\>\/\?])+$/", $upw)) {

        echo "<script type='text/javascript'>";
        echo "$('#check_upw').val('".text($upw)."');";
        echo "msg.removeClass('on').html(\"\");";
        echo "icon.html('안전').removeClass('pw1').removeClass('pw2').removeClass('pw3').removeClass('pw4').removeClass('pw5').removeClass('pw6').addClass('pw6');";
        echo "</script>";
        exit;

    }

    exit;

}

else if ($m == 'upw_re') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#upw_re').parent().find('span.msg');";
    echo "var icon = $('#upw_re').parent().find('span.icon');";
    echo "</script>";

    if (!$upw_re) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
        echo "icon.removeClass('pwre1').removeClass('pwre2').addClass('pwre1');";
        echo "</script>";
        exit;

    }

    if ($upw != $upw_re) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>비밀번호가 일치하지 않습니다.</font>\");";
        echo "icon.removeClass('pwre1').removeClass('pwre2').addClass('pwre1');";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_upw_re').val('".text($upw_re)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "icon.removeClass('pwre1').removeClass('pwre2').addClass('pwre2');";
    echo "</script>";
    exit;

    exit;

}

else if ($m == 'nick') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#nick').parent().find('span.msg');";
    echo "</script>";

    if (!$_POST['nick']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
        echo "</script>";
        exit;

    }

    if ($nick) { $nick = trim($nick); $nick = preg_match("/^[A-Za-z0-9_가-힣\x20]{1,300}+$/", $nick) ? $nick : ""; }

    if (!$nick) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>한글, 영문, 숫자 그리고 1~10자까지만 사용할 수 있습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $check_block = false;
    $row = explode("|", $setup['block_id']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] == $nick) {

            $check_block = true;

        }

    }

    if ($check_block) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>사용하실 수 없는 닉네임입니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[member_table] where nick = '".addslashes($nick)."' limit 0, 1 ");

    if (!$chk['nick'] && $nick) {

        echo "<script type='text/javascript'>";
        echo "$('#check_nick').val('".text($nick)."');";
        echo "$('#nick').val('".text($nick)."');";
        echo "msg.addClass('on').html(\"<font color='#00a651'>사용하실 수 있는 닉네임입니다.</font>\");";
        echo "</script>";
        exit;

    } else {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이미 사용중인 닉네임입니다.</font>\");";
        echo "</script>";
        exit;

    }

}

else if ($m == 'hp') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#hp').parent().find('span.msg');";
    echo "</script>";

    if ($setup_join['hp'] == 3 && !$setup['sms_onoff']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>SMS문자 발송 기능을 사용하지 않습니다.</font>\");";
        echo "</script>";
        exit;

    }

    // 미사용
    if ($setup_join['hp'] == 0) {

        exit;

    }

    if (!$hp) {

        // 자유입력
        if ($setup_join['hp'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['hp'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

        // 필수입력 + 인증
        else if ($setup_join['hp'] == 3) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($hp) { $hp = str_replace("-", "", trim($hp)); $hp = preg_match("/^[0-9\-]+$/", $hp) ? $hp : ""; }
    if ($check_hp_real) { $check_hp_real = preg_match("/^[0-9]+$/", $check_hp_real) ? $check_hp_real : ""; }

    if (!$hp) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>휴대폰 번호가 잘못되었습니다.</font>\");";
        echo "</script>";
        exit;

    }

    if (!preg_match("/^[0-9\-]{6,20}+$/", $hp)) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>휴대폰 번호가 잘못되었습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[member_table] where hp = '".addslashes($hp)."' and dropout = 0 limit 0, 1 ");

    if (!$chk['hp'] && $hp) {

        // 자유입력, 필수입력
        if ($setup_join['hp'] == 1 || $setup_join['hp'] == 2) {

            echo "<script type='text/javascript'>";
            echo "$('#check_hp').val('".text($hp)."');";
            echo "$('#hp').val('".text($hp)."');";
            echo "msg.addClass('on').html(\"<font color='#00a651'>사용 가능한 휴대폰 번호입니다.</font>\");";
            echo "</script>";
            exit;

        }

        // 인증
        $chk = sql_fetch(" select * from $web[real_hp_table] where id = '".addslashes($check_hp_real)."' limit 0, 1 ");

        if ($chk['id'] && $chk['hp'] != $hp || !$chk['id']) {

            echo "<script type='text/javascript'>";
            echo "$('#check_hp').val('".text($hp)."');";
            echo "$('#hp').val('".text($hp)."');";
            echo "msg.addClass('on').html(\"<font color='#969696'>휴대폰 인증 버튼을 클릭하세요.</font>\");";
            if ($callback) { echo "joinCheck('".text($callback)."');"; }
            echo "</script>";

            exit;

        } else {

            echo "<script type='text/javascript'>";
            echo "$('#check_hp').val('".text($hp)."');";
            echo "$('#hp').val('".text($hp)."');";
            echo "msg.addClass('on').html(\"<font color='#00a651'>휴대폰으로 인증코드가 발송되었습니다.</font>\");";
            if ($callback) { echo "joinCheck('".text($callback)."');"; }
            echo "</script>";
            exit;

        }

    } else {

        echo "<script type='text/javascript'>";
        echo "$('#check_hp').val('');";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이미 사용중인 휴대폰 번호입니다.</font>\");";
        echo "</script>";
        exit;

    }

}

else if ($m == 'hp_real') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#hp').parent().find('span.msg');";
    echo "</script>";

    if (!$setup['sms_onoff']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>SMS문자 발송 기능을 사용하지 않습니다.</font>\");";
        echo "</script>";
        exit;

    }

    // 인증이 아닐 때
    if ($setup_join['hp'] != 3) {

        exit;

    }

    if (!$hp) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
        echo "</script>";
        exit;

    }

    if ($hp) { $hp = str_replace("-", "", trim($hp)); $hp = preg_match("/^[0-9\-]+$/", $hp) ? $hp : ""; }
    if ($check_hp_real) { $check_hp_real = preg_match("/^[0-9]+$/", $check_hp_real) ? $check_hp_real : ""; }

    if (!$hp) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>휴대폰 번호가 잘못되었습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $mb = sql_fetch(" select * from $web[member_table] where hp = '".addslashes($hp)."' and dropout = 0 limit 0, 1 ");

    if ($mb['hp']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이미 사용중인 휴대폰 번호입니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select count(id) as total_count from $web[real_hp_table] where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (1 * 86400))."' ");

    if ($chk['total_count'] >= $web['real_hp_count']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>1일 인증요청횟수를 초과하였습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[real_hp_table] where id = '".addslashes($check_hp_real)."' limit 0, 1 ");

    if ($chk['id'] && $chk['hp'] != $hp || !$chk['id']) {

        $code = rand(1000,9999);

        $sql_common = "";
        $sql_common .= " set hp = '".addslashes($hp)."' ";
        $sql_common .= ", code = '".addslashes($code)."' ";
        $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[real_hp_table] $sql_common ");

        $check_hp_real = sql_insert_id();

        $message = $setup['sms_real'];
        $message = str_replace("{인증코드}", $code, $message);
        $message = str_replace("{홈페이지명}", $setup['title'], $message);

        sms_send($hp, $setup['callernumber'], $message);

        echo "<script type='text/javascript'>";
        echo "$('#check_hp_real').val('".text($check_hp_real)."');";
        echo "msg.addClass('on').html(\"<font color='#00a651'>휴대폰으로 인증코드가 발송되었습니다.</font>\");";
        if ($callback) { echo "joinCheck('".text($callback)."');"; }
        echo "</script>";

        exit;

    } else {

        echo "<script type='text/javascript'>";
        echo "$('#check_hp_real').val('".text($check_hp_real)."');";
        echo "msg.addClass('on').html(\"<font color='#00a651'>휴대폰으로 인증코드가 발송되었습니다.</font>\");";
        if ($callback) { echo "joinCheck('".text($callback)."');"; }
        echo "</script>";
        exit;

    }

}

else if ($m == 'hp_code') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    if ($hp) { $hp = str_replace("-", "", trim($hp)); $hp = preg_match("/^[0-9\-]+$/", $hp) ? $hp : ""; }
    if ($hp_code) { $hp_code = trim($hp_code); $hp_code = preg_match("/^[0-9]+$/", $hp_code) ? $hp_code : ""; }
    if ($check_hp_real) { $check_hp_real = preg_match("/^[0-9]+$/", $check_hp_real) ? $check_hp_real : ""; }

    echo "<script type='text/javascript'>";
    echo "var msg = $('#hp_code').parent().find('span.msg');";
    echo "</script>";

    if (!$check_hp_real) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드가 발송되지 않았습니다.</font>\");";
        echo "</script>";
        exit;

    }

    if (!$hp_code) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[real_hp_table] where id = '".addslashes($check_hp_real)."' limit 0, 1 ");

    if (!$chk['id']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드가 발송되지 않았습니다.</font>\");";
        echo "</script>";
        exit;

    }

    if ($chk['mode']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드를 다시 발송하세요.</font>\");";
        echo "</script>";
        exit;

    }

    if ($chk['hp'] != $hp) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드를 다시 발송하세요.</font>\");";
        echo "</script>";
        exit;

    }

    if ($chk['code'] != $hp_code) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드가 잘못되었습니다.</font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_hp_code').val('".text($check_hp_real)."_".text($hp_code)."_".text($hp)."');";
    echo "msg.addClass('on').html(\"<font color='#00a651'>휴대폰 인증이 완료되었습니다.</font>\");";
    echo "</script>";
    exit;

}

else if ($m == 'email') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#email').parent().find('span.msg');";
    echo "</script>";

    if ($setup_join['email'] == 3 && !$setup['email_onoff']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이메일 발송 기능을 사용하지 않습니다.</font>\");";
        echo "</script>";
        exit;

    }

    // 미사용
    if ($setup_join['email'] == 0) {

        exit;

    }

    if (!$email) {

        // 자유입력
        if ($setup_join['email'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['email'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

        // 필수입력 + 인증
        else if ($setup_join['email'] == 3) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($email) { $email = strtolower(trim($email)); }
    if ($check_email_real) { $check_email_real = preg_match("/^[0-9]+$/", $check_email_real) ? $check_email_real : ""; }

    if (!$email) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이메일이 주소가 잘못되었습니다.</font>\");";
        echo "</script>";
        exit;

    }

    if (!preg_match("/^[a-zA-Z0-9_\-\.]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,6}$/i", $email)) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이메일이 주소가 잘못되었습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[member_table] where email = '".addslashes($email)."' and dropout = 0 limit 0, 1 ");

    if (!$chk['email'] && $email) {

        // 자유입력, 필수입력
        if ($setup_join['email'] == 1 || $setup_join['email'] == 2) {

            echo "<script type='text/javascript'>";
            echo "$('#check_email').val('".text($email)."');";
            echo "$('#email').val('".text($email)."');";
            echo "msg.addClass('on').html(\"<font color='#00a651'>사용 가능한 이메일 주소입니다.</font>\");";
            echo "</script>";
            exit;

        }

        // 인증
        $chk = sql_fetch(" select * from $web[real_email_table] where id = '".addslashes($check_email_real)."' limit 0, 1 ");

        if ($chk['id'] && $chk['email'] != $email || !$chk['id']) {

            echo "<script type='text/javascript'>";
            echo "$('#check_email').val('".text($email)."');";
            echo "$('#email').val('".text($email)."');";
            echo "msg.addClass('on').html(\"<font color='#969696'>이메일 인증 버튼을 클릭하세요.</font>\");";
            if ($callback) { echo "joinCheck('".text($callback)."');"; }
            echo "</script>";

            exit;

        } else {

            echo "<script type='text/javascript'>";
            echo "$('#check_email').val('".text($email)."');";
            echo "$('#email').val('".text($email)."');";
            echo "msg.addClass('on').html(\"<font color='#00a651'>이메일로 인증코드가 발송되었습니다.</font>\");";
            if ($callback) { echo "joinCheck('".text($callback)."');"; }
            echo "</script>";
            exit;

        }

    } else {

        echo "<script type='text/javascript'>";
        echo "$('#check_email').val('');";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이미 사용중인 이메일 주소입니다.</font>\");";
        echo "</script>";
        exit;

    }

}

else if ($m == 'email_real') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#email').parent().find('span.msg');";
    echo "</script>";

    if (!$setup['sms_onoff']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이메일 발송 기능을 사용하지 않습니다.</font>\");";
        echo "</script>";
        exit;

    }

    if (!$setup['email']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>운영정보의 대표 메일이 미설정입니다.</font>\");";
        echo "</script>";
        exit;

    }

    // 인증이 아닐 때
    if ($setup_join['email'] != 3) {

        exit;

    }

    if (!$email) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
        echo "</script>";
        exit;

    }

    if ($email) { $email = strtolower(trim($email)); }
    if ($check_email_real) { $check_email_real = preg_match("/^[0-9]+$/", $check_email_real) ? $check_email_real : ""; }

    if (!$email) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이메일 주소가 잘못되었습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $mb = sql_fetch(" select * from $web[member_table] where email = '".addslashes($email)."' and dropout = 0 limit 0, 1 ");

    if ($mb['email']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>이미 사용중인 이메일 주소입니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select count(id) as total_count from $web[real_email_table] where ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' and datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (1 * 86400))."' ");

    if ($chk['total_count'] >= $web['real_email_count']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>1일 인증요청횟수를 초과하였습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[real_email_table] where id = '".addslashes($check_email_real)."' limit 0, 1 ");

    if ($chk['id'] && $chk['email'] != $email || !$chk['id']) {

        $code = rand(1000,9999);

        $sql_common = "";
        $sql_common .= " set email = '".addslashes($email)."' ";
        $sql_common .= ", code = '".addslashes($code)."' ";
        $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[real_email_table] $sql_common ");

        $check_email_real = sql_insert_id();

        if (!$setup['title']) {

            $setup['title'] = $web['host_default'];

        }

        $title = $setup_email_real['title'];
        $title = str_replace("{메일인증코드}", $code, $title);
        $title = str_replace("{홈페이지명}", $setup['title'], $title);
        $title = str_replace("{도메인주소}", $web['host_default'], $title);
        $title = str_replace("{회사명}", $setup['company'], $title);
        $title = str_replace("{대표자명}", $setup['ceo'], $title);
        $title = str_replace("{대표메일}", $setup['email'], $title);
        $title = str_replace("{대표번호}", $setup['tel'], $title);
        $title = str_replace("{회사주소}", $setup['addr'], $title);

        $content = $setup_email_real['content'];
        $content = str_replace("{메일인증코드}", $code, $content);
        $content = str_replace("{홈페이지명}", $setup['title'], $content);
        $content = str_replace("{도메인주소}", $web['host_default'], $content);
        $content = str_replace("{회사명}", $setup['company'], $content);
        $content = str_replace("{대표자명}", $setup['ceo'], $content);
        $content = str_replace("{대표메일}", $setup['email'], $content);
        $content = str_replace("{대표번호}", $setup['tel'], $content);
        $content = str_replace("{회사주소}", $setup['addr'], $content);

        $sql_common = "";
        $sql_common .= " set name = '".addslashes($setup['title'])."' ";
        $sql_common .= ", send_email = '".addslashes($setup['email'])."' ";
        $sql_common .= ", email = '".addslashes($email)."' ";
        $sql_common .= ", title = '".$title."' ";
        $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[email_table] $sql_common ");

        email_send($email, $title, text2(trim($content), 1), $setup['title'], $setup['email'], 1);

        echo "<script type='text/javascript'>";
        echo "$('#check_email_real').val('".text($check_email_real)."');";
        echo "msg.addClass('on').html(\"<font color='#00a651'>이메일로 인증코드가 발송되었습니다.</font>\");";
        if ($callback) { echo "joinCheck('".text($callback)."');"; }
        echo "</script>";

        exit;

    } else {

        echo "<script type='text/javascript'>";
        echo "$('#check_email_real').val('".text($check_email_real)."');";
        echo "msg.addClass('on').html(\"<font color='#00a651'>이메일로 인증코드가 발송되었습니다.</font>\");";
        if ($callback) { echo "joinCheck('".text($callback)."');"; }
        echo "</script>";
        exit;

    }

}

else if ($m == 'email_code') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    if ($email) { $email = strtolower(trim($email)); }
    if ($email_code) { $email_code = trim($email_code); $email_code = preg_match("/^[0-9]+$/", $email_code) ? $email_code : ""; }
    if ($check_email_real) { $check_email_real = preg_match("/^[0-9]+$/", $check_email_real) ? $check_email_real : ""; }

    echo "<script type='text/javascript'>";
    echo "var msg = $('#email_code').parent().find('span.msg');";
    echo "</script>";

    if (!$check_email_real) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드가 발송되지 않았습니다.</font>\");";
        echo "</script>";
        exit;

    }

    if (!$email_code) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = sql_fetch(" select * from $web[real_email_table] where id = '".addslashes($check_email_real)."' limit 0, 1 ");

    if (!$chk['id']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드가 발송되지 않았습니다.</font>\");";
        echo "</script>";
        exit;

    }

    if ($chk['mode']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드를 다시 발송하세요.</font>\");";
        echo "</script>";
        exit;

    }

    if ($chk['email'] != $email) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드를 다시 발송하세요.</font>\");";
        echo "</script>";
        exit;

    }

    if ($chk['code'] != $email_code) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>인증 코드가 잘못되었습니다.</font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_email_code').val('".text($check_email_real)."_".text($email_code)."_".text($email)."');";
    echo "msg.addClass('on').html(\"<font color='#00a651'>이메일 인증이 완료되었습니다.</font>\");";
    echo "</script>";
    exit;

}

else if ($m == 'name') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#name').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['name'] == 0) {

        exit;

    }

    if (!$name) {

        // 자유입력
        if ($setup_join['name'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['name'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($name) { $name = trim($name); $name = preg_match("/^[가-힣\x20]{1,100}+$/", $name) ? $name : ""; }

    if (!$name) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>성명을 한글로 입력하세요.</font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_name').val('".text($name)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'sex') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#sex').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['sex'] == 0) {

        exit;

    }

    if (!$sex) {

        // 자유입력
        if ($setup_join['sex'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['sex'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($sex) { $sex = preg_match("/^[0-9]+$/", $sex) ? $sex : ""; }

    if (!$sex) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>성별을 입력하세요.</font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_sex').val('".text($sex)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'birth') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#birthmsg');";
    echo "</script>";

    // 미사용
    if ($setup_join['birth'] == 0) {

        exit;

    }

    if (!$birth1 && !$birth2 && !$birth3) {

        // 자유입력
        if ($setup_join['birth'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['birth'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($birth1) { $birth1 = trim($birth1); $birth1 = preg_match("/^[0-9]+$/", $birth1) ? $birth1 : ""; }
    if ($birth2) { $birth2 = trim($birth2); $birth2 = preg_match("/^[0-9]+$/", $birth2) ? $birth2 : ""; }
    if ($birth3) { $birth3 = trim($birth3); $birth3 = preg_match("/^[0-9]+$/", $birth3) ? $birth3 : ""; }

    $birth = date("Y-m-d", strtotime($birth1."-".$birth2."-".$birth3));

    if (!$birth1 || !$birth2 || !$birth3 || $birth1 < 1900 || $birth1 > date("Y", $web['server_time']) || $birth2 < 1 || $birth2 > 12 || $birth3 < 1 || $birth3 > 31 || $birth > $web['time_ymd']) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>생년월일을 정확히 입력하세요.</font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_birth').val('".text($birth)."');";
    echo "$('#birth3').val('".text(sprintf("%02d" , $birth3))."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'tel') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#tel').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['tel'] == 0) {

        exit;

    }

    if (!$tel) {

        // 자유입력
        if ($setup_join['tel'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['tel'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($tel) { $tel = str_replace("-", "", trim($tel)); $tel = preg_match("/^[0-9\-]{6,20}+$/", $tel) ? $tel : ""; }

    if (!$tel) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>번호가 올바르지 않습니다.</font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_tel').val('".text($tel)."');";
    echo "$('#tel').val('".text($tel)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'addr1') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#address').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['addr'] == 0) {

        exit;

    }

    if (!$addr1) {

        // 자유입력
        if ($setup_join['addr'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['addr'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_addr1').val('".text($addr1)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'addr2') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#addr2').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['addr'] == 0) {

        exit;

    }

    if (!$addr2) {

        // 자유입력
        if ($setup_join['addr'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['addr'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_addr2').val('".text($addr2)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'homepage') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#homepage').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['homepage'] == 0) {

        exit;

    }

    if (!$homepage) {

        // 자유입력
        if ($setup_join['homepage'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['homepage'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_homepage').val('".text($homepage)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'recommend') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#recommend').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['recommend'] == 0) {

        exit;

    }

    if (!$recommend) {

        // 자유입력
        if ($setup_join['recommend'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['recommend'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($recommend) { $recommend = strtolower(trim($recommend)); $recommend = preg_match("/^[a-zA-Z0-9_\-]{3,12}+$/", $recommend) ? $recommend : ""; }

    if (!$recommend) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>추천한 아이디가 존재하지 않습니다.</font>\");";
        echo "</script>";
        exit;

    }

    $chk = member_uid($recommend);

    if ($chk['uid'] && $recommend) {

        echo "<script type='text/javascript'>";
        echo "$('#check_recommend').val('".text($recommend)."');";
        echo "$('#recommend').val('".text($recommend)."');";
        echo "msg.removeClass('on').html(\"\");";
        echo "</script>";
        exit;

    } else {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>추천한 아이디가 존재하지 않습니다.</font>\");";
        echo "</script>";
        exit;

    }


}

else if ($m == 'profile') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#profile').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['profile'] == 0) {

        exit;

    }

    if (!$profile) {

        // 자유입력
        if ($setup_join['profile'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['profile'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($profile) { $profile = trim($profile); }

    if (!$profile) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>자기소개를 입력하세요.</font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_profile').val('1');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'text1') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#text1').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['text1'] == 0) {

        exit;

    }

    if (!$text1) {

        // 자유입력
        if ($setup_join['text1'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['text1'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($text1) { $text1 = trim($text1); }

    if (!$text1) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_text1').val('".text($text1)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'text2') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#text2').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['text2'] == 0) {

        exit;

    }

    if (!$text2) {

        // 자유입력
        if ($setup_join['text2'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['text2'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($text2) { $text2 = trim($text2); }

    if (!$text2) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_text2').val('".text($text2)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'text3') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#text3').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['text3'] == 0) {

        exit;

    }

    if (!$text3) {

        // 자유입력
        if ($setup_join['text3'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['text3'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if ($text3) { $text3 = trim($text3); }

    if (!$text3) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "$('#check_text3').val('".text($text3)."');";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'file1') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#file1').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['file1'] == 0) {

        exit;

    }

    if (!$file1) {

        // 자유입력
        if ($setup_join['file1'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['file1'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if (!$file1) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'file2') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#file2').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['file2'] == 0) {

        exit;

    }

    if (!$file2) {

        // 자유입력
        if ($setup_join['file2'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['file2'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if (!$file2) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'file3') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#file3').parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['file3'] == 0) {

        exit;

    }

    if (!$file3) {

        // 자유입력
        if ($setup_join['file3'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['file3'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if (!$file3) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 입력 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'single1') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#single1').parent().parent().parent().parent().parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['single1'] == 0) {

        exit;

    }

    if (!$single1) {

        // 자유입력
        if ($setup_join['single1'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['single1'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 선택 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if (!$single1) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 선택 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'single2') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#single2').parent().parent().parent().parent().parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['single2'] == 0) {

        exit;

    }

    if (!$single2) {

        // 자유입력
        if ($setup_join['single2'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['single2'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 선택 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if (!$single2) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 선택 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == 'single3') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    echo "<script type='text/javascript'>";
    echo "var msg = $('#single3').parent().parent().parent().parent().parent().find('span.msg');";
    echo "</script>";

    // 미사용
    if ($setup_join['single3'] == 0) {

        exit;

    }

    if (!$single3) {

        // 자유입력
        if ($setup_join['single3'] == 1) {

            echo "<script type='text/javascript'>";
            echo "msg.removeClass('on').html(\"\");";
            echo "</script>";
            exit;

        }

        // 필수입력
        else if ($setup_join['single3'] == 2) {

            echo "<script type='text/javascript'>";
            echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 선택 항목입니다.</font>\");";
            echo "</script>";
            exit;

        }

    }

    if (!$single3) {

        echo "<script type='text/javascript'>";
        echo "msg.addClass('on').html(\"<font color='#f26d7d'>필수 선택 항목입니다./font>\");";
        echo "</script>";
        exit;

    }

    echo "<script type='text/javascript'>";
    echo "msg.removeClass('on').html(\"\");";
    echo "</script>";
    exit;

}

else if ($m == '') {

    if (!$service || !$privacy) {

        message("<p class='title'>알림</p><p class='text'>이용약관과 개인정보 취급방침에 모두 동의해주세요.</p>", "b");

    }

}
?>