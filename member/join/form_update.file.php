<?php // 파일폼
if (!defined("_WEB_")) exit;

$tmp_name = $_FILES[$upload_mode]['tmp_name'];
$name = $_FILES[$upload_mode]['name'];
$size = $_FILES[$upload_mode]['size'];
$error = $_FILES[$upload_mode]['error'];

// 100MB 이하일 때만 업로드
if ($size < 104857600) {

    if (is_uploaded_file($tmp_name)) {

        $upload = array();
        $upload['source'] = $name;
        $upload['size'] = $size;

        $name = preg_replace("/\.(php|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $name);

        $upload['file'] = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr(md5(uniqid($web['server_time'])),0,8).'_'.str_replace('%', '', urlencode(substr($name,-10)));

        $dir = $disk['path']."/member/".data_path("", "");

        @mkdir($dir, 0707);
        @chmod($dir, 0707);

        $dest_file = $dir.'/'.$upload['file'];

        $error_code = move_uploaded_file($tmp_name, $dest_file) or die($error);

        @chmod($dest_file, 0606);

        $upload['image'] = @getimagesize($dest_file);

        $sql_common = "";
        $sql_common .= " set mid = '".addslashes($mid)."' ";
        $sql_common .= ", upload_mode = '".addslashes($upload_mode)."' ";
        $sql_common .= ", upload_source = '".trim(strip_tags(sql_real_escape_string($upload['source'])))."' ";
        $sql_common .= ", upload_file = '".$upload['file']."' ";
        $sql_common .= ", upload_filesize = '".$upload['size']."' ";
        $sql_common .= ", upload_width = '".$upload['image'][0]."' ";
        $sql_common .= ", upload_height = '".$upload['image'][1]."' ";
        $sql_common .= ", upload_time = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[member_file_table] $sql_common ");

    }

}
?>