var zindex = 20000;
$(document).ready( function() {

    $('.layout-join .item .block .input_focus, .layout-join .item .block .textarea_focus').focus(function() {

    }).blur(function() {

        if ($(this).val() == '') {

            $(this).parent().find('span.name').show();

        } else {

            $(this).parent().find('span.name').hide();

        }

        var fn = $(this).attr('name');

        if (fn == 'uid' || fn == 'upw' || fn == 'upw_re' || fn == 'nick' || fn == 'hp' || fn == 'hp_code' || fn == 'email' || fn == 'email_code' || fn == 'name' || fn == 'sex' || fn == 'tel' || fn == 'address' || fn == 'addr2' || fn == 'homepage' || fn == 'recommend' || fn == 'profile' || fn == 'text1' || fn == 'text2' || fn == 'text3' || fn == 'single1' || fn == 'single2' || fn == 'single3') {

            joinCheck(fn);

        }

        else if (fn == 'birth1' || fn == 'birth2' || fn == 'birth3') {

            joinCheck('birth');

        }

    }).keydown(function() {

        $(this).parent().find('span.name').hide();

    });

    $('.layout-join .item .block span.name, .layout-join .item .block span.msg, .layout-join .item .block span.help').click(function() {

        $(this).parent().find('input.input_focus').focus();

        var obj = $(this).parent().find('textarea');

        if (obj) {
            obj.show().focus();
        }

    });

    $('.layout-join .btn_hp_real').click(function() {

        joinCheckHpReal();

    });

    $('.layout-join .btn_hp_code').click(function() {

        joinCheckHpCode();

    });

    $('.layout-join .btn_email_real').click(function() {

        joinCheckEmailReal();

    });

    $('.layout-join .btn_email_code').click(function() {

        joinCheckEmailCode();

    });

    $('.layout-join .item .block .btn_male').click(function() {

        $('.layout-join .item .block .btn_male').addClass('on');
        $('.layout-join .item .block .btn_female').removeClass('on');
        $('#sex').val('1');

        $(this).parent().find('span.msg').removeClass('on').html("");

    });

    $('.layout-join .item .block .btn_female').click(function() {

        $('.layout-join .item .block .btn_male').removeClass('on');
        $('.layout-join .item .block .btn_female').addClass('on');
        $('#sex').val('2');

        $(this).parent().find('span.msg').removeClass('on').html("");

    });

    $('#address, #address_name, .layout-join .btn_zipcode').click(function() {

        zipcodeOpen('formSetup', 'zipcode', 'addr1', 'addr2');

    });

    $('.layout-join .item.fileform .block input.input_file').change(function() {

        if ($(this).val() == '') {

            $(this).parent().parent().find('span.name').removeClass('on').text($(this).attr('title'));

            joinCheck($(this).attr('name'));

        } else {

            $(this).parent().parent().find('span.name').addClass('on').text($(this).val());

            joinCheck($(this).attr('name'));

        }

    });

    $('.select .sblock span').click(function() {

        if ($(this).parent().children('div.option').is(':hidden') == true) {

            zindex++;

            $(this).parent().parent().parent('div.selectbox').addClass('opt').zIndex(zindex);

        } else {

            $(this).parent().parent().parent('div.selectbox').removeClass('opt');

        }

    });

    $('.select .sblock .option ul.array li').click(function() {

        $(this).parent().parent().children('input.add').val($(this).attr('name'));
        $(this).parent().parent().parent().children('span.text').text($(this).text());
        $(this).parent().parent().parent().parent().parent('div.selectbox').removeClass('opt');
        $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
        $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg[name="'+$(this).attr('name')+'"]').show();

        if ($(this).attr('name') == '') {

            $(this).parent().parent().parent().parent().find('.sblock').removeClass('on');

        } else {

            $(this).parent().parent().parent().parent().find('.sblock').addClass('on');
            $(this).parent().parent().parent().parent().parent().parent().find('span.msg').removeClass('on').html("");

        }

    });

    $('.selectbox .add').each(function() {

        var addtext = $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').text();

        if (addtext) {

            $(this).parent().parent().children('span.text').text(addtext);
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[name="'+$(this).val()+'"]').show();

        } else {

            $(this).parent().parent().children('span.text').html('&nbsp;');

        }

        if ($(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').attr('name') == '') {

            $(this).parent().parent().parent().find('.sblock').removeClass('on');

        } else {

            $(this).parent().parent().parent().find('.sblock').addClass('on');

        }

    });

    $('.layout-join .item.multiform .block .cblock ul li, .layout-join .item.multiform .block .cblock ul li input').click(function() {

        if ($(this).parent().find('input:checked').length >= $(this).parent().attr('rel')) {

            $(this).parent().parent().parent().find('span.msg').removeClass('on').html("");

        }

        if ($(this).parent().find('input').is(':checked') == true) {

            $(this).parent().addClass('on');

        } else {

            $(this).parent().removeClass('on');

        }

    });

    $('.layout-join .btn_confirm').click(function() {

        submitSetup();

    });

    $(document).click(function(e) {

        if (!$(e.target).is('.selectbox *')) {

            $('.selectbox').removeClass('opt');

        }

    });

});

function joinCheck(id, callback)
{

    if (id == 'uid') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "uid", "uid" : $('#uid').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'upw') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "upw", "upw" : $('#upw').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'upw_re') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "upw_re", "upw" : $('#upw').val(), "upw_re" : $('#upw_re').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'nick') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "nick", "nick" : $('#nick').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'hp') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "hp", "hp" : $('#hp').val(), "check_hp_real" : $('#check_hp_real').val(), "callback" : callback}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'hp_real') {

        //$('#check_'+id).val('');

        $.post("form.php", {"m" : "hp_real", "hp" : $('#hp').val(), "check_hp_real" : $('#check_hp_real').val(), "callback" : callback}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'hp_code') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "hp_code", "hp" : $('#hp').val(), "hp_code" : $('#hp_code').val(), "check_hp_real" : $('#check_hp_real').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'email') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "email", "email" : $('#email').val(), "check_email_real" : $('#check_email_real').val(), "callback" : callback}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'email_real') {

        //$('#check_'+id).val('');

        $.post("form.php", {"m" : "email_real", "email" : $('#email').val(), "check_email_real" : $('#check_email_real').val(), "callback" : callback}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'email_code') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "email_code", "email" : $('#email').val(), "email_code" : $('#email_code').val(), "check_email_real" : $('#check_email_real').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'name') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "name", "name" : $('#name').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'sex') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "sex", "sex" : $('#sex').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'birth' ) {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "birth", "birth1" : $('#birth1').val(), "birth2" : $('#birth2').val(), "birth3" : $('#birth3').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'tel') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "tel", "tel" : $('#tel').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'address') {

        $('#check_addr1').val('');

        $.post("form.php", {"m" : "addr1", "addr1" : $('#addr1').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'addr2') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "addr2", "addr2" : $('#addr2').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'homepage') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "homepage", "homepage" : $('#homepage').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'recommend') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "recommend", "recommend" : $('#recommend').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'profile') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "profile", "profile" : $('#profile').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'text1') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "text1", "text1" : $('#text1').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'text2') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "text2", "text2" : $('#text2').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'text3') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "text3", "text3" : $('#text3').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'file1') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "file1", "file1" : $('#file1').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'file2') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "file2", "file2" : $('#file2').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'file3') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "file3", "file3" : $('#file3').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'single1') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "single1", "single1" : $('#single1').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'single2') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "single2", "single2" : $('#single2').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

    else if (id == 'single3') {

        $('#check_'+id).val('');

        $.post("form.php", {"m" : "single3", "single3" : $('#single3').val()}, function(data) {

            $("#update_data").html(data);

        });

    }

}

function joinCheckHpReal()
{

    if ($('#check_hp').val() == '') {

        joinCheck('hp', 'hp_real');
        return false;

    }

    joinCheck('hp_real');

}

function joinCheckHpCode()
{

    if ($('#check_hp').val() == '') {

        joinCheck('hp', 'hp_real');
        return false;

    }

    if ($('#check_hp_real').val() == '') {

        joinCheck('hp_real', 'hp_code');
        return false;

    }

    joinCheck('hp_code');

}

function joinCheckEmailReal()
{

    if ($('#check_email').val() == '') {

        joinCheck('email', 'email_real');
        return false;

    }

    joinCheck('email_real');

}

function joinCheckEmailCode()
{

    if ($('#check_email').val() == '') {

        joinCheck('email', 'email_real');
        return false;

    }

    if ($('#check_email_real').val() == '') {

        joinCheck('email_real', 'email_code');
        return false;

    }

    joinCheck('email_code');

}

function joinCheckMulti1()
{

    var msg = $('#multi_list1').parent().parent().find('span.msg');
    var n = $('#multi_list1 input:checked').length;

    if (n < check_join_multi1_count || n > check_join_multi1_count) {

        msg.addClass('on').html("<font color='#f26d7d'>"+check_join_multi1_count+"개 선택하세요.</font>");
        return false;

    }

    msg.removeClass('on').html("");

    return true;

}

function joinCheckMulti2()
{

    var msg = $('#multi_list2').parent().parent().find('span.msg');
    var n = $('#multi_list2 input:checked').length;

    if (n < check_join_multi2_count || n > check_join_multi2_count) {

        msg.addClass('on').html("<font color='#f26d7d'>"+check_join_multi2_count+"개 선택하세요.</font>");
        return false;

    }

    msg.removeClass('on').html("");

    return true;

}

function joinCheckMulti3()
{

    var msg = $('#multi_list3').parent().parent().find('span.msg');
    var n = $('#multi_list3 input:checked').length;

    if (n < check_join_multi3_count || n > check_join_multi3_count) {

        msg.addClass('on').html("<font color='#f26d7d'>"+check_join_multi3_count+"개 선택하세요.</font>");
        return false;

    }

    msg.removeClass('on').html("");

    return true;

}

function submitCheck()
{

    var check_join_false = false;

    var f = document.formSetup;

    if (f.check_uid.value == '' || f.check_uid.value != f.uid.value) {

        joinCheck('uid');

        check_join_false = "uid";

    }

    if (f.check_upw.value == '' || f.check_upw.value != f.upw.value) {

        joinCheck('upw');

        check_join_false = "upw";

    }

    if (f.check_upw_re.value == '' || f.check_upw_re.value != f.upw_re.value) {

        joinCheck('upw_re');

        check_join_false = "upw_re";

    }

    if (f.check_nick.value == '' || f.check_nick.value != f.nick.value) {

        joinCheck('nick');

        check_join_false = "nick";

    }

    if (check_join_hp) {

        // 자유
        if (check_join_hp == 1) {

            if (f.hp.value != '' && f.check_hp.value != f.hp.value) {

                joinCheck('hp');

                check_join_false = "hp";

            }

        }

        // 필수
        else if (check_join_hp == 2) {

            if (f.check_hp.value == '' || f.check_hp.value != f.hp.value) {

                joinCheck('hp');

                check_join_false = "hp";

            }

        }

        // 인증
        else if (check_join_hp == 3) {

            if (f.check_hp_real.value == '') {

                joinCheck('hp_real');

                check_join_false = "hp_real";

            }

            var real = f.check_hp_real.value+"_"+f.hp_code.value+"_"+f.hp.value;

            if (f.check_hp_code.value == '' || f.check_hp_code.value != real) {

                joinCheck('hp_code');

                check_join_false = "hp_code";

            }

        }

    }

    if (check_join_email) {

        // 자유
        if (check_join_email == 1) {

            if (f.email.value != '' && f.check_email.value != f.email.value) {

                joinCheck('email');

                check_join_false = "email";

            }

        }

        // 필수
        else if (check_join_email == 2) {

            if (f.check_email.value == '' || f.check_email.value != f.email.value) {

                joinCheck('email');

                check_join_false = "email";

            }

        }

        // 인증
        else if (check_join_email == 3) {

            if (f.check_email_real.value == '') {

                joinCheck('email_real');

                check_join_false = "email_real";

            }

            var real = f.check_email_real.value+"_"+f.email_code.value+"_"+f.email.value;

            if (f.check_email_code.value == '' || f.check_email_code.value != real) {

                joinCheck('email_code');

                check_join_false = "email_code";

            }

        }

    }

    if (check_join_name) {

        // 자유
        if (check_join_name == 1) {

            if (f.name.value != '' && f.check_name.value != f.name.value) {

                joinCheck('name');

                check_join_false = "name";

            }

        }

        // 필수
        else if (check_join_name == 2) {

            if (f.check_name.value == '' || f.check_name.value != f.name.value) {

                joinCheck('name');

                check_join_false = "name";

            }

        }

    }

    if (check_join_sex) {

        // 자유
        if (check_join_sex == 1) {

            if (f.sex.value != '' && f.check_sex.value != f.sex.value) {

                joinCheck('sex');

                check_join_false = "sex";

            }

        }

        // 필수
        else if (check_join_sex == 2) {

            if (f.check_sex.value == '' || f.check_sex.value != f.sex.value) {

                joinCheck('sex');

                check_join_false = "sex";

            }

        }

    }

    if (check_join_birth) {

        // 자유
        if (check_join_birth == 1) {

            if ((f.birth1.value != '' || f.birth2.value != '' || f.birth3.value != '') && f.check_birth.value != f.birth1.value+'-'+f.birth2.value+'-'+f.birth3.value) {

                joinCheck('birth');

                check_join_false = "birth";

            }

        }

        // 필수
        else if (check_join_birth == 2) {

            if (f.check_birth.value == '' || f.check_birth.value != f.birth1.value+'-'+f.birth2.value+'-'+f.birth3.value) {

                joinCheck('birth');

                check_join_false = "birth";

            }

        }

    }

    if (check_join_tel) {

        // 자유
        if (check_join_tel == 1) {

            if (f.tel.value != '' && f.check_tel.value != f.tel.value) {

                joinCheck('tel');

                check_join_false = "tel";

            }

        }

        // 필수
        else if (check_join_tel == 2) {

            if (f.check_tel.value == '' || f.check_tel.value != f.tel.value) {

                joinCheck('tel');

                check_join_false = "tel";

            }

        }

    }

    if (check_join_addr) {

        // 자유
        if (check_join_addr == 1) {

            if (f.addr1.value != '' && f.check_addr1.value != f.addr1.value) {

                joinCheck('address');

                check_join_false = "address";

            }

            if (f.addr2.value != '' && f.check_addr2.value != f.addr2.value) {

                joinCheck('addr2');

                check_join_false = "addr2";

            }

        }

        // 필수
        else if (check_join_addr == 2) {

            if (f.check_addr1.value == '' || f.check_addr1.value != f.addr1.value) {

                joinCheck('address');

                check_join_false = "address";

            }

            if (f.check_addr2.value == '' || f.check_addr2.value != f.addr2.value) {

                joinCheck('addr2');

                check_join_false = "addr2";

            }

        }

    }

    if (check_join_homepage) {

        // 자유
        if (check_join_homepage == 1) {

            if (f.homepage.value != '' && f.check_homepage.value != f.homepage.value) {

                joinCheck('homepage');

                check_join_false = "homepage";

            }

        }

        // 필수
        else if (check_join_homepage == 2) {

            if (f.check_homepage.value == '' || f.check_homepage.value != f.homepage.value) {

                joinCheck('homepage');

                check_join_false = "homepage";

            }

        }

    }

    if (check_join_recommend) {

        // 자유
        if (check_join_recommend == 1) {

            if (f.recommend.value != '' && f.check_recommend.value != f.recommend.value) {

                joinCheck('recommend');

                check_join_false = "recommend";

            }

        }

        // 필수
        else if (check_join_recommend == 2) {

            if (f.check_recommend.value == '' || f.check_recommend.value != f.recommend.value) {

                joinCheck('recommend');

                check_join_false = "recommend";

            }

        }

    }

    if (check_join_profile) {

        // 필수
        if (check_join_profile == 2) {

            if (f.check_profile.value == '') {

                joinCheck('profile');

                check_join_false = "profile";

            }

        }

    }

    if (check_join_text1) {

        // 자유
        if (check_join_text1 == 1) {

            if (f.text1.value != '' && f.check_text1.value != f.text1.value) {

                joinCheck('text1');

                check_join_false = "text1";

            }

        }

        // 필수
        else if (check_join_text1 == 2) {

            if (f.check_text1.value == '' || f.check_text1.value != f.text1.value) {

                joinCheck('text1');

                check_join_false = "text1";

            }

        }

    }

    if (check_join_text2) {

        // 자유
        if (check_join_text2 == 1) {

            if (f.text2.value != '' && f.check_text2.value != f.text2.value) {

                joinCheck('text2');

                check_join_false = "text2";

            }

        }

        // 필수
        else if (check_join_text2 == 2) {

            if (f.check_text2.value == '' || f.check_text2.value != f.text2.value) {

                joinCheck('text2');

                check_join_false = "text2";

            }

        }

    }

    if (check_join_text3) {

        // 자유
        if (check_join_text3 == 1) {

            if (f.text3.value != '' && f.check_text3.value != f.text3.value) {

                joinCheck('text3');

                check_join_false = "text3";

            }

        }

        // 필수
        else if (check_join_text3 == 2) {

            if (f.check_text3.value == '' || f.check_text3.value != f.text3.value) {

                joinCheck('text3');

                check_join_false = "text3";

            }

        }

    }

    if (check_join_file1) {

        // 필수
        if (check_join_file1 == 2) {

            if (f.file1.value == '') {

                joinCheck('file1');

                check_join_false = "file1";

            }

        }

    }

    if (check_join_file2) {

        // 필수
        if (check_join_file2 == 2) {

            if (f.file2.value == '') {

                joinCheck('file2');

                check_join_false = "file2";

            }

        }

    }

    if (check_join_file3) {

        // 필수
        if (check_join_file3 == 2) {

            if (f.file3.value == '') {

                joinCheck('file3');

                check_join_false = "file3";

            }

        }

    }

    if (check_join_single1) {

        // 필수
        if (check_join_single1 == 2) {

            if (f.single1.value == '') {

                joinCheck('single1');

                check_join_false = "single1";

            }

        }

    }

    if (check_join_single2) {

        // 필수
        if (check_join_single2 == 2) {

            if (f.single2.value == '') {

                joinCheck('single2');

                check_join_false = "single2";

            }

        }

    }

    if (check_join_single3) {

        // 필수
        if (check_join_single3 == 2) {

            if (f.single3.value == '') {

                joinCheck('single3');

                check_join_false = "single3";

            }

        }

    }

    if (check_join_multi1) {

        // 필수
        if (check_join_multi1 == 2) {

            if (!joinCheckMulti1()) {

                check_join_false = "multi1";

            }

        }

    }

    if (check_join_multi2) {

        // 필수
        if (check_join_multi2 == 2) {

            if (!joinCheckMulti2()) {

                check_join_false = "multi2";

            }

        }

    }

    if (check_join_multi3) {

        // 필수
        if (check_join_multi3 == 2) {

            if (!joinCheckMulti3()) {

                check_join_false = "multi3";

            }

        }

    }

    if (check_join_false) {

        return false;

    } else {

        return true;

    }

}

function submitSetup()
{

    // 검사
    submitCheck();

    setTimeout( function() {

        if (submitCheck()) {

            var f = document.formSetup;

            f.action = "form_update.php";
            f.submit();

        }

    }, 100);

}
