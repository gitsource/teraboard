<?php // 회원가입
include_once("./_tb.php");
if ($check_login) { url($web['host_default'])."/"; }
if ($service) { $service = preg_match("/^[0-9]+$/", $service) ? $service : ""; }
if ($privacy) { $privacy = preg_match("/^[0-9]+$/", $privacy) ? $privacy : ""; }
if ($receive) { $receive = preg_match("/^[0-9]+$/", $receive) ? $receive : ""; }

$setup_join = setup_join();
$setup_email_real = setup_email("real");

// check
include_once("./form.check.php");

// head start
ob_start();
?>
<link rel="stylesheet" href="form.css" type="text/css" />
<script type="text/javascript">
var check_join_hp = <?=text($setup_join['hp'])?>;
var check_join_email = <?=text($setup_join['email'])?>;
var check_join_name = <?=text($setup_join['name'])?>;
var check_join_sex = <?=text($setup_join['sex'])?>;
var check_join_birth = <?=text($setup_join['birth'])?>;
var check_join_tel = <?=text($setup_join['tel'])?>;
var check_join_addr = <?=text($setup_join['addr'])?>;
var check_join_homepage = <?=text($setup_join['homepage'])?>;
var check_join_recommend = <?=text($setup_join['recommend'])?>;
var check_join_profile = <?=text($setup_join['profile'])?>;
var check_join_text1 = <?=text($setup_join['text1'])?>;
var check_join_text2 = <?=text($setup_join['text2'])?>;
var check_join_text3 = <?=text($setup_join['text3'])?>;
var check_join_file1 = <?=text($setup_join['file1'])?>;
var check_join_file2 = <?=text($setup_join['file2'])?>;
var check_join_file3 = <?=text($setup_join['file3'])?>;
var check_join_single1 = <?=text($setup_join['single1'])?>;
var check_join_single2 = <?=text($setup_join['single2'])?>;
var check_join_single3 = <?=text($setup_join['single3'])?>;
var check_join_multi1 = <?=text($setup_join['multi1'])?>;
var check_join_multi2 = <?=text($setup_join['multi2'])?>;
var check_join_multi3 = <?=text($setup_join['multi3'])?>;
var check_join_multi1_count = <?=text($setup_join['multi1_count'])?>;
var check_join_multi2_count = <?=text($setup_join['multi2_count'])?>;
var check_join_multi3_count = <?=text($setup_join['multi3_count'])?>;
</script>
<script type="text/javascript" src="form.js"></script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("$web[path]/_top.php");
?>
<form method="post" name="formSetup" enctype="multipart/form-data" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" id="url" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<input type="hidden" name="receive" value="<?=text($receive)?>" />
<input type="hidden" id="check_uid" value="" />
<input type="hidden" id="check_upw" value="" />
<input type="hidden" id="check_upw_re" value="" />
<input type="hidden" id="check_nick" value="" />
<input type="hidden" id="check_hp" value="" />
<input type="hidden" id="check_hp_real" name="check_hp_real" value="" />
<input type="hidden" id="check_hp_code" name="check_hp_code" value="" />
<input type="hidden" id="check_email" value="" />
<input type="hidden" id="check_email_real" name="check_email_real" value="" />
<input type="hidden" id="check_email_code" name="check_email_code" value="" />
<input type="hidden" id="check_name" value="" />
<input type="hidden" id="check_sex" value="" />
<input type="hidden" id="check_birth" value="" />
<input type="hidden" id="check_tel" value="" />
<input type="hidden" id="check_addr1" value="" />
<input type="hidden" id="check_addr2" value="" />
<input type="hidden" id="check_homepage" value="" />
<input type="hidden" id="check_recommend" value="" />
<input type="hidden" id="check_profile" value="" />
<input type="hidden" id="check_text1" value="" />
<input type="hidden" id="check_text2" value="" />
<input type="hidden" id="check_text3" value="" />
<input type="hidden" id="zipcode" name="zipcode" value="" />
<input type="hidden" id="addr1" name="addr1" value="" />
<div class="layout-join">
<div class="title"></div>
<div class="tab"><div><span class="block"><span class="icon1"></span>약관 동의</span></div><div class="on"><span class="block"><span class="icon2"></span>정보입력</span></div><div class="last"><span class="block"><span class="icon3"></span>가입완료</span></div></div>
<!-- box start //-->
<div class="box first">
<div class="line">
<div class="item"><div class="block"><input type="text" id="uid" name="uid" value="" class="input_focus" maxlength="12" /><span class="name">아이디</span><span class="msg"></span></div></div>
<div class="item"><div class="block"><input type="password" id="upw" name="upw" value="" class="input_focus" maxlength="16" /><span class="name">비밀번호</span><span class="msg"></span><span class="icon pw1"></span></div></div>
<div class="item"><div class="block"><input type="password" id="upw_re" name="upw_re" value="" class="input_focus" maxlength="16" /><span class="name">비밀번호 재확인</span><span class="msg"></span><span class="icon pwre1"></span></div></div>
<div class="item"><div class="block"><input type="text" id="nick" name="nick" value="" class="input_focus" /><span class="name">닉네임</span><span class="msg"></span></div></div>
</div>
</div>
<!-- box end //-->
<!-- box start //-->
<div class="box">
<div class="line">
<? if ($setup_join['hp']) { ?>
<div class="item"><div class="block"><input type="number" id="hp" name="hp" value="" class="input_focus" /><span class="name">휴대폰</span><span class="msg"></span><? if ($setup_join['hp'] == 3) { echo "<span class='btn btn_hp_real'>인증</span>"; } ?></div></div>
<? if ($setup_join['hp'] == 3) { ?>
<div class="item"><div class="block"><input type="number" id="hp_code" name="hp_code" value="" class="input_focus" /><span class="name">휴대폰 인증코드</span><span class="msg"></span><span class="btn btn_hp_code">확인</span></div></div>
<? } ?>
<? } ?>
<? if ($setup_join['email']) { ?>
<div class="item"><div class="block"><input type="text" id="email" name="email" value="" class="input_focus" /><span class="name">이메일</span><span class="msg"></span><? if ($setup_join['email'] == 3) { echo "<span class='btn btn_email_real'>인증</span>"; } ?></div></div>
<? if ($setup_join['email'] == 3) { ?>
<div class="item"><div class="block"><input type="number" id="email_code" name="email_code" value="" class="input_focus" /><span class="name">이메일 인증코드</span><span class="msg"></span><span class="btn btn_email_code">확인</span></div></div>
<? } ?>
<? } ?>
<? if ($setup_join['name']) { ?>
<div class="item"><div class="block"><input type="text" id="name" name="name" value="" class="input_focus" maxlength="50" /><span class="name">성명</span><span class="msg"></span></div></div>
<? } ?>
<? if ($setup_join['sex']) { ?>
<div class="item sex"><div class="block"><input type="hidden" id="sex" name="sex" value="" class="input_focus" /><span class="btn_male">남자</span><span class="btn_female">여자</span><span class="name">성별</span><span class="msg"></span></div></div>
<? } ?>
<? if ($setup_join['birth']) { ?>
<div class="item birth">
<div class="outside"><div class="block"><input type="number" id="birth1" name="birth1" value="" class="input_focus" maxlength="4" /><span class="name">생년</span></div></div>
<div class="outside"><div class="block"><select id="birth2" name="birth2"><option value="">월</option><option value="01">1월</option><option value="02">2월</option><option value="03">3월</option><option value="04">4월</option><option value="05">5월</option><option value="06">6월</option><option value="07">7월</option><option value="08">8월</option><option value="09">9월</option><option value="10">10월</option><option value="11">11월</option><option value="12">12월</option></select></div></div>
<div class="outside"><div class="block"><input type="number" id="birth3" name="birth3" value="" class="input_focus" maxlength="2" /><span class="name">일</span></div></div>
<span class="msg" id="birthmsg"></span>
</div>
<? } ?>
<? if ($setup_join['tel']) { ?>
<div class="item"><div class="block"><input type="number" id="tel" name="tel" value="" class="input_focus" maxlength="50" /><span class="name">일반전화</span><span class="msg"></span></div></div>
<? } ?>
<? if ($setup_join['addr']) { ?>
<div class="item"><div class="block"><input type="text" id="address" name="address" value="" class="input_focus" readonly /><span id="address_name" class="name">주소(시/군/구)</span><span class="msg"></span><span class="btn btn_zipcode">우편번호</span></div></div>
<? } ?>
<? if ($setup_join['addr']) { ?>
<div class="item"><div class="block"><input type="text" id="addr2" name="addr2" value="" class="input_focus" /><span class="name">상세주소(번지/호)</span><span class="msg"></span></div></div>
<? } ?>
<? if ($setup_join['homepage']) { ?>
<div class="item"><div class="block"><input type="text" id="homepage" name="homepage" value="" class="input_focus" /><span class="name">홈페이지</span><span class="msg"></span></div></div>
<? } ?>
<? if ($setup_join['recommend']) { ?>
<div class="item"><div class="block"><input type="text" id="recommend" name="recommend" value="" class="input_focus" /><span class="name">추천인 아이디</span><span class="msg"></span></div></div>
<? } ?>
<? if ($setup_join['profile']) { ?>
<div class="item profile"><div class="block"><span class="name" tabindex="3">자기소개</span><textarea id="profile" name="profile" value="" class="textarea_focus" /></textarea><span class="msg"></span></div></div>
<? } ?>
<? for ($i=1; $i<=3; $i++) { ?>
<? if ($setup_join['text'.$i]) { ?>
<div class="item"><div class="block"><span class="name"><?=text($setup_join['text'.$i.'_title'])?></span><input type="text" id="text<?=$i?>" name="text<?=$i?>" value="" class="input_focus" /><span class="help"><?=text($setup_join['text'.$i.'_help'])?></span><span class="msg"></span></div></div>
<? } ?>
<? } ?>
<? for ($i=1; $i<=3; $i++) { ?>
<? if ($setup_join['file'.$i]) { ?>
<div class="item fileform"><div class="block"><span class="name"><?=text($setup_join['file'.$i.'_title'])?></span><input type="file" id="file<?=$i?>" name="file<?=$i?>" value="" class="input_file" title="<?=text($setup_join['file'.$i.'_title'])?>" /><span class="help"><?=text($setup_join['file'.$i.'_help'])?></span><span class="msg"></span></div></div>
<? } ?>
<? } ?>
<? for ($i=1; $i<=3; $i++) { ?>
<? if ($setup_join['single'.$i]) { ?>
<div class="item singleform">
<div class="block">
<span class="name"><?=text($setup_join['single'.$i.'_title'])?></span>
<!-- selectbox //-->
<div class="selectbox">
<div class="select">
<div class="sblock">
<span class="text"></span>
<div class="option"><input type="hidden" id="single<?=$i?>" name="single<?=$i?>" value="" class="add" />
<ul class="array">
<li name="" class="first">선택하세요.</li>
<?
$row = explode("|", $setup_join['single'.$i.'_list']);
for ($k=0; $k<count($row); $k++) {

    if ($row[$k]) {
        echo "<li name='".text($row[$k])."'>".text($row[$k])."</li>";
    }

}
?>
</ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
<span class="help"><?=text($setup_join['single'.$i.'_help'])?></span>
<span class="msg"></span>
</div>
</div>
<? } ?>
<? } ?>
<? for ($i=1; $i<=3; $i++) { ?>
<? if ($setup_join['multi'.$i]) { ?>
<div class="item multiform">
<div class="block">
<span class="name"><?=text($setup_join['multi'.$i.'_title'])?> (<?=text($setup_join['multi'.$i.'_count'])?>개 선택)</span>
<span class="help"><?=text($setup_join['multi'.$i.'_help'])?></span>
<div class="cblock">
<ul id="multi_list<?=$i?>" rel="<?=text($setup_join['multi'.$i.'_count'])?>">
<?
$row = explode("|", $setup_join['multi'.$i.'_list']);
for ($k=0; $k<count($row); $k++) {

    if ($row[$k]) {
        echo "<li><input type='checkbox' id='multi".$i."_choice".$k."' name='multi".$i."_choice".$k."' value='".text($row[$k])."' /><label for='multi".$i."_choice".$k."'>".text($row[$k])."</label></li>";
    }

}
?>
</ul>
</div>
<span class="msg"></span>
</div>
</div>
<? } ?>
<? } ?>
</div>
</div>
<!-- box end //-->
<div class="submit"><span class="btn_confirm">가입하기</span></div>
<div class="menu">
<a href="<?=$web['host_member']?>/login/">로그인</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/id_hp.php">아이디 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/pw_hp.php">비밀번호 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/join/">회원가입</a>
</div>
</div>
</form>
<?
include_once("$web[path]/_bottom.php");
?>