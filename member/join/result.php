<?php // 회원가입
include_once("./_tb.php");
if (!$check_login) { url($web['host_default'])."/"; }

if (!get_session("join_result")) {

    url($web['host_default'])."/";

}

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px; min-height:654px;}

.layout-join {padding:50px 0;}

.layout-join .title {position:relative; margin:0 auto; margin-bottom:19px; width:162px; height:162px; background:url('<?=$web['host_member']?>/img/join_title.png') no-repeat;}

.layout-join .tab {height:51px; border-bottom:1px solid #ffffff;}
.layout-join .tab div {display:inline-block; width:33.3%;}
.layout-join .tab div span.block {position:relative; text-align:center; display:block; height:49px; background-color:#eff0f2; border:1px solid #e0e1e3;}
.layout-join .tab div span.block {text-decoration:none; font-weight:bold; line-height:49px; font-size:16px; color:#bdc3c7; font-family:gulim,serif;}
.layout-join .tab div span.icon1,
.layout-join .tab div span.icon2,
.layout-join .tab div span.icon3 {position:relative; overflow:hidden; right:5px; top:2px; display:inline-block; width:16px; height:16px; background:url('<?=$web['host_member']?>/img/join_step.png') no-repeat;}
.layout-join .tab div span.icon2 {background-position:0 -16px;}
.layout-join .tab div span.icon3 {background-position:0 -32px;}

.layout-join .tab div.on {width:33.4%;}
.layout-join .tab div.on span.block {background-color:#bdc3c7; border:1px solid #abb1b5; color:#ffffff;}
.layout-join .tab div.on span.icon1 {background-position:-16px 0;}
.layout-join .tab div.on span.icon2 {background-position:-16px -16px;}
.layout-join .tab div.on span.icon3 {background-position:-16px -32px;}

.layout-join .msg {text-align:center; padding-top:67px;}
.layout-join .msg p {margin:0px;}
.layout-join .msg p.text {font-weight:700; line-height:36px; font-size:36px; color:#333333; font-family:'Nanum Gothic',gulim,serif;}
.layout-join .msg p.text2 {margin-top:25px; font-weight:bold; line-height:22px; font-size:13px; color:#858a8d; font-family:gulim,serif;}
.layout-join .msg p.text3 {padding:0 10px; margin-top:20px; line-height:23px; font-size:13px; color:#999999; font-family:gulim,serif;}

.layout-join .submit {cursor:pointer; margin-top:44px; height:59px; border:1px solid #1192ca; background-color:#42abd7;}
.layout-join .submit {font-weight:700; line-height:59px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-join .submit span {display:block; text-align:center;}
.layout-join .submit.on {cursor:default; border:1px solid #b0b0b0; background-color:#f5f6f7; color:#999999;}

@media screen and (max-width:600px) {

.layout-join .tab div span.icon1,
.layout-join .tab div span.icon2,
.layout-join .tab div span.icon3 {display:none;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    $('.layout-join .submit span').click(function() {

        location.replace("<?=$web['host_default']?>/");

    });

});
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("$web[path]/_top.php");
?>
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" id="url" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<div class="layout-join">
<div class="title"></div>
<div class="tab"><div><span class="block"><span class="icon1"></span>약관 동의</span></div><div><span class="block"><span class="icon2"></span>정보입력</span></div><div class="last on"><span class="block"><span class="icon3"></span>가입완료</span></div></div>
<div class="msg">
<p class="text">환영합니다!</p>
<p class="text2"><?=text($member['nick'])?>님의 가입을 진심으로 축하합니다.<br /><?=text($setup['title'])?>의 아이디는 <?=text($member['uid'])?>입니다.</p>
<p class="text3">저희 운영진은 홈페이지는 물론, 전화/메일 등을 통하여 회원님의 정보를 요구하지 않으며, 무엇보다 소중한 개인정보를 안전하게 지키고자 노력하겠습니다. 감사합니다.</p>
</div>
<div class="submit"><span tabindex="1">시작하기</span></div>
</div>
</form>
<?
include_once("$web[path]/_bottom.php");
?>