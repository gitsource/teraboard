<?php // 회원가입
include_once("./_tb.php");
if ($check_login) { url($web['host_default'])."/"; }

$page_service = page("service");
$page_service['page_content'] = str_replace("{홈페이지명}", $setup['title'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{도메인주소}", $web['host_default'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{회사명}", $setup['company'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{대표자명}", $setup['ceo'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{사업자등록번호}", $setup['number1'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{통신판매업신고번호}", $setup['number2'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{대표메일}", $setup['email'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{대표번호}", $setup['tel'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{팩스번호}", $setup['fax'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{회사주소}", $setup['addr'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{개인정보책임자명}", $setup['privace_name'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{개인정보책임자메일}", $setup['privace_email'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{닉네임}", $member['nick'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{회원ID}", $member['uid'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{성명}", $member['name'], $page_service['page_content']);
$page_service['page_content'] = str_replace("{가입일}", text_date("Y년 m월 d일", $member['datetime'], ""), $page_service['page_content']);

$page_privacy = page("privacy");
$page_privacy['page_content'] = str_replace("{홈페이지명}", $setup['title'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{도메인주소}", $web['host_default'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{회사명}", $setup['company'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{대표자명}", $setup['ceo'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{사업자등록번호}", $setup['number1'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{통신판매업신고번호}", $setup['number2'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{대표메일}", $setup['email'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{대표번호}", $setup['tel'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{팩스번호}", $setup['fax'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{회사주소}", $setup['addr'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{개인정보책임자명}", $setup['privace_name'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{개인정보책임자메일}", $setup['privace_email'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{닉네임}", $member['nick'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{회원ID}", $member['uid'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{성명}", $member['name'], $page_privacy['page_content']);
$page_privacy['page_content'] = str_replace("{가입일}", text_date("Y년 m월 d일", $member['datetime'], ""), $page_privacy['page_content']);

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px; min-height:654px;}

.layout-join {padding:50px 0;}

.layout-join .title {position:relative; margin:0 auto; margin-bottom:19px; width:162px; height:162px; background:url('<?=$web['host_member']?>/img/join_title.png') no-repeat;}

.layout-join .tab {height:51px; border-bottom:1px solid #ffffff;}
.layout-join .tab div {display:inline-block; width:33.3%;}
.layout-join .tab div span.block {position:relative; text-align:center; display:block; height:49px; background-color:#eff0f2; border:1px solid #e0e1e3;}
.layout-join .tab div span.block {text-decoration:none; font-weight:bold; line-height:49px; font-size:16px; color:#bdc3c7; font-family:gulim,serif;}
.layout-join .tab div span.icon1,
.layout-join .tab div span.icon2,
.layout-join .tab div span.icon3 {position:relative; overflow:hidden; right:5px; top:2px; display:inline-block; width:16px; height:16px; background:url('<?=$web['host_member']?>/img/join_step.png') no-repeat;}
.layout-join .tab div span.icon2 {background-position:0 -16px;}
.layout-join .tab div span.icon3 {background-position:0 -32px;}

.layout-join .tab div.on {width:33.4%;}
.layout-join .tab div.on span.block {background-color:#bdc3c7; border:1px solid #abb1b5; color:#ffffff;}
.layout-join .tab div.on span.icon1 {background-position:-16px 0;}
.layout-join .tab div.on span.icon2 {background-position:-16px -16px;}
.layout-join .tab div.on span.icon3 {background-position:-16px -32px;}

.layout-join .box {margin-top:20px; border-bottom:1px solid #ffffff; background-color:#ffffff;}
.layout-join .box .line {padding-bottom:19px; border:1px solid #dadada;}
.layout-join .subj {padding-top:2px; font-weight:bold; line-height:50px; font-size:14px; color:#464646; font-family:gulim,serif;}
.layout-join .ess {margin-left:6px; font-weight:normal; line-height:50px; font-size:12px; color:#999999; font-family:gulim,serif;}
.layout-join .ess.on {color:#1192ca;}
.layout-join p.btn_check {margin:0px; cursor:pointer;}
.layout-join span.btn_check {cursor:pointer; position:absolute; right:20px; top:16px; display:block; width:21px; height:21px; background:url('<?=$web['host_member']?>/img/_check.png') no-repeat;}
.layout-join span.btn_check.on {background-position:0 -21px;}
.layout-join .textarea {overflow:auto; overflow-x:hidden; margin:0 1px; height:75px; border:1px solid #dadada; background-color:#f5f6f7; padding:7px 9px; word-break:break-all;}
.layout-join .textarea {line-height:1.5; font-size:12px; color:#666666; font-family:gulim,serif;}

.layout-join .all {padding:0 19px; position:relative; border-bottom:1px solid #f0f0f0;}
.layout-join .service {padding:0 19px; position:relative;}
.layout-join .privacy {margin-top:10px; padding:0 19px; position:relative;}
.layout-join .receive {margin-top:10px; padding:0 19px; position:relative;}
.layout-join .msg {display:none; padding:0 19px;}
.layout-join .msg {line-height:20px; font-size:12px; color:#ff0000; font-family:gulim,serif;}

.layout-join .submit {margin-top:20px;}
.layout-join .submit div {display:inline-block; width:50%;}
.layout-join .submit div span {margin-left:2px; display:block; text-align:center; cursor:pointer; height:59px; border:1px solid #dadada; background-color:#ffffff;}
.layout-join .submit div span {font-weight:700; line-height:59px; font-size:20px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
.layout-join .submit div.on span {margin-left:0px; margin-right:2px; border:1px solid #1192ca; background-color:#42abd7; color:#ffffff;}

.layout-join .menu {padding-top:19px; text-align:center; line-height:0px; margin-top:50px; border-top:1px solid #dadada;}
.layout-join .menu a {line-height:14px; font-size:12px; color:#999999; font-family:gulim,serif;}
.layout-join .menu a:hover {color:#000000;}
.layout-join .menu .line {padding:0 4px; line-height:14px; font-size:12px; color:#dadada; font-family:gulim,serif;}

@media screen and (max-width:600px) {

.layout-join .tab div span.icon1,
.layout-join .tab div span.icon2,
.layout-join .tab div span.icon3 {display:none;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    $('.layout-join input.check').each(function() {

        if ($(this).val() == 1) {

            $(this).parent().children('span.btn_check').addClass('on');

        } else {

            $(this).parent().children('span.btn_check').removeClass('on');

        }

    });

    $('.layout-join p.btn_check, .layout-join span.btn_check').click(function() {

        if ($(this).attr('rel') == 'all') {

            var obj = $(this).parent().children('input.check');

            if (obj.val() == 1) {

                $('.layout-join span.btn_check').removeClass('on');
                $('.layout-join input.check').val('');

            } else {

                $('.layout-join span.btn_check').addClass('on');
                $('.layout-join input.check').val('1');

            }

        } else {

            var obj = $(this).parent().children('input.check');

            if (obj.val() == 1) {

                $(this).parent().children('span.btn_check').removeClass('on');
                obj.val('');

            } else {

                $(this).parent().children('span.btn_check').addClass('on');
                obj.val('1');

            }

        }

    });

    $('.layout-join .btn_confirm').click(function() {

        submitSetup();

    });

    $('.layout-join .btn_cancel').click(function() {

        history.go(-1);

    });

});

function submitSetup()
{

    var f = document.formSetup;

    if (f.service.value == '' || f.privacy.value == '') {

        $('#msg').show().text('이용약관과 개인정보 취급방침에 모두 동의해주세요.');
        return false;

    }

    f.action = "form.php";
    f.submit();

}
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("$web[path]/_top.php");
?>
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" id="url" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<div class="layout-join">
<div class="title"></div>
<div class="tab"><div class="on"><span class="block"><span class="icon1"></span>약관 동의</span></div><div><span class="block"><span class="icon2"></span>정보입력</span></div><div class="last"><span class="block"><span class="icon3"></span>가입완료</span></div></div>
<div class="box">
<div class="line">
<div class="all">
<p class="subj btn_check" rel="all">전체 약관에 동의 합니다.</p><input type="hidden" name="all" value="" class="check" /><span class="btn_check" rel="all"></span>
</div>
<div class="service">
<p class="subj btn_check">서비스 이용약관 동의<span class="ess on">(필수)</span></p><input type="hidden" name="service" value="" class="check" /><span class="btn_check"></span>
<div class="textarea"><?=text2($page_service['page_content'], 2);?></div>
</div>
<div class="privacy">
<p class="subj btn_check">개인정보 취급방침 동의<span class="ess on">(필수)</span></p><input type="hidden" name="privacy" value="" class="check" /><span class="btn_check"></span>
<div class="textarea"><?=text2($page_privacy['page_content'], 2);?></div>
</div>
<div class="receive">
<p class="subj btn_check">정보 메일/문자 수신<span class="ess">(선택)</span></p><input type="hidden" name="receive" value="" class="check" /><span class="btn_check"></span>
</div>
<div class="msg" id="msg"></div>
</div>
</div>
<div class="submit"><div class="on"><span tabindex="1" class="btn_confirm">동의</span></div><div><span tabindex="2" class="btn_cancel">비동의</span></div></div>
<div class="menu">
<a href="<?=$web['host_member']?>/login/">로그인</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/id_hp.php">아이디 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/find/pw_hp.php">비밀번호 찾기</a>
<span class="line">|</span>
<a href="<?=$web['host_member']?>/join/">회원가입</a>
</div>
</div>
</form>
<?
include_once("$web[path]/_bottom.php");
?>