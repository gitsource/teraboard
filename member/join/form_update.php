<?php
include_once("./_tb.php");
echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

/*------------------------------
두 파일에서 같이 체크하기 때문에, 체크하는 코드가 바뀌면 같이 변경합니다.
from.check.php
form_update.php
------------------------------*/

if ($check_login) { url($web['host_default'])."/"; }
if ($receive) { $receive = preg_match("/^[0-9]+$/", $receive) ? $receive : ""; }

$setup_join = setup_join();
$setup_email_join = setup_email("join");

if ($m == '') {

    // 폼 체크
    if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    // 아이디 체크
    if ($uid) { $uid = strtolower(trim($uid)); $uid = preg_match("/^[a-zA-Z0-9_\-]{3,12}+$/", $uid) ? $uid : ""; }

    if (!$uid) {

        message("<p class='title'>알림</p><p class='text'>아이디를 올바르게 입력하세요.</p>", "b");

    }

    if (preg_match("/^([_\-])/", $uid)) {

        message("<p class='title'>알림</p><p class='text'>아이디를 올바르게 입력하세요.</p>", "b");

    }

    $check_block = false;
    $row = explode("|", $setup['block_id']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] == $uid) {

            $check_block = true;

        }

    }

    if ($check_block) {

        message("<p class='title'>알림</p><p class='text'>사용하실 수 없는 아이디입니다.</p>", "b");

    }

    $chk = member_uid($uid);

    if ($chk['uid']) {

        message("<p class='title'>알림</p><p class='text'>이미 사용중인 아이디입니다.</p>", "b");

    }

    // 비밀번호 체크
    $upw = trim(strip_tags(sql_real_escape_string($_POST['upw'])));
    if ($upw) { $upw = trim($upw); $upw = preg_match("/^[a-zA-Z0-9`~!@#\$\%^&\*\(\)\-_\=\+\|\\\[\{\]\}\;\:\'\"\<\,\.\>\/\?]{6,16}+$/", $upw) ? $upw : ""; }

    if (!$upw) {

        message("<p class='title'>알림</p><p class='text'>비밀번호 6~16자 대/소문자, 숫자, 기호를 사용하세요.</p>", "b");

    }

    $n = 0;
    for ($i=0; $i<strlen($upw); $i++) {

        if (substr($upw,0,1) == substr($upw,$i,1)) {

            $n++;

        }

    }

    if ($n >= 6) {

        message("<p class='title'>알림</p><p class='text'>비밀번호 연속된 문자를 6자이상 사용할 수 없습니다.</p>", "b");

    }

    // 닉네임 체크
    $nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));
    if ($nick) { $nick = trim($nick); $nick = preg_match("/^[A-Za-z0-9_가-힣\x20]{1,300}+$/", $nick) ? $nick : ""; }

    if (!$nick) {

        message("<p class='title'>알림</p><p class='text'>사용하실 수 없는 닉네임입니다.</p>", "b");

    }

    $check_block = false;
    $row = explode("|", $setup['block_id']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] == $nick) {

            $check_block = true;

        }

    }

    if ($check_block) {

        message("<p class='title'>알림</p><p class='text'>사용하실 수 없는 닉네임입니다.</p>", "b");

    }

    $chk = sql_fetch(" select * from $web[member_table] where nick = '".addslashes($nick)."' limit 0, 1 ");

    if ($chk['nick']) {

        message("<p class='title'>알림</p><p class='text'>이미 사용중인 닉네임입니다.</p>", "b");

    }

    // 휴대폰 체크
    if ($hp) { $hp = str_replace("-", "", trim($hp)); $hp = preg_match("/^[0-9\-]+$/", $hp) ? $hp : ""; }
    if ($check_hp_real) { $check_hp_real = preg_match("/^[0-9]+$/", $check_hp_real) ? $check_hp_real : ""; }

    // 미입력
    if (!$hp) {

        // 필수, 인증
        if ($setup_join['hp'] == 2 || $setup_join['hp'] == 3) {

            message("<p class='title'>알림</p><p class='text'>휴대폰 번호를 입력하세요.</p>", "b");

        }

    } else {

        if (!preg_match("/^[0-9\-]{6,20}+$/", $hp)) {

            message("<p class='title'>알림</p><p class='text'>휴대폰 번호가 잘못되었습니다.</p>", "b");

        }

        $chk = sql_fetch(" select * from $web[member_table] where hp = '".addslashes($hp)."' and dropout = 0 limit 0, 1 ");

        if ($chk['hp']) {

            message("<p class='title'>알림</p><p class='text'>이미 사용중인 휴대폰 번호입니다.</p>", "b");

        }

        // 인증
        if ($setup_join['hp'] == 3) {

            if (!$check_hp_real || !$hp_code) {

                message("<p class='title'>알림</p><p class='text'>휴대폰 인증이 완료되지 않았습니다..</p>", "b");

            }

            // 인증기록 체크
            $chk = sql_fetch(" select * from $web[real_hp_table] where id = '".addslashes($check_hp_real)."' limit 0, 1 ");

            if (!$chk['id']) {

                message("<p class='title'>알림</p><p class='text'>휴대폰 인증이 완료되지 않았습니다..</p>", "b");

            }

            if ($chk['mode']) {

                message("<p class='title'>알림</p><p class='text'>휴대폰 인증이 완료되지 않았습니다..</p>", "b");

            }

            if ($chk['hp'] != $hp) {

                message("<p class='title'>알림</p><p class='text'>휴대폰 인증이 완료되지 않았습니다..</p>", "b");

            }

            if ($chk['code'] != $hp_code) {

                message("<p class='title'>알림</p><p class='text'>휴대폰 인증이 완료되지 않았습니다..</p>", "b");

            }

        }

    }

    // 이메일 체크
    $email = trim(strip_tags(sql_real_escape_string($_POST['email'])));
    if ($email) { $email = strtolower(trim($email)); }
    if ($check_email_real) { $check_email_real = preg_match("/^[0-9]+$/", $check_email_real) ? $check_email_real : ""; }

    // 미입력
    if (!$email) {

        // 필수, 인증
        if ($setup_join['email'] == 2 || $setup_join['email'] == 3) {

            message("<p class='title'>알림</p><p class='text'>이메일 주소를 입력하세요.</p>", "b");

        }

    } else {

        if (!preg_match("/^[a-zA-Z0-9_\-\.]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,6}$/i", $email)) {

            message("<p class='title'>알림</p><p class='text'>이메일 주소가 잘못되었습니다.</p>", "b");

        }

        $chk = sql_fetch(" select * from $web[member_table] where email = '".addslashes($email)."' and dropout = 0 limit 0, 1 ");

        if ($chk['email']) {

            message("<p class='title'>알림</p><p class='text'>이미 사용중인 이메일 주소입니다.</p>", "b");

        }

        // 인증
        if ($setup_join['email'] == 3) {

            if (!$check_email_real || !$email_code) {

                message("<p class='title'>알림</p><p class='text'>이메일 인증이 완료되지 않았습니다..</p>", "b");

            }

            // 인증기록 체크
            $chk = sql_fetch(" select * from $web[real_email_table] where id = '".addslashes($check_email_real)."' limit 0, 1 ");

            if (!$chk['id']) {

                message("<p class='title'>알림</p><p class='text'>이메일 인증이 완료되지 않았습니다..</p>", "b");

            }

            if ($chk['mode']) {

                message("<p class='title'>알림</p><p class='text'>이메일 인증이 완료되지 않았습니다..</p>", "b");

            }

            if ($chk['email'] != $email) {

                message("<p class='title'>알림</p><p class='text'>이메일 인증이 완료되지 않았습니다..</p>", "b");

            }

            if ($chk['code'] != $email_code) {

                message("<p class='title'>알림</p><p class='text'>이메일 인증이 완료되지 않았습니다..</p>", "b");

            }

        }

    }

    // 성명
    $name = trim(strip_tags(sql_real_escape_string($_POST['name'])));
    if ($name) { $name = trim($name); $name = preg_match("/^[가-힣\x20]{1,100}+$/", $name) ? $name : ""; }

    if (!$name && $setup_join['name'] == 2) {

        message("<p class='title'>알림</p><p class='text'>성명을 올바르게 입력하세요.</p>", "b");

    }

    // 성별
    if ($sex) { $sex = preg_match("/^[0-9]+$/", $sex) ? $sex : ""; }

    if (!$sex && $setup_join['sex'] == 2) {

        message("<p class='title'>알림</p><p class='text'>성별을 올바르게 입력하세요.</p>", "b");

    }

    // 생년월일
    if ($birth1) { $birth1 = trim($birth1); $birth1 = preg_match("/^[0-9]+$/", $birth1) ? $birth1 : ""; }
    if ($birth2) { $birth2 = trim($birth2); $birth2 = preg_match("/^[0-9]+$/", $birth2) ? $birth2 : ""; }
    if ($birth3) { $birth3 = trim($birth3); $birth3 = preg_match("/^[0-9]+$/", $birth3) ? $birth3 : ""; }

    $birth = date("Y-m-d", strtotime($birth1."-".$birth2."-".$birth3));

    if (!$birth1 || !$birth2 || !$birth3 || $birth1 < 1900 || $birth1 > date("Y", $web['server_time']) || $birth2 < 1 || $birth2 > 12 || $birth3 < 1 || $birth3 > 31 || $birth > $web['time_ymd']) {

        if ($setup_join['birth'] == 2) {

            message("<p class='title'>알림</p><p class='text'>생년월일을 올바르게 입력하세요.</p>", "b");

        }

        $birth = "0000-00-00";

    }

    // 일반전화
    if ($tel) { $tel = str_replace("-", "", trim($tel)); $tel = preg_match("/^[0-9\-]{6,20}+$/", $tel) ? $tel : ""; }

    if (!$tel && $setup_join['tel'] == 2) {

        message("<p class='title'>알림</p><p class='text'>일반전화를 올바르게 입력하세요.</p>", "b");

    }

    // 주소
    $zipcode = trim(strip_tags(sql_real_escape_string($_POST['zipcode'])));
    $addr1 = trim(strip_tags(sql_real_escape_string($_POST['addr1'])));
    $addr2 = trim(strip_tags(sql_real_escape_string($_POST['addr2'])));

    if (!$addr1 && $setup_join['addr'] == 2) {

        message("<p class='title'>알림</p><p class='text'>주소를 올바르게 입력하세요.</p>", "b");

    }

    if (!$addr2 && $setup_join['addr'] == 2) {

        message("<p class='title'>알림</p><p class='text'>주소를 올바르게 입력하세요.</p>", "b");

    }

    // 홈페이지
    $homepage = trim(strip_tags(sql_real_escape_string($_POST['homepage'])));
    if (!$homepage && $setup_join['homepage'] == 2) {

        message("<p class='title'>알림</p><p class='text'>홈페이지를 올바르게 입력하세요.</p>", "b");

    }

    // 추천인
    if ($recommend) { $recommend = strtolower(trim($recommend)); $recommend = preg_match("/^[a-zA-Z0-9_\-]{3,12}+$/", $recommend) ? $recommend : ""; }
    if (!$recommend && $setup_join['recommend'] == 2) {

        message("<p class='title'>알림</p><p class='text'>추천인을 올바르게 입력하세요.</p>", "b");

    }

    if ($recommend) {

        $chk = member_uid($recommend);

        if (!$chk['uid']) {

            message("<p class='title'>알림</p><p class='text'>추천한 아이디가 존재하지 않습니다.</p>", "b");

        }

        // 자기 자신이면
        if ($chk['uid'] == $uid) {

            // 초기화
            $recommend = "";

        } else {

            // mid 지정
            $recommend = $chk['mid'];

        }

    }

    // 자기소개
    $profile = trim(sql_real_escape_string($_POST['profile']));
    if (!$profile && $setup_join['profile'] == 2) {

        message("<p class='title'>알림</p><p class='text'>자기소개를 올바르게 입력하세요.</p>", "b");

    }

    // 텍스트폼1
    $text1 = trim(strip_tags(sql_real_escape_string($_POST['text1'])));
    if (!$text1 && $setup_join['text1'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['text1_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 텍스트폼2
    $text2 = trim(strip_tags(sql_real_escape_string($_POST['text2'])));
    if (!$text2 && $setup_join['text2'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['text2_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 텍스트폼3
    $text3 = trim(strip_tags(sql_real_escape_string($_POST['text3'])));
    if (!$text3 && $setup_join['text3'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['text3_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 단일선택폼1
    $single1 = trim(strip_tags(sql_real_escape_string($_POST['single1'])));
    if (!$single1 && $setup_join['single1'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['single1_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 단일선택폼2
    $single2 = trim(strip_tags(sql_real_escape_string($_POST['single2'])));
    if (!$single2 && $setup_join['single2'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['single2_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 단일선택폼3
    $single3 = trim(strip_tags(sql_real_escape_string($_POST['single3'])));
    if (!$single3 && $setup_join['single3'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['single3_title'])." 올바르게 입력하세요.</p>", "b");

    }

    $sql_common = "";
    $sql_common .= " set uid = '".$uid."' ";
    $sql_common .= ", upw = '".sql_password($upw)."' ";
    $sql_common .= ", nick = '".$nick."' ";
    $sql_common .= ", hp = '".$hp."' ";
    $sql_common .= ", email = '".$email."' ";
    $sql_common .= ", name = '".$name."' ";
    $sql_common .= ", sex = '".$sex."' ";
    $sql_common .= ", birth = '".$birth."' ";
    $sql_common .= ", tel = '".$tel."' ";
    $sql_common .= ", zipcode = '".$zipcode."' ";
    $sql_common .= ", addr1 = '".$addr1."' ";
    $sql_common .= ", addr2 = '".$addr2."' ";
    $sql_common .= ", homepage = '".$homepage."' ";
    $sql_common .= ", recommend = '".$recommend."' ";
    $sql_common .= ", profile = '".$profile."' ";
    $sql_common .= ", text1 = '".$text1."' ";
    $sql_common .= ", text2 = '".$text2."' ";
    $sql_common .= ", text3 = '".$text3."' ";
    $sql_common .= ", single1 = '".$single1."' ";
    $sql_common .= ", single2 = '".$single2."' ";
    $sql_common .= ", single3 = '".$single3."' ";

    $list = "";
    $row = explode("|", $setup_join['multi1_list']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $_POST['multi1_choice'.$i]) {

            if ($list) {
                $list .= "|";
            }

            $list .= trim(strip_tags(sql_real_escape_string($_POST['multi1_choice'.$i])));

        }

    }

    if ($setup_join['multi1'] && $list) {

        $sql_common .= ", multi1 = '".$list."' ";

    }

    $list = "";
    $row = explode("|", $setup_join['multi2_list']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $_POST['multi2_choice'.$i]) {

            if ($list) {
                $list .= "|";
            }

            $list .= trim(strip_tags(sql_real_escape_string($_POST['multi2_choice'.$i])));

        }

    }

    if ($setup_join['multi2'] && $list) {

        $sql_common .= ", multi2 = '".$list."' ";

    }

    $list = "";
    $row = explode("|", $setup_join['multi3_list']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $_POST['multi3_choice'.$i]) {

            if ($list) {
                $list .= "|";
            }

            $list .= trim(strip_tags(sql_real_escape_string($_POST['multi3_choice'.$i])));

        }

    }

    if ($setup_join['multi3'] && $list) {

        $sql_common .= ", multi3 = '".$list."' ";

    }

    if ($setup_join['hp'] == 3) {

        $sql_common .= ", certify_hp = '1' ";

    }

    if ($setup_join['email'] == 3) {

        $sql_common .= ", certify_email = '1' ";

    }

    if ($receive) {

        $sql_common .= ", hp_consent = '1' ";
        $sql_common .= ", email_consent = '1' ";

    }

    $sql_common .= ", photo = '".rand(1,70).".jpg' ";
    $sql_common .= ", level = '2' ";
    $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";
    $sql_common .= ", form_check = '".addslashes(substr(md5($_SERVER['REMOTE_ADDR'].date("His", $web['server_time']).rand(100000,999999)),0,30))."' ";

    sql_query(" insert into $web[member_table] $sql_common ");

    $mid = sql_insert_id();

    if ($check_hp_real) {

        sql_query(" update $web[real_hp_table] set mode = '1' where id = '".addslashes($check_hp_real)."' ");

    }

    if ($check_email_real) {

        sql_query(" update $web[real_email_table] set mode = '1' where id = '".addslashes($check_email_real)."' ");

    }

    // 첨부파일폼1
    if ($setup_join['file1']) {

        $upload_mode = "file1";
        include("./form_update.file.php");

    }

    // 첨부파일폼2
    if ($setup_join['file2']) {

        $upload_mode = "file2";
        include("./form_update.file.php");

    }

    // 첨부파일폼3
    if ($setup_join['file3']) {

        $upload_mode = "file3";
        include("./form_update.file.php");

    }

    // 회원가입 시
    if ($setup['point_join']) {

        member_point($mid, $setup['point_join'], 1, "회원가입", $web['server_time'].rand(10000,99999));

    }

    // 추천인 입력 시
    if ($recommend && $setup['point_recommend']) {

        member_point($mid, $setup['point_recommend'], 1, "추천인", $web['server_time'].rand(10000,99999));

    }

    // 추천인 입력받을 시
    if ($recommend && $setup['point_recommend_target']) {

        member_point($recommend, $setup['point_recommend_target'], 1, "추천인", $web['server_time'].rand(10000,99999));

    }

    set_session('ss_mid', $mid.'|'.$_SERVER['REMOTE_ADDR'].'|'.md5($_SERVER['HTTP_USER_AGENT'].$web['time_ymdhis']));

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."' ";
    $sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
    $sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
    $sql_common .= ", login = '1' ";
    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[member_login_table] $sql_common ");

    // 가입축하 메세지
    if ($setup['message_onoff'] && $setup['message_join_onoff']) {

        $content = $setup['message_join'];
        $content = str_replace("{닉네임}", $nick, $content);
        $content = str_replace("{회원ID}", $uid, $content);
        $content = str_replace("{홈페이지명}", $setup['title'], $content);

        $sql_common = "";
        $sql_common .= " set mid = '".$mid."' ";
        $sql_common .= ", fid = '1' ";
        $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
        $sql_common .= ", type = '0' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[message_receive_table] $sql_common ");

    }

    // 가입축하 문자
    if ($hp && $setup['sms_onoff'] && $setup['sms_join_onoff']) {

        $message = $setup['sms_join'];
        $message = str_replace("{닉네임}", $nick, $message);
        $message = str_replace("{회원ID}", $uid, $message);
        $message = str_replace("{홈페이지명}", $setup['title'], $message);

        sms_send($hp, $setup['callernumber'], $message);

    }

    // 가입축하 이메일
    if ($email && $setup['email'] && $setup['email_onoff'] && $setup_email_join['onoff']) {

        if (!$setup['title']) {

            $setup['title'] = $web['host_default'];

        }

        $title = $setup_email_join['title'];
        $title = str_replace("{닉네임}", $nick, $title);
        $title = str_replace("{회원ID}", $uid, $title);
        $title = str_replace("{성명}", $name, $title);
        $title = str_replace("{가입일}", text_date("Y년 m월 d일", $web['time_ymdhis'], ""), $title);
        $title = str_replace("{홈페이지명}", $setup['title'], $title);
        $title = str_replace("{도메인주소}", $web['host_default'], $title);
        $title = str_replace("{회사명}", $setup['company'], $title);
        $title = str_replace("{대표자명}", $setup['ceo'], $title);
        $title = str_replace("{대표메일}", $setup['email'], $title);
        $title = str_replace("{대표번호}", $setup['tel'], $title);
        $title = str_replace("{회사주소}", $setup['addr'], $title);

        $content = $setup_email_join['content'];
        $content = str_replace("{닉네임}", $nick, $content);
        $content = str_replace("{회원ID}", $uid, $content);
        $content = str_replace("{성명}", $name, $content);
        $content = str_replace("{가입일}", text_date("Y년 m월 d일", $web['time_ymdhis'], ""), $content);
        $content = str_replace("{홈페이지명}", $setup['title'], $content);
        $content = str_replace("{도메인주소}", $web['host_default'], $content);
        $content = str_replace("{회사명}", $setup['company'], $content);
        $content = str_replace("{대표자명}", $setup['ceo'], $content);
        $content = str_replace("{대표메일}", $setup['email'], $content);
        $content = str_replace("{대표번호}", $setup['tel'], $content);
        $content = str_replace("{회사주소}", $setup['addr'], $content);

        $sql_common = "";
        $sql_common .= " set mid = '".$mid."' ";
        $sql_common .= ", name = '".addslashes($setup['title'])."' ";
        $sql_common .= ", send_email = '".addslashes($setup['email'])."' ";
        $sql_common .= ", email = '".addslashes($email)."' ";
        $sql_common .= ", title = '".$title."' ";
        $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

        sql_query(" insert into $web[email_table] $sql_common ");

        email_send($email, $title, text2(trim($content), 1), $setup['title'], $setup['email'], 1);

    }

    $ss_name = "join_result";
    if (!get_session($ss_name)) {

        set_session($ss_name, true);

    }

    url("result.php");

}
?>