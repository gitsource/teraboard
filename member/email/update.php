<?php
include_once("./_tb.php");
if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }
if ($tid) { $tid = preg_match("/^[a-zA-Z0-9_\-,]+$/", $tid) ? $tid : ""; }
$title = trim(strip_tags(sql_real_escape_string($_POST['title'])));

// 비회원
if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

if (!$setup['email_onoff']) {

    message("<p class='title'>알림</p><p class='text'>이메일 발송 기능을 사용하지 않습니다.</p>", "c");

}

if (!$setup['sideview_email'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>이메일 발송이 미사용중입니다.</p>", "c");

}

if (!$member['email'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>이메일 인증 후 사용하세요.</p>", "c");

}

if (!$member['email_consent'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>이메일 수신동의 후 사용하세요.</p>", "c");

}

// 폼 체크
if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

if ($check_admin) {

    $member['nick'] = $setup['title'];
    $member['email'] = $setup['email'];

}

if ($m == 'send') {

    if (!$title) {

        message("<p class='title'>알림</p><p class='text'>제목을 입력하세요.</p>", "b");

    }

    if (!$_POST['content']) {

        message("<p class='title'>알림</p><p class='text'>내용을 입력하세요.</p>", "b");

    }

    // 일괄
    if ($nicks) {

        $n = 0;
        $array = array();
        $row = explode(",", text($nicks));
        for ($i=0; $i<count($row); $i++) {

            $mb = sql_fetch(" select * from $web[member_table] where nick = '".addslashes($row[$i])."' limit 0, 1 ");

            if (!$mb['dropout'] && ($mb['email'] && $mb['email_consent'] || $mb['email'] && $check_admin)) {

                $array[$n] = $mb['email'];
                $n++;

            }

        }

        for ($i=0; $i<count($array); $i++) {

            $sql_common = "";
            $sql_common .= " set mid = '".$member['mid']."' ";
            $sql_common .= ", name = '".addslashes($member['nick'])."' ";
            $sql_common .= ", send_email = '".addslashes($member['email'])."' ";
            $sql_common .= ", email = '".addslashes($array[$i])."' ";
            $sql_common .= ", title = '".$title."' ";
            $sql_common .= ", content = '".trim(sql_real_escape_string($_POST['content']))."' ";
            $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

            sql_query(" insert into $web[email_table] $sql_common ");

            email_send($array[$i], $title, text2(trim($_POST['content']), 1), $member['nick'], $member['email'], 1);

        }

        if ($n > 1) {

            message("<p class='title'>알림</p><p class='text'>".text($n)." 명에게 이메일을 발송하였습니다.</p>", "c");

        }

        else if ($n) {

            message("<p class='title'>알림</p><p class='text'>이메일을 발송하였습니다.</p>", "c");

        } else {

            message("<p class='title'>알림</p><p class='text'>이메일 발송을 실패하였습니다. (상대방 이메일 미인증 및 수신거부)</p>", "c");

        }

    } else {
    // 개별

        $mb = member($tid);

        if ($mb['dropout']) {

            message("<p class='title'>알림</p><p class='text'>탈퇴한 회원입니다.</p>", "c");

        }

        if ($mb['email'] && $mb['email_consent'] || $mb['email'] && $check_admin) {

            $sql_common = "";
            $sql_common .= " set mid = '".$member['mid']."' ";
            $sql_common .= ", name = '".addslashes($member['nick'])."' ";
            $sql_common .= ", send_email = '".addslashes($member['email'])."' ";
            $sql_common .= ", email = '".addslashes($mb['email'])."' ";
            $sql_common .= ", title = '".$title."' ";
            $sql_common .= ", content = '".trim(sql_real_escape_string($_POST['content']))."' ";
            $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

            sql_query(" insert into $web[email_table] $sql_common ");

            email_send($mb['email'], $title, text2(trim($_POST['content']), 1), $member['nick'], $member['email'], 1);

            message("<p class='title'>알림</p><p class='text'>".text($mb['nick'])." 님께 이메일을 발송하였습니다.</p>", "c");

        } else {

            message("<p class='title'>알림</p><p class='text'>".text($mb['nick'])." 님은 이메일을 수신할 수 없는 상태입니다.</p>", "c");

        }

    }

}
?>