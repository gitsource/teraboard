<?php
include_once("./_tb.php");
if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }
if ($tid) { $tid = preg_match("/^[a-zA-Z0-9_\-,]+$/", $tid) ? $tid : ""; }

// 비회원
if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

if (!$setup['sms_onoff']) {

    message("<p class='title'>알림</p><p class='text'>문자발송이 미사용중입니다.</p>", "c");

}

if (!$setup['sideview_sms'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>문자발송이 미사용중입니다.</p>", "c");

}

if (!$member['hp'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>휴대폰 인증 후 사용하세요.</p>", "c");

}

if (!$member['hp_consent'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>휴대폰 수신동의 후 사용하세요.</p>", "c");

}

// 폼 체크
if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

if ($check_admin) {

    $member['hp'] = $setup['hp'];

}

if ($m == 'send') {

    if (!$_POST['content']) {

        message("<p class='title'>알림</p><p class='text'>내용을 입력하세요.</p>", "b");

    }

    // 일괄
    if ($nicks) {

        $n = 0;
        $array = array();
        $row = explode(",", text($nicks));
        for ($i=0; $i<count($row); $i++) {

            $mb = sql_fetch(" select * from $web[member_table] where nick = '".addslashes($row[$i])."' limit 0, 1 ");

            if (!$mb['dropout'] && ($mb['hp'] && $mb['hp_consent'] || $mb['hp'] && $check_admin)) {

                $array[$n] = $mb['hp'];
                $n++;

            }

        }

        for ($i=0; $i<count($array); $i++) {

            sms_send($array[$i], $member['hp'], trim($_POST['content']));

        }

        if ($n > 1) {

            message("<p class='title'>알림</p><p class='text'>".text($n)." 명에게 문자를 발송하였습니다.</p>", "c");

        }

        else if ($n) {

            message("<p class='title'>알림</p><p class='text'>문자를 발송하였습니다.</p>", "c");

        } else {

            message("<p class='title'>알림</p><p class='text'>문자 발송을 실패하였습니다. (상대방 휴대폰 미인증 및 수신거부)</p>", "c");

        }

    } else {
    // 개별

        $mb = member($tid);

        if ($mb['dropout']) {

            message("<p class='title'>알림</p><p class='text'>탈퇴한 회원입니다.</p>", "c");

        }

        if ($mb['hp'] && $mb['hp_consent'] || $mb['hp'] && $check_admin) {

            sms_send($mb['hp'], $member['hp'], trim($_POST['content']));

            message("<p class='title'>알림</p><p class='text'>".text($mb['nick'])." 님께 문자를 발송하였습니다.</p>", "c");

        } else {

            message("<p class='title'>알림</p><p class='text'>".text($mb['nick'])." 님은 문자를 수신할 수 없는 상태입니다.</p>", "c");

        }

    }

}
?>