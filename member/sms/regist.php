<?php
include_once("./_tb.php");

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "c");

}

if (!$setup['sms_onoff']) {

    message("<p class='title'>알림</p><p class='text'>문자발송이 미사용중입니다.</p>", "c");

}

if (!$setup['sms_onoff'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>문자발송이 미사용중입니다.</p>", "c");

}

if (!$member['hp'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>휴대폰 인증 후 사용하세요.</p>", "c");

}

if (!$member['hp_consent'] && !$check_admin) {

    message("<p class='title'>알림</p><p class='text'>휴대폰 수신동의 후 사용하세요.</p>", "c");

}

$mb = array();

if ($mid) {

    $mb = member($mid);

    if (!$mb['mid']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

    }

    if ($mb['dropout']) {

        message("<p class='title'>알림</p><p class='text'>탈퇴한 회원입니다.</p>", "c");

    }

    if (!$mb['hp']) {

        message("<p class='title'>알림</p><p class='text'>".text($mb['nick'])." 님은 휴대폰을 인증하지 않으셨습니다.</p>", "c");

    }

    if (!$mb['hp_consent']) {

        message("<p class='title'>알림</p><p class='text'>".text($mb['nick'])." 님은 수신거부 상태입니다.</p>", "c");

    }

}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<?
$web['title'] = "문자 발송";
include_once("$web[path]/_head.php");
?>
<style type="text/css">
body {background-color:#ffffff; min-width:360px;}
.title {position:relative; height:49px; border-bottom:1px solid #3d4753; background-color:#485362;}
.title {font-weight:700; line-height:49px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.title img {vertical-align:middle;}
.title .btn {font-size:0px; position:absolute; right:10px; top:9px; display:block; width:32px; height:30px;}
.title .btn a {display:inline-block; width:32px; height:32px; background:url('<?=$web['host_member']?>/img/_btn_option.png') no-repeat;}
.title .btn a.close {background-position:0 0;}
.title .btn a.close:hover {background-position:0 -32px;}
.title .btn a.close:active {background-position:0 -64px;}

.wrap {padding:20px 20px 0 20px;}

.to {position:relative; height:93px; background:url('<?=$web['host_member']?>/img/_line_w7_h1.png') repeat-x 0 bottom; background-color:#f0f3f6;}
.to ul {font-size:0px;}
.to ul li {display:inline-block; vertical-align:middle;}
.to ul li.subj {margin-left:20px; margin-right:10px; font-weight:700; line-height:93px; font-size:24px; color:#a3b1c0; font-family:'Nanum Gothic',gulim,serif;}
.to ul li p {margin:0px;}
.to ul li .member_thumb {margin-right:10px; position:relative; width:48px; height:48px;}
.to ul li .member_thumb img {position:absolute; top:0; left:0; width:48px; height:48px; border-radius:48px; border:2px solid #a3b1c0;}
.to ul li.block {width:60%;}
.to ul li .nick {line-height:20px; font-size:13px; color:#1192ca; font-family:gulim,serif;}
.to ul li .nick .sideview {font-weight:bold; color:#000000;}
.to ul li .nick nobr {vertical-align:top; display:inline-block; overflow:hidden; height:20px; max-width:100%; text-overflow:ellipsis; word-break:break-all;}
.to ul li.nicks {width:80%;}
.to ul li.nicks .msg {margin:0px; margin-top:8px; font-weight:400; line-height:12px; font-size:12px; color:#a3b1c0; font-family:'Nanum Gothic',gulim,serif;}
.to ul li.nicks .msg2 {display:none; margin:0px; margin-top:8px; font-weight:400; line-height:12px; font-size:12px; color:#a3b1c0; font-family:'Nanum Gothic',gulim,serif;}

.content {margin-top:20px;}

.input {height:24px; border:1px solid #dbe1e8; background-color:#f8f8f8; padding:0 6px 0 6px;}
.input .input_focus {margin-top:3px; width:100%; height:20px; border:0px; background:transparent;}
.input .input_focus {font-weight:bold; line-height:20px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.input.focus {border:1px solid #59c2e6; background-color:#ffffff;}
.input.focus .input_focus {color:#000000;}

.textarea {border:1px solid #dbe1e8; background-color:#f8f8f8; padding:6px 9px;}
.textarea .textarea_focus {width:100%; height:220px; border:0px; background:transparent;}
.textarea .textarea_focus {line-height:20px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.textarea.focus {border:1px solid #59c2e6; background-color:#ffffff;}
.textarea.focus .textarea_focus {color:#000000;}

.bytes {margin-top:10px; text-align:right;}
.bytes {font-weight:400; line-height:14px; font-size:14px; color:#a1b1c2; font-family:'Nanum Gothic',gulim,serif;}

 .submit {margin-top:10px; padding-top:20px; background:url('<?=$web['host_member']?>/img/_line_w7_h1.png') repeat-x 0 top;}
 .submit div {display:inline-block; width:50%;}
 .submit div span {margin-left:2px; display:block; text-align:center; cursor:pointer; height:59px; border:1px solid #dadada; background-color:#ffffff;}
 .submit div span {font-weight:700; line-height:59px; font-size:20px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
 .submit div.on span {margin-left:0px; margin-right:2px; border:1px solid #1192ca; background-color:#42abd7; color:#ffffff;}

@media screen and (max-width:600px) {

.wrap {padding:0px;}
.content {padding:0 10px;}
.bytes {padding:0 10px;}
.submit {margin:20px 10px 0 10px;}
.to ul li.nicks .msg {display:none;}
.to ul li.nicks .msg2 {display:block;}

}
</style>
<script type="text/javascript">
$(document).ready(function() {

    $("#content").focus();

    $('.textarea_focus, .input_focus').focus(function() {

        $(this).parent().addClass('focus');

    }).blur(function() {

        $(this).parent().removeClass('focus');

    });

    $('.btn_confirm').click(function() {

        submitSetup();

    });

    $('.btn_cancel').click(function() {

        window.close();

    });

    $('.btn_confirm').on('keypress', function(e) {

        if (e.which == 13) {

            submitSetup();

        }

    });

    $('.btn_cancel').on('keypress', function(e) {

        if (e.which == 13) {

            window.close();

        }

    });

});

function submitSetup()
{

    var f = document.formSetup;

    if (f.content.value == '') {

        alert("내용을 입력하세요.");
        f.content.focus();
        return false;

    }

    f.action = "update.php";
    f.submit();

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="send" />
<input type="hidden" name="tid" value="<?=text($mb['mid'])?>" />
<div class="title">
<img src="<?=$web['host_member']?>/img/sms_title.png">문자 발송
<div class="btn">
<a href="#" onclick="window.close(); return false;" title="닫기" class="close"></a>
</div>
</div>
<div class="wrap">
<div class="to">
<ul>
<li class="subj">T<span>o</span>.</li>
<? if ($mb['mid']) { ?>
<? if ($setup['image_onoff']) { ?>
<li><div class="member_thumb"><?=member_thumb($mb['mid'])?></div></li>
<? } ?>
<li class="block">
<p class="nick"><nobr><span class="sideview" onclick="sideviewLoad(this, '<?=text($mb['mid'])?>');"><?=text($mb['nick'])?></span> (<?=text($mb['uid'])?>)</nobr></p>
</li>
<? } else { ?>
<li class="nicks">
<div class="input"><input type="text" name="nicks" value="" class="input_focus" /></div>
<p class="msg">한명 이상의 회원에게 발송 시, 닉네임 사이에 콤마(,)를 사용하세요.</p>
<p class="msg2">한명 이상의 회원에게 발송 시, 콤마(,) 구분</p>
</li>
<? } ?>
</ul>
</div>
<div class="content"><div class="textarea"><textarea id="content" name="content" class="textarea_focus" onblur="smsByte('content', 'bytes');"  onkeyup="smsByte('content', 'bytes');"></textarea></div></div>
<div class="bytes"><span id="bytes" style="color:#42abd7;">0</span> / 1800byte</div>
<div class="submit"><div class="on"><span tabindex="1" class="btn_confirm">발송</span></div><div><span tabindex="2" class="btn_cancel">취소</span></div></div>
</div>
</form>
</body>
</html>