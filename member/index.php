<?php // 마이페이지
include_once("./_tb.php");
if (!$check_login) { url($web['host_member']."/login/?url={$urlencode}"); }

if ($m == 'consent') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    if ($hp_consent) { $hp_consent = preg_match("/^[0-9]+$/", $hp_consent) ? $hp_consent : ""; }
    if ($email_consent) { $email_consent = preg_match("/^[0-9]+$/", $email_consent) ? $email_consent : ""; }

    // 폼 체크
    if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    $sql_common = "";
    $sql_common .= " set hp_consent = '".$hp_consent."' ";
    $sql_common .= ", email_consent = '".$email_consent."' ";

    sql_query(" update $web[member_table] $sql_common where mid = '".addslashes($member['mid'])."' ");

    message("<p class='title'>알림</p><p class='text'>수신설정을 변경하였습니다.</p>", "", "", false, true);

    exit;

}

$level = member_level($member['level']);

// head start
ob_start();
?>
<link rel="stylesheet" href="<?=$web['host_member']?>/_menu.css" type="text/css" />
<style type="text/css">
body {min-width:320px;}

.layout-member {padding:50px 0;}

.layout-member .wrap {margin:0 0 0 -20px;}

.layout-member .container:after {display:block; clear:both; content:'';}
.layout-member .container .sizer,
.layout-member .container .item {width:50%; float:left;}
.layout-member .container .item .block {margin:20px 0 0 20px; padding:20px; position:relative; height:208px; background-color:#ffffff; border:1px solid #dadada;}

.layout-member .title {margin:0px; font-weight:700; line-height:20px; font-size:20px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .text {margin:0px; line-height:14px; font-size:12px; color:#b0b0b0; font-family:gulim,serif;}
.layout-member .subj {width:70px; font-weight:bold; line-height:16px; font-size:12px; color:#b0b0b0; font-family:gulim,serif;}

.layout-member .btn {font-size:0px;}
.layout-member .btn a {margin-left:4px; display:inline-block; background-color:#ffffff; border:1px solid #dadada; border-radius:3px; padding:1px 0 2px 0; width:78px; text-align:center;}
.layout-member .btn a:first-child {margin-left:10px;}
.layout-member .btn a {text-decoration:none; font-weight:700; line-height:26px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .btn a:hover {border:1px solid #42abd7; color:#42abd7;}

.layout-member .btn2 {font-size:0px;}
.layout-member .btn2 a {margin-left:4px; display:inline-block; background-color:#ffffff; border:1px solid #dadada; border-radius:3px; padding:1px 7px 2px 7px;}
.layout-member .btn2 a {text-decoration:none; font-weight:700; line-height:26px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .btn2 a:first-child {margin-left:0px;}
.layout-member .btn2 a:hover {border:1px solid #42abd7; color:#42abd7;}

.layout-member .info ul {font-size:0px;}
.layout-member .info ul li {display:inline-block; vertical-align:top;}
.layout-member .info ul li:first-child {margin-right:34px;}
.layout-member .info ul li p.text:first-child {margin-top:21px;}
.layout-member .info ul li p.text {margin-top:17px;}
.layout-member .info ul li .subj,
.layout-member .info ul li .uid,
.layout-member .info ul li .nick {display:inline-block;}
.layout-member .info ul li .uid {font-weight:bold; line-height:16px; font-size:14px; color:#42abd7; font-family:gulim,serif;}
.layout-member .info ul li .nick {line-height:16px; font-size:14px; color:#485362; font-family:gulim,serif;}
.layout-member .info ul li .member_thumb {position:relative; width:96px; height:96px; margin-top:21px;}
.layout-member .info ul li .member_thumb img {position:absolute; top:0; left:0; width:96px; height:96px; border-radius:96px; border:2px solid #d8d8d8;}
.layout-member .info ul li:last-child {width:57%;}
.layout-member .info ul li nobr {vertical-align:top; display:inline-block; overflow:hidden; height:16px; max-width:60%; text-overflow:ellipsis; word-break:break-all;}
.layout-member .member .btn {position:absolute; bottom:20px; left:10px;}

.layout-member .consent p.title {margin-bottom:21px;}
.layout-member .consent p.text {margin-top:17px;}
.layout-member .consent .subj,
.layout-member .consent .hp,
.layout-member .consent .email {display:inline-block; vertical-align:middle;}
.layout-member .consent .hp {line-height:16px; font-size:14px; color:#485362; font-family:gulim,serif;}
.layout-member .consent .email {width:70%; line-height:16px; font-size:14px; color:#485362; font-family:gulim,serif;}
.layout-member .consent .email nobr {vertical-align:top; display:inline-block; overflow:hidden; height:16px; max-width:85%; text-overflow:ellipsis; word-break:break-all;}
.layout-member .consent .icon {cursor:pointer; margin-right:9px; position:relative; overflow:hidden; left:0; top:-1px; vertical-align:top; display:inline-block; width:17px; height:17px; background:url('img/_onoff.png') no-repeat;}
.layout-member .consent .icon.on {background-position:0px -17px;}
.layout-member .consent .btn {position:absolute; bottom:20px; left:10px;}

.layout-member .activity p.title {margin-bottom:21px;}
.layout-member .activity ul {font-size:0px;}
.layout-member .activity ul {margin-top:17px;}
.layout-member .activity ul li {width:50%; display:inline-block; vertical-align:top;}
.layout-member .activity ul li .subj,
.layout-member .activity ul li nobr {vertical-align:top; display:inline-block; overflow:hidden; height:16px; max-width:55%; text-overflow:ellipsis; word-break:break-all;}
.layout-member .activity ul li nobr {line-height:16px; font-size:14px; color:#485362; font-family:gulim,serif;}
.layout-member .activity ul li nobr.point {font-weight:bold; color:#42abd7;}

.layout-member .certify p.title {margin-bottom:21px;}
.layout-member .certify p.text {margin-top:17px; position:relative;}
.layout-member .certify .subj,
.layout-member .certify .name,
.layout-member .certify .email,
.layout-member .certify .hp,
.layout-member .certify .adult {display:inline-block; vertical-align:middle;}
.layout-member .certify .name,
.layout-member .certify .email,
.layout-member .certify .hp,
.layout-member .certify .adult {line-height:16px; font-size:14px; color:#485362; font-family:gulim,serif;}
.layout-member .certify .icon {margin-right:9px; position:relative; overflow:hidden; left:0; top:-1px; vertical-align:top; display:inline-block; width:17px; height:17px; background:url('img/_onoff.png') no-repeat;}
.layout-member .certify .icon.ok {background-position:0px -17px;}
.layout-member .certify .ok {font-weight:bold; color:#42abd7;}
.layout-member .certify .text a {position:absolute; top:0px; right:0px;}
.layout-member .certify .text a {text-decoration:underline; font-weight:bold; line-height:16px; font-size:12px; color:#5674b9; font-family:gulim,serif;}

.layout-member .log ul {font-size:0px;}
.layout-member .log ul li {display:inline-block; vertical-align:top;}
.layout-member .log ul li:first-child {margin-right:34px;}
.layout-member .log ul li p.text:first-child {margin-top:21px;}
.layout-member .log ul li p.text {margin-top:17px;}
.layout-member .log ul li .subj,
.layout-member .log ul li .datetime,
.layout-member .log ul li .login_datetime,
.layout-member .log ul li .ip {display:inline-block;}
.layout-member .log ul li .icon {position:relative; width:80px; height:90px; margin-top:21px; background:url('img/_img_pc.png') no-repeat;}
.layout-member .log ul li:last-child {width:57%;}
.layout-member .log ul li nobr {vertical-align:top; display:inline-block; overflow:hidden; height:16px; max-width:60%; text-overflow:ellipsis; word-break:break-all;}
.layout-member .log ul li nobr {line-height:16px; font-size:14px; color:#485362; font-family:gulim,serif;}
.layout-member .mylog .btn {position:absolute; bottom:20px; left:10px;}

.layout-member .msg {margin:0px; margin-top:19px; line-height:14px; font-size:12px; color:#b0b0b0; font-family:gulim,serif;}
.layout-member .privacy {margin:0px; margin-top:3px;}
.layout-member .privacy a {text-decoration:underline; font-weight:bold; line-height:14px; font-size:12px; color:#485362; font-family:gulim,serif;}

.layout-member .service p.title {margin-bottom:21px;}
.layout-member .service p.text {margin-top:17px; margin-bottom:17px;}
.layout-member .service ul {font-size:0px;}
.layout-member .service ul li {display:inline-block; vertical-align:top;}
.layout-member .service .btn2 {margin-top:10px;}

@media screen and (max-width:1000px) {

.layout-member .wrap {margin:0 0 0 -10px;}

.layout-member .container .sizer,
.layout-member .container .item {width:100%;}
.layout-member .container .item .block {margin:10px 0 0 10px; padding:20px 10px;}

}

@media screen and (max-width:640px) {

.layout-member .btn a {width:80px;}
.layout-member .btn a:first-child {margin-left:5px;}
.layout-member .btn a {font-weight:normal; line-height:26px; font-size:13px; color:#7d7d7d; font-family:gulim,serif;}
.layout-member .btn2 a {font-weight:normal; line-height:26px; font-size:13px; color:#7d7d7d; font-family:gulim,serif;}

.layout-member .info ul li .member_thumb {width:70px; height:70px;}
.layout-member .info ul li .member_thumb img {width:70px; height:70px; border-radius:70px;}
.layout-member .info ul li:first-child {margin-right:25px;}
.layout-member .info ul li:last-child {width:65%;}

.layout-member .activity ul li nobr {max-width:40%;}

.layout-member .log ul li .icon {width:50px; height:56px; background-size:50px 56px;}
.layout-member .log ul li:first-child {margin-right:25px;}
.layout-member .log ul li:last-child {width:70%;}

.layout-member .msg,
.layout-member .privacy {padding-left:10px;}

}
</style>
<script type="text/javascript" src="<?=$web['host_js'];?>/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js'];?>/masonry.pkgd.min.js"></script>
<script type="text/javascript">
$(document).ready( function() {

    // 내정보
    $('.member-menu div[name=info]').addClass('on');

    $('.layout-member .consent input.onoff').each(function() {

        if ($(this).val() == '1') {

            $(this).parent().find('.icon').addClass('on');

        }

    });

    $('.layout-member .consent .icon').click(function() {

        var obj = $(this).parent().find('input.onoff');

        if (obj.val() == 1) {

            $(this).removeClass('on');
            obj.val('0');

        } else {

            $(this).addClass('on');
            obj.val('1');

        }

    });

    var $grid = $('.container').imagesLoaded( function() {

      $grid.masonry({
        columnWidth: '.sizer',
        itemSelector: '.item',
        percentPosition: true
      });

    });

});

function consentSubmit()
{

    $.post("index.php", {"form_check" : "<?=$member['form_check']?>", "m" : "consent", "hp_consent" : $('#hp_consent').val(), "email_consent" : $('#email_consent').val()}, function(data) {

        $("#update_data").html(data);

    });

}
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "내 정보";
include_once("$web[path]/_top.php");
?>
<div class="layout-member">
<?
// 메뉴 호출
include_once("$web[path]/member/_menu.php");
?>
<!-- wrap start //-->
<div class="wrap">
<div class="container">
<div class="sizer"></div>
<div class="item">
<div class="block member">
<p class="title">회원정보</p>
<div class="info">
<ul>
<? if ($setup['image_onoff']) { ?>
<li><div class="member_thumb"><?=member_thumb($member['mid'])?></div></li>
<? } ?>
<li>
<p class="text">회원님의 개인정보를 수정합니다!</p>
<p class="text"><span class="subj">아이디</span><nobr class="uid"><?=text($member['uid'])?></nobr></p>
<p class="text"><span class="subj">닉네임</span><nobr class="nick"><?=text($member['nick'])?></nobr></p>
</li>
</ul>
</div>
<div class="btn">
<? if ($setup['image_onoff']) { ?>
<a href="#" onclick="photoOpen('<?=text($member['mid'])?>'); return false;">IMG 변경</a>
<? } ?>
<a href="<?=$web['host_member']?>/modify/">정보수정</a>
</div>
</div>
</div>
<div class="item">
<div class="block consent">
<p class="title">수신설정</p>
<p class="text">연락처 비공개시 일부 서비스를 이용하실 수 없습니다.</p>
<p class="text"><span class="subj">휴대폰</span><span class="hp"><input type="hidden" id="hp_consent" value="<?=$member['hp_consent']?>" class="onoff" /><span class="icon"></span><?=text_view($member['hp'], "미입력")?></span></p>
<p class="text"><span class="subj">이메일</span><span class="email" title="<?=text($member['email'])?>"><input type="hidden" id="email_consent" value="<?=$member['email_consent']?>" class="onoff" /><span class="icon"></span><nobr><?=text_view($member['email'], "미입력")?></nobr></span></p>
<div class="btn">
<a href="#" onclick="consentSubmit(); return false;">확인</a>
</div>
</div>
</div>
<div class="item">
<div class="block activity">
<p class="title">활동정보</p>
<p class="text">회원님의 주요 활동내역 입니다.</p>
<div>
<ul>
<li><span class="subj">등급</span><nobr><?=text($level['title'])?></nobr></li>
<li><span class="subj">포인트</span><nobr class="point"><?=number($member['point'], $setup['point_number'])?> 점</nobr></li>
</ul>
<ul>
<li><span class="subj">게시물</span><nobr><?=number($member['bbs_write_count'])?> 건</nobr></li>
<li><span class="subj">댓글</span><nobr><?=number($member['bbs_reply_count'])?> 건</nobr></li>
</ul>
<ul>
<li><span class="subj">추천</span><nobr><?=number($member['good'])?> 건</nobr></li>
<li><span class="subj">비추천</span><nobr><?=number($member['nogood'])?> 건</nobr></li>
</ul>
</div>
</div>
</div>
<div class="item">
<div class="block certify">
<p class="title">인증내역</p>
<p class="text">실명, 휴대폰, 이메일, 성인 인증유무를 확인합니다.</p>
<p class="text"><span class="subj">본인확인</span><span class="hp"><span class="icon<? if ($member['certify_name']) { echo " ok"; } ?>"></span><? if ($member['certify_name']) { echo "<span class='ok'>인증완료</span>"; } else { echo "미인증"; } ?></span><? if (!$member['certify_name']) { echo "<a href='#' onclick=\"nameCertify('reload'); return false;\">인증하기</a>"; } ?></p>
<p class="text"><span class="subj">휴대폰</span><span class="hp"><span class="icon<? if ($member['certify_hp']) { echo " ok"; } ?>"></span><? if ($member['certify_hp']) { echo "<span class='ok'>인증완료</span>"; } else { echo "미인증"; } ?></span><? if (!$member['certify_hp']) { echo "<a href='#' onclick=\"hpCertify('reload'); return false;\">인증하기</a>"; } ?></p>
<p class="text"><span class="subj">이메일</span><span class="email"><span class="icon<? if ($member['certify_email']) { echo " ok"; } ?>"></span><? if ($member['certify_email']) { echo "<span class='ok'>인증완료</span>"; } else { echo "미인증"; } ?></span><? if (!$member['certify_email']) { echo "<a href='#' onclick=\"emailCertify('reload'); return false;\">인증하기</a>"; } ?></p>
<p class="text"><span class="subj">성인인증</span><span class="adult"><span class="icon<? if ($member['certify_adult']) { echo " ok"; } ?>"></span><? if ($member['certify_adult']) { echo "<span class='ok'>인증완료</span>"; } else { echo "미인증"; } ?></span><? if (!$member['certify_adult']) { echo "<a href='#' onclick=\"nameCertify('reload'); return false;\">인증하기</a>"; } ?></p>
</div>
</div>
<div class="item">
<div class="block mylog">
<p class="title">기록/탈퇴</p>
<div class="log">
<ul>
<li><div class="icon"></div></li>
<li>
<p class="text">홈페이지 접근기록 확인 및 탈퇴신청</p>
<p class="text"><span class="subj">가입일</span><nobr class="datetime"><?=text_date("Y. m. d", $member['datetime'], "")?></nobr></p>
<p class="text"><span class="subj">최종접속</span><nobr class="login_datetime"><?=text_date("Y. m. d", $member['login_datetime'], "")?></nobr></p>
<p class="text"><span class="subj">접근IP</span><nobr class="ip"><?=text($member['ip'])?></nobr></p>
</li>
</ul>
</div>
<div class="btn">
<a href="#" onclick="memberLogin('<?=$member['mid']?>'); return false;">로그인 기록</a><a href="<?=$web['host_member']?>/dropout/">탈퇴</a>
</div>
</div>
</div>
<div class="item">
<div class="block service">
<p class="title">기타 서비스</p>
<p class="text">기타 서비스입니다.</p>
<p class="btn2"><a href="#" onclick="bbsScrapList('<?=$member['mid']?>'); return false;">게시물 스크랩</a></p>
</div>
</div>
</div>
</div>
<!-- wrap end //-->
<p class="msg">회원님의 개인정보를 소중히 관리하고자 노력하겠습니다.</p>
<p class="privacy"><a href="<?=$web['host_page']?>/privacy">개인정보 취급방침 바로가기</a></p>
</div>
<?
include_once("$web[path]/_bottom.php");
?>