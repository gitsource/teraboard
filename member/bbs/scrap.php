<?php
include_once("./_tb.php");
if ($sort) { $sort = preg_match("/^[0-9]+$/", $sort) ? $sort : ""; }

$mb = member($mid);

if (!$mb['mid']) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "c");

}

$web['title'] = $mb['nick']."님의 게시물 스크랩";

$colspan = 3;

$sql_search = " where mid = '".$mb['mid']."' ";
$cnt = sql_fetch(" select count(*) as cnt from $web[bbs_scrap_table] $sql_search ");
$total_count = $cnt['cnt'];
$rows = 20;
$total_page  = ceil($total_count / $rows);
if (!$page) { $page = 1; }
$from_record = ($page - 1) * $rows;
$paging = paging(10, $page, $total_page, "?mid=".$mb['mid']."&sort=".$sort."&page=");

if ($sort == '1') {

    $sql_sort = "position asc, id asc";

}

else if ($sort == '2') {

    $sql_sort = "ar_datetime desc";

}

else if ($sort == '3') {

    $sql_sort = "datetime desc";

} else {

    $sql_sort = "position asc, id asc";
    $sort = 1;

}

$result = sql_query(" select * from $web[bbs_scrap_table] $sql_search order by $sql_sort limit $from_record, $rows ");
?>
<!DOCTYPE html>
<html>
<head>
<?
include_once("$web[path]/_head.php");
?>
<script type="text/javascript">
$(document).ready( function() {

    $(".selectb select").selectBox();

    var f = document.formUpdate;

    f.mid.value = "<?=$mb['mid']?>";
    f.p.value = "<?=$p?>";

});

function bbsScrapPosition()
{

    win_open("scrap_position.php", "bbsScrapPosition", "width=520, height=650, scrollbars=no");

}

function bbsScrapDelete(scrap_id)
{

    var f = document.formUpdate;

    f.m.value = "d";
    f.id.value = scrap_id;

    if (confirm("삭제 하시겠습니까?")) {

        f.action = "<?=$web['host_bbs']?>/scrap_update.php";
        f.submit();

    }

}

function scrapLoading(sort)
{

    document.location.href = "?mid=<?=$mb['mid']?>&sort="+sort;

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div id="pop_wrap" class="layout-popup">
<div class="title">
<img src="<?=$web['host_img']?>/popup_arrow.png"><?=text($web['title']);?><div class="close"><a href="#" onclick="window.close(); return false;" title="닫기"><img src="<?=$web['host_img']?>/popup_close.png"></a></div>
</div>
<table height="40" border="0" cellspacing="0" cellpadding="0" class="selectb">
<tr>
    <td width="10"></td>
    <td class="selectb100">
<select id="sort" name="sort" class="select" onchange="scrapLoading(this.value);">
    <option value="1">기본정렬</option>
    <option value="2">작성일</option>
    <option value="3">스크랩일</option>
</select>
<script type="text/javascript">
document.getElementById("sort").value = "<?=$sort?>";
</script>
    </td>
</tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="list-array">
<colgroup>
    <col width="127">
    <col width="">
    <col width="100">
</colgroup>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#e8e8e8"></td></tr>
<tr class="subj">
    <td>이미지</td>
    <td>제목</td>
    <td>날짜</td>
</tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#e8e8e8"></td></tr>
<?
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $article = article($row['bbs_id'], $row['article_id']);

    $href = http_bbs($row['bbs_id'], $row['article_id']);

    $thumb_width = 80;
    $thumb_height = 80;

    $dir = $disk['path']."/thumb/".$row['bbs_id'];

    @mkdir($dir, 0707);
    @chmod($dir, 0707);

    $thumb_file = "";
    $thumb = "";

    $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($row['bbs_id'])."' and article_id = '".$row['article_id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

    if ($file['upload_file']) {

        $thumb_file = $disk['path']."/bbs/".$row['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
        $thumb_path = $dir."/".data_path("u", $file['upload_time']);
        $thumb = $thumb_path."/thumb".$thumb_width."x".$thumb_height."_".$file['upload_file'];

        if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

            @mkdir($thumb_path, 0707);
            @chmod($thumb_path, 0707);

            image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, false);

        }

        $thumb = str_replace($disk['path'], $disk['server'], $thumb);

    }

    if (!$thumb) {

        $thumb = bbs_thumb_create($row['bbs_id'], $article['datetime'], $article['ar_content'], $thumb_width, $thumb_height, false);
        $thumb_file = image_editor($article['ar_content']);

    }

    $img = "";

    if ($thumb) {

        $img = "<img src='".$thumb."' border='0'>";

    }

    if ($list[$i]['ar_adult']) {

        $img = "<img src='".$web['host_img']."/blank.png' width='80' height='80' border='0'>";

    }
?>
<tr class="array" height="100">
    <td><a href="<?=$href?>" target="_blank"><?=$img?></a></td>
    <td><p><a href="<?=$href?>" target="_blank"><? if (!$article['id']) { echo "<font color='#ff3300'>[삭제됨]</font> "; } ?><?=text($row['ar_title']);?><? if ($article['ar_reply']) { echo "<b class='bbsreply'>[".$article['ar_reply']."]</b>"; } ?></a><? if ($member['mid'] == $mb['mid']) { ?> <a href="#" onclick="bbsScrapDelete('<?=$row['id']?>'); return false;"><img src="<?=$web['host_img']?>/btn_delete.gif" border="0" align="absmiddle"></a><? } ?></p></td>
    <td><?=date("Y.m.d", strtotime($article['datetime']));?></td>
</tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#e8e8e8"></td></tr>
<? } ?>
<? if (!$i) { ?>
<tr class="not"><td colspan="<?=$colspan?>">데이터가 없습니다.</td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#e8e8e8"></td></tr>
<? } ?>
</table>
<? if ($total_count && $total_count > $rows) { ?>
<div class="web-page"><?=$paging;?></div>
<? } ?>
<table border="0" cellspacing="0" cellpadding="0" class="auto btn">
<tr>
<? if ($check_login && $member['mid'] == $mid) { ?>
    <td><a href="#" onclick="bbsScrapPosition(); return false;">위치변경</a></td>
<? } ?>
    <td><a href="#" onclick="window.close(); return false;">닫기</a></td>
</tr>
</table>
</div>
</body>
</html>