<?php // 댓글
include_once("./_tb.php");
if ($bid) { $bid = preg_match("/^[a-zA-Z0-9_\-]+$/", $bid) ? $bid : ""; }

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px;}

.layout-block {padding:50px 0;}
.layout-bbs {background-color:#ffffff;}
.layout-bbs .toptitle {text-align:center;}

.layout-bbs .blocktop {margin-top:30px; clear:both; position:relative; height:30px; padding-bottom:10px; border-bottom:1px solid #dadada;}
.layout-bbs .blocktop .btn {position:absolute; left:0; top:0; font-size:0px;}
.layout-bbs .blocktop .btn a {margin-left:4px; width:78px; text-align:center; display:inline-block; background-color:#ffffff; border:1px solid #dadada; border-radius:3px; padding:1px 0 2px 0;}
.layout-bbs .blocktop .btn a:first-child {margin-left:0;}
.layout-bbs .blocktop .btn a {text-decoration:none; font-weight:700; line-height:26px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .blocktop .btn a:hover {border:1px solid #7d7d7d;}
.layout-bbs .blocktop .btn a.on {border:1px solid #42abd7; color:#42abd7;}
.layout-bbs .blocktop .search {clear:both; position:absolute; right:0px; top:0; width:206px; vertical-align:top;}
.layout-bbs .blocktop .search .input, .layout-bbs .blocktop .search .btn_search {float:left;}
.layout-bbs .blocktop .search .input {margin-right:4px; padding:0 9px; width:150px; height:28px; border:0px; background-color:#f8f8f8; border:1px solid #dadada;}
.layout-bbs .blocktop .search .input {font-weight:bold; line-height:28px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-bbs .blocktop .search .input.focus {border:1px solid #59c2e6; background-color:#ffffff; color:#000000;}
.layout-bbs .blocktop .search .btn_search {display:block; width:32px; height:32px; background:url('<?=$web['host_member']?>/img/_btn_search_w32_h32.png') no-repeat;}
.layout-bbs .blocktop .search .btn_search:hover {background-position:0 -32px;}
.layout-bbs .blocktop .btn_search:active {background-position:0 -64px;}

.layout-bbs .header {clear:both; position:relative; height:26px; margin-top:10px;}
.layout-bbs .header .count {position:absolute; right:0px; top:0;}
.layout-bbs .header .count {font-weight:400; line-height:28px; font-size:13px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .header .count img {vertical-align:middle; margin:0 7px 0 0px;}

.layout-bbs .list {clear:both; margin-top:10px;}
.layout-bbs .list tr.title td {position:relative; text-align:center; font-weight:700; line-height:38px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr.title td {border-left:1px solid #dadada;}
.layout-bbs .list tr.title td:last-child {border-right:1px solid #dadada;}
.layout-bbs .list tr td.date {width:160px;}
.layout-bbs .list tr td.bbs_title {width:120px;}
.layout-bbs .list tr.array:hover {background-color:#f7fbfd;}
.layout-bbs .list tr.array td.date {text-align:center; font-weight:400; line-height:14px; font-size:14px; color:#959595; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr.array td.bbs_title a {text-decoration:none; display:block; text-align:center; font-weight:700; line-height:14px; font-size:14px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr.array td.content {position:relative;}
.layout-bbs .list tr.array td.content a {text-decoration:none; display:block; height:80px; vertical-align:top;}
.layout-bbs .list tr.array td.content .content {padding-top:20px; overflow:hidden; height:20px; text-overflow:ellipsis; word-break:break-all;}
.layout-bbs .list tr.array td.content .content {font-weight:700; line-height:20px; font-size:14px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr.array td.content .block {margin-top:2px; font-size:0px;}
.layout-bbs .list tr.array td.content .nick {display:inline-block; font-weight:bold; line-height:14px; font-size:12px; color:#7d7d7d; font-family:gulim,serif;}
.layout-bbs .list tr.array td.content .ar_title {margin-top:2px; vertical-align:top; display:inline-block; font-weight:normal; line-height:14px; font-size:12px; color:#959595; font-family:gulim,serif;}
.layout-bbs .list tr.array td.content .ar_title nobr {display:block; width:300px; overflow:hidden; text-overflow:ellipsis; word-break:break-all; height:14px;}
.layout-bbs .list tr.array td.content .line {position:relative; overflow:hidden; left:0; top:2px; margin:0 7px; display:inline-block; border-left:1px solid #ededed; height:12px;}
.layout-bbs .list tr.array td.content .date {display:inline-block; font-weight:normal; line-height:14px; font-size:12px; color:#959595; font-family:gulim,serif;}
.layout-bbs .list tr.array td.content .mobile {display:none;}

.layout-bbs .list tr.not {text-align:center; font-weight:700; line-height:200px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}

.layout-bbs .header .selectb .select {text-align:left; height:24px; font-weight:bold; line-height:15px; font-size:13px; color:#7d7d7d; font-family:gulim,Helvetica,AppleGothic,Sans-serif;}
.layout-bbs .header .selectb .selectBox-dropdown .selectBox-label {line-height:12px; text-align:left; padding:7px 5px 0px 5px; color:#7d7d7d;}
.selectBox-dropdown {border: solid 1px #dbe1e8;}
.selectBox-dropdown .selectBox-arrow {width:25px; background: url('<?=$web['host_img']?>/jquery.selectBox-arrow.gif') 50% center no-repeat; border-left: solid 1px #dbe1e8;}
.selectBox-dropdown:focus,
.selectBox-dropdown:focus .selectBox-arrow {border-color: #59c2e6;}
.selectBox-dropdown-menu {border: solid 1px #59c2e6;}
.selectBox-options li.selectBox-hover A {background-color: #f8f8f8;}
.selectBox-options li.selectBox-selected A {background-color: #f8f8f8;}

@media screen and (max-width:1000px) {

.layout-bbs .list tr.array td.content .pc {display:none;}
.layout-bbs .list tr.array td.content .ar_title {display:none;}

}

@media screen and (max-width:600px) {

.layout-bbs .blocktop .btn {left:5px;}
.layout-bbs .blocktop .search {right:5px;}
.layout-bbs .header {padding:0 5px;}
.layout-bbs .header .count {right:5px;}
.layout-bbs .blocktop .search {width:196px;}
.layout-bbs .blocktop .search .input {width:140px;}

.layout-bbs .list tr.line {display:none;}
.layout-bbs .list tr.title {display:none;}

.layout-bbs .list tr td.date,
.layout-bbs .list tr td.bbs_title,
.layout-bbs .list tr.array td.content .pc {display:none;}
.layout-bbs .list tr.array td.content a {padding:0 10px;}
.layout-bbs .list tr.array td.content .mobile {display:inline-block;}

.layout-bbs .list tr.array td.content .content .title {font-weight:bold; line-height:20px; font-size:14px; color:#363636; font-family:gulim,serif;}
.layout-bbs .blocktop .btn a {width:auto; padding:1px 9px 2px 9px;}

}
</style>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "전체 댓글";
include_once("$web[path]/_top.php");

$colspan = 3;

$sql_search = " where reply = 1 ";

if ($bid) {

    $sql_search .= " and bbs_id = '".$bid."' ";

}

if ($q) {

    $sql_search .= " and nick = '".addslashes($q)."' ";

}

$cnt = sql_fetch(" select count(*) as cnt from $web[notice_table] $sql_search ");
$total_count = $cnt['cnt'];
$rows = 20;
$total_page  = ceil($total_count / $rows);
if (!$p) { $p = 1; }
$from_record = ($p - 1) * $rows;
$paging = paging(5, $p, $total_page, "?bid=".addslashes($bid)."&amp;q=".addslashes($q)."&amp;p=");
$result = sql_query(" select * from $web[notice_table] $sql_search order by id desc limit $from_record, $rows ");
$list = array();
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $article = article($row['bbs_id'], $row['article_id']);
    $bbs = bbs($row['bbs_id']);
    $reply = reply($row['bbs_id'], $row['reply_id']);

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id'])."#reply".$row['reply_id'];
    $list[$i]['bbs_title'] = $bbs['bbs_title'];
    $list[$i]['ar_title'] = text($article['ar_title']);
    $list[$i]['content'] = text($reply['content']);

    if ($article['ar_secret']) {

        $list[$i]['content'] = "비밀 게시물입니다.";

    }

    $list[$i]['date1'] = text_date("Y. m. d [H:i]", $list[$i]['datetime'], "&nbsp;");

    if (date("Y-m-d", strtotime($list[$i]['datetime'])) == $web['time_ymd']) {

        $list[$i]['date2'] = text_date("H:i", $list[$i]['datetime'], "&nbsp;");

    } else {

        $list[$i]['date2'] = text_date("m.d", $list[$i]['datetime'], "&nbsp;");

    }

}

$dq = "작성자 검색";

if (!$q) {
    $q = $dq;
}
?>
<script type="text/javascript">
$(document).ready( function() {

    $(".selectb select").selectBox();

    $('.layout-bbs .blocktop .search .input').focus(function() {

        if ($(this).val() == '<?=text($dq)?>') {

            $(this).val('');
            $(this).addClass('focus');

        }

    }).blur(function() {

        if ($(this).val() == '') {

            $(this).val('<?=text($dq)?>');

        }

        $(this).removeClass('focus');

    });

});

function bbsSearch()
{

    var f = document.formbbs;

    if (f.q.value == '<?=text($dq)?>') {
        f.q.value = '';
    }

    f.submit();

}
</script>
<div class="layout-block">
<div class="layout-bbs">
<div class="toptitle"><img src="<?=$web['host_member']?>/img/bbs_reply_title.png"></div>
<form method="get" name="formbbs">
<div class="blocktop">
<div class="btn">
<a href="<?=$web['host_member']?>/bbs/all_article.php?q=<? if ($q != $dq) { echo text($q); } ?>">게시물</a>
<a href="<?=$web['host_member']?>/bbs/all_reply.php?q=<? if ($q != $dq) { echo text($q); } ?>" class="on">댓글</a>
</div>
<div class="search"><input type="text" name="q" value="<?=text($q)?>" class="input" /><a href="#" onclick="bbsSearch(); return false;" class="btn_search"><input type="image" src="<?=$web['host_img']?>/blank.png" alt="" width="0" height="0" /></a></div>
</div>
<div class="header">
<div class="selectb selectb200">
<select id="bid" name="bid" class="select" onchange="bbsSearch();">
<option value="">전체 게시판</option>
<?
$result = sql_query(" select * from $web[bbs_table] where bbs_onoff = 1 order by bbs_group asc, bbs_title asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    echo "<option value='".$row['bbs_id']."'>".text($row['bbs_title'])."</option>";

}
?>
</select>
<script type="text/javascript">
document.getElementById("bid").value = "<?=text($bid)?>";
</script>
</div>
<div class="count"><img src="<?=$web['host_member']?>/img/bbs_title.png">댓글 <font color="#363636"><?=number($total_count)?></font>건</div>
</div>
</form>
<div class="list">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr class="title">
    <td class="date">작성일시</td>
    <td class="bbs_title">게시판명</td>
    <td class="content">댓글</td>
</tr>
<tr class="line"><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? for ($i=0; $i<count($list); $i++) { ?>
<tr class="array">
    <td class="date"><span class="date1"><?=$list[$i]['date1']?></span></td>
    <td class="bbs_title"><p class="center"><a href="<?=http_bbs($list[$i]['bbs_id'], "")?>" target="_blank"><?=text($list[$i]['bbs_title'])?></a></p></td>
    <td class="content">
<a href="<?=$list[$i]['href']?>" target="_blank">
<? if ($list[$i]['thumb']) { ?><div class="thumb"><div class="table"><div><?=$list[$i]['thumb']?></div></div></div><? } ?>
<div class="content"><?=$list[$i]['content']?></div>
<div class="block">
<span class="nick"><span class="sideview" onclick="sideviewLoad(this, '<?=text($list[$i]['mid'])?>'); return false;" title="<?=text($list[$i]['nick'])?>"><?=text($list[$i]['nick'])?></span></span>
<span class="pc line"></span>
<span class="pc ar_title"><nobr><?=$list[$i]['ar_title']?></nobr></span>
<span class="mobile line"></span>
<span class="mobile date"><?=text($list[$i]['date2'])?></span>
</div>
</a>
    </td>
</tr>
<tr><td height="1" bgcolor="#f0f0f0" colspan="<?=$colspan?>"></td></tr>
<? } ?>
<? if (!$i) { ?>
<tr class="not"><td colspan="<?=$colspan?>">데이터가 없습니다.</td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? } ?>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
</table>
</div>
<div class="web-page">
<? if ($total_count && $total_count > $rows) { echo $paging; } ?>
</div>
</div>
</div>
<?
include_once("$web[path]/_bottom.php");
?>