<?php // 게시물
include_once("./_tb.php");
if (!$check_login) { url($web['host_member']."/login/?url={$urlencode}"); }

// head start
ob_start();
?>
<link rel="stylesheet" href="<?=$web['host_member']?>/_menu.css" type="text/css" />
<style type="text/css">
body {min-width:320px;}

.layout-block {padding:50px 0;}

.layout-bbs {background-color:#ffffff;}
.layout-bbs .btn {clear:both; font-size:0px; padding:10px 0;}
.layout-bbs .btn a {margin-left:4px; display:inline-block; background-color:#ffffff; border:1px solid #dadada; border-radius:3px; padding:1px 9px 2px 9px;}
.layout-bbs .btn a:first-child {margin-left:10px;}
.layout-bbs .btn a {text-decoration:none; font-weight:700; line-height:26px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .btn a:hover {border:1px solid #7d7d7d;}
.layout-bbs .btn a.on {border:1px solid #42abd7; color:#42abd7;}

.layout-bbs .header {clear:both; position:relative; border-top:1px solid #dadada; padding-top:10px; margin-bottom:10px;}
.layout-bbs .header .count {font-weight:400; line-height:28px; font-size:13px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .header .count img {vertical-align:middle; margin:0 7px 0 0px;}
.layout-bbs .header .search {clear:both; position:absolute; right:0px; top:10px; width:206px; vertical-align:top;}
.layout-bbs .header .search .input, .layout-bbs .header .search .btn_search {float:left;}
.layout-bbs .header .search .input {margin-right:4px; padding:0 9px; width:150px; height:28px; border:0px; background-color:#f8f8f8; border:1px solid #dadada;}
.layout-bbs .header .search .input {font-weight:bold; line-height:28px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-bbs .header .search .input.focus {border:1px solid #59c2e6; background-color:#ffffff; color:#000000;}
.layout-bbs .header .search .btn_search {display:block; width:32px; height:32px; background:url('<?=$web['host_member']?>/img/_btn_search_w32_h32.png') no-repeat;}
.layout-bbs .header .search .btn_search:hover {background-position:0 -32px;}
.layout-bbs .header .search .btn_search:active {background-position:0 -64px;}
.layout-bbs .header .search.mobile {display:none;}
.layout-bbs .header .block {display:none;}
.layout-bbs .header .block.on {display:block;}
.layout-bbs .header .block {margin-top:12px; height:30px; padding:10px 10px 0 10px; clear:both; border-top:1px solid #dadada;}
.layout-bbs .header .block .input, .layout-bbs .header .block .btn_search {float:left;}
.layout-bbs .header .block .input {margin-right:4px; padding:0 9px; width:72%; height:28px; border:0px; background-color:#f8f8f8; border:1px solid #dadada;}
.layout-bbs .header .block .input {font-weight:bold; line-height:28px; font-size:13px; color:#999999; font-family:gulim,serif;}
.layout-bbs .header .block .btn_search {display:block; width:56px; height:30px; background:url('<?=$web['host_member']?>/img/_btn_search_w56_h30.png') no-repeat;}
.layout-bbs .header .hide {display:none;}

.layout-bbs .list {clear:both;}
.layout-bbs .list tr.title td {position:relative; text-align:center; font-weight:700; line-height:38px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr.title td {border-left:1px solid #dadada;}
.layout-bbs .list tr.title td:last-child {border-right:1px solid #dadada;}
.layout-bbs .list tr td.date {width:160px;}
.layout-bbs .list tr td.bbs_title {width:120px;}
.layout-bbs .list tr.array:hover {background-color:#f7fbfd;}
.layout-bbs .list tr.array td.date {text-align:center; line-height:50px; font-size:13px; color:#959595; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr.array td.bbs_title a {display:block; text-align:center; line-height:50px; font-size:13px; color:#7d7d7d; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr.array td.ar_title {text-align:left; font-weight:700; line-height:50px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr.array td a {text-decoration:none; display:block;}
.layout-bbs .list tr.array td p {margin:0px; text-align:left; overflow:hidden; height:50px; padding:0 10px; word-break:break-all;}
.layout-bbs .list tr.array td p.center {text-align:center;}
.layout-bbs .list tr.array .mobile {display:none;}
.layout-bbs .list tr.not {text-align:center; font-weight:700; line-height:200px; font-size:13px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-bbs .list tr td.date .date2 {display:none;}

@media screen and (max-width:700px) {

.layout-bbs .list tr td.date {width:70px;}
.layout-bbs .list tr td.date .date1 {display:none;}
.layout-bbs .list tr td.date .date2 {display:block;}

}

@media screen and (max-width:600px) {

.layout-bbs .btn a:first-child {margin-left:5px;}
.layout-bbs .header .count img {display:none;}
.layout-bbs .header .count {margin-left:5px;}
.layout-bbs .header .search {width:32px; right:5px;}
.layout-bbs .header .search .input {display:none;}

.layout-bbs .list tr.line {display:none;}
.layout-bbs .list tr.title {display:none;}
.layout-bbs .list tr.array td.date {display:none;}
.layout-bbs .list tr.array td.bbs_title {display:none;}
.layout-bbs .list tr.array .pc {display:none;}
.layout-bbs .list tr.array .mobile {position:relative; display:block; padding:13px 9px 12px 0;}
.layout-bbs .list tr.array td.ar_title {font-weight:bold; line-height:18px; font-size:14px; color:#363636; font-family:gulim,serif;}
.layout-bbs .list tr.array td p {height:18px;}
.layout-bbs .list tr.array span.bbs_title {line-height:13px; font-size:12px; color:#959595; font-family:gulim,serif;}
.layout-bbs .list tr.array span.date {line-height:13px; font-size:12px; color:#959595; font-family:gulim,serif;}
.layout-bbs .list tr.array span.line {padding:0 6px 0 7px; line-height:13px; font-size:11px; color:#dadada; font-family:dotum,serif;}
.layout-bbs .header .search.pc {display:none;}
.layout-bbs .header .search.mobile {display:block;}
.layout-bbs .header .hide {display:block;}

}
</style>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "내 게시물";
include_once("$web[path]/_top.php");

$colspan = 3;

$sql_search = " where mid = '".$member['mid']."' ";

if ($q) {

    $qq = search_text(2, $q);

    $sql_search .= " and INSTR(q, '".addslashes($qq)."') ";

}

$cnt = sql_fetch(" select count(*) as cnt from $web[search_table] $sql_search ");
$total_count = $cnt['cnt'];
$rows = 20;
$total_page  = ceil($total_count / $rows);
if (!$p) { $p = 1; }
$from_record = ($p - 1) * $rows;
$paging = paging(5, $p, $total_page, "?q=".addslashes($q)."&amp;p=");
$result = sql_query(" select * from $web[search_table] $sql_search order by id desc limit $from_record, $rows ");
$list = array();
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $article = article($row['bbs_id'], $row['article_id']);
    $bbs = bbs($row['bbs_id']);

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);
    $list[$i]['bbs_title'] = $bbs['bbs_title'];
    $list[$i]['ar_title'] = text($article['ar_title']);

    $list[$i]['ar_reply'] = "";

    if ($article['ar_reply']) {

        $list[$i]['ar_reply'] = " <font color='#42abd7'>(".$article['ar_reply'].")</font>";

    }

    $list[$i]['date1'] = text_date("Y. m. d [H:i]", $list[$i]['datetime'], "&nbsp;");

    if (date("Y-m-d", strtotime($list[$i]['datetime'])) == $web['time_ymd']) {

        $list[$i]['date2'] = text_date("H:i", $list[$i]['datetime'], "&nbsp;");

    } else {

        $list[$i]['date2'] = text_date("m.d", $list[$i]['datetime'], "&nbsp;");

    }

}

$dq = "게시물 검색";

if (!$q) {
    $q = $dq;
}
?>
<script type="text/javascript">
$(document).ready( function() {

    // 게시물
    $('.member-menu div[name=bbs]').addClass('on');

    $('#mq, .layout-bbs .header .search .input').focus(function() {

        if ($(this).val() == '<?=text($dq)?>') {

            $(this).val('');
            $(this).addClass('focus');

        }

    }).blur(function() {

        if ($(this).val() == '') {

            $(this).val('<?=text($dq)?>');

        }

        $(this).removeClass('focus');

    });

});

function bbsSearch()
{

    var f = document.formbbs;

    if (f.q.value == '<?=text($dq)?>') {
        f.q.value = '';
    }

    if ($('.layout-bbs .header .block').is(':hidden') == false) {

        if ($('#mq').val() != '<?=text($dq)?>') {
            f.q.value = $('#mq').val();

        }

    }

    f.submit();

}

function bbsSearchLayer()
{

    var obj = $('.layout-bbs .header .block');

    if (obj.is(':hidden') == true) {

        obj.addClass('on');

        $('#mq').focus();

    } else {

        obj.removeClass('on');

    }

}
</script>
<div class="layout-block">
<div class="layout-bbs">
<?
// 메뉴 호출
include_once("$web[path]/member/_menu.php");
?>
<div class="btn">
<a href="<?=$web['host_member']?>/bbs/" class="on">게시물</a>
<a href="<?=$web['host_member']?>/bbs/reply.php">댓글</a>
</div>
<div class="header">
<div class="count"><img src="<?=$web['host_member']?>/img/bbs_title.png">게시물 <font color="#363636"><?=number($total_count)?></font>건</div>
<form method="get" name="formbbs">
<div class="search pc"><input type="text" name="q" value="<?=text($q)?>" class="input" /><a href="#" onclick="bbsSearch(); return false;" class="btn_search"><input type="image" src="<?=$web['host_img']?>/blank.png" alt="" width="0" height="0" /></a></div>
<div class="search mobile"><a href="#" onclick="bbsSearchLayer(); return false;" class="btn_search"></a></div>
<div class="hide">
<div class="block">
<input type="text" id="mq" value="<?=text($q)?>" class="input" /><a href="#" onclick="bbsSearch(); return false;" class="btn_search"></a>
</div>
</div>
</form>
</div>
<div class="list">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr class="title">
    <td class="date">작성일시</td>
    <td class="bbs_title">게시판명</td>
    <td class="ar_title">게시물 제목</td>
</tr>
<tr class="line"><td colspan="<?=$colspan?>" height="1" bgcolor="#dadada"></td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? for ($i=0; $i<count($list); $i++) { ?>
<tr class="array">
    <td class="date"><span class="date1"><?=$list[$i]['date1']?></span><span class="date2"><?=$list[$i]['date2']?></span></td>
    <td class="bbs_title"><p class="center"><a href="<?=http_bbs($list[$i]['bbs_id'], "")?>" target="_blank"><?=text($list[$i]['bbs_title'])?></a></p></td>
    <td class="ar_title">
<div class="pc"><a href="<?=$list[$i]['href']?>" target="_blank"><p title="<?=text($list[$i]['ar_title']);?>"><?=text($list[$i]['ar_title'])?><?=$list[$i]['ar_reply']?></p></a></div>
<div class="mobile">
<a href="<?=$list[$i]['href']?>" target="_blank">
<p title="<?=text($list[$i]['ar_title']);?>"><?=text($list[$i]['ar_title'])?><?=$list[$i]['ar_reply']?></p>
<p><span class="bbs_title"><?=text($list[$i]['bbs_title'])?></span><span class="line">|</span><span class="date"><?=$list[$i]['date2']?></span></p>
</a>
</div>
    </td>
</tr>
<tr><td height="1" bgcolor="#f0f0f0" colspan="<?=$colspan?>"></td></tr>
<? } ?>
<? if (!$i) { ?>
<tr class="not"><td colspan="<?=$colspan?>">데이터가 없습니다.</td></tr>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
<? } ?>
<tr><td colspan="<?=$colspan?>" height="1" bgcolor="#f0f0f0"></td></tr>
</table>
</div>
<div class="web-page">
<? if ($total_count && $total_count > $rows) { echo $paging; } ?>
</div>
</div>
</div>
<?
include_once("$web[path]/_bottom.php");
?>