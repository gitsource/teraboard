<?php
include_once("./_tb.php");

if (!$check_login) {

    message("<p class='title'>알림</p><p class='text'>로그인 후 이용하세요.</p>", "b");

}

$web['title'] = "게시물 스크랩 위치 변경";
?>
<!DOCTYPE html>
<html>
<head>
<?
include_once("$web[path]/_head.php");
?>
<style type="text/css">
html {overflow:hidden;}
</style>
<script type="text/javascript">
var removedlist = new Array();
var addedlist = new Array();

$(document).ready( function() {

    selectColor();

});

function moveToRight()
{

    var selLeft = document.formArticle.left_select.options;
    var selRight = document.formArticle.right_select.options;

    if (selLeft.selectedIndex < 0) {

        alert("스크랩을 선택하세요.");
        return false;

    }

    for (var i=0;i<selLeft.length;i++) {

        if (selRight.length>=30) {

            alert("30개까지만 선택할 수 있습니다.");
            selectColor();
            return false;

        }

        if (selLeft[i].selected) {

            var found_in_right = false;

            for (var j=0;j<selRight.length;j++) {

                if (selLeft[i].value == selRight[j].value) {

                    found_in_right = true;
                    break;

                }

            }

            if (!found_in_right) {

                var newOpt = document.createElement("OPTION");

                newOpt.text = selLeft[i].text;	
                newOpt.value = selLeft[i].value;
                selRight.add(newOpt);

                if (!removedlist[selLeft[i].value]) {

                    addedlist[selLeft[i].value] = true;

                }

                delete removedlist[selLeft[i].value];

            }

        }

    }

    selectColor();

}

function moveToLeft()
{

    var selRight = document.formArticle.right_select.options;

    if (selRight.selectedIndex < 0) {

        alert("삭제할 스크랩을 선택하세요.");
        return false;

    }

    for (var i=0;i<selRight.length;i++) {

        if (selRight[i].selected) {

            removedlist[selRight[i].value] = true;

            if (!addedlist[selRight[i].value]) {

                delete addedlist[selRight[i].value];

            }

            document.formArticle.right_select.remove(i);
            i--;

        }

    }

    selectColor();

}

function selectColor()
{

    var n1=document.formArticle.right_select.options.length;
    var n2=document.formArticle.left_select.options.length;

    for (var j=0;j<n2;j++) {

        document.formArticle.left_select.options[j].style.color='black';

    }

    for (var i=0;i<n1;i++) {

        for (var j=0;j<n2;j++) {

            if (document.formArticle.left_select.options[j].value==document.formArticle.right_select.options[i].value) {

                document.formArticle.left_select.options[j].style.color='#3366FF';
                break;

            }

        }

    }

}

function moveVertically(type, frm)
{

    var selRight = frm.right_select;
    var index = selRight.selectedIndex;

    if (index < 0) {
        alert("게시물을 선택하세요.");
        return false;
    }

    if (type == "U") {

        if (index > 0) {

            swap(selRight, index, index - 1);

        }

    }

    else if (type == "D") {

        if (index < selRight.options.length-1) {

            swap(selRight, index, index + 1);

        }
    }

    else if (type == "T") {

		    for (var i = index; i > 0; i--) {

            swap(selRight, i, i - 1);

		    }

    }

    else if (type == "B") {

        for (var i = index; i < selRight.options.length - 1; i++) {

            swap(selRight, i, i + 1);

        }

    }

    selectColor();

}

function swap(selectedOption, index, targetIndex)
{

    var onetext = selectedOption.options[targetIndex].text;
    var onevalue = selectedOption.options[targetIndex].value;

    selectedOption.options[targetIndex].text = selectedOption.options[index].text;
    selectedOption.options[targetIndex].value = selectedOption.options[index].value;
    selectedOption.options[index].text = onetext;
    selectedOption.options[index].value = onevalue;
    selectedOption.options.selectedIndex = targetIndex;
    selectedOption.options[targetIndex].selected = true;

}

function updateMyMenuList()
{

    var selRight = document.formArticle.right_select;
    var addedArray = new Array(selRight.options.length);

    for (var i = 0; i < selRight.options.length; i++) {

        addedArray[i] = selRight.options[i].value;

    }

    document.formArticle.added_list.value = addedArray;

    var removedArray = new Array();

    for (var i in removedlist) {

        removedArray.push(i);

    }

    var addedArray = new Array();

    for (var i in addedlist) {

        addedArray.push(i);

    }

    document.formArticle.removed_list.value = removedArray;
    document.formArticle.inserted_list.value = addedArray;
	
    document.formArticle.submit();

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div id="pop_wrap" class="layout-popup">
<form method="post" name="formArticle" action="<?=$web['host_bbs']?>/scrap_update.php">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="u" />
<input type="hidden" name="added_list" />
<input type="hidden" name="removed_list" />
<input type="hidden" name="inserted_list" />
<div class="title">
<img src="<?=$web['host_img']?>/popup_arrow.png"><?=text($web['title']);?><div class="close"><a href="#" onclick="window.close(); return false;" title="닫기"><img src="<?=$web['host_img']?>/popup_close.png"></a></div>
</div>
<div style="padding:10px;">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td>
<select name="left_select" size="10" ondblclick="moveToRight();" style="display:none;">
<?
$result = sql_query(" select * from $web[bbs_scrap_table] where mid = '".$member['mid']."' order by position asc, id asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    echo "<option value='".$row['id']."'>1</option>";

}
?>
</select>

<select id="right_select" Name="right_select" size="25" style="width:500px;">
<?
$result = sql_query(" select * from $web[bbs_scrap_table] where mid = '".$member['mid']."' order by position asc, id asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    echo "<option value='".$row['id']."'>".text($row['ar_title'])."</option>";

}
?>
</select>
    </td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" class="button" style="margin-top:5px;">
<tr>
    <td><button onclick="moveVertically('T', document.formArticle); return false;">맨위</button></td>
    <td width="2"></td>
    <td><button onclick="moveVertically('U', document.formArticle); return false;">위</button></td>
    <td width="2"></td>
    <td><button onclick="moveVertically('D', document.formArticle); return false;">아래</button></td>
    <td width="2"></td>
    <td><button onclick="moveVertically('B', document.formArticle); return false;">맨아래</button></td>
</tr>
</table>
</div>
<table border="0" cellspacing="0" cellpadding="0" class="auto btn">
<tr>
    <td><a href="#" onclick="updateMyMenuList(); return false;">확인</a></td>
    <td><a href="#" onclick="window.close(); return false;">닫기</a></td>
</tr>
</table>
</form>
</div>
</body>
</html>