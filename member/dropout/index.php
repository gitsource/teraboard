<?php // 회원 탈퇴
include_once("./_tb.php");
if (!$check_login) { url($web['host_member']."/login/?url={$urlencode}"); }

$setup_join = setup_join();

if ($m == 'overlap_uid') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    if ($uid) { $uid = strtolower(trim($uid)); $uid = preg_match("/^[a-zA-Z0-9_\-]+$/", $uid) ? $uid : ""; }

    exit;

}

// head start
ob_start();
?>
<link rel="stylesheet" href="<?=$web['host_member']?>/_menu.css" type="text/css" />
<style type="text/css">
body {min-width:360px;}

.layout-member {padding:50px 0;}
.layout-member p {margin:0px;}
.layout-member .title {margin-top:38px; font-weight:700; line-height:20px; font-size:20px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .msg {margin:11px 0 38px 0; line-height:14px; font-size:12px; color:#b0b0b0; font-family:gulim,serif;}

.layout-member .box {border-top:1px solid #dbe1e8;}
.layout-member .subj {min-width:160px; width:160px; text-align:center; background-color:#f8f8f8;}
.layout-member .subj {font-weight:bold; line-height:18px; font-size:12px; color:#90959b; font-family:gulim,serif;}
.layout-member .ess {position:relative; overflow:hidden; left:0; top:1px;  margin-left:5px; display:inline-block; width:8px; height:11px;}
.layout-member .line {min-width:20px; width:20px;}
.layout-member .list {padding:20px 0;}
.layout-member td.text {font-weight:bold; line-height:15px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member td.text2 {line-height:22px; font-size:13px; color:#000000; font-family:gulim,serif;}

.layout-member .input_focus {width:186px; height:24px; border:1px solid #dbe1e8; background-color:#f8f8f8; padding:0px 6px;}
.layout-member .input_focus {font-weight:bold; line-height:24px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member .input_focus.focus {border:1px solid #59c2e6; background-color:#ffffff; color:#000000;}

.layout-member .submit {margin-top:20px;}
.layout-member .submit div {display:inline-block; width:50%;}
.layout-member .submit div span {margin-left:2px; display:block; text-align:center; cursor:pointer; height:59px; border:1px solid #dadada; background-color:#ffffff;}
.layout-member .submit div span {font-weight:700; line-height:59px; font-size:20px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .submit div.on span {margin-left:0px; margin-right:2px; border:1px solid #9e0b0f; background-color:#ed1c24; color:#ffffff;}
</style>
<script type="text/javascript">
$(document).ready( function() {

    // 내정보
    $('.member-menu div[name=info]').addClass('on');

    $('.input_focus').focus(function() {

        $(this).addClass('focus');

    }).blur(function() {

        $(this).removeClass('focus');

    });

    $('.btn_confirm').click(function() {

        submitSetup();

    });

    $('.btn_cancel').click(function() {

        history.go(-1);

    });

});

function submitSetup()
{

    var f = document.formSetup;

<? if ($member['upw']) { ?>
    if (f.upw.value == '') {

        alert("비밀번호를 입력하세요.");
        f.upw.focus();
        return false;

    }
<? } ?>

    f.action = "update.php";
    f.submit();

}
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "회원 탈퇴";
include_once("$web[path]/_top.php");
?>
<div class="layout-member">
<?
// 메뉴 호출
include_once("$web[path]/member/_menu.php");
?>
<form method="post" name="formSetup" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="d" />
<p class="title">회원 탈퇴</p>
<p class="msg">회원 탈퇴 시, 안내사항을 반드시 확인하시기 바랍니다.</p>
<!-- box start //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">아이디<span class="ess"></span></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="text"><?=text($member['uid'])?></td>
</tr>
</table>
    </td>
</tr>
</table>
<!-- box end //-->
<!-- box start //-->
<? if ($member['upw']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">비밀번호</td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="password" id="upw" name="upw" value="" class="input_focus" /></td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">안내사항</td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="text2"><b>사용하고 계신 아이디(<?=text($member['uid'])?>)는 탈퇴 시, 재사용 및 복구가 불가능하오니 신중히 생각하시기 바랍니다.</b></td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="text2">- 탈퇴시에는 즉시 본인 또는 타인을 통해서도 로그인 및 서비스 이용이 불가합니다.</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:30px;">
<tr>
    <td class="text2">
<?
// 자동삭제사용
if ($setup['del_dropout_onoff']) {

    // 기간 입력
    if ($setup['del_dropout_day']) {

        echo "<b>탈퇴 회원의 사칭 및 기타 사고 방지를 목적으로 회원정보는 <font color='#ed1c24'>".text($setup['del_dropout_day'])."일</font> 후에 자동 삭제됩니다.</b>";

    } else {

        echo "<b>탈퇴 회원의 사칭 및 기타 사고 방지를 목적으로 회원정보는 <font color='#ed1c24'>탈퇴 즉시</font> 삭제됩니다.</b>";

    }

} else {

    echo "<b>탈퇴 회원의 사칭 및 기타 사고 방지를 목적으로 회원정보는 <font color='#ed1c24'>관리자 확인</font> 후에 삭제됩니다.</b>";

}
?>
    </td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="text2">- 탈퇴시에는 즉시 본인 또는 타인을 통해서도 로그인 및 서비스 이용이 불가합니다.</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="text2">- 임시보존 기간 동안에는 관리자를 제외한 그 누구도 회원님의 개인정보를 열람할 수 없습니다.</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:30px;">
<tr>
    <td class="text2"><b>탈퇴 후에도 회원님께서 남기신 게시판형 서비스의 기록은 남아있습니다.</b></td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="text2">- 삭제를 원하는 게시물이 있다면, 탈퇴전 반드시 비공개 처리하거나 삭제하시기 바랍니다.</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="text2">- 탈퇴 후에는 회원정보가 삭제되어 본인 유무를 판단할 수 없으므로, 게시물을 임의로 삭제해드릴 수 없습니다. </td>
</tr>
</table>
    </td>
</tr>
</table>
<!-- box end //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr><td></td></tr>
</table>
<div class="submit"><div class="on"><span tabindex="1" class="btn_confirm">탈퇴</span></div><div><span tabindex="2" class="btn_cancel">취소</span></div></div>
</form>
</div>
<?
include_once("$web[path]/_bottom.php");
?>