<?php // 회원 탈퇴
include_once("./_tb.php");
if ($check_login) { url($web['host_default'])."/"; }

// head start
ob_start();
?>
<style type="text/css">
body {min-width:320px; min-height:654px;}

.layout-dropout {padding:50px 0;}

.layout-dropout .msg {text-align:center;}
.layout-dropout .msg p {margin:0px;}
.layout-dropout .msg p.text {font-weight:700; line-height:36px; font-size:36px; color:#333333; font-family:'Nanum Gothic',gulim,serif;}
.layout-dropout .msg p.text2 {margin-top:25px; font-weight:bold; line-height:22px; font-size:13px; color:#858a8d; font-family:gulim,serif;}

.layout-dropout .submit {cursor:pointer; margin-top:44px; height:59px; border:1px solid #1192ca; background-color:#42abd7;}
.layout-dropout .submit {font-weight:700; line-height:59px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-dropout .submit span {display:block; text-align:center;}
.layout-dropout .submit.on {cursor:default; border:1px solid #b0b0b0; background-color:#f5f6f7; color:#999999;}
</style>
<script type="text/javascript">
$(document).ready( function() {

    $('.layout-dropout .submit span').click(function() {

        location.replace("<?=$web['host_default']?>/");

    });

});
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

include_once("$web[path]/_top.php");
?>
<form method="post" name="formSetup" autocomplete="off">
<input type="hidden" id="url" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="" />
<div class="layout-dropout">
<div class="msg">
<p class="text">회원 탈퇴 완료</p>
<p class="text2">회원 탈퇴를 완료하였습니다.</p>
</div>
<div class="submit"><span tabindex="1">홈으로</span></div>
</div>
</form>
<?
include_once("$web[path]/_bottom.php");
?>