<?php
include_once("./_tb.php");
echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

if (!$check_login) { url($web['host_default'])."/"; }

if ($m == 'd') {

    // 폼 체크
    if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    $upw = trim(strip_tags(sql_real_escape_string($_POST['upw'])));

    if ($member['upw'] && !$upw) {

        message("<p class='title'>알림</p><p class='text'>회원 비밀번호가 입력되지 않았습니다.</p>", "b");

    }

    if ($member['upw'] && $member['upw'] != sql_password($upw)) {

        message("<p class='title'>알림</p><p class='text'>회원 비밀번호가 올바르지 않습니다.</p>", "b");

    }

    if ($check_admin) {

        message("<p class='title'>알림</p><p class='text'>관리자는 관리자 페이지에서만 탈퇴가 가능합니다.</p>", "b");

    }

    member_dropout($member['mid']);

    set_cookie("login_mid", "", 0);
    set_cookie("login", "", 0);

    session_unset();
    session_destroy();

    url("result.php");

}
?>