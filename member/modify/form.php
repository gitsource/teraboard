<?php // 정보수정
include_once("./_tb.php");
if (!$check_login) { url($web['host_member']."/login/?url={$urlencode}"); }

$mid = get_session("member_modify");

if (!$mid || $member['mid'] != $mid) {

    message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

}

$setup_join = setup_join();

if ($m == 'overlap_uid') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    if ($uid) { $uid = strtolower(trim($uid)); $uid = preg_match("/^[a-zA-Z0-9_\-]+$/", $uid) ? $uid : ""; }

    $chk = member_uid($uid);

    if (!$chk['uid'] && $uid) {

        echo "<font color='#00a651'>사용가능. 다음 단계로 넘어가세요.</font>";

        echo "<script type='text/javascript'>";
        echo "document.getElementById('overlap_uid').value = \"".text($uid)."\";";
        echo "document.getElementById('uid').value = \"".text($uid)."\";";
        echo "</script>";

    } else {

        echo "<font color='#ed1c24'>사용불가. 새로운 회원 ID를 다시 입력하세요.</font>";

        echo "<script type='text/javascript'>";
        echo "document.getElementById('overlap_uid').value = \"\";";
        echo "</script>";

    }

    exit;

}

else if ($m == 'overlap_nick') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    if ($nick) { $nick = preg_match("/^[A-Za-z0-9_가-힣\x20]+$/", $nick) ? $nick : ""; }

    $chk = sql_fetch(" select * from $web[member_table] where nick = '".addslashes($nick)."' limit 0, 1 ");

    if (!$chk['nick'] && $nick) {

        echo "<font color='#00a651'>사용가능. 다음 단계로 넘어가세요.</font>";

        echo "<script type='text/javascript'>";
        echo "document.getElementById('overlap_nick').value = \"".text($nick)."\";";
        echo "document.getElementById('nick').value = \"".text($nick)."\";";
        echo "</script>";

    } else {

        echo "<font color='#ed1c24'>사용불가. 새로운 닉네임을 다시 입력하세요.</font>";

        echo "<script type='text/javascript'>";
        echo "document.getElementById('overlap_nick').value = \"\";";
        echo "</script>";

    }

    exit;

}

// head start
ob_start();
?>
<link rel="stylesheet" href="<?=$web['host_member']?>/_menu.css" type="text/css" />
<style type="text/css">
body {min-width:360px;}

.layout-member {padding:50px 0; min-width:700px; background-color:#ffffff;}
.layout-member p {margin:0px;}
.layout-member .title {margin-top:38px; font-weight:700; line-height:20px; font-size:20px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .msg {margin:11px 0 38px 0; line-height:14px; font-size:12px; color:#b0b0b0; font-family:gulim,serif;}

.layout-member .box {border-top:1px solid #dbe1e8;}
.layout-member .subj {min-width:160px; width:160px; text-align:center; background-color:#f8f8f8;}
.layout-member .subj {font-weight:bold; line-height:18px; font-size:12px; color:#90959b; font-family:gulim,serif;}
.layout-member .ess {position:relative; overflow:hidden; left:0; top:1px;  margin-left:5px; display:inline-block; width:8px; height:11px; background:url('<?=$web['host_member']?>/img/_ess.png') no-repeat;}
.layout-member .line {min-width:20px; width:20px;}
.layout-member .list {padding:20px 0;}
.layout-member td.text {font-weight:bold; line-height:15px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member .help {font-weight:400; line-height:15px; font-size:12px; color:#babec1; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .filesource {font-weight:400; line-height:18px; font-size:14px; color:#7e8e9f; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .filesize {font-weight:400; line-height:18px; font-size:14px; color:#b7b7b7; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .filedel {font-weight:400; line-height:18px; font-size:12px; color:#4f5f6f; font-family:'Nanum Gothic',gulim,serif;}

.layout-member .member_thumb {width:48px; height:48px; border:2px solid #dbe1e8; background-color:#f0f3f6;}
.layout-member .member_thumb img {width:48px; height:48px;}

.layout-member .selectbox {position:relative; width:200px; height:26px;}
.layout-member .selectbox .select {background:url('<?=$web['host_member']?>/img/_selectbox_w200_h26.png') no-repeat;}
.layout-member .selectbox .select {position:absolute; left:0; top:0; width:200px; height:26px; background-position:0 0; cursor:pointer;}
.layout-member .selectbox .select .block {position:relative; width:200px;}
.layout-member .selectbox .select .block {font-weight:bold; line-height:26px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member .selectbox .select .block span.text {overflow-x:hidden; height:26px; display:block; padding:0 16px 0 6px;}
.layout-member .selectbox .select .block .option {display:none; position:absolute; left:0; top:26px; width:200px;}
.layout-member .selectbox .select .block .option ul.array {overflow:auto; overflow-x:hidden; max-height:200px; font-size:0px; background-color:#f0f3f6; border-left:1px solid #dbe1e8; border-right:1px solid #dbe1e8; border-bottom:1px solid #dbe1e8;}
.layout-member .selectbox .select .block .option ul.array li {overflow-x:hidden; padding-left:6px; display:block; vertical-align:middle; height:30px; border-top:1px solid #dbe1e8;}
.layout-member .selectbox .select .block .option ul.array li.first {border-top:0px;}
.layout-member .selectbox .select .block .option ul.array li {font-weight:bold; line-height:30px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member .selectbox .select .block .option ul.array li:hover {color:#42abd7;}
.layout-member .selectbox.opt .select {background-position:0 -26px;}
.layout-member .selectbox.opt .select span.text {color:#ffffff;}
.layout-member .selectbox.opt .select .block .option {display:block;}

.layout-member .selectbox.type2 {position:relative; width:500px; height:26px;}
.layout-member .selectbox.type2 .select {background:url('<?=$web['host_member']?>/img/_selectbox_w500_h26.png') no-repeat;}
.layout-member .selectbox.type2 .select {position:absolute; left:0; top:0; width:500px; height:26px; background-position:0 0; cursor:pointer;}
.layout-member .selectbox.type2 .select .block {position:relative; width:500px;}
.layout-member .selectbox.type2 .select .block {font-weight:bold; line-height:26px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member .selectbox.type2 .select .block span.text {overflow-x:hidden; height:26px; display:block; padding:0 16px 0 6px;}
.layout-member .selectbox.type2 .select .block .option {display:none; position:absolute; left:0; top:26px; width:500px;}
.layout-member .selectbox.type2 .select .block .option ul.array {overflow:auto; overflow-x:hidden; max-height:200px; font-size:0px; background-color:#f0f3f6; border-left:1px solid #dbe1e8; border-right:1px solid #dbe1e8; border-bottom:1px solid #dbe1e8;}
.layout-member .selectbox.type2 .select .block .option ul.array li {overflow-x:hidden; padding-left:6px; display:block; vertical-align:middle; height:30px; border-top:1px solid #dbe1e8;}
.layout-member .selectbox.type2 .select .block .option ul.array li.first {border-top:0px;}
.layout-member .selectbox.type2 .select .block .option ul.array li {font-weight:bold; line-height:30px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member .selectbox.type2 .select .block .option ul.array li:hover {color:#42abd7;}
.layout-member .selectbox.type2.opt .select {background-position:0 -26px;}
.layout-member .selectbox.type2.opt .select span.text {color:#ffffff;}
.layout-member .selectbox.type2.opt .select .block .option {display:block;}

.layout-member .input_focus {width:186px; height:24px; border:1px solid #dbe1e8; background-color:#f8f8f8; padding:0px 6px;}
.layout-member .input_focus {font-weight:bold; line-height:24px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member .input_focus.focus {border:1px solid #59c2e6; background-color:#ffffff; color:#000000;}

.layout-member .input {width:186px; height:24px; border:1px solid #dbe1e8; background-color:#f8f8f8; padding:0px 6px;}
.layout-member .input {font-weight:bold; line-height:24px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}

.layout-member .input_file {width:298px; height:24px; border:1px solid #dbe1e8; background-color:#f8f8f8;}
.layout-member .input_file {line-height:24px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}

.layout-member .checkbox {width:13px; height:13px; position:relative; overflow:hidden; left:0; top:2px;}

.layout-member .textarea_focus {width:478px; height:95px; border:1px solid #dbe1e8; background-color:#f8f8f8; padding:6px 10px;}
.layout-member .textarea_focus {font-weight:bold; line-height:24px; font-size:13px; color:#7e8e9f; font-family:gulim,serif;}
.layout-member .textarea_focus.focus {border:1px solid #59c2e6; background-color:#ffffff; color:#000000;}

.layout-member button {cursor:pointer; height:22px; background-color:#a1b1c2; border:1px solid #a1b1c2; border-radius:3px; padding:0px 6px 1px 6px; height:26px;}
.layout-member button {font-weight:700; line-height:15px; font-size:13px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-member button:hover {border:1px solid #4f5f6f; background-color:#4f5f6f;}

.layout-member .icon_check {cursor:pointer; position:relative; overflow:hidden; left:0; top:0px; vertical-align:top; display:inline-block; width:17px; height:17px; background:url('<?=$web['host_member']?>/img/_onoff.png') no-repeat;}
.layout-member .icon_check.on {background-position:0px -17px;}

.layout-member .multiblock {border:1px solid #bdc3c7; margin-bottom:9px; padding:10px 20px 10px 20px;}
.layout-member .multiblock ul {font-size:0px;}
.layout-member .multiblock ul li {display:inline-block; min-width:180px; height:35px; vertical-align:middle;}
.layout-member .multiblock ul li {font-weight:bold; line-height:35px; font-size:13px; color:#abb1b5; font-family:gulim,serif;}
.layout-member .multiblock ul li:hover {color:#42abd7;}
.layout-member .multiblock ul li.on {color:#000000;}
.layout-member .multiblock ul li input {margin-right:5px; width:13px; height:13px; position:relative; overflow:hidden; left:0; top:2px;}

.layout-member .submit {margin-top:20px;}
.layout-member .submit div {display:inline-block; width:50%;}
.layout-member .submit div span {margin-left:2px; display:block; text-align:center; cursor:pointer; height:59px; border:1px solid #dadada; background-color:#ffffff;}
.layout-member .submit div span {font-weight:700; line-height:59px; font-size:20px; color:#999999; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .submit div.on span {margin-left:0px; margin-right:2px; border:1px solid #1192ca; background-color:#42abd7; color:#ffffff;}
</style>
<script type="text/javascript" src="<?=$web['host_js']?>/util.js"></script>
<script type="text/javascript">
var check_join_hp = <?=text($setup_join['hp'])?>;
var check_join_email = <?=text($setup_join['email'])?>;
var check_join_name = <?=text($setup_join['name'])?>;
var check_join_sex = <?=text($setup_join['sex'])?>;
var check_join_birth = <?=text($setup_join['birth'])?>;
var check_join_tel = <?=text($setup_join['tel'])?>;
var check_join_addr = <?=text($setup_join['addr'])?>;
var check_join_homepage = <?=text($setup_join['homepage'])?>;
var check_join_recommend = <?=text($setup_join['recommend'])?>;
var check_join_profile = <?=text($setup_join['profile'])?>;
var check_join_text1 = <?=text($setup_join['text1'])?>;
var check_join_text2 = <?=text($setup_join['text2'])?>;
var check_join_text3 = <?=text($setup_join['text3'])?>;
var check_join_file1 = <?=text($setup_join['file1'])?>;
var check_join_file1_title = "<?=text($setup_join['file1_title'])?>";
var check_join_file2 = <?=text($setup_join['file2'])?>;
var check_join_file2_title = "<?=text($setup_join['file2_title'])?>";
var check_join_file3 = <?=text($setup_join['file3'])?>;
var check_join_file3_title = "<?=text($setup_join['file3_title'])?>";
var check_join_single1 = <?=text($setup_join['single1'])?>;
var check_join_single1_title = "<?=text($setup_join['single1_title'])?>";
var check_join_single2 = <?=text($setup_join['single2'])?>;
var check_join_single2_title = "<?=text($setup_join['single2_title'])?>";
var check_join_single3 = <?=text($setup_join['single3'])?>;
var check_join_single3_title = "<?=text($setup_join['single3_title'])?>";
var check_join_multi1 = <?=text($setup_join['multi1'])?>;
var check_join_multi1_title = "<?=text($setup_join['multi1_title'])?>";
var check_join_multi2 = <?=text($setup_join['multi2'])?>;
var check_join_multi2_title = "<?=text($setup_join['multi2_title'])?>";
var check_join_multi3 = <?=text($setup_join['multi3'])?>;
var check_join_multi3_title = "<?=text($setup_join['multi3_title'])?>";
var check_join_multi1_count = <?=text($setup_join['multi1_count'])?>;
var check_join_multi2_count = <?=text($setup_join['multi2_count'])?>;
var check_join_multi3_count = <?=text($setup_join['multi3_count'])?>;
</script>
<script type="text/javascript" src="form.js"></script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "회원정보 수정";
include_once("$web[path]/_top.php");
?>
<div class="layout-member">
<?
// 메뉴 호출
include_once("$web[path]/member/_menu.php");
?>
<form method="post" name="formSetup" enctype="multipart/form-data" autocomplete="off" onsubmit="return false;">
<input type="hidden" name="form_check" value="<?=$member['form_check']?>" />
<input type="hidden" name="url" value="<?=text($urlencode)?>" />
<input type="hidden" name="m" value="u" />
<input type="hidden" id="overlap_uid" name="overlap_uid" value="<?=text($member['uid'])?>" />
<input type="hidden" id="overlap_nick" name="overlap_nick" value="<?=text($member['nick'])?>" />
<input type="hidden" id="check_hp_real" value="<?=text($member['hp'])?>" />
<input type="hidden" id="check_email_real" value="<?=text($member['email'])?>" />
<p class="title">회원정보 수정</p>
<p class="msg"><?=text($member['nick'])?>님의 회원 가입정보를 수정합니다.</p>
<!-- box start //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">아이디<span class="ess"></span></td>
    <td class="line"></td>
    <td class="list">
<? if ($member['social'] && !$member['social_uid']) { ?>
<table border="0" cellspacing="0" cellpadding="0" style="margin-bottom:9px;">
<tr>
    <td class="help">아이디는 소셜 회원에 한하여 최초 1회만 변경하실 수 있습니다.</td>
</tr>
</table>
<? } ?>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<? if ($member['social'] && !$member['social_uid']) { ?>
    <td><input type="text" id="uid" name="uid" value="<?=text($member['uid'])?>" class="input_focus" /></td>
    <td width="5"></td>
    <td><button onclick="uidOverlap();">중복확인</button></td>
    <td width="10"></td>
    <td class="help" id="uid_msg">아이디 입력 후, 버튼을 클릭 하세요.</td>
<? } else { ?>
    <td class="text"><input type="hidden" id="uid" name="uid" value="<?=text($member['uid'])?>" /><?=text($member['uid'])?></td>
<? } ?>
</tr>
</table>
    </td>
</tr>
</table>
<!-- box end //-->
<!-- box start //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">비밀번호</td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="password" id="upw" name="upw" value="" class="input_focus" /></td>
</tr>
</table>
    </td>
</tr>
</table>
<!-- box end //-->
<!-- box start //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">비밀번호 확인</td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="password" id="upw_re" name="upw_re" value="" class="input_focus" /></td>
</tr>
</table>
    </td>
</tr>
</table>
<!-- box end //-->
<!-- box start //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">닉네임<span class="ess"></span></td>
    <td class="line"></td>
    <td class="list">
<? if ($setup['nick_change_onoff'] || !$setup['nick_change_onoff'] && $member['social'] && !$member['social_nick']) { ?>
<? if (!$setup['nick_change_onoff'] && $member['social'] && !$member['social_nick']) { ?>
<table border="0" cellspacing="0" cellpadding="0" style="margin-bottom:9px;">
<tr>
    <td class="help">닉네임은 소셜 회원에 한하여 최초 1회만 변경하실 수 있습니다.</td>
</tr>
</table>
<? } ?>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" id="nick" name="nick" value="<?=text($member['nick'])?>" class="input_focus" /></td>
    <td width="5"></td>
    <td><button onclick="nickOverlap();">중복확인</button></td>
    <td width="10"></td>
    <td class="help" id="nick_msg">닉네임 입력 후, 버튼을 클릭 하세요.</td>
</tr>
</table>
<? } else { ?>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="text"><input type="hidden" id="nick" name="nick" value="<?=text($member['nick'])?>" /><?=text($member['nick'])?></td>
</tr>
</table>
<? } ?>
    </td>
</tr>
</table>
<!-- box end //-->
<!-- box start //-->
<? if ($setup['image_onoff']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">프로필 이미지</td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><div class="member_thumb"><?=member_thumb($member['mid'])?></div></td>
    <td width="20"></td>
    <td><button onclick="photoOpen('<?=$member['mid']?>');">IMG 변경</button></td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<? if ($setup_join['hp']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">휴대폰<? if ($setup_join['hp'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" id="hp" name="hp" value="<?=text($member['hp'])?>" class="input" <? if ($setup_join['hp'] == 3) { echo "readonly"; } ?> /></td>
<? if ($setup_join['hp'] == 3) { ?>
    <td width="5"></td>
    <td><button onclick="hpCertify('');">변경</button></td>
<? } ?>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
<tr>
    <td><input type="hidden" id="hp_consent" name="hp_consent" value="<?=$member['hp_consent']?>" class="input_check" /><span class="icon_check"></span></td>
    <td width="9"></td>
    <td class="help">정보 문자 수신 동의</td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<? if ($setup_join['email']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">이메일<? if ($setup_join['email'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" id="email" name="email" value="<?=text($member['email'])?>" class="input" <? if ($setup_join['email'] == 3) { echo "readonly"; } ?> /></td>
<? if ($setup_join['email'] == 3) { ?>
    <td width="5"></td>
    <td><button onclick="emailCertify('');">변경</button></td>
<? } ?>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
<tr>
    <td><input type="hidden" id="email_consent" name="email_consent" value="<?=$member['email_consent']?>" class="input_check" /><span class="icon_check"></span></td>
    <td width="9"></td>
    <td class="help">정보 메일 수신 동의</td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr height="20"><td></td></tr>
</table>
<!-- box start //-->
<? if ($setup_join['name']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">성명<? if ($setup_join['name'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" name="name" value="<?=text($member['name'])?>" class="input_focus" /></td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<? if ($setup_join['sex']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">성별<? if ($setup_join['sex'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<!-- selectbox //-->
<div class="selectbox">
<div class="select">
<div class="block">
<span class="text"></span>
<div class="option"><input type="hidden" name="sex" value="<?=text($member['sex'])?>" class="add" />
<ul class="array">
<li name="0" class="first">선택하세요.</li>
<li name="1">남자</li>
<li name="2">여자</li>
</ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<? if ($setup_join['birth']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">생년월일<? if ($setup_join['birth'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" id="birth" name="birth" value="<?=text_date("Y-m-d", $member['birth'], "")?>" class="input_focus" /></td>
    <td width="5"></td>
    <td><button onclick="calendarOpen('birth');">달력</button></td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<? if ($setup_join['tel']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">일반전화<? if ($setup_join['tel'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" name="tel" value="<?=text($member['tel'])?>" class="input_focus" /></td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<? if ($setup_join['addr']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">주소<? if ($setup_join['addr'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" name="zipcode" value="<?=text($member['zipcode'])?>" class="input_focus" readonly /></td>
    <td width="5"></td>
    <td><button onclick="zipcodeOpen('formSetup', 'zipcode', 'addr1', 'addr2');">우편번호</button></td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
<tr>
    <td><input type="text" name="addr1" value="<?=text($member['addr1'])?>" class="input_focus" style="width:486px;" readonly /></td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
<tr>
    <td><input type="text" name="addr2" value="<?=text($member['addr2'])?>" class="input_focus" style="width:486px;" /></td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<? if ($setup_join['homepage']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">홈페이지<? if ($setup_join['homepage'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" name="homepage" value="<?=text($member['homepage'])?>" class="input_focus" style="width:486px;" /></td>
</tr>
</table>
    </td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<!-- box start //-->
<? if ($setup_join['profile']) { ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj">자기소개<? if ($setup_join['profile'] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list"><textarea name="profile" class="textarea_focus"><?=text($member['profile']);?></textarea></td>
</tr>
</table>
<? } ?>
<!-- box end //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr height="20"><td></td></tr>
</table>
<!-- box start //-->
<?
for ($i=1; $i<=3; $i++) {
if ($setup_join['text'.$i]) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj"><?=text($setup_join['text'.$i.'_title'])?><? if ($setup_join['text'.$i] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="text" name="text<?=$i?>" value="<?=text($member['text'.$i])?>" class="input_focus" style="width:486px;" /></td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
<tr>
    <td class="help"><?=text($setup_join['text'.$i.'_help'])?></td>
</tr>
</table>
    </td>
</tr>
</table>
<?
}
}
?>
<!-- box end //-->
<!-- box start //-->
<?
for ($i=1; $i<=3; $i++) {
if ($setup_join['file'.$i]) {

$upload_mode = "file".$i;
$file = member_file($member['mid'], $upload_mode);
?>
<input type="hidden" id="check_file<?=$i?>" value="<?=text($file['upload_source'])?>" />
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj"><?=text($setup_join['file'.$i.'_title'])?><? if ($setup_join['file'.$i] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><input type="file" name="<?=$upload_mode?>" value="" class="input_file" /></td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
<tr>
    <td class="help"><?=text($setup_join['file'.$i.'_help'])?></td>
</tr>
</table>
<? if ($file['upload_source']) { ?>
<div style="margin-top:6px; border:1px solid #dadde0; background-color:#ffffff; padding:9px;">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<? if (preg_match("/\.(jp[e]?g|gif|png)$/i", $file['upload_file'])) { ?>
    <td><div style="background-color:#dbe1e8; padding:2px; width:48px; height:48px;"><a href="<?=$disk['server_member']?>/member/<?=data_path("u", $file['upload_time'])?>/<?=$file['upload_file']?>" target="_blank"><img src="<?=$disk['server_member']?>/member/<?=data_path("u", $file['upload_time'])?>/<?=$file['upload_file']?>" width="48" height="48" border="0"></a></div></td>
    <td width="20"></td>
<? } ?>
    <td>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><a href="download.php?id=<?=text($file['id'])?>" class="filesource"><?=text($file['upload_source'])?></a></td>
    <td width="5"></td>
    <td class="filesize">(<?=file_size($file['upload_filesize'])?>)</td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
<tr>
    <td><input type="checkbox" name="filedel_<?=$upload_mode?>" value="1" class="checkbox" /></td>
    <td width="5"></td>
    <td class="filedel" onclick="elementCheck('formSetup', 'filedel_<?=$upload_mode?>');">삭제</td>
    <td width="5"></td>
    <td class="help" style="color:#dbe1e8;">|</td>
    <td width="5"></td>
    <td class="help">첨부된 파일을 제거 합니다.</td>
</tr>
</table>
    </td>
</tr>
</table>
</div>
<? } ?>
    </td>
</tr>
</table>
<?
}
}
?>
<!-- box end //-->
<!-- box start //-->
<?
for ($i=1; $i<=3; $i++) {
if ($setup_join['single'.$i]) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj"><?=text($setup_join['single'.$i.'_title'])?><? if ($setup_join['single'.$i] >= 2) { ?><span class="ess"></span><? } ?></td>
    <td class="line"></td>
    <td class="list">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>
<!-- selectbox //-->
<div class="selectbox type2">
<div class="select">
<div class="block">
<span class="text"></span>
<div class="option"><input type="hidden" id="single<?=$i?>" name="single<?=$i?>" value="<?=text($member['single'.$i])?>" class="add" />
<ul class="array">
<li name="" class="first">선택하세요.</li>
<?
$row = explode("|", $setup_join['single'.$i.'_list']);
for ($k=0; $k<count($row); $k++) {

    if ($row[$k]) {
        echo "<li name='".text($row[$k])."'>".text($row[$k])."</li>";
    }

}
?>
</ul>
</div>
</div>
</div>
</div>
<!-- selectbox //-->
    </td>
</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
<tr>
    <td class="help"><?=text($setup_join['single'.$i.'_help'])?></td>
</tr>
</table>
    </td>
</tr>
</table>
<?
}
}
?>
<!-- box end //-->
<!-- box start //-->
<?
for ($i=1; $i<=3; $i++) {
if ($setup_join['multi'.$i]) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr>
    <td class="subj"><?=text($setup_join['multi'.$i.'_title'])?><? if ($setup_join['multi'.$i] >= 2) { ?><span class="ess"></span><? } ?><br />(<?=text($setup_join['multi'.$i.'_count'])?>개 선택)</td>
    <td class="line"></td>
    <td class="list">
<div class="multiblock">
<ul id="multi_list<?=$i?>" rel="<?=text($setup_join['multi'.$i.'_count'])?>">
<?
$row = explode("|", $setup_join['multi'.$i.'_list']);
for ($k=0; $k<count($row); $k++) {

    if ($row[$k]) {

        echo "<li><input type='checkbox' id='multi".$i."_choice".$k."' name='multi".$i."_choice".$k."' value='".text($row[$k])."' ";

        $row2 = explode("|", $member['multi'.$i]);
        for ($kk=0; $kk<count($row2); $kk++) {

            if ($row[$k] == $row2[$kk]) {
                echo "checked";
            }

        }

        echo "/><label for='multi".$i."_choice".$k."'>".text($row[$k])."</label></li>";

    }

}
?>
</ul>
</div>
<table border="0" cellspacing="0" cellpadding="0" style="margin-top:7px;">
<tr>
    <td class="help"><?=text($setup_join['multi'.$i.'_help'])?></td>
</tr>
</table>
    </td>
</tr>
</table>
<?
}
}
?>
<!-- box end //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="box">
<tr><td></td></tr>
</table>
<div class="submit"><div class="on"><span tabindex="1" class="btn_confirm">확인</span></div><div><span tabindex="2" class="btn_cancel">취소</span></div></div>
</form>
</div>
<?
include_once("$web[path]/_bottom.php");
?>