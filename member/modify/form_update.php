<?php
include_once("./_tb.php");
echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

if (!$check_login) { url($web['host_default'])."/"; }

$setup_join = setup_join();

if ($m == 'u') {

    // 폼 체크
    if (!$_POST['form_check'] || $member['form_check'] != $_POST['form_check']) {

        message("<p class='title'>알림</p><p class='text'>요청하신 서비스를 찾을 수 없습니다. 확인하신 후 다시 이용하시기 바랍니다.</p>", "b");

    }

    // 아이디 체크
    if ($uid) { $uid = strtolower(trim($uid)); $uid = preg_match("/^[a-zA-Z0-9_\-]{3,12}+$/", $uid) ? $uid : ""; }

    if (!$uid) {

        message("<p class='title'>알림</p><p class='text'>아이디를 올바르게 입력하세요.</p>", "b");

    }

    if (preg_match("/^([_\-])/", $uid)) {

        message("<p class='title'>알림</p><p class='text'>아이디를 올바르게 입력하세요.</p>", "b");

    }

    if ($member['uid'] != $uid) {

        if (!$member['social'] || $member['social_uid']) {

            message("<p class='title'>알림</p><p class='text'>아이디는 소셜 회원에 한하여 최초 1회만 변경하실 수 있습니다.</p>", "b");

        }

        $check_block = false;
        $row = explode("|", $setup['block_id']);
        for ($i=0; $i<count($row); $i++) {

            if ($row[$i] == $uid) {

                $check_block = true;

            }

        }

        if ($check_block) {

            message("<p class='title'>알림</p><p class='text'>사용하실 수 없는 아이디입니다.</p>", "b");

        }

        $chk = member_uid($uid);

        if ($chk['uid']) {

            message("<p class='title'>알림</p><p class='text'>이미 사용중인 아이디입니다.</p>", "b");

        }

    }

    $upw = trim(strip_tags(sql_real_escape_string($_POST['upw'])));
    if ($upw) {

        // 비밀번호 체크
        if ($upw) { $upw = trim($upw); $upw = preg_match("/^[a-zA-Z0-9`~!@#\$\%^&\*\(\)\-_\=\+\|\\\[\{\]\}\;\:\'\"\<\,\.\>\/\?]{6,16}+$/", $upw) ? $upw : ""; }

        if (!$upw) {

            message("<p class='title'>알림</p><p class='text'>비밀번호 6~16자 대/소문자, 숫자, 기호를 사용하세요.</p>", "b");

        }

        $n = 0;
        for ($i=0; $i<strlen($upw); $i++) {

            if (substr($upw,0,1) == substr($upw,$i,1)) {

                $n++;

            }

        }

        if ($n >= 6) {

            message("<p class='title'>알림</p><p class='text'>비밀번호 연속된 문자를 6자이상 사용할 수 없습니다.</p>", "b");

        }

    }

    // 닉네임 체크
    $nick = trim(strip_tags(sql_real_escape_string($_POST['nick'])));
    if ($nick) { $nick = trim($nick); $nick = preg_match("/^[A-Za-z0-9_가-힣\x20]{1,300}+$/", $nick) ? $nick : ""; }

    if (!$nick) {

        message("<p class='title'>알림</p><p class='text'>사용하실 수 없는 닉네임입니다.</p>", "b");

    }

    if ($member['nick'] != $nick) {

        if (!$setup['nick_change_onoff']) {

            if (!$member['social'] || $member['social_nick']) {

                message("<p class='title'>알림</p><p class='text'>닉네임은 소셜 회원에 한하여 최초 1회만 변경하실 수 있습니다.</p>", "b");

            }

        }

        $check_block = false;
        $row = explode("|", $setup['block_id']);
        for ($i=0; $i<count($row); $i++) {

            if ($row[$i] == $nick) {

                $check_block = true;

            }

        }

        if ($check_block) {

            message("<p class='title'>알림</p><p class='text'>사용하실 수 없는 닉네임입니다.</p>", "b");

        }

        $chk = sql_fetch(" select * from $web[member_table] where nick = '".addslashes($nick)."' limit 0, 1 ");

        if ($chk['nick']) {

            message("<p class='title'>알림</p><p class='text'>이미 사용중인 닉네임입니다.</p>", "b");

        }

    }

    // 휴대폰 체크
    if ($hp) { $hp = str_replace("-", "", trim($hp)); $hp = preg_match("/^[0-9\-]+$/", $hp) ? $hp : ""; }

    // 필수, 인증
    if ($setup_join['hp'] == 2 || $setup_join['hp'] == 3) {

        // 미입력
        if (!$hp) {

            message("<p class='title'>알림</p><p class='text'>휴대폰 번호를 입력하세요.</p>", "b");

        }

    }

    // 이메일 체크
    $email = trim(strip_tags(sql_real_escape_string($_POST['email'])));
    if ($email) { $email = strtolower(trim($email)); }

    // 필수, 인증
    if ($setup_join['email'] == 2 || $setup_join['email'] == 3) {

        // 미입력
        if (!$email) {

            message("<p class='title'>알림</p><p class='text'>이메일 주소를 입력하세요.</p>", "b");

        }

    }

    // 성명
    $name = trim(strip_tags(sql_real_escape_string($_POST['name'])));
    if ($name) { $name = trim($name); $name = preg_match("/^[가-힣\x20]{1,100}+$/", $name) ? $name : ""; }

    if (!$name && $setup_join['name'] == 2) {

        message("<p class='title'>알림</p><p class='text'>성명을 올바르게 입력하세요.</p>", "b");

    }

    // 성별
    if ($sex) { $sex = preg_match("/^[0-9]+$/", $sex) ? $sex : ""; }

    if (!$sex && $setup_join['sex'] == 2) {

        message("<p class='title'>알림</p><p class='text'>성별을 올바르게 입력하세요.</p>", "b");

    }

    // 생년월일
    if ($birth) { $birth = trim($birth); $birth = preg_match("/^[0-9\-]+$/", $birth) ? $birth : ""; }

    if ($setup_join['birth'] == 2) {

        if (!$birth) {

            message("<p class='title'>알림</p><p class='text'>생년월일을 올바르게 입력하세요.</p>", "b");

        }

        $birth = date("Y-m-d", strtotime($birth));

        if ($birth < '1900-01-01' || $birth > $web['time_ymd']) {

            message("<p class='title'>알림</p><p class='text'>생년월일을 올바르게 입력하세요.</p>", "b");

        }

    }

    // 일반전화
    if ($tel) { $tel = str_replace("-", "", trim($tel)); $tel = preg_match("/^[0-9\-]{6,20}+$/", $tel) ? $tel : ""; }

    if (!$tel && $setup_join['tel'] == 2) {

        message("<p class='title'>알림</p><p class='text'>일반전화를 올바르게 입력하세요.</p>", "b");

    }

    // 주소
    $zipcode = trim(strip_tags(sql_real_escape_string($_POST['zipcode'])));
    $addr1 = trim(strip_tags(sql_real_escape_string($_POST['addr1'])));
    $addr2 = trim(strip_tags(sql_real_escape_string($_POST['addr2'])));

    if (!$addr1 && $setup_join['addr'] == 2) {

        message("<p class='title'>알림</p><p class='text'>주소를 올바르게 입력하세요.</p>", "b");

    }

    if (!$addr2 && $setup_join['addr'] == 2) {

        message("<p class='title'>알림</p><p class='text'>주소를 올바르게 입력하세요.</p>", "b");

    }

    // 홈페이지
    $homepage = trim(strip_tags(sql_real_escape_string($_POST['homepage'])));
    if (!$homepage && $setup_join['homepage'] == 2) {

        message("<p class='title'>알림</p><p class='text'>홈페이지를 올바르게 입력하세요.</p>", "b");

    }

    // 자기소개
    $profile = trim(sql_real_escape_string($_POST['profile']));
    if (!$profile && $setup_join['profile'] == 2) {

        message("<p class='title'>알림</p><p class='text'>자기소개를 올바르게 입력하세요.</p>", "b");

    }

    // 텍스트폼1
    $text1 = trim(strip_tags(sql_real_escape_string($_POST['text1'])));
    if (!$text1 && $setup_join['text1'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['text1_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 텍스트폼2
    $text2 = trim(strip_tags(sql_real_escape_string($_POST['text2'])));
    if (!$text2 && $setup_join['text2'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['text2_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 텍스트폼3
    $text3 = trim(strip_tags(sql_real_escape_string($_POST['text3'])));
    if (!$text3 && $setup_join['text3'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['text3_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 단일선택폼1
    $single1 = trim(strip_tags(sql_real_escape_string($_POST['single1'])));
    if (!$single1 && $setup_join['single1'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['single1_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 단일선택폼2
    $single2 = trim(strip_tags(sql_real_escape_string($_POST['single2'])));
    if (!$single2 && $setup_join['single2'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['single2_title'])." 올바르게 입력하세요.</p>", "b");

    }

    // 단일선택폼3
    $single3 = trim(strip_tags(sql_real_escape_string($_POST['single3'])));
    if (!$single3 && $setup_join['single3'] == 2) {

        message("<p class='title'>알림</p><p class='text'>".text($setup_join['single3_title'])." 올바르게 입력하세요.</p>", "b");

    }

    $sql_common = "";
    $sql_common .= " set level = '".$member['level']."' ";

    // 아이디변경, 소셜회원이면서 미변경일 때
    if ($member['uid'] != $uid && $member['social'] && !$member['social_uid']) {

        $sql_common .= ", uid = '".$uid."' ";

        // 소셜회원 변경횟수 증가
        if ($member['social']) {

            $sql_common .= ", social_uid = social_uid + 1 ";

        }

    }

    if ($upw) {

        $sql_common .= ", upw = '".sql_password($upw)."' ";

    }

    $check_nick = false;

    if ($member['nick'] != $nick) {

        // 상시수정가능
        if ($setup['nick_change_onoff']) {

            $check_nick = true;

            $sql_common .= ", nick = '".$nick."' ";

            // 소셜회원 변경횟수 증가
            if ($member['social']) {

                $sql_common .= ", social_nick = social_nick + 1 ";

            }

        } else {

            // 소셜회원 1회 닉 변경 가능
            if ($member['social'] && !$member['social_nick']) {

                $check_nick = true;

                $sql_common .= ", nick = '".$nick."' ";

                // 소셜회원 변경횟수 증가
                if ($member['social']) {

                    $sql_common .= ", social_nick = social_nick + 1 ";

                }

            }

        }

    }

    if ($hp_consent) { $hp_consent = preg_match("/^[0-9]+$/", $hp_consent) ? $hp_consent : ""; }

    if ($setup_join['hp']) {

        $sql_common .= ", hp = '".$hp."' ";
        $sql_common .= ", hp_consent = '".$hp_consent."' ";

    }

    if ($email_consent) { $email_consent = preg_match("/^[0-9]+$/", $email_consent) ? $email_consent : ""; }

    if ($setup_join['email']) {

        $sql_common .= ", email = '".$email."' ";
        $sql_common .= ", email_consent = '".$email_consent."' ";

    }

    if ($setup_join['name']) {

        $sql_common .= ", name = '".$name."' ";

    }

    if ($setup_join['sex']) {

        $sql_common .= ", sex = '".$sex."' ";

    }

    if ($setup_join['birth']) {

        $sql_common .= ", birth = '".$birth."' ";

    }

    if ($setup_join['tel']) {

        $sql_common .= ", tel = '".$tel."' ";

    }

    if ($setup_join['addr']) {

        $sql_common .= ", zipcode = '".$zipcode."' ";
        $sql_common .= ", addr1 = '".$addr1."' ";
        $sql_common .= ", addr2 = '".$addr2."' ";

    }

    if ($setup_join['homepage']) {

        $sql_common .= ", homepage = '".$homepage."' ";

    }

    if ($setup_join['profile']) {

        $sql_common .= ", profile = '".$profile."' ";

    }

    if ($setup_join['text1']) {

        $sql_common .= ", text1 = '".$text1."' ";

    }

    if ($setup_join['text2']) {

        $sql_common .= ", text2 = '".$text2."' ";

    }

    if ($setup_join['text3']) {

        $sql_common .= ", text3 = '".$text3."' ";

    }

    if ($setup_join['single1']) {

        $sql_common .= ", single1 = '".$single1."' ";

    }

    if ($setup_join['single2']) {

        $sql_common .= ", single2 = '".$single2."' ";

    }

    if ($setup_join['single3']) {

        $sql_common .= ", single3 = '".$single3."' ";

    }

    $list = "";
    $row = explode("|", $setup_join['multi1_list']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $_POST['multi1_choice'.$i]) {

            if ($list) {
                $list .= "|";
            }

            $list .= trim(strip_tags(sql_real_escape_string($_POST['multi1_choice'.$i])));

        }

    }

    if ($setup_join['multi1'] && $list) {

        $sql_common .= ", multi1 = '".$list."' ";

    }

    $list = "";
    $row = explode("|", $setup_join['multi2_list']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $_POST['multi2_choice'.$i]) {

            if ($list) {
                $list .= "|";
            }

            $list .= trim(strip_tags(sql_real_escape_string($_POST['multi2_choice'.$i])));

        }

    }

    if ($setup_join['multi2'] && $list) {

        $sql_common .= ", multi2 = '".$list."' ";

    }

    $list = "";
    $row = explode("|", $setup_join['multi3_list']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $_POST['multi3_choice'.$i]) {

            if ($list) {
                $list .= "|";
            }

            $list .= trim(strip_tags(sql_real_escape_string($_POST['multi3_choice'.$i])));

        }

    }

    if ($setup_join['multi3'] && $list) {

        $sql_common .= ", multi3 = '".$list."' ";

    }

    sql_query(" update $web[member_table] $sql_common where mid = '".$member['mid']."' ");

    if ($check_nick) {

        // 전체 게시판
        $result = sql_query(" select * from $web[bbs_table] ");
        for ($i=0; $row=sql_fetch_array($result); $i++) {

            // 일괄 변경
            sql_query(" update {$web['article_table']}{$row['bbs_id']} set nick = '".$nick."' where mid = '".$member['mid']."' ");
            sql_query(" update {$web['reply_table']}{$row['bbs_id']} set nick = '".$nick."' where mid = '".$member['mid']."' ");

        }

        sql_query(" update $web[search_table] set nick = '".$nick."' where mid = '".$member['mid']."' ");
        sql_query(" update $web[notice_table] set nick = '".$nick."' where mid = '".$member['mid']."' ");

    }

    $mid = $member['mid'];

    // 첨부파일폼1
    if ($setup_join['file1']) {

        $upload_mode = "file1";
        include("./form_update.file.php");

    }

    // 첨부파일폼2
    if ($setup_join['file2']) {

        $upload_mode = "file2";
        include("./form_update.file.php");

    }

    // 첨부파일폼3
    if ($setup_join['file3']) {

        $upload_mode = "file3";
        include("./form_update.file.php");

    }

    message("<p class='title'>알림</p><p class='text'>회원정보를 수정 하였습니다.</p>", "", $web['host_member']."/", true, true);

}
?>