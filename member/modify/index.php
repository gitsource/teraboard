<?php // 정보수정
include_once("./_tb.php");
if (!$check_login) { url($web['host_member']."/login/?url={$urlencode}"); }

if ($member['social']) {

    unset($_SESSION['member_modify']);

    set_session("member_modify", $member['mid']);

    url("form.php");

}

if ($m == 'check') {

    echo "<meta http-equiv='content-type' content='text/html; charset=$web[charset]'>";

    $upw = trim($_POST['upw']);

    if (!$upw) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "비밀번호가 입력되지 않았습니다.";
        exit;

    }

    if (sql_password($upw) != $member['upw']) {

        echo "<script type='text/javascript'>";
        echo "$('#msg').show();";
        echo "</script>";

        echo "입력하신 비밀번호가 올바르지 않습니다.";
        exit;

    }

    unset($_SESSION['member_modify']);

    set_session("member_modify", $member['mid']);

    url("form.php");

}

// head start
ob_start();
?>
<link rel="stylesheet" href="<?=$web['host_member']?>/_menu.css" type="text/css" />
<style type="text/css">
body {min-width:360px;}

.layout-member {padding:50px 0;}
.layout-member .item {width:100%; margin-top:20px;}
.layout-member .item .block {padding:20px; position:relative; background-color:#ffffff; border:1px solid #dadada;}
.layout-member .item p {margin:0px;}
.layout-member .item .title {font-weight:700; line-height:20px; font-size:20px; color:#363636; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .item .text {margin-top:11px; line-height:14px; font-size:12px; color:#b0b0b0; font-family:gulim,serif;}

.layout-member .uid {margin-top:18px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_id.png') no-repeat left 0; background-color:#ffffff;}
.layout-member .uid input {margin-left:40px; width:calc(100% - 40px);  height:49px; border:0px; background:transparent;}
.layout-member .uid input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-member .uid.on {border:1px solid #1192ca; background-position:0 -49px;}
.layout-member .uid.on input {color:#000000;}
.layout-member .uid span {position:absolute; left:40px; top:0px; display:block; width:80px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}

.layout-member .upw {margin-top:10px; position:relative; height:49px; border:1px solid #dadada; background:url('<?=$web['host_member']?>/img/_icon_pw.png') no-repeat left 0; background-color:#ffffff;}
.layout-member .upw input {margin-left:40px; width:calc(100% - 40px);  height:49px; border:0px; background:transparent;}
.layout-member .upw input {font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}
.layout-member .upw.on {border:1px solid #1192ca; background-position:0 -49px;}
.layout-member .upw.on input {color:#000000;}
.layout-member .upw span {position:absolute; left:40px; top:0px; display:block; width:80px; font-weight:bold; line-height:49px; font-size:14px; color:#c2c2c2; font-family:gulim,serif;}

.layout-member .msg {display:none; text-align:center; padding-top:30px;}
.layout-member .msg {font-weight:bold; line-height:18px; font-size:13px; color:#f26d7d; font-family:gulim,serif;}

.layout-member .submit {cursor:pointer; margin-top:30px; height:59px; border:1px solid #1192ca; background-color:#42abd7;}
.layout-member .submit {font-weight:700; line-height:59px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-member .submit span {display:block; text-align:center;}
</style>
<script type="text/javascript">
$(document).ready( function() {

    // 내정보
    $('.member-menu div[name=info]').addClass('on');

    $('#upw').focus(function() {

        $(this).parent().addClass('on');
        $('#msg').hide();

    }).blur(function() {

        $(this).parent().removeClass('on');
        $('#msg').hide();

        if ($(this).val() == '') {

            $(this).parent().find('span').show();

        }

    }).keydown(function(e) {

        $(this).parent().find('span').hide();

        if (e.keyCode == 13) {

            memberCheck();

        }

    });

    $('.upw span').click(function() {

        $(this).parent().find('input').focus();

    });

    $('.layout-member .submit span').click(function() {

        memberCheck();

    });

});

function memberCheck()
{

    $.post("index.php", {"form_check" : "<?=$member['form_check']?>", "m" : "check", "upw" : $('#upw').val()}, function(data) {

        $("#msg").html(data);

    });

}
</script>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = "비밀번호 재확인";
include_once("$web[path]/_top.php");
?>
<div class="layout-member">
<?
// 메뉴 호출
include_once("$web[path]/member/_menu.php");
?>
<div class="item">
<div class="block">
<p class="title">비밀번호 재확인</p>
<p class="text">소중한 개인정보 보호를 위해 비밀번호를 체크합니다.</p>
<div class="uid"><input type="text" id="uid" name="uid" value="<?=text($member['uid'])?>" readonly /></div>
<div class="upw"><input type="password" id="upw" name="upw" value="" tabindex="1" /><span>비밀번호</span></div>
<div class="msg" id="msg"></div>
<div class="submit"><span tabindex="2">확인</span></div>
</div>
</div>
</div>
<?
include_once("$web[path]/_bottom.php");
?>