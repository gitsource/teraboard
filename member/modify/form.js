var zindex = 1;
$(document).ready( function() {

    // 내정보
    $('.member-menu div[name=info]').addClass('on');

    $('.input_focus').focus(function() {

        $(this).addClass('focus');

    }).blur(function() {

        $(this).removeClass('focus');

    });

    $('.textarea_focus').focus(function() {

        $(this).addClass('focus');

    }).blur(function() {

        $(this).removeClass('focus');

    });

    $('.layout-member .input_check').each(function() {

        if ($(this).val() == '1') {

            $(this).parent().find('.icon_check').addClass('on');

        }

    });

    $('.layout-member .icon_check').click(function() {

        var obj = $(this).parent().find('.input_check');

        if (obj.val() == 1) {

            $(this).removeClass('on');
            obj.val('0');

        } else {

            $(this).addClass('on');
            obj.val('1');

        }

    });


    $('.select .block span').click(function() {

        if ($(this).parent().children('div.option').is(':hidden') == true) {

            zindex++;

            $(this).parent().parent().parent('div.selectbox').addClass('opt').zIndex(zindex);

        } else {

            $(this).parent().parent().parent('div.selectbox').removeClass('opt');

        }

    });

    $('.select .block .option ul.array li').click(function() {

        $(this).parent().parent().children('input.add').val($(this).attr('name'));
        $(this).parent().parent().parent().children('span.text').text($(this).text());
        $(this).parent().parent().parent().parent().parent('div.selectbox').removeClass('opt');
        $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
        $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg[name="'+$(this).attr('name')+'"]').show();

    });

    $('.selectbox .add').each(function() {

        var addtext = $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').text();

        if (addtext) {

            $(this).parent().parent().children('span.text').text(addtext);
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[name="'+$(this).val()+'"]').show();

        } else {

            $(this).parent().parent().children('span.text').html('&nbsp;');

        }

    });

    $('.layout-member .multiblock ul li, .layout-member .multiblock ul li input').click(function() {

        if ($(this).parent().find('input:checked').length >= $(this).parent().attr('rel')) {

            $(this).parent().parent().parent().find('span.msg').removeClass('on');

        }

        if ($(this).parent().find('input').is(':checked') == true) {

            $(this).parent().addClass('on');

        } else {

            $(this).parent().removeClass('on');

        }

    });

    $('.btn_confirm').click(function() {

        submitSetup();

    });

    $('.btn_cancel').click(function() {

        history.go(-1);

    });

    $(document).click(function(e) {

        if (!$(e.target).is('.selectbox *')) {

            $('.selectbox').removeClass('opt');

        }

    });

});

function uidOverlap()
{

    var f = document.formSetup;

    if (f.uid.value == ''){

        alert("회원 회원 ID를 입력하세요.");
        f.uid.focus();
        return false;

    } else {

        retVal = isValid_uid(f.uid.value);

        if (!retVal) {

	    	    f.uid.focus();
            return false;

        }

    }

    $.post('form.php', { 'm' : 'overlap_uid', 'uid' : f.uid.value }, function(data) {

        $('#uid_msg').html(data);

    });

}

function nickOverlap()
{

    var f = document.formSetup;

    if (f.nick.value == ''){

        alert("회원 닉네임을 입력하세요.");
        f.nick.focus();
        return false;

    } else {

        retVal = isValid_nick(f.nick.value);

        if (!retVal) {

	    	    f.nick.focus();
            return false;

        }

    }

    $.post('form.php', { 'm' : 'overlap_nick', 'nick' : f.nick.value }, function(data) {

        $('#nick_msg').html(data);

    });

}

function submitSetup()
{

    var f = document.formSetup;

    if (f.uid.value == '') {

        alert("아이디를 입력하세요.");
        f.uid.focus();
        return false;

    }

    if (f.overlap_uid.value == '' || f.overlap_uid.value != f.uid.value) {

        alert("아이디를 중복확인 하세요.");
        f.uid.focus();
        return false;

    }

    if (!isValid_uid(f.uid.value)) {

	       f.uid.focus();
    	   return false;

    }

    if (f.upw.value != '') {

        res = isValid_passwd(f.upw.value);

        if (!res) {

            f.upw.focus();
            return false;

        }

        if (f.upw.value != f.upw_re.value) {

            alert("비밀번호가 일치하지 않습니다.");
            f.upw.focus();
            return false;

         }

    }

    if (f.overlap_nick.value == '' || f.overlap_nick.value != f.nick.value) {

        alert("닉네임을 중복확인 하세요.");
        f.nick.focus();
        return false;

    }

    if (!isValid_nick(f.nick.value)) {

	       f.nick.focus();
    	   return false;

    }

    if (check_join_hp) {

        if (check_join_hp == 2 && f.hp.value == '') {

            alert("휴대폰을 입력하세요.");
            f.hp.focus();
            return false;

        }

        else if (check_join_hp == 3 && (f.check_hp_real.value == '' || f.check_hp_real.value != f.hp.value)) {

            alert("변경 버튼을 눌러 휴대폰 인증을 완료하세요.");
            f.hp.focus();
            return false;

        }

        if (f.hp.value != '') {

            var chk = /^[0-9\-]{6,20}$/;
            if (!chk.test(f.hp.value)) {

                alert("휴대폰 번호를 올바르게 입력하세요.");
                f.hp.focus();
                return false;

            }

        }

    }

    if (check_join_email) {

        if (check_join_email == 2 && f.email.value == '') {

            alert("이메일을 입력하세요.");
            f.email.focus();
            return false;

        }

        else if (check_join_email == 3 && (f.check_email_real.value == '' || f.check_email_real.value != f.email.value)) {

            alert("변경 버튼을 눌러 이메일 인증을 완료하세요.");
            f.email.focus();
            return false;

        }

        if (f.email.value != '') {

            if (!isValid_email(f.email.value)) {

        	       f.email.focus();
            	   return false;

            }

        }

    }

    if (check_join_name) {

        if (check_join_name == 2 && f.name.value == '') {

            alert("성명을 입력하세요.");
            f.name.focus();
            return false;

        }

        if (f.name.value != '') {

            if (!isValid_name(f.name.value)) {

                f.name.focus();
                return false;

            }

        }

    }

    if (check_join_sex) {

        if (check_join_sex == 2 && f.sex.value == '') {

            alert("성별을 선택하세요.");
            return false;

        }

    }

    if (check_join_birth) {

        if (check_join_birth == 2 && f.birth.value == '') {

            alert("생년월일을 입력하세요.");
            return false;

        }

        if (f.birth.value != '') {

            var chk = /^[0-9\-]{10,10}$/;
            if (!chk.test(f.birth.value)) {

                alert("생년월일을 올바르게 입력하세요.");
                return false;

            }

            if (!isValid_Date(f.birth.value)) {

                f.birth.focus();
                return false;

            }

        }

    }

    if (check_join_tel) {

        if (check_join_tel == 2 && f.tel.value == '') {

            alert("일반전화를 입력하세요.");
            f.tel.focus();
            return false;

        }

        if (f.tel.value != '') {

            var chk = /^[0-9\-]{6,20}$/;
            if (!chk.test(f.tel.value)) {

                alert("일반전화를 올바르게 입력하세요.");
                f.tel.focus();
                return false;

            }

        }

    }

    if (check_join_addr) {

        if (check_join_addr == 2 && (f.addr1.value == '' || f.addr2.value == '')) {

            alert("주소를 입력하세요.");
            f.addr1.focus();
            return false;

        }

    }

    if (check_join_homepage) {

        if (check_join_homepage == 2 && f.homepage.value == '') {

            alert("홈페이지를 입력하세요.");
            f.homepage.focus();
            return false;

        }

    }

    if (check_join_profile) {

        if (check_join_profile == 2 && f.profile.value == '') {

            alert("자기소개를 입력하세요.");
            f.profile.focus();
            return false;

        }

    }

    if (check_join_text1) {

        if (check_join_text1 == 2 && f.text1.value == '') {

            alert("필수입력 항목입니다.");
            f.text1.focus();
            return false;

        }

    }

    if (check_join_text2) {

        if (check_join_text2 == 2 && f.text2.value == '') {

            alert("필수입력 항목입니다.");
            f.text2.focus();
            return false;

        }

    }

    if (check_join_text3) {

        if (check_join_text3 == 2 && f.text3.value == '') {

            alert("필수입력 항목입니다.");
            f.text3.focus();
            return false;

        }

    }

    if (check_join_file1) {

        if (check_join_file1 == 2 && f.file1.value == '' && $('#check_file1').val() == '') {

            alert("파일을 첨부하세요. ("+check_join_file1_title+")");
            f.file1.focus();
            return false;

        }

    }

    if (check_join_file2) {

        if (check_join_file2 == 2 && f.file2.value == '' && $('#check_file2').val() == '') {

            alert("파일을 첨부하세요. ("+check_join_file2_title+")");
            f.file2.focus();
            return false;

        }

    }

    if (check_join_file3) {

        if (check_join_file3 == 2 && f.file3.value == '' && $('#check_file3').val() == '') {

            alert("파일을 첨부하세요. ("+check_join_file3_title+")");
            f.file3.focus();
            return false;

        }

    }

    if (check_join_single1) {

        if (check_join_single1 == 2 && f.single1.value == '') {

            alert("필수선택 항목입니다. ("+check_join_single1_title+")");
            return false;

        }

    }

    if (check_join_single2) {

        if (check_join_single2 == 2 && f.single2.value == '') {

            alert("필수선택 항목입니다. ("+check_join_single2_title+")");
            return false;

        }

    }

    if (check_join_single3) {

        if (check_join_single3 == 2 && f.single3.value == '') {

            alert("필수선택 항목입니다. ("+check_join_single3_title+")");
            return false;

        }

    }

    if (check_join_multi1) {

        if (check_join_multi1 == 2) {

            var n = $('#multi_list1 input:checked').length;

            if (n < check_join_multi1_count || n > check_join_multi1_count) {

                alert(""+check_join_multi1_count+"개 선택하세요. ("+check_join_multi1_title+")");
                return false;

            }

        }

    }

    if (check_join_multi2) {

        if (check_join_multi2 == 2) {

            var n = $('#multi_list2 input:checked').length;

            if (n < check_join_multi2_count || n > check_join_multi2_count) {

                alert(""+check_join_multi2_count+"개 선택하세요. ("+check_join_multi2_title+")");
                return false;

            }

        }

    }

    if (check_join_multi3) {

        if (check_join_multi3 == 2) {

            var n = $('#multi_list3 input:checked').length;

            if (n < check_join_multi3_count || n > check_join_multi3_count) {

                alert(""+check_join_multi3_count+"개 선택하세요. ("+check_join_multi3_title+")");
                return false;

            }

        }

    }

    f.action = "form_update.php";
    f.submit();

}