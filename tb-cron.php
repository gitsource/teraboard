<?php
@set_time_limit(0);
include_once("./_tb.php");

if (!$m || $setup['cronkey'] != $m || $setup['crondate'] == $web['time_ymd']) {
    exit;
}

/*------------------------------
    ## 휴면 아이디 이메일 ##
------------------------------*/

$setup_email_dormancy = setup_email("dormancy");

// 휴면 회원 탈퇴 기간, 이메일 사용
if ($setup['del_dormancy_day'] && $setup['email_onoff'] && $setup_email_dormancy['onoff']) {

    // 휴면아이디 대상 (30일전 이메일 발송)
    $list = array();
    $result = sql_query(" select * from $web[member_table] where login_datetime <= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * ($setup['del_dormancy_day'] - 30)))."' and dormancy = 0 and dropout = 0 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $list[$i] = $row;

    }

    // 이메일
    if ($setup['email_onoff'] && $setup_email_dormancy['onoff']) {

        if (!$setup['title']) {

            $setup['title'] = $web['host_default'];

        }

        for ($i=0; $i<count($list); $i++) {

            // 이메일이 있는 회원
            if ($list[$i]['email']) {

                // 미로그인
                if ($list[$i]['login_datetime'] == '0000-00-00 00:00:00') {

                    // 가입일
                    $dormancy = date("Y-m-d H:i:s", strtotime($list[$i]['datetime']) + (86400 * $setup['del_dormancy_day']));

                } else {

                    // 로그인
                    $dormancy = date("Y-m-d H:i:s", strtotime($list[$i]['login_datetime']) + (86400 * $setup['del_dormancy_day']));

                }

                // 가입하고 한번도 미로그인한 경우가 있기에 휴면일로부터 30일 남았다면 이메일 발송
                if ($dormancy <= date("Y-m-d H:i:s", $web['server_time'] + (86400 * 30))) {

                    $title = $setup_email_dormancy['title'];
                    $title = str_replace("{닉네임}", $list[$i]['nick'], $title);
                    $title = str_replace("{회원ID}", $list[$i]['uid'], $title);
                    $title = str_replace("{성명}", $list[$i]['name'], $title);
                    $title = str_replace("{가입일}", $list[$i]['datetime'], $title);
                    $title = str_replace("{휴면일}", $dormancy, $title);
                    $title = str_replace("{홈페이지명}", $setup['title'], $title);
                    $title = str_replace("{도메인주소}", $web['host_default'], $title);
                    $title = str_replace("{회사명}", $setup['company'], $title);
                    $title = str_replace("{대표자명}", $setup['ceo'], $title);
                    $title = str_replace("{대표메일}", $setup['email'], $title);
                    $title = str_replace("{대표번호}", $setup['tel'], $title);
                    $title = str_replace("{회사주소}", $setup['addr'], $title);

                    $content = $setup_email_dormancy['content'];
                    $content = str_replace("{닉네임}", $list[$i]['nick'], $content);
                    $content = str_replace("{회원ID}", $list[$i]['uid'], $content);
                    $content = str_replace("{성명}", $list[$i]['name'], $content);
                    $content = str_replace("{가입일}", $list[$i]['datetime'], $content);
                    $content = str_replace("{휴면일}", $dormancy, $content);
                    $content = str_replace("{홈페이지명}", $setup['title'], $content);
                    $content = str_replace("{도메인주소}", $web['host_default'], $content);
                    $content = str_replace("{회사명}", $setup['company'], $content);
                    $content = str_replace("{대표자명}", $setup['ceo'], $content);
                    $content = str_replace("{대표메일}", $setup['email'], $content);
                    $content = str_replace("{대표번호}", $setup['tel'], $content);
                    $content = str_replace("{회사주소}", $setup['addr'], $content);

                    $sql_common = "";
                    $sql_common .= " set mid = '".$list[$i]['mid']."' ";
                    $sql_common .= ", name = '".addslashes($setup['title'])."' ";
                    $sql_common .= ", send_email = '".addslashes($setup['email'])."' ";
                    $sql_common .= ", email = '".addslashes($list[$i]['email'])."' ";
                    $sql_common .= ", title = '".$title."' ";
                    $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
                    $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

                    sql_query(" insert into $web[email_table] $sql_common ");

                    email_send($list[$i]['email'], $title, text2(trim($content), 1), $setup['title'], $setup['email'], 1);

                    $sql_common = "";
                    $sql_common .= " set dormancy = '1' ";
                    $sql_common .= ", dormancy_datetime = '".$web['time_ymdhis']."' ";

                    sql_query(" update $web[member_table] $sql_common where mid = '".$list[$i]['mid']."' ");

                }

            }

        }

    }

}

/*------------------------------
    ## 데이터 관리 (휴면 회원 탈퇴) ##
------------------------------*/

if ($setup['del_dormancy_day']) {

    $result = sql_query(" select * from $web[member_table] where login_datetime < '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * $setup['del_dormancy_day']))."' and dropout = 0 ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        if ($row['login_datetime'] == '0000-00-00 00:00:00') {

            $dormancy = date("Y-m-d H:i:s", strtotime($row['datetime']) + (86400 * $setup['del_dormancy_day']));

        } else {

            $dormancy = date("Y-m-d H:i:s", strtotime($row['login_datetime']) + (86400 * $setup['del_dormancy_day']));

        }

        // 1년이 지났다면
        if ($dormancy < $web['time_ymdhis']) {

            member_dropout($row['mid']);

        }

    }

}

/*------------------------------
    ## 데이터 관리 (탈퇴 회원 삭제) ##
------------------------------*/

// 자동삭제
if ($setup['del_dropout_onoff']) {

    // 지정기간 후 삭제
    if ($setup['del_dropout_day']) {

        // index 안 잡았기 때문에 if로 제어
        $result = sql_query(" select * from $web[member_table] where dropout = 1 ");
        for ($i=0; $row=sql_fetch_array($result); $i++) {

            // 탈퇴일시 + 저장기간
            $dropout_datetime = date("Y-m-d H:i:s", strtotime($row['dropout_datetime']) + (86400 * $setup['del_dropout_day']));

            // 현재일시보다 작다면 삭제처리
            if ($dropout_datetime <= $web['time_ymdhis']) {

                member_delete($row['mid']);

            }

        }

    } else {
    // 즉시삭제

        $result = sql_query(" select * from $web[member_table] where dropout = 1 ");
        for ($i=0; $row=sql_fetch_array($result); $i++) {

            member_delete($row['mid']);

        }

    }

}

/*------------------------------
    ## 데이터 관리 (방문자 접근 기록) ##
------------------------------*/

// 자동삭제
if ($setup['del_visit_day']) {

    sql_query(" delete from $web[visit_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_visit_day'] * 86400))."' ");
    sql_query(" delete from $web[visit_page_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_visit_day'] * 86400))."' ");

}

/*------------------------------
    ## 데이터 관리 (회원 로그인 기록) ##
------------------------------*/

// 자동삭제
if ($setup['del_login_day']) {

    // 로그인 기록
    sql_query(" delete from $web[member_login_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_login_day'] * 86400))."' ");

}

/*------------------------------
    ## 데이터 관리 (인증 기록) ##
------------------------------*/

// 자동삭제
if ($setup['del_real_day']) {

    // 본인확인 기록
    sql_query(" delete from $web[real_name_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_real_day'] * 86400))."' ");

    // 휴대폰 기록
    sql_query(" delete from $web[real_hp_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_real_day'] * 86400))."' ");

    // 이메일 기록
    sql_query(" delete from $web[real_email_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_real_day'] * 86400))."' ");

}

/*------------------------------
    ## 데이터 관리 (메세지) ##
------------------------------*/

// 자동삭제
if ($setup['del_message_day']) {

    // 보관하지 않은 받은 쪽지
    sql_query(" delete from $web[message_receive_table] where type in (0,1) and datetime < '".date("Y-m-d H:i:s", strtotime($web['time_ymdhis']) - ($setup['del_message_day'] * 86400))."' ");

    // 보낸쪽지
    sql_query(" delete from $web[message_send_table] where datetime < '".date("Y-m-d H:i:s", strtotime($web['time_ymdhis']) - ($setup['del_message_day'] * 86400))."' ");

}

/*------------------------------
    ## 데이터 관리 (이메일 발송 내역) ##
------------------------------*/

// 자동삭제
if ($setup['del_email_day']) {

    // 이메일 발송 내역
    sql_query(" delete from $web[email_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_email_day'] * 86400))."' ");

}

/*------------------------------
    ## 데이터 관리 (문자 발송 내역) ##
------------------------------*/

// 자동삭제
if ($setup['del_sms_day']) {

    // 이메일 발송 내역
    sql_query(" delete from $web[sms_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_sms_day'] * 86400))."' ");

}

/*------------------------------
    ## 데이터 관리 (포인트 지급내역) ##
------------------------------*/

// 자동삭제
if ($setup['del_point_day']) {

    // 포인트 지급내역
    sql_query(" delete from $web[member_point_table] where datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['del_point_day'] * 86400))."' ");

}

/*------------------------------
    ## 포인트 초기화 ##
------------------------------*/

// 초기화
if ($setup['point_reset']) {

    // 로그인 이전
    $list = array();
    $result = sql_query(" select mid, point from $web[member_table] where login_datetime < '".date("Y-m-d H:i:s", $web['server_time'] - ($setup['point_reset'] * 86400))."' ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $list[$i] = $row;

    }

    for ($i=0; $i<count($list); $i++) {

        member_point($list[$i]['mid'], ($list[$i]['point'] * -1), 5, "포인트 초기화 (".$setup['point_reset']."일 비로그인)", $web['server_time'].rand(10000,99999));

    }

}

/*------------------------------
    ## 메세지, 문자, 이메일 (생일 축하) ##
------------------------------*/

$setup_email_birth = setup_email("birth");

// 메세지, 문자, 이메일
if ($setup['message_onoff'] && $setup['message_birth_onoff'] || $setup['sms_onoff'] && $setup['sms_birth_onoff'] || $setup['email_onoff'] && $setup_email_birth['onoff']) {

    // 오늘 생일인 회원
    $list = array();
    $result = sql_query(" select * from $web[member_table] where dropout = 0 and substring(birth,6,5) = '".date("m-d", $web['server_time'])."' ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $list[$i] = $row;

    }

    // 메세지
    if ($setup['message_onoff'] && $setup['message_birth_onoff']) {

        for ($i=0; $i<count($list); $i++) {

            $content = $setup['message_birth'];
            $content = str_replace("{닉네임}", $list[$i]['nick'], $content);
            $content = str_replace("{회원ID}", $list[$i]['uid'], $content);
            $content = str_replace("{홈페이지명}", $setup['title'], $content);

            $sql_common = "";
            $sql_common .= " set mid = '".$list[$i]['mid']."' ";
            $sql_common .= ", fid = '1' ";
            $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
            $sql_common .= ", type = '0' ";
            $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

            sql_query(" insert into $web[message_receive_table] $sql_common ");

            $code = sql_insert_id();

            $sql_common = "";
            $sql_common .= " set mid = '1' ";
            $sql_common .= ", tid = '".$list[$i]['mid']."' ";
            $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
            $sql_common .= ", code = '".$code."' ";
            $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

            sql_query(" insert into $web[message_send_table] $sql_common ");

        }

    }

    // 문자
    if ($setup['sms_onoff'] && $setup['sms_birth_onoff']) {

        for ($i=0; $i<count($list); $i++) {

            // 휴대폰이 있으면서 수신동의한 회원
            if ($list[$i]['hp'] && $list[$i]['hp_consent']) {

                $content = $setup['sms_birth'];
                $content = str_replace("{닉네임}", $list[$i]['nick'], $content);
                $content = str_replace("{회원ID}", $list[$i]['uid'], $content);
                $content = str_replace("{홈페이지명}", $setup['title'], $content);

                sms_send($list[$i]['hp'], $setup['callernumber'], $content);

            }

        }

    }

    // 이메일
    if ($setup['email_onoff'] && $setup_email_birth['onoff']) {

        if (!$setup['title']) {

            $setup['title'] = $web['host_default'];

        }

        for ($i=0; $i<count($list); $i++) {

            // 이메일이 있으면서 수신동의한 회원
            if ($list[$i]['email'] && $list[$i]['email_consent']) {

                $title = $setup_email_birth['title'];
                $title = str_replace("{닉네임}", $list[$i]['nick'], $title);
                $title = str_replace("{회원ID}", $list[$i]['uid'], $title);
                $title = str_replace("{성명}", $list[$i]['name'], $title);
                $title = str_replace("{가입일}", $list[$i]['datetime'], $title);
                $title = str_replace("{생년월일}", $list[$i]['birth'], $title);
                $title = str_replace("{홈페이지명}", $setup['title'], $title);
                $title = str_replace("{도메인주소}", $web['host_default'], $title);
                $title = str_replace("{회사명}", $setup['company'], $title);
                $title = str_replace("{대표자명}", $setup['ceo'], $title);
                $title = str_replace("{대표메일}", $setup['email'], $title);
                $title = str_replace("{대표번호}", $setup['tel'], $title);
                $title = str_replace("{회사주소}", $setup['addr'], $title);

                $content = $setup_email_birth['content'];
                $content = str_replace("{닉네임}", $list[$i]['nick'], $content);
                $content = str_replace("{회원ID}", $list[$i]['uid'], $content);
                $content = str_replace("{성명}", $list[$i]['name'], $content);
                $content = str_replace("{가입일}", $list[$i]['datetime'], $content);
                $content = str_replace("{생년월일}", $list[$i]['birth'], $content);
                $content = str_replace("{홈페이지명}", $setup['title'], $content);
                $content = str_replace("{도메인주소}", $web['host_default'], $content);
                $content = str_replace("{회사명}", $setup['company'], $content);
                $content = str_replace("{대표자명}", $setup['ceo'], $content);
                $content = str_replace("{대표메일}", $setup['email'], $content);
                $content = str_replace("{대표번호}", $setup['tel'], $content);
                $content = str_replace("{회사주소}", $setup['addr'], $content);

                $sql_common = "";
                $sql_common .= " set mid = '".$list[$i]['mid']."' ";
                $sql_common .= ", name = '".addslashes($setup['title'])."' ";
                $sql_common .= ", send_email = '".addslashes($setup['email'])."' ";
                $sql_common .= ", email = '".addslashes($list[$i]['email'])."' ";
                $sql_common .= ", title = '".$title."' ";
                $sql_common .= ", content = '".trim(sql_real_escape_string($content))."' ";
                $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

                sql_query(" insert into $web[email_table] $sql_common ");

                email_send($list[$i]['email'], $title, text2(trim($content), 1), $setup['title'], $setup['email'], 1);

            }

        }

    }

}

/*------------------------------
    ## 성인인증 ##
------------------------------*/

// 본인확인하였으면서 성인이 아니라면
$result = sql_query(" select mid, birth from $web[member_table] where certify_name = 1 and certify_adult = 0 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    if ($row['birth'] != '0000-00-00') {

        $age = (date("Y") - date("Y", strtotime($row['birth']))) + 1;

        // 20살은 성인
        if ($age >= 20) {

            // 성인갱신
            sql_query(" update $web[member_table] set certify_adult = '1' where mid = '".$row['mid']."' ");

        }

    }

}

$sql_common = "";
$sql_common .= " set crondate = '".$web['time_ymd']."' ";

sql_query(" update $web[setup_table] $sql_common ");

echo "ok<br />";

exit;
?>