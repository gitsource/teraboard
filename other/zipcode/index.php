<?php // 우편번호
include_once("./_tb.php");
if ($fname) { $fname = preg_match("/^[A-Za-z0-9_\-]+$/", $fname) ? $fname : ""; }
if ($fzipcode) { $fzipcode = preg_match("/^[A-Za-z0-9_\-]+$/", $fzipcode) ? $fzipcode : ""; }
if ($faddr1) { $faddr1 = preg_match("/^[A-Za-z0-9_\-]+$/", $faddr1) ? $faddr1 : ""; }
if ($faddr2) { $faddr2 = preg_match("/^[A-Za-z0-9_\-]+$/", $faddr2) ? $faddr2 : ""; }
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0" />
<?
include_once("$web[path]/_head.php");
?>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div id="wrap" style="display:none;width:100%;height:100%;position:relative;"></div>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script type="text/javascript">
// 우편번호 찾기 찾기 화면을 넣을 element
var element_wrap = document.getElementById('wrap');

function foldDaumPostcode() {

    // iframe을 넣은 element를 안보이게 한다.
    element_wrap.style.display = 'none';

}

function sample3_execDaumPostcode() {
    
    var obj = opener.document.<?=text($fname)?>;
    
    // 현재 scroll 위치를 저장해놓는다.
    var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    new daum.Postcode({
        oncomplete: function(data) {
            // 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var fullAddr = data.address; // 최종 주소 변수
            var extraAddr = ''; // 조합형 주소 변수

            // 기본 주소가 도로명 타입일때 조합한다.
            if(data.addressType === 'R'){
                //법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
                fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            obj.<?=text($fzipcode)?>.value = data.zonecode; //5자리 새우편번호 사용
            obj.<?=text($faddr1)?>.value = fullAddr;

            // iframe을 넣은 element를 안보이게 한다.
            // (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
            element_wrap.style.display = 'none';

            // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
            document.body.scrollTop = currentScroll;
                
            if (obj.<?=text($faddr2)?>) {
                obj.<?=text($faddr2)?>.focus();
            }

            if (obj.address) {
        
                obj.address.value = "("+data.zonecode+") "+fullAddr;
        
                opener.document.getElementById("address_name").style.display = "none";
        
                $(opener.document).find('#address').focus();
        
            }
        
            window.close();

        },
        // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
        onresize : function(size) {
            element_wrap.style.height = size.height+'px';
        },
        width : '100%',
        height : '100%'
    }).embed(element_wrap);

    // iframe을 넣은 element를 보이게 한다.
    element_wrap.style.display = 'block';

}

sample3_execDaumPostcode();
</script>
</body>
</html>