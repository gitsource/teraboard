<?php // 캘린더
include_once("./_tb.php");
if ($d) { $d = preg_match("/^[0-9\-]+$/", $d) ? $d : ""; }
if ($id) { $id = preg_match("/^[A-Za-z0-9_\-]+$/", $id) ? $id : ""; }

$datetime = $d;

if (!$datetime) {

    $datetime = $web['time_ymd'];

}

// 현재 시각에서 월을 구한다.
$dateT1 = date("Y-m", strtotime($datetime));

// 현재 월의 1일의 요일 값을 구한다.
$dateT2 = date("w", strtotime($dateT1."-01"));

// 현재 월의 1일에서 요일 값을 뺀다.
$dateT3 = date("Y-m-d", strtotime($dateT1."-01") - (86400 * $dateT2));

// 현재 월의 1일에서 31일을 더한다.
$dateN1 = date("Y-m-d", strtotime($dateT1."-01") + (86400 * 31));

// 다음 달의 월을 구한다.
$dateN2 = date("Y-m", strtotime($dateN1));

// 다음 달 1일을 구한다.
$dateN3 = date("Y-m-d", strtotime($dateN2."-01"));

// 다음 달 1일에서 1일을 뺀다. 그럼 이번 달 마지막일
$dateN4 = date("d", strtotime($dateN3) - (86400 * 1));

// 6 뺀다. 현재 달 마지막 일 요일을 구해서.
$dateN5 = 6 - date("w", strtotime($dateT1."-".$dateN4));

// 현재 월의 1일에서 1일을 뺀다.
$dateP1 = date("Y-m-d", strtotime($dateT1."-01") - (86400 * 1));

// 작년
$dateM1 = (int)(date("Y", strtotime($datetime)) - 1)."-".date("m", strtotime($datetime))."-01";

// 내년
$dateM2 = (int)(date("Y", strtotime($datetime)) + 1)."-".date("m", strtotime($datetime))."-01";
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<?
include_once("$web[path]/_head.php");
?>
<link rel="stylesheet" href="<?=$web['host_css']?>/jquery.jscrollpane.css" type="text/css" />
<style type="text/css">
body {background-color:#f8f8f8; margin:0px;}
a {text-decoration:none;}
a:hover {text-decoration:none;}

.title {background-color:#a1b1c2; border-bottom:1px solid #7e8e9f;}
.title .text {font-weight:700; line-height:50px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.title .icon, .title .close {width:50px; height:50px; background:url('img/title.png') no-repeat;}
.title .close {background-position:-50px 0px;}
.title .close a {display:block; width:50px; height:50px;}

.ym {border-top:1px solid #ffffff; position:relative; height:50px; margin:0 29px;}
.ym .today {display:block; position:absolute; left:0; top:0px; width:33px; height:50px;}
.ym .today {background:url('img/today.png') no-repeat;}
.ym .today:hover {background-position:0px -50px;}
.ym .today:active {background-position:0px -100px;}
.ym .text {font-weight:800; line-height:24px; font-size:24px; color:#7e8e9f; font-family:'Nanum Gothic',gulim,serif;}
.ym .text span {position:relative; left:0; top:4px;}

.ym .arrow {position:relative; cursor:pointer;}
.ym .arrow,
.ym .paging1,
.ym .paging2,
.ym .paging3,
.ym .paging4  {display:block; width:20px; height:50px; background:url('img/paging.png') no-repeat;}
.ym .arrow {background-position:0px 0px;}
.ym .arrow:hover {background-position:0px -50px;}
.ym .arrow:active {background-position:0px -100px;}
.ym .paging1 {background-position:-20px 0px;}
.ym .paging1:hover {background-position:-20px -50px;}
.ym .paging1:active {background-position:-20px -100px;}
.ym .paging2 {background-position:-40px 0px;}
.ym .paging2:hover {background-position:-40px -50px;}
.ym .paging2:active {background-position:-40px -100px;}
.ym .paging3 {background-position:-60px 0px;}
.ym .paging3:hover {background-position:-60px -50px;}
.ym .paging3:active {background-position:-60px -100px;}
.ym .paging4 {background-position:-80px 0px;}
.ym .paging4:hover {background-position:-80px -50px;}
.ym .paging4:active {background-position:-80px -100px;}

.ym .layer {display:none; position:absolute; left:-140px; top:50px; width:193px; border:1px solid #dbe1e8; background-color:#f8f8f8; cursor:default;}
.ym .array {height:100px; overflow:auto; overflow-x:hidden; background-color:#ffffff;}
.ym .array p {margin:0px; display:block; padding-left:10px; height:20px; cursor:pointer;}
.ym .array p {line-height:20px; font-size:13px; color:#000000; font-family:gulim,serif; cursor:pointer;}
.ym .array p:hover {background-color:#e9f0f4; color:#0d5c9b;}
.ym .array p.on {background-color:#e9f0f4; font-weight:bold; color:#0d5c9b;}
.ym .close {display:block; position:absolute; right:0; top:0px; width:16px; height:29px; background:url('img/close.png') no-repeat; cursor:pointer;}
.ym .close:hover {background-position:0px -29px;}
.ym .close:active {background-position:0px -58px;}
.ym .subj {position:relative; padding-left:10px; width:86px; font-weight:bold; line-height:30px; font-size:12px; color:#7e8e9f; font-family:gulim,serif;}

.box {margin:0 20px;}
.box .round1,
.box .round2,
.box .round3,
.box .round4 {width:10px; height:10px; background:url('img/tablebg_round.png') no-repeat;}
.box .round1 {background-position:0px 0px;}
.box .round2 {background-position:-10px 0px;}
.box .round3 {background-position:0px -10px;}
.box .round4 {background-position:-10px -10px;}
.box .width1,
.box .width2 {height:10px; background:url('img/tablebg_width.png') repeat-x;}
.box .width1 {background-position:0px 0px;}
.box .width2 {background-position:0px -10px;}
.box .height1,
.box .height2 {height:10px; background:url('img/tablebg_height.png') repeat-y;}
.box .height1 {background-position:0px 0px;}
.box .height2 {background-position:-10px 0px;}

.list {border:1px solid #dbe1e8; background-color:#fdfdfd;}
.list .week {border-bottom:1px solid #e8ebf0; border-left:1px solid #e8ebf0; width:14.2%; text-align:center;}
.list .week {font-weight:700; line-height:30px; font-size:13px; color:#7e8e9f; font-family:'Nanum Gothic',gulim,serif;}
.list .week.first {border-left:0px;}

.list a {display:block; text-align:center;}
.list a:hover {background-color:#e9f0f4;}
.list .day {font-weight:700; line-height:40px; font-size:16px; color:#7e8e9f; font-family:'Nanum Gothic',gulim,serif;}
.list .day:hover {color:#000000;}
.list .sun {font-weight:700; line-height:40px; font-size:16px; color:#c26d6d; font-family:'Nanum Gothic',gulim,serif;}
.list .sun:hover {color:#ff0000;}
.list .sat {font-weight:700; line-height:40px; font-size:16px; color:#6294d3; font-family:'Nanum Gothic',gulim,serif;}
.list .sat:hover {color:#0072ff;}

.list .day2 {background-color:#e9f0f4; font-weight:700; line-height:40px; font-size:16px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.list .sun2 {background-color:#e9f0f4; font-weight:700; line-height:40px; font-size:16px; color:#ff0000; font-family:'Nanum Gothic',gulim,serif;}
.list .sat2 {background-color:#e9f0f4; font-weight:700; line-height:40px; font-size:16px; color:#0072ff; font-family:'Nanum Gothic',gulim,serif;}

@media screen and (max-width:500px) {

.box {margin:0 10px;}

}
</style>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="<?=$web['host_js']?>/jquery.mousewheel.js"></script>
<script type="text/javascript">
var year = "<?=date("Y", strtotime($datetime))?>";
var month = "<?=date("m", strtotime($datetime))?>";
var dyear = "<?=date("Y", strtotime($datetime))?>";
var dmonth = "<?=date("m", strtotime($datetime))?>";

$(document).ready( function() {

    $('body').swipe( {

        swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {

            location.href = "?d=<?=$dateN3?>&id=<?=$id?>";

        },

        swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {

            location.href = "?d=<?=$dateP1?>&id=<?=$id?>";

        }

    });

    $('.ym .arrow').click(function(e) {

        if (!$(e.target).is('.ym .layer *')) {

            var block = $('.ym .layer');

            if (block.is(':hidden') == true) {

                block.show();

                $('#year p').removeClass('on');
                $('#year p[name='+dyear+']').addClass('on');

                $('#month p').removeClass('on');
                $('#month p[name='+dmonth+']').addClass('on');

                $('#year').jScrollPane({ showArrows: true });
                $('#month').jScrollPane({ showArrows: true });

                var yeartop = ($('#year p[name='+dyear+']').offset().top) - 175;
                $('#year').data('jsp').scrollTo(0, yeartop);

                var monthtop = ($('#month p[name='+dmonth+']').offset().top) - 180;
                $('#month').data('jsp').scrollTo(0, monthtop);

            } else {

                block.hide();

            }

        }

    });

    $('.ym .close').click(function() {

        $('.ym .layer').hide();

    });

    $('#year p').click(function() {

        year = $(this).attr('name');

        $('#year p').removeClass('on');
        $(this).addClass('on');

    });

    $('#month p').click(function() {

        month = $(this).attr('name');

        $('#month p').removeClass('on');
        $(this).addClass('on');

        location.href = "?d="+year+"-"+month+"-01&id=<?=$id?>";

    });

});

function dateAdd(ymd)
{

    opener.document.getElementById("<?=$id?>").value = ymd;

    if (opener.document.getElementById("calendar")) {

        var evt = opener.document.getElementById("calendar").value;

        if (evt == 'listSearch') {

            opener.listSearch('sort');

        }

        else if (evt == 'chartLoad') {

            opener.chartLoad();

        }

    }

    window.close();

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<div class="title">
<table width="100%" height="50" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="icon"></td>
    <td class="text">달력</td>
    <td class="close"><a href="#" onclick="window.close();"></a></td>
</tr>
</table>
</div>
<div class="ym">
<a href="?d=<?=$web['time_ymd']?>&amp;id=<?=$id?>" class="today" title="오늘"></a>
<table border="0" cellspacing="0" cellpadding="0" class="auto">
<tr>
    <td><a href="?d=<?=$dateM1?>&amp;id=<?=$id?>" class="paging1" title="전년"></a></td>
    <td><a href="?d=<?=$dateP1?>&amp;id=<?=$id?>" class="paging2" title="전월"></a></td>
    <td width="10"></td>
    <td class="text"><span><?=date("Y. m", strtotime($dateT1."-01"))?></span></td>
    <td width="3"></td>
    <td>
<div class="arrow">
<div class="layer">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="subj">년 선택</td>
    <td width="1" bgcolor="#dbe1e8"></td>
    <td class="subj">월 선택<span class="close"></span></td>
</tr>
<tr><td colspan="3" bgcolor="#dbe1e8" height="1"></td></tr>
<tr>
    <td valign="top">
<div id="year" class="array">
<?
$year =date("Y", $web['server_time']);
for ($i=(int)($year - 100); $i<=(int)($year + 20); $i++) {

    echo "<p name='".$i."'>".$i."년</p>";

}
?>
</div>
    </td>
    <td width="1" bgcolor="#dbe1e8"></td>
    <td valign="top">
<div id="month" class="array">
<?
for ($i=1; $i<=12; $i++) {

    $month = sprintf("%02d" , $i);

    echo "<p name='".$month."'>".$month."월</p>";

}
?>
</div>
    </td>
</tr>
</table>
</div>
</div>
    </td>
    <td><a href="?d=<?=$dateN3?>&amp;id=<?=$id?>" class="paging3" title="다음월"></a></td>
    <td><a href="?d=<?=$dateM2?>&amp;id=<?=$id?>" class="paging4" title="다음년"></a></td>
</tr>
</table>
</div>
<div class="box">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td class="round1"></td>
    <td class="width1"></td>
    <td class="round2"></td>
</tr>
<tr>
    <td class="height1"></td>
    <td>
<div class="list">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height='30'>
    <td class="week first">일</td>
    <td class="week">월</td>
    <td class="week">화</td>
    <td class="week">수</td>
    <td class="week">목</td>
    <td class="week">금</td>
    <td class="week">토</td>
</tr>
<tr height='50'>
<?
$n = 0;
$mod = 7;
for ($i=0; $i<($dateN4 + $dateT2 + $dateN5); $i++) {

    $dateT4 = date("Y-m-d", strtotime($dateT3) + (86400 * $i));

    $dateT5 = date("w", strtotime($dateT3) + (86400 * $i));

    if ($i && $i%$mod == 0) {

        $n++;

        echo "</tr>\n<tr>\n";

    }

    if ($dateT5 != 0) {

        $line = "border-left:1px solid #f8f8f8;";

    } else {

        $line = "";

    }

    if ($n) {

        echo "<td style='".$line." border-top:1px solid #f8f8f8;'>";

    } else {

        echo "<td style='".$line."'>";

    }

    if ($dateT1 == substr($dateT4,0,7)) {

        if ($web['time_ymd'] == $dateT4) {

            if ($dateT5 == 0) {

                $class = "sun2";

            }

            else if ($dateT5 == '6') {

                $class = "sat2";

            } else {

                $class = "day2";

            }

        } else {

            if ($dateT5 == 0) {

                $class = "sun";

            }

            else if ($dateT5 == '6') {

                $class = "sat";

            } else {

                $class = "day";

            }

        }

        echo "<a href=\"#\" onclick=\"dateAdd('".substr($dateT4,0,10)."'); return false;\" class='".$class."'>";
        echo substr($dateT4,8,2);
        echo "</a>";

    }

    echo "</td>\n";

}

$cnt = $i%$mod;
if ($cnt) {

    for ($i=$cnt; $i<$mod; $i++) {

        echo "<td>&nbsp;</td>\n";

    }

}
?>
</tr>
</table>
</div>
    </td>
    <td class="height2"></td>
</tr>
<tr>
    <td class="round3"></td>
    <td class="width2"></td>
    <td class="round4"></td>
</tr>
</table>
</div>
</body>
</html>