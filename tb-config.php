<?php // 설정
define("_WEB_", TRUE);

// db
$web['db_web'] = "tb_";
$web['area_table'] = $web['db_web']."area";
$web['banner_table'] = $web['db_web']."banner";
$web['bbs_table'] = $web['db_web']."bbs";
$web['bbs_group_table'] = $web['db_web']."bbs_group";
$web['bbs_file_table'] = $web['db_web']."bbs_file";
$web['bbs_good_table'] = $web['db_web']."bbs_good";
$web['bbs_police_table'] = $web['db_web']."bbs_police";
$web['bbs_scrap_table'] = $web['db_web']."bbs_scrap";
$web['connect_table'] = $web['db_web']."connect";
$web['page_table'] = $web['db_web']."page";
$web['page_group_table'] = $web['db_web']."page_group";
$web['popup_table'] = $web['db_web']."popup";
$web['sms_table'] = $web['db_web']."sms";
$web['email_table'] = $web['db_web']."email";
$web['session_table'] = $web['db_web']."session";
$web['setup_table'] = $web['db_web']."setup";
$web['setup_api_table'] = $web['db_web']."setup_api";
$web['setup_auth_table'] = $web['db_web']."setup_auth";
$web['setup_join_table'] = $web['db_web']."setup_join";
$web['setup_email_table'] = $web['db_web']."setup_email";
$web['real_name_table'] = $web['db_web']."real_name";
$web['real_hp_table'] = $web['db_web']."real_hp";
$web['real_email_table'] = $web['db_web']."real_email";
$web['notice_table'] = $web['db_web']."notice";
$web['search_table'] = $web['db_web']."search";
$web['visit_table'] = $web['db_web']."visit";
$web['visit_page_table'] = $web['db_web']."visit_page";

//$web['db_bbs'] = "bbs.tb_";
$web['db_bbs'] = "tb_";
$web['article_table'] = $web['db_bbs']."article_";
$web['reply_table'] = $web['db_bbs']."reply_";

//$web['db_member'] = "member.tb_";
$web['db_member'] = "tb_";
$web['member_table'] = $web['db_member']."member";
$web['member_file_table'] = $web['db_member']."member_file";
$web['member_login_table'] = $web['db_member']."member_login";
$web['member_photo_table'] = $web['db_member']."member_photo";
$web['member_level_table'] = $web['db_member']."member_level";
$web['member_point_table'] = $web['db_member']."member_point";
$web['member_hp_table'] = $web['db_member']."member_hp";
$web['member_ipin_table'] = $web['db_member']."member_ipin";

//$web['db_message'] = "message.tb_";
$web['db_message'] = "tb_";
$web['message_receive_table'] = $web['db_message']."message_receive";
$web['message_send_table'] = $web['db_message']."message_send";

// other
$web['charset'] = "utf-8";
$web['mysqli'] = false;
$web['server_time'] = time();
$web['time_ymd'] = date("Y-m-d", $web['server_time']);
$web['time_his'] = date("H:i:s", $web['server_time']);
$web['time_ymdhis'] = date("Y-m-d H:i:s", $web['server_time']);

$web['host_default'] = $web['url'];
$web['host_adm'] = $web['url']."/adm";
$web['host_img'] = $web['url']."/img";
$web['host_css'] = $web['url']."/css";
$web['host_js'] = $web['url']."/js";
$web['host_member'] = $web['url']."/member";
$web['host_cheditor'] = $web['url']."/cheditor";
$web['host_smarteditor'] = $web['url']."/smarteditor2";
$web['host_page'] = $web['url']."/page";
$web['host_bbs'] = $web['url']."/bbs";
$web['host_rbbs'] = $web['url']."/bbs"; // 글쓰기 서버

$disk['path'] = $web['path']."/data";
$disk['server'] = $web['url']."/data";
$disk['server_bbs'] = $disk['server'];
$disk['server_member'] = $disk['server'];
$disk['server_editor'] = $disk['server']."/editor";
$disk['server_popup'] = $disk['server']."/popup";
$disk['server_banner'] = $disk['server']."/banner";

$web['real_name_count'] = 100; // 본인확인 1일 제한 수
$web['real_hp_count'] = 100; // 휴대폰 인증 1일 제한 수
$web['real_email_count'] = 100; // 이메일 인증 1일 제한 수

$web['cookie_domain'] = ""; // 서브 도메인 사용시 .teraboard.net 처럼 점을 붙여 입력
$web['document_domain'] = ""; // 서브 도메인 사용시 teraboard.net 처럼 도메인 입력
?>