/* 공통 */
function message(mode)
{

    var overlay = $("#overlay");
    var box = $("#messagebox");

    if (mode == 'open') {

        box.show();

        var win = $(window);
        var boxLeft = (win.scrollLeft() + (win.width() / 2)) - (box.width() / 2);
        var boxTop = (win.scrollTop() + (win.height() / 2)) - (box.height() / 2);

        overlay.css( { 'opacity' : '0.5'} );
        overlay.show();

        box.css( { 'left': boxLeft+'px', 'top': boxTop+'px', 'opacity' : '1.0' } );

        $(document).keydown(function (e) {

            if ($("#messagebox").is(":hidden") == false) {

                if (e.keyCode == '13' || e.keyCode == '27') {

                    $('#messagebox p.btn a.msgclose').click();

                }

            }

        });

        overlay.click(function() {

            $('#messagebox p.btn a.msgclose').click();

        });

    } else {

        box.html("");
        box.hide();
        overlay.hide();

    }

}

function sideviewBox(obj, mode)
{

    var box = $("#sideviewbox");

    if (mode == 'open') {

        box.show();

        var win = $(window);
        var pos = $(obj);
        var postop = pos.offset().top;
        var posleft = pos.offset().left;

        postop = postop + obj.offsetHeight;
        posleft = (posleft + (obj.offsetWidth / 2)) - (box.width() / 2);

/*
        if ((win.scrollTop() + win.height()) < (postop + box.height())) {
            postop = postop - (box.height() + obj.offsetHeight);
        }
*/

        box.css( { 'left': posleft+'px', 'top': postop+'px', 'opacity' : '1.0' } );

        $(document).keydown(function (e) {

            if ($("#sideviewbox").is(":hidden") == false) {

                if (e.keyCode == '27') {

                    sideviewBox(obj, 'close');

                }

            }

        });

        $(document).click(function(e) {

            sideviewBox(obj, 'close');

        });

    } else {

        box.html("");
        box.hide();

    }

}

function imageBox(obj, mode)
{

    var ibox = $("#imagebox");

    if (mode == 'open') {

        ibox.show();

        var win = $(window);
        var pos = $(obj);
        var postop = pos.offset().top;
        var posleft = pos.offset().left;
        var objw = obj.width;
        var objh = obj.height;

        if ((postop + ibox.height() + (objh / 2)) < (win.scrollTop() + win.height())) {

            //var iboxTop = postop + 50;
            var iboxTop = postop + (objh / 2);

        } else {

            //var iboxTop = (postop + 50) - ibox.height();
            var iboxTop = (postop + (objh / 2)) - ibox.height();

        }

        if ((posleft + ibox.width() + (objw / 2)) < (win.scrollLeft() + win.width())) {

            //var iboxLeft = posleft + 50;
            var iboxLeft = posleft + (objw / 2);

        } else {

            //var iboxLeft = (posleft + 25) - ibox.width();
            var iboxLeft = (posleft + (objw / 2)) - ibox.width();

        }

        ibox.css( { 'left': iboxLeft+'px', 'top': iboxTop+'px', 'opacity' : '1.0' } );

        $(document).keydown(function (e) {

            if ($("#imagebox").is(":hidden") == false) {

                if (e.keyCode == '27') {

                    imageBox('', 'close');

                }

            }

        });

        ibox.click(function() {

            imageBox('', 'close');

        });

    } else {

        ibox.html("");
        ibox.hide();

    }

}

function imageLoad(obj, source, w, h)
{

    w = parseInt(w);
    h = parseInt(h);

    if (w > 500 && w >= h) {

        var rate = (500 / w);
        w = 500;
        h = parseInt(h * rate);

    }

    else if (h > 500 && w <= h) {

        var rate = (500 / h);
        w = parseInt(w * rate);
        h = 500;

    }

    $('#imagebox').draggable({ cancel : '.cancel', handle: '.handler' });
    $('#imagebox').css( { 'width': w+'px', 'height': h+'px' } );

    $.post("/_image.php", {"source" : source, "w" : w, "h" : h}, function(data) {

        $("#imagebox").html(data);
        imageBox(obj, 'open');

    });

}

function sideviewLoad(obj, mid)
{

    $('#sideviewbox').draggable();

    var nick = $(obj).attr('title');

    $.post(web_url+"/_sideview.php", {"mid" : mid, "bbs_id" : bbs_id, "nick" : nick}, function(data) {

        $("#sideviewbox").html(data);
        sideviewBox(obj, 'open');

    });

}

function browserCheck()
{

    if (check_browser == 'MSIE 5' || check_browser == 'MSIE 5.5' || check_browser == 'MSIE 6' || check_browser == 'MSIE 7' || check_browser == 'MSIE 8') {

        var trident = navigator.userAgent.match(/Trident\/(\d.\d)/i);

        // ie9 이상
        if (trident != null && parseInt(trident[1]) >= 5) {

            return false;

        }

        $.post(web_url+"/_browser.php", {"m" : ""}, function(data) {

            $("#browserbox").html(data);
            browserLoad('open');

        });

        return true;

    }

    return false;

}

function browserLoad(mode)
{

    var overlay = $("#overlay");
    var box = $("#browserbox");

    if (mode == 'open') {

        box.show();

        var win = $(window);
        var boxLeft = (win.scrollLeft() + (win.width() / 2)) - (box.width() / 2);
        var boxTop = (win.scrollTop() + (win.height() / 2)) - (box.height() / 2);

        overlay.css( { 'opacity' : '0.5'} );
        overlay.show();

        box.css( { 'left': boxLeft+'px', 'top': boxTop+'px', 'opacity' : '1.0' } );

        $(document).keydown(function (e) {

            if ($("#browserbox").is(":hidden") == false) {

                if (e.keyCode == '13' || e.keyCode == '27') {

                    $('#browserbox .btnclose').click();

                }

            }

        });

        overlay.click(function() {

            browserLoad('close');

        });

        $('#browserbox .btnclose').click(function() {

            browserLoad('close');

        });

    } else {

        box.html("");
        box.hide();
        overlay.hide();

    }

}

function webCurrent()
{

    var currentId = document.location.hash.split('#')[1];

    if (currentId) {

        var win = $(window);
        var box = $("#current_"+currentId);
        var boxTop = box.offset().top - ((win.height() / 2) - (box.height() / 2));

        $('html,body').animate({scrollTop: boxTop}, 700);
        box.css({ 'border': '1px solid #2e9700' });

    }

}

function pageTop()
{

    $('html,body').animate({scrollTop: 0}, 700);

}

function webGo(url)
{

    if (!url) {

        return;

    }

    document.location.href = url;

}

function resizePopup(w, h)
{

    var oBody=document.getElementsByTagName("Body")[0];

    var oDivPopWrap = document.getElementById("pop_wrap");

    if (h == '') {

        var h = oDivPopWrap.offsetHeight + 30;

    }

    var clintAgent = navigator.userAgent;

    if ( clintAgent.indexOf("MSIE") != -1 ) {

        window.resizeBy(w-oBody.clientWidth, (h + 36)-oBody.clientHeight);

    } else {

        window.resizeBy(w-window.innerWidth, h-oBody.clientHeight);

    }

}

function resizeWindow(w, h)
{

    var oBody=document.getElementsByTagName("Body")[0];

    var oDivPopWrap = document.getElementById("pop_wrap");

    if (h == '') {

        var h = oDivPopWrap.offsetHeight;

    }

    if (h > screen.height - 200) {

        return false;

    }

    var clintAgent = navigator.userAgent;

    if ( clintAgent.indexOf("MSIE") != -1 ) {

        window.resizeBy(w-oBody.clientWidth, h-oBody.clientHeight);

    } else {

        window.resizeBy(w-window.innerWidth, h-oBody.clientHeight);

    }

}

function elementFocus(form_name, element_name, element_id)
{

    var obj = eval("document."+form_name+"."+element_name)[element_id];

    obj.checked = true;

}

function elementCheck(form_name, element_name)
{

    var obj = eval("document."+form_name+"."+element_name);

    if (obj.checked == true) {

        obj.checked = false;

    } else {

        obj.checked = true;

    }

}

function elementCheckID(element_id)
{

    var obj = document.getElementById(element_id);

    if (obj.checked == true) {

        obj.checked = false;

    } else {

        obj.checked = true;

    }

}

function elementNumberFormat(element_id, val)
{

    var obj = document.getElementById(element_id);

    if (obj.value) {
        obj.value = webNumberFormat(String(obj.value.replace(/(,)/g, "")));
    }

}

function elementAddVal(id1, id2, val)
{

    if (document.getElementById(id1).checked == true) {

        document.getElementById(id2).value = val;

    } else {

        document.getElementById(id2).value = "";

    }

}

function elementAddTime(id1, id2)
{

    var date = new Date();

    var Year = date.getFullYear();
    var Month = date.getMonth() + 1;
    var Day = date.getDate();
    var Hour = date.getHours();
    var Min = date.getMinutes();
    var Sec = date.getSeconds();

    if (Month < 10) {

        var Month = "0"+Month;

    }

    if (Day < 10) {

        var Day = "0"+Day;

    }

    if (Hour < 10) {

        var Hour = "0"+Hour;

    }

    if (Min < 10) {

        var Min = "0"+Min;

    }

    if (Sec < 10) {

        var Sec = "0"+Sec;

    }

    var time_view = Year+"-"+Month+"-"+Day+" "+Hour+":"+Min+":"+Sec;

    if (document.getElementById(id1).checked == true) {

        document.getElementById(id2).value = time_view;

    } else {

        document.getElementById(id2).value = "";

    }

}

function webNumberFormat(data)
{

    var tmp = '';
    var number = '';
    var cutlen = 3;
    var comma = ',';
    var i;
    var str = data.split(".");
    var minority = '';

    if (str.length) {

        if (data.charAt(data.length - 1) == '.') {

            return data;

        }

        data = str[0];
        minority = str[1];

    }

    if (data < 0) {

        data = data.replace(/(-)/g, "");

        len = data.length;
        mod = (len % cutlen);
        k = cutlen - mod;

        for (i=0; i<data.length; i++) {

            number = number + data.charAt(i);

            if (i < data.length - 1 && data.charAt(i)) {

                k++;

                if ((k % cutlen) == 0) {

                    number = number + comma;
                    k = 0;

                }

            }

        }

        number  = "-"+number;

    } else {

        len = data.length;
        mod = (len % cutlen);
        k = cutlen - mod;

        for (i=0; i<data.length; i++) {

            number = number + data.charAt(i);

            if (i < data.length - 1 && data.charAt(i)) {

                k++;

                if ((k % cutlen) == 0) {

                    number = number + comma;
                    k = 0;

                }

            }

        }

    }

    if (minority) {

        return number+'.'+minority;

    } else {

        return number;

    }

}

function trim(str)
{

    str = str.replace(/(^\s*)|(\s*$)/g, "");
    return str;

}

function smsByte(content, bytes)
{

    var conts = document.getElementById(content);
    var bytes = document.getElementById(bytes);

    var i = 0;
    var cnt = 0;
    var exceed = 0;
    var ch = '';

    for (i=0; i<conts.value.length; i++) {

        ch = conts.value.charAt(i);

        if (escape(ch).length > 4) {

            cnt += 2;

        } else {

            cnt += 1;

        }

    }

    bytes.innerHTML = cnt;

/*
    if (cnt > 80) {

        exceed = cnt - 80;
        alert('메시지 내용은 80바이트를 넘을수 없습니다.\n\n작성하신 메세지 내용은 '+ exceed +'byte가 초과되었습니다.\n\n초과된 부분은 자동으로 삭제됩니다.');
        var tcnt = 0;
        var xcnt = 0;
        var tmp = conts.value;
        for (i=0; i<tmp.length; i++) {

            ch = tmp.charAt(i);

            if (escape(ch).length > 4) {

                tcnt += 2;

            } else {

                tcnt += 1;

            }

            if (tcnt > 80) {

                tmp = tmp.substring(0,i);
                break;

            } else {

                xcnt = tcnt;

            }

        }

        conts.value = tmp;
        bytes.innerHTML = xcnt;

        return;

    }
*/

}

function textLenth(content, bytes)
{

    var conts = document.getElementById(content);
    var bytes = document.getElementById(bytes);

    var i = 0;
    var cnt = 0;

    for (i=0; i<conts.value.length; i++) {

        cnt += 1;

    }

    bytes.innerHTML = cnt;

}

function win_open(url, name, option)
{

    var popup = window.open(url, name, option);
    popup.focus();

}

function scrollbarWidth()
{

    var div = $('<div style="display:none; overflow:scroll;"></div>');
    $('body').append(div);
    var w = div.innerWidth();
    div.css({ overflow: 'auto', height: '1px' });
    var w2 = div.innerWidth();
    return w - w2;

}

function calendarOpen(id)
{

    win_open(web_url+"/other/calendar/?id="+id+"&d="+$("#"+id).val(),"calendarOpen","width=420, height=420, scrollbars=no");

}

function zipcodeOpen(fname, fzipcode, faddr1, faddr2)
{

    win_open(web_url+"/other/zipcode/?fname="+fname+"&fzipcode="+fzipcode+"&faddr1="+faddr1+"&faddr2="+faddr2,"zipcodeOpen","width=620, height=450, scrollbars=yes");

}

function photoOpen(mid)
{

    win_open(web_url+"/member/photo/image.php?mid="+mid,"photoOpen","width=620, height=620, scrollbars=yes");

}

function memberProfile(mid)
{

    var url = web_url+"/member/profile/?mid="+mid;

    win_open(url, "memberProfile", "width=640, height=560, scrollbars=yes");

}

function messageRegist(mid)
{

    var url = web_url+"/member/message/regist.php";

    if (mid) {

        url += "?mid="+mid;

    }

    win_open(url, "memoSend", "width=620, height=560, scrollbars=yes");

}

function messageReceive(id)
{

    win_open(web_url+"/member/message/receive.php?id="+id, "memoOpen"+id, "width=620, height=560, scrollbars=yes");

}

function messageAuto(id)
{

    win_open(web_url+"/member/message/receive.php?id="+id, "memoOpen"+id, "width=620, height=560, scrollbars=yes");

}

function emailRegist(mid)
{

    var url = web_url+"/member/email/regist.php";

    if (mid) {

        url += "?mid="+mid;

    }

    win_open(url, "emailRegist", "width=900, height=700, scrollbars=yes");

}

function smsRegist(mid)
{

    var url = web_url+"/member/sms/regist.php";

    if (mid) {

        url += "?mid="+mid;

    }

    win_open(url, "smsRegist", "width=620, height=560, scrollbars=yes");

}

function nameCertify(callback)
{

    var url = web_url+"/member/certify/name.php";

    if (callback) {

        url += "?callback="+callback;

    }

    win_open(url, "nameCertify", "width=450, height=550, scrollbars=no");

}

function hpCertify(callback)
{

    var url = web_url+"/member/certify/hp.php";

    if (callback) {

        url += "?callback="+callback;

    }

    win_open(url, "hpCertify", "width=320, height=400, scrollbars=no");

}

function emailCertify(callback)
{

    var url = web_url+"/member/certify/email.php";

    if (callback) {

        url += "?callback="+callback;

    }

    win_open(url, "emailCertify", "width=320, height=400, scrollbars=no");

}

function memberLogin(mid)
{

    win_open(web_url+"/member/login/list.php?mid="+mid, "memberLogin"+mid, "width=550, height=700, scrollbars=no");

}

function bannerClick(id, key)
{

    $.post(web_url+"/_banner.php", {"id" : id, "key" : key}, function(data) {

        $('#update_data').html(data);

    });

}

function loginOk()
{

    var url = $('#url').val();

    if (url) {

        location.href = url;

    } else {

        location.reload();

    }

}

function naverLogin()
{

    if (check_touch) {

        location.href = web_url+"/member/login/naver.php?loginmode=document&url="+document.formUpdate.url.value;

    } else {

        win_open(web_url+"/member/login/naver.php", "socialLogin", "width=500, height=600, scrollbars=1");

    }

}

function kakaoLogin()
{

    if (check_touch) {

        location.href = web_url+"/member/login/kakao.php?loginmode=document&url="+document.formUpdate.url.value;

    } else {

        win_open(web_url+"/member/login/kakao.php", "socialLogin", "width=500, height=600, scrollbars=1");

    }

}

function facebookLogin()
{

    if (check_touch) {

        location.href = web_url+"/member/login/facebook.php?loginmode=document&url="+document.formUpdate.url.value;

    } else {

        win_open(web_url+"/member/login/facebook.php", "socialLogin", "width=500, height=600, scrollbars=1");

    }

}

function twitterLogin()
{

    if (check_touch) {

        location.href = web_url+"/member/login/twitter.php?loginmode=document&url="+document.formUpdate.url.value;

    } else {

        win_open(web_url+"/member/login/twitter.php", "socialLogin", "width=500, height=600, scrollbars=1");

    }

}

function googleLogin()
{

    if (check_touch) {

        location.href = web_url+"/member/login/google.php?loginmode=document&url="+document.formUpdate.url.value;

    } else {

        win_open(web_url+"/member/login/google.php", "socialLogin", "width=500, height=600, scrollbars=1");

    }

}

function instagramLogin()
{

    if (check_touch) {

        location.href = web_url+"/member/login/instagram.php?loginmode=document&url="+document.formUpdate.url.value;

    } else {

        win_open(web_url+"/member/login/instagram.php", "socialLogin", "width=500, height=600, scrollbars=1");

    }

}

function kakaoLink()
{

    if (!check_touch) {
        alert("휴대폰에서만 가능합니다.");
        return false;
    }

    Kakao.Link.sendTalkLink({
        label: web_kakaomsg
    });

}

function smsLink()
{

    if (!check_touch) {
        alert("휴대폰에서만 가능합니다.");
        return false;
    }

    location = "sms:"+web_sms;

}

function telLink()
{

    if (!check_touch) {
        alert("휴대폰에서만 가능합니다.");
        return false;
    }

    location = "tel:"+web_tel;

}

function bbsPolice(bbs_id, article_id)
{

    win_open(bbs_url+"/police.php?mode=1&bbs_id="+bbs_id+"&article_id="+article_id, "policeOpen", "width=550, height=530, scrollbars=no");

}

function replyPolice(bbs_id, article_id, reply_id)
{

    win_open(bbs_url+"/police.php?mode=2&bbs_id="+bbs_id+"&article_id="+article_id+"&reply_id="+reply_id, "policeOpen", "width=550, height=530, scrollbars=no");

}

function bbsScrap(bbs_id, article_id)
{

    var f = document.formUpdate;

    $.post(bbs_url+"/scrap_update.php", {"form_check" : f.form_check.value, "bbs_id" : bbs_id, "article_id" : article_id, "m" : ""}, function(data) {

        $("#update_data").html(data);

    });

}

function bbsScrapList(mid)
{

    var h = screen.height - 200;

    win_open(web_url+"/member/bbs/scrap.php?mid="+mid, "bbsScrapList", "width=1000, height="+h+", scrollbars=1");

}