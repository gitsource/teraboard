(function ($) {
$.fn.tbSlide = function(o) {

    var obj = $(this);

    o = $.extend({
        num: 0,
        end: 0,
        speed: 300,
        time: 4000,
        touch: true
    }, o);

    var play = function(n) {

        obj.find('.block').stop().hide();

        if (obj.find('.block').length == 1) {

            obj.find('.block').eq(n).stop().show();

        } else {

            obj.find('.block').eq(n).stop().fadeIn(o.speed);

        }

        obj.find('.num ul li').removeClass('on');
        obj.find('.num ul li').eq(n).addClass('on');

        o.num = n;
        o.end = obj.find('.block').length - 1;

    };

    var auto = function(type) {

        if (!type || type == 'left') {

            if (o.num == o.end) {

                o.num = 0;

            } else {

                o.num = o.num + 1;

            }

        } else {

            if (o.num == 0) {

                o.num = o.end;

            } else {

                o.num = o.num - 1;

            }

        }

        play(o.num);

    };

    if (o.touch && check_touch) {

        obj.swipe( {

            swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {

                clearInterval(interval);

                auto(direction);

            },

            swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {

                clearInterval(interval);

                auto(direction);

            }

        });

    }

    play(o.num);

    obj.find('.num ul li').click(function() {

        play(parseInt($(this).index()));

    });

    obj.find('.btn-prev').click(function() {

        auto('right');

    });

    obj.find('.btn-next').click(function() {

        auto('left');

    });

    var interval;
    interval = setInterval(auto, o.time);

    obj.mouseenter(function() { clearInterval(interval); }).mouseleave(function(){ interval = setInterval(auto, o.time); });

};
})(jQuery);