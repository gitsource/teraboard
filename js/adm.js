/* 관리자 */
var default_height = 0;
var zindex = 1000;
$(document).ready( function() {

    if (menu_id) {

        $('.layout-leftmenu .menu ul.'+menu_id).removeClass("off").addClass('on');

        if (menu_num) {

            $('.layout-leftmenu .menu ul.'+menu_id+' li[name='+menu_num+'] a').addClass('on');

        }

    }

    if (check_touch) {

        $('.layout-leftmenu .menu ul li.first a').click(function() {

            return false;

        });

    }

    $('.layout-leftmenu .menu ul.off').mouseenter(function() {

        $('.layout-leftmenu .menu ul.on li.first a').addClass('hover');

    }).mouseleave(function() {

        $('.layout-leftmenu .menu ul.on li.first a').removeClass('hover');

    });

    $('.nav div.search').click(function() {

        var block = $(this).parent().children('div.block');

        if (block.is(':hidden') == true) {

            block.show();
            $('#navq').focus();

        } else {

            block.hide();

        }

    });

    $('.nav ul li.search .block span.submit').click(function() {

        if (submitNavSearch()) {

            document.formNavSearch.submit();

        }

    });

    $('.nav ul li.level .click').click(function(e) {

        if (!$(e.target).is('.nav ul li.level .authbox .close')) {

            $('.nav ul li.level .authbox').show();

        }

    });

    $('.nav ul li.level .authbox .close').click(function() {

        $('.nav ul li.level .authbox').hide();

    });

    $('.select .block span').click(function() {

        if ($(this).parent().children('div.option').is(':hidden') == true) {

            zindex++;

            $(this).parent().parent().parent('div.selectbox').addClass('opt').zIndex(zindex);

        } else {

            $(this).parent().parent().parent('div.selectbox').removeClass('opt');

        }

    });

    $('.select .block .option ul.array li').click(function() {

        // new
        if ($(this).is('[data-value]')) {

            $(this).parent().parent().children('input.add').val($(this).attr('data-value'));
            $(this).parent().parent().parent().children('span.text').text($(this).text());
            $(this).parent().parent().parent().parent().parent('div.selectbox').removeClass('opt');
            $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg[data-value="'+$(this).attr('data-value')+'"]').show();

        } else {

            $(this).parent().parent().children('input.add').val($(this).attr('name'));
            $(this).parent().parent().parent().children('span.text').text($(this).text());
            $(this).parent().parent().parent().parent().parent('div.selectbox').removeClass('opt');
            $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg[name="'+$(this).attr('name')+'"]').show();

        }

        var act = $(this).parent().parent().parent().parent().attr('rel');

        if (act == 'listSearch') {

            listSearch('sort');

        }

        if (act == 'reportMove') {

            reportMove();

        }

    });

    $('.selectbox .add').each(function() {

        // new
        if ($(this).parent().children('ul.array').find('li').is('[data-value]')) {

            var addtext = $(this).parent().children('ul.array').children('li[data-value="'+$(this).val()+'"]').text();

            if (addtext) {

                $(this).parent().children('ul.array').children('li[data-value="'+$(this).val()+'"]').addClass('on');
                $(this).parent().parent().children('span.text').text(addtext);
                $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
                $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[data-value="'+$(this).val()+'"]').show();

            } else {

                $(this).parent().parent().children('span.text').html('&nbsp;');

            }

        } else {
        // old

            var addtext = $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').text();

            if (addtext) {

                $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').addClass('on');
                $(this).parent().parent().children('span.text').text(addtext);
                $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
                $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[name="'+$(this).val()+'"]').show();

            } else {

                $(this).parent().parent().children('span.text').html('&nbsp;');

            }

        }

    });

    $('#period').each(function() {

        if ($(this).val() != '') {

            $('.list-period .btn_period[name="'+$(this).val()+'"]').addClass('on');

        }

    });

    $('.list-period .btn_period').click(function() {

        var num = $(this).attr('name');

        $('.list-period .btn_period').removeClass('on');
        $(this).addClass('on');

        $('#period').val(num);
        $('#periodstart').val(eval('period'+num+'start'));
        $('#periodend').val(eval('period'+num+'end'));

        listSearch('sort');

    });

    $('#periodstart').click(function() {

        $('.list-period .btn_period').removeClass('on');

    });

    $('#periodend').click(function() {

        $('.list-period .btn_period').removeClass('on');

    });

    $('.list-top .period').click(function() {

        var obj = $('.list-period');

        if (obj.is(':hidden') == true) {

            $(this).addClass('on');

            obj.show();

            $.cookie('list-period', 'show', { expires: 30, path: '/' });

        } else {

            $(this).removeClass('on');

            obj.hide();

            $.cookie('list-period', 'show', { expires: 0, path: '/' });

        }

    });

    if ($.cookie('list-period') == 'show') {

        $('.list-top .period').addClass('on');

        $('.list-period').show();

    }

    $('.list-top .scope').click(function() {

        var obj = $('.list-scope');

        if (obj.is(':hidden') == true) {

            $(this).addClass('on');

            obj.show();

            $.cookie('list-scope', 'show', { expires: 30, path: '/' });

        } else {

            $(this).removeClass('on');

            obj.hide();

            $.cookie('list-scope', 'show', { expires: 0, path: '/' });

        }

    });

    if ($.cookie('list-scope') == 'show') {

        $('.list-top .scope').addClass('on');

        $('.list-scope').show();

    }

    $('.list-box tr.title td.title .arrow .click').click(function() {

        var obj = $(this).parent().parent().parent().parent().children('div.sbox');

        if (obj.is(':hidden') == true) {

            obj.show().draggable();

            zindex++;

            $(this).parent().parent().parent().parent('div.arrow').zIndex(zindex);

        } else {

            obj.hide();

        }

    });

    $('.list-box tr.title td.title .arrow .sbox').click(function() {

        zindex++;

        $(this).parent('div.arrow').zIndex(zindex);

    });

    $('.list-box tr.title td.title .arrow .sbox .close').click(function() {

        $(this).parent().parent('div.sbox').hide();

    });

    if (search_order) {

        if (search_sort == 'desc') {

            $('.list-box tr.title td.title a span.sort[name='+search_order+']').addClass('desc');

        } else {

            $('.list-box tr.title td.title a span.sort[name='+search_order+']').addClass('asc');

        }

    }

    $('.list-box tr.title td.title .arrow .sbox ul.list li').click(function(e) {

        var obj = $(this).children('input');

        if ($(e.target).is('input')) {

            if (obj.is(':checked') == false) {

                $('#'+obj.attr('rel')).val('');

            } else {

                $('#'+obj.attr('rel')).val(obj.val());

            }

        } else {

            if (obj.is(':checked') == false) {

                obj.prop('checked', true);

                $('#'+obj.attr('rel')).val(obj.val());

            } else {

                obj.prop('checked', false);

                $('#'+obj.attr('rel')).val('');

            }

        }

    });

    $('.list-box tr.title td.title .arrow .sbox ul.btn li.ok').click(function() {

        listSearch('sort');

    });

    $('.list-box tr.title td.title .arrow .sbox ul.btn li.reset').click(function() {

        $(this).parent().parent().children('ul.list').children('li').children('input').each(function() {

            if ($(this).attr('data') == 1) {

                $(this).prop('checked', true);

                $('#'+$(this).attr('rel')).val($(this).val());

            } else {

                $(this).prop('checked', false);

                $('#'+$(this).attr('rel')).val('');

            }

        });

    });

    $('.list-box tr.list td.chk_id').click(function(e) {

        var obj = $(this).children('input.checkbox');

        if (!$(e.target).is('input')) {

            if (obj.is(':checked') == false) {

                obj.prop('checked', true);

            } else {

                obj.prop('checked', false);

            }

        }

    });

    if (check_auth && check_auth == 'full') {

        $('.list-box tr.list td.list div.view').click(function(e) {

            if (!$(e.target).is('.list-box tr.list td.list a.open')) {

                $(this).hide();
                $(this).parent().children('div.edit').show();
                $(this).parent().children('div.edit').children('input').focus();

            }

        });

    }

    $('.input_focus').focus(function() {

        $(this).addClass('focus');

    }).blur(function() {

        $(this).removeClass('focus');

    });

    $('.textarea_focus').focus(function() {

        $(this).addClass('focus');

    }).blur(function() {

        $(this).removeClass('focus');

    });

    $('.form-box input.onoff').each(function() {

        if ($(this).val() == 1 || $(this).val() == 2) {

            $(this).parent().children('span.onoff').addClass('on');
            $(this).parent().parent().find('td.onoffess').show();
            $(this).parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().find('.onoffmsg[name='+$(this).val()+']').show();
            $(this).parent().parent().parent().parent().parent().find('.onofflayer').show();

            if ($(this).val() == 2) {

                $(this).parent().parent().find('td.onoffess .checkbox').prop('checked', true);

            } else {

                $(this).parent().parent().find('td.onoffess .checkbox').prop('checked', false);

            }

            if ($(this).attr('rel') == 'layer') {

                $('.'+$(this).attr('name')).show();

            }

        } else {

            $(this).parent().children('span.onoff').removeClass('on');
            $(this).parent().parent().find('td.onoffess').hide();
            $(this).parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().find('.onoffmsg[name=0]').show();
            $(this).parent().parent().parent().parent().parent().find('.onofflayer').hide();

            if ($(this).attr('rel') == 'layer') {

                $('.'+$(this).attr('name')).hide();

            }

        }

    });

    $('.form-box span.onoff').click(function() {

        var obj = $(this).parent().children('input.onoff');

        if (obj.val() == 1 || obj.val() == 2) {

            $(this).removeClass('on');
            obj.val('0');

            $(this).parent().parent().find('td.onoffess .checkbox').prop('checked', false);
            $(this).parent().parent().find('td.onoffess').hide();
            $(this).parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().find('.onoffmsg[name=0]').show();
            $(this).parent().parent().parent().parent().parent().find('.onofflayer').hide();

            if (obj.attr('rel') == 'layer') {

                $('.'+obj.attr('name')).hide();

            }

        } else {

            $(this).addClass('on');
            obj.val('1');
            $(this).parent().parent().find('td.onoffess').show();
            $(this).parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().find('.onoffmsg[name='+obj.val()+']').show();
            $(this).parent().parent().parent().parent().parent().find('.onofflayer').show();

            if (obj.attr('rel') == 'layer') {

                $('.'+obj.attr('name')).show();

            }

        }

        winResize();

    });

    $('.form-box td.onoffess .checkbox').click(function() {

        var obj = $(this).parent().parent().parent().parent().parent().parent().find('input.onoff');

        if ($(this).is(':checked') == true) {

            obj.val('2');

            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[name=2]').show();

        } else {

            obj.val('1');
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[name=1]').show();

        }

    });

    $('.socialload').click(function() {

        socialLoad($(this).attr('rel'));

    });

    $(document).click(function(e) {

        if (!$(e.target).is('.selectbox *')) {

            $('.selectbox').removeClass('opt');

        }

        if (!$(e.target).is('.list-box tr.title td.title *')) {

            $('.list-box tr.title td.title .arrow .sbox').hide();

        }

        if (!$(e.target).is('.nav ul li.level *')) {

            $('.nav ul li.level .authbox').hide();

        }

    });

    winResize();
    $(window).resize(function() { winResize(); });

});

function checkAuthFull()
{

    if (check_auth != 'full') {

        alert("사용 권한이 없습니다. 관리자에게 문의하세요.");
        return false;

    }

    return true;

}

function winResize()
{

    var leftmenu = $('.layout-leftmenu').height();
    var contents = $('.layout-contents').height();

    $('.layout-contents').css( { 'height' : window.innerHeight+'px' } );

    if (window.innerHeight < $(document).height()) {

        $('.layout-contents').css( { 'height' : window.innerHeight+'px' } );

    }

    if (leftmenu < contents) {

        if (contents < $(document).height()) {

            $('.layout-leftmenu').css( { 'height' : $(document).height()+'px' } );

        } else {

            $('.layout-leftmenu').css( { 'height' : contents+'px' } );

        }

    } else {

        $('.layout-leftmenu').css( { 'height' : contents+'px' } );
        $('.layout-leftmenu').css( { 'height' : $(document).height()+'px' } );

    }

}

function selectboxValue()
{

    $('.selectbox .add').each(function() {

        var addtext = $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').text();

        if (addtext) {

            $(this).parent().parent().children('span.text').text(addtext);

        } else {

            $(this).parent().parent().children('span.text').html('&nbsp;');

        }

    });

}

function submitNavSearch()
{

    var f = document.formNavSearch;

    if (f.q.value == ''){

        alert("검색어를 입력하세요.");
        f.q.focus();
        return false;

    }

    return true;

}

function searchFocus(obj, m, text)
{

    if (m == 'in') {

        if (obj.value == text) {
            obj.value = "";
        }

        $(obj).parent().parent('div.selectbox').addClass('on');

    } else {

        if (obj.value == '') {
            obj.value = text;
        }

        $(obj).parent().parent('div.selectbox').removeClass('on');

    }

}

function listSearch(mode)
{

    var f = document.formListSearch;

    if (f.q.value == dq && dq) {
        f.q.value = '';
    }

    if (mode == 'sort') {

        f.submit();

    }

}

function listSort(id)
{

    var f = document.formListSearch;

    if (f.order.value == id) {

        if (f.sort.value == 'desc') {

            f.sort.value = "asc";

        } else {

            f.sort.value = "desc";

        }

    } else {

        f.sort.value = "desc";
        f.order.value = id;

    }

    listSearch('sort');

}

function checkAll(mode)
{

    $('.form_list .chk_id input.checkbox').prop('checked', mode);

}

function checkConfirm(msg)
{

    var n = $('.form_list .chk_id input.checkbox:checked').length;

    if (n <= 0) {

        alert(msg + "할 데이터를 선택하세요.");
        return false;

    }

    return true;

}

function reportMove()
{

    var id = $('#report_move').val();

    if (!id) {
        return false;
    }

    var url = "";

    if (id == 1) {
        url = "report.php";
    }
    else if (id == 10) {
        url = "report_visit_date.php";
    }
    else if (id == 11) {
        url = "report_visit_month.php";
    }
    else if (id == 12) {
        url = "report_visit_week.php";
    }
    else if (id == 13) {
        url = "report_visit_time.php";
    }
    else if (id == 14) {
        url = "report_visit_browser.php";
    }
    else if (id == 15) {
        url = "report_visit_os.php";
    }
    else if (id == 16) {
        url = "report_visit_keyword.php";
    }
    else if (id == 17) {
        url = "report_visit_domain.php";
    }
    else if (id == 18) {
        url = "report_visit_ip.php";
    }
    else if (id == 71) {
        url = "report_page_date.php";
    }
    else if (id == 72) {
        url = "report_page_month.php";
    }
    else if (id == 73) {
        url = "report_page_top.php";
    }
    else if (id == 21) {
        url = "report_member_date.php";
    }
    else if (id == 22) {
        url = "report_member_month.php";
    }
    else if (id == 23) {
        url = "report_member_age.php";
    }
    else if (id == 24) {
        url = "report_member_sex.php";
    }
    else if (id == 25) {
        url = "report_member_addr.php";
    }
    else if (id == 26) {
        url = "report_member_recommend.php";
    }
    else if (id == 27) {
        url = "report_login_date.php";
    }
    else if (id == 28) {
        url = "report_login_month.php";
    }
    else if (id == 31) {
        url = "report_member_single1.php";
    }
    else if (id == 32) {
        url = "report_member_single2.php";
    }
    else if (id == 33) {
        url = "report_member_single3.php";
    }
    else if (id == 41) {
        url = "report_member_multi1.php";
    }
    else if (id == 42) {
        url = "report_member_multi2.php";
    }
    else if (id == 43) {
        url = "report_member_multi3.php";
    }
    else if (id == 51) {
        url = "report_article_date.php";
    }
    else if (id == 52) {
        url = "report_article_month.php";
    }
    else if (id == 61) {
        url = "report_reply_date.php";
    }
    else if (id == 62) {
        url = "report_reply_month.php";
    }

    location.href = url;

}

function socialBox(mode)
{

    var overlay = $("#overlay");
    var box = $("#socialbox");

    if (mode == 'open') {

        box.show();

        var win = $(window);
        var boxLeft = (win.scrollLeft() + (win.width() / 2)) - (box.width() / 2);
        var boxTop = (win.scrollTop() + (win.height() / 2)) - (box.height() / 2);

        overlay.css( { 'opacity' : '0.5'} );
        overlay.fadeIn('300');

        box.css( { 'left': boxLeft+'px', 'top': boxTop+'px', 'opacity' : '1.0' } );

        $(document).keydown(function (e) {

            if ($("#socialbox").is(":hidden") == false) {

                if (e.keyCode == '27') {

                    socialBox('close');

                }

            }

        });

        overlay.click(function() {

            socialBox('close');

        });

    } else {

        box.html("");
        box.hide();
        overlay.hide();

    }

}

function socialLoad(mid)
{

    $('#socialbox').draggable();

    $.post("_social.php", {"mid" : mid}, function(data) {

        $("#socialbox").html(data);
        socialBox('open');

    });

}

function pointAdditionOpen()
{

    win_open("member_point_addition.php","pointAdditionOpen","width=620, height=670, scrollbars=yes");

}

function pointSubtractionOpen()
{

    win_open("member_point_subtraction.php","pointSubtractionOpen","width=620, height=670, scrollbars=yes");

}

function messageSendOpen()
{

    win_open("message_send.php","messageSendOpen","width=620, height=800, scrollbars=yes");

}

function messageViewOpen(id)
{

    win_open("message_view.php?id="+id,"messageViewOpen","width=620, height=520, scrollbars=yes");

}

function smsSendOpen()
{

    win_open("sms_send.php","smsSendOpen","width=620, height=800, scrollbars=yes");

}

function emailSendOpen()
{

    win_open("email_send.php","emailSendOpen","width=940, height=800, scrollbars=yes");

}

function emailViewOpen(id)
{

    win_open("email_view.php?id="+id,"emailViewOpen","width=940, height=800, scrollbars=yes");

}
