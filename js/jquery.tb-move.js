(function ($) {
$.fn.tbMove = function(o) {

    var obj = $(this);

    o = $.extend({
        num: 0,
        speed: 300,
        time: 2500,
        type: 'left',
    }, o);

    o.count = obj.find('.block').length - 1;

    if (o.count > 0) {

        var html = "<div class='block'>"+obj.find('.block').eq((obj.find('.block').length-1)).html()+"</div>";
        html += obj.find('.move').html();
        html += "<div class='block'>"+obj.find('.block').eq(0).html()+"</div>";

        obj.find('.move').html(html);

    } else {

        obj.find('.move').show();

        return false;

    }

    o.end = obj.find('.block').length - 1;
    o.width = obj.width();

    if (o.type == 'left') {

        obj.find('.move').css({ 'left': -(o.width * (o.num + 1))+'px' });

    } else {

        obj.find('.move').css({ 'left': -(o.width * ((o.end - o.num) - 1))+'px' });

    }

    obj.find('.move').show();

    var play = function(type, n) {

        if (type == 'left') {

            obj.find('.move').stop().animate({ 'left': '-'+(o.width * n)+'px' }, o.speed, 'linear', function() {

                if (n == o.end) {

                    obj.find('.move').css({ 'left': -(o.width)+'px' });

                }

            });

        } else {

            obj.find('.move').stop().animate({ 'left': '-'+(o.width * n)+'px' }, o.speed, 'linear', function() {

                if (n == 0) {

                    obj.find('.move').css({ 'left': -(o.width * (o.end - 1))+'px' });

                }

            });

        }

        o.num = n;

    };

    var auto = function(type) {

        if (!type) {
            type = o.type;
        }

        if (type == 'left') {

            if (o.num == o.end) {

                o.num = 2;

            } else {

                o.num = o.num + 1;

            }

        } else {

            if (o.num == 0) {

                o.num = o.end - 2;

            } else {

                o.num = o.num - 1;

            }


        }

        play(type, o.num);

    };

    var interval;
    interval = setInterval(auto, o.time);

    obj.mouseenter(function() { clearInterval(interval); }).mouseleave(function(){ interval = setInterval(auto, o.time); });

};
})(jQuery);