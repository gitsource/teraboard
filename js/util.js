// 입력할 때 자동으로 다음 input 으로 이동한다.
var next_go = true;
var cur_val = null;

function moveNext(id_from, id_to, maxSize)
{

    var cur = document.getElementById(id_from).value;

    curSize = cur.length;
    numFlag = isNumeric(cur);

    if (!numFlag && curSize >= 1 && cur != '00' &&  cur != '000') {

        alert('숫자를 넣어주세요');

        document.getElementById(id_from).value='';
        document.getElementById(id_from).focus();

        return false;

    }

    if (curSize == maxSize) {

        if (next_go || cur_val != cur) {

            cur_val = cur;
            next_go = false;

            document.getElementById(id_to).focus();

        }

        return true;

    }

    next_go = true;

}

function containsCharsOnly(input, chars)
{

    for (var i=0; i< input.length; i++) {

        if (chars.indexOf(input.charAt(i)) == -1) {

            return false;

        }

  }

  return  true;

}

function isNumeric(input)
{

    var chars = "0123456789";

    return containsCharsOnly(input,chars);

}

function isMoney(input)
{

    var chars = "0123456789-,.";

    return containsCharsOnly(input,chars);

}

function isValid_name(str)
{
    
    str = str.replace(/(^\s*)|(\s*$)/g, "");

    if (str == ''){

        alert("성명을 입력하세요.");
        return false;

    }
    
    var retVal = checkSpace(str);

    if (retVal){

        alert("성명은 띄어쓰기 없이 입력하세요.");
        return false;

    }

    if (!isHangul(str)) {

        alert("성명을 한글로 입력하세요.");
        return false;

    }

    if (str.length > 20) {

        alert("성명은 20자까지만 사용할 수 있습니다.");
        return false;

    }

    return true;

}

function isValid_nick(str)
{

    str = str.replace(/(^\s*)|(\s*$)/g, "");

    if (str == '') {

        alert("별명을 입력하세요.");
        return false;

    }
    
/*
    var retVal = checkSpace(str);

    if (retVal) {

        alert("별명은 띄어쓰기 없이 입력하세요.");
        return false;

    }
*/

    // 허용 문자를 체크한다.
    var isNick = /^[A-Za-z0-9_가-힣\x20]{1,10}$/;
    if( !isNick.test(str) ) {

        alert("별명은 한글, 영문, 숫자 그리고 1~10자까지만 사용할 수 있습니다.");
        return false;

    }

    if (str.length > 10) {

        alert("별명은 10자까지만 사용할 수 있습니다.");
        return false;

    }

    return true;

}

function isValid_uid(str)
{
     // check whether input value is included space or not
     if( str == ""){
     	alert("아이디를 입력하세요.");
     	return false;
     }

	// 아이디 가운데 빈 공간이 없도록 체크한다.
     var retVal = checkSpace( str );
     if( retVal ) {
         alert("아이디는 빈 공간 없이 연속된 영문 소문자와 숫자만 사용할 수 있습니다.");
         return false;
     }

     // 아이디는 '-' 로 시작할 수 없다.
	if( str.charAt(0) == '_') {
		alert("아이디의 첫문자는 '_'로 시작할수 없습니다.");
		return false;
	}

     // 길이와 허용 문자를 체크한다.
     var isID = /^[a-z0-9_]{3,12}$/;
     if( !isID.test(str) ) {
         alert("아이디는 3~12자의 영문 소문자와 숫자,특수기호(_)만 사용할 수 있습니다.");
         return false;
     }

	 var isNum = /\d/;
     var i;
     var cnt = 0;
     for( i=0; i < str.length; i++) {
     	if( isNum.test( str.substring( i, i+1 ) ) ) {
     		cnt++;
     	}
     	if( cnt > 7 ) {
     		alert("같은 문자가 7개 이상 사용되면 안됩니다.");
     		return false;
     	}
     }

     return true;
}

function isValid_passwd(str)
{
     var cnt = 0;
     if( str == ""){
     	alert("비밀번호를 입력하세요.");
     	return false;
     }

    /* check whether input value is included space or not  */
     var retVal = checkSpace( str );
     if( retVal ) {
         alert("비밀번호에는 공백이 있으면 안됩니다.");
         return false;
     }
			if( str.length < 6 ){
				alert("비밀번호는 6~16자의 영문 대소문자와 숫자, 특수문자를 사용할 수 있습니다.");
				return false;
			}
     for( var i=0; i < str.length; ++i)
     {
         if( str.charAt(0) == str.substring( i, i+1 ) ) ++cnt;
     }
     if( cnt == str.length ) {
         alert("보안상의 이유로 한 문자로 연속된 비밀번호는 허용하지 않습니다.");
         return false;
     }

     var isPW = /^[A-Za-z0-9`\-=\\\[\];',\./~!@#\$%\^&\*\(\)_\+|\{\}:"<>\?]{6,16}$/;
     if( !isPW.test(str) ) {
         alert("비밀번호는 6~16자의 영문 대소문자와 숫자, 특수문자를 사용할 수 있습니다.");
         return false;
     }
     return true;
}

function isValid_email(str)
{

    /* check whether input value is included space or not  */
    if (str == "") {
    
        alert("이메일 주소를 입력하세요.");
        return false;
    
    }

    var retVal = checkSpace( str );

    if (retVal) {

        alert("이메일 주소를 빈공간 없이 넣으세요.");
        return false;

    }
    
    if (-1 == str.indexOf('.')) {

        alert("이메일 형식이 잘못 되었습니다.");
        return false;

    }
    
    /* checkFormat */
    var isEmail = /[-!#$%&'*+\/^_~{}|0-9a-zA-Z]+(\.[-!#$%&'*+\/^_~{}|0-9a-zA-Z]+)*@[-!#$%&'*+\/^_~{}|0-9a-zA-Z]+(\.[-!#$%&'*+\/^_~{}|0-9a-zA-Z]+)*/;

    if (!isEmail.test(str)) {

        alert("이메일 형식이 잘못 되었습니다.");
        return false;

    }

    if (str.length > 60) {

        alert("이메일 주소는 60자까지 유효합니다.");
        return false;

    }
    
    return true;

}

function isValid_email2(word){

    for (var i=0; i< word.length; i++) {

        var checkStr = word.charAt(i);
        
        if ("@" == checkStr) {
    
            return false;
    
        }

    }

    return true;

}

function isValid_Date(val)
{

    if (val == '0000-00-00') {
        return true;
    }

    if (val.length != 10) {

        alert("날짜형식이 올바르지 않습니다.\n예) 0000-00-00");
        return false;

    }

    try{
        $.datepicker.parseDate('yy-mm-dd', val, null);
        return true;
    }
    catch(error){
        return false;
    }

}

function checkSpace(str)
{

    if (str.search(/\s/) != -1) {

        return true;

    } else {

        return false;

    }

}

function isHangul(s)
{

     var len;

     len = s.length;

     for (var i = 0; i < len; i++)  {

         if (s.charCodeAt(i) != 32 && (s.charCodeAt(i) < 44032 || s.charCodeAt(i) > 55203)) {

         		return false;

        }

     }

     return true;

}
