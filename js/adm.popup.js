/* 팝업용 */
var zindex = 20000;
$(document).ready( function() {

    $('.select .block span').click(function() {

        if ($(this).parent().children('div.option').is(':hidden') == true) {

            zindex++;

            $(this).parent().parent().parent('div.selectbox').addClass('opt').zIndex(zindex);

        } else {

            $(this).parent().parent().parent('div.selectbox').removeClass('opt');

        }

    });

    $('.select .block .option ul.array li').click(function() {

        // new
        if ($(this).is('[data-value]')) {

            $(this).parent().parent().children('input.add').val($(this).attr('data-value'));
            $(this).parent().parent().parent().children('span.text').text($(this).text());
            $(this).parent().parent().parent().parent().parent('div.selectbox').removeClass('opt');
            $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg[data-value="'+$(this).attr('data-value')+'"]').show();

        } else {

            $(this).parent().parent().children('input.add').val($(this).attr('name'));
            $(this).parent().parent().parent().children('span.text').text($(this).text());
            $(this).parent().parent().parent().parent().parent('div.selectbox').removeClass('opt');
            $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().parent().find('.onoffmsg[name="'+$(this).attr('name')+'"]').show();

        }

        var act = $(this).parent().parent().parent().parent().attr('rel');

        if (act == 'listSearch') {

            listSearch('sort');

        }

        if (act == 'reportMove') {

            reportMove();

        }

    });

    $('.selectbox .add').each(function() {

        // new
        if ($(this).parent().children('ul.array').find('li').is('[data-value]')) {

            var addtext = $(this).parent().children('ul.array').children('li[data-value="'+$(this).val()+'"]').text();

            if (addtext) {

                $(this).parent().children('ul.array').children('li[data-value="'+$(this).val()+'"]').addClass('on');
                $(this).parent().parent().children('span.text').text(addtext);
                $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
                $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[data-value="'+$(this).val()+'"]').show();

            } else {

                $(this).parent().parent().children('span.text').html('&nbsp;');

            }

        } else {
        // old

            var addtext = $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').text();

            if (addtext) {

                $(this).parent().children('ul.array').children('li[name="'+$(this).val()+'"]').addClass('on');
                $(this).parent().parent().children('span.text').text(addtext);
                $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
                $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[name="'+$(this).val()+'"]').show();

            } else {

                $(this).parent().parent().children('span.text').html('&nbsp;');

            }

        }

    });

    $('.input_focus').focus(function() {

        $(this).addClass('focus');

    }).blur(function() {

        $(this).removeClass('focus');

    });

    $('.textarea_focus').focus(function() {

        $(this).addClass('focus');

    }).blur(function() {

        $(this).removeClass('focus');

    });

    $('.form-box input.onoff').each(function() {

        if ($(this).val() == 1 || $(this).val() == 2) {

            $(this).parent().children('span.onoff').addClass('on');
            $(this).parent().parent().find('td.onoffess').show();
            $(this).parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().find('.onoffmsg[name='+$(this).val()+']').show();
            $(this).parent().parent().parent().parent().parent().find('.onofflayer').show();

            if ($(this).val() == 2) {

                $(this).parent().parent().find('td.onoffess .checkbox').prop('checked', true);

            } else {

                $(this).parent().parent().find('td.onoffess .checkbox').prop('checked', false);

            }

        } else {

            $(this).parent().children('span.onoff').removeClass('on');
            $(this).parent().parent().find('td.onoffess').hide();
            $(this).parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().find('.onoffmsg[name=0]').show();
            $(this).parent().parent().parent().parent().parent().find('.onofflayer').hide();

        }

    });

    $('.form-box span.onoff').click(function() {

        var obj = $(this).parent().children('input.onoff');

        if (obj.val() == 1 || obj.val() == 2) {

            $(this).removeClass('on');
            obj.val('0');

            $(this).parent().parent().find('td.onoffess .checkbox').prop('checked', false);
            $(this).parent().parent().find('td.onoffess').hide();
            $(this).parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().find('.onoffmsg[name=0]').show();
            $(this).parent().parent().parent().parent().parent().find('.onofflayer').hide();

        } else {

            $(this).addClass('on');
            obj.val('1');
            $(this).parent().parent().find('td.onoffess').show();
            $(this).parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().find('.onoffmsg[name='+obj.val()+']').show();
            $(this).parent().parent().parent().parent().parent().find('.onofflayer').show();

        }

    });

    $('.form-box td.onoffess .checkbox').click(function() {

        var obj = $(this).parent().parent().parent().parent().parent().parent().find('input.onoff');

        if ($(this).is(':checked') == true) {

            obj.val('2');

            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[name=2]').show();

        } else {

            obj.val('1');
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg').hide();
            $(this).parent().parent().parent().parent().parent().parent().find('.onoffmsg[name=1]').show();

        }

    });

    $(document).click(function(e) {

        if (!$(e.target).is('.selectbox *')) {

            $('.selectbox').removeClass('opt');

        }

    });

});