<?php
include_once("./_tb.php");

$sFileInfo = '';
$headers = array();
foreach($_SERVER as $k => $v) {

    if(substr($k, 0, 9) == "HTTP_FILE") {

        $k = substr(strtolower($k), 5);
        $headers[$k] = $v;

    }

}

$file = new stdClass;
$file->name = str_replace("\0", "", rawurldecode($headers['file_name']));
$file->size = $headers['file_size'];
$file->content = file_get_contents("php://input");

$filename_ext = strtolower(array_pop(explode('.',$file->name)));
$allow_file = array("jpg", "png", "gif");

if (!in_array($filename_ext, $allow_file)) {

    echo "NOTALLOW_";

} else {

    $dir = $disk['path']."/editor/".data_path("", "");

    if (!is_dir($dir)){

        @mkdir($dir, 0707);

    }

    $filename = $file->name;
    $filename = preg_replace("/\.(php|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $filename);
    $filename = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr(md5(uniqid($web['server_time'])),0,8).'_'.str_replace('%', '', urlencode(substr($filename,-10))); 

    $dest_file = $dir."/".$filename;
    $dest_file2 = $dir."/thumb_".$filename;

    if (file_put_contents($dest_file, $file->content)) {

        @chmod($dest_file, 0606);

        $upload['image'] = @getimagesize($dest_file);

        if ($upload['image'][2] != 1 && $upload['image'][2] != 2 && $upload['image'][2] != 3) {

            @unlink($dest_file);

            echo "NOTALLOW_";

            exit;

        }

        $sFileInfo .= "&bNewLine=true";
        $sFileInfo .= "&sFileName=".text($file->name);

        if (preg_match("/\.(jp[e]?g|png)$/i", $dest_file) && $upload['image'][0] > $setup['image_scaling']) {

            image_resize($setup['image_scaling'], $setup['image_scaling'], $dest_file, $dest_file2, 100);

            $upload['image'] = @getimagesize($dest_file2);

            @unlink($dest_file);

            $sFileInfo .= "&sFileURL=".$disk['server_editor']."/".data_path("u", $web['time_ymd'])."/thumb_".$filename;
            $sFileInfo .= "&sFileWidth=".$upload['image'][0];
            $sFileInfo .= "&sFileHeight=".$upload['image'][1];

        } else {

            $sFileInfo .= "&sFileURL=".$disk['server_editor']."/".data_path("u", $web['time_ymd'])."/".$filename;
            $sFileInfo .= "&sFileWidth=".$upload['image'][0];
            $sFileInfo .= "&sFileHeight=".$upload['image'][1];

        }

    }

    echo addslashes($sFileInfo);

}
?>