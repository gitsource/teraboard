<?php
include_once("./_tb.php");

$url = $_REQUEST["callback"].'?callback_func='.$_REQUEST["callback_func"];
$bSuccessUpload = is_uploaded_file($_FILES['Filedata']['tmp_name']);

if ($bSuccessUpload) {

    $tmp_name = $_FILES['Filedata']['tmp_name'];
    $name = $_FILES['Filedata']['name'];

    $filename_ext = strtolower(array_pop(explode('.',$name)));
    $allow_file = array("jpg", "png", "bmp", "gif");

    if (!in_array($filename_ext, $allow_file)) {

        $url .= '&errstr=error';

    } else {

        $dir = $disk['path']."/editor/".data_path("", "");

        if (!is_dir($dir)) {

            @mkdir($dir, 0707);

        }

        $filename = $name;
        $filename = preg_replace("/\.(php|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $filename);
        $filename = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr(md5(uniqid($web['server_time'])),0,8).'_'.str_replace('%', '', urlencode(substr($filename,-10))); 

        $dest_file = $dir."/".$filename;
        $dest_file2 = $dir."/thumb_".$filename;

        @move_uploaded_file($tmp_name, $dest_file);
        @chmod($dest_file, 0606);

        $upload = array();
        $upload['image'] = @getimagesize($dest_file);

        if ($upload['image'][2] != 1 && $upload['image'][2] != 2 && $upload['image'][2] != 3) {

            @unlink($dest_file);

            $url .= '&errstr=error';

        } else {

            $url .= "&bNewLine=true";
            $url .= "&sFileName=";

            if (preg_match("/\.(jp[e]?g|png)$/i", $dest_file) && $upload['image'][0] > $setup['image_scaling']) {

                image_resize($setup['image_scaling'], $setup['image_scaling'], $dest_file, $dest_file2, 100);

                $upload['image'] = @getimagesize($dest_file2);

                @unlink($dest_file);

                $url .= "&sFileURL=".$disk['server_editor']."/".data_path("u", $web['time_ymd'])."/thumb_".$filename;
                $url .= "&sFileWidth=".$upload['image'][0];
                $url .= "&sFileHeight=".$upload['image'][1];

            } else {

                $url .= "&sFileURL=".$disk['server_editor']."/".data_path("u", $web['time_ymd'])."/".$filename;
                $url .= "&sFileWidth=".$upload['image'][0];
                $url .= "&sFileHeight=".$upload['image'][1];

            }

        }

    }

} else {

    $url .= '&errstr=error';

}

header('Location: '. addslashes($url));
?>