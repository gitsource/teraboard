<?php // 게시판
if (!defined("_WEB_")) exit;

$sql_search = " where bbs_group = '".addslashes($group_id)."' and bbs_search = 1 and INSTR(q, '".addslashes($qq)."') ";

$cnt = sql_fetch(" select count(id) as cnt from $web[search_table] $sql_search ");

$search_count += $cnt['cnt'];

if ($t == '') {

    $result = sql_query(" select * from $web[search_table] $sql_search order by id desc limit 0, $rows ");

} else {

    $total_count = $cnt['cnt'];
    $total_page  = ceil($total_count / $rows);
    if (!$p) { $p = 1; }
    $from_record = ($p - 1) * $rows;
    $paging = paging(5, $p, $total_page, "?t=".$group_id."&amp;q=$q&amp;p=");
    $result = sql_query(" select * from $web[search_table] $sql_search order by id desc limit $from_record, $rows ");

}

$list = array();
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);
    $bbs = bbs($row['bbs_id']);

    $subject = str_replace("&nbsp;", "", stripslashes($article['ar_title']));
    $subject = str_replace("\r", "", $subject);
    $subject = str_replace("\n", " ", $subject);
    $subject = text_cut($subject,255,'...');
    $subject = str_replace($q, "<b>$q</b>", $subject);

    $content = str_replace("&nbsp;", "", stripslashes($article['ar_content']));
    $content = str_replace("\r", "", $content);
    $content = str_replace("\n", " ", $content);
    $content = text_cut($content, 300, '...');
    $content = str_replace($q, "<b>$q</b>", $content);

    $thumb_width = 80;
    $thumb_height = 80;

    $dir = $disk['path']."/thumb/".$row['bbs_id'];

    @mkdir($dir, 0707);
    @chmod($dir, 0707);

    $thumb_file = "";
    $thumb = "";

    $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($row['bbs_id'])."' and article_id = '".$row['article_id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

    if ($file['upload_file']) {

        $thumb_file = $disk['path']."/bbs/".$row['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
        $thumb_path = $dir."/".data_path("u", $file['upload_time']);
        $thumb = $thumb_path."/thumb".$thumb_width."x".$thumb_height."_".$file['upload_file'];

        if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

            @mkdir($thumb_path, 0707);
            @chmod($thumb_path, 0707);

            image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, false);

        }

        $thumb = str_replace($disk['path'], $disk['server'], $thumb);

    }

    if (!$thumb) {

        $thumb = bbs_thumb_create($row['bbs_id'], $article['datetime'], $article['ar_content'], $thumb_width, $thumb_height, false);
        $thumb_file = image_editor($article['ar_content']);

    }

    $list[$i]['thumb'] = "";

    if ($thumb) {

        $list[$i]['thumb'] = $thumb;

    }

    $list[$i]['title'] = $subject;
    $list[$i]['content'] = $content;
    $list[$i]['datetime'] = substr($row['datetime'],0,16);
    $list[$i]['source'] = http_bbs($row['bbs_id'], "");
    $list[$i]['bbs_title'] = $bbs['bbs_title'];

    if ($list[$i]['ar_adult']) {

        $list[$i]['content'] = "성인 게시물입니다.";
        $list[$i]['thumb'] = "";

    }

    if ($article['ar_secret']) {

        $list[$i]['content'] = "비밀 게시물입니다.";
        $list[$i]['thumb'] = "";

    }

}

if (count($list)) {
?>
<div class="box">
<p class="group_title"><?=text($group_title)?><? if ($t == '') { ?><a href="?t=<?=text($group_id)?>&amp;q=<?=text($q)?>">More</a><? } ?></p>
<? for ($i=0; $i<count($list); $i++) { ?>
<a href="<?=$list[$i]['href']?>" class="array<? if ($list[$i]['thumb']) { echo " thumb"; } ?>">
<? if ($list[$i]['thumb']) { ?>
<div class="img"><span><img src="<?=$list[$i]['thumb']?>" alt=""></span></div>
<? } ?>
<p class="title"><?=$list[$i]['title']?></p>
<p class="content"><?=$list[$i]['content']?></p>
<p class="source"><?=text($list[$i]['bbs_title'])?></p>
</a>
<? } ?>
<? if ($t != '' && $total_count && $total_count > $rows) { ?>
<div class="web-page"><?=$paging?></div>
<? } ?>
</div>
<?
}
?>