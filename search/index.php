<?php // 검색
include_once("./_tb.php");
if ($t) { $t = preg_match("/^[A-Za-z0-9]+$/", $t) ? $t : ""; }

$bbs_group = array();
$result = sql_query(" select * from $web[bbs_group_table] where bbs_group not in ('bottom') and bbs_group_onoff = 1 order by bbs_group_position asc, bbs_group_title asc ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $bbs_group[$i] = $row;

}

$paging = "";
$qq = "";

if (!$q) {
    $q = addslashes($setup['title']);
}

if ($q) {

    $qq = search_text(2, $q);

}

$total_count = 0;
$search_count = 0;

if ($t) {

    $rows = 10;

} else {

    $rows = 5;

}

// 페이지가 없거나 1보다 같거나 작을 때
if (!$p || $p <= 1) {

    $this_page_1 = 1; // 시작
    $this_page_2 = $rows; // 마지막

} else {

    $this_page_1 = ($p * $rows) - ($rows - 1); // 시작
    $this_page_2 = $p * $rows; // 마지막

}

// head start
ob_start();
?>
<link rel="stylesheet" href="search.css" type="text/css">
<? if ($t == 'image') { ?>
<style type="text/css">
.search-wrap {background-image:none;}
.search-line {background-image:none;}
.search-contents {margin-right:0; width:100%;}
.search-main {padding:0 5px;}
</style>
<? } ?>
<?
$web['head'] = ob_get_contents();
ob_end_clean();
// head end

$web['title'] = $q." : 통합검색";
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<?
include_once("$web[path]/_head.php");
?>
<script type="text/javascript">
var searchtab = "<?=text($t)?>";
$(document).ready( function() {

    if (searchtab) {

        $('.search-tab ul li[name="'+searchtab+'"] a').addClass('on');

    } else {

        $('.search-tab ul li[name="total"] a').addClass('on');

    }

    $('.search-tab .more').click(function() {

        $('.search-tab').addClass('on');

    });

    $('.search-tab .close').click(function() {

        $('.search-tab').removeClass('on');

    });

    $('.search-side .side-hit .tab .title').click(function() {

        searchHitTab($(this).attr('name'));

    });

    searchHitTab(1);

    var searchzoom = parseInt($.cookie('searchzoom'));

    if (!searchzoom) {
        searchzoom = 0;
    }

    var searchzoomUpdate = function(zoom) {

       $('.search-main .search_count').css({ 'font-size': parseInt($('.search-main .search_count').css("font-size")) + zoom+'px' });
       $('.search-main .group_title').css({ 'font-size': parseInt($('.search-main .group_title').css("font-size")) + zoom+'px' });
       $('.search-main .array .title').css({ 'font-size': parseInt($('.search-main .array .title').css("font-size")) + zoom+'px' });
       $('.search-main .array .content').css({ 'font-size': parseInt($('.search-main .array .content').css("font-size")) + zoom+'px' });
       $('.search-main .array .source').css({ 'font-size': parseInt($('.search-main .array .source').css("font-size")) + zoom+'px' });
       $('.search-main .image .title a').css({ 'font-size': parseInt($('.search-main .image .title a').css("font-size")) + zoom+'px' });
       $('.search-main .image .source a').css({ 'font-size': parseInt($('.search-main .image .source a').css("font-size")) + zoom+'px' });

       $('.search-side .side_title').css({ 'font-size': parseInt($('.search-side .side_title').css("font-size")) + zoom+'px' });
       $('.search-side .side-good a span.text').css({ 'font-size': parseInt($('.search-side .side-good a span.text').css("font-size")) + zoom+'px' });
       $('.search-side .side-hit .layer a .text').css({ 'font-size': parseInt($('.search-side .side-hit .layer a .text').css("font-size")) + zoom+'px' });
       $('.search-side .side-new a .text').css({ 'font-size': parseInt($('.search-side .side-new a .text').css("font-size")) + zoom+'px' });

    }

    $('.search-nav .btn .zoom-in').click(function() {

        searchzoomUpdate(1);

        searchzoom++;
        $.cookie('searchzoom', searchzoom, { expires: 365, path: '/' });

    });

    $('.search-nav .btn .zoom-out').click(function() {

        searchzoomUpdate(-1);

        searchzoom--;

        $.cookie('searchzoom', searchzoom, { expires: 365, path: '/' });

    });

    if (searchzoom) {

        searchzoomUpdate(searchzoom);

    }

});

function searchHitTab(id)
{

    $('.search-side .side-hit .tab .title').removeClass('on');
    $('.search-side .side-hit .tab .title[name="'+id+'"]').addClass('on');
    $('.search-side .side-hit .layer').hide();
    $('.search-side .side-hit .layer[name="'+id+'"]').show();

}

function searchSubmit(id)
{

    if (id == 1) {

        var f = document.formSearch;

    } else {

        var f = document.formSearch2;

    }

    if (f.q.value == '') {

        alert("검색어를 입력하세요.");
        f.q.focus();
        return false;

    }

    return true;

}
</script>
</head>
<body>
<?
include_once("$web[path]/_body.php");
?>
<!-- search-nav start //-->
<form method="get" name="formSearch" onsubmit="return searchSubmit(1);" autocomplete="off">
<input type="hidden" name="t" value="<?=text($t)?>" />
<div class="search-nav">
<ul>
<li class="logo"><a href="<?=$web['host_default']?>/"><span>SEARCH</span></a></li>
<li class="search"><input type="text" name="q" value="<?=text($q)?>" class="input" /><input type="image" src="<?=$web['host_img']?>/blank.png" class="submit" /></li>
<li class="btn">
<a href="#" onclick="return false;" class="zoom-out"><span class="icon"></span>작게</a>
<a href="#" onclick="return false;" class="zoom-in"><span class="icon"></span>크게</a>
<? if ($check_login) { ?>
<a href="<?=$web['host_member']?>/" class="login">마이페이지</a>
<? } else { ?>
<a href="<?=$web['host_member']?>/login/?url=<?=text($urlencode)?>" class="login">로그인</a>
<? } ?>
</li>
</ul>
</div>
</form>
<!-- search-nav end //-->
<!-- search-tab start //-->
<div class="search-tab">
<div class="block">
<ul>
<li name="total"><a href="?q=<?=text($q)?>">통합검색</a></li>
<?
for ($i=0; $i<count($bbs_group); $i++) {

    echo "<li name='".text($bbs_group[$i]['bbs_group'])."'><a href='?t=".text($bbs_group[$i]['bbs_group'])."&amp;q=".text($q)."'>".text($bbs_group[$i]['bbs_group_title'])."</a></li>";

}

echo "<li name='image'><a href='?t=image&amp;q=".text($q)."'>이미지</a></li>";
?>
</ul>
<? if ((count($bbs_group)+1) > 4) { ?><div class="more"></div><? } ?>
</div>
<div class="close">메뉴 닫기</div>
</div>
<!-- search-tab end //-->
<!-- search-body start //-->
<div class="search-body">
<div class="search-wrap">
<div class="search-line">
<div class="search-contents">
<div class="search-main">
<?
// 키워드가 있을 경우 통과
if ($q && $q != '') {

    if (!$p) {

        $p = 1;

    }

    if ($t == '') {

        echo "<p class='search_count'><font color='#010101'>‘".text($q)."’</font>의 <font color='#176bae'>통합검색 결과</font>는 총 <span id='search_count' style='color:#ff3e1a;'>0</span>건 입니다.</p>";

    }

    if ($t == 'image') {

        include_once("./skin.image.php");

    }

    for ($n=0; $n<count($bbs_group); $n++) {

        $group_title = $bbs_group[$n]['bbs_group_title'];
        $group_id = $bbs_group[$n]['bbs_group'];

        if ($t == $group_id || $t == '') {

            include("./skin.group.php");

        }

    }

    echo "<script type=\"text/javascript\">";
    echo "$('#search_count').text('".number($search_count)."');";
    if (!$search_count) {
    echo "$('.search_count').hide();";
    }
    echo "</script>";

} // end if q

// 검색 결과가 없다.
if (!$q || !$search_count) {
?>
<div class="not">
<p class="img"><img src="img/no.png"></p>
<p class="msg">‘<?=text($q)?>’의 검색 결과가 없습니다.</p>
<div class="text">
<span class="dot"></span>검색 서비스는 제목과 내용에 포함된 단어를 기준으로 검색합니다.<br />
<span class="dot"></span>입력하신 키워드의 맞춤법 또는 철자가 올바른지 확인해 보세요.<br />
<span class="dot"></span>한글을 영어로 또는 영어를 한글로 입력했는지 확인해 보세요.<br />
<span class="dot"></span>특수문자 또는 띄어쓰기를 제거하고 다시 시도해보시기 바랍니다.<br />
</div>
</div>
<?
}
?>
</div>
</div>
<!-- search-side start //-->
<? if ($t != 'image') { ?>
<div class="search-side">
<div class="linetop"></div>
<div>
<!-- side-img start //-->
<?
$list = array();
$result = sql_query(" select * from $web[search_table] where bbs_search = 1 and ar_img in (1,2,3) and INSTR(q, '".addslashes($qq)."') and ar_adult = 0 order by id desc limit 0, 9 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);

    $thumb_width = 80;
    $thumb_height = 80;

    $dir = $disk['path']."/thumb/".$row['bbs_id'];

    @mkdir($dir, 0707);
    @chmod($dir, 0707);

    $thumb_file = "";
    $thumb = "";

    $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($row['bbs_id'])."' and article_id = '".$row['article_id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

    if ($file['upload_file']) {

        $thumb_file = $disk['path']."/bbs/".$row['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
        $thumb_path = $dir."/".data_path("u", $file['upload_time']);
        $thumb = $thumb_path."/thumb".$thumb_width."x".$thumb_height."_".$file['upload_file'];

        if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

            @mkdir($thumb_path, 0707);
            @chmod($thumb_path, 0707);

            image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, false);

        }

        $thumb = str_replace($disk['path'], $disk['server'], $thumb);

    }

    if (!$thumb) {

        $thumb = bbs_thumb_create($row['bbs_id'], $article['datetime'], $article['ar_content'], $thumb_width, $thumb_height, false);
        $thumb_file = image_editor($article['ar_content']);

    }

    $list[$i]['thumb'] = "";

    if ($thumb) {

        $list[$i]['thumb'] = $thumb;

    }

    if ($list[$i]['ar_adult']) {

        $list[$i]['thumb'] = "";

    }

}

if (count($list)) {
?>
<div class="sideline side-img">
<p class="side_title">‘<?=text_cut($q, 10, '...')?>’의 이미지<a href="?t=image&amp;q=<?=text($q)?>">More</a></p>
<ul>
<? for ($i=0; $i<count($list); $i++) { ?>
<li><a href="<?=$list[$i]['href']?>"><span><img src="<?=$list[$i]['thumb']?>" alt=""></span></a></li>
<? } ?>
</ul>
</div>
<? } ?>
<!-- side-img end //-->
<!-- side-good start //-->
<?
$list = array();
$result = sql_query(" select * from $web[search_table] where bbs_search = 1 and ar_good > 0 and INSTR(q, '".addslashes($qq)."') order by ar_good desc limit 0, 10 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);

    $subject = str_replace("&nbsp;", "", stripslashes($article['ar_title']));
    $subject = str_replace("\r", "", $subject);
    $subject = str_replace("\n", " ", $subject);
    $subject = text_cut($subject,255,'...');
    $subject = str_replace($q, "<b>$q</b>", $subject);

    $list[$i]['title'] = $subject;

}

if (count($list)) {
?>
<div class="sideline side-good">
<p class="side_title">‘<?=text_cut($q, 10, '...')?>’의 추천 검색결과</p>
<? for ($i=0; $i<count($list); $i++) { ?>
<a href="<?=$list[$i]['href']?>"><span class="num"><?=$i+1?></span><span class="text"><?=$list[$i]['title']?></span></a>
<? } ?>
</div>
<? } ?>
<!-- side-good end //-->
<!-- side-hit start //-->
<div class="sideline side-hit">
<p class="side_title">많이 본 콘텐츠 (종합)</p>
<div class="tab">
<span class="title" name="1">전체</span>
<span class="title" name="2">월간</span>
<span class="title" name="3">주간</span>
<span class="title" name="4">일간</span>
</div>
<?
echo "<div class='layer' name='1'>";
$list = array();
$result = sql_query(" select * from $web[search_table] where bbs_search = 1 order by ar_hit desc limit 0, 10 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);

    $subject = str_replace("&nbsp;", "", stripslashes($article['ar_title']));
    $subject = str_replace("\r", "", $subject);
    $subject = str_replace("\n", " ", $subject);
    $subject = text_cut($subject,255,'...');

    $list[$i]['title'] = $subject;

}

for ($i=0; $i<count($list); $i++) {

    echo "<a href='".$list[$i]['href']."'><span class='num'>".($i+1)."</span><span class='text'>".$list[$i]['title']."</span><span class='hit'>".$list[$i]['ar_hit']."</span></a>";

}

if (count($list) == 0) {
    echo "<span class='not'>검색 결과가 없습니다.</span>";
}

echo "</div>";

echo "<div class='layer' name='2'>";
$list = array();
$result = sql_query(" select * from $web[search_table] where bbs_search = 1 and datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 30))."' order by ar_hit desc limit 0, 10 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);

    $subject = str_replace("&nbsp;", "", stripslashes($article['ar_title']));
    $subject = str_replace("\r", "", $subject);
    $subject = str_replace("\n", " ", $subject);
    $subject = text_cut($subject,255,'...');

    $list[$i]['title'] = $subject;

}

for ($i=0; $i<count($list); $i++) {

    echo "<a href='".$list[$i]['href']."'><span class='num'>".($i+1)."</span><span class='text'>".$list[$i]['title']."</span><span class='hit'>".$list[$i]['ar_hit']."</span></a>";

}

if (count($list) == 0) {
    echo "<span class='not'>검색 결과가 없습니다.</span>";
}

echo "</div>";

echo "<div class='layer' name='3'>";
$list = array();
$result = sql_query(" select * from $web[search_table] where bbs_search = 1 and datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 7))."' order by ar_hit desc limit 0, 10 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);

    $subject = str_replace("&nbsp;", "", stripslashes($article['ar_title']));
    $subject = str_replace("\r", "", $subject);
    $subject = str_replace("\n", " ", $subject);
    $subject = text_cut($subject,255,'...');

    $list[$i]['title'] = $subject;

}

for ($i=0; $i<count($list); $i++) {

    echo "<a href='".$list[$i]['href']."'><span class='num'>".($i+1)."</span><span class='text'>".$list[$i]['title']."</span><span class='hit'>".$list[$i]['ar_hit']."</span></a>";

}

if (count($list) == 0) {
    echo "<span class='not'>검색 결과가 없습니다.</span>";
}

echo "</div>";

echo "<div class='layer' name='4'>";
$list = array();
$result = sql_query(" select * from $web[search_table] where bbs_search = 1 and datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (86400 * 1))."' order by ar_hit desc limit 0, 10 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);

    $subject = str_replace("&nbsp;", "", stripslashes($article['ar_title']));
    $subject = str_replace("\r", "", $subject);
    $subject = str_replace("\n", " ", $subject);
    $subject = text_cut($subject,255,'...');

    $list[$i]['title'] = $subject;

}

for ($i=0; $i<count($list); $i++) {

    echo "<a href='".$list[$i]['href']."'><span class='num'>".($i+1)."</span><span class='text'>".$list[$i]['title']."</span><span class='hit'>".$list[$i]['ar_hit']."</span></a>";

}

if (count($list) == 0) {
    echo "<span class='not'>검색 결과가 없습니다.</span>";
}

echo "</div>";
?>
</div>
<!-- side-hit end //-->
<!-- side-new start //-->
<div class="sideline side-new">
<p class="side_title">종합 최신 글</p>
<?
$list = array();
$result = sql_query(" select * from $web[search_table] where bbs_search = 1 order by id desc limit 0, 10 ");
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);

    $subject = str_replace("&nbsp;", "", stripslashes($article['ar_title']));
    $subject = str_replace("\r", "", $subject);
    $subject = str_replace("\n", " ", $subject);
    $subject = text_cut($subject,255,'...');

    $list[$i]['title'] = $subject;

    $list[$i]['date'] = date("m-d", strtotime($article['datetime']));

    if (date("Y-m-d", strtotime($article['datetime'])) == $web['time_ymd']) {

        $list[$i]['date'] = "<font color='#fb5151'>NEW</font>";

    }

}

for ($i=0; $i<count($list); $i++) {

    echo "<a href='".$list[$i]['href']."'><span class='num'>".($i+1)."</span><span class='text'>".$list[$i]['title']."</span><span class='date'>".$list[$i]['date']."</span></a>";

}

if (count($list) == 0) {
    echo "<span class='not'>검색 결과가 없습니다.</span>";
}
?>
</div>
<!-- side-new end //-->
</div>
</div>
<? } ?>
<!-- search-side end //-->
<div class="clr"></div>
</div>
</div>
</div>
<!-- search-body end //-->
<!-- search-bottom start //-->
<form method="get" name="formSearch2" onsubmit="return searchSubmit(2);" autocomplete="off">
<input type="hidden" name="t" value="<?=text($t)?>" />
<div class="search-bottom">
<ul>
<li class="search"><input type="text" name="q" value="<?=text($q)?>" class="input" /><input type="image" src="<?=$web['host_img']?>/blank.png" class="submit" /></li>
</ul>
</div>
</form>
<!-- search-bottom end //-->
<!-- search-copyright start //-->
<div class="search-copyright">
<div class="block">
<a href="<?=$web['host_default']?>/" class="logo"><img src="img/logo_copyright.png"></a>
<div class="copyright">Copyright © <span><?=text($setup['company'])?></span> Corp. All Rights Reserved.</div>
<div class="text">
<?
if ($setup['company']) {

    echo "<span>회사명 : ".text($setup['company'])."</span>";

}

if ($setup['number1']) {

    echo "<span>사업자등록번호 : ".text($setup['number1'])."</span>";

}

if ($setup['ceo']) {

    echo "<span>(대표자명 : ".text($setup['ceo']).")</span>";

}

if ($setup['company'] || $setup['number1'] || $setup['ceo']) {

    echo "<br class='pc' />";

}

if ($setup['ceo']) {

    echo "<span>통신판매업 신고번호 : ".text($setup['number2'])."</span>";

}

if ($setup['email']) {

    echo "<span>대표메일 : ".text($setup['email'])."</span>";

}

if ($setup['tel']) {

    echo "<span>대표번호 : ".text($setup['tel'])."</span>";

}

if ($setup['fax']) {

    echo "<span>(팩스 : ".text($setup['fax']).")</span>";

}

if ($setup['ceo'] || $setup['email'] || $setup['tel'] || $setup['fax']) {

    echo "<br class='pc' />";

}

if ($setup['addr']) {

    echo "<span>회사주소 : ".text($setup['addr'])."</span>";

}

if ($setup['privace_name']) {

    echo "<span>개인정보 책임자 : ".text($setup['privace_name'])."</span>";

}
?>
</div>
</div>
</div>
<!-- search-copyright end //-->
</body>
</html>