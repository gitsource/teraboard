<?php // 이미지
if (!defined("_WEB_")) exit;

$img_rows = 49;

$sql_search = " where bbs_search = 1 and ar_img in (1,2,3) and ar_adult = 0 and INSTR(q, '".addslashes($qq)."') ";

$cnt = sql_fetch(" select count(id) as cnt from $web[search_table] $sql_search ");

$search_count += $cnt['cnt'];

$total_count = $cnt['cnt'];
$total_page  = ceil($total_count / $img_rows);
if (!$p) { $p = 1; }
$from_record = ($p - 1) * $img_rows;
$paging = paging(5, $p, $total_page, "?t=image&amp;q=$q&amp;p=");
$result = sql_query(" select * from $web[search_table] $sql_search order by id desc limit $from_record, $img_rows ");

$list = array();
for ($i=0; $row=sql_fetch_array($result); $i++) {

    $list[$i] = $row;
    $list[$i]['href'] = http_bbs($row['bbs_id'], $row['article_id']);

    $article = article($row['bbs_id'], $row['article_id']);
    $bbs = bbs($row['bbs_id']);

    $subject = str_replace("&nbsp;", "", stripslashes($article['ar_title']));
    $subject = str_replace("\r", "", $subject);
    $subject = str_replace("\n", " ", $subject);
    $subject = text_cut($subject,255,'...');
    $subject = str_replace($q, "<b>$q</b>", $subject);

    $thumb_width = 80;
    $thumb_height = 80;

    $dir = $disk['path']."/thumb/".$row['bbs_id'];

    @mkdir($dir, 0707);
    @chmod($dir, 0707);

    $thumb_file = "";
    $thumb = "";

    $file = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".addslashes($row['bbs_id'])."' and article_id = '".$row['article_id']."' and upload_type in (1,2,3) order by number asc limit 0, 1 ");

    if ($file['upload_file']) {

        $thumb_file = $disk['path']."/bbs/".$row['bbs_id']."/".data_path("u", $file['upload_time'])."/".$file['upload_file'];
        $thumb_path = $dir."/".data_path("u", $file['upload_time']);
        $thumb = $thumb_path."/thumb".$thumb_width."x".$thumb_height."_".$file['upload_file'];

        if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

            @mkdir($thumb_path, 0707);
            @chmod($thumb_path, 0707);

            image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, false);

        }

        $thumb = str_replace($disk['path'], $disk['server'], $thumb);

    }

    if (!$thumb) {

        $thumb = bbs_thumb_create($row['bbs_id'], $article['datetime'], $article['ar_content'], $thumb_width, $thumb_height, false);
        $thumb_file = image_editor($article['ar_content']);

    }

    $list[$i]['thumb'] = "";

    if ($thumb) {

        $list[$i]['thumb'] = $thumb;

    }

    $list[$i]['title'] = $subject;
    $list[$i]['datetime'] = substr($row['datetime'],0,16);
    $list[$i]['source'] = http_bbs($row['bbs_id'], "");
    $list[$i]['bbs_title'] = $bbs['bbs_title'];

    if ($article['ar_adult']) {

        $list[$i]['title'] = "성인 게시물입니다.";
        $list[$i]['thumb'] = "";

    }

    if ($article['ar_secret']) {

        $list[$i]['title'] = "비밀 게시물입니다.";
        $list[$i]['thumb'] = "";

    }

}

if (count($list)) {
?>
<div class="box">
<p class="group_title">‘<?=text_cut($q, 10, '...')?>’의 이미지</p>
<div class="imgblock">
<? for ($i=0; $i<count($list); $i++) { ?>
<div class="image">
<? if ($list[$i]['thumb']) { ?>
<div class="img"><span><a href="<?=$list[$i]['href']?>"  target="_blank"><img src="<?=$list[$i]['thumb']?>" alt=""></a></span></div>
<? } ?>
<p class="title"><a href="<?=$list[$i]['href']?>"  target="_blank"><?=$list[$i]['title']?></a></p>
<p class="source"><a href="<?=$list[$i]['source']?>"  target="_blank"><?=text($list[$i]['bbs_title'])?></a></p>
</div>
<? } ?>
</div>
<? if ($t != '' && $total_count && $total_count > $img_rows) { ?>
<div class="web-page"><?=$paging?></div>
<? } ?>
</div>
<?
}
?>