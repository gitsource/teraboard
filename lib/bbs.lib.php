<?php // 게시판
if (!defined("_WEB_")) exit;

function http_bbs($bbs_id, $article_id)
{

    global $web;

    $url = $web['host_bbs'];
    if ($bbs_id) {
        $url .= "/".$bbs_id;
    }
    if ($bbs_id && $article_id) {
        $url .= "/".$article_id;
    }
    return $url;

}

function bbs($bbs_id)
{

    if ($bbs_id) { $bbs_id = preg_match("/^[a-zA-Z0-9_\-]+$/", $bbs_id) ? $bbs_id : ""; }

    global $web;

    if (!$bbs_id) {
        return false;
    }

    $data = sql_fetch(" select * from $web[bbs_table] where bbs_id = '".$bbs_id."' limit 0, 1 ");

    return $data;

}

function bbs_group($bbs_group)
{

    if ($bbs_group) { $bbs_group = preg_match("/^[a-zA-Z0-9_\-]+$/", $bbs_group) ? $bbs_group : ""; }

    global $web;

    if (!$bbs_group) {
        return false;
    }

    $data = sql_fetch(" select * from $web[bbs_group_table] where bbs_group = '".$bbs_group."' limit 0, 1 ");

    return $data;

}

function bbs_group_count()
{

    global $web;

    $result = sql_query(" select * from $web[bbs_group_table] ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $chk = sql_fetch(" select count(bbs_id) as cnt from $web[bbs_table] where bbs_group = '".addslashes($row['bbs_group'])."' ");

        $sql_common = "";
        $sql_common .= " set bbs_count = '".(int)($chk['cnt'])."' ";

        sql_query(" update $web[bbs_group_table] $sql_common where bbs_group = '".addslashes($row['bbs_group'])."' ");

    }

    return false;

}

function article($bbs_id, $article_id)
{

    if ($bbs_id) { $bbs_id = preg_match("/^[a-zA-Z0-9_\-]+$/", $bbs_id) ? $bbs_id : ""; }
    if ($article_id) { $article_id = preg_match("/^[0-9]+$/", $article_id) ? $article_id : ""; }

    global $web;

    if (!$bbs_id || !$article_id) {
        return false;
    }

    $data = sql_fetch(" select * from {$web['article_table']}{$bbs_id} where id = '".$article_id."' limit 0, 1 ");

    return $data;

}

function reply($bbs_id, $reply_id)
{

    if ($bbs_id) { $bbs_id = preg_match("/^[a-zA-Z0-9_\-]+$/", $bbs_id) ? $bbs_id : ""; }
    if ($reply_id) { $reply_id = preg_match("/^[0-9]+$/", $reply_id) ? $reply_id : ""; }

    global $web;

    if (!$bbs_id || !$reply_id) {
        return false;
    }

    $data = sql_fetch(" select * from {$web['reply_table']}{$bbs_id} where id = '".$reply_id."' limit 0, 1 ");

    return $data;

}

function bbs_thumb($bbs_id, $datetime, $text, $thumb_size)
{

    global $disk;

    if (!$bbs_id || !$datetime || !$text || !$thumb_size) {
        return false;
    }

    $fileurl = image_editor($text);

    if (!$fileurl) {
        return false;
    }

    $filename = substr(md5(str_replace("%", "", urlencode($fileurl))), 0, 10).image_filetype($fileurl);

    $thumb = $disk['server_bbs']."/thumb/".$bbs_id."/".data_path("u", $datetime)."/".$thumb_size."_".$filename;

    return $thumb;

}

function bbs_thumb_create($bbs_id, $datetime, $text, $thumb_width, $thumb_height, $iscut)
{

    global $disk;

    if (!$bbs_id || !$datetime || !$text || !$thumb_width || !$thumb_height) {
        return false;
    }

    $thumb_path = $disk['path']."/thumb/".$bbs_id;

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $thumb_path = $thumb_path."/".data_path("u", $datetime);

    @mkdir($thumb_path, 0707);
    @chmod($thumb_path, 0707);

    $thumb = bbs_thumb_path($text, $thumb_path, $thumb_width, $thumb_height, $iscut, 70);

    return $thumb;

}

function bbs_thumb_path($text, $thumb_path, $thumb_width, $thumb_height, $iscut)
{

    global $disk, $web;

    $thumb = "";
    $thumb_file = "";
    $img_filename = "";
    $save_mode = false;

    $text = stripslashes($text);
    $text = str_replace("\\", "", $text);

    preg_match_all("/<img (.*)>/Uis", $text, $matches);

    if (!$matches[1][0]) {

        preg_match_all("/<IMG (.*)>/Uis", $text, $matches);

    }

    $tmp = preg_match("/src=\"(.*)\"/Uis", $matches[1][0], $match);
    $tmp = trim($match[1]);

    if (!$tmp) {

        $tmp = preg_match("/src='(.*)'/Uis", $matches[1][0], $match);
        $tmp = trim($match[1]);

    }

    //if (preg_match("/\.(jp[e]?g|gif|png)$/i", $tmp) && preg_match("/(onnada\.com)/i", $tmp)) {
    if (preg_match("/\.(jp[e]?g|gif|png)$/i", $tmp)) {

        $fileurl = $tmp;
        $filename = substr(md5(str_replace("%", "", urlencode($fileurl))), 0, 10).image_filetype($fileurl);
        $thumb = $thumb_path.'/'.$thumb_width.'x'.$thumb_height.'_'.$filename;
        $thumb_file = $fileurl;
        $img_filename = $filename;
        $save_mode = true;

    } else {

        return false;

    }

    if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

        if ($save_mode) {

            $editor = preg_replace("/\//i", "\\/", $disk['server_editor']);
            if (preg_match("/^($editor)/", $thumb_file)) {

                $path = $disk['path']."/editor";
                $path = preg_replace("/$editor/", $path, $thumb_file);
                $thumb_file = $path;

            } else {

                $thumb_file = image_save($thumb_file, $img_filename, $save_mode);

            }

        }

        if (preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

            image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $iscut);

        }

    }

    if (file_exists($thumb) && $img_filename) {

        $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        return $thumb;

    } else {

        return false;

    }

}

function bbs_file($bbs_id, $article_id, $number)
{

    if ($bbs_id) { $bbs_id = preg_match("/^[a-zA-Z0-9_\-]+$/", $bbs_id) ? $bbs_id : ""; }
    if ($article_id) { $article_id = preg_match("/^[0-9]+$/", $article_id) ? $article_id : ""; }
    if ($number) { $number = preg_match("/^[0-9]+$/", $number) ? $number : ""; }

    global $web;

    if (!$bbs_id || !$article_id || !$number) {
        return false;
    }

    $data = sql_fetch(" select * from $web[bbs_file_table] where bbs_id = '".$bbs_id."' and article_id = '".$article_id."' and number = '".$number."' limit 0, 1 ");

    return $data;

}

function bbs_file_icon($file)
{

    if (preg_match("/\.(psd)/i", $file)) { return "psd"; }
    else if (preg_match("/\.(ai|eps)/i", $file)) { return "ai"; }
    else if (preg_match("/\.(pdf)/i", $file)) { return "pdf"; }
    else if (preg_match("/\.(hwp)/i", $file)) { return "hwp"; }
    else if (preg_match("/\.(doc|docx|dot|dotx)/i", $file)) { return "doc"; }
    else if (preg_match("/\.(xls|xlsx)/i", $file)) { return "xls"; }
    else if (preg_match("/\.(ppt|pptx|pot|pps|ppsx)/i", $file)) { return "ppt"; }
    else if (preg_match("/\.(zip|rar|7z|gz|bz2|alz|egg|raw|lha)/i", $file)) { return "zip"; }
    else { return "etc"; }

}

function bbs_skin_dir()
{

    global $web;

    $list = array();

    $dirname = $web['path']."/bbs/skin/";
    $handle = opendir($dirname);
    while ($file = readdir($handle)) 
    {
        if($file == "."||$file == "..") continue;

        if (is_dir($dirname.$file)) $list[] = $file;
    }
    closedir($handle);
    sort($list);

    return $list;
}

function bbs_police_reason($id)
{

    if (!$id) {
        return false;
    }

    if ($id == 1) { return "불법성"; }
    else if ($id == 2) { return "음란성"; }
    else if ($id == 3) { return "저작권 침해"; }
    else if ($id == 4) { return "욕설/상호 비방"; }
    else if ($id == 5) { return "서비스품질 저해"; }

}
?>