<?php // 공통
if (!defined("_WEB_")) exit;

function blockkeyword($text)
{

    if (!$text) { return false; }

    $text = strip_tags($text);
    $text = preg_replace("/[0-9\-|,.\&;\/\!\?\~\[\]]/", "", $text);

    $replace = "는|은|를|을|네|들아|들|야|하고|하자|할까|다";
    $text = preg_replace("/($replace)$/", "", $text);
    $text = preg_replace("/($replace)([[:space:]])/", " ", $text);

    $chk = blockkeyword_check($text);

    if (!$chk) {

        $text = preg_replace("/(ㅏ|ㅑ|ㅓ|ㅕ|ㅗ|ㅛ|ㅜ|ㅠ|ㅡ|ㅣ|ㄱ|ㄴ|ㄷ|ㄹ|ㅁ|ㅂ|ㅅ|ㅇ|ㅈ|ㅊ|ㅋ|ㅌ|ㅍ|ㅎ)/", "", $text);
        $chk = blockkeyword_check($text);

    }

    if ($chk) {

        return $chk;

    } else {

        return false;

    }

}

function blockkeyword_check($text)
{

    global $setup;

    if (!$text) {

        return false;

    }

    $filter_list = $setup['block_keyword'];

    if (!$filter_list) {
        return false;
    }

    if (preg_match("/^($filter_list)$/", $text, $match)) {

        return $match[0];

    }

    else if (preg_match("/^($filter_list)/", $text, $match)) {

        return $match[0];

    }

    else if (preg_match("/($filter_list)$/", $text, $match)) {

        return $match[0];

    }

    else if (preg_match("/^($filter_list)([[:space:]])/", $text, $match)) {

        return $match[0];

    }

    else if (preg_match("/($filter_list)([[:space:]])/", $text, $match)) {

        return $match[0];

    }

    else if (preg_match("/([[:space:]])($filter_list)/", $text, $match)) {

        return $match[0];

    }

    else if (preg_match("/([[:space:]])($filter_list)$/", $text, $match)) {

        return $match[0];

    }

    else if (preg_match("/([[:space:]])($filter_list)([[:space:]])/", $text, $match)) {

        return $match[0];

    } else {

        return false;

    }

}

function check_auth($menu_id, $menu_num)
{

    if ($menu_id) { $menu_id = preg_match("/^[a-zA-Z0-9_\-]+$/", $menu_id) ? $menu_id : ""; }
    if ($menu_num) { $menu_num = preg_match("/^[a-zA-Z0-9_\-]+$/", $menu_num) ? $menu_num : ""; }

    if (!$menu_id || !$menu_num) {

        return false;

    }

    global $web, $member;

    if (!$member['uid']) {

        return false;

    }

    if ($member['level'] < 9) {

        return false;

    }

    if ($member['level'] >= 10) {

        return "full";

    }

    $chk = setup_auth($menu_id, $menu_num);

    if (!$chk['menu_id']) {

        return false;

    }

    $row = explode("|", $chk['full_uid']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] == $member['uid']) {

            return "full";

        }

    }

    $row = explode("|", $chk['read_uid']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] == $member['uid']) {

            return "read";

        }

    }

    return false;

}

function setup_api()
{

    global $web;

    $data = sql_fetch(" select * from $web[setup_api_table] limit 0, 1 ");

    return $data;

}

function setup_auth($menu_id, $menu_num)
{

    if ($menu_id) { $menu_id = preg_match("/^[a-zA-Z0-9_\-]+$/", $menu_id) ? $menu_id : ""; }
    if ($menu_num) { $menu_num = preg_match("/^[a-zA-Z0-9_\-]+$/", $menu_num) ? $menu_num : ""; }

    if (!$menu_id || !$menu_num) {

        return false;

    }

    global $web, $member;

    $data = sql_fetch(" select * from $web[setup_auth_table] where menu_id = '".$menu_id."' and menu_num = '".$menu_num."' limit 0, 1 ");

    return $data;

}

function setup_join()
{

    global $web;

    $data = sql_fetch(" select * from $web[setup_join_table] limit 0, 1 ");

    return $data;

}

function setup_email($id)
{

    if ($id) { $id = preg_match("/^[a-zA-Z0-9_\-]+$/", $id) ? $id : ""; }

    global $web;

    $data = sql_fetch(" select * from $web[setup_email_table] where id = '".$id."' limit 0, 1 ");

    return $data;

}

function area($id)
{

    if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }

    if (!$id) {

        return false;

    }

    global $web;

    $data = sql_fetch(" select * from $web[area_table] where id = '".$id."' limit 0, 1 ");

    return $data;

}

function banner($id)
{

    if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }

    global $web;

    if (!$id) {
        return false;
    }

    $data = sql_fetch(" select * from $web[banner_table] where id = '".$id."' limit 0, 1 ");

    return $data;

}

function popup($popup_id)
{

    if ($popup_id) { $popup_id = preg_match("/^[0-9]+$/", $popup_id) ? $popup_id : ""; }

    global $web;

    if (!$popup_id) {
        return false;
    }

    $data = sql_fetch(" select * from $web[popup_table] where popup_id = '".$popup_id."' limit 0, 1 ");

    return $data;

}

function editor_thumb($matches)
{

    $text = $matches[0];

    $text = preg_replace("/thumb_/i", "", $text);

    return $text;

}

function http_fsockopen($url, $port="80", $method="POST")
{

    if (!$url) {

        return false;

    }

    $url = parse_url($url);

    $host = $url['host'];
    $path = $url['path'];
    $query = $url['query'];

    $fp = fsockopen($host, $port, $errno, $errstr, 30);
    if (!is_resource($fp)) {

        return false;

    }

    fputs($fp, $method." $path HTTP/1.1\r\n");
    fputs($fp, "Host: $host\r\n");
    fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
    fputs($fp, "Content-length: " . strlen($query) . "\r\n");
    fputs($fp, "Connection:close" . "\r\n\r\n");
    fputs($fp, $query);

    $result = '';
    while(!feof($fp)) {
        $result .= fgets($fp, 128);
    }
    fclose($fp);

    return $result;

}

function number($number, $point=0)
{

    if ($point) {

        $data = number_format($number, $point);
        return $data;

    } else {

        // number_format 사용할 경우 반올림함(1.51->2). 그렇기 때문에 아래코드로 반올림을 하지 않고 콤마를 붙임
        return number_format((int)($number));

    }

}

function search_keyword($q)
{

    global $web;

    if (!$q) {
        return false;
    }

    // 키워드
    if ($q && !blockkeyword($q)) {

        $chk = sql_fetch(" select id from $web[keyword_ip_table] where keyword = '".addslashes($q)."' and ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' limit 0, 1 ");

        if (!$chk['id']) {

            $keyword = sql_fetch(" select id from $web[keyword_table] where keyword = '".addslashes($q)."' and datetime >= '".date("Y-m-d H:i:s", $web['server_time'] - (1 * 86400))."' limit 0, 1 ");

            // 체크
            if ($keyword['id']) {

                sql_query(" update $web[keyword_table] set key_count = key_count + 1, total_count = total_count + 1, lasttime = '".$web['time_ymdhis']."' where id = '".$keyword['id']."' ");

            } else {

                sql_query(" insert into $web[keyword_table] set keyword = '".trim(strip_tags(sql_real_escape_string($q)))."', key_count = '1', total_count = '1', position = '-1', position2 = '-1', type = '0', datetime = '".$web['time_ymdhis']."', lasttime = '".$web['time_ymdhis']."' ");

            }

        }

    }

}

function search_text($mode, $text)
{

    if (!$mode || !$text) {

        return false;

    }

    if ($mode == 1) {

        $text = trim(stripslashes(strip_tags($text)));
        $text = preg_replace("/&#8238/", "", $text);
        $text = preg_replace("/  /i", "", $text);
        $text = preg_replace("/\\r/i", " ", $text);
        $text = preg_replace("/\\n/i", " ", $text);
        $text = preg_replace("/\&nbsp;/i", " ", $text);

        return text_cut($text, 100, "");

    }

    else if ($mode == 2) {

        $text = trim(stripslashes(strip_tags($text)));
        $text = preg_replace("/&#8238/", "", $text);
        $text = preg_replace("/  /i", "", $text);
        $text = preg_replace("/\\r/i", " ", $text);
        $text = preg_replace("/\\n/i", " ", $text);
        $text = preg_replace("/\&nbsp;/i", " ", $text);
        $text = str_replace(" ", "", $text);

        return text_cut($text, 100, "");

    } else {

        return false;

    }

}

function memo($mid, $message)
{

    global $web;

    if (!$mid || !$message) {
        return false;
    }

    $message = stripslashes($message);

    $sql_common = "";
    $sql_common .= " set mid = '".$mid."', ";
    $sql_common .= " fid = '1', ";
    $sql_common .= " content = '".trim(strip_tags(sql_real_escape_string($message)))."', ";
    $sql_common .= " type = '0', ";
    $sql_common .= " datetime = '".$web['time_ymdhis']."' ";

    sql_query(" insert into $web[message_receive_table] $sql_common ");

}

function message($message="", $mode="", $url="", $html=true, $stop=true)
{

    global $web;

    if (!$message) {

        $message = '메세지 내용이 없습니다.';

    }

    if ($url) {

        $message .= "<p class='btn'><a href='#' onclick='message(\"close\"); location.replace(\"".$url."\"); return false;' class='msgclose'></a></p>";

    }

    else if ($mode == 'b') {

        $message .= "<p class='btn'><a href='#' onclick='message(\"close\"); history.go(-1); return false;' class='msgclose'></a></p>";

    }

    else if ($mode == 'c') {

        $message .= "<p class='btn'><a href='#' onclick='window.close(); return false;' class='msgclose'></a></p>";

    } else {

        $message .= "<p class='btn'><a href='#' onclick='message(\"close\"); return false;' class='msgclose'></a></p>";

    }

    if ($html) {

        echo "<!DOCTYPE html>";
        echo "<html>";
        echo "<head>";
        echo "<meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0\" /><meta charset=\"".$web['charset']."\"><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,IE=9,chrome=1\" />";
        echo "<title>알림</title>";
        echo "<link rel=\"stylesheet\" href=\"".$web['host_css']."/web.css\" type=\"text/css\" />";
        echo "<script type=\"text/javascript\" src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>";
        echo "<script type=\"text/javascript\" src=\"".$web['host_js']."/web.js\"></script>";
        echo "</head>";
        echo "<body style='overflow:hidden;'>";
        echo "<div id='overlay'></div>";
        echo "<div id='messagebox'></div>";

    }

    $message = preg_replace("/\"/", "&#034;", $message);

    echo "<script type='text/javascript'>$(document).ready(function() { setTimeout( function() { $('#messagebox').html(\"$message\"); message('open'); }, 300 ); }); </script>";

    if ($html) {

        echo "</body>";
        echo "</html>";

    }

    if ($stop) {

        exit;

    }

}

function updatetime($datetime)
{

    global $web;

    // 시간
    $time = (int)($web['server_time'] - strtotime($datetime));

    // 1일
    if ($time >= '86400') {

        return (int)((strtotime(date("Y-m-d", $web['server_time'])) - strtotime(date("Y-m-d", strtotime($datetime)))) / 86400)."일전";

    }

    // 1시간
    else if ($time >= '3600') {

        return (int)(($web['server_time'] - strtotime($datetime)) / 3600)."시간전";

    }

    // 1분
    else if ($time >= '60') {

        return (int)(($web['server_time'] - strtotime($datetime)) / 60)."분전";

    }

    // 1초
    else if ($time >= '0') {

        return (int)($web['server_time'] - strtotime($datetime))."초전";

    } else {

        return date("y-m-d H:i", strtotime($datetime));

    }

}

function image_filetype($source)
{

    // .으로 배열처리
    $file = explode('.', $source); // test.jpg.bmp.gif

    // 마지막 배열 구하기
    $file = array_pop($file); // -> gif

    // 확장자 체크
    if (preg_match("/(jpg)/i", $file)) {

        $filetype = "jpg";

    }

    else if (preg_match("/(jpeg)/i", $file)) {

        $filetype = "jpeg";

    }

    else if (preg_match("/(gif)/i", $file)) {

        $filetype = "gif";

    }

    else if (preg_match("/(png)/i", $file)) {

        $filetype = "png";

    }

    else if (preg_match("/(bmp)/i", $file)) {

        $filetype = "bmp";

    } else {

        //return true;
        $filetype = "jpg";

    }

    // .jpg 형식으로 출력.
    return ".".$filetype;

}

function image_save($source, $file, $mode=false)
{

    global $web, $disk, $save_count;

    // 저장 경로 지정
    $tmp_dir = $disk['path']."/tmp";

    // 디렉토리가 없다면 생성합니다. (퍼미션도 변경하구요.)
    @mkdir($tmp_dir, 0707);
    @chmod($tmp_dir, 0707);

    // 원본파일
    $source = text($source);

    // file type
    if (!preg_match("/\.(jp[e]?g|gif|png)$/i", $file)) {

        return false;

    }

    // 파일명 선언
    $tmp_filename = $file;

    // 쓰기파일
    $target = $tmp_dir . "/" . $tmp_filename;

    // 존재
    if (file_exists($target) && @filesize($target)) {

        return $target;

    }

    // 읽기, 쓰기
    $rf = @fopen($source, 'r');
    $wf = @fopen($target, 'w');

    // error reading or opening file
    if ($rf == false || $wf == false) {

        @unlink($target);

        include_once("$web[path]/other/curl/Curl.php");

        $curl = new Curl();
        $curl->download($source, $target);

        if (!file_exists($target)) {

            @unlink($target);

        }

        @chmod($target, 0606);

    } else {

        while (!feof($rf)) {

            // 'Download error: Cannot write to file ('.$target.')';
            if (fwrite($wf, fread($rf, 1024)) == false) {

                return false;

            }

        }

        @fclose($rf);
        @fclose($wf);
        @chmod($wf, 0606);

    }

    // 파일 정보
    $tmp_type = @getimagesize($target);

    // 이미지 파일이 있다면.
    if ($tmp_type[2] == 1 || $tmp_type[2] == 2 || $tmp_type[2] == 3) {

        return $target;

    } else {
    // 삭제

        @unlink($target);

        if ($save_count > 5) {

            return false;

        } else {

            $save_count++;

            return image_save($source, $file);

        }

    }

}

function image_thumb($thumb_width, $thumb_height, $img_source, $img_thumb='', $iscut="", $quality="100", $crop_left="0", $crop_right="0", $crop_top="0", $crop_bottom="0")
{

    global $setup, $web, $thumb_transparent;

    if ($thumb_transparent) { $thumb_transparent = preg_match("/^[0-9]+$/", $thumb_transparent) ? $thumb_transparent : ""; }

    if (file_exists($img_source)) {

        // pass

    } else {

        return false;

    }

    $iscut = addslashes($iscut);

    $gifsicle_path = $setup['gifsicle_path'];

    if (!$img_thumb) {

        $img_thumb = $img_source;

    }

    $size = @getimagesize($img_source);

    if ($size[2] == 1) {

        $source = @imagecreatefromgif($img_source);

    }

    else if ($size[2] == 2) {

        $source = @imagecreatefromjpeg($img_source);

    }

    else if ($size[2] == 3) {

        $source = @imagecreatefrompng($img_source);

    } else {

        return false;

    }

    // 원본이 썸네일보다 작을 때 (가로 세로 작다)
    if ($size[0] <= $thumb_width && $size[1] <= $thumb_height) {

        // gif
        if ($gifsicle_path && $size[2] == 1) {

            @exec(" $gifsicle_path --interlace --resize {$size[0]}"."x"."{$size[1]} {$img_source} > {$img_thumb} ");

        } else {
        // etc

            // 원본 비율
            $target = @imagecreatetruecolor($size[0], $size[1]);

            if ($size[2] == 3) {

                if ($thumb_transparent) {

                    $bgcolor = imagecolorallocate($target, 0, 0, 0);
                    imagecolortransparent($target, $bgcolor);

                } else {

                    $bgcolor = imagecolorallocate($target, 255, 255, 255);
                    imagefill($target, 0, 0, $bgcolor);

                }

            }

            @imagecopyresampled($target, $source, 0, 0, 0, 0, $size[0], $size[1], $size[0], $size[1]);

            // jpg, png
            if ($size[2] == 2 || $size[2] == 3) {

                @UnsharpMask($target, 50, 0.5, 0);

            }

            if ($size[2] == 3 && $thumb_transparent) {

                @imagepng($target, $img_thumb);

            } else {

                @imagejpeg($target, $img_thumb, $quality);

            }

        }

    } else {

        // 원본 중앙 크롭
        if ($iscut == 'oc') {

            // 원본가로가 썸네일보다 크다
            if ($size[0] >= $thumb_width) {

                $crop_left = ceil(($size[0] / 2) - ($thumb_width / 2));
                $crop_top = ceil(($size[1] / 2) - ($thumb_height / 2));

                $create_width = $size[0];
                $create_height = $size[1];

            } else {
            // 작다

                $thumb_width = $size[0];

                $crop_left = ceil(($size[0] / 2) - ($thumb_width / 2));
                $crop_top = ceil(($size[1] / 2) - ($thumb_height / 2));

                $create_width = $size[0];
                $create_height = $size[1];

            }

            // 원본세로가 썸네일보다 크다
            if ($size[1] >= $thumb_height) {

                $crop_left = ceil(($size[0] / 2) - ($thumb_width / 2));
                $crop_top = ceil(($size[1] / 2) - ($thumb_height / 2));

                $create_width = $size[0];
                $create_height = $size[1];

            } else {
            // 작다

                $thumb_height = $size[1];

                $crop_left = ceil(($size[0] / 2) - ($thumb_width / 2));
                $crop_top = ceil(($size[1] / 2) - ($thumb_height / 2));

                $create_width = $size[0];
                $create_height = $size[1];

            }

            // gif
            if ($gifsicle_path && $size[2] == 1) {

                @exec(" $gifsicle_path --interlace --resize {$thumb_width}"."x"."{$thumb_height} {$img_source} > {$img_thumb} ");

            } else {

                $target = @imagecreatetruecolor($thumb_width, $thumb_height);

                if ($size[2] == 3) {

                    if ($thumb_transparent) {

                        $bgcolor = imagecolorallocate($target, 0, 0, 0);
                        imagecolortransparent($target, $bgcolor);

                    } else {

                        $bgcolor = imagecolorallocate($target, 255, 255, 255);
                        imagefill($target, 0, 0, $bgcolor);

                    }

                }

                @imagecopyresampled($target, $source, 0, 0, $crop_left, $crop_top, $create_width, $create_height, $size[0], $size[1]);

                // jpg, png
                if ($size[2] == 2 || $size[2] == 3) {

                    @UnsharpMask($target, 50, 0.5, 0);

                }

                if ($size[2] == 3 && $thumb_transparent) {

                    @imagepng($target, $img_thumb);

                } else {

                    @imagejpeg($target, $img_thumb, $quality);

                }

            }

        }

        // 비율 조정 크롭
        else if ($iscut == 'sc') {

            // 원본이 썸네일보다 크다
            if ($size[0] >= $thumb_width && $size[1] >= $thumb_height) {

                // 가로 비율을 구한다
                $rate1 = $thumb_height / $size[1];
                $width = (int)($size[0] * $rate1);

                // 세로 비율을 구한다
                $rate2 = $thumb_width / $size[0];
                $height = (int)($size[1] * $rate2);

                // 원본가로가 크다
                if ($size[0] >= $size[1]) {

                    if ($height <= $thumb_height) {

                        $create_width = $width;
                        $create_height = $thumb_height;

                        $crop_left = ceil(($size[0] / 2) - (($thumb_width / 2) / $rate1));
                        $crop_top = ceil(($thumb_height / 2) - ($thumb_height / 2));

                    } else {

                        $create_width = $thumb_width;
                        $create_height = $height;

                        $crop_left = ceil(($thumb_width / 2) - ($thumb_width / 2));
                        $crop_top = ceil(($size[1] / 2) - (($thumb_height / 2) / $rate2));

                    }

                } else {
                // 원본세로가 크다

                    if ($width <= $thumb_width) {

                        $create_width = $thumb_width;
                        $create_height = $height;

                        $crop_left = ceil(($thumb_width / 2) - ($thumb_width / 2));
                        $crop_top = ceil(($size[1] / 2) - (($thumb_height / 2) / $rate2));

                    } else {

                        $create_width = $width;
                        $create_height = $thumb_height;

                        $crop_left = ceil(($size[0] / 2) - (($thumb_width / 2) / $rate1));
                        $crop_top = ceil(($thumb_height / 2) - ($thumb_height / 2));

                    }

                }

            } else {
            // 하나라도 작다면

                // 원본가로가 썸네일보다 크다
                if ($size[0] >= $thumb_width) {

                    $crop_left = ceil(($size[0] / 2) - ($thumb_width / 2));
                    $crop_top = ceil(($size[1] / 2) - ($thumb_height / 2));

                    $create_width = $size[0];
                    $create_height = $size[1];

                } else {
                // 작다

                    $thumb_width = $size[0];

                    $crop_left = ceil(($size[0] / 2) - ($thumb_width / 2));
                    $crop_top = ceil(($size[1] / 2) - ($thumb_height / 2));

                    $create_width = $size[0];
                    $create_height = $size[1];

                }

                // 원본세로가 썸네일보다 크다
                if ($size[1] >= $thumb_height) {

                    $crop_left = ceil(($size[0] / 2) - ($thumb_width / 2));
                    $crop_top = ceil(($size[1] / 2) - ($thumb_height / 2));

                    $create_width = $size[0];
                    $create_height = $size[1];

                } else {
                // 작다

                    $thumb_height = $size[1];

                    $crop_left = ceil(($size[0] / 2) - ($thumb_width / 2));
                    $crop_top = ceil(($size[1] / 2) - ($thumb_height / 2));

                    $create_width = $size[0];
                    $create_height = $size[1];

                }

            }

            // gif
            if ($gifsicle_path && $size[2] == 1) {

                @exec(" $gifsicle_path --interlace --resize {$thumb_width}"."x"."{$thumb_height} {$img_source} > {$img_thumb} ");

            } else {

                $target = @imagecreatetruecolor($thumb_width, $thumb_height);

                if ($size[2] == 3) {

                    if ($thumb_transparent) {

                        $bgcolor = imagecolorallocate($target, 0, 0, 0);
                        imagecolortransparent($target, $bgcolor);

                    } else {

                        $bgcolor = imagecolorallocate($target, 255, 255, 255);
                        imagefill($target, 0, 0, $bgcolor);

                    }

                }

                @imagecopyresampled($target, $source, 0, 0, $crop_left, $crop_top, $create_width, $create_height, $size[0], $size[1]);

                // jpg, png
                if ($size[2] == 2 || $size[2] == 3) {

                    @UnsharpMask($target, 50, 0.5, 0);

                }

                if ($size[2] == 3 && $thumb_transparent) {

                    @imagepng($target, $img_thumb);

                } else {

                    @imagejpeg($target, $img_thumb, $quality);

                }

            }

        }

        // 비율 처리(세로고정)
        else if ($iscut == 'sh') {

            $size[0] = (int)($size[0] - ($crop_left + $crop_right));
            $size[1] = (int)($size[1] - $crop_top);

            // 가로 비율을 구한다
            $rate = $thumb_height / $size[1];
            $width = (int)($size[0] * $rate);

            // 세로 비율을 구한다
            $rate = $thumb_width / $size[0];
            $height = (int)($size[1] * $rate);

            // 가로 비율이 썸네일을 초과
            if ($width > $thumb_width) {

                $create_width = $width;

            } else {
            // 미초과

                $create_width = $width;

            }

            // 세로 비율이 썸네일을 초과
            if ($height > $thumb_height) {

                $create_height = $thumb_height;

            } else {
            // 미초과

                $create_height = $height;

            }

            // gif
            if ($gifsicle_path && $size[2] == 1) {

                @exec(" $gifsicle_path --interlace --resize {$create_width}"."x"."{$create_height} {$img_source} > {$img_thumb} ");

            } else {
            // etc

                $target = @imagecreatetruecolor($create_width, $create_height);

                if ($size[2] == 3) {

                    if ($thumb_transparent) {

                        $bgcolor = imagecolorallocate($target, 0, 0, 0);
                        imagecolortransparent($target, $bgcolor);

                    } else {

                        $bgcolor = imagecolorallocate($target, 255, 255, 255);
                        imagefill($target, 0, 0, $bgcolor);

                    }

                }

                @imagecopyresampled($target, $source, 0, 0, $crop_left, $crop_top, $create_width, $create_height, $size[0], $size[1]);

                // jpg, png
                if ($size[2] == 2 || $size[2] == 3) {

                    @UnsharpMask($target, 50, 0.5, 0);

                }

                if ($size[2] == 3 && $thumb_transparent) {

                    @imagepng($target, $img_thumb);

                } else {

                    @imagejpeg($target, $img_thumb, $quality);

                }

            }

        }

        // 하단 크롭
        else if ($iscut) {

            $size[0] = (int)($size[0] - ($crop_left + $crop_right));
            $size[1] = (int)($size[1] - $crop_top);

            // 가로 비율을 구한다
            $rate = $thumb_height / $size[1];
            $width = (int)($size[0] * $rate);

            // 세로 비율을 구한다
            $rate = $thumb_width / $size[0];
            $height = (int)($size[1] * $rate);

            // 가로가 썸네일보다 작다면 원본
            if ($size[0] <= $thumb_width) {

                $thumb_width = $size[0];

            }

            // gif
            if ($gifsicle_path && $size[2] == 1) {

                // 비율이 썸네일을 초과한다면
                if ($height > $thumb_height) {

                    $height = $thumb_height;

                }

                @exec(" $gifsicle_path --interlace --resize {$thumb_width}"."x"."{$height} {$img_source} > {$img_thumb} ");

            } else {
            // etc

                // 비율이 thumb 보다 작을 때
                if ($height <= $thumb_height) {

                    $thumb_height = $height;

                }

                $target = @imagecreatetruecolor($thumb_width, $thumb_height);

                if ($size[2] == 3) {

                    if ($thumb_transparent) {

                        $bgcolor = imagecolorallocate($target, 0, 0, 0);
                        imagecolortransparent($target, $bgcolor);

                    } else {

                        $bgcolor = imagecolorallocate($target, 255, 255, 255);
                        imagefill($target, 0, 0, $bgcolor);

                    }

                }

                @imagecopyresampled($target, $source, 0, 0, $crop_left, $crop_top, $thumb_width, $height, $size[0], $size[1]);

                // jpg, png
                if ($size[2] == 2 || $size[2] == 3) {

                    @UnsharpMask($target, 50, 0.5, 0);

                }

                if ($size[2] == 3 && $thumb_transparent) {

                    @imagepng($target, $img_thumb);

                } else {

                    @imagejpeg($target, $img_thumb, $quality);

                }

            }

        } else {
        // 비율 처리

            $size[0] = (int)($size[0] - ($crop_left + $crop_right));
            $size[1] = (int)($size[1] - $crop_top);

            // 가로 비율을 구한다
            $rate = $thumb_height / $size[1];
            $width = (int)($size[0] * $rate);

            // 세로 비율을 구한다
            $rate = $thumb_width / $size[0];
            $height = (int)($size[1] * $rate);

            // 가로 비율이 썸네일을 초과
            if ($width > $thumb_width) {

                $create_width = $thumb_width;

            } else {
            // 미초과

                $create_width = $width;

            }

            // 세로 비율이 썸네일을 초과
            if ($height > $thumb_height) {

                $create_height = $thumb_height;

            } else {
            // 미초과

                $create_height = $height;

            }

            // gif
            if ($gifsicle_path && $size[2] == 1) {

                @exec(" $gifsicle_path --interlace --resize {$create_width}"."x"."{$create_height} {$img_source} > {$img_thumb} ");

            } else {
            // etc

                $target = @imagecreatetruecolor($create_width, $create_height);

                if ($size[2] == 3) {

                    if ($thumb_transparent) {

                        $bgcolor = imagecolorallocate($target, 0, 0, 0);
                        imagecolortransparent($target, $bgcolor);

                    } else {

                        $bgcolor = imagecolorallocate($target, 255, 255, 255);
                        imagefill($target, 0, 0, $bgcolor);

                    }

                }

                @imagecopyresampled($target, $source, 0, 0, $crop_left, $crop_top, $create_width, $create_height, $size[0], $size[1]);

                // jpg, png
                if ($size[2] == 2 || $size[2] == 3) {

                    @UnsharpMask($target, 50, 0.5, 0);

                }

                if ($size[2] == 3 && $thumb_transparent) {

                    @imagepng($target, $img_thumb);

                } else {

                    @imagejpeg($target, $img_thumb, $quality);

                }

            }

        }

    }

    @chmod($img_thumb, 0606); // 추후 삭제를 위하여 파일모드 변경

}

function image_resize($thumb_width, $thumb_height, $img_source, $img_thumb='', $quality="100")
{

    global $setup, $web, $thumb_transparent;

    if ($thumb_transparent) { $thumb_transparent = preg_match("/^[0-9]+$/", $thumb_transparent) ? $thumb_transparent : ""; }

    if (file_exists($img_source)) {

        // pass

    } else {

        return false;

    }

    $gifsicle_path = $setup['gifsicle_path'];

    if (!$img_thumb) {

        $img_thumb = $img_source;

    }

    $size = @getimagesize($img_source);

    if ($size[2] == 1) {

        $source = @imagecreatefromgif($img_source);

    }

    else if ($size[2] == 2) {

        $source = @imagecreatefromjpeg($img_source);

    }

    else if ($size[2] == 3) {

        $source = @imagecreatefrompng($img_source);

    } else {

        return false;

    }

    // 원본이 썸네일보다 작을 때 (가로 세로 작다)
    if ($size[0] <= $thumb_width && $size[1] <= $thumb_height) {

        // gif
        if ($gifsicle_path && $size[2] == 1) {

            @exec(" $gifsicle_path --interlace --resize {$size[0]}"."x"."{$size[1]} {$img_source} > {$img_thumb} ");

        } else {
        // etc

            // 원본 비율
            $target = @imagecreatetruecolor($size[0], $size[1]);

            if ($size[2] == 3) {

                if ($thumb_transparent) {

                    $bgcolor = imagecolorallocate($target, 0, 0, 0);
                    imagecolortransparent($target, $bgcolor);

                } else {

                    $bgcolor = imagecolorallocate($target, 255, 255, 255);
                    imagefill($target, 0, 0, $bgcolor);

                }

            }

            @imagecopyresampled($target, $source, 0, 0, 0, 0, $size[0], $size[1], $size[0], $size[1]);

            if ($size[2] == 3 && $thumb_transparent) {

                @imagepng($target, $img_thumb);

            } else {

                @imagejpeg($target, $img_thumb, $quality);

            }

        }

    } else {

        $size[0] = (int)($size[0] - ($crop_left + $crop_right));
        $size[1] = (int)($size[1] - $crop_top);

        // 가로 비율을 구한다
        $rate = $thumb_height / $size[1];
        $width = (int)($size[0] * $rate);

        // 가로 비율이 썸네일 초과, 원본이 가로 초과
        if ($width > $thumb_width || $size[0] >= $thumb_width) {

            $create_width = $thumb_width;

        }

        // 초과하지 않다면
        else if ($size[0] <= $thumb_width) {

            $create_width = $size[0];

        } else {

            $create_width = $width;

        }

        // 세로 비율을 구한다
        $rate = $create_width / $size[0];
        $height = (int)($size[1] * $rate);

        $create_height = $height;

        // gif
        if ($gifsicle_path && $size[2] == 1) {

            @exec(" $gifsicle_path --interlace --resize {$create_width}"."x"."{$create_height} {$img_source} > {$img_thumb} ");

        } else {
        // etc

            $target = @imagecreatetruecolor($create_width, $create_height);

            if ($size[2] == 3) {

                if ($thumb_transparent) {

                    $bgcolor = imagecolorallocate($target, 0, 0, 0);
                    imagecolortransparent($target, $bgcolor);

                } else {

                    $bgcolor = imagecolorallocate($target, 255, 255, 255);
                    imagefill($target, 0, 0, $bgcolor);

                }

            }

            @imagecopyresampled($target, $source, 0, 0, $crop_left, $crop_top, $create_width, $create_height, $size[0], $size[1]);

            if ($size[2] == 3 && $thumb_transparent) {

                @imagepng($target, $img_thumb);

            } else {

                @imagejpeg($target, $img_thumb, $quality);

            }

        }

    }

    @chmod($img_thumb, 0606); // 추후 삭제를 위하여 파일모드 변경

}

function image_area($source_file, $thumb_file, $thumb_width, $thumb_height, $area_x1, $area_y1, $area_x2, $area_y2, $area_w, $area_h, $quality)
{

    global $thumb_transparent;

    if ($thumb_transparent) { $thumb_transparent = preg_match("/^[0-9]+$/", $thumb_transparent) ? $thumb_transparent : ""; }

    if (file_exists($source_file)) {

        // pass

    } else {

        return false;

    }

    $size = @getimagesize($source_file);

    if ($size['mime'] == 'image/gif' || $size['mime'] == 'image/pjpeg' || $size['mime'] == 'image/jpeg' || $size['mime'] == 'image/jpg' || $size['mime'] == 'image/png' || $size['mime'] == 'image/x-png') {

        // pass

    } else {

        return false;

    }

    if ($size[2] == 1) {

        $source = @imagecreatefromgif($source_file);

    }

    else if ($size[2] == 2) {

        $source = @imagecreatefromjpeg($source_file);

    }

    else if ($size[2] == 3) {

        $source = @imagecreatefrompng($source_file);

    } else {

        return false;

    }

    $scale_width = ceil($area_x2 - $area_x1);
    $scale_height = ceil($area_y2 - $area_y1);

    $target = imagecreatetruecolor($thumb_width, $thumb_height);

    if ($size[2] == 3) {

        if ($thumb_transparent) {

            $bgcolor = imagecolorallocate($target, 0, 0, 0);
            imagecolortransparent($target, $bgcolor);

        } else {

            $bgcolor = imagecolorallocate($target, 255, 255, 255);
            imagefill($target, 0, 0, $bgcolor);

        }

    }

    imagecopyresampled($target, $source, 0, 0, $area_x1, $area_y1, $thumb_width, $thumb_height, $scale_width, $scale_height);

    if ($area_x1 == '0' && $area_y1 == '0' && $thumb_width == $size[0] && $thumb_height == $size[1]) {

        // 원본

    } else {

        // jpg, png
        if ($size[2] == 2 || $size[2] == 3) { @UnsharpMask($target, 50, 0.5, 0); }

    }

    if ($size[2] == 3 && $thumb_transparent) {

        @imagepng($target, $thumb_file);

    } else {

        @imagejpeg($target, $thumb_file, $quality);

    }

    @chmod($thumb_file, 0606);

}

function image_delete($html)
{

    return 1;

}

function image_editor($text)
{

    if (!$text) {
        return false;
    }

    $text = stripslashes($text);
    $text = str_replace("\\", "", $text);

    preg_match_all("/<img (.*)>/U", $text, $matches);

    if (!$matches[1][0]) {

        preg_match_all("/<IMG (.*)>/U", $text, $matches);

    }

    $tmp = preg_match("/src=\"(.*)\"/U", $matches[1][0], $match);
    $tmp = trim($match[1]);

    if (!$tmp) {

        $tmp = preg_match("/src='(.*)'/U", $matches[1][0], $match);
        $tmp = trim($match[1]);

    }

    if ($tmp) {

        if (preg_match("/\.(jp[e]?g|gif|png)$/i", $tmp)) {

            return $tmp;

        } else {

            return false;

        }

    } else {

        return false;

    }

}

function thumb_path($text, $thumb_path, $thumb_width, $thumb_height, $iscut)
{

    global $disk, $web;

    $thumb = "";
    $thumb_file = "";
    $img_filename = "";
    $save_mode = false;

    $text = stripslashes($text);
    $text = str_replace("\\", "", $text);

    preg_match_all("/<img (.*)>/Uis", $text, $matches);

    if (!$matches[1][0]) {

        preg_match_all("/<IMG (.*)>/Uis", $text, $matches);

    }

    $tmp = preg_match("/src=\"(.*)\"/Uis", $matches[1][0], $match);
    $tmp = trim($match[1]);

    if (!$tmp) {

        $tmp = preg_match("/src='(.*)'/Uis", $matches[1][0], $match);
        $tmp = trim($match[1]);

    }

    if (preg_match("/\.(jp[e]?g|gif|png)$/i", $tmp)) {

        $fileurl = $tmp;
        $filename = substr(md5(str_replace("%", "", urlencode($fileurl))), 0, 10).image_filetype($fileurl);
        $thumb = $thumb_path.'/'.$filename;
        $thumb_file = $fileurl;
        $img_filename = $filename;
        $save_mode = true;

    } else {

        return false;

    }

    if (!file_exists($thumb) && preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

        if ($save_mode) {

            $editor = preg_replace("/\//i", "\\/", $disk['server_editor']);
            if (preg_match("/^($editor)/", $thumb_file)) {

                $path = $disk['path']."/editor";
                $path = preg_replace("/$editor/", $path, $thumb_file);
                $thumb_file = $path;

            } else {

                $thumb_file = image_save($thumb_file, $img_filename, $save_mode);

            }

        }

        if (preg_match("/\.(jp[e]?g|gif|png)$/i", $thumb_file)) {

            image_thumb($thumb_width, $thumb_height, $thumb_file, $thumb, $iscut);

        }

    }

    if (file_exists($thumb) && $img_filename) {

        $thumb = str_replace($disk['path'], $disk['server'], $thumb);

        return $thumb;

    } else {

        return false;

    }

}

/*
Unsharp Mask for PHP - version 2.1.1
Unsharp mask algorithm by Torstein Hønsi 2003-07.
thoensi_at_netcom_dot_no.
Please leave this notice.
http://vikjavev.no/computing/ump.php

Amount: 50 - 200
Radius: 0.5 - 1
Threshold: 0 - 5
*/
function UnsharpMask($img, $amount, $radius, $threshold)
{

    // $img is an image that is already created within php using
    // imgcreatetruecolor. No url! $img must be a truecolor image.

    // Attempt to calibrate the parameters to Photoshop:
    if ($amount > 500)    $amount = 500;
    $amount = $amount * 0.016;
    if ($radius > 50)    $radius = 50;
    $radius = $radius * 2;
    if ($threshold > 255)    $threshold = 255;

    $radius = abs(round($radius));     // Only integers make sense.
    if ($radius == 0) {
        return $img;
    }
    $w = imagesx($img); $h = imagesy($img);
    $imgCanvas = imagecreatetruecolor($w, $h);
    $imgBlur = imagecreatetruecolor($w, $h);


    // Gaussian blur matrix:
    //
    //    1    2    1
    //    2    4    2
    //    1    2    1
    //
    //////////////////////////////////////////////////


    if (function_exists('imageconvolution')) { // PHP >= 5.1
            $matrix = array(
            array( 1, 2, 1 ),
            array( 2, 4, 2 ),
            array( 1, 2, 1 )
        );
        imagecopy ($imgBlur, $img, 0, 0, 0, 0, $w, $h);
        imageconvolution($imgBlur, $matrix, 16, 0);
    }
    else {

    // Move copies of the image around one pixel at the time and merge them with weight
    // according to the matrix. The same matrix is simply repeated for higher radii.
        for ($i = 0; $i < $radius; $i++)    {
            imagecopy ($imgBlur, $img, 0, 0, 1, 0, $w - 1, $h); // left
            imagecopymerge ($imgBlur, $img, 1, 0, 0, 0, $w, $h, 50); // right
            imagecopymerge ($imgBlur, $img, 0, 0, 0, 0, $w, $h, 50); // center
            imagecopy ($imgCanvas, $imgBlur, 0, 0, 0, 0, $w, $h);

            imagecopymerge ($imgBlur, $imgCanvas, 0, 0, 0, 1, $w, $h - 1, 33.33333 ); // up
            imagecopymerge ($imgBlur, $imgCanvas, 0, 1, 0, 0, $w, $h, 25); // down
        }
    }

    if($threshold>0){
        // Calculate the difference between the blurred pixels and the original
        // and set the pixels
        for ($x = 0; $x < $w-1; $x++)    { // each row
            for ($y = 0; $y < $h; $y++)    { // each pixel

                $rgbOrig = ImageColorAt($img, $x, $y);
                $rOrig = (($rgbOrig >> 16) & 0xFF);
                $gOrig = (($rgbOrig >> 8) & 0xFF);
                $bOrig = ($rgbOrig & 0xFF);

                $rgbBlur = ImageColorAt($imgBlur, $x, $y);

                $rBlur = (($rgbBlur >> 16) & 0xFF);
                $gBlur = (($rgbBlur >> 8) & 0xFF);
                $bBlur = ($rgbBlur & 0xFF);

                // When the masked pixels differ less from the original
                // than the threshold specifies, they are set to their original value.
                $rNew = (abs($rOrig - $rBlur) >= $threshold)
                    ? max(0, min(255, ($amount * ($rOrig - $rBlur)) + $rOrig))
                    : $rOrig;
                $gNew = (abs($gOrig - $gBlur) >= $threshold)
                    ? max(0, min(255, ($amount * ($gOrig - $gBlur)) + $gOrig))
                    : $gOrig;
                $bNew = (abs($bOrig - $bBlur) >= $threshold)
                    ? max(0, min(255, ($amount * ($bOrig - $bBlur)) + $bOrig))
                    : $bOrig;



                if (($rOrig != $rNew) || ($gOrig != $gNew) || ($bOrig != $bNew)) {
                        $pixCol = ImageColorAllocate($img, $rNew, $gNew, $bNew);
                        ImageSetPixel($img, $x, $y, $pixCol);
                    }
            }
        }
    }
    else{
        for ($x = 0; $x < $w; $x++)    { // each row
            for ($y = 0; $y < $h; $y++)    { // each pixel
                $rgbOrig = ImageColorAt($img, $x, $y);
                $rOrig = (($rgbOrig >> 16) & 0xFF);
                $gOrig = (($rgbOrig >> 8) & 0xFF);
                $bOrig = ($rgbOrig & 0xFF);

                $rgbBlur = ImageColorAt($imgBlur, $x, $y);

                $rBlur = (($rgbBlur >> 16) & 0xFF);
                $gBlur = (($rgbBlur >> 8) & 0xFF);
                $bBlur = ($rgbBlur & 0xFF);

                $rNew = ($amount * ($rOrig - $rBlur)) + $rOrig;
                    if($rNew>255){$rNew=255;}
                    elseif($rNew<0){$rNew=0;}
                $gNew = ($amount * ($gOrig - $gBlur)) + $gOrig;
                    if($gNew>255){$gNew=255;}
                    elseif($gNew<0){$gNew=0;}
                $bNew = ($amount * ($bOrig - $bBlur)) + $bOrig;
                    if($bNew>255){$bNew=255;}
                    elseif($bNew<0){$bNew=0;}
                $rgbNew = ($rNew << 16) + ($gNew <<8) + $bNew;
                    ImageSetPixel($img, $x, $y, $rgbNew);
            }
        }
    }
    imagedestroy($imgCanvas);
    imagedestroy($imgBlur);

    return $img;

}

function data_path($m, $datetime)
{

    global $web;

    if ($m == '') {

        $date = date("Ym", $web['server_time']);

    }

    else if ($m == 'u') {

        $date = date("Ym", strtotime($datetime));

    }

    return $date;

}

function file_view($file, $width, $height)
{

    static $ids;

    $ids++;

    if (preg_match("/\.(jp[e]?g|gif|png)$/i", $file)) {

        return "<img src='{$file}' width='".$width."' height='".$height."' border='0'>";

    }

    else if (preg_match("/\.(swf)$/i", $file)) {

        return "<script type=\"text/javascript\">flash('".$file."', 'flash_{$ids}', '".$width."', '".$height."', 'transparent')</script>";

    } else {

        return false;

    }

}

function alert($msg='', $url='')
{

    global $web;

    if (!$msg) {

        $msg = '메세지 내용이 없습니다.';

    }

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=$web[charset]\">";
    echo "<script type='text/javascript'>alert('$msg');";

    if (!$url) {

        echo "history.go(-1);";

    }

    echo "</script>";

    if ($url) {

        url($url);

    }

    exit;

}

function alert_close($msg)
{

    global $web;

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=$web[charset]\">";
    echo "<script type='text/javascript'> alert('$msg'); window.close(); </script>";
    exit;

}

function url($url)
{

    $url = preg_replace("/&amp;/", "&", $url);

    echo "<script type='text/javascript'>location.replace('".text($url)."');</script>";
    exit;

}

function file_size($size)
{

    if ($size >= 1048576) {

        $size = number_format($size/1048576, 1) . "MB";

    }

    else if ($size >= 1024) {

        $size = number_format($size/1024, 1) . "KB";

    } else {

        $size = number_format($size, 0) . "B";

    }

    $size = preg_replace("/\.0/", "", $size);
    return $size;

}

function dir_size($dir)
{

    $size = 0;
    $d = dir($dir);
    while ($entry = $d->read()) {

        if ($entry != "." && $entry != "..") {

            $size += filesize("$dir/$entry");

        }

    }

    $d->close();

    return $size;

}

function text($text, $html=0)
{

    $text = stripslashes($text);

    //$text = preg_replace("/&amp;/", "&", $text);
    $text = preg_replace("/&#8238/", "", $text); // 거꾸로 방지

    if ($html == 0) {

        $text = text_symbol($text);

    }

    $source[] = "/</";
    $target[] = "&lt;";
    $source[] = "/>/";
    $target[] = "&gt;";
    $source[] = "/\'/";
    $target[] = "&#039;";
    $source[] = "/\"/";
    $target[] = "&#034;";

    if ($html) {

        $source[] = "/\n/";
        $target[] = "<br/>";

    }

    return preg_replace($source, $target, $text);

}

function text2($text, $html="0", $cut="")
{

    if (!$text) {

        return false;

    }

    $text = preg_replace("/&#8238/", "", $text);
    $text = stripslashes($text);
    $text = filter_check($text, $html);

    if ($cut) {

        return text_cut($text, $cut, "");

    } else {

        return $text;

    }

}

function text3($text, $cut="")
{

    if (!$text) {

        return false;

    }

    $text = preg_replace("/&#8238/", "", $text);
    $text = trim(stripslashes(strip_tags($text)));

    $text = preg_replace("/  /i", "", $text);
    $text = preg_replace("/\\r/i", " ", $text);
    $text = preg_replace("/\\n/i", " ", $text);
    $text = preg_replace("/\&nbsp;/i", " ", $text);
    $text = preg_replace("/\&/i", "&amp;", $text);
    $text = preg_replace("/\"/", "&#034;", $text);
    $text = preg_replace("/\'/", "&#039;", $text);

    if ($cut) {

        return text_cut($text, $cut, "");

    } else {

        return $text;

    }

}

function text_view($text, $data="")
{

    if ($text) {

        return text($text);

    } else {

        return $data;

    }

}

function text_hp($text, $data="")
{

    if ($text == '--' || $text == '') {

        return $data;

    } else {

        $text = text(preg_replace("/-/", "", $text));

        if (strlen($text) == 12) {

            return substr($text,0,4)."-".substr($text,4,4)."-".substr($text,8,4);

        }

        else if (strlen($text) == 11) {

            return substr($text,0,3)."-".substr($text,3,4)."-".substr($text,7,4);

        }

        else if (strlen($text) == 10) {

            return substr($text,0,3)."-".substr($text,3,3)."-".substr($text,6,4);

        }

        else if (strlen($text) == 9) {

            return substr($text,0,2)."-".substr($text,2,3)."-".substr($text,5,4);

        }

        else if (strlen($text) == 8) {

            return substr($text,0,4)."-".substr($text,4,4);

        } else {

            return text($text);

        }

    }

}

function text_tel($text, $data="")
{

    if ($text == '--' || $text == '') {

        return $data;

    } else {

        $text = text(preg_replace("/-/", "", $text));

        if (strlen($text) == 12) {

            return substr($text,0,4)."-".substr($text,4,4)."-".substr($text,8,4);

        }

        else if (strlen($text) == 11) {

            return substr($text,0,3)."-".substr($text,3,4)."-".substr($text,7,4);

        }

        else if (strlen($text) == 10) {

            return substr($text,0,2)."-".substr($text,2,4)."-".substr($text,6,4);

        }

        else if (strlen($text) == 9) {

            return substr($text,0,2)."-".substr($text,2,3)."-".substr($text,5,4);

        }

        else if (strlen($text) == 8) {

            return substr($text,0,4)."-".substr($text,4,4);

        } else {

            return text($text);

        }

    }

}

function text_date($view, $datetime, $data="")
{

    if ($datetime == '' || !$datetime) {

        return $data;

    }

    else if ($datetime == '0000-00-00 00:00:00') {

        return $data;

    }

    else if ($datetime == '0000-00-00') {

        return $data;

    } else {

        return date($view, strtotime($datetime));

    }

}

function filter_check($text, $html="", $tag="")
{

    global $bbs_id;

    if ($html) {

        $source = array();
        $target = array();

        $source[] = "//";
        $target[] = "";

         // 자동 줄바꿈
        if ($html == 2) {

            $source[] = "/\r\n/";
            $target[] = "<br/>";

            $source[] = "/\n/";
            $target[] = "<br/>";

            $text = auto_link($text);

        }

        $text = preg_replace($source, $target, $text);

        if (!$tag) {

            $text = preg_replace('@<(\/?(?:html|body|head|title|meta|base|link|script|style|applet|gsound|frame|frameset|layer|ilayer|object|xml|plaintext|form)(/*).*?>)@i', '&lt;$1', $text);
            $text = preg_replace_callback('@<(/?)([a-z]+[0-9]?)((?>"[^"]*"|\'[^\']*\'|[^>])*?\b(?:on[a-z]+|data|style|background|href|(?:dyn|low)?src)\s*=[\s\S]*?)(/?)($|>|<)@i', 'filter_callback', $text);

        }

        // 이런 경우를 방지함 <IMG STYLE="xss:expr/*XSS*/ession(alert('XSS'))">
        $text = preg_replace("#\/\*.*\*\/#iU", "", $text);

        //$text = preg_replace("/(on)([a-z]+)([^a-z]*)(\=)/i", "&#111;&#110;$2$3$4", $text);
        $text = preg_replace("/(dy)(nsrc)/i", "&#100;&#121;$2", $text);
        $text = preg_replace("/(lo)(wsrc)/i", "&#108;&#111;$2", $text);
        $text = preg_replace("/(sc)(ript)/i", "&#115;&#99;$2", $text);
        $text = preg_replace("/\<(\w|\s|\?)*(xml)/i", "", $text);

        $pattern = "";
        $pattern .= "(e|&#(x65|101);?)";
        $pattern .= "(x|&#(x78|120);?)";
        $pattern .= "(p|&#(x70|112);?)";
        $pattern .= "(r|&#(x72|114);?)";
        $pattern .= "(e|&#(x65|101);?)";
        $pattern .= "(s|&#(x73|115);?)";
        $pattern .= "(s|&#(x73|115);?)";
        $pattern .= "(i|&#(x6a|105);?)";
        $pattern .= "(o|&#(x6f|111);?)";
        $pattern .= "(n|&#(x6e|110);?)";
        $text = preg_replace("/".$pattern."/i", "__EXPRESSION__", $text);

    } else {

        //$text = preg_replace("/&amp;/", "&", $text);

        // & 처리 : &amp; &nbsp; 등의 코드를 정상 출력함
        $text = text_symbol($text);

        // 공백 처리
        $text = str_replace("  ", "&nbsp; ", $text);
        $text = str_replace("\n ", "\n&nbsp;", $text);

        $text = text($text, 1);

        $text = auto_link($text);

    }

    return $text;

}

function filter_callback($match)
{

    $tag = strtolower($match[2]);

    if ($tag == 'xmp') {

        return "<{$match[1]}xmp>";

    }

    if ($match[1]) {

        return $match[0];

    }

    if ($match[4]) {

        $match[4] = ' ' . $match[4];

    }

    $attrs = array();
    if (preg_match_all('/([\w:-]+)\s*=(?:\s*(["\']))?(?(2)(.*?)\2|([^ ]+))/s', $match[3], $m)) {

        foreach($m[1] as $idx => $name) {

            //$name = strtolower($name);

            if (strlen($name) >= 2 && substr_compare($name, 'on', 0, 2) === 0) {

                continue;

            }

            $val = preg_replace('/&#(?:x([a-fA-F0-9]+)|0*(\d+));/', 'chr("\\1"?0x00\\1:\\2+0)', $m[3][$idx] . $m[4][$idx]);
            $val = preg_replace('/^\s+|[\t\n\r]+/', '', $val);

            if (preg_match('/^[a-z]+script:/i', $val)) {

                continue;

            }

            $attrs[$name] = $val;

        }

    }

    if (isset($attrs['style']) && preg_match('@(?:/\*|\*/|\n|:\s*expression\s*\()@i', $attrs['style'])) {
        unset($attrs['style']);
    }

    $attr = array();
    foreach($attrs as $name => $val) {

        if ($tag == 'object' || $tag == 'embed' || $tag == 'a') {

            $attribute = strtolower(trim($name));

            if ($attribute == 'data' || $attribute == 'src' || $attribute == 'href') {

                if (stripos($val, 'data:') === 0) {

                    continue;

                }

            }

        }

        if ($tag == 'img') {

            $attribute = strtolower(trim($name));

            if (stripos($val, 'data:') === 0) {

                continue;

            }

        }

        $val = str_replace('"', '&quot;', $val);
        $attr[] = $name . "=\"{$val}\"";

    }

    $attr = count($attr) ? ' ' . implode(' ', $attr) : '';

    return "<{$match[1]}{$tag}{$attr}{$match[4]}>";

}

function text_bgm($matches)
{

    global $web, $bbs_id, $article_id;

    $text = $matches[0];

    if (preg_match("/(youtube\.com)/", $text) && !preg_match("/(autoplay=1|autoplay=true)/i", $text)) {

        return $text;

    }

    else if (preg_match("/(\.swf|bgmstore|heartbrea|googledrive|autoplay=1)/", $text)) {

        return false;

    } else {

        return $text;

    }

}

function text_symbol($str)
{

    return preg_replace("/\&([a-z0-9]{1,20}|\#[0-9]{0,3});/i", "&#038;\\1;", $str);

}

function text_cut($text, $length, $suffix="")
{

    if (!$text) {

        return false;

    }

    $text = strip_tags(htmlspecialchars_decode($text));
    $text = text2($text,1);
    $text = preg_replace("/\&nbsp;/i", " ", $text);

    if (!$length) {

        return $text;

    }

    $data = mb_substr(text($text), 0, $length, "UTF-8");

    if (mb_strlen($text, "UTF-8") > $length) {

        return $data.$suffix;

    } else {

        return $data;

    }

}

function text_mbcut($text, $start="0", $length="0", $suffix="")
{

    if (!$text) {

        return false;

    }

    $text = strip_tags(htmlspecialchars_decode($text));
    $text = text2($text,1);
    $text = preg_replace("/\&nbsp;/i", " ", $text);

    $data = mb_substr(text($text), $start, $length, "UTF-8");

    if (mb_strlen($text, "UTF-8") > $length) {

        return $data.$suffix;

    } else {

        return $data;

    }

}

function text_split($mark, $text, $n)
{

    $list = array();
    $s = explode($mark, $text);
    for ($i=0; $i<count($s); $i++) {

        $list[$i] = $s[$i];

    }

    return $list[$n];

}

function text_first($text, $mode)
{

    $arr_cho = array("ㄱ", "ㄲ", "ㄴ", "ㄷ", "ㄸ", "ㄹ", "ㅁ","ㅂ", "ㅃ", "ㅅ", "ㅆ", "ㅇ", "ㅈ", "ㅉ","ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ");
    $arr_jung = array("ㅏ", "ㅐ", "ㅑ", "ㅒ", "ㅓ", "ㅔ", "ㅕ","ㅖ", "ㅗ", "ㅘ", "ㅙ", "ㅚ", "ㅛ", "ㅜ","ㅝ", "ㅞ", "ㅟ", "ㅠ", "ㅡ", "ㅢ", "ㅣ");
    $arr_jong = array("", "ㄱ", "ㄲ", "ㄳ", "ㄴ", "ㄵ", "ㄶ","ㄷ", "ㄹ", "ㄺ", "ㄻ", "ㄼ", "ㄽ", "ㄾ","ㄿ", "ㅀ", "ㅁ", "ㅂ", "ㅄ", "ㅅ", "ㅆ","ㅇ", "ㅈ", "ㅊ", "ㅋ", "ㅌ", "ㅍ", "ㅎ");

    $unicode = array();
    $values = array();
    $lookingFor = 1;

    for ($i=0, $loop=strlen($text);$i<$loop;$i++) {

        $thisValue = ord($text[$i]);

        if ($thisValue < 128) {

            $unicode[] = $thisValue;

        } else {

            if (count($values) == 0) {

                $lookingFor = $thisValue < 224 ? 2 : 3;

            }

            $values[] = $thisValue;

            if (count($values) == $lookingFor) {

                $number = $lookingFor == 3 ? (($values[0]%16)*4096)+(($values[1]%64)*64)+($values[2]%64) : (($values[0]%32)*64)+($values[1]%64);
                $unicode[] = $number;
                $values = array();
                $lookingFor = 1;

            }

        }

    }

    $splitStr = '';
    while (list($key,$code) = each($unicode)) {

        if ($code >= 44032 && $code <= 55203) {

            $temp = $code-44032;

            $cho = (int)($temp/21/28);
            $jung = (int)(($temp%(21*28)/28));
            $jong = (int)($temp%28);

            if ($mode == '1') {

                $splitStr.= $arr_cho[$cho];

            } else {

                $splitStr.= $arr_cho[$cho].$arr_jung[$jung].$arr_jong[$jong];

            }

        } else {

            $temp = array($unicode[$key]);

            foreach ($temp as $ununicode) {

                if ($ununicode < 128) {

                    $splitStr.= chr($ununicode);

                }

                else if ($ununicode < 2048) {

                    $splitStr.= chr(192+(($ununicode-($ununicode%64))/64));
                    $splitStr.= chr(128+($ununicode%64));

                } else {

                    $splitStr.= chr(224+(($ununicode-($ununicode%4096))/4096));
                    $splitStr.= chr(128+((($ununicode%4096)-($ununicode%64))/64));
                    $splitStr.= chr(128+($ununicode%64));

                }

            }

        }

    }

    //$splitStr = str_replace(' ','',$splitStr);

    return $splitStr;

}

function auto_link($str)
{

    $str = str_replace(array("&lt;", "&gt;", "&amp;", "&quot;", "&nbsp;", "&#039;"), array("\t_lt_\t", "\t_gt_\t", "&", "\"", "\t_nbsp_\t", "'"), $str);
    $str = preg_replace("/([^(href=\"?'?)|(src=\"?'?)]|\(|^)((http|https|ftp|telnet|news|mms):\/\/[a-zA-Z0-9\.-]+\.[가-힣\xA1-\xFEa-zA-Z0-9\.:&#=_\?\/~\+%@;\-\|\,\(\)]+)/i", "\\1<A HREF=\"\\2\" TARGET=\"_BLANK\">\\2</A>", $str);
    $str = preg_replace("/(^|[\"'\s(])(www\.[^\"'\s()]+)/i", "\\1<A HREF=\"http://\\2\" TARGET=\"_BLANK\">\\2</A>", $str);
    $str = preg_replace("/[0-9a-z_-]+@[a-z0-9._-]{4,}/i", "<a href=\"mailto:\\0\">\\0</a>", $str);
    $str = str_replace(array("\t_nbsp_\t", "\t_lt_\t", "\t_gt_\t", "'"), array("&nbsp;", "&lt;", "&gt;", "&#039;"), $str);
    $str = preg_replace_callback("/(<A HREF=\")(.*)(<\/a>)/i", 'auto_link_callback', $str);

    return $str;

}

function auto_link2($str)
{

    $str = str_replace(array("&lt;", "&gt;", "&amp;", "&quot;", "&nbsp;", "&#039;"), array("\t_lt_\t", "\t_gt_\t", "&", "\"", "\t_nbsp_\t", "'"), $str);
    $str = preg_replace("/([^(href=\"?'?)|(src=\"?'?)]|\(|^)((http|https|ftp|telnet|news|mms):\/\/[a-zA-Z0-9\.-]+\.[가-힣\xA1-\xFEa-zA-Z0-9\.:&#=_\?\/~\+%@;\-\|\,\(\)]+)/i", "\\1<A HREF=\"\\2\" TARGET=\"_BLANK\">[새창열기]</A>", $str);
    $str = preg_replace("/(^|[\"'\s(])(www\.[^\"'\s()]+)/i", "\\1<A HREF=\"http://\\2\" TARGET=\"_BLANK\">\\2</A>", $str);
    $str = preg_replace("/[0-9a-z_-]+@[a-z0-9._-]{4,}/i", "<a href=\"mailto:\\0\">\\0</a>", $str);
    $str = str_replace(array("\t_nbsp_\t", "\t_lt_\t", "\t_gt_\t", "'"), array("&nbsp;", "&lt;", "&gt;", "&#039;"), $str);
    $str = preg_replace_callback("/(<A HREF=\")(.*)(<\/a>)/i", 'auto_link_callback', $str);

    return $str;

}

function auto_link_callback($matches)
{

    $text = $matches[0];

    $text = preg_replace("/&#038;amp;/", "&amp;", $text);

    return $text;

}

function week_today()
{

    global $web;

    return week(date("w", $web['server_time']));

}

function week($w)
{

    if ($w == '1') {

        $msg = "월";

    }

    else if ($w == '2') {

        $msg = "화";

    }

    else if ($w == '3') {

        $msg = "수";

    }

    else if ($w == '4') {

        $msg = "목";

    }

    else if ($w == '5') {

        $msg = "금";

    }

    else if ($w == '6') {

        $msg = "토";

    }

    else if ($w == '0') {

        $msg = "일";

    }

    return $msg;

}

function url_http($url)
{

    if (!trim($url)) {

        return;

    }

    if (!preg_match("/^(http|https|ftp|telnet|news|mms)\:\/\//i", $url)) {

        $url = "http://" . $url;

    }

    return text($url);

}

function dir_delete($file)
{

    if (file_exists($file)) {

        @chmod($file,0777);

        if (is_dir($file)) {

            $handle = opendir($file);

            while($filename = readdir($handle)) {

                if ($filename != "." && $filename != "..") {

                    dir_delete("$file/$filename");

                }

            }

            closedir($handle);
            rmdir($file);

        } else {

            unlink($file);

        }

    }

}

function is_utf8($string)
{

    // From http://w3.org/International/questions/qa-forms-utf-8.html
    return preg_match('%^(?:
          [\x09\x0A\x0D\x20-\x7E]            # ASCII
        | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
        |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
        | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
        |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
        |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
    )*$%xs', $string);

}

function email_send($to_email, $title, $content, $from_name, $from_email, $html_type)
{

    global $web;

    $from_name   = "=?".$web['charset']."?B?" . base64_encode($from_name) . "?=";
    $title = "=?".$web['charset']."?B?" . base64_encode($title) . "?=";

    $header   = array();
    $header[] = "Return-Path: <$from_email>";
    $header[] = "From: $from_name <$from_email>";
    $header[] = "Reply-To: <$from_email>";
    $header[] = "MIME-Version: 1.0";
    $header[] = "X-Mailer: ".$_SERVER['HTTP_HOST']." : {$_SERVER[SERVER_ADDR]} : {$_SERVER[REMOTE_ADDR]} : ".$_SERVER['HTTP_HOST']." : {$_SERVER[PHP_SELF]} : {$_SERVER[HTTP_REFERER]}";

    if ($html_type) {

        $header[] = "Content-Type: TEXT/HTML; charset=".$web['charset'];

        if ($html_type == '2') {

            $content = nl2br($content);

        }

    } else {

        $header[] = "Content-Type: TEXT/PLAIN; charset=".$web['charset'];
        $content = stripslashes($content);

    }

    $header[] = "Content-Transfer-Encoding: BASE64";
    $header[] = chunk_split(base64_encode($content));

    @mail($to_email, $title, "", implode("\r\n", $header));

}

function template_dir()
{

    global $web;

    $list = array();

    $dirname = $web['path']."/template/";
    $handle = opendir($dirname);
    while ($file = readdir($handle))
    {
        if($file == "."||$file == "..") continue;

        if (is_dir($dirname.$file)) $list[] = $file;
    }
    closedir($handle);
    sort($list);

    return $list;
}

function paging($write_pages, $cur_page, $total_page, $url, $add="")
{

    $str = "";
    $str .= "<table cellspacing='0' cellpadding='0' border='0' class='paging'><tr><td>";

    if ($cur_page > 1) {

        $str .= "<td><a href='" . $url . "{$add}&amp;p=1' class='prev1'></a></td>";

    }

    $start_page = ( ( (int)( ($cur_page - 1 ) / $write_pages ) ) * $write_pages ) + 1;
    $end_page = $start_page + $write_pages - 1;

    if ($end_page >= $total_page) $end_page = $total_page;

    if ($start_page > 1) {

        $str .= "<td><a href='" . $url . ($start_page-1) . "{$add}' class='prev2'></a></td>";

    }

    if ($total_page > 1) {

        $n = 0;

        for ($k=$start_page;$k<=$end_page;$k++) {

            $n++;

            if ($cur_page != $k) {

                $str .= "<td class='number".$n."'><a href='{$url}{$k}' class='off'>$k</a></td>";

            } else {

                $str .= "<td class='number".$n."'><a href='{$url}{$k}' class='on'>$k</a></td>";
            }

        }

    }

    if ($total_page > $end_page) {

        $str .= "<td><a href='" . $url . ($end_page+1) . "{$add}' class='next1'></a></td>";

    }

    if ($cur_page < $total_page) {

        $str .= "<td><a href='$url$total_page{$add}' class='next2'></a></td>";

    }

    $str .= "</tr></table>";

    return $str;

}

function set_session($session_name, $value)
{

    if (PHP_VERSION < '5.3.0')
        session_register($session_name);

    $$session_name = $_SESSION["$session_name"] = $value;

}

function get_session($session_name)
{

    return $_SESSION[$session_name];

}

function set_cookie($cookie_name, $value, $expire)
{

    global $web;

    setcookie(md5($cookie_name), base64_encode($value), $web['server_time'] + $expire, '/', $web['cookie_domain']);

}

function get_cookie($cookie_name)
{

    return base64_decode($_COOKIE[md5($cookie_name)]);
    //return $_COOKIE[md5($cookie_name)];

}

function visit_mobile()
{

    $agent = trim(strip_tags(sql_real_escape_string($_SERVER['HTTP_USER_AGENT'])));

    if (preg_match("/(Mobile|Android)/", $agent)) {

        return true;

    } else {

        return false;

    }

}

// 브라우저
function visit_browser($agent)
{

    $agent = strtolower(text($agent));

    if (preg_match("/msie 5.0[0-9]*/", $agent)) { $data = "MSIE 5"; }
    else if (preg_match("/msie 5.5[0-9]*/", $agent)) { $data = "MSIE 5.5"; }
    else if (preg_match("/msie 6.0[0-9]*/", $agent)) { $data = "MSIE 6"; }
    else if (preg_match("/msie 7.0[0-9]*/", $agent)) { $data = "MSIE 7"; }
    else if (preg_match("/msie 8.0[0-9]*/", $agent)) { $data = "MSIE 8"; }
    else if (preg_match("/msie 9.0[0-9]*/", $agent)) { $data = "MSIE 9"; }
    else if (preg_match("/msie 10.0[0-9]*/", $agent)) { $data = "MSIE 10"; }
    else if (preg_match("/msie 11.0[0-9]*/", $agent)) { $data = "MSIE 11"; }
    else if (preg_match("/msie 12.0[0-9]*/", $agent)) { $data = "MSIE 12"; }
    else if (preg_match("/msie 13.0[0-9]*/", $agent)) { $data = "MSIE 13"; }
    else if (preg_match("/msie 14.0[0-9]*/", $agent)) { $data = "MSIE 14"; }
    else if (preg_match("/msie 15.0[0-9]*/", $agent)) { $data = "MSIE 15"; }
    else if (preg_match("/msie 4.[0-9]*/", $agent)) { $data = "MSIE 4.x"; }
    else if (preg_match("/trident\/4.[0-9]/", $agent)) { $data = "MSIE 8"; }
    else if (preg_match("/trident\/5.[0-9]/", $agent)) { $data = "MSIE 9"; }
    else if (preg_match("/trident\/6.[0-9]/", $agent)) { $data = "MSIE 10"; }
    else if (preg_match("/trident\/7.[0-9]/", $agent)) { $data = "MSIE 11"; }
    else if (preg_match("/trident\/8.[0-9]/", $agent)) { $data = "MSIE 12"; }
    else if (preg_match("/trident\/9.[0-9]/", $agent)) { $data = "MSIE 13"; }
    else if (preg_match("/edge/", $agent)) { $data = "Edge"; }
    else if (preg_match("/firefox/", $agent)) { $data = "Firefox"; }
    else if (preg_match("/chrome/", $agent)) { $data = "Chrome"; }
    else if (preg_match("/safari/", $agent)) { $data = "Safari"; }
    else if (preg_match("/x11/", $agent)) { $data = "Netscape"; }
    else if (preg_match("/opera/", $agent)) { $data = "Opera"; }
    else if (preg_match("/gec/", $agent)) { $data = "Gecko"; }
    else if (preg_match("/bot|slurp|daumoa/", $agent)) { $data = "Robot"; }
    else if (preg_match("/internet explorer/", $agent)) { $data = "IE"; }
    else if (preg_match("/mozilla/", $agent)) { $data = "Mozilla"; }
    else { $data = "기타"; }

    return $data;

}

// OS
function visit_os($agent)
{

    $agent = strtolower(text($agent));

    if (preg_match("/iphone/", $agent)) { $data = "iPhone"; } //iPhone
    else if (preg_match("/ipad/", $agent)) { $data = "iPad"; } //iPad
    else if (preg_match("/iPod/", $agent)) { $data = "iPod"; } //iPod
    else if (preg_match("/android/", $agent)) { $data = "Android"; } //Android
    else if (preg_match("/berry/", $agent)) { $data = "BlackBerry"; } //BlackBerry
    else if (preg_match("/symbian/", $agent)) { $data = "Symbian"; } //Symbian
    else if  (preg_match("/windows 98/", $agent)) { $data = "98"; }
    else if (preg_match("/windows 95/", $agent)) { $data = "95"; }
    else if (preg_match("/windows nt 4\.[0-9]*/", $agent)) { $data = "NT"; }
    else if (preg_match("/windows nt 5\.0/", $agent)) { $data = "2000"; }
    else if (preg_match("/windows nt 5\.1/", $agent)) { $data = "XP"; }
    else if (preg_match("/windows nt 5\.2/", $agent)) { $data = "2003"; }
    else if (preg_match("/windows nt 6\.0/", $agent)) { $data = "Vista"; }
    else if (preg_match("/windows nt 6\.1/", $agent)) { $data = "Windows7"; }
    else if (preg_match("/windows nt 6\.2/", $agent)) { $data = "Windows8"; }
    else if (preg_match("/windows nt 6\.3/", $agent)) { $data = "Windows8.1"; }
    else if (preg_match("/windows nt 10\.0/", $agent)) { $data = "Windows10"; }
    else if (preg_match("/windows 9x/", $agent)) { $data = "ME"; }
    else if (preg_match("/windows ce/", $agent)) { $data = "CE"; }
    else if (preg_match("/mac/", $agent)) { $data = "MAC"; }
    else if (preg_match("/linux/", $agent)) { $data = "Linux"; }
    else if (preg_match("/sunos/", $agent)) { $data = "sunOS"; }
    else if (preg_match("/irix/", $agent)) { $data = "IRIX"; }
    else if (preg_match("/mobile|phone|skt|lge|0450/", $agent)) { $data = "Mobile"; }
    else if (preg_match("/bot|slurp|daumoa/", $agent)) { $data = "Robot"; }
    else if (preg_match("/internet explorer/", $agent)) { $data = "IE"; }
    else if (preg_match("/mozilla/", $agent)) { $data = "Mozilla"; }
    else { $data = "기타"; }

    return $data;

}

// HOST
function visit_host($referer)
{

    if (!$referer) {

        return false;

    }

    $host = preg_match("/^http[s]*:\/\/([\.\-\_0-9a-zA-Z]*)\//", $referer, $match);
    $host = trim($match[1]);

    if ($host) {

        return $host;

    } else {

        return false;

    }

}

function visit_keyword($ref)
{

    if (!$ref) {

        return false;

    }

    $parameter = "\?query=|\&query=|\?q=|\&q=";

    if (preg_match("/($parameter)/", $ref) && (!preg_match("/(\?seq=)/", $ref))) {

        $keyword = preg_replace("/(.*)($parameter)(.*)/", "$3", $ref);

        if (preg_match("/(&)/", $keyword)) {

            $keyword = preg_match("/(.*)\&/U", $keyword, $match);
            $keyword = trim($match[1]);

        }

        if (is_utf8(urldecode($keyword))) { $keyword = urldecode($keyword); } else { $keyword = mb_convert_encoding(urldecode($keyword), 'UTF-8', 'CP949'); }

        if ($keyword && $keyword != '1') {

            return $keyword;

        } else {

            return false;

        }

    }

    return false;

}

function sql_connect($host, $user, $pass, $db)
{

    global $web;

    if ($web['mysqli']) {

        return @mysqli_connect($host, $user, $pass, $db);

    } else {

        return @mysql_connect($host, $user, $pass);

    }

}

function sql_select_db($db, $connect)
{

    global $web;

    if ($web['mysqli']) {

        if (strtolower($web['charset']) == 'utf-8') @mysqli_query($connect, " set names utf8 ");
        else if (strtolower($web['charset']) == 'euc-kr') @mysqli_query($connect, " set names euckr ");
        return @mysqli_select_db($connect, $db);

    } else {

        if (strtolower($web['charset']) == 'utf-8') @mysql_query(" set names utf8 ");
        else if (strtolower($web['charset']) == 'euc-kr') @mysql_query(" set names euckr ");
        return @mysql_select_db($db, $connect);

    }

}

function sql_query($sql, $error=TRUE)
{

    global $web;

    if ($web['mysqli']) {
    
        if ($error)
            $result = @mysqli_query($web['sql_connect'], $sql) or die("<p>{$sql}<p>" . mysqli_errno($web['sql_connect']) . " : " .  mysqli_errno($web['sql_connect']) . "<p>error file : {$_SERVER[PHP_SELF]}");
        else
            $result = @mysqli_query($web['sql_connect'], $sql);
        return $result;

    } else {

        if ($error)
            $result = @mysql_query($sql) or die("<p>$sql<p>" . mysql_errno() . " : " .  mysql_error() . "<p>error file : $_SERVER[PHP_SELF]");
        else
            $result = @mysql_query($sql);
        return $result;

    }

}

function sql_fetch($sql, $error=TRUE)
{

    $result = sql_query($sql, $error);
    $row = sql_fetch_array($result);
    return $row;

}

function sql_fetch_array($result)
{

    global $web;

    if ($web['mysqli']) {

        $row = @mysqli_fetch_assoc($result);

    } else {

        $row = @mysql_fetch_assoc($result);

    }

    return $row;

}

function sql_free_result($result)
{

    global $web;

    if ($web['mysqli']) {

        return mysqli_free_result($result);

    } else {

        return mysql_free_result($result);

    }

}

function sql_insert_id()
{

    global $web;

    if ($web['mysqli']) {

        return mysqli_insert_id($web['sql_connect']);

    } else {

        return mysql_insert_id($web['sql_connect']);

    }

}

function sql_password($value)
{

    $row = sql_fetch(" select password('$value') as pass ");
    return $row[pass];

}

function sql_real_escape_string($value)
{

    global $web;

    if ($value) {

        if ($web['mysqli']) {

            return mysqli_real_escape_string($web['sql_connect'], $value);

        } else {

            return mysql_real_escape_string($value);

        }

    } else {

        return false;

    }

}

function sql_table_copy($table, $crlf="\n")
{

    global $web;

    // For MySQL < 3.23.20
    $schema_create .= 'CREATE TABLE ' . $table . ' (' . $crlf;

    $sql = 'SHOW FIELDS FROM ' . $table;
    $result = sql_query($sql);
    while ($row = sql_fetch_array($result))
    {
        $schema_create .= '    ' . $row['Field'] . ' ' . $row['Type'];
        if (isset($row['Default']) && $row['Default'] != '')
        {
            $schema_create .= ' DEFAULT \'' . $row['Default'] . '\'';
        }
        if ($row['Null'] != 'YES')
        {
            $schema_create .= ' NOT NULL';
        }
        if ($row['Extra'] != '')
        {
            $schema_create .= ' ' . $row['Extra'];
        }
        $schema_create     .= ',' . $crlf;
    } // end while
    sql_free_result($result);

    $schema_create = preg_replace('/,' . $crlf . '$/', '', $schema_create);

    $sql = 'SHOW KEYS FROM ' . $table;
    $result = sql_query($sql);
    while ($row = sql_fetch_array($result))
    {
        $kname    = $row['Key_name'];
        $comment  = (isset($row['Comment'])) ? $row['Comment'] : '';
        $sub_part = (isset($row['Sub_part'])) ? $row['Sub_part'] : '';

        if ($kname != 'PRIMARY' && $row['Non_unique'] == 0) {
            $kname = "UNIQUE|$kname";
        }
        if ($comment == 'FULLTEXT') {
            $kname = 'FULLTEXT|$kname';
        }
        if (!isset($index[$kname])) {
            $index[$kname] = array();
        }
        if ($sub_part > 1) {
            $index[$kname][] = $row['Column_name'] . '(' . $sub_part . ')';
        } else {
            $index[$kname][] = $row['Column_name'];
        }
    } // end while
    sql_free_result($result);

    while (list($x, $columns) = @each($index)) {
        $schema_create     .= ',' . $crlf;
        if ($x == 'PRIMARY') {
            $schema_create .= '    PRIMARY KEY (';
        } else if (substr($x, 0, 6) == 'UNIQUE') {
            $schema_create .= '    UNIQUE ' . substr($x, 7) . ' (';
        } else if (substr($x, 0, 8) == 'FULLTEXT') {
            $schema_create .= '    FULLTEXT ' . substr($x, 9) . ' (';
        } else {
            $schema_create .= '    KEY ' . $x . ' (';
        }
        $schema_create     .= implode($columns, ', ') . ')';
    } // end while

    if (strtolower($web['charset']) == "utf-8")
        $schema_create .= $crlf . ') DEFAULT CHARSET=utf8';
    else
        $schema_create .= $crlf . ')';

    return $schema_create;

}
?>