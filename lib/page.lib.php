<?php // 페이지
if (!defined("_WEB_")) exit;

function page($page_id)
{

    if ($page_id) { $page_id = preg_match("/^[a-zA-Z0-9_\-]+$/", $page_id) ? $page_id : ""; }

    global $web;

    if (!$page_id) {
        return false;
    }

    $data = sql_fetch(" select * from $web[page_table] where page_id = '".$page_id."' limit 0, 1 ");

    return $data;

}

function page_group($page_group)
{

    if ($page_group) { $page_group = preg_match("/^[a-zA-Z0-9_\-]+$/", $page_group) ? $page_group : ""; }

    global $web;

    if (!$page_group) {
        return false;
    }

    $data = sql_fetch(" select * from $web[page_group_table] where page_group = '".$page_group."' limit 0, 1 ");

    return $data;

}

function page_group_count()
{

    global $web;

    $result = sql_query(" select * from $web[page_group_table] ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $chk = sql_fetch(" select count(page_id) as cnt from $web[page_table] where page_group = '".addslashes($row['page_group'])."' ");

        $sql_common = "";
        $sql_common .= " set page_count = '".(int)($chk['cnt'])."' ";

        sql_query(" update $web[page_group_table] $sql_common where page_group = '".addslashes($row['page_group'])."' ");

    }

    return false;

}

function page_skin_dir()
{

    global $web;

    $list = array();

    $dirname = $web['path']."/page/skin/";
    $handle = opendir($dirname);
    while ($file = readdir($handle)) 
    {
        if($file == "."||$file == "..") continue;

        if (is_dir($dirname.$file)) $list[] = $file;
    }
    closedir($handle);
    sort($list);

    return $list;
}
?>