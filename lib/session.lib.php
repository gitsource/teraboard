<?php
if (!defined('_WEB_')) exit;

function web_session_open($save_path, $session_name)
{

    //mysqli_select_db
    return true;

}

function web_session_close()
{

    global $web;

    if ($web['mysqli']) {

        return mysqli_close($web['sql_connect']);

    } else {

        return mysql_close($web['sql_connect']);

    }

}

function web_session_read($id)
{

    global $web;

    $id = sql_real_escape_string($id);

    $row = sql_fetch(" select ss_data from $web[session_table] where id = '$id' limit 0, 1 ");

    return $row['ss_data'];

}

function web_session_write($id, $data)
{

    global $web;

    $id = sql_real_escape_string($id);
    $ss_data = sql_real_escape_string($data);

    $ss_datetime = $web['time_ymdhis'];

    $query = sql_query(" replace into $web[session_table] values ('$id', '$ss_datetime', '$ss_data') ");

    return $query;

}

function web_session_destroy($id)
{

    global $web;

    $id = sql_real_escape_string($id);

    $query = sql_query(" delete from $web[session_table] where id = '$id' ");

    return $query;

}

function web_session_clean($clean)
{

    global $web;

    $time = date("Y-m-d H:i:s", $web['server_time'] - $clean);

    $query = sql_query(" delete from $web[session_table] where ss_datetime < '$time' ");

    return $query;

}
?>