<?php //신디케이션
if (!defined("_WEB_")) exit;

function syndi_ping()
{

    global $web, $setup_api;

    if (!$setup_api['syndi_onoff'] || !$setup_api['syndi_token']) {
        return false;
    }

    $ping_auth_header = "Authorization: Bearer ".addslashes($setup_api['syndi_token']);
    $ping_url = urlencode($web['host_rbbs']."/syndi.xml");
    $ping_client_opt = array(
    CURLOPT_URL => "https://apis.naver.com/crawl/nsyndi/v2",
    CURLOPT_POST => true,
    CURLOPT_POSTFIELDS => "ping_url=" . $ping_url, 
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_CONNECTTIMEOUT => 10, 
    CURLOPT_TIMEOUT => 10, 
    CURLOPT_HTTPHEADER => array("Host: apis.naver.com", "Pragma: no-cache", "Accept: */*", $ping_auth_header)
    );

    $ping = curl_init();
    curl_setopt_array($ping, $ping_client_opt);
    $query = curl_exec($ping);
    curl_close($ping);

    return true;

}

function syndi_create($file, $content)
{

    if (file_exists($file)) {

        @chmod($file, 0707);

    }

    $f = fopen($file, "w");
    @fwrite($f, $content);
    @fclose($f);
    @chmod($file, 0606);

    return syndi_ping();

}

function syndi_article($bbs_id, $article_id, $mode)
{

    global $web, $setup;

    if (!$setup['company']) {

        $setup['company'] = $setup['title'];

    }

    if (!$bbs_id || !$article_id || !$mode) {

        return false;

    }

    $timestamp = date("Y-m-d\\TH:i:s", $web['server_time']). "+09:00";

    $bbs = bbs($bbs_id);

    if (!$bbs['bbs_id']) {

        return false;

    }

    if ($mode == 'regist') {

        $article = article($bbs_id, $article_id);

        if (!$article['id']) {

            return false;

        }

        $datetime = date("Y-m-d\\TH:i:s", strtotime($article['datetime'])). "+09:00";

    }

    ob_start();

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    echo "<feed xmlns=\"http://webmastertool.naver.com\">\n";
    echo "<id>".text($web['host_default'])."</id>\n";
    echo "<title>".text($setup['title'])."</title>\n";
    echo "<author>\n";
    echo "<name>".text($setup['company'])."</name>\n";
    echo "<email>".text($setup['email'])."</email>\n";
    echo "</author>\n";
    echo "<updated>".$timestamp."</updated>\n";
    echo "<link rel=\"site\" href=\"".text($web['host_default'])."\" title=\"".text($setup['title'])."\" />\n";

    if ($mode == 'regist') {

        echo "<entry>\n";
        echo "<id>".http_bbs($bbs_id, $article_id)."</id>\n";
        echo "<title><![CDATA[".text($article['ar_title'])."]]></title>\n";
        echo "<author>\n";
        echo "<name>".text($article['nick'])."</name>\n";
        echo "</author>\n";
        echo "<updated>".$datetime."</updated>\n";
        echo "<published>".$datetime."</published>\n";
        echo "<link rel=\"via\" href=\"".http_bbs($bbs_id, "")."\" title=\"".text($bbs['bbs_title'])."\" />\n";
        //echo "<link rel=\"mobile\" href=\"\" />\n";
        echo "<content type=\"html\"><![CDATA[".text2($article['ar_content'],1)."]]></content>\n";
        echo "<summary type=\"text\"><![CDATA[".text3($article['ar_content'])."]]></summary>\n";
        //echo "<category term=\"search_info\" label=\"\" />\n";
        echo "</entry>\n";

    }

    else if ($mode == 'delete') {

        $datetime = date("Y-m-d\\TH:i:s", $web['server_time']). "+09:00";

        echo "<deleted-entry ref=\"".http_bbs($bbs_id, $article_id)."\" when=\"".$datetime."\" />\n";

    }

    echo "</feed>\n";

    $content = ob_get_contents();
    ob_end_clean();

    return $content;

}
?>