<?php // 회원
if (!defined("_WEB_")) exit;

function member($mid)
{

    if ($mid) { $mid = preg_match("/^[0-9]+$/", $mid) ? $mid : ""; }

    if (!$mid) {

        return false;

    }

    global $web;

    $data = sql_fetch(" select * from $web[member_table] where mid = '".$mid."' limit 0, 1 ");

    return $data;

}

function member_uid($uid)
{

    if ($uid) { $uid = preg_match("/^[a-zA-Z0-9_\-]+$/", $uid) ? $uid : ""; }

    if (!$uid) {

        return false;

    }

    global $web;

    $data = sql_fetch(" select * from $web[member_table] where uid = '".$uid."' limit 0, 1 ");

    if ($data['uid']) {

        return $data;

    }

    return false;

}

function member_level($level)
{

    if ($level) { $level = preg_match("/^[0-9]+$/", $level) ? $level : ""; }

    if (!$level) {

        return false;

    }

    global $web;

    $data = sql_fetch(" select * from $web[member_level_table] where level = '".$level."' limit 0, 1 ");

    if ($data['level']) {

        return $data;

    }

    return false;

}

function member_level_auto($mid)
{

    global $web;

    $mb = member($mid);

    if (!$mb['mid']) {

        return false;

    }

    // 1보다 작거나, 8부터는 자동등업 없다.
    if ($mb['level'] <= 1 || $mb['level'] >= 8) {

        return false;

    }

    // 가입일수
    $day = (int)(((strtotime($web['time_ymdhis']) - strtotime($mb['datetime'])) / 86400) + 1);

    // 상위레벨 조건
    $level = $mb['level'] + 1;

    // 3~8까지만
    if ($level >= 3 && $level <= 8) {

        $data = sql_fetch(" select * from $web[member_level_table] where level = $level limit 0, 1 ");

        if ($data['levelup']) {

            if ($mb['bbs_write_count'] >= $data['article'] && $mb['bbs_reply_count'] >= $data['reply'] && $day >= $data['day']) {

                $sql_common = "";
                $sql_common .= " set level = '".$level."' ";

                sql_query(" update $web[member_table] $sql_common where mid = '".$mid."' ");

            }

        }

    }

    return false;

}

function member_file($mid, $upload_mode)
{

    if ($upload_mode) { $upload_mode = preg_match("/^[a-zA-Z0-9_\-]+$/", $upload_mode) ? $upload_mode : ""; }

    if (!$mid || !$upload_mode) {

        return false;

    }

    global $web;

    $data = sql_fetch(" select * from $web[member_file_table] where mid = '".$mid."' and upload_mode = '".$upload_mode."' limit 0, 1 ");

    if ($data['upload_file']) {

        return $data;

    }

    return false;

}

function message_receive($id)
{

    if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }

    if (!$id) {

        return false;

    }

    global $web;

    $data = sql_fetch(" select * from $web[message_receive_table] where id = '".$id."' limit 0, 1 ");

    return $data;

}

function message_send($id)
{

    if ($id) { $id = preg_match("/^[0-9]+$/", $id) ? $id : ""; }

    if (!$id) {

        return false;

    }

    global $web;

    $data = sql_fetch(" select * from $web[message_send_table] where id = '".$id."' limit 0, 1 ");

    return $data;

}

function member_type($data)
{

    global $web;

    if (!$data['mid']) {

        return false;

    }

    if ($data['dropout']) {

        $msg = "탈퇴";

    }

    else if ($data['stop']) {

        $msg = "이용정지";

    }

    else if ($data['social']) {

        $msg = "소셜회원";

    } else {

        $msg = "일반회원";

    }

    return $msg;

}

function member_social($id)
{

    if (!$id) {

        return false;

    }

    if ($id == 1) {

        return "네이버";

    }

    else if ($id == 2) {

        return "카카오톡";

    }

    else if ($id == 3) {

        return "페이스북";

    }

    else if ($id == 4) {

        return "트위터";

    }

    else if ($id == 5) {

        return "구글";

    }

    else if ($id == 6) {

        return "인스타그램";

    } else {

        return false;

    }

}

function member_sex($id)
{

    if (!$id) {

        return false;

    }

    if ($id == 1) {

        return "남자";

    }

    else if ($id == 2) {

        return "여자";

    } else {

        return false;

    }

}

function member_profile($text, $mb="")
{

    global $web, $setup;

    if (!$text) {

        return "&nbsp;";

    }

    $text = text2($text, 0);

    return $text;

}

function member_thumb($mid, $mb="", $file_id="")
{

    if ($mid) { $mid = preg_match("/^[0-9]+$/", $mid) ? $mid : ""; }
    if ($file_id) { $file_id = preg_match("/^[0-9]+$/", $file_id) ? $file_id : ""; }

    global $web, $disk;

    if (!$mid) {

        return "<img src='".$web['host_img']."/noimg_photo.png'>";

    }

    if (!$mb) {

        $mb = member($mid);

    }

    if ($mb['photo']) {

        return "<img src='".$web['host_img']."/photo/".text($mb['photo'])."' onclick=\"imageLoad(this, '".$web['host_img']."/photo/".text($mb['photo'])."', '200', '200');\" style='cursor:pointer;'>";

    }

    if ($file_id) {

        $file = sql_fetch(" select * from $web[member_photo_table] where id = '".$file_id."' limit 0, 1 ");

    } else {

        $file = sql_fetch(" select * from $web[member_photo_table] where mid = '".$mid."' and onoff = 1 limit 0, 1 ");

    }

    if ($file['upload_file'] && preg_match("/\.(jp[e]?g|gif|png)$/i", $file['upload_file'])) {

        $source = $disk['server_member']."/photo/".data_path("u", $file['upload_time'])."/".$file['upload_file'];

        return "<img src='".$disk['server_member']."/photo/".data_path("u", $file['upload_time'])."/".$file['upload_file']."' onclick=\"imageLoad(this, '".$source."', '".$file['upload_width']."', '".$file['upload_height']."');\" style='cursor:pointer;'>";

    } else {

        return "<img src='".$web['host_img']."/noimg_photo.png'>";

    }

}

function member_point($mid, $point, $ct, $content='', $chk='')
{

    if ($mid) { $mid = preg_match("/^[0-9]+$/", $mid) ? $mid : ""; }
    if ($point) { $point = preg_match("/^[0-9\.\-]+$/", $point) ? $point : ""; }

    global $web, $setup;

    if (!$setup['point_onoff']) {

        return false;

    }

    if (!$point || !$mid) {

        return false;

    }

    $mb = member($mid);

    if (!$mb['uid']) {

        return false;

    }

    $data = sql_fetch(" select mid from $web[member_point_table] where mid = '".$mid."' and chk = '".$chk."' limit 0, 1 ");

    if ($data['mid']) {

        return false;

    }

    if ($point < 0 && ($mb['point'] + $point) < 0) {

        if (!$mb['point']) {

            return false;

        }

        $sql_common = "";
        $sql_common .= " set mid = '".$mid."' ";
        $sql_common .= ", point = '".($mb['point'] * -1)."' ";
        $sql_common .= ", this_point = '0' ";
        $sql_common .= ", ct = '".trim(strip_tags(sql_real_escape_string($ct)))."' ";
        $sql_common .= ", content = '".trim(strip_tags(sql_real_escape_string($content)))."' ";
        $sql_common .= ", chk = '".trim(strip_tags(sql_real_escape_string($chk)))."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."'";

        sql_query(" insert into $web[member_point_table] $sql_common ");

        sql_query(" update $web[member_table] set point = 0 where mid = '".$mid."' ");

    } else {

        $sql_common = "";
        $sql_common .= " set mid = '".$mid."' ";
        $sql_common .= ", point = '".$point."' ";
        $sql_common .= ", this_point = '".($mb['point'] + $point)."' ";
        $sql_common .= ", ct = '".trim(strip_tags(sql_real_escape_string($ct)))."' ";
        $sql_common .= ", content = '".trim(strip_tags(sql_real_escape_string($content)))."' ";
        $sql_common .= ", chk = '".trim(strip_tags(sql_real_escape_string($chk)))."' ";
        $sql_common .= ", datetime = '".$web['time_ymdhis']."'";

        sql_query(" insert into $web[member_point_table] $sql_common ");

        sql_query(" update $web[member_table] set point = point + ".$point." where mid = '".$mid."' ");

    }

    return true;

}

// 회원탈퇴
function member_dropout($mid)
{

    if ($mid) { $mid = preg_match("/^[0-9]+$/", $mid) ? $mid : ""; }

    global $setup, $web, $disk;

    $data = member($mid);

    if (!$data['mid']) {
        return false;
    }

    if ($data['dropout']) {
        return false;
    }

    // 관리자 불가
    if ($data['level'] >= 10) {
        return false;
    }

    $sql_common = "";
    $sql_common .= " set dropout = '1' ";
    $sql_common .= ", dropout_datetime = '".$web['time_ymdhis']."' ";

    sql_query(" update $web[member_table] $sql_common where mid = '".$mid."' ");

    // 자동삭제 && 기간이 없을 때(즉시삭제)
    if ($setup['del_dropout_onoff'] && !$setup['del_dropout_day']) {

        member_delete($mid);

    }

    return true;

}

// 회원삭제
function member_delete($mid)
{

    if ($mid) { $mid = preg_match("/^[0-9]+$/", $mid) ? $mid : ""; }

    global $web, $disk;

    $data = member($mid);

    if (!$data['mid']) {
        return false;
    }

    // 관리자 불가
    if ($data['level'] >= 10) {
        return false;
    }

    $result = sql_query(" select * from $web[member_photo_table] where mid = '".$mid."' ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $file_path = $disk['path']."/photo/".data_path("u", $row['upload_time'])."/".$row['upload_file'];

        if (file_exists($file_path) && $row['upload_file']) {

            @unlink($file_path);

        }

    }

    $result = sql_query(" select * from $web[member_file_table] where mid = '".$mid."' ");
    for ($i=0; $row=sql_fetch_array($result); $i++) {

        $file_path = $disk['path']."/member/".data_path("u", $row['upload_time'])."/".$row['upload_file'];

        if (file_exists($file_path) && $row['upload_file']) {

            @unlink($file_path);

        }

    }

    sql_query(" delete from $web[member_photo_table] where mid = '".$mid."' ");
    sql_query(" delete from $web[member_file_table] where mid = '".$mid."' ");
    sql_query(" delete from $web[member_login_table] where mid = '".$mid."' ");
    sql_query(" delete from $web[message_receive_table] where mid = '".$mid."' ");
    sql_query(" delete from $web[message_send_table] where mid = '".$mid."' ");
    sql_query(" delete from $web[member_point_table] where mid = '".$mid."' ");
    sql_query(" delete from $web[member_table] where mid = '".$mid."' ");
    sql_query(" delete from $web[bbs_scrap_table] where mid = '".$mid."' ");

    return true;

}
?>