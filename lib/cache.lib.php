<?php
if (!defined("_WEB_")) exit;

function cache_write($cache_file, $content)
{

    if (file_exists($cache_file)) {

        @chmod($cache_file, 0707);

    }

    $f = fopen($cache_file, "w");
    @fwrite($f, $content);
    @fclose($f);
    @chmod($cache_file, 0606);

}

function cache_read($cache_file, $cache_time)
{

    global $web;

    if (!is_file($cache_file)) {

        return false;

    }

    if (!$cache_time) {

        return false;

    }

    // 파일 시간 + 캐시시간 = 현재시간
    $datetime = date("Y-m-d H:i:s", (filemtime($cache_file) + $cache_time));

    // 현재 시간이 캐시 시간보다 크다면
    if ($web['time_ymdhis'] >= $datetime) {

        return false;

    }

    ob_start();
    readfile($cache_file);
    $content = ob_get_contents();
    ob_end_clean();

    return $content;

}
?>