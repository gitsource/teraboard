<?php // 확장
if (!defined("_WEB_")) exit;
if ($m) { $m = preg_match("/^[a-zA-Z0-9_\-\.]+$/", $m) ? $m : ""; }
if ($c) { $c = preg_match("/^[a-zA-Z0-9_\-]+$/", $c) ? $c : ""; }
if ($q) { $q = preg_match("/^[A-Za-z0-9_가-힣\x20\/\-\.!,！@:\=\+]+$/", $q) ? $q : ""; $q = addslashes(urlencode(trim($q))); if (is_utf8(urldecode($q))) { $q = urldecode($q); } else { $q = mb_convert_encoding(urldecode($q), 'UTF-8', 'CP949'); } }
if ($p) { $p = preg_match("/^[0-9]+$/", $p) ? $p : ""; }
if ($sort) { $sort = preg_match("/^[a-zA-Z0-9_\-]+$/", $sort) ? $sort : ""; }
if ($rows) { $rows = preg_match("/^[0-9]+$/", $rows) ? $rows : ""; }
if ($order) { $order = preg_match("/^[a-zA-Z0-9_\-]+$/", $order) ? $order : ""; }
if ($mid) { $mid = preg_match("/^[0-9]+$/", $mid) ? $mid : ""; }
if ($uid) { $uid = preg_match("/^[a-zA-Z0-9_\-]+$/", $uid) ? $uid : ""; }
if ($bbs_group) { $bbs_group = preg_match("/^[a-zA-Z0-9_\-]+$/", $bbs_group) ? $bbs_group : ""; }
if ($bbs_id) { $bbs_id = preg_match("/^[a-zA-Z0-9_\-]+$/", $bbs_id) ? $bbs_id : ""; }
if ($article_id) { $article_id = preg_match("/^[0-9]+$/", $article_id) ? $article_id : ""; }
if ($reply_id) { $reply_id = preg_match("/^[0-9]+$/", $reply_id) ? $reply_id : ""; }
if ($ct) { $ct = preg_match("/^[가-힣a-zA-Z0-9_\-%\.,\/\& ]+$/", $ct) ? $ct : ""; }

if (function_exists('mysqli_connect')) { $web['mysqli'] = true; }

// database
$database_file = "tb-database.php";

if (file_exists("$web[path]/$database_file")) {
include_once("$web[path]/$database_file");
$web['sql_connect'] = sql_connect($mysql_host, $mysql_user, $mysql_password, $mysql_db);
$web['sql_select_db'] = sql_select_db($mysql_db, $web['sql_connect']);
if (!$web['sql_connect']) { url($web['host_default']."/error.html"); }
} else {
url($web['host_default']."/install/");
}

$setup = sql_fetch(" select * from $web[setup_table] limit 0, 1 ");
$setup_api = setup_api();

if ($setup['block_ip']) {

    $row = explode("|", $setup['block_ip']);
    for ($i=0; $i<count($row); $i++) {

        if ($row[$i] && $row[$i] == $_SERVER['REMOTE_ADDR']) {

            url($web['host_default']."/ip.html");

        }

    }

}

// template
$template_skin_path = $web['path']."/template/".$setup['template'];
$template_skin_url = $web['url']."/template/".$setup['template'];

ini_set("session.use_trans_sid", 0);
ini_set("url_rewriter.tags","");

include_once("$web[path]/lib/session.lib.php");
session_set_save_handler("web_session_open", "web_session_close", "web_session_read", "web_session_write", "web_session_destroy", "web_session_clean");
if (isset($SESSION_CACHE_LIMITER)) { @session_cache_limiter($SESSION_CACHE_LIMITER); } else { @session_cache_limiter("nocache"); }

ini_set("session.cache_expire", 360);
ini_set("session.gc_maxlifetime", 21600);
ini_set("session.gc_probability", 100);
ini_set("session.gc_divisor", 100);

session_set_cookie_params(0, "/");
ini_set("session.cookie_domain", $web['cookie_domain']);
@session_start();
if ($_REQUEST['PHPSESSID'] && $_REQUEST['PHPSESSID'] != session_id()) { url($web['host_member']."/logout.php"); }

if ($url) { $url = preg_replace("/\"/i", "", $url); $url = preg_replace("/'/i", "", $url); }

if (isset($url)) {

    $urlencode = urlencode($url);

} else {

    $urlencode = urlencode('http'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 's' : '').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

}

if ($_SESSION['ss_mid']) {

    $member = member(text_split("|", $_SESSION['ss_mid'], 0));

    // 브라우저와 아이피를 검증한다.
    //if (!$member['mid'] || md5($_SERVER['HTTP_USER_AGENT'].$member['datetime']) != text_split("|", $_SESSION['ss_mid'], 2) || $_SERVER['REMOTE_ADDR'] != text_split("|", $_SESSION['ss_mid'], 1)) {

    // 브라우저를 검증한다.
    if (!$member['mid'] || md5($_SERVER['HTTP_USER_AGENT'].$member['datetime']) != text_split("|", $_SESSION['ss_mid'], 2)) {

        session_unset();
        session_destroy();

        set_cookie("login_mid", "", 0);
        set_cookie("login", "", 0);

        url($web['host_default']);

    }

    // 탈퇴, 차단
    if ($member['dropout'] || $member['stop']) {

        session_unset();
        session_destroy();

        set_cookie("login_mid", "", 0);
        set_cookie("login", "", 0);

        url($web['host_default']);

    }

    if (substr($member['login_datetime'],0,10) != $web['time_ymd']) {

        $sql_common = "";
        $sql_common .= " set login_datetime = '".$web['time_ymdhis']."' ";
        $sql_common .= ", login_ip = '".trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])))."' ";

        sql_query(" update $web[member_table] $sql_common where mid = '".$member['mid']."' ");

        if ($setup['point_login']) {

            member_point($member['mid'], $setup['point_login'], 2, "로그인", $web['server_time'].rand(10000,99999));

        }

    }

} else {

    $cookie_mid = get_cookie("login_mid");

    if ($cookie_mid) {

        $cookie_mid = substr(preg_replace("/[^0-9]/", "", $cookie_mid), 0, 50);
        $chk = sql_fetch(" select * from $web[member_table] where mid = '".$cookie_mid."' limit 0, 1 ");
        if ($chk['uid']) {

            if ($chk['dropout'] || $chk['stop']) {

                set_cookie("login_mid", "", 0);
                set_cookie("login", "", 0);

                echo "<script type='text/javascript'>window.location.reload();</script>";
                exit;

            }

            $cookie_id = get_cookie("login");
            $cookie_key = md5($_SERVER['HTTP_USER_AGENT'].$chk['upw'].$chk['datetime']);

            if ($cookie_id && $cookie_id == $cookie_key) {

                $sql_common = "";
                $sql_common .= " set mid = '".$chk['mid']."' ";
                $sql_common .= ", ip = '".trim(strip_tags(sql_real_escape_string($_SERVER['REMOTE_ADDR'])))."' ";
                $sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
                $sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
                $sql_common .= ", login = '1' ";
                $sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

                sql_query(" insert into $web[member_login_table] $sql_common ");

                set_session('ss_mid', $chk['mid'].'|'.$_SERVER['REMOTE_ADDR'].'|'.md5($_SERVER['HTTP_USER_AGENT'].$chk['datetime']));

                echo "<script type='text/javascript'>window.location.reload();</script>";
                exit;

            }

        }

        unset($chk);

    }

}

// 초기화
$check_login = false;
$check_admin = false;
$check_bbsadmin = false;

// 회원
if ($member['mid']) {

    $check_login = true;

    if ($member['level'] >= 9) {

        // 관리자
        $check_admin = true;

    }

} else {
// 비회원

    $member['level'] = 1;
    $member['form_check'] = substr(md5($_SERVER['HTTP_USER_AGENT']),0,20);

}

$member['user'] = $_SERVER['REMOTE_ADDR'].visit_browser($_SERVER['HTTP_USER_AGENT']).visit_os($_SERVER['HTTP_USER_AGENT']);

if ($bbs_id) {

    $bbs = bbs($bbs_id);

    if ($bbs['bbs_id']) {

        $bbs_skin_path = $web['path']."/bbs/skin/".$bbs['bbs_skin'];
        $bbs_skin_url = $web['host_bbs']."/skin/".$bbs['bbs_skin'];

        if ($article_id) {

            $article = article($bbs_id, $article_id);

        }

        if ($check_login) {

            $row = explode("|", $bbs['bbs_admin']);
            for ($i=0; $i<count($row); $i++) {

                if ($row[$i] && $row[$i] == $member['uid']) {

                    $check_bbsadmin = true;

                }

            }

        }

    }

}

$check_robot = false;
$agent = trim(strip_tags(sql_real_escape_string($_SERVER['HTTP_USER_AGENT'])));
if (preg_match("/(bot|slurp|daumoa)/", $agent)) { $check_robot = true; }

$check_touch = false;
if (preg_match("/(Mobile|Android)/", $_SERVER['HTTP_USER_AGENT'])) { $check_touch = true; }
?>