<?php
$web = array();
$web['url'] = "..";
include_once("../tb-config.php");

if (file_exists("../tb-database.php")) {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<h1>데이터베이스 파일이 생성되어있습니다. (../tb-database.php)</h1>";
    exit;

}

if (!is_writable("..")) {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<h1>상위 디렉토리의 퍼미션을 읽기, 쓰기, 실행가능한 707으로 변경하여 주시기 바랍니다.</h1>";
    exit;

}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<meta charset="<?=$web['charset']?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,chrome=1" />
<title>테라보드 이용약관</title>
<link rel="stylesheet" href="http://fonts.googleapis.com/earlyaccess/nanumgothic.css" type="text/css" />
<link rel="stylesheet" href="<?=$web['host_css']?>/web.css" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<style type="text/css">
body {min-width:320px; min-height:650px; height:100%; background:url('img/bg_pattern.png') repeat;}

.layout-install {font-size:0; line-height:0; margin:0; padding-bottom:150px;}
.layout-install .wrap {padding-top:30px; max-width:960px; width:100%; margin:0 auto;}
.layout-install .logo {margin:0 auto; width:252px; height:190px; background:url('img/logo.png') no-repeat;}
.layout-install .logo.on {background-position:0 -190px;}
.layout-install .logo.on2 {background-position:0 -380px;}
.layout-install .title {text-align:center; margin-top:20px; font-weight:400; line-height:60px; font-size:60px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process {margin-top:30px; border-top:1px solid #73c3e4; border-bottom:1px solid #73c3e4; padding:15px 0;}
.layout-install .process:after {display:block; clear:both; content:'';}
.layout-install .process ul:first-child {float:left;}
.layout-install .process ul:last-child {float:right;}
.layout-install .process ul li {display:inline-block; vertical-align:top;}
.layout-install .process ul:first-child li {font-weight:400; line-height:45px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process ul:last-child li {margin-left:10px; border:2px solid #73c3e4; border-radius:25px; padding:0 45px 2px 45px;}
.layout-install .process ul:last-child li {font-weight:400; line-height:45px; font-size:20px; color:#73c3e4; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process ul:last-child li:first-child {margin-left:0;}
.layout-install .process ul:last-child li.on {border:2px solid #ffffff; color:#ffffff;}
.layout-install .terms {margin-top:40px;}
.layout-install .terms .text {font-weight:400; line-height:30px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .assent {margin-top:40px; border-top:1px solid #73c3e4; padding-top:20px;}
.layout-install .assent ul:first-child {float:left; margin-top:10px;}
.layout-install .assent ul:last-child {float:right;}
.layout-install .assent ul li {display:inline-block; vertical-align:top;}
.layout-install .assent ul:first-child li:last-child {margin-left:5px; cursor:pointer;}
.layout-install .assent ul:first-child {font-weight:400; line-height:24px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .assent .icon {display:inline-block; width:27px; height:27px; background:url('img/checkbox.png') no-repeat; cursor:pointer;}
.layout-install .assent .icon.on {background-position:-27px 0;}
.layout-install .assent ul:last-child span {display:inline-block; width:200px; text-align:center; border-radius:5px; background-color:#ffffff; cursor:pointer;}
.layout-install .assent ul:last-child span {font-weight:700; line-height:50px; font-size:20px; color:#2c9ecf; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .assent ul:last-child span:hover {background-color:#deff00;}
.layout-install .assent ul:last-child span:active {background-color:#252525;}

.license-title {padding:0 0 25px 0; margin-top:70px; border-bottom:1px solid #73c3e4; margin-bottom:5px;}
.license-title p {margin:0;}
.license-title .ltitle {text-align:center;}
.license-title .ltitle {font-weight:700; line-height:48px; font-size:48px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.license-title .ltext {margin-top:40px;}
.license-title .ltext {font-weight:400; line-height:24px; font-size:15px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}

.license-text {padding:45px 0 45px 0; border-top:1px solid #73c3e4;}
.license-text p {margin:0;}
.license-text .ltitle {font-weight:400; line-height:24px; font-size:18px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.license-text .ltext {margin-top:20px;}
.license-text .ltext {font-weight:400; line-height:24px; font-size:15px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}

@media screen and (max-width:640px) {

.layout-install {padding-left:10px; padding-right:10px;}
.layout-install .process ul li {display:block;}
.layout-install .process ul:last-child li {margin:10px 0 0 0;}
.layout-install .assent ul:first-child {width:100%; margin-top:10px;}
.layout-install .assent ul:last-child {width:100%; margin-top:30px; text-align:center;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    $('#assent').val('');

    $('.layout-install .assent .icon, .layout-install .assent ul:first-child li:last-child').click(function() {

        var obj = $('#assent');

        if (obj.val() == '') {

            obj.val('1');
            $('.layout-install .assent .icon').addClass('on');

        } else {

            obj.val('');
            $('.layout-install .assent .icon').removeClass('on');

        }

    });

    $('.layout-install .assent ul:last-child span').click(function() {

        installSubmit();

    });

});

function installSubmit()
{

    var f = document.formInstall;

    if (f.assent.value == '') {

        alert("이용약관에 동의하지 않으셨습니다.\n테라보드는 본 이용약관에 위배되는 사용을 금합니다..");
        return false;

    }

    f.action = "setup.php";
    f.submit();

}
</script>
</head>
<body>
<div class="layout-install">
<div class="wrap">
<div class="logo on"></div>
<div class="title">START THE TERABOARD INSTALL</div>
<div class="process">
<ul>
<li>Install Process</li>
</ul>
<ul>
<li class="on">1. 약관 동의</li>
<li>2. 정보입력</li>
<li>3. 설치 완료</li>
</ul>
</div>
<div class="terms">
<!-- start //-->
<div class="license-title">
<p class="ltitle">TERA BOARD<br />LICENSING CLAUSES</p>
<p class="ltext">테라보드는 개인과 기업을 대상으로 제공되는 CMS 솔루션입니다. 테라보드는 이용 목적에 따라 상업용(유료)과 비영리용(무료)으로 제공되며, 모든 사용자는 아래의 이용 약관 동의 후, 절차에 따라 라이선스를 발급받아 이용하실 수 있습니다. 라이선스 발급은 사용 권리의 취득을 의미합니다. 상업용 라이선스 비용은 현금(카드 포함) or 포인트를 통해 결제하실 수 있습니다. 상업용 라이선스 발급을 통해 발생하는 모든 수익금은 전액 공식 사이트의 전액 운영비와 학생 및 영세 개발자를 위해 사용되며, 그 사용 내역은 공식사이트를 통해 투명하게 공개됩니다. 이러한 방침을 통해 테라보드는 꾸준한 업데이트와 보안패치를 통해 안정된 서비스를 제공하고, 회원으로부터 다양한 스킨과 활용 팁 등을 제공받아 보답하기 위함이오니 양해를 부탁드립니다. 감사합니다.</p>
</div>
<div class="license-text">
<p class="ltitle">제1조 (목적)</p>
<p class="ltext">
본 이용약관은 CMS 솔루션 테라보드를 제공하는 개발사(이하 “회사”)와 본 솔루션을 이용하는 모든 사용자(이하 “사용자”) 간에 솔루션 이용에 관한 권리와 의무, 책임 및 기타 제반 사항을 규정하는 것을 목적으로 합니다.
</p>
</div>
<div class="license-text">
<p class="ltitle">제2조 (용어의 정의)</p>
<p class="ltext">
① 솔루션 : 개발사가 사용자에게 제공하는 자사의 소프트웨어 ‘테라보드’를 말함<br />
② 공식 사이트 : 테라보드의 개발사가 솔루션과 응용 프로그램의 판매/배포, 라이선스 인증 및 고객지원을 목적으로 제공하는 커뮤니티 성격의 홈페이지.<br />
③ 사용 권리 : 회원가입과 라이선스 발급 등(4조 2항 참고)을 절차에 따라 개발사로 부터 사용자에게 제공되는 솔루션의 ‘사용 권리’를 의미하며, 이는 소유권이나 저작권과는 무관합니다.<br />
④ 정품 인증서(라이선스)  : 솔루션의 사용 권리가 부여한 날짜와, 취득자의 정보가 기재된 전자문서<br />
⑤ 정품 인증번호(라이선스 키) : 솔루션의 사용 권리를 부여받은 사용자에게 제공되는 암호화된 일련번호<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제3조 (솔루션의 저작권)</p>
<p class="ltext">
① 솔루션의 소유권과 저작권은 개발사에 있으며, 소유권과 저작권은 개인 또는 기업에 제공하지 않습니다.<br />
② 솔루션은 한국저작권위원회(등록번호 : C-2012-003399)로부터 보호받고 있으며, 대한민국과 국제적인 저작권법, 조약, 저작권 협정으로부터 보호받고 있습니다.<br />
③ 솔루션에 포함된 일부 기능은 개별적인 라이선스를 지니고 있으며, 저작권은 각각의 개발사에 있습니다.<br />
④ 솔루션을 통해 유/무상으로 제공되는 응용 프로그램은 본 이용 약관을 적용하지 않으며, 독자적인 이용 약관에 따라 이용하실 수 있습니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제4조 (사용 권리의 취득과 철회)</p>
<p class="ltext">
① 솔루션의 사용 권리는 한 개의 도메인을 기준으로 제공됩니다.<br />
② 솔루션의 사용 권리는 이용 목적에 따라 상업용과 비영리용으로 구분하여 제공됩니다.<br />
③ 사용자는 솔루션의 사용 권리를 개발사로부터 아래의 절차에 따라 취득하실 수 있습니다.<br />
    가. ‘회원 이용약관’ 동의를 통한 공식 사이트의 회원가입<br />
    나. 솔루션 이용약관의 동의 및 이용 목적 · 사용자 · 사이트 정보 입력<br />
    다. 개발사 심사를 통한, 정품 인증서와 정품 인증번호 발급<br />
④ 아래의 경우, 개발사는 정품 인증서 및 정품 인증번호의 발급을 거절할 수 있으며, 발급된 이후에도 이를 철회(폐기)할 수 있습니다.<br />
    가. 사용자가 이용 목적 · 사용자 정보 · 사이트 정보를 허위로 기재하였을 때<br />
    나. 홈페이지의 이용 목적과 사용자 정보 · 사이트 정보가 변동되었을 때<br />
    다. 본 이용약관과 정보통신윤리강령을 침해한다고 판단되었을 때<br />
⑤ 사용자는 초기에 작성된 이용 목적 · 사용자 정보 · 사이트 정보가 변동될 경우, 기존의 정품 인증서를 30일 이내에 폐기하고, 새로운 인증서를 취득해야 합니다.<br />
⑥ 정품 인증서와 정품 인증번호는 개발사와 사용자 양측에 공동 발급되며, 양쪽의 정보가 불일치하거나, 이용목적에 어긋나는 경우 법적인 효력이 상실됩니다.<br />
⑦ 사용자가 정품 인증서를 소지하지 않거나, 폐기된 인증서를 통해 테라보드를 이용하실 경우, 저작권을 침해한 것으로 간주하여 민형사상의 책임을 물으실 수 있습니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제5조 (사용자 및 사이트 정보 수집)</p>
<p class="ltext">
① 개발사는 공식 사이트의 회원가입을 통하여 사용자의 닉네임 · 휴대폰 · 이메일 · 홈페이지 등의 정보를 수집하며, 사용 권리를 부여하는 과정에서 사용자명, 도메인, 사업자 번호, 연락처, 이메일 주소를 수집합니다. 이는 개발사의 사용자 통계 및 서비스 제공을 목적으로 하며, 수집된 정보는 제삼자 또는 외부 기업에 일절 제공하지 않습니다.<br />
② 개발사는 솔루션의 설치와 환경설정 과정에서 공식 사이트와 사용자의 홈페이지를 연동하여, 정품인증 번호와 도메인의 유효성과 버전 정보를 수집하고 관련된 기능을 제공합니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제6조 (솔루션의 이용 범위)</p>
<p class="ltext">
① 절차에 따라 사용 권리를 취득한 사용자는 솔루션을 통해 자유롭게 홈페이지를 제작/이용하실 수 있습니다.<br />
② 솔루션은 사용자의 목적과 용도에 따라 직접 또는 제삼자를 통하여 자유롭게 변경작업(Customizing)을 수행하실 수 있습니다.<br />
③ 판매 및 양도를 목적으로 하는 제작자(웹에이전시 또는 웹프리랜서)는 상업용 라이선스만 사용 권리를 취득하여 제작/공급하실 수 있습니다.<br />
④ 판매 및 양도를 목적으로 하는 제작자(웹에이전시 또는 웹프리랜서)는 사용자 정보 · 사이트 정보 작성 시, 실 사용자(고객)를 대상으로 등록해야합니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제7조 (솔루션의 변경 책임)</p>
<p class="ltext">
① 사용자가 직접 또는 제삼자를 통하여 솔루션의 변경(Customizing) 작업을 수행하여, 문제 또는 피해가 발생할 경우, 그 책임은 사용자 또는 변경작업을 수행한 자에게 있습니다.<br />
② 개발사가 제공하는 업데이트 진행 시에는 이와 함께 변동된 파일의 백업 및 업데이트 작업을 수행해야 하며, 그 책임은 사용자 또는 업데이트를 수행한 자에게 있습니다.<br />
③ 개발사는 위 항목을 포함하여, 사용자가 변경작업을 수행하며 발생하는 모든 문제에 일절 관여하지 않습니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제8조 (솔루션의 이용 제한)</p>
<p class="ltext">
① 사용자는 개발사의 동의 없이, 솔루션의 사용 권리를 타인에게 양도하실 수 없습니다.<br />
② 사용자는 개발사의 솔루션 또는 솔루션의 일부분을 발췌하여 무단 배포하실 수 없습니다.<br />
③ 개작 또는 변경을 통해 2차 저작물의 저작권을 취득하고자 할 경우, 개발사의 솔루션 또는 솔루션의 일부분이 포함되어서는 안 됩니다.<br />
④ 솔루션은 소스코드와 수집된 데이터의 보호하고, 합법적인 사용자를 구분하기 위한 목적으로 일부분을 암호된 파일로 제공되고 있으며, 암호화된 파일을 제거 및 변조, 이를 무력화하는 별도의 파일을 제작하여 이를 사용하거나 배포해서는 안됩니다.<br />
⑤ 개발사는 솔루션과 사용자의 권리를 지키고자 노력하고 있으며, 이러한 권리가 침해할 경우 솔루션의 이용을 즉시 제한할 수 있습니다. 또한, 컴퓨터프로그램 보호법 46조의 벌칙 규정에 따라 민형사상의 책임을 물으실 수 있습니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제9조 (고객지원의 범위)</p>
<p class="ltext">
① 배포되는 소스코드의 결함은 지속적인 업데이트와 패치를 통해 지원합니다.<br />
② 공식 사이트를 통해 별도로 제공하는 이용 팁, 질의응답, 1:1문의 등의 고객 지원 서비스는 답변에 대한 강제성을 동반하지 않으며, 법적인 책임과 의무는 없습니다.<br />
③ 개발사는 사용자에게 별도의 댓가성이나 계약성이 아닌, 학습과 친목을 목적으로 고객 지원 서비스를 제공하는 것이며, 이러한 서비스는 개발사의 운영 방침에 따라 중단할 수 있습니다.<br />
④ 개발사는 솔루션에 포함된 기본적인 기능 결함 외에 발생하는 다양한 문제에 관여하지 않으며, 책임을 지지 않습니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제10조 (면책 조항)</p>
<p class="ltext">
① 개발사는 천재지변 및 경영악화, 천재지변, 디도스(DDOS) 공격, IDC 장애, 기간통신사업자의 회선 장애 또는 이에 준하는 불가항력으로 서비스 종료 시 이에 대한 모든 책임이 면책됩니다.<br />
② 개발사의 솔루션을 통해 구축된 사용자의 홈페이지에 기록되는 모든 정보(회원, 게시물 등)에 관여하지 않으며, 유실 및 유출로 발생하는 모든 책임은 사용자 본인에게 있습니다.<br />
③ 개발사는 사용자가 제삼자와 함께 솔루션을 사용하며 발생하는 모든 분쟁에 대해, 어떠한 책임도 지지 않으며, 이에 개입하지 않습니다.<br />
④ 개발사는 사용자의 귀책사유로 인한 서비스 이용의 장애에 대하여는 책임을 지지 않습니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제11조 (그 외 기타 사항)</p>
<p class="ltext">
① 본 이용약관에 명시되지 않은 사항에 대해서는 관계 법령 및 한국 소프트웨어 저작권 협회의 법령과 판례를 따릅니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제12조 (효력의 발생)</p>
<p class="ltext">
① 본 이용약관의 효력은 본 약관의 동의 후, 정품 인증서가 발급된 시점으로부터 발생합니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">제13조 (합의 관할)</p>
<p class="ltext">
① 본 이용약관과 관련하여 발생하는 제반·분쟁사항에 대한 소송은 회사의 소재지관할 법원으로 합니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">부칙 - 제 1조 (시행일)</p>
<p class="ltext">
① 시행일 : 본 약관은 2015년 11월 15일부터 시행합니다.<br />
</p>
</div>
<div class="license-text">
<p class="ltitle">부칙 - 제 2조 (상업용 라이선스의 환불 정책)</p>
<p class="ltext">
① 사용자가 솔루션을 설치하신 후에는 교환 또는 환불이 안 됩니다. 단, 사용자가 결제 후 설치를 하지 않은 상태에서 7일 이내 이용을 철회할 경우, 지불된 전자 결제 또는 송금 수수료를 사용자가 부담하는 조건으로 환불이 가능합니다.<br />
</p>
</div>
<!-- end //-->
</div>
<form method="post" name="formInstall" autocomplete="off">
<input type="hidden" id="assent" name="assent" value="" />
<div class="assent">
<ul>
<li><span class="icon"></span></li>
<li>본 약관을 모두 읽었으며, 이에 동의합니다.</li>
</ul>
<ul>
<li><span>다음 단계</span></a>
</ul>
</div>
</form>
</div>
</div>
</body>
</html>