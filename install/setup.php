<?php
$web = array();
$web['url'] = "..";
include_once("../tb-config.php");

if (file_exists("../tb-database.php")) {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<h1>데이터베이스 파일이 생성되어있습니다. (../tb-database.php)</h1>";
    exit;

}

if (!is_writable("..")) {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<h1>상위 디렉토리의 퍼미션을 읽기, 쓰기, 실행가능한 707으로 변경하여 주시기 바랍니다.</h1>";
    exit;

}

$gmnow = gmdate("D, d M Y H:i:s") . " GMT";
header("Expires: 0"); // rfc2616 - Section 14.21
header("Last-Modified: " . $gmnow);
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: pre-check=0, post-check=0, max-age=0"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0

if (!$_POST['assent']) {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>alert('이용약관에 동의하지 않으셨습니다.'); history.go(-1);</script>";
    exit;

}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<meta charset="<?=$web['charset']?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,chrome=1" />
<title>테라보드 정보입력</title>
<link rel="stylesheet" href="http://fonts.googleapis.com/earlyaccess/nanumgothic.css" type="text/css" />
<link rel="stylesheet" href="<?=$web['host_css']?>/web.css" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<style type="text/css">
body {min-width:320px; min-height:650px; height:100%; background:url('img/bg_pattern.png') repeat;}

.layout-install {font-size:0; line-height:0; margin:0; padding-bottom:150px;}
.layout-install .wrap {padding-top:30px; max-width:960px; width:100%; margin:0 auto;}
.layout-install .logo {margin:0 auto; width:252px; height:190px; background:url('img/logo.png') no-repeat;}
.layout-install .logo.on {background-position:0 -190px;}
.layout-install .logo.on2 {background-position:0 -380px;}
.layout-install .title {text-align:center; margin-top:20px; font-weight:400; line-height:60px; font-size:60px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process {margin-top:30px; border-top:1px solid #73c3e4; border-bottom:1px solid #73c3e4; padding:15px 0;}
.layout-install .process:after {display:block; clear:both; content:'';}
.layout-install .process ul:first-child {float:left;}
.layout-install .process ul:last-child {float:right;}
.layout-install .process ul li {display:inline-block; vertical-align:top;}
.layout-install .process ul:first-child li {font-weight:400; line-height:45px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process ul:last-child li {margin-left:10px; border:2px solid #73c3e4; border-radius:25px; padding:0 45px 2px 45px;}
.layout-install .process ul:last-child li {font-weight:400; line-height:45px; font-size:20px; color:#73c3e4; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process ul:last-child li:first-child {margin-left:0;}
.layout-install .process ul:last-child li.on {border:2px solid #ffffff; color:#ffffff;}
.layout-install .info {margin-top:40px;}
.layout-install .info .block {width:50%; display:inline-block; vertical-align:top;}
.layout-install .info .block:first-child .line {padding:30px 20px 30px 0;}
.layout-install .info .block:last-child .line {padding:30px 0 30px 20px; border-left:1px solid #73c3e4;}
.layout-install .info .subj {margin-bottom:30px;}
.layout-install .info .subj {text-align:center; font-weight:400; line-height:24px; font-size:24px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .info ul {margin-top:5px;}
.layout-install .info ul li {display:inline-block; vertical-align:top;}
.layout-install .info ul li:first-child {width:205px;}
.layout-install .info ul li:first-child {font-weight:400; line-height:42px; font-size:18px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .info ul li input {border:0; width:212px; height:41px; background:url('img/input.png') no-repeat; padding:0 20px 1px 20px;}
.layout-install .info ul li input {font-weight:700; line-height:41px; font-size:16px; color:#000000; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .info ul li input.on {background-position:0 -42px; color:#ffffff;}
.layout-install .info .msg {margin-top:17px; background-color:#1f92c3;}
.layout-install .info .msg {text-align:center; font-weight:400; line-height:30px; font-size:14px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .btn {margin-top:40px; border-top:1px solid #73c3e4; padding-top:20px;}
.layout-install .btn ul:first-child {float:left; margin-top:10px;}
.layout-install .btn ul:last-child {float:right;}
.layout-install .btn ul li {display:inline-block; vertical-align:top;}
.layout-install .btn ul:first-child {font-weight:400; line-height:24px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .btn ul:last-child span {display:inline-block; width:200px; text-align:center; border-radius:5px; background-color:#ffffff; cursor:pointer;}
.layout-install .btn ul:last-child span {font-weight:700; line-height:50px; font-size:20px; color:#2c9ecf; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .btn ul:last-child span:hover {background-color:#deff00;}
.layout-install .btn ul:last-child span:active {background-color:#252525;}

@media screen and (max-width:640px) {

.layout-install {padding-left:10px; padding-right:10px;}
.layout-install .process ul li {display:block;}
.layout-install .process ul:last-child li {margin:10px 0 0 0;}
.layout-install .btn ul:first-child {width:100%; margin-top:10px;}
.layout-install .btn ul:last-child {width:100%; margin-top:30px; text-align:center;}

}
</style>
<script type="text/javascript">
$(document).ready( function() {

    $('.layout-install .info ul li input').focus(function() {

        $(this).addClass('on');

        if ($(this).attr('rel') == '1') {

            $('.layout-install .logo').removeClass('on').addClass('on2');

        } else {

            $('.layout-install .logo').removeClass('on2').addClass('on');

        }

    }).blur(function() {

        $(this).removeClass('on');
        $('.layout-install .logo').removeClass('on2').addClass('on');

    });

    $('.layout-install .btn ul:last-child span').click(function() {

        installSubmit();

    });

});

function installSubmit()
{

    var f = document.formInstall;

    if (f.mysql_host.value == '') {

        alert("MYSQL_HOST NAME을 입력하세요."); 
        f.mysql_host.focus();
        return false;

    }

    if (f.mysql_user.value == '') {

        alert("MYSQL_USER ID를 입력하세요."); 
        f.mysql_user.focus();
        return false;

    }

    if (f.mysql_password.value == '') {

        alert("MYSQL_PASSWORD를 입력하세요."); 
        f.mysql_password.focus();
        return false;

    }

    if (f.mysql_db.value == '') {

        alert("MYSQL_DB NAME을 입력하세요."); 
        f.mysql_db.focus();
        return false;

    }

    if (f.uid.value == '') {

        alert("ADMIN_ID를 입력하세요."); 
        f.uid.focus();
        return false;

    }

    if (f.upw.value == '') {

        alert("ADMIN_PASS를 입력하세요."); 
        f.upw.focus();
        return false;

    }

    if (f.upw_re.value == '') {

        alert("ADMIN_Re-PASS 재입력을 입력하세요."); 
        f.upw_re.focus();
        return false;

    }

    if (f.upw.value != f.upw_re.value) {

        alert("입력하신 비밀번호가 일치하지 않습니다."); 
        f.upw_re.focus();
        return false;

    }

    f.action = "update.php";
    f.submit();

}
</script>
</head>
<body>
<form method="post" name="formInstall" autocomplete="off">
<div class="layout-install">
<div class="wrap">
<div class="logo on"></div>
<div class="title">START THE TERABOARD INSTALL</div>
<div class="process">
<ul>
<li>Install Process</li>
</ul>
<ul>
<li>1. 약관 동의</li>
<li class="on">2. 정보입력</li>
<li>3. 설치 완료</li>
</ul>
</div>
<div class="info">
<div class="block">
<div class="line">
<p class="subj">::: 데이터베이스 정보 :::</p>
<ul>
<li>MYSQL_<b>HOST NAME</b></li>
<li><input type="text" name="mysql_host" value="localhost" /></li>
</ul>
<ul>
<li>MYSQL_<b>USER ID</b></li>
<li><input type="text" name="mysql_user" value="" rel="1" /></li>
</ul>
<ul>
<li>MYSQL_<b>PASSWORD</b></li>
<li><input type="password" name="mysql_password" value="" /></li>
</ul>
<ul>
<li>MYSQL_<b>DB NAME</b></li>
<li><input type="text" name="mysql_db" value="" rel="1" /></li>
</ul>
</div>
</div>
<div class="block">
<div class="line">
<p class="subj">::: 홈페이지 관리자 정보 :::</p>
<ul>
<li>ADMIN_<b>ID</b></li>
<li><input type="text" name="uid" value="admin" /></li>
</ul>
<ul>
<li>ADMIN_<b>PASS</b></li>
<li><input type="password" name="upw" value="" rel="1" /></li>
</ul>
<ul>
<li>ADMIN_<b>Re-PASS</b></li>
<li><input type="password" name="upw_re" value="" /></li>
</ul>
<p class="msg">최초 설정한 관리자 ID와 비밀번호는 절대로 분실하시면 안됩니다.</p>
</div>
</div>
</div>
<div class="btn">
<ul>
<li>데이터베이스 정보를 모르실 경우, 호스팅사에 문의하세요.</li>
</ul>
<ul>
<li><span>설치 시작</span></a>
</ul>
</div>
</div>
</div>
</form>
</body>
</html>