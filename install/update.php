<?php
set_time_limit(0);
$web = array();
$web['url'] = "..";
include_once("../tb-config.php");
include_once("../other/curl/Curl.php");
include_once("../lib/system.lib.php");

if (file_exists("../tb-database.php")) {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<h1>데이터베이스 파일이 생성되어있습니다. (../tb-database.php)</h1>";
    exit;

}

if (!is_writable("..")) {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<h1>상위 디렉토리의 퍼미션을 읽기, 쓰기, 실행가능한 707으로 변경하여 주시기 바랍니다.</h1>";
    exit;

}

$gmnow = gmdate("D, d M Y H:i:s") . " GMT";
header("Expires: 0"); // rfc2616 - Section 14.21
header("Last-Modified: " . $gmnow);
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: pre-check=0, post-check=0, max-age=0"); // HTTP/1.1
header("Pragma: no-cache"); // HTTP/1.0

$mysql_host = addslashes(trim($_POST['mysql_host']));
$mysql_user = addslashes(trim($_POST['mysql_user']));
$mysql_password = addslashes(trim($_POST['mysql_password']));
$mysql_db = addslashes(trim($_POST['mysql_db']));
$uid = strtolower(addslashes(trim($_POST['uid'])));
$upw = addslashes(trim($_POST['upw']));

if (function_exists('mysqli_connect')) { $web['mysqli'] = true; }

$web['sql_connect'] = sql_connect($mysql_host, $mysql_user, $mysql_password, $mysql_db);
$web['sql_select_db'] = sql_select_db($mysql_db, $web['sql_connect']);

if (!$web['sql_connect']) {

    echo "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">";
    echo "<script type='text/javascript'>alert('데이터 베이스 정보가 올바르지 않습니다.\\n해당 정보는 호스팅 사에서 제공하는 세팅 메일을\\n참고하시거나, 호스팅사로 문의하시기 바랍니다.'); history.go(-1);</script>";
    exit;

}

// 테이블 생성
$file = implode("", file("update.sql"));
preg_match_all("/CREATE TABLE(.*)\;[\r\n]/Uis", $file, $matches);
for ($i=0; $i<count($matches[1]); $i++) {

    $sql = "CREATE TABLE ".$matches[1][$i].";";
    sql_query($sql);

}

// 데이터 생성
$file = implode("", file("update2.sql"));
preg_match_all("/INSERT INTO(.*)\)\;[\r\n]/Uis", $file, $matches);
for ($i=0; $i<count($matches[1]); $i++) {

    $sql = "INSERT INTO".$matches[1][$i].");";
    sql_query($sql);

}

// 관리자 변경
$sql_common = "";
$sql_common .= " set uid = '".$uid."' ";
$sql_common .= ", upw = PASSWORD('$upw') ";
$sql_common .= ", level = '10' ";
$sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
$sql_common .= ", datetime = '".$web['time_ymdhis']."' ";
$sql_common .= ", form_check = '".addslashes(substr(md5($_SERVER['REMOTE_ADDR'].date("His", $web['server_time']).rand(100000,999999)),0,30))."' ";

sql_query(" update $web[member_table] $sql_common where mid = 1 ");

// 로그인
$sql_common = "";
$sql_common .= " set mid = '1' ";
$sql_common .= ", ip = '".addslashes($_SERVER['REMOTE_ADDR'])."' ";
$sql_common .= ", browser = '".visit_browser($_SERVER['HTTP_USER_AGENT'])."' ";
$sql_common .= ", os = '".visit_os($_SERVER['HTTP_USER_AGENT'])."' ";
$sql_common .= ", login = '1' ";
$sql_common .= ", datetime = '".$web['time_ymdhis']."' ";

sql_query(" insert into $web[member_login_table] $sql_common ");

// 환경설정
$sql_common = "";
$sql_common .= " set ver  = '14' ";
$sql_common .= ", cronkey = '".addslashes(substr(md5($_SERVER['REMOTE_ADDR'].date("His", $web['server_time']).rand(100000,999999)),0,10))."' ";
$sql_common .= ", installdate = '".$web['time_ymd']."' ";

sql_query(" update $web[setup_table] $sql_common ");

// db 설정파일 생성
$text ='<?php
if (!defined("_WEB_")) exit;
$mysql_host = "'.$mysql_host.'";
$mysql_user = "'.$mysql_user.'";
$mysql_password = "'.$mysql_password.'";
$mysql_db = "'.$mysql_db.'";
?>';

$file = "../tb-database.php";
$f = @fopen($file, "w");
@fwrite($f, $text);
@fclose($f);
@chmod($file, 0606);

echo "<script type='text/javascript'>location.replace('result.php');</script>";
exit;
?>