
-- --------------------------------------------------------

--
-- Table structure for table `tb_area`
--

CREATE TABLE IF NOT EXISTS `tb_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `area_x1` int(11) NOT NULL DEFAULT '0',
  `area_y1` int(11) NOT NULL DEFAULT '0',
  `area_x2` int(11) NOT NULL DEFAULT '0',
  `area_y2` int(11) NOT NULL DEFAULT '0',
  `area_w` int(11) NOT NULL DEFAULT '0',
  `area_h` int(11) NOT NULL DEFAULT '0',
  `upload_source` varchar(255) NOT NULL,
  `upload_file` varchar(255) NOT NULL,
  `upload_filesize` int(11) NOT NULL DEFAULT '0',
  `upload_width` int(11) NOT NULL DEFAULT '0',
  `upload_height` int(11) NOT NULL DEFAULT '0',
  `upload_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_article_bbs1`
--

CREATE TABLE IF NOT EXISTS `tb_article_bbs1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL,
  `ar_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `ar_category` varchar(255) NOT NULL COMMENT '분류',
  `ar_title` varchar(255) NOT NULL COMMENT '제목',
  `ar_content` longtext NOT NULL COMMENT '내용',
  `ar_keyword` varchar(100) NOT NULL COMMENT '키워드',
  `ar_source` varchar(255) NOT NULL COMMENT '출처',
  `ar_img` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이미지',
  `ar_file` tinyint(4) NOT NULL DEFAULT '0' COMMENT '파일',
  `ar_secret` tinyint(4) NOT NULL DEFAULT '0' COMMENT '비밀글',
  `ar_hit` int(11) NOT NULL DEFAULT '0' COMMENT '조회수',
  `ar_reply` int(11) NOT NULL DEFAULT '0' COMMENT '댓글수',
  `ar_good` int(11) NOT NULL DEFAULT '0' COMMENT '추천수',
  `ar_nogood` int(11) NOT NULL DEFAULT '0' COMMENT '비추천수',
  `ar_adult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성인',
  `ar_blind` varchar(100) NOT NULL COMMENT '블라인드',
  `ar_jump` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '점프일',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `ar_id` (`ar_id`),
  KEY `ar_jump` (`ar_jump`),
  KEY `mid` (`mid`,`ar_jump`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_article_bbs2`
--

CREATE TABLE IF NOT EXISTS `tb_article_bbs2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL,
  `ar_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `ar_category` varchar(255) NOT NULL COMMENT '분류',
  `ar_title` varchar(255) NOT NULL COMMENT '제목',
  `ar_content` longtext NOT NULL COMMENT '내용',
  `ar_keyword` varchar(100) NOT NULL COMMENT '키워드',
  `ar_source` varchar(255) NOT NULL COMMENT '출처',
  `ar_img` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이미지',
  `ar_file` tinyint(4) NOT NULL DEFAULT '0' COMMENT '파일',
  `ar_secret` tinyint(4) NOT NULL DEFAULT '0' COMMENT '비밀글',
  `ar_hit` int(11) NOT NULL DEFAULT '0' COMMENT '조회수',
  `ar_reply` int(11) NOT NULL DEFAULT '0' COMMENT '댓글수',
  `ar_good` int(11) NOT NULL DEFAULT '0' COMMENT '추천수',
  `ar_nogood` int(11) NOT NULL DEFAULT '0' COMMENT '비추천수',
  `ar_adult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성인',
  `ar_blind` varchar(100) NOT NULL COMMENT '블라인드',
  `ar_jump` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '점프일',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  `ar_subject` varchar(255) NOT NULL COMMENT '부제목',
  PRIMARY KEY (`id`),
  KEY `ar_id` (`ar_id`),
  KEY `ar_jump` (`ar_jump`),
  KEY `mid` (`mid`,`ar_jump`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_article_bbs3`
--

CREATE TABLE IF NOT EXISTS `tb_article_bbs3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL,
  `ar_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `ar_category` varchar(255) NOT NULL COMMENT '분류',
  `ar_title` varchar(255) NOT NULL COMMENT '제목',
  `ar_content` longtext NOT NULL COMMENT '내용',
  `ar_keyword` varchar(100) NOT NULL COMMENT '키워드',
  `ar_source` varchar(255) NOT NULL COMMENT '출처',
  `ar_img` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이미지',
  `ar_file` tinyint(4) NOT NULL DEFAULT '0' COMMENT '파일',
  `ar_secret` tinyint(4) NOT NULL DEFAULT '0' COMMENT '비밀글',
  `ar_hit` int(11) NOT NULL DEFAULT '0' COMMENT '조회수',
  `ar_reply` int(11) NOT NULL DEFAULT '0' COMMENT '댓글수',
  `ar_good` int(11) NOT NULL DEFAULT '0' COMMENT '추천수',
  `ar_nogood` int(11) NOT NULL DEFAULT '0' COMMENT '비추천수',
  `ar_adult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성인',
  `ar_blind` varchar(100) NOT NULL COMMENT '블라인드',
  `ar_jump` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '점프일',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `ar_id` (`ar_id`),
  KEY `ar_jump` (`ar_jump`),
  KEY `mid` (`mid`,`ar_jump`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_article_bbs4`
--

CREATE TABLE IF NOT EXISTS `tb_article_bbs4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL COMMENT '아이피',
  `ar_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `ar_category` varchar(255) NOT NULL COMMENT '분류',
  `ar_title` varchar(255) NOT NULL COMMENT '제목',
  `ar_content` longtext NOT NULL COMMENT '내용',
  `ar_keyword` varchar(100) NOT NULL COMMENT '키워드',
  `ar_source` varchar(255) NOT NULL COMMENT '출처',
  `ar_img` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이미지',
  `ar_file` tinyint(4) NOT NULL DEFAULT '0' COMMENT '파일',
  `ar_secret` tinyint(4) NOT NULL DEFAULT '0' COMMENT '비밀글',
  `ar_hit` int(11) NOT NULL DEFAULT '0' COMMENT '조회수',
  `ar_reply` int(11) NOT NULL DEFAULT '0' COMMENT '댓글수',
  `ar_good` int(11) NOT NULL DEFAULT '0' COMMENT '추천수',
  `ar_nogood` int(11) NOT NULL DEFAULT '0' COMMENT '비추천수',
  `ar_adult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성인',
  `ar_blind` varchar(100) NOT NULL COMMENT '블라인드',
  `ar_jump` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '점프일',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `ar_id` (`ar_id`),
  KEY `ar_jump` (`ar_jump`),
  KEY `mid` (`mid`,`ar_jump`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_article_notice`
--

CREATE TABLE IF NOT EXISTS `tb_article_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL,
  `ar_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `ar_category` varchar(255) NOT NULL COMMENT '분류',
  `ar_title` varchar(255) NOT NULL COMMENT '제목',
  `ar_content` longtext NOT NULL COMMENT '내용',
  `ar_keyword` varchar(100) NOT NULL COMMENT '키워드',
  `ar_source` varchar(255) NOT NULL COMMENT '출처',
  `ar_img` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이미지',
  `ar_file` tinyint(4) NOT NULL DEFAULT '0' COMMENT '파일',
  `ar_secret` tinyint(4) NOT NULL DEFAULT '0' COMMENT '비밀글',
  `ar_hit` int(11) NOT NULL DEFAULT '0' COMMENT '조회수',
  `ar_reply` int(11) NOT NULL DEFAULT '0' COMMENT '댓글수',
  `ar_good` int(11) NOT NULL DEFAULT '0' COMMENT '추천수',
  `ar_nogood` int(11) NOT NULL DEFAULT '0' COMMENT '비추천수',
  `ar_adult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성인',
  `ar_blind` varchar(100) NOT NULL COMMENT '블라인드',
  `ar_jump` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '점프일',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `ar_id` (`ar_id`),
  KEY `ar_jump` (`ar_jump`),
  KEY `mid` (`mid`,`ar_jump`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_banner`
--

CREATE TABLE IF NOT EXISTS `tb_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` varchar(100) NOT NULL COMMENT '그룹id',
  `title` varchar(100) NOT NULL COMMENT '배너명',
  `onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '상태',
  `hit` int(11) NOT NULL DEFAULT '0' COMMENT '노출수',
  `click` int(11) NOT NULL DEFAULT '0' COMMENT '클릭수',
  `pc_blank` tinyint(4) NOT NULL DEFAULT '0' COMMENT '새창',
  `pc_url` varchar(255) NOT NULL COMMENT '경로',
  `pc_upload_source` varchar(255) NOT NULL COMMENT '첨부파일명',
  `pc_upload_file` varchar(255) NOT NULL COMMENT '변환파일명',
  `pc_upload_filesize` int(11) NOT NULL DEFAULT '0' COMMENT '파일사이즈',
  `pc_upload_width` int(11) NOT NULL DEFAULT '0' COMMENT '파일가로크기',
  `pc_upload_height` int(11) NOT NULL DEFAULT '0' COMMENT '파일세로크기',
  `pc_upload_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '업로드일시',
  `pc_upload2_source` varchar(255) NOT NULL COMMENT '첨부파일명',
  `pc_upload2_file` varchar(255) NOT NULL COMMENT '변환파일명',
  `pc_upload2_filesize` int(11) NOT NULL DEFAULT '0' COMMENT '파일사이즈',
  `pc_upload2_width` int(11) NOT NULL DEFAULT '0' COMMENT '파일가로크기',
  `pc_upload2_height` int(11) NOT NULL DEFAULT '0' COMMENT '파일세로크기',
  `pc_upload2_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '업로드일시',
  `mobile_blank` tinyint(4) NOT NULL DEFAULT '0' COMMENT '새창',
  `mobile_url` varchar(255) NOT NULL COMMENT '경로',
  `mobile_upload_source` varchar(255) NOT NULL COMMENT '첨부파일명',
  `mobile_upload_file` varchar(255) NOT NULL COMMENT '변환파일명',
  `mobile_upload_filesize` int(11) NOT NULL DEFAULT '0' COMMENT '파일사이즈',
  `mobile_upload_width` int(11) NOT NULL DEFAULT '0' COMMENT '파일가로크기',
  `mobile_upload_height` int(11) NOT NULL DEFAULT '0' COMMENT '파일세로크기',
  `mobile_upload_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '업로드일시',
  `mobile_upload2_source` varchar(255) NOT NULL COMMENT '첨부파일명',
  `mobile_upload2_file` varchar(255) NOT NULL COMMENT '변환파일명',
  `mobile_upload2_filesize` int(11) NOT NULL DEFAULT '0' COMMENT '파일사이즈',
  `mobile_upload2_width` int(11) NOT NULL DEFAULT '0' COMMENT '파일가로크기',
  `mobile_upload2_height` int(11) NOT NULL DEFAULT '0' COMMENT '파일세로크기',
  `mobile_upload2_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '업로드일시',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '생성일시',
  PRIMARY KEY (`id`)
)  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bbs`
--

CREATE TABLE IF NOT EXISTS `tb_bbs` (
  `bbs_id` varchar(50) NOT NULL COMMENT '게시판 ID',
  `bbs_group` varchar(100) NOT NULL COMMENT '게시판그룹',
  `bbs_title` varchar(100) NOT NULL COMMENT '게시판명',
  `bbs_subject` varchar(100) NOT NULL COMMENT '부제목',
  `bbs_category` varchar(255) NOT NULL COMMENT '카테고리',
  `bbs_category_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '카테고리사용',
  `bbs_admin` varchar(255) NOT NULL COMMENT '게시물 관리자 ID',
  `bbs_position` int(11) NOT NULL DEFAULT '0' COMMENT '출력 순서',
  `bbs_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '상태',
  `bbs_skin` varchar(50) NOT NULL COMMENT '스킨',
  `bbs_start` tinyint(4) NOT NULL DEFAULT '0' COMMENT '시작 페이지',
  `bbs_answer` tinyint(4) NOT NULL DEFAULT '0' COMMENT '답변 글',
  `bbs_secret` tinyint(4) NOT NULL DEFAULT '0' COMMENT '비밀 글',
  `bbs_reply` tinyint(4) NOT NULL DEFAULT '0' COMMENT '댓글 작성',
  `bbs_good` tinyint(4) NOT NULL DEFAULT '0' COMMENT '추천',
  `bbs_nogood` tinyint(4) NOT NULL DEFAULT '0' COMMENT '비추천',
  `bbs_adult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성인 전용',
  `bbs_source` tinyint(4) NOT NULL DEFAULT '0' COMMENT '출처 표기',
  `bbs_search` tinyint(4) NOT NULL DEFAULT '0' COMMENT '통합검색',
  `bbs_icon_new` int(11) NOT NULL DEFAULT '0' COMMENT 'NEW 아이콘',
  `bbs_icon_hit` int(11) NOT NULL DEFAULT '0' COMMENT 'HIT 아이콘',
  `bbs_rows` int(11) NOT NULL DEFAULT '0' COMMENT '게시물 수',
  `bbs_thumb_width` int(11) NOT NULL DEFAULT '0' COMMENT '섬네일 가로 폭',
  `bbs_thumb_height` int(11) NOT NULL DEFAULT '0' COMMENT '섬네일 세로 폭',
  `bbs_order` varchar(100) NOT NULL COMMENT '게시물 정렬 방식',
  `bbs_view_list` tinyint(4) NOT NULL DEFAULT '0' COMMENT '하단 목록 출력',
  `bbs_sns` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'SNS 퍼가기',
  `bbs_file_count` int(11) NOT NULL DEFAULT '0' COMMENT '첨부파일 갯수',
  `bbs_file_size` int(11) NOT NULL DEFAULT '0' COMMENT '첨부파일 용량',
  `bbs_syndi` tinyint(4) NOT NULL DEFAULT '0' COMMENT '네이버 신디케이션',
  `bbs_level_list` int(11) NOT NULL DEFAULT '0' COMMENT '목록 보기',
  `bbs_level_view` int(11) NOT NULL DEFAULT '0' COMMENT '내용 보기',
  `bbs_level_write` int(11) NOT NULL DEFAULT '0' COMMENT '게시물 쓰기',
  `bbs_level_answer` int(11) NOT NULL DEFAULT '0' COMMENT '답변글 쓰기',
  `bbs_level_reply` int(11) NOT NULL DEFAULT '0' COMMENT '댓글 쓰기',
  `bbs_level_download` int(11) NOT NULL DEFAULT '0' COMMENT '첨부파일 다운',
  `bbs_level_good` int(11) NOT NULL DEFAULT '0' COMMENT '추천/비추천',
  `bbs_point_view` double NOT NULL DEFAULT '0' COMMENT '내용 열람',
  `bbs_point_write` double NOT NULL DEFAULT '0' COMMENT '게시물 쓰기',
  `bbs_point_reply` double NOT NULL DEFAULT '0' COMMENT '댓글 쓰기',
  `bbs_point_download` double NOT NULL DEFAULT '0' COMMENT '다운로드 받음',
  `bbs_point_download_write` double NOT NULL DEFAULT '0' COMMENT '다운로드 제공',
  `bbs_point_good` double NOT NULL DEFAULT '0' COMMENT '추천/비추천 클릭',
  `bbs_include_top` varchar(100) NOT NULL COMMENT '상단 파일',
  `bbs_include_bottom` varchar(100) NOT NULL COMMENT '하단 파일',
  `bbs_text_write` longtext NOT NULL COMMENT '자동완성내용',
  `bbs_text_top` longtext NOT NULL COMMENT '상단 디자인 내용',
  `bbs_text_bottom` longtext NOT NULL COMMENT '하단 디자인 내용',
  `bbs_notice` text NOT NULL COMMENT '공지사항게시물번호',
  `bbs_write_count` int(11) NOT NULL DEFAULT '0' COMMENT '게시물수',
  `bbs_reply_count` int(11) NOT NULL DEFAULT '0' COMMENT '댓글수',
  `bbs_extend1_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend1_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend2_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend2_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend3_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend3_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend4_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend4_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend5_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend5_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend6_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend6_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend7_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend7_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend8_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend8_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend9_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend9_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_extend10_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_extend10_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '생성일시',
  `bbs_skin_install` varchar(50) NOT NULL COMMENT '스킨인스톨',
  PRIMARY KEY (`bbs_id`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bbs_file`
--

CREATE TABLE IF NOT EXISTS `tb_bbs_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bbs_id` varchar(20) NOT NULL COMMENT '게시판',
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '게시물',
  `number` varchar(100) NOT NULL COMMENT '첨부번호',
  `upload_source` varchar(255) NOT NULL COMMENT '첨부파일명',
  `upload_file` varchar(255) NOT NULL COMMENT '변환파일명',
  `upload_filesize` int(11) NOT NULL DEFAULT '0' COMMENT '파일사이즈',
  `upload_width` int(11) NOT NULL DEFAULT '0' COMMENT '파일가로크기',
  `upload_height` int(11) NOT NULL DEFAULT '0' COMMENT '파일세로크기',
  `upload_type` int(11) NOT NULL DEFAULT '0' COMMENT '파일형식',
  `upload_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '업로드일시',
  `upload_count` int(11) NOT NULL DEFAULT '0' COMMENT '다운수',
  PRIMARY KEY (`id`),
  KEY `article` (`bbs_id`,`article_id`),
  KEY `articlenumber` (`bbs_id`,`article_id`,`number`),
  KEY `articleimg` (`bbs_id`,`article_id`,`upload_type`),
  KEY `bbs_id` (`bbs_id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bbs_good`
--

CREATE TABLE IF NOT EXISTS `tb_bbs_good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `ip` varchar(50) NOT NULL COMMENT 'ip',
  `good` tinyint(4) NOT NULL DEFAULT '0',
  `bbs_id` varchar(20) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `reply_id` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `member` (`mid`,`datetime`),
  KEY `bbs` (`mid`,`bbs_id`,`article_id`),
  KEY `reply mid` (`mid`,`bbs_id`,`reply_id`),
  KEY `reply` (`bbs_id`,`reply_id`),
  KEY `datetime` (`good`,`datetime`),
  KEY `ip` (`ip`,`bbs_id`,`article_id`,`reply_id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bbs_group`
--

CREATE TABLE IF NOT EXISTS `tb_bbs_group` (
  `bbs_group` varchar(50) NOT NULL COMMENT '그룹아이디',
  `bbs_group_title` varchar(100) NOT NULL COMMENT '그룹명',
  `bbs_group_position` int(11) NOT NULL DEFAULT '0' COMMENT '출력순서',
  `bbs_group_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '상태',
  `bbs_group_extend1_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend1_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend2_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend2_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend3_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend3_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend4_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend4_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend5_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend5_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend6_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend6_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend7_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend7_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend8_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend8_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend9_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend9_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_group_extend10_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `bbs_group_extend10_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `bbs_count` int(11) NOT NULL DEFAULT '0' COMMENT '게시판수',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '생성일시',
  PRIMARY KEY (`bbs_group`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bbs_police`
--

CREATE TABLE IF NOT EXISTS `tb_bbs_police` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bbs_id` varchar(20) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `reply_id` int(11) NOT NULL DEFAULT '0',
  `mid` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `reason` tinyint(4) NOT NULL DEFAULT '0',
  `mode` tinyint(4) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `bbs` (`bbs_id`,`article_id`),
  KEY `reply` (`bbs_id`,`article_id`,`reply_id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bbs_scrap`
--

CREATE TABLE IF NOT EXISTS `tb_bbs_scrap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `bbs_id` varchar(50) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `ar_title` varchar(255) NOT NULL,
  `ar_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `position` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `index_position` (`mid`,`position`,`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_connect`
--

CREATE TABLE IF NOT EXISTS `tb_connect` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '고유번호',
  `user` varchar(50) NOT NULL DEFAULT '' COMMENT '접속자',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT '아이피',
  `mid` int(11) NOT NULL COMMENT '회원',
  `robot` tinyint(4) NOT NULL DEFAULT '0' COMMENT '로봇',
  `title` varchar(100) NOT NULL COMMENT '제목',
  `host` varchar(100) NOT NULL COMMENT '호스트',
  `url` varchar(100) NOT NULL COMMENT '경로',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '최초',
  `updatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '갱신',
  `loadtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '로드',
  PRIMARY KEY (`id`),
  KEY `load` (`loadtime`),
  KEY `ip` (`ip`)
)  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_email`
--

CREATE TABLE IF NOT EXISTS `tb_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '발송회원',
  `name` varchar(50) NOT NULL COMMENT '발송이름',
  `send_email` varchar(100) NOT NULL COMMENT '발송메일',
  `email` varchar(100) NOT NULL COMMENT '수신메일',
  `title` varchar(100) NOT NULL COMMENT '제목',
  `content` longtext NOT NULL COMMENT '내용',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '발송일시',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member`
--

CREATE TABLE IF NOT EXISTS `tb_member` (
  `mid` int(11) NOT NULL AUTO_INCREMENT COMMENT '고유키',
  `uid` varchar(50) NOT NULL COMMENT '아이디',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '레벨',
  `point` double NOT NULL DEFAULT '0' COMMENT '포인트',
  `nick` varchar(50) NOT NULL COMMENT '닉네임',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '성명',
  `birth` date NOT NULL DEFAULT '0000-00-00' COMMENT '생년월일',
  `sex` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성별',
  `hp` varchar(50) NOT NULL DEFAULT '' COMMENT '휴대폰',
  `hp_consent` tinyint(4) NOT NULL DEFAULT '0' COMMENT '휴대폰수신동의',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '이메일',
  `email_consent` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이메일수신동의',
  `tel` varchar(50) NOT NULL DEFAULT '' COMMENT '일반전화',
  `zipcode` varchar(50) NOT NULL DEFAULT '' COMMENT '우편번호',
  `addr1` varchar(100) NOT NULL DEFAULT '' COMMENT '주소',
  `addr2` varchar(100) NOT NULL DEFAULT '' COMMENT '상세주소',
  `homepage` varchar(255) NOT NULL COMMENT '홈페이지',
  `recommend` int(11) NOT NULL COMMENT '추천인',
  `profile` text NOT NULL COMMENT '자기소개',
  `photo` varchar(100) NOT NULL COMMENT '회원이미지(기본)',
  `text1` varchar(255) NOT NULL COMMENT '텍스트폼1',
  `text2` varchar(255) NOT NULL COMMENT '텍스트폼2',
  `text3` varchar(255) NOT NULL COMMENT '텍스트폼3',
  `single1` varchar(255) NOT NULL COMMENT '단일선택폼1',
  `single2` varchar(255) NOT NULL COMMENT '단일선택폼2',
  `single3` varchar(255) NOT NULL COMMENT '단일선택폼3',
  `multi1` varchar(255) NOT NULL COMMENT '복수선택폼1',
  `multi2` varchar(255) NOT NULL COMMENT '복수선택폼2',
  `multi3` varchar(255) NOT NULL COMMENT '복수선택폼3',
  `good` int(11) NOT NULL DEFAULT '0' COMMENT '추천수',
  `nogood` int(11) NOT NULL DEFAULT '0' COMMENT '비추천수',
  `bbs_write_count` int(11) NOT NULL DEFAULT '0' COMMENT '게시물수',
  `bbs_reply_count` int(11) NOT NULL DEFAULT '0' COMMENT '댓글수',
  `social` int(11) NOT NULL DEFAULT '0' COMMENT '소셜회원',
  `social_key` varchar(100) NOT NULL COMMENT '소셜사용자',
  `social_uid` int(11) NOT NULL DEFAULT '0' COMMENT '아이디변경횟수',
  `social_nick` int(11) NOT NULL DEFAULT '0' COMMENT '닉네임변경횟수',
  `ip` varchar(50) NOT NULL COMMENT '가입ip',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '가입일시',
  `login_ip` varchar(50) NOT NULL COMMENT '로그인ip',
  `login_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '로그인일시',
  `stop` tinyint(4) NOT NULL DEFAULT '0' COMMENT '차단',
  `stop_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '차단일시',
  `dormancy` tinyint(4) NOT NULL DEFAULT '0' COMMENT '휴면상태',
  `dormancy_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '휴면일시',
  `dropout` tinyint(4) NOT NULL DEFAULT '0' COMMENT '탈퇴',
  `dropout_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '탈퇴일시',
  `certify_name` tinyint(4) NOT NULL DEFAULT '0' COMMENT '본인확인',
  `certify_name_ok` varchar(100) NOT NULL COMMENT '본인인증값',
  `certify_name_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '본인인증일시',
  `certify_email` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이메일인증',
  `certify_hp` tinyint(4) NOT NULL DEFAULT '0' COMMENT '휴대폰인증',
  `certify_adult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성인인증',
  `certify_foreigner` tinyint(4) NOT NULL DEFAULT '0' COMMENT '외국인',
  `extend1_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend1_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend2_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend2_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend3_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend3_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend4_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend4_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend5_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend5_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend6_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend6_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend7_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend7_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend8_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend8_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend9_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend9_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `extend10_name` varchar(100) NOT NULL COMMENT '확장필드이름',
  `extend10_data` varchar(100) NOT NULL COMMENT '확장필드값',
  `form_check` varchar(50) NOT NULL COMMENT '폼체크',
  PRIMARY KEY (`mid`),
  UNIQUE KEY `find_pw_email` (`uid`,`email`,`dropout`),
  KEY `uid` (`uid`),
  KEY `dropout` (`dropout`),
  KEY `datetime` (`datetime`),
  KEY `social_key` (`social_key`),
  KEY `find_id_hp` (`hp`,`dropout`),
  KEY `find_id_email` (`email`,`dropout`),
  KEY `find_pw_hp` (`uid`,`hp`,`dropout`),
  KEY `birth` (`birth`,`dropout`),
  KEY `autoadult` (`certify_name`,`certify_adult`),
  KEY `login_datetime` (`login_datetime`),
  KEY `login_ip` (`login_ip`)
)  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member_file`
--

CREATE TABLE IF NOT EXISTS `tb_member_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '회원',
  `upload_mode` varchar(100) NOT NULL COMMENT '첨부구분',
  `upload_source` varchar(255) NOT NULL COMMENT '첨부파일명',
  `upload_file` varchar(255) NOT NULL COMMENT '변환파일명',
  `upload_filesize` int(11) NOT NULL DEFAULT '0' COMMENT '파일사이즈',
  `upload_width` int(11) NOT NULL DEFAULT '0' COMMENT '파일가로크기',
  `upload_height` int(11) NOT NULL DEFAULT '0' COMMENT '파일세로크기',
  `upload_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '업로드일시',
  PRIMARY KEY (`id`),
  KEY `list` (`mid`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member_level`
--

CREATE TABLE IF NOT EXISTS `tb_member_level` (
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '레벨',
  `title` varchar(100) NOT NULL COMMENT '레벨명',
  `levelup` tinyint(4) NOT NULL DEFAULT '0' COMMENT '자동등업',
  `article` int(11) NOT NULL DEFAULT '0' COMMENT '게시글',
  `reply` int(11) NOT NULL DEFAULT '0' COMMENT '댓글',
  `day` int(11) NOT NULL DEFAULT '0' COMMENT '가입',
  PRIMARY KEY (`level`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member_login`
--

CREATE TABLE IF NOT EXISTS `tb_member_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '회원',
  `ip` varchar(50) NOT NULL COMMENT '아이피',
  `browser` varchar(50) NOT NULL COMMENT 'browser',
  `os` varchar(50) NOT NULL COMMENT 'os',
  `login` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성공유무',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '접근일시',
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`,`datetime`),
  KEY `login` (`mid`,`login`,`datetime`),
  KEY `ip` (`ip`),
  KEY `datetime` (`login`,`datetime`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member_photo`
--

CREATE TABLE IF NOT EXISTS `tb_member_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '회원',
  `onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사용유무',
  `area_x1` int(11) NOT NULL DEFAULT '0' COMMENT '셀렉터좌표',
  `area_y1` int(11) NOT NULL DEFAULT '0' COMMENT '셀렉터좌표',
  `area_x2` int(11) NOT NULL DEFAULT '0' COMMENT '셀렉터좌표',
  `area_y2` int(11) NOT NULL DEFAULT '0' COMMENT '셀렉터좌표',
  `area_w` int(11) NOT NULL DEFAULT '0' COMMENT '셀렉터가로',
  `area_h` int(11) NOT NULL DEFAULT '0' COMMENT '셀렉터세로',
  `upload_source` varchar(255) NOT NULL COMMENT '첨부파일명',
  `upload_file` varchar(255) NOT NULL COMMENT '변환파일명',
  `upload_filesize` int(11) NOT NULL DEFAULT '0' COMMENT '파일사이즈',
  `upload_width` int(11) NOT NULL DEFAULT '0' COMMENT '파일가로크기',
  `upload_height` int(11) NOT NULL DEFAULT '0' COMMENT '파일세로크기',
  `upload_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '업로드일시',
  PRIMARY KEY (`id`),
  KEY `onoff` (`mid`,`onoff`),
  KEY `list` (`mid`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_member_point`
--

CREATE TABLE IF NOT EXISTS `tb_member_point` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '회원',
  `point` double NOT NULL DEFAULT '0' COMMENT '포인트',
  `this_point` double NOT NULL DEFAULT '0' COMMENT '누적',
  `ct` int(11) NOT NULL DEFAULT '0' COMMENT '분류',
  `content` varchar(255) NOT NULL COMMENT '내용',
  `chk` varchar(100) NOT NULL COMMENT '검증키',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '발생일시',
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`),
  KEY `insert` (`mid`,`chk`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_message_receive`
--

CREATE TABLE IF NOT EXISTS `tb_message_receive` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '수신회원',
  `fid` int(11) NOT NULL COMMENT '발송회원',
  `content` text NOT NULL COMMENT '내용',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '저장여부',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '발송일',
  `readtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '열람일',
  PRIMARY KEY (`id`),
  KEY `keyword` (`mid`,`type`,`fid`,`datetime`),
  KEY `default` (`mid`,`type`,`datetime`),
  KEY `send` (`mid`,`fid`,`datetime`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_message_send`
--

CREATE TABLE IF NOT EXISTS `tb_message_send` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '발송회원',
  `tid` int(11) NOT NULL COMMENT '수신회원',
  `content` mediumtext NOT NULL COMMENT '내용',
  `code` bigint(20) NOT NULL DEFAULT '0' COMMENT '고유코드',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '발송일시',
  `readtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '열람일시',
  PRIMARY KEY (`id`),
  KEY `default` (`mid`,`datetime`),
  KEY `keyword` (`mid`,`tid`,`datetime`),
  KEY `code` (`code`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_notice`
--

CREATE TABLE IF NOT EXISTS `tb_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '닉네임',
  `bbs_id` varchar(50) NOT NULL,
  `bbs_title` varchar(50) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `ar_title` varchar(255) NOT NULL,
  `ar_reply` int(11) NOT NULL DEFAULT '0',
  `content` varchar(255) NOT NULL,
  `reply` tinyint(4) NOT NULL DEFAULT '0',
  `reply_id` int(11) NOT NULL DEFAULT '0',
  `reply_first` int(11) NOT NULL DEFAULT '0',
  `reply_new` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updatetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `readtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `member` (`mid`,`reply`,`updatetime`),
  KEY `all` (`reply`,`updatetime`),
  KEY `bbs` (`bbs_id`,`article_id`),
  KEY `bbs_list` (`bbs_id`,`article_id`,`reply`),
  KEY `reply_delete` (`bbs_id`,`reply_id`),
  KEY `mid` (`mid`,`bbs_id`,`article_id`),
  KEY `reply_first` (`bbs_id`,`reply_first`),
  KEY `datetime` (`reply`,`datetime`),
  KEY `nick` (`nick`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_page`
--

CREATE TABLE IF NOT EXISTS `tb_page` (
  `page_id` varchar(50) NOT NULL COMMENT '페이지 ID',
  `page_group` varchar(100) NOT NULL COMMENT '페이지그룹',
  `page_title` varchar(100) NOT NULL COMMENT '페이지명',
  `page_subject` varchar(100) NOT NULL COMMENT '부제목',
  `page_position` int(11) NOT NULL DEFAULT '0' COMMENT '출력순서',
  `page_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '상태',
  `page_skin` varchar(50) NOT NULL COMMENT '페이지스킨',
  `page_blank` tinyint(4) NOT NULL DEFAULT '0' COMMENT '링크방식',
  `page_url` varchar(255) NOT NULL COMMENT '링크경로',
  `page_adult` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성인전용',
  `page_level` int(11) NOT NULL DEFAULT '0' COMMENT '접근레벨',
  `page_mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '제작 방식',
  `page_include_top` varchar(100) NOT NULL COMMENT '상단 파일',
  `page_include_bottom` varchar(100) NOT NULL COMMENT '하단 파일',
  `page_content` longtext NOT NULL COMMENT '내용',
  `page_text_top` longtext NOT NULL COMMENT '상단 디자인 내용',
  `page_text_bottom` longtext NOT NULL COMMENT '하단 디자인 내용',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '생성일시',
  PRIMARY KEY (`page_id`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_page_group`
--

CREATE TABLE IF NOT EXISTS `tb_page_group` (
  `page_group` varchar(50) NOT NULL COMMENT '그룹아이디',
  `page_group_title` varchar(100) NOT NULL COMMENT '그룹명',
  `page_group_position` int(11) NOT NULL DEFAULT '0' COMMENT '출력순서',
  `page_group_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '상태',
  `page_count` int(11) NOT NULL DEFAULT '0' COMMENT '페이지수',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '생성일시',
  PRIMARY KEY (`page_group`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_popup`
--

CREATE TABLE IF NOT EXISTS `tb_popup` (
  `popup_id` int(11) NOT NULL AUTO_INCREMENT,
  `popup_title` varchar(100) NOT NULL COMMENT '팝업창명',
  `popup_start` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '시작일',
  `popup_end` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '종료일',
  `popup_x` int(11) NOT NULL DEFAULT '0' COMMENT '팝업창 위치x',
  `popup_y` int(11) NOT NULL DEFAULT '0' COMMENT '팝업창 위치y',
  `popup_z` int(11) NOT NULL DEFAULT '0' COMMENT '팝업창 위치z',
  `popup_mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '제작 방식',
  `popup_width` int(11) NOT NULL DEFAULT '0' COMMENT '창 크기 가로',
  `popup_height` int(11) NOT NULL DEFAULT '0' COMMENT '창 크기 세로',
  `popup_content` longtext NOT NULL COMMENT '팝업 내용',
  `popup_blank` tinyint(4) NOT NULL DEFAULT '0' COMMENT '연결방식',
  `popup_url` varchar(255) NOT NULL COMMENT '연결URL',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '생성일시',
  `upload_source` varchar(255) NOT NULL COMMENT '첨부파일명',
  `upload_file` varchar(255) NOT NULL COMMENT '변환파일명',
  `upload_filesize` int(11) NOT NULL DEFAULT '0' COMMENT '파일사이즈',
  `upload_width` int(11) NOT NULL DEFAULT '0' COMMENT '파일가로크기',
  `upload_height` int(11) NOT NULL DEFAULT '0' COMMENT '파일세로크기',
  `upload_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '업로드일시',
  PRIMARY KEY (`popup_id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_real_email`
--

CREATE TABLE IF NOT EXISTS `tb_real_email` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '회원',
  `email` varchar(100) NOT NULL COMMENT '이메일주소',
  `code` varchar(20) NOT NULL COMMENT '인증코드',
  `ip` varchar(50) NOT NULL COMMENT '아이피',
  `mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '인증유무',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '발송일시',
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`,`datetime`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_real_hp`
--

CREATE TABLE IF NOT EXISTS `tb_real_hp` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '회원',
  `hp` varchar(20) NOT NULL COMMENT '휴대폰번호',
  `code` varchar(20) NOT NULL COMMENT '인증코드',
  `ip` varchar(50) NOT NULL COMMENT '아이피',
  `mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '인증유무',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '발송일시',
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`,`datetime`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_real_name`
--

CREATE TABLE IF NOT EXISTS `tb_real_name` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '회원',
  `ip` varchar(50) NOT NULL COMMENT '아이피',
  `mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '인증유무',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '인증일시',
  PRIMARY KEY (`id`),
  KEY `ip` (`ip`,`datetime`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reply_bbs1`
--

CREATE TABLE IF NOT EXISTS `tb_reply_bbs1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '게시물아이디',
  `reply_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `content` text NOT NULL COMMENT '내용',
  `good` int(11) NOT NULL DEFAULT '0',
  `nogood` int(11) NOT NULL DEFAULT '0',
  `blind` varchar(100) NOT NULL COMMENT '블라인드',
  `del` tinyint(4) NOT NULL DEFAULT '0' COMMENT '삭제',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `list` (`article_id`,`reply_id`,`datetime`),
  KEY `good` (`article_id`,`good`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reply_bbs2`
--

CREATE TABLE IF NOT EXISTS `tb_reply_bbs2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '게시물아이디',
  `reply_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `content` text NOT NULL COMMENT '내용',
  `good` int(11) NOT NULL DEFAULT '0',
  `nogood` int(11) NOT NULL DEFAULT '0',
  `blind` varchar(100) NOT NULL COMMENT '블라인드',
  `del` tinyint(4) NOT NULL DEFAULT '0' COMMENT '삭제',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `list` (`article_id`,`reply_id`,`datetime`),
  KEY `good` (`article_id`,`good`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reply_bbs3`
--

CREATE TABLE IF NOT EXISTS `tb_reply_bbs3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '게시물아이디',
  `reply_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `content` text NOT NULL COMMENT '내용',
  `good` int(11) NOT NULL DEFAULT '0',
  `nogood` int(11) NOT NULL DEFAULT '0',
  `blind` varchar(100) NOT NULL COMMENT '블라인드',
  `del` tinyint(4) NOT NULL DEFAULT '0' COMMENT '삭제',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `list` (`article_id`,`reply_id`,`datetime`),
  KEY `good` (`article_id`,`good`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reply_bbs4`
--

CREATE TABLE IF NOT EXISTS `tb_reply_bbs4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL COMMENT '아이피',
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '게시물아이디',
  `reply_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `content` text NOT NULL COMMENT '내용',
  `good` int(11) NOT NULL DEFAULT '0',
  `nogood` int(11) NOT NULL DEFAULT '0',
  `blind` varchar(100) NOT NULL COMMENT '블라인드',
  `del` tinyint(4) NOT NULL DEFAULT '0' COMMENT '삭제',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `list` (`article_id`,`reply_id`,`datetime`),
  KEY `good` (`article_id`,`good`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_reply_notice`
--

CREATE TABLE IF NOT EXISTS `tb_reply_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '작성자',
  `upw` varchar(50) NOT NULL COMMENT '비밀번호',
  `ip` varchar(50) NOT NULL,
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '게시물아이디',
  `reply_id` int(11) NOT NULL DEFAULT '0' COMMENT '원본아이디',
  `content` text NOT NULL COMMENT '내용',
  `good` int(11) NOT NULL DEFAULT '0',
  `nogood` int(11) NOT NULL DEFAULT '0',
  `blind` varchar(100) NOT NULL COMMENT '블라인드',
  `del` tinyint(4) NOT NULL DEFAULT '0' COMMENT '삭제',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '작성일',
  PRIMARY KEY (`id`),
  KEY `list` (`article_id`,`reply_id`,`datetime`),
  KEY `good` (`article_id`,`good`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_search`
--

CREATE TABLE IF NOT EXISTS `tb_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL,
  `nick` varchar(50) NOT NULL COMMENT '닉네임',
  `bbs_group` varchar(100) NOT NULL COMMENT '게시판그룹',
  `bbs_id` varchar(50) NOT NULL,
  `bbs_search` tinyint(4) NOT NULL DEFAULT '0' COMMENT '통합검색',
  `article_id` int(11) NOT NULL DEFAULT '0',
  `ar_img` tinyint(4) NOT NULL DEFAULT '0',
  `ar_hit` int(11) NOT NULL DEFAULT '0',
  `ar_good` int(11) NOT NULL DEFAULT '0',
  `ar_nogood` int(11) NOT NULL DEFAULT '0',
  `ar_reply` int(11) NOT NULL DEFAULT '0',
  `ar_adult` tinyint(4) NOT NULL DEFAULT '0',
  `ar_blind` tinyint(4) NOT NULL DEFAULT '0',
  `q` varchar(100) NOT NULL,
  `ar_jump` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `q` (`q`),
  KEY `update` (`bbs_id`,`article_id`),
  KEY `nick` (`nick`),
  KEY `nick_bbs` (`nick`,`bbs_id`),
  KEY `mid` (`mid`),
  KEY `mid_q` (`mid`,`q`),
  KEY `bbs_group_q` (`bbs_group`,`bbs_search`,`q`),
  KEY `img_q` (`bbs_search`,`ar_img`,`q`),
  KEY `good_q` (`bbs_search`,`ar_good`,`q`),
  KEY `bbs_search` (`bbs_search`),
  KEY `datetime` (`bbs_search`,`datetime`),
  KEY `img_q2` (`bbs_search`,`ar_img`,`ar_adult`,`q`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_session`
--

CREATE TABLE IF NOT EXISTS `tb_session` (
  `id` varchar(32) NOT NULL,
  `ss_datetime` datetime NOT NULL,
  `ss_data` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `se_datetime` (`ss_datetime`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_setup`
--

CREATE TABLE IF NOT EXISTS `tb_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL COMMENT '도메인',
  `license` varchar(50) NOT NULL COMMENT '라이센스',
  `ver` int(11) NOT NULL DEFAULT '0' COMMENT '버전',
  `template` varchar(100) NOT NULL COMMENT '템플릿',
  `editor` int(11) NOT NULL DEFAULT '0' COMMENT '에디터',
  `title` varchar(100) NOT NULL COMMENT '사이트명',
  `browsertitle` varchar(100) NOT NULL COMMENT '브라우저타이틀',
  `company` varchar(100) NOT NULL,
  `number1` varchar(100) NOT NULL COMMENT '사업자 등록번호',
  `number2` varchar(100) NOT NULL COMMENT '통신판매업 신고번호',
  `ceo` varchar(100) NOT NULL COMMENT '대표',
  `tel` varchar(100) NOT NULL COMMENT '대표전화',
  `hp` varchar(50) NOT NULL COMMENT '휴대폰',
  `fax` varchar(100) NOT NULL COMMENT '팩스 번호',
  `callernumber` varchar(50) NOT NULL COMMENT '발신번호',
  `email` varchar(100) NOT NULL COMMENT '대표메일',
  `addr` varchar(100) NOT NULL COMMENT '회사주소',
  `privace_name` varchar(100) NOT NULL COMMENT '개인정보 책임자명',
  `privace_email` varchar(100) NOT NULL COMMENT '개인정보 책임자 메일',
  `icon_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '등급 아이콘 사용',
  `point_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '포인트 기능 사용',
  `point_number` int(11) NOT NULL DEFAULT '0' COMMENT '포인트소수점',
  `point_join` double NOT NULL DEFAULT '0' COMMENT '회원가입포인트',
  `point_login` double NOT NULL DEFAULT '0' COMMENT '로그인포인트',
  `point_recommend` double NOT NULL DEFAULT '0' COMMENT '추천인포인트(가입자)',
  `point_recommend_target` double NOT NULL DEFAULT '0' COMMENT '추천인포인트(추천자)',
  `point_reset` int(11) NOT NULL DEFAULT '0' COMMENT '포인트초기화',
  `nick_change_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '닉네임수정',
  `sideview_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사이드 뷰어',
  `sideview_message` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사이드뷰어메세지',
  `sideview_email` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사이드뷰어이메일',
  `sideview_sms` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사이드뷰어문자발송',
  `sideview_homepage` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사이드뷰어홈페이지',
  `sideview_profile` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사이드뷰어자기소개',
  `sideview_bbs` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사이드뷰어게시물 보기',
  `image_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '프로필 이미지',
  `block_ip` text NOT NULL COMMENT '차단 IP',
  `block_keyword` text NOT NULL COMMENT '차단 키워드',
  `block_id` text NOT NULL COMMENT '사용불가 ID/닉네임',
  `bbs_write_time` int(11) NOT NULL DEFAULT '0' COMMENT '게시물 작성 제한',
  `message_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '메세지 사용',
  `message_join` text NOT NULL COMMENT '가입 축하 메세지',
  `message_join_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '가입 축하 메세지사용',
  `message_birth` text NOT NULL COMMENT '생일 축하 메세지',
  `message_birth_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '생일 축하 메세지사용',
  `sms_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'SMS문자 사용',
  `sms_real` varchar(255) NOT NULL COMMENT '인증코드 문자 발송',
  `sms_join` varchar(255) NOT NULL COMMENT '가입 축하 문자',
  `sms_join_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '가입 축하 문자사용',
  `sms_birth` varchar(255) NOT NULL COMMENT '생일 축하 문자',
  `sms_birth_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '생일 축하 문자사용',
  `sms_uid` varchar(255) NOT NULL COMMENT '아이디 문자 발송',
  `sms_upw` varchar(255) NOT NULL COMMENT '비밀번호 문자 발송',
  `email_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이메일 사용',
  `analytics_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '구글 애널리틱스',
  `analytics_code` text NOT NULL COMMENT '구글 애널리틱스 소스',
  `del_dormancy_day` int(11) NOT NULL DEFAULT '0' COMMENT '휴면아이디기간',
  `del_dropout_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '회원탈퇴',
  `del_dropout_day` int(11) NOT NULL DEFAULT '0' COMMENT '회원탈퇴기간',
  `del_visit_day` int(11) NOT NULL DEFAULT '0' COMMENT '방문자 접근 기록',
  `del_login_day` int(11) NOT NULL DEFAULT '0' COMMENT '회원 로그인 기록',
  `del_real_day` int(11) NOT NULL DEFAULT '0' COMMENT '인증 기록',
  `del_message_day` int(11) NOT NULL DEFAULT '0' COMMENT '메세지',
  `del_email_day` int(11) NOT NULL DEFAULT '0' COMMENT '이메일 발송 내역',
  `del_sms_day` int(11) NOT NULL DEFAULT '0' COMMENT '문자 발송 내역',
  `del_point_day` int(11) NOT NULL DEFAULT '0' COMMENT '포인트 지급내역',
  `image_scaling` int(11) NOT NULL DEFAULT '0' COMMENT '이미지 스케일링',
  `gifsicle_path` varchar(100) NOT NULL COMMENT 'GIF Sicle 경로',
  `cronkey` varchar(50) NOT NULL COMMENT '크론키값',
  `crondate` date NOT NULL DEFAULT '0000-00-00' COMMENT '크론실행일',
  `installdate` date NOT NULL DEFAULT '0000-00-00' COMMENT '설치일',
  PRIMARY KEY (`id`)
)  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_setup_api`
--

CREATE TABLE IF NOT EXISTS `tb_setup_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icode_id` varchar(100) NOT NULL COMMENT '아이코드ID',
  `icode_pw` varchar(100) NOT NULL COMMENT '아이코드ㅖㅉ',
  `kcb_id` varchar(100) NOT NULL COMMENT 'kcb 회원사 ID',
  `social_login_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '소셜 연동',
  `login_naver` tinyint(4) NOT NULL DEFAULT '0' COMMENT '네이버 로그인',
  `login_naver_count` int(11) NOT NULL DEFAULT '0' COMMENT '네이버 순번',
  `login_naver_id` varchar(100) NOT NULL COMMENT '네이버 Client ID',
  `login_naver_secret` varchar(100) NOT NULL COMMENT '네이버 Client Secret',
  `login_kakao` tinyint(4) NOT NULL DEFAULT '0' COMMENT '카카오톡 로그인',
  `login_kakao_count` int(11) NOT NULL DEFAULT '0' COMMENT '카카오 순번',
  `login_kakao_key` varchar(100) NOT NULL COMMENT '카카오톡 REST API',
  `login_facebook` tinyint(4) NOT NULL DEFAULT '0' COMMENT '페이스북 로그인',
  `login_facebook_count` int(11) NOT NULL DEFAULT '0' COMMENT '페이스북 순번',
  `login_facebook_id` varchar(100) NOT NULL COMMENT '페이스북 App ID',
  `login_facebook_secret` varchar(100) NOT NULL COMMENT '페이스북 App Secret',
  `login_twitter` tinyint(4) NOT NULL DEFAULT '0' COMMENT '트위터 로그인',
  `login_twitter_count` int(11) NOT NULL DEFAULT '0' COMMENT '트위터 순번',
  `login_twitter_key` varchar(100) NOT NULL COMMENT '트위터 Consumer Key',
  `login_twitter_secret` varchar(100) NOT NULL COMMENT '트위터 Consumer Secret',
  `login_google` tinyint(4) NOT NULL DEFAULT '0' COMMENT '구글 로그인',
  `login_google_count` int(11) NOT NULL DEFAULT '0' COMMENT '구글 순번',
  `login_google_id` varchar(100) NOT NULL COMMENT '구글 클라이언트 ID',
  `login_google_secret` varchar(100) NOT NULL COMMENT '구글 클라이언트 보안 비밀',
  `login_instagram` tinyint(4) NOT NULL DEFAULT '0' COMMENT '인스타그램 로그인',
  `login_instagram_count` int(11) NOT NULL DEFAULT '0' COMMENT '인스타그램 순번',
  `login_instagram_id` varchar(100) NOT NULL COMMENT '인스타그램 Client ID',
  `login_instagram_secret` varchar(100) NOT NULL COMMENT '인스타그램 Client Secret',
  `zipcode_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '도로명 주소',
  `zipcode_host` varchar(100) NOT NULL COMMENT '도로명 주소 호스트',
  `syndi_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '네이버 신디케이션',
  `syndi_token` varchar(100) NOT NULL COMMENT '네이버 신디케이션 token',
  `daum_onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '다음 API',
  `daum_apikey` varchar(100) NOT NULL COMMENT '다음 apikey',
  PRIMARY KEY (`id`)
)  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_setup_auth`
--

CREATE TABLE IF NOT EXISTS `tb_setup_auth` (
  `menu_id` varchar(100) NOT NULL COMMENT 'menu_id',
  `menu_num` varchar(100) NOT NULL COMMENT 'menu_num',
  `title` varchar(100) NOT NULL COMMENT '제목',
  `url` varchar(100) NOT NULL COMMENT '링크',
  `blank` tinyint(4) NOT NULL DEFAULT '0' COMMENT '새창',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT '순서',
  `full_uid` text NOT NULL COMMENT '전체관리',
  `read_uid` text NOT NULL COMMENT '열람전용',
  PRIMARY KEY (`menu_id`,`menu_num`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_setup_email`
--

CREATE TABLE IF NOT EXISTS `tb_setup_email` (
  `id` varchar(100) NOT NULL,
  `onoff` tinyint(4) NOT NULL DEFAULT '0' COMMENT '사용유무',
  `title` varchar(100) NOT NULL COMMENT '인증메일제목',
  `content` longtext NOT NULL COMMENT '인증메일내용',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_setup_join`
--

CREATE TABLE IF NOT EXISTS `tb_setup_join` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hp` tinyint(4) NOT NULL DEFAULT '0' COMMENT '휴대폰',
  `email` tinyint(4) NOT NULL DEFAULT '0' COMMENT '이메일',
  `name` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성명',
  `sex` tinyint(4) NOT NULL DEFAULT '0' COMMENT '성별',
  `birth` tinyint(4) NOT NULL DEFAULT '0' COMMENT '생년월일',
  `tel` tinyint(4) NOT NULL DEFAULT '0' COMMENT '일반전화',
  `addr` tinyint(4) NOT NULL DEFAULT '0' COMMENT '주소',
  `homepage` tinyint(4) NOT NULL DEFAULT '0' COMMENT '홈페이지',
  `recommend` tinyint(4) NOT NULL DEFAULT '0' COMMENT '추천인아이디',
  `profile` tinyint(4) NOT NULL DEFAULT '0' COMMENT '자기소개',
  `text1` tinyint(4) NOT NULL DEFAULT '0' COMMENT '텍스트폼',
  `text1_title` varchar(100) NOT NULL COMMENT '텍스트폼제목',
  `text1_help` varchar(100) NOT NULL COMMENT '텍스트폼설명',
  `text2` tinyint(4) NOT NULL DEFAULT '0' COMMENT '텍스트폼',
  `text2_title` varchar(100) NOT NULL COMMENT '텍스트폼제목',
  `text2_help` varchar(100) NOT NULL COMMENT '텍스트폼설명',
  `text3` tinyint(4) NOT NULL DEFAULT '0' COMMENT '텍스트폼',
  `text3_title` varchar(100) NOT NULL COMMENT '텍스트폼제목',
  `text3_help` varchar(100) NOT NULL COMMENT '텍스트폼설명',
  `file1` tinyint(4) NOT NULL DEFAULT '0' COMMENT '첨부파일폼',
  `file1_title` varchar(100) NOT NULL COMMENT '첨부파일폼제목',
  `file1_help` varchar(100) NOT NULL COMMENT '첨부파일폼설명',
  `file2` tinyint(4) NOT NULL DEFAULT '0' COMMENT '첨부파일폼',
  `file2_title` varchar(100) NOT NULL COMMENT '첨부파일폼제목',
  `file2_help` varchar(100) NOT NULL COMMENT '첨부파일폼설명',
  `file3` tinyint(4) NOT NULL DEFAULT '0' COMMENT '첨부파일폼',
  `file3_title` varchar(100) NOT NULL COMMENT '첨부파일폼제목',
  `file3_help` varchar(100) NOT NULL COMMENT '첨부파일폼설명',
  `single1` tinyint(4) NOT NULL DEFAULT '0' COMMENT '단일선택폼',
  `single1_title` varchar(100) NOT NULL COMMENT '단일선택제목',
  `single1_help` varchar(100) NOT NULL COMMENT '단일선택설명',
  `single1_list` varchar(255) NOT NULL COMMENT '단일선택항목',
  `single2` tinyint(4) NOT NULL DEFAULT '0' COMMENT '단일선택폼',
  `single2_title` varchar(100) NOT NULL COMMENT '단일선택제목',
  `single2_help` varchar(100) NOT NULL COMMENT '단일선택설명',
  `single2_list` varchar(255) NOT NULL COMMENT '단일선택항목',
  `single3` tinyint(4) NOT NULL DEFAULT '0' COMMENT '단일선택폼',
  `single3_title` varchar(100) NOT NULL COMMENT '단일선택제목',
  `single3_help` varchar(100) NOT NULL COMMENT '단일선택설명',
  `single3_list` varchar(255) NOT NULL COMMENT '단일선택항목',
  `multi1` tinyint(4) NOT NULL DEFAULT '0' COMMENT '복수선택폼',
  `multi1_title` varchar(100) NOT NULL COMMENT '복수선택폼제목',
  `multi1_help` varchar(100) NOT NULL COMMENT '복수선택폼설명',
  `multi1_count` int(11) NOT NULL DEFAULT '0' COMMENT '복수선택폼선택',
  `multi1_list` varchar(255) NOT NULL COMMENT '복수선택폼항목',
  `multi2` tinyint(4) NOT NULL DEFAULT '0' COMMENT '복수선택폼',
  `multi2_title` varchar(100) NOT NULL COMMENT '복수선택폼제목',
  `multi2_help` varchar(100) NOT NULL COMMENT '복수선택폼설명',
  `multi2_count` int(11) NOT NULL DEFAULT '0' COMMENT '복수선택폼선택',
  `multi2_list` varchar(255) NOT NULL COMMENT '복수선택폼항목',
  `multi3` tinyint(4) NOT NULL DEFAULT '0' COMMENT '복수선택폼',
  `multi3_title` varchar(100) NOT NULL COMMENT '복수선택폼제목',
  `multi3_help` varchar(100) NOT NULL COMMENT '복수선택폼설명',
  `multi3_count` int(11) NOT NULL DEFAULT '0' COMMENT '복수선택폼선택',
  `multi3_list` varchar(255) NOT NULL COMMENT '복수선택폼항목',
  PRIMARY KEY (`id`)
)  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_sms`
--

CREATE TABLE IF NOT EXISTS `tb_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '발송회원',
  `send_hp` varchar(100) NOT NULL COMMENT '발송휴대폰번호',
  `hp` varchar(100) NOT NULL COMMENT '수신번호',
  `content` varchar(255) NOT NULL COMMENT '내용',
  `mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT '전송유형',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '발송일시',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_visit`
--

CREATE TABLE IF NOT EXISTS `tb_visit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '고유번호',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'ip',
  `browser` varchar(50) NOT NULL DEFAULT '' COMMENT 'browser',
  `os` varchar(50) NOT NULL DEFAULT '' COMMENT 'os',
  `referer` varchar(255) NOT NULL DEFAULT '' COMMENT 'referer',
  `keyword` varchar(100) NOT NULL DEFAULT '' COMMENT '키워드',
  `connect` varchar(255) NOT NULL COMMENT '접속위치',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '접속일시',
  PRIMARY KEY (`id`),
  KEY `datetime` (`datetime`),
  KEY `chk` (`ip`,`datetime`),
  KEY `keyword` (`keyword`,`datetime`),
  KEY `referer` (`referer`,`datetime`)
)  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_visit_page`
--

CREATE TABLE IF NOT EXISTS `tb_visit_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '고유번호',
  `mid` int(11) NOT NULL COMMENT 'mid',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'ip',
  `browser` varchar(50) NOT NULL COMMENT 'browser',
  `os` varchar(50) NOT NULL COMMENT 'os',
  `title` varchar(255) NOT NULL COMMENT '페이지명',
  `connect` varchar(255) NOT NULL COMMENT '접속위치',
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '접속일시',
  PRIMARY KEY (`id`),
  KEY `datetime` (`datetime`),
  KEY `ip` (`ip`),
  KEY `mid` (`mid`)
)  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
