<?php
include_once("../tb-common.php");

if (!$check_login) {

    $mb = member(1);

    set_session('ss_mid', $mb['mid'].'|'.$_SERVER['REMOTE_ADDR'].'|'.md5($_SERVER['HTTP_USER_AGENT'].$mb['datetime']));

}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<meta charset="<?=$web['charset']?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,chrome=1" />
<title>테라보드 설치 완료</title>
<link rel="stylesheet" href="http://fonts.googleapis.com/earlyaccess/nanumgothic.css" type="text/css" />
<link rel="stylesheet" href="<?=$web['host_css']?>/web.css" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<style type="text/css">
body {min-width:320px; min-height:650px; height:100%; background:url('img/bg_pattern.png') repeat;}

.layout-install {font-size:0; line-height:0; margin:0; padding-bottom:150px;}
.layout-install .wrap {padding-top:30px; max-width:960px; width:100%; margin:0 auto;}
.layout-install .logo {margin:0 auto; width:252px; height:190px; background:url('img/logo.png') no-repeat;}
.layout-install .logo.on {background-position:0 -190px;}
.layout-install .logo.on2 {background-position:0 -380px;}
.layout-install .title {text-align:center; margin-top:20px; font-weight:400; line-height:60px; font-size:60px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process {margin-top:30px; border-top:1px solid #73c3e4; border-bottom:1px solid #73c3e4; padding:15px 0;}
.layout-install .process:after {display:block; clear:both; content:'';}
.layout-install .process ul:first-child {float:left;}
.layout-install .process ul:last-child {float:right;}
.layout-install .process ul li {display:inline-block; vertical-align:top;}
.layout-install .process ul:first-child li {font-weight:400; line-height:45px; font-size:20px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process ul:last-child li {margin-left:10px; border:2px solid #73c3e4; border-radius:25px; padding:0 45px 2px 45px;}
.layout-install .process ul:last-child li {font-weight:400; line-height:45px; font-size:20px; color:#73c3e4; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .process ul:last-child li:first-child {margin-left:0;}
.layout-install .process ul:last-child li.on {border:2px solid #ffffff; color:#ffffff;}
.layout-install .msg {margin-top:40px;}
.layout-install .msg .subj {text-align:center; font-weight:400; line-height:36px; font-size:36px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .msg .text {text-align:center; font-weight:400; line-height:30px; font-size:16px; color:#ffffff; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .btn {margin-top:40px; border-top:1px solid #73c3e4; padding-top:20px; text-align:center;}
.layout-install .btn ul li {display:inline-block; vertical-align:top; margin-left:20px;}
.layout-install .btn ul li:first-child {margin-left:0;}
.layout-install .btn ul li a {text-decoration:none; display:inline-block; width:200px; text-align:center; border-radius:5px; background-color:#ffffff; cursor:pointer;}
.layout-install .btn ul li a {font-weight:700; line-height:50px; font-size:20px; color:#2c9ecf; font-family:'Nanum Gothic',gulim,serif;}
.layout-install .btn ul li a:hover {background-color:#deff00;}
.layout-install .btn ul li a:active {background-color:#252525;}

@media screen and (max-width:640px) {

.layout-install {padding-left:10px; padding-right:10px;}
.layout-install .process ul li {display:block;}
.layout-install .process ul:last-child li {margin:10px 0 0 0;}
.layout-install .btn ul li {display:block; margin-left:0; margin-top:20px;}

}
</style>
</head>
<body>
<div class="layout-install">
<div class="wrap">
<div class="logo on"></div>
<div class="title">START THE TERABOARD INSTALL</div>
<div class="process">
<ul>
<li>Install Process</li>
</ul>
<ul>
<li>1. 약관 동의</li>
<li>2. 정보입력</li>
<li class="on">3. 설치 완료</li>
</ul>
</div>
<div class="msg">
<p class="subj">기본 설치가 완료되었습니다.</p>
<p class="text">
<br />
<!-- start //-->
이제, 템플릿과 스킨을 선택하여 자유롭게 나만의 홈페이지를 직접 꾸며보세요.<br />
참고로 가장 쉽고, 중요한 일은 기본 환경설정과 운영정보를 등록하는 일입니다.<br />
힘들고 어려운 일이 생기면 언제든 www.teraboard.net 찾아주세요.<br />
테라보드의 개발진은 당신의 성공적인 온라인 서비스 런칭을 위해 노력하겠습니다.<br />
감사합니다.<br />
<!-- end //-->
</p>
</div>
<form method="post" name="formInstall" autocomplete="off">
<input type="hidden" id="btn" name="btn" value="" />
<div class="btn">
<ul>
<li><a href="<?=$web['host_default']?>/">홈페이지</a></a>
<li><a href="<?=$web['host_adm']?>/">관리자 모드</a></a>
</ul>
</div>
</form>
</div>
</div>
</body>
</html>